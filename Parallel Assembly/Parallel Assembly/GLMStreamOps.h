#pragma once
#ifndef GLM_STREAM_OPS_H
#define GLM_STREAM_OPS_H

#include <iosfwd>

#include "GLMIncludes.h"

namespace glm {

std::ostream& operator<<(std::ostream& out, const ivec2& v);
std::ostream& operator<<(std::ostream& out, const ivec3& v);
std::ostream& operator<<(std::ostream& out, const ivec4& v);

std::ostream& operator<<(std::ostream& out, const vec2& v);
std::ostream& operator<<(std::ostream& out, const vec3& v);
std::ostream& operator<<(std::ostream& out, const vec4& v);

std::ostream& operator<<(std::ostream& out, const mat2& m);
std::ostream& operator<<(std::ostream& out, const mat3& m);
std::ostream& operator<<(std::ostream& out, const mat4& m);

std::ostream& operator<<(std::ostream& out, const quat& q);
std::ostream& operator<<(std::ostream& out, const dualquat& dq);

} // namespace glm

#endif // GLM_STREAM_OPS_H
