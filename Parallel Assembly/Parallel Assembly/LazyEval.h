#pragma once
#ifndef LAZY_EVAL_H
#define LAZY_EVAL_H

#include <functional>

#include "Flag.h"

template <typename T>
class LazyEval final {
  mutable Flag m_invalid;
  std::function<T()> m_fastGet;
  std::function<T()> m_slowGet;
  std::function<void(const T&)> m_store;

public:
  template <typename FastGet, typename SlowGet, typename Store>
  LazyEval(bool valid, FastGet&& fastGet, SlowGet&& slowGet, Store&& store)
    : m_invalid(!valid)
    , m_fastGet(std::forward<FastGet>(fastGet))
    , m_slowGet(std::forward<SlowGet>(slowGet))
    , m_store(std::forward<Store>(store))
  {}
  LazyEval(const LazyEval& o) = default;
  LazyEval(LazyEval&& o) = default;
  ~LazyEval() = default;

  LazyEval& operator=(const LazyEval&) = default;
  LazyEval& operator=(LazyEval&& o) = default;

  T get() const {
    if (m_invalid.get()) {
      auto result = m_slowGet();
      m_store(result);
      m_invalid.clear();
      return result;
    } else
      return m_fastGet();
  }
  void store(const T& value) {
    m_store(value);
    m_invalid.clear();
  }
  void update() const {
    if (m_invalid.get()) {
      auto result = m_slowGet();
      m_store(result);
      m_invalid.clear();
    }
  }

  void invalidate() { m_invalid.set(); }
};

#endif // LAZY_EVAL_H
