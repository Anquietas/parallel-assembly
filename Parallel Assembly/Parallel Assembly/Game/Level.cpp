#include "Level.h"

#include "../Assert.h"
#include "../Logger.h"
#include "../StringUtils.h"

namespace game {

namespace {
STATIC_LOGGER_DECLARE(logger);
}

Level::Level(std::string name_, std::string id_, boost::filesystem::path directory_)
  : m_name(std::move(name_))
  , m_id(std::move(id_))
  , m_directory(std::move(directory_))
  , m_levelInfoFilePath(m_directory / levelInfoFileName)
  , m_loaded(false)
{
  ASSERT_MSG(utf32Length(m_name) <= maxNameUTF32Length,
             "Level constructed with name exceeding max utf32 length");
  ASSERT_MSG(utf32Length(m_id) <= maxIDUTF32Length,
             "Level constructed with id exceeding max utf32 length");
}

void Level::loadLevel() {
  if (!loaded()) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Loading level \"" << name() << '\"';
    loadLevelImpl();
    setLoaded();
  }
  ASSERT_MSG(loaded(), "Level not loaded after loadLevel()");
}

void Level::unloadLevel() {
  if (loaded()) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Unloading level \"" << name() << '\"';
    unloadLevelImpl();
    clearLoaded();
  }
  ASSERT_MSG(!loaded(), "Level loaded after unloadLevel()");
}

namespace {
STATIC_LOGGER_INIT(logger) {
  logging::Logger lg;
  logging::setModuleName(lg, "Level");
  return lg;
}
}

} // namespace game
