#pragma once
#ifndef GAME_WORKER_H
#define GAME_WORKER_H

#include "../GLMIncludes.h"

#include "Direction.h"

namespace game {

class LevelGrid;

class Worker final {
  std::size_t m_id;
  glm::ivec2 m_initialPosition;
  glm::ivec2 m_position;
  Direction m_initialDirection;
  Direction m_direction;

public:
  static constexpr std::size_t maxWorkers = 4u;

  Worker(std::size_t id);
  Worker(const Worker& o) = default;
  Worker(Worker&& o) noexcept = default;
  ~Worker() = default;

  Worker& operator=(const Worker&) = default;
  Worker& operator=(Worker&& o) noexcept = default;

  std::size_t id() const noexcept { return m_id; }

  void setInitialPosition(const glm::ivec2& pos) noexcept { m_initialPosition = pos; }
  const glm::ivec2& initialPosition() const noexcept { return m_initialPosition; }

  void setPosition(const glm::ivec2& pos) noexcept { m_position = pos; }
  const glm::ivec2& position() const noexcept { return m_position; }

  void setInitialDirection(Direction dir) noexcept { m_initialDirection = dir; }
  Direction initialDirection() const noexcept { return m_initialDirection; }

  void setDirection(Direction dir) noexcept { m_direction = dir; }
  Direction direction() const noexcept { return m_direction; }

  // Runs one step of the simulation for this worker in the grid.
  // This assumes the worker is in the given grid.
  void advance(LevelGrid& grid);

  // Restores the worker to its initial position and direction
  void reset() noexcept;
};

} // namespace game

#endif // GAME_WORKER_H
