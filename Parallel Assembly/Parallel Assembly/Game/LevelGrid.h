#pragma once
#ifndef GAME_LEVEL_GRID_H
#define GAME_LEVEL_GRID_H

#include "../ConstPropPtr.h"
#include "../GLMIncludes.h"

#include "GridCell.h"

namespace game {

class LevelGrid final {
  class Impl;

  const_prop_ptr<Impl> m_impl;
public:
  static const glm::ivec2 maxSize; // (36, 24)

  LevelGrid();
  LevelGrid(std::size_t width, std::size_t height);
  LevelGrid(const glm::ivec2& size);
  LevelGrid(const LevelGrid& o);
  LevelGrid(LevelGrid&& o) noexcept;
  ~LevelGrid();

  LevelGrid& operator=(const LevelGrid& o);
  LevelGrid& operator=(LevelGrid&& o) noexcept;

  void setGridSize(std::size_t width, std::size_t height);
  void setGridSize(const glm::ivec2& size);
  glm::ivec2 gridSize() const noexcept;

  void setNumWorkers(std::size_t numWorkers);

  GridCell& operator()(std::size_t x, std::size_t y);
  const GridCell& operator()(std::size_t x, std::size_t y) const;

  GridCell& operator()(const glm::ivec2& position);
  const GridCell& operator()(const glm::ivec2& position) const;

  // Resets all GridCells to initial state
  void reset();
};

} // namespace game

#endif // GAME_LEVEL_GRID_H
