#pragma once
#ifndef GAME_LEVEL_SPEED_H
#define GAME_LEVEL_SPEED_H

namespace game {

enum class LevelSpeed {
  Pause = 0,
  Step,
  Play,
  Fast,
  VeryFast,
};

} // namespace game

#endif // GAME_LEVEL_SPEED_H
