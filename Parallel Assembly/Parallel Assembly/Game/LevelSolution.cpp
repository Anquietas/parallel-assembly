#include "LevelSolution.h"

#include <sstream>

#include <boost/filesystem.hpp>

#include "../Assert.h"
#include "../Exception.h"
#include "../Logger.h"
#include "../StringUtils.h"

#include "Profile.h"

namespace game {

namespace fs = boost::filesystem;

namespace {
STATIC_LOGGER_DECLARE(logger);
}

LevelSolution::LevelSolution(Profile& profile_,
                             std::string name_,
                             fs::path filepath_)
  : m_profile(&profile_)
  , m_name(std::move(name_))
  , m_filepath(std::move(filepath_))
  , m_loaded(false)
{
  ASSERT_MSG(utf32Length(m_name) <= maxNameUTF32Length,
             "LevelSolution constructed with name exceeding max utf32 length");
  ASSERT(m_filepath.has_parent_path());

  auto levelDirectory = filepath().parent_path();
  auto directoryStatus = fs::status(levelDirectory);
  if (!fs::is_directory(directoryStatus)) {
    if (fs::exists(directoryStatus)) {
      std::ostringstream ss;
      ss << "LevelSolution \"" << name() << "\" constructed with filepath " << filepath();
      ss << "; level directory path " << levelDirectory << " exists and is not a directory. Please delete it";
      throw MAKE_EXCEPTION(ss.str());
    }
    bool result = fs::create_directories(levelDirectory);
    if (!result) {
      std::ostringstream ss;
      ss << "LevelSolution \"" << name() << "\" constructed with filepath " << filepath();
      ss << "; failed to create level directory " << levelDirectory;
      throw MAKE_EXCEPTION(ss.str());
    }
  }
}

void LevelSolution::setName(std::string name_) {
  m_name = std::move(name_);
}

void LevelSolution::loadSolution() {
  if (!loaded()) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Loading Solution \"" << name() << "\" for level \"" << level().name() << '\"';
    loadSolutionImpl();
    setLoaded();
  }
  ASSERT_MSG(loaded(), "LevelSolution not loaded after loadSolution()");
}
void LevelSolution::unloadSolution() {
  if (loaded()) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Unloading Solution \"" << name() << "\" for level \"" << level().name() << '\"';
    unloadSolutionImpl();
    clearLoaded();
  }
  ASSERT_MSG(!loaded(), "LevelSolution loaded after unloadSolution()");
}
void LevelSolution::saveSolution() const {
  LOG_SEVERITY(logger::get(), Debug)
    << "Saving Solution \"" << name() << "\" for level \"" << level().name() << '\"';
  saveSolutionImpl();
}

void LevelSolution::initializeEmptySolution() {
  ASSERT_MSG(!loaded(), "LevelSolution::initializeEmptySolution called on loaded solution");
  
  LOG_SEVERITY(logger::get(), Debug)
    << "Initializing Solution \"" << name() << "\" for level \"" << level().name() << "\" as empty solution \"";
  initializeEmptySolutionImpl();
  setLoaded();

  ASSERT_MSG(loaded(), "LevelSolution not loaded after initializeEmptySolution()");
}
void LevelSolution::saveNewlyCreatedSolution() {
  ASSERT(!loaded());

  initializeEmptySolution();
  saveSolution();
  unloadSolution();

  ASSERT(!loaded());
}

namespace {
STATIC_LOGGER_INIT(logger) {
  logging::Logger lg;
  logging::setModuleName(lg, "LevelSolution");
  return lg;
}
}

} // namespace game
