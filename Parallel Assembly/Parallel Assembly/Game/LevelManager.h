#pragma once
#ifndef GAME_LEVEL_MANAGER_H
#define GAME_LEVEL_MANAGER_H

#include <memory>
#include <vector>
#include <map>
#include <type_traits>

#include <boost/container/flat_map.hpp>
#include <boost/graph/adjacency_list.hpp>

#include "../Logger.h"

#include "Level.h"

namespace game {

class LevelFactory;

class LevelManager final {
public:
  using FactoryMap = boost::container::flat_map<std::string, std::unique_ptr<LevelFactory>>;
  using LookupMap = std::map<std::string, std::size_t>;
  using LevelGraph = boost::adjacency_list<
    boost::vecS, boost::vecS, boost::directedS,
    boost::no_property, boost::no_property, boost::no_property, boost::vecS>;

private:
  mutable logging::Logger m_logger;

  FactoryMap m_levelFactories;

  boost::filesystem::path m_levelsDirectory;
  boost::filesystem::path m_levelsInfoFilePath;

  std::vector<std::unique_ptr<Level>> m_levels;
  LookupMap m_lookupMap;
  LevelGraph m_dependencyGraph;

  void registerLevelTypes();
  void loadLevelsData();
  void logLevelData() const;

public:
  LevelManager();
  LevelManager(const LevelManager&) = delete;
  LevelManager(LevelManager&&) = delete;
  ~LevelManager();

  LevelManager& operator=(const LevelManager&) = delete;
  LevelManager& operator=(LevelManager&&) = delete;

  static constexpr char* levelsDirectoryPath = "./Assets/Levels";
  static constexpr char* levelsInfoFileName = "levelsInfo.xml";

  template <typename LevelFactoryType>
  std::enable_if_t<std::is_base_of<LevelFactory, LevelFactoryType>::value>
  addLevelFactory(std::string name);
  template <typename LevelType>
  std::enable_if_t<std::is_base_of<Level, LevelType>::value>
  addLevelType();

  std::size_t numLevels() const noexcept { return m_levels.size(); }

  // Returns the level ID (in the range [0, size) ) corresponding to the given
  // name if it exists. If it does not exist, returns -1 instead.
  std::size_t lookupLevelID(const std::string& name) const;
  // Returns the level corresponding to the given name if it exists.
  // If it does not exist, returns nullptr.
  // returned Level is non-const to allow loading/unloading
  Level* lookupLevelByName(const std::string& name) const;
  // Returns the level corresponding to the given level ID.
  // This assumes the ID is in the range [0, size)
  // returned Level is non-const to allow loading/unloading
  Level* getLevel(std::size_t index) const;

  const LevelGraph& dependencyGraph() const { return m_dependencyGraph; }
};

} // namespace game

#endif // GAME_LEVEL_MANAGER_H
