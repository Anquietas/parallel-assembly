#include "SimpleLevel.h"

#include <cstdlib>
#include <fstream>
#include <iterator>
#include <vector>

#include "../GUI/SceneNames.h"

#include "../Assert.h"
#include "../Exception.h"
#include "../Logger.h"

#include "LevelGrid.h"

#include "XMLHelpers.h"

namespace game {

namespace {
STATIC_LOGGER_DECLARE(logger);
}

using namespace std::string_literals;

const std::string SimpleLevel::levelTypeName{"SimpleLevel"s};

SimpleLevel::SimpleLevel(std::string name_, std::string id_, boost::filesystem::path directory_)
  : Level(std::move(name_), std::move(id_), std::move(directory_))
  , m_gridSize()
  , m_numWorkers(0u)
{
  LOG_SEVERITY(logger::get(), Debug)
    << "Constructed SimpleLevel with name=\"" << name() << "\", directory=\"" << directory();
}

const std::string& SimpleLevel::sceneName() const noexcept {
  return gui::simpleLevelSceneName;
}

namespace {
const std::string simpleLevelNodeName{"SimpleLevel"s};
const std::string gridSizeNodeName{"GridSize"s};
const std::string numWorkersNodeName{"NumWorkers"s};

const std::string widthAttrName{"width"s};
const std::string heightAttrName{"height"s};
const std::string valueAttrName{"value"s};
}

template <typename ErrorHandler>
glm::ivec2 loadGridSizeNode(const xml::xml_node<>& node, const ErrorHandler& errorHandler) {
  auto gridSizeErrHandler = xml::makeErrorHandler(errorHandler,
    gridSizeNodeName, " node: ");

  const xml::xml_attribute<char>
    *widthAttr = nullptr,
    *heightAttr = nullptr;
  std::tie(widthAttr, heightAttr) =
    xml::getAttributes(node, gridSizeErrHandler, widthAttrName, heightAttrName);

  glm::ivec2 result;
  char* end = nullptr;
  
  result.x = static_cast<int>(std::strtol(widthAttr->value(), &end, 10));
  if (end == widthAttr->value() || *end != '\0' ||
      result.x > LevelGrid::maxSize.x || result.x <= 0)
    gridSizeErrHandler(widthAttrName, " is invalid.\n",
      "Valid values are positive integers less than or equal to ", LevelGrid::maxSize.x);

  result.y = static_cast<int>(std::strtol(heightAttr->value(), &end, 10));
  if (end == heightAttr->value() || *end != '\0' ||
      result.y > LevelGrid::maxSize.y || result.y <= 0)
    gridSizeErrHandler(heightAttrName, " is invalid.\n",
      "valid values are positive integers less than or equal to ", LevelGrid::maxSize.y);

  return result;
}

template <typename ErrorHandler>
std::size_t loadNumWorkersNode(const xml::xml_node<>& node, const ErrorHandler& errorHandler) {
  auto numWorkersErrHandler = xml::makeErrorHandler(errorHandler,
    numWorkersNodeName, " node: ");
  auto valueAttr = xml::getAttribute(node, numWorkersErrHandler, valueAttrName);

  char* end = nullptr;
  std::size_t result = std::strtoul(valueAttr->value(), &end, 10);
  if (end == valueAttr->value() || *end != '\0' ||
      result > Worker::maxWorkers || result == 0)
    numWorkersErrHandler(valueAttrName, " is invalid.\n",
      "Valid values are positive integers less than or equal to ", Worker::maxWorkers);

  return result;
}

void SimpleLevel::loadLevelImpl() {
  auto errorHandler = xml::makeErrorHandler("Level \"", name(), "\"'s ",
    Level::levelInfoFileName, " file is invalid:\n");

  std::ifstream levelInfoFile(levelInfoFilePath().string(),
                              std::ios::in | std::ios::binary | std::ios::ate);
  if (!levelInfoFile) {
    std::ostringstream ss;
    ss << "Failed to open " << levelInfoFilePath() << " for Level \"" << name()
       << "\"; please try reinstalling the game and contact support if the issue persists.";
    throw MAKE_EXCEPTION(ss.str());
  }

  auto fileSize = static_cast<std::size_t>(levelInfoFile.tellg());
  levelInfoFile.seekg(0);

  std::vector<char> fileBuffer(fileSize + 1, '\0');
  fileBuffer.assign(std::istreambuf_iterator<char>(levelInfoFile),
                    std::istreambuf_iterator<char>());
  ASSERT(fileBuffer.size() + 1 <= fileBuffer.capacity());
  fileBuffer.push_back('\0');
  levelInfoFile.close();

  xml::xml_document<> xmlDoc;
  xml::parse(xmlDoc, fileBuffer.data(), fileBuffer.size(), errorHandler);

  auto simpleLevelNode = xml::firstNode(xmlDoc, simpleLevelNodeName);
  if (!simpleLevelNode)
    errorHandler("missing ", simpleLevelNodeName, " node");

  auto levelErrHandler = xml::makeErrorHandler(errorHandler,
    simpleLevelNodeName, " node: ");

  auto gridSizeNode = xml::firstNode(simpleLevelNode, gridSizeNodeName);
  if (!gridSizeNode)
    levelErrHandler("missing ", gridSizeNodeName, " node");
  m_gridSize = loadGridSizeNode(*gridSizeNode, levelErrHandler);

  auto numWorkersNode = xml::firstNode(simpleLevelNode, numWorkersNodeName);
  if (!numWorkersNode)
    levelErrHandler("missing ", numWorkersNodeName, " node");
  m_numWorkers = loadNumWorkersNode(*numWorkersNode, levelErrHandler);
}

void SimpleLevel::unloadLevelImpl() {
}

namespace {
STATIC_LOGGER_INIT(logger) {
  logging::Logger lg;
  logging::setModuleName(lg, "SimpleLevel");
  return lg;
}
}

} // namespace game
