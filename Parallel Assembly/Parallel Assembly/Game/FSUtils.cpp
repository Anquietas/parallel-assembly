#include "FSUtils.h"

#include <sstream>

#include <boost/filesystem.hpp>

#include "../Assert.h"

namespace game {

std::string sanitizeName(const std::string& name) {
  std::string sanitized;
  sanitized.reserve(name.size());
  for (char c : name) {
    switch (c) {
    case ' ':
    case '\"':
    case '*':
    case '/':
    case ':':
    case '<':
    case '>':
    case '?':
    case '\\':
    case '|':
      sanitized.push_back('_');
      break;
    default:
      if ('\0' <= c && c <= '\31')
        sanitized.push_back('_');
      else
        sanitized.push_back(c);
    }
  }
  ASSERT_MSG(sanitized.size() == name.size(),
             "Sanitized name length doesn't match original name length");
  return sanitized;
}

// TODO: optimize to avoid unnecessary allocations (not a priority)
template <typename F>
fs::path getSanitizedPath(const fs::path& baseDir, const std::string& name, F&& buildItemName) {
  auto sanitized = sanitizeName(name);

  fs::path path{baseDir / buildItemName(sanitized)};
  std::ostringstream idBuffer;
  unsigned int id = 1u;
  while (fs::exists(path)) {
    idBuffer.seekp(0, std::ios::beg);
    idBuffer << '(' << id << ')';
    path = baseDir / buildItemName(sanitized, idBuffer.str());
    ++id;
  }
  return path;
}

fs::path getSanitizedDirectory(const fs::path& baseDir, const std::string& name) {
  struct Builder {
    inline const std::string& operator()(const std::string& sanitized) const {
      return sanitized;
    }
    inline std::string operator()(const std::string& sanitized, const std::string& idStr) const {
      return sanitized + idStr;
    }
  };
  return getSanitizedPath(baseDir, name, Builder{});
}
fs::path getSanitizedFilePath(const fs::path& baseDir,
                              const std::string& name,
                              const std::string& suffix)
{
  struct Builder {
    const std::string& _suffix;
    inline std::string operator()(const std::string& sanitized) const {
      return sanitized + _suffix;
    }
    inline std::string operator()(const std::string& sanitized, const std::string& idStr) const {
      return sanitized + idStr + _suffix;
    }
  };
  return getSanitizedPath(baseDir, name, Builder{suffix});
}

} // namespace game
