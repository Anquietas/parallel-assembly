#pragma once
#ifndef GAME_GRID_CELL_H
#define GAME_GRID_CELL_H

#include <boost/container/static_vector.hpp>

#include "Direction.h"
#include "Worker.h"

namespace game {

class GridCell final {
  template <typename T, std::size_t Capacity>
  using static_vector = boost::container::static_vector<T, Capacity>;
  using DirectionVec = static_vector<Direction, Worker::maxWorkers>;

  DirectionVec m_directions;

public:
  GridCell();
  GridCell(const GridCell& o) = default;
  GridCell(GridCell&& o) = default;
  ~GridCell() = default;

  GridCell& operator=(const GridCell& o) = default;
  GridCell& operator=(GridCell&& o) = default;

  void setNumWorkers(std::size_t numWorkers);

  Direction direction(std::size_t workerID) const noexcept;
  void setDirection(std::size_t workerID, Direction direction) noexcept;

  void execute(Worker& worker);

  // Resets all instructions to their initial state
  void reset();
};

} // namespace game

#endif // GAME_GRID_CELL_H
