#pragma once
#ifndef GAME_FS_UTILS_H
#define GAME_FS_UTILS_H

#include <boost/filesystem/path.hpp>

namespace game {

namespace fs = boost::filesystem;

fs::path getSanitizedDirectory(const fs::path& baseDir, const std::string& name);
fs::path getSanitizedFilePath(const fs::path& baseDir,
                              const std::string& name,
                              const std::string& suffix);

} // namespace game

#endif // GAME_FS_UTILS
