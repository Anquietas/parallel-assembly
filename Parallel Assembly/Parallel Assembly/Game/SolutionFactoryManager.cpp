#include "SolutionFactoryManager.inl"

#include "SimpleLevelSolution.h"

#include "../Assert.h"

namespace game {

SolutionFactoryManager::SolutionFactoryManager()
  : m_solutionFactories()
  , m_levelSolutionFactoryMap()
{
  registerSolutionTypes();
}

void SolutionFactoryManager::registerSolutionTypes() {
  addSolutionType<SimpleLevelSolution>();
}

const LevelSolutionFactory& SolutionFactoryManager::getSolutionFactory(const Level& level) const {
  auto& levelTypeName = level.typeName();
  auto levelSolutionFactoryIter = m_levelSolutionFactoryMap.find(levelTypeName);
  if (levelSolutionFactoryIter == m_levelSolutionFactoryMap.end()) {
    std::ostringstream ss;
    ss << "No level solution factory registered for level type \"" << levelTypeName << '\"';
    throw MAKE_EXCEPTION(ss.str());
  }
  auto& solutionTypeName = levelSolutionFactoryIter->second;
  auto factoryIter = m_solutionFactories.find(solutionTypeName);
  // If there's a map from level type to solution type, there should also be a factory
  // for that type since they're set at the same time by addSolutionFactory
  ASSERT(factoryIter != m_solutionFactories.end());

  return *factoryIter->second;
}

} // namespace game
