#pragma once
#ifndef GAME_LEVEL_MANAGER_INL
#define GAME_LEVEL_MANAGER_INL

#include "LevelManager.h"

#include <sstream>

#include <boost/core/ignore_unused.hpp>

#include "../Assert.h"
#include "../Exception.h"

#include "LevelFactory.h"

namespace game {

template <typename LevelFactoryType>
std::enable_if_t<std::is_base_of<LevelFactory, LevelFactoryType>::value>
LevelManager::addLevelFactory(std::string name) {
  if (m_levelFactories.find(name) != m_levelFactories.end()) {
    std::ostringstream ss;
    ss << "Failed to create level factory with name \"" << name
       << "\": a factory with that name already exists";
    throw MAKE_EXCEPTION(ss.str());
  }

  auto result = m_levelFactories.emplace(
    std::move(name),
    std::make_unique<LevelFactoryType>()
  );
  ASSERT(result.second); boost::ignore_unused(result);

  LOG_SEVERITY(m_logger, Debug)
    << "Added level factory with name \"" << result.first->first << "\"";
}

template <typename LevelType>
std::enable_if_t<std::is_base_of<Level, LevelType>::value>
LevelManager::addLevelType() {
  addLevelFactory<TemplateLevelFactory<LevelType>>(LevelType::levelTypeName);
}

} // namespace game

#endif // GAME_LEVEL_MANAGER_INL
