#include "LevelGrid.h"

#include <algorithm>

#include <boost/multi_array.hpp>

#include "../Assert.h"
#include "../Logger.h"

namespace game {

namespace {
STATIC_LOGGER_DECLARE(logger);
}

const glm::ivec2 LevelGrid::maxSize{36, 24};

class LevelGrid::Impl final {
  using GridArray = boost::multi_array<GridCell, 2>;
  using index = GridArray::index;

  GridArray m_array;
public:
  Impl()
    : m_array()
  {}
  Impl(std::size_t width, std::size_t height)
    : m_array(boost::extents[width][height])
  {}
  Impl(const Impl& o)
    : m_array(o.m_array)
  {}
  Impl(Impl&&) = delete;
  ~Impl() = default;

  Impl& operator=(const Impl& o) {
    // Need to resize first if grid shapes don't match.
    if (!std::equal(m_array.shape(), m_array.shape() + m_array.num_dimensions(),
                    o.m_array.shape(), o.m_array.shape() + m_array.num_dimensions()))
    {
      auto size = o.gridSize();
      setGridSize(std::size_t(size.x), std::size_t(size.y));
    }
    m_array = o.m_array;
    return *this;
  }
  Impl& operator=(Impl&&) = delete;

  void setGridSize(std::size_t width, std::size_t height) {
    m_array.resize(boost::extents[width][height]);
  }
  glm::ivec2 gridSize() const noexcept {
    static_assert(GridArray::dimensionality == 2, "DERP");
    auto shape = m_array.shape();
    return {int(shape[0]), int(shape[1])};
  }

  void setNumWorkers(std::size_t numWorkers) {
    auto end = m_array.data() + m_array.num_elements();
    for (auto iter = m_array.data(); iter != end; ++iter)
      iter->setNumWorkers(numWorkers);
  }

  GridCell& at(std::size_t x, std::size_t y) {
    static_assert(GridArray::dimensionality == 2, "DERP");
    ASSERT(0u <= x && x < m_array.shape()[0]);
    ASSERT(0u <= y && y < m_array.shape()[1]);
    return m_array[index(x)][index(y)];
  }
  const GridCell& at(std::size_t x, std::size_t y) const {
    static_assert(GridArray::dimensionality == 2, "DERP");
    ASSERT(0u <= x && x < m_array.shape()[0]);
    ASSERT(0u <= y && y < m_array.shape()[1]);
    return m_array[index(x)][index(y)];
  }

  GridCell& at(const glm::ivec2& position) {
    ASSERT(glm::all(glm::greaterThanEqual(position, glm::ivec2())));
    ASSERT(glm::all(glm::lessThan(position, gridSize())));
    return m_array[index(position.x)][index(position.y)];
  }
  const GridCell& at(const glm::ivec2& position) const {
    ASSERT(glm::all(glm::greaterThanEqual(position, glm::ivec2())));
    ASSERT(glm::all(glm::lessThan(position, gridSize())));
    return m_array[index(position.x)][index(position.y)];
  }

  void reset() {
    auto end = m_array.data() + m_array.num_elements();
    for (auto iter = m_array.data(); iter != end; ++iter)
      iter->reset();
  }
};

LevelGrid::LevelGrid()
  : m_impl()
{}
LevelGrid::LevelGrid(std::size_t width, std::size_t height)
  : m_impl(make_const_prop_ptr<Impl>(width, height))
{
  ASSERT_MSG(glm::all(glm::lessThanEqual(glm::ivec2{width, height}, maxSize)),
             "LevelGrid constructed with invalid size");
  LOG_SEVERITY(logger::get(), Debug)
    << "LevelGrid constructed with size " << width << " x " << height;
}
LevelGrid::LevelGrid(const glm::ivec2& size)
  : LevelGrid(std::size_t(size.x), std::size_t(size.y))
{
  ASSERT_MSG(glm::all(glm::greaterThan(size, {})),
             "LevelGrid constructed with invalid size");
}
LevelGrid::LevelGrid(const LevelGrid& o)
  : m_impl((ASSERT(o.m_impl), make_const_prop_ptr<Impl>(*o.m_impl)))
{
  ASSERT(m_impl && o.m_impl);
}
LevelGrid::LevelGrid(LevelGrid&& o) noexcept
  : m_impl(std::move(o.m_impl))
{
  ASSERT(m_impl && !o.m_impl);
}
LevelGrid::~LevelGrid() = default;

LevelGrid& LevelGrid::operator=(const LevelGrid& o) {
  ASSERT(o.m_impl);
  ASSERT_MSG(m_impl != o.m_impl, "Assigned LevelGrid to itself");

  *m_impl = *o.m_impl;

  ASSERT_MSG(m_impl != o.m_impl, "LevelGrid copy assignment copied pointer");
  ASSERT(m_impl && o.m_impl);
  return *this;
}
LevelGrid& LevelGrid::operator=(LevelGrid&& o) noexcept {
  ASSERT(o.m_impl);
  ASSERT_MSG(m_impl != o.m_impl, "Move-assigned LevelGrid to itself");

  m_impl = std::move(o.m_impl);

  ASSERT_MSG(m_impl && !o.m_impl, "LevelGrid move assignment failed to invalidate moved-from pointer");
  return *this;
}

void LevelGrid::setGridSize(std::size_t width, std::size_t height) {
  ASSERT_MSG(glm::all(glm::lessThanEqual(glm::ivec2{width, height}, maxSize)),
             "LevelGrid::setGridSize() called with invalid size");

  if (!m_impl)
    m_impl = make_const_prop_ptr<Impl>(width, height);
  else
    m_impl->setGridSize(width, height);

  LOG_SEVERITY(logger::get(), Debug)
    << "LevelGrid set grid size to " << width << " x " << height;
}
void LevelGrid::setGridSize(const glm::ivec2& size) {
  ASSERT_MSG(glm::all(glm::greaterThanEqual(size, glm::ivec2())),
             "LevelGrid::setGridSize() called with invalid size");
  setGridSize(std::size_t(size.x), std::size_t(size.y));
}
glm::ivec2 LevelGrid::gridSize() const noexcept {
  return m_impl->gridSize();
}

void LevelGrid::setNumWorkers(std::size_t numWorkers) {
  m_impl->setNumWorkers(numWorkers);
  LOG_SEVERITY(logger::get(), Debug)
    << "LevelGrid set num workers to " << numWorkers;
}

GridCell& LevelGrid::operator()(std::size_t x, std::size_t y) {
  return m_impl->at(x, y);
}
const GridCell& LevelGrid::operator()(std::size_t x, std::size_t y) const {
  return m_impl->at(x, y);
}

GridCell& LevelGrid::operator()(const glm::ivec2& position) {
  return m_impl->at(position);
}
const GridCell& LevelGrid::operator()(const glm::ivec2& position) const {
  return m_impl->at(position);
}

void LevelGrid::reset() {
  m_impl->reset();
}

namespace {
STATIC_LOGGER_INIT(logger) {
  logging::Logger lg;
  logging::setModuleName(lg, "LevelGrid");
  return lg;
}
}

} // namespace game
