#include "Worker.h"

#include "LevelGrid.h"

#include "../Assert.h"

namespace game {

Worker::Worker(std::size_t id)
  : m_id(id)
  , m_initialPosition()
  , m_position(m_initialPosition)
  , m_initialDirection(Direction::Right)
  , m_direction(m_initialDirection)
{}

void Worker::advance(LevelGrid& grid) {
  ASSERT(glm::all(glm::greaterThanEqual(m_position, glm::ivec2())));
  ASSERT(glm::all(glm::lessThan(m_position, grid.gridSize())));
  ASSERT(m_direction != Direction::None); // Should be initialized before calling

  auto& cell = grid(m_position);
  cell.execute(*this);

  auto delta = getStepOffset(m_direction);
  ASSERT(delta != glm::ivec2{});
  m_position = glm::clamp(m_position + delta, glm::ivec2(), grid.gridSize() - glm::ivec2{1, 1});
}

void Worker::reset() noexcept {
  m_position = initialPosition();
  m_direction = initialDirection();
}

} // namespace game
