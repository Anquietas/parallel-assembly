#pragma once
#ifndef GAME_LEVEL_H
#define GAME_LEVEL_H

#include <string>

#include <boost/filesystem/path.hpp>

namespace game {

class Level {
  std::string m_name;
  std::string m_id;
  boost::filesystem::path m_directory;
  boost::filesystem::path m_levelInfoFilePath;
  bool m_loaded;

  void setLoaded() noexcept { m_loaded = true; }
  void clearLoaded() noexcept { m_loaded = false; }

  virtual void loadLevelImpl() = 0;
  virtual void unloadLevelImpl() = 0;

protected:
  const boost::filesystem::path& levelInfoFilePath() const noexcept { return m_levelInfoFilePath; }

  Level(std::string name_, std::string id_, boost::filesystem::path directory_);
public:
  static constexpr std::size_t maxNameUTF32Length = 16u;
  static constexpr std::size_t maxIDUTF32Length = 3u;

  static constexpr char* levelInfoFileName = "levelInfo.xml";

  Level(const Level&) = delete;
  Level(Level&&) = delete;
  virtual ~Level() = default;

  Level& operator=(const Level&) = delete;
  Level& operator=(Level&&) = delete;

  const std::string& name() const noexcept { return m_name; }
  // name is immutable

  const std::string& id() const noexcept { return m_id; }
  // id string is immutable

  const boost::filesystem::path& directory() const noexcept { return m_directory; }
  // level directory is immutable

  // Should return a unique name for the most-derived type for the purpose
  // of looking up solution factories and similar
  virtual const std::string& typeName() const noexcept = 0;
  // Should return the name of the corresponding UI scene to use.
  virtual const std::string& sceneName() const noexcept = 0;

  bool loaded() const noexcept { return m_loaded; }

  void loadLevel();
  void unloadLevel();
};

} // namespace game

#endif // GAME_LEVEL_H
