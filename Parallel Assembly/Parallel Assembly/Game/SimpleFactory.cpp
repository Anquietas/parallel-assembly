#include "SimpleFactory.h"

#include <algorithm>

#include "../Assert.h"

namespace game {

SimpleFactory::SimpleFactory(std::size_t width, std::size_t height, std::size_t numWorkers)
  : m_grid(width, height)
  , m_workers()
{
  m_grid.setNumWorkers(numWorkers);
  for (std::size_t id = 0; id < numWorkers; ++id)
    m_workers.emplace_back(id);

  ASSERT(m_workers.size() == numWorkers);
  ASSERT(std::all_of(
    m_workers.begin(), m_workers.end(),
    [numWorkers](const auto& w) -> bool {
      return 0 <= w.id() && w.id() < numWorkers;
    }));
  ASSERT(std::is_sorted(
    m_workers.begin(), m_workers.end(),
    [](const auto& x, const auto& y) -> bool {
      return x.id() < y.id();
    }));
}
SimpleFactory::SimpleFactory(const glm::ivec2& gridSize, std::size_t numWorkers)
  : SimpleFactory(std::size_t(gridSize.x), std::size_t(gridSize.y), numWorkers)
{}

void SimpleFactory::advance(std::size_t numSteps) {
  for (std::size_t i = 0; i < numSteps; ++i) {
    for (auto& worker : m_workers)
      worker.advance(m_grid);
  }
}

void SimpleFactory::reset() {
  for (auto& worker : m_workers)
    worker.reset();
  m_grid.reset();
}

} // namespace game
