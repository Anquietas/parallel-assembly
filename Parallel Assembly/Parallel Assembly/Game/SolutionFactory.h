#pragma once
#ifndef GAME_SOLUTION_FACTORY_H
#define GAME_SOLUTION_FACTORY_H

#include "LevelSolution.h"

namespace game {

class LevelSolutionFactory {
protected:
  LevelSolutionFactory() = default;
public:
  virtual ~LevelSolutionFactory() = default;

  virtual std::unique_ptr<LevelSolution> createSolution(
    const Level& level,
    Profile& profile,
    std::string name,
    boost::filesystem::path filepath) const = 0;
};

template <
  typename SolutionType,
  typename = std::enable_if_t<std::is_base_of<LevelSolution, SolutionType>::value>
>
class TemplateSolutionFactory : public LevelSolutionFactory {
public:
  TemplateSolutionFactory() = default;
  virtual ~TemplateSolutionFactory() override = default;

  virtual std::unique_ptr<LevelSolution> createSolution(
    const Level& level,
    Profile& profile,
    std::string name,
    boost::filesystem::path filepath) const
  {
    return std::make_unique<SolutionType>(level, profile, std::move(name), std::move(filepath));
  }
};

} // namespace game

#endif // GAME_SOLUTION_FACTORY_H
