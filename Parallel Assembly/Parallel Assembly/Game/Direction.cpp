#include "Direction.h"

#include <boost/container/flat_map.hpp>

#include "../Assert.h"
#include "../Exception.h"

namespace game {

Direction parseDirection(const std::string& direction) {
  static const boost::container::flat_map<std::string, Direction> table{
    {"left", Direction::Left},
    {"right", Direction::Right},
    {"up", Direction::Up},
    {"down", Direction::Down},
  };

  auto iter = table.find(direction);
  if (iter == table.end())
    return Direction::None;
  else
    return iter->second;
}

const std::string& toString(Direction direction) {
  static const boost::container::flat_map<Direction, std::string> table{
    {Direction::Left, "left"},
    {Direction::Right, "right"},
    {Direction::Up, "up"},
    {Direction::Down, "down"},
  };

  auto iter = table.find(direction);
  if (iter == table.end()) {
    ASSERT_MSG(direction == Direction::None, "Unhandled direction in toString(direction)");
    throw MAKE_EXCEPTION("Can't convert Direction::None to string");
  } else
    return iter->second;
}

glm::ivec2 getStepOffset(Direction direction) {
  switch (direction) {
  case Direction::None:
    return {0, 0};
  case Direction::Left:
    return {-1, 0};
  case Direction::Right:
    return {1, 0};
  case Direction::Up:
    return {0, -1};
  case Direction::Down:
    return {0, 1};
  default:
    ASSERT_MSG(false, "Unhandled direction in getStepOffset(direction)");
    return {0, 0};
  }
}

Direction getOpposite(Direction direction) {
  switch (direction) {
  case Direction::None:
    return Direction::None;
  case Direction::Left:
    return Direction::Right;
  case Direction::Right:
    return Direction::Left;
  case Direction::Up:
    return Direction::Down;
  case Direction::Down:
    return Direction::Up;
  default:
    ASSERT_MSG(false, "Unhandled direction in getOpposite(direction)");
    return Direction::None;
  }
}

Direction rotateCW(Direction direction) {
  switch (direction) {
  case Direction::None:
    return Direction::None;
  case Direction::Left:
    return Direction::Up;
  case Direction::Right:
    return Direction::Down;
  case Direction::Up:
    return Direction::Right;
  case Direction::Down:
    return Direction::Left;
  default:
    ASSERT_MSG(false, "Unhandled direction in rotateCW(direction)");
    return Direction::None;
  }
}

Direction rotateCCW(Direction direction) {
  switch (direction) {
  case Direction::None:
    return Direction::None;
  case Direction::Left:
    return Direction::Down;
  case Direction::Right:
    return Direction::Up;
  case Direction::Up:
    return Direction::Left;
  case Direction::Down:
    return Direction::Right;
  default:
    ASSERT_MSG(false, "Unhandled direction in rotateCCW(direction)");
    return Direction::None;
  }
}

bool isPerpendicular(Direction direction, Direction otherDirection) {
  return rotateCW(direction) == otherDirection ||
         rotateCCW(direction) == otherDirection;
}

} // namespace game
