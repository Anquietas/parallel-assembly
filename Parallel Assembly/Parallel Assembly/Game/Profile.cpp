#include "Profile.h"

#include <fstream>
#include <sstream>
#include <iterator>
#include <vector>

#include <boost/core/ignore_unused.hpp>
#include <boost/filesystem.hpp>

#include <rapidxml_print.hpp>

#include "../Assert.h"
#include "../Exception.h"
#include "../Logger.h"
#include "../StringUtils.h"

#include "FSUtils.h"
#include "LevelManager.h"
#include "SolutionFactory.h"
#include "SolutionFactoryManager.h"
#include "XMLHelpers.h"

namespace game {

namespace {
STATIC_LOGGER_DECLARE(logger);
}

using namespace std::string_literals;

namespace fs = boost::filesystem;

Profile::Profile(
  const LevelManager& levelManager,
  const SolutionFactoryManager& solutionFactoryManager,
  std::string name_,
  boost::filesystem::path directory_)
  : m_levelManager(&levelManager)
  , m_solutionFactoryManager(&solutionFactoryManager)
  , m_name(std::move(name_))
  , m_directory(std::move(directory_.make_preferred()))
  , m_profileInfoFilePath(m_directory / profileInfoFileName)
  , m_loaded(false)
  , m_completedLevels()
  , m_levelSolutions()
{
  ASSERT_MSG(utf32Length(m_name) <= maxNameUTF32Length, "name passed to Profile constructor exceeds max utf32 length");
  auto directoryStatus = fs::status(m_directory);
  if (!fs::is_directory(directoryStatus)) {
    if (fs::exists(directoryStatus)) {
      std::ostringstream ss;
      ss << "Profile \"" << name() << "\" constructed with directory set to " << directory();
      ss << "; " << directory() << " is not a directory. Please delete it";
      throw MAKE_EXCEPTION(ss.str());
    }
    bool result = fs::create_directories(m_directory);
    if (!result) {
      std::ostringstream ss;
      ss << "Profile \"" << name() << "\" constructed with directory " << directory();
      ss << "; failed to create directory.";
      throw MAKE_EXCEPTION(ss.str());
    }
  }
}

namespace {
const std::string completedNodeName{"CompletedLevels"s};
const std::string levelNodeName{"Level"s};
const std::string levelSolutionsNodeName{"LevelSolutions"s};
const std::string solutionNodeName{"Solution"s};

const std::string nameAttrName{"name"s};
const std::string fileNameAttrName{"filename"s};
}

template <typename ErrorHandler>
void loadCompletedLevels(const xml::xml_document<>& xmlDoc,
                         Profile::CompletedLevelSet& completedLevels,
                         const LevelManager& levelManager,
                         const ErrorHandler& errorHandler)
{
  ASSERT_MSG(completedLevels.empty(), "loadCompletedLevels called with non-empty completedLevels set");

  auto completedNode = xml::firstNode(xmlDoc, completedNodeName);
  if (!completedNode)
    errorHandler(completedNodeName, " node is missing");

  std::size_t levelID = 1u;
  for (auto levelNode = xml::firstNode(completedNode, levelNodeName);
       levelNode != nullptr;
       levelNode = xml::nextSibling(levelNode, levelNodeName), ++levelID)
  {
    auto levelErrHandler = xml::makeErrorHandler(errorHandler,
      completedNodeName, ": ", levelNodeName, " node ", levelID, ": ");

    auto levelName = xml::getAttributeValue(*levelNode, levelErrHandler, nameAttrName);

    auto level = levelManager.lookupLevelByName(levelName);
    if (!level)
      levelErrHandler("refers to non-existent level \"", levelName, "\"");

    auto result = completedLevels.insert(level);
    if (!result.second)
      levelErrHandler("duplicate ", levelNodeName, " (", nameAttrName, " = \"", levelName, "\")");
  }
}

// Returns (name, filename) pair
template <typename ErrorHandler>
std::pair<const xml::xml_attribute<>*, const xml::xml_attribute<>*>
loadSolutionNode(
  const xml::xml_node<>& solutionNode,
  const ErrorHandler& errorHandler)
{
  const xml::xml_attribute<char>
    *nameAttr = nullptr,
    *fileNameAttr = nullptr;
  std::tie(nameAttr, fileNameAttr) =
    xml::getAttributes(solutionNode, errorHandler, nameAttrName, fileNameAttrName);

  auto nameLength = utf32Length(xml::value(nameAttr));
  if (nameLength > LevelSolution::maxNameUTF32Length) {
    errorHandler(nameAttrName, " (\"", nameAttr->value(), "\") exceeds max length (",
      LevelSolution::maxNameUTF32Length, "): length = ", nameLength);
  }

  return {nameAttr, fileNameAttr};
}

template <typename ErrorHandler>
void loadLevelSolutions(const xml::xml_node<>& levelNode,
                        const Level& level,
                        Profile& profile,
                        const fs::path& levelDirectory,
                        const LevelSolutionFactory& solutionFactory,
                        Profile::SolutionMap& solutions,
                        const ErrorHandler& errorHandler)
{
  std::size_t solutionID = 1u;
  for (auto solutionNode = xml::firstNode(levelNode, solutionNodeName);
       solutionNode != nullptr;
       solutionNode = xml::nextSibling(solutionNode, solutionNodeName), ++solutionID)
  {
    auto solutionErrHandler = xml::makeErrorHandler(errorHandler,
      solutionNodeName, ' ', solutionID, ": ");
    auto pair = loadSolutionNode(*solutionNode, solutionErrHandler);
    ASSERT_MSG(pair.first && pair.second, "loadSolutionNode returned null pointers");

    std::string solutionName{xml::value(pair.first)};
    const auto solutionFileName = pair.second->value();

    if (solutions.find(solutionName) != solutions.end())
      solutionErrHandler("duplicate ", solutionNodeName, " definition ", solutionID,
        " (", nameAttrName, " = \"", solutionName, "\")");

    auto solution = solutionFactory.createSolution(
      level, profile, solutionName, levelDirectory / solutionFileName);
    auto result = solutions.emplace(std::move(solutionName), std::move(solution));
    ASSERT(result.second); boost::ignore_unused(result);
  }
}

template <typename ErrorHandler>
void loadSolutionInfo(
  const xml::xml_document<>& xmlDoc,
  Profile& profile,
  const fs::path& profileDirectory,
  Profile::LevelSolutionMap& levelSolutions,
  const LevelManager& levelManager,
  const SolutionFactoryManager& solutionFactoryManager,
  const ErrorHandler& errorHandler)
{
  ASSERT_MSG(levelSolutions.empty(), "loadSolutionInfo called with non-empty levelSolutions map");

  auto levelSolutionsNode = xml::firstNode(xmlDoc, levelSolutionsNodeName);
  if (!levelSolutionsNode)
    errorHandler(levelSolutionsNodeName, " node is missing");
  if (xml::nextSibling(levelSolutionsNode, levelSolutionsNodeName))
    errorHandler("duplicate ", levelSolutionsNodeName, " node");

  std::size_t levelID = 1u;
  for (auto levelNode = xml::firstNode(levelSolutionsNode, levelNodeName);
       levelNode != nullptr;
       levelNode = xml::nextSibling(levelNode, levelNodeName), ++levelID)
  {
    auto levelErrHandler = xml::makeErrorHandler(errorHandler,
      levelSolutionsNodeName, ": ", levelNodeName, " node ", levelID, ": ");

    auto levelName = xml::getAttributeValue(*levelNode, levelErrHandler, nameAttrName);

    auto level = levelManager.lookupLevelByName(levelName);
    if (!level)
      levelErrHandler("refers to non-existent level \"", levelName, '\"');

    auto levelIter = levelSolutions.find(level);
    if (levelIter != levelSolutions.end())
      levelErrHandler(
        "duplicate ", levelNodeName, " node ", levelID,
        " (", nameAttrName, " = \"", levelName, "\")");

    // Construct solution name -> solution map for this level
    auto inserted = levelSolutions.emplace(
      std::piecewise_construct,
      std::forward_as_tuple(level),
      std::forward_as_tuple());
    ASSERT(inserted.second);

    auto levelDirectory = profileDirectory / level->directory().filename();
    auto& solutions = inserted.first->second.map;

    auto& solutionFactory = solutionFactoryManager.getSolutionFactory(*level);

    loadLevelSolutions(
      *levelNode, *level, profile, levelDirectory, solutionFactory, solutions, levelErrHandler);
  }
}

void Profile::loadProfile() {
  ASSERT(m_levelManager);
  ASSERT(m_solutionFactoryManager);

  if (!loaded()) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Loading profile \"" << name() << '\"';

    auto errorHandler = xml::makeErrorHandler(
      "Profile \"", name(), "\"'s ", Profile::profileInfoFileName, " file is invalid:\n");

    // Open profile file at end to get file size
    std::ifstream profileInfoFile(m_profileInfoFilePath.string(),
                                  std::ios::in | std::ios::binary | std::ios::ate);
    if (profileInfoFile) {
      auto fileSize = static_cast<std::size_t>(profileInfoFile.tellg());
      profileInfoFile.seekg(0);

      std::vector<char> fileBuffer(fileSize + 1, '\0');
      fileBuffer.assign(std::istreambuf_iterator<char>(profileInfoFile),
                        std::istreambuf_iterator<char>());
      ASSERT(fileBuffer.size() + 1 <= fileBuffer.capacity());
      fileBuffer.push_back('\0');
      profileInfoFile.close();

      xml::xml_document<> xmlDoc;
      xml::parse(xmlDoc, fileBuffer.data(), fileBuffer.size(), errorHandler);

      loadCompletedLevels(xmlDoc, m_completedLevels, *m_levelManager, errorHandler);
      loadSolutionInfo(xmlDoc, *this, directory(), m_levelSolutions,
                       *m_levelManager, *m_solutionFactoryManager, errorHandler);
    } else {
      LOG_SEVERITY(logger::get(), Warning)
        << "Couldn't open " << m_profileInfoFilePath << " for profile \"" << name()
        << "\"; creating empty profile";
      saveProfile();
    }

    setLoaded();
  }

  ASSERT_MSG(loaded(), "Profile not loaded after loadProfile()");
}

void Profile::unloadProfile() {
  if (loaded()) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Unloading profile \"" << name() << '\"';

    m_completedLevels.clear();
    m_levelSolutions.clear();
    m_completedLevels.shrink_to_fit();
    m_levelSolutions.shrink_to_fit();

    clearLoaded();
  }

  ASSERT_MSG(!loaded(), "Profile loaded after unloadProfile()");
}

void saveCompletedLevels(xml::xml_document<>& xmlDoc,
                         const Profile::CompletedLevelSet& completedLevels)
{
  auto completedNode = xml::allocateNode(xmlDoc, xml::node_element, completedNodeName);
  xmlDoc.append_node(completedNode);

  for (auto level : completedLevels) {
    ASSERT_MSG(level, "completedLevels passed to saveCompletedLevels() contains nullptrs");
    auto& levelName = level->name();

    auto levelNode = xml::allocateNode(xmlDoc, xml::node_element, levelNodeName);
    levelNode->append_attribute(xml::allocateAttribute(xmlDoc, nameAttrName, levelName));

    completedNode->append_node(levelNode);
  }
}
void saveSolutionInfo(xml::xml_document<>& xmlDoc,
                      const Profile::LevelSolutionMap& levelSolutions)
{
  auto levelSolutionsNode = xml::allocateNode(xmlDoc, xml::node_element, levelSolutionsNodeName);
  xmlDoc.append_node(levelSolutionsNode);

  for (auto& levelPair : levelSolutions) {
    auto& levelName = levelPair.first->name();

    auto levelNode = xml::allocateNode(xmlDoc, xml::node_element, levelNodeName);
    levelNode->append_attribute(xml::allocateAttribute(xmlDoc, nameAttrName, levelName));

    levelSolutionsNode->append_node(levelNode);

    auto& solutions = levelPair.second.map;
    for (auto& solutionPair : solutions) {
      ASSERT_MSG(solutionPair.first == solutionPair.second->name(),
                 "levelSolutions map passed to saveSolutionInfo() has elements where key does not match solution name");
      auto& solutionName = solutionPair.first;
      auto solutionFileName = solutionPair.second->filepath().filename().string();

      auto solutionNode = xml::allocateNode(xmlDoc, xml::node_element, solutionNodeName);
      solutionNode->append_attribute(xml::allocateAttribute(xmlDoc, nameAttrName, solutionName));
      solutionNode->append_attribute(
        xml::allocateAttribute(xmlDoc, fileNameAttrName, std::move(solutionFileName)));
      
      levelNode->append_node(solutionNode);
    }
  }
}

void Profile::saveProfile() const {
  // Write to temporary file first to avoid corrupting the original if something goes wrong
  auto tempFilePath = fs::path(m_profileInfoFilePath).concat(".tmp");
  std::ofstream profileInfoFile(tempFilePath.string(),
                                std::ios::out | std::ios::binary | std::ios::trunc);
  if (!profileInfoFile) {
    std::ostringstream ss;
    ss << "Failed to open temporary file " << tempFilePath << " for profile \"" << name()
       << "\" in write mode.\nDid you set an existing one as read-only?";
    throw MAKE_EXCEPTION(ss.str());
  }
  xml::xml_document<> xmlDoc;

  saveCompletedLevels(xmlDoc, m_completedLevels);
  saveSolutionInfo(xmlDoc, m_levelSolutions);

  profileInfoFile << xmlDoc;
  // Close temp file to commit to disk
  profileInfoFile.close();

  boost::system::error_code error;
  // Remove the original if it exists
  if (fs::exists(m_profileInfoFilePath) &&
      !fs::remove(m_profileInfoFilePath, error))
  {
    std::ostringstream ss;
    ss << "Failed to replace save file " << m_profileInfoFilePath << " for profile \""
       << name() << "\": " << error.message()
       << "\nDid you set the file as read-only?\n\n"
       << "Your changes have been saved in " << tempFilePath
       << "; delete " << m_profileInfoFilePath.filename()
       << " and rename " << tempFilePath.filename()
       << " to " << m_profileInfoFilePath.filename() << " to update the file manually.";
    throw MAKE_EXCEPTION(ss.str());
  }
  // Rename temporary to original file name
  fs::rename(tempFilePath, m_profileInfoFilePath);
}

void Profile::deleteProfile() {
  if (loaded())
    unloadProfile();

  ASSERT(fs::exists(m_directory) && fs::is_directory(m_directory));
  fs::remove_all(m_directory);
}

void Profile::setName(std::string name_) {
  m_name = std::move(name_);
}

void Profile::setDirectory(boost::filesystem::path directory_) {
  m_directory = std::move(directory_);
}

void Profile::addCompletedLevel(const Level& level) {
  auto result = m_completedLevels.insert(&level);
  if (!result.second) {
    std::ostringstream ss;
    ss << "Tried to register level \"" << level.name() << "\" as completed in profile \"";
    ss << name() << "\"; level is already registered as completed";
    throw MAKE_EXCEPTION(ss.str());
  }
  saveProfile();
}
bool Profile::isLevelCompleted(const Level& level) const noexcept {
  auto iter = m_completedLevels.find(&level);
  return iter != m_completedLevels.end();
}

bool Profile::isLevelUnlocked(std::size_t levelID) const noexcept {
  ASSERT(m_levelManager);

  auto& depGraph = m_levelManager->dependencyGraph();
  ASSERT_MSG(boost::vertex(levelID, depGraph) == levelID,
             "Level dependency graph vertex descriptors don't match vertex IDs");

  auto outEdges = boost::out_edges(levelID, depGraph);
  
  for (auto iter = outEdges.first; iter != outEdges.second; ++iter) {
    ASSERT_MSG(levelID == boost::source(*iter, depGraph),
               "invalid out edge in level dependency graph");
    auto dependencyID = boost::target(*iter, depGraph);
    auto dependency = m_levelManager->getLevel(dependencyID);
    ASSERT_MSG(dependency, "LevelManager::getLevel() returned nullptr");

    if (!isLevelCompleted(*dependency))
      return false;
  }
  return true;
}
bool Profile::isLevelUnlocked(const Level& level) const {
  ASSERT(m_levelManager);

  // Slightly cheaper computation, plus allows cheating :)
  bool completed = isLevelCompleted(level);
  if (completed) return true;

  const auto levelID = m_levelManager->lookupLevelID(level.name());
  return isLevelUnlocked(levelID);
}

bool Profile::createSolution(Level& level, std::string name_) {
  ASSERT_MSG(!level.loaded(), "Profile::createSolution expects the level passed to be unloaded");
  ASSERT(m_solutionFactoryManager);

  auto& solutions = m_levelSolutions[&level].map;
  auto iter = solutions.find(name_);
  if (iter != solutions.end()) {
    LOG_SEVERITY(logger::get(), Warning)
      << "Tried to create solution \"" << name_ << "\" for level \""
      << level.name() << "\" in profile \"" << name()
      << "\".\nA solution for that level and profile with that name already exists.";
    return false;
  }

  auto levelDirectory = directory() / level.directory().filename();
  auto filePath = getSanitizedFilePath(levelDirectory, name_, ".xml"s);

  auto& solutionFactory = m_solutionFactoryManager->getSolutionFactory(level);
  auto solution = solutionFactory.createSolution(level, *this, name_, std::move(filePath));
  auto result = solutions.emplace(std::move(name_), std::move(solution));
  ASSERT(result.second);

  LOG_SEVERITY(logger::get(), Debug)
    << "Created new solution \"" << result.first->first << "\" for level \"" << level.name()
    << "\" in profile \"" << name() << "\" with filePath = "
    << result.first->second->filepath() << ".";
  // Save solution on creation to save empty solution to disk (ensures duplicate detection will work)
  level.loadLevel();
  result.first->second->saveNewlyCreatedSolution();
  level.unloadLevel();

  saveProfile();

  return true;
}
std::pair<bool, bool> Profile::copySolution(
  const Level& level,
  const std::string& name_,
  std::string copyName)
{
  ASSERT(m_solutionFactoryManager);

  auto mapIter = m_levelSolutions.find(&level);

  bool exists = mapIter != m_levelSolutions.end();
  if (exists) {
    auto& solutions = mapIter->second.map;
    auto iter = solutions.find(name_);
    if (iter == solutions.end())
      exists = false;
    else {
      auto iter2 = solutions.find(copyName);
      if (iter2 != solutions.end()) {
        LOG_SEVERITY(logger::get(), Warning)
          << "Tried to copy solution \"" << name_ << "\" for level \"" << level.name()
          << "\" in profile \"" << name() << "\" to \"" << copyName
          << "\"; a solution with that name already exists.";
        return {true, false};
      }

      const auto origSolution = iter->second.get();

      auto levelDirectory = directory() / level.directory().filename();
      auto copyFilePath = getSanitizedFilePath(levelDirectory, copyName, ".xml"s);

      // Copy solution file to new solution if it exists
      if (fs::exists(origSolution->filepath())) {
        if (fs::exists(copyFilePath)) {
          std::ostringstream ss;
          ss << "Tried to copy solution \"" << name_ << "\" for level \"" << level.name()
             << "\" in profile \"" << name() << "\"\nfrom " << origSolution->filepath()
             << "\nto " << copyFilePath
             << "\nDestination file already exists. Please delete it and try again.";
          throw MAKE_EXCEPTION(ss.str());
        }
        fs::copy_file(origSolution->filepath(), copyFilePath);
      }

      auto& solutionFactory = m_solutionFactoryManager->getSolutionFactory(level);
      auto solution = solutionFactory.createSolution(
        level, *this, copyName, std::move(copyFilePath));
      auto result = solutions.emplace(std::move(copyName), std::move(solution));
      ASSERT(result.second);
      ASSERT_MSG(iter->second, "Original solution deleted after copying");
      ASSERT_MSG(iter->second != result.first->second,
                 "Copied solution points to the original instead of the copy");

      LOG_SEVERITY(logger::get(), Debug)
        << "Created a copy of solution \"" << name_ << "\" for level \"" << level.name()
        << "\" in profile \"" << name() << "\" with name = \"" << result.first->first
        << "\", filePath = " << result.first->second->filepath() << ".";

      saveProfile();

      return {true, true};
    }
  }

  ASSERT(!exists);
  LOG_SEVERITY(logger::get(), Warning)
    << "Tried to copy solution \"" << name_ << "\" for level \"" << level.name()
    << "\" in profile \"" << name() << "\" to \"" << copyName << "\"; solution does not exist.";
  return {false, false};
}
std::pair<bool, bool> Profile::renameSolution(
  const Level& level,
  const std::string& name_,
  std::string newName)
{
  auto mapIter = m_levelSolutions.find(&level);
  
  bool exists = mapIter != m_levelSolutions.end();
  if (exists) {
    auto& solutions = mapIter->second.map;
    auto iter = solutions.find(name_);
    if (iter == solutions.end())
      exists = false;
    else {
      auto iter2 = solutions.find(newName);
      if (iter2 != solutions.end()) {
        LOG_SEVERITY(logger::get(), Warning)
          << "Tried to rename solution \"" << name_ << "\" for level \"" << level.name()
          << "\" in profile \"" << name() << "\" to \"" << newName
          << "\"; a solution with that name already exists.";
        return {true, false};
      }
      auto solutionPtr = std::move(iter->second);
      solutionPtr->setName(newName);
      solutions.erase(iter);
      auto result = solutions.emplace(std::move(newName), std::move(solutionPtr));
      ASSERT(result.second);

      LOG_SEVERITY(logger::get(), Debug)
        << "Renamed solution \"" << name_ << "\" for level \"" << level.name()
        << "\" in profile \"" << name() << "\" to \"" << result.first->first << "\".";

      saveProfile();

      return {true, true};
    }
  }

  ASSERT(!exists);
  LOG_SEVERITY(logger::get(), Warning)
    << "Tried to rename solution \"" << name_ << "\" for level \"" << level.name()
    << "\" in profile \"" << name() << "\" to \"" << newName << "\"; solution does not exist.";
  return {false, false};
}
bool Profile::removeSolution(const Level& level, const std::string& name_) {
  auto mapIter = m_levelSolutions.find(&level);

  bool exists = mapIter != m_levelSolutions.end();
  if (exists) {
    auto& solutions = mapIter->second.map;
    auto iter = solutions.find(name_);
    if (iter == solutions.end())
      exists = false;
    else {
      fs::remove(iter->second->filepath());
      solutions.erase(iter);
      LOG_SEVERITY(logger::get(), Debug)
        << "Removed solution \"" << name_ << "\" for level \"" << level.name()
        << "\" in profile \"" << name() << "\".";

      if (solutions.empty()) {
        m_levelSolutions.erase(mapIter);
        LOG_SEVERITY(logger::get(), Debug)
          << "Solution removed was the last one for level \"" << level.name()
          << "\" in profile \"" << name() << "\"; removed solution set for level to save memory.";
      }

      saveProfile();

      return true;
    }
  }

  ASSERT(!exists);
  LOG_SEVERITY(logger::get(), Warning)
    << "Tried to remove solution \"" << name_ << "\" for level \"" << level.name()
    << "\" in profile \"" << name() << "\"; solution does not exist.";
  return false;
}

auto Profile::getSolutions(const Level& level) const -> boost::optional<SolutionRange> {
  auto mapIter = m_levelSolutions.find(&level);
  if (mapIter == m_levelSolutions.end())
    return {};
  else {
    auto& solutions = mapIter->second.map;
    return {{solutions.begin(), solutions.end()}};
  }
}

LevelSolution* Profile::lookupSolution(const Level& level, const std::string& name_) {
  auto mapIter = m_levelSolutions.find(&level);
  // No solutions exist at all for the level
  if (mapIter == m_levelSolutions.end())
    return nullptr;
  else {
    auto& solutions = mapIter->second.map;
    auto iter = solutions.find(name_);
    // A solution with the given name does not exist
    if (iter == solutions.end())
      return nullptr;
    // Solution exists; return it
    else
      return iter->second.get();
  }
}
  
namespace {
STATIC_LOGGER_INIT(logger) {
  logging::Logger lg;
  logging::setModuleName(lg, "Profile");
  return lg;
}
}

} // namespace game
