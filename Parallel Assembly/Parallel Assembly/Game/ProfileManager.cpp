#include "ProfileManager.h"

#include <fstream>
#include <sstream>
#include <iterator>

#include <boost/filesystem.hpp>

#include <rapidxml_print.hpp>

#include "../Assert.h"
#include "../Exception.h"
#include "../StringUtils.h"

#include "Profile.h"
#include "LevelManager.h"

#include "FSUtils.h"
#include "XMLHelpers.h"

namespace game {

using namespace std::string_literals;

namespace fs = boost::filesystem;

ProfileManager::ProfileManager(
  const LevelManager& levelManager,
  const SolutionFactoryManager& solutionFactoryManager)
  : m_logger()
  , m_savesDirectory(savesDirectoryName)
  , m_profilesFilePath()
  , m_levelManager(&levelManager)
  , m_solutionFactoryManager(&solutionFactoryManager)
  , m_profiles()
  , m_activeProfile(nullptr)
{
  logging::setModuleName(m_logger, "ProfileManager");

  m_savesDirectory.make_preferred();
  auto directoryStatus = fs::status(m_savesDirectory);
  if (!fs::is_directory(directoryStatus)) {
    // ./Saves exists and is not a directory - can't safely proceed.
    if (fs::exists(directoryStatus)) {
      std::ostringstream ss;
      ss << "Save directory " << m_savesDirectory << " exists and is not a directory; please delete it.";
      throw MAKE_EXCEPTION(ss.str());
    // ./Saves does not exist; create it
    } else {
      LOG_SEVERITY(m_logger, Warning)
        << "Save directory " << m_savesDirectory << " doesn't exist; creating it";
      if (!fs::create_directory(m_savesDirectory))
        throw MAKE_EXCEPTION("Failed to create save directory");
    }
  }
  m_profilesFilePath = m_savesDirectory / profilesFileName;

  loadProfiles();
}

namespace {
const std::string activeProfileNodeName{"ActiveProfile"s};
const std::string profileNodeName{"Profile"s};

const std::string nameAttrName{"name"s};
const std::string directoryAttrName{"directory"s};
}

template <typename ErrorHandler>
std::string loadActiveProfileName(
  const xml::xml_document<>& xmlDoc,
  const ErrorHandler& errorHandler)
{
  auto activeProfileNode = xml::firstNode(xmlDoc, activeProfileNodeName);

  if (!activeProfileNode)
    errorHandler("missing ", activeProfileNodeName, " node");
  if (xml::nextSibling(activeProfileNode, activeProfileNodeName))
    errorHandler("duplicate ", activeProfileNodeName, " node");

  auto activeProfileErrHandler = xml::makeErrorHandler(errorHandler,
    activeProfileNodeName, " node: ");

  auto profileName = xml::getAttributeValue(*activeProfileNode, activeProfileErrHandler, nameAttrName);
  auto nameLength = utf32Length(profileName);
  if (nameLength > Profile::maxNameUTF32Length)
    activeProfileErrHandler(nameAttrName, " (\"", profileName, "\") exceeds max length (",
      Profile::maxNameUTF32Length, "): length = ", nameLength);
  return profileName;
}

// Returns (name, directory) pair
template <typename ErrorHandler>
std::pair<const xml::xml_attribute<>*, const xml::xml_attribute<>*>
loadProfileNode(const xml::xml_node<>& node, const ErrorHandler& errorHandler) {
  const xml::xml_attribute<char>
    *nameAttr = nullptr,
    *directoryAttr = nullptr;

  std::tie(nameAttr, directoryAttr) =
    xml::getAttributes(node, errorHandler, nameAttrName, directoryAttrName);

  auto nameLength = utf32Length(xml::value(nameAttr));
  if (nameLength > Profile::maxNameUTF32Length)
    errorHandler(nameAttrName, " (\"", nameAttr->value(), "\") exceeds max length (",
      Profile::maxNameUTF32Length, "): length = ", nameLength);

  return {nameAttr, directoryAttr};
}

void ProfileManager::loadProfiles() {
  ASSERT(m_levelManager);
  ASSERT(m_solutionFactoryManager);

  auto errorHandler = xml::makeErrorHandler("Profiles file ", m_profilesFilePath, " is invalid:\n");

  // Open profiles file at end to get file size
  std::ifstream profilesFile(m_profilesFilePath.string(),
                             std::ios::in | std::ios::binary | std::ios::ate);
  if (profilesFile) {
    auto fileSize = static_cast<std::size_t>(profilesFile.tellg());
    profilesFile.seekg(0);

    std::vector<char> fileBuffer(fileSize + 1, '\0');
    fileBuffer.assign(std::istreambuf_iterator<char>(profilesFile),
                      std::istreambuf_iterator<char>());
    ASSERT(fileBuffer.size() + 1 <= fileBuffer.capacity());
    fileBuffer.push_back('\0');
    profilesFile.close();

    xml::xml_document<> xmlDoc;
    xml::parse(xmlDoc, fileBuffer.data(), fileBuffer.size(), errorHandler);

    std::string activeProfileName = loadActiveProfileName(xmlDoc, errorHandler);
    bool matchedActive = false;
    std::size_t profileID = 1u;
    for (auto profileNode = xml::firstNode(xmlDoc, profileNodeName);
         profileNode != nullptr;
         profileNode = xml::nextSibling(profileNode, profileNodeName), ++profileID)
    {
      ASSERT(profileNode);
      auto profileErrHandler = xml::makeErrorHandler(errorHandler,
        profileNodeName, " node ", profileID, ": ");

      auto pair = loadProfileNode(*profileNode, profileErrHandler);
      ASSERT(pair.first && pair.second);

      std::string profileName{xml::value(pair.first)};
      const auto profileDirectory = pair.second->value();

      if (m_profiles.find(profileName) != m_profiles.end())
        profileErrHandler("duplicate ", profileNodeName, " definition (",
          nameAttrName, " = \"", profileName, "\")");

      auto result = m_profiles.emplace(std::piecewise_construct,
        std::forward_as_tuple(profileName),
        std::forward_as_tuple(*m_levelManager, *m_solutionFactoryManager,
                              std::move(profileName), profileDirectory));
      ASSERT(result.second);

      if (!matchedActive && activeProfileName == result.first->second.name()) {
        matchedActive = true;
        m_activeProfile = &result.first->second;
      }
    }
    // Active profile wasn't found
    if (!matchedActive && !activeProfileName.empty())
      errorHandler(activeProfileNodeName, " set to \"", activeProfileName,
        "\", but did not match a profile name");
    else if (m_activeProfile)
      m_activeProfile->loadProfile();
  } else {
    LOG_SEVERITY(m_logger, Warning)
      << "Couldn't open profiles file " << m_profilesFilePath << "; creating a new one";
    // Write default profiles file instead from the default values
    saveProfiles();
  }
}
void ProfileManager::saveProfiles() {
  // Write to temporary file first to avoid corrupting the original if something goes wrong
  auto tempFilePath = fs::path(m_profilesFilePath).concat(".tmp");
  std::ofstream profilesFile(tempFilePath.string(),
                             std::ios::out | std::ios::binary | std::ios::trunc);
  if (!profilesFile) {
    std::ostringstream ss;
    ss << "Failed to open temporary file " << m_profilesFilePath << " to write profiles.\n";
    ss << "Did you set an existing one as read-only?";
    throw MAKE_EXCEPTION(ss.str());
  }
  xml::xml_document<> xmlDoc;

  auto activeProfileNode = xml::allocateNode(xmlDoc, xml::node_element, activeProfileNodeName);
  auto activeProfileNameAttr = activeProfile() ?
    xml::allocateAttribute(xmlDoc, nameAttrName, activeProfile()->name()) :
    xml::allocateAttribute(xmlDoc, nameAttrName);
  activeProfileNode->append_attribute(activeProfileNameAttr);
  xmlDoc.append_node(activeProfileNode);

  for (const auto& pair : m_profiles) {
    ASSERT(pair.first == pair.second.name());
    auto& profileName = pair.first;
    auto profileDirectory = pair.second.directory().string();

    auto profileNode = xml::allocateNode(xmlDoc, xml::node_element, profileNodeName);
    profileNode->append_attribute(xml::allocateAttribute(xmlDoc, nameAttrName, profileName));
    profileNode->append_attribute(
      xml::allocateAttribute(xmlDoc, directoryAttrName, std::move(profileDirectory)));
    xmlDoc.append_node(profileNode);
  }

  profilesFile << xmlDoc;
  // Close temp file to commit to disk
  profilesFile.close();

  boost::system::error_code error;
  // Remove the original if it exists
  if (fs::exists(m_profilesFilePath) &&
      !fs::remove(m_profilesFilePath, error))
  {
    std::ostringstream ss;
    ss << "Failed to replace profiles file " << m_profilesFilePath << ": " << error.message()
      << "\nDid you set the file as read-only?\n\n"
      << "Your changes have been saved in " << tempFilePath
      << "; delete " << m_profilesFilePath.filename()
      << " and rename " << tempFilePath.filename()
      << " to " << m_profilesFilePath.filename() << " to update the file manually.";
    throw MAKE_EXCEPTION(ss.str());
  }
  // Rename temporary to original file name
  fs::rename(tempFilePath, m_profilesFilePath);
}

bool ProfileManager::createProfile(std::string name) {
  ASSERT(m_solutionFactoryManager);

  if (name.empty() || name == "")
    throw MAKE_EXCEPTION("Invalid profile name; name can't be empty");
  ASSERT_MSG(utf32Length(name) <= Profile::maxNameUTF32Length,
             "ProfileManager::createProfile called with name exceeding max utf32 length");
  auto directory = getSanitizedDirectory(m_savesDirectory, name);
  
  auto profileIter = m_profiles.find(name);
  if (profileIter != m_profiles.end()) {
    LOG_SEVERITY(m_logger, Warning)
      << "Tried to create profile with name=\"" << name << "\"; profile already exists";
    return false;
  }

  auto result = m_profiles.emplace(
    std::piecewise_construct,
    std::forward_as_tuple(name),
    std::forward_as_tuple(*m_levelManager, *m_solutionFactoryManager,
                          std::move(name), std::move(directory)));
  ASSERT(result.second);

  LOG_SEVERITY(m_logger, Debug)
    << "Created profile with name=\"" << result.first->first
    << "\", directory=" << result.first->second.directory();
  // Save profile on creation to save empty profile to disk
  result.first->second.saveProfile();

  return result.second;
}
std::pair<bool, bool> ProfileManager::renameProfile(const std::string& name, std::string newName) {
  if (newName.empty() || newName == "")
    throw MAKE_EXCEPTION("Invalid profile name; name can't be empty");

  auto profileIter = m_profiles.find(name);
  if (profileIter == m_profiles.end()) {
    LOG_SEVERITY(m_logger, Warning)
      << "Tried to rename non-existent profile with name=\"" << name << '\"';
    return std::make_pair(false, false);
  }

  auto newNameIter = m_profiles.find(newName);
  if (newNameIter != m_profiles.end()) {
    LOG_SEVERITY(m_logger, Warning)
      << "Tried to rename profile \"" << name << "\" to \"" << newName
      << "\", which already exists as another profile";
    return std::make_pair(true, false);
  }

  auto node = m_profiles.extract(profileIter);
  node.key() = std::move(newName);
  auto insertResult = m_profiles.insert(std::move(node));
  ASSERT(insertResult.inserted);
  insertResult.position->second.setName(insertResult.position->first);

  return std::make_pair(true, true);
}
bool ProfileManager::removeProfile(const std::string& name) {
  auto iter = m_profiles.find(name);
  if (iter == m_profiles.end()) {
    LOG_SEVERITY(m_logger, Warning)
      << "Tried to remove non-existent profile with name=\"" << name << '\"';
    return false;
  }
  if (&iter->second == activeProfile())
    setActiveProfile(nullptr);

  iter->second.deleteProfile();
  m_profiles.erase(iter);
  return true;
}

Profile* ProfileManager::lookupProfile(const std::string& name) {
  auto iter = m_profiles.find(name);
  if (iter == m_profiles.end()) {
    LOG_SEVERITY(m_logger, Warning)
      << "Tried to lookup non-existent profile with name=\"" << name << '\"';
    return nullptr;
  }

  return &iter->second;
}
const Profile* ProfileManager::lookupProfile(const std::string& name) const {
  auto iter = m_profiles.find(name);
  if (iter == m_profiles.end()) {
    LOG_SEVERITY(m_logger, Warning)
      << "Tried to lookup non-existent profile with name=\"" << name << '\"';
    return nullptr;
  }

  return &iter->second;
}

void ProfileManager::setActiveProfile(Profile* profile) {
  if (m_activeProfile) {
    m_activeProfile->unloadProfile();
    ASSERT(!m_activeProfile->loaded());
  }

  m_activeProfile = profile;
}

} // namespace game
