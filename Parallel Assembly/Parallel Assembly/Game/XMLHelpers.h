// This file contains some utility wrappers for RapidXML
// functions designed to simplify usage

#pragma once
#ifndef GAME_XML_HELPERS_H
#define GAME_XML_HELPERS_H

#include <string>
#include <sstream>
#include <tuple>
#include <type_traits>
#include <utility>

#include <rapidxml.hpp>

#include "../Assert.h"
#include "../Exception.h"
#include "../StringUtils.h"

namespace game {
namespace xml {

using namespace rapidxml;

namespace detail {
template <typename MessageData, std::size_t... Is>
void writeMessageDataImpl(std::ostream& out, const MessageData& t, std::index_sequence<Is...>) {
  using expander = int[];
  (void) expander {0, (void(out << std::get<Is>(t)), 0)...};
}
template <typename... Args>
void writeMessageData(std::ostream& out, const std::tuple<Args...>& t) {
  writeMessageDataImpl(out, t, std::index_sequence_for<Args...>{});
};

} // namespace detail

// Lightweight error handler that can be nested to hold scope-like information
// MessageData should always be a std::tuple of printable types,
// Prefix should either be void or an instantiation of XMLErrorHandler
template <typename MessageData, typename Prefix = void>
class XMLErrorHandler {
  const Prefix* m_prefix; // non-owning
  MessageData m_messageData;

  template <typename, typename> friend class XMLErrorHandler;

  void write(std::ostream& out) const {
    ASSERT(m_prefix);
    m_prefix->write(out);
    detail::writeMessageData(out, m_messageData);
  }
public:
  template <typename MessageData2>
  XMLErrorHandler(const Prefix& prefix, MessageData2&& messageData)
    : m_prefix(&prefix)
    , m_messageData(std::forward<MessageData2>(messageData))
  {}
  XMLErrorHandler(const XMLErrorHandler& o) = default;
  XMLErrorHandler(XMLErrorHandler&& o) noexcept = default;
  ~XMLErrorHandler() noexcept = default;

  XMLErrorHandler& operator=(const XMLErrorHandler& o) = default;
  XMLErrorHandler& operator=(XMLErrorHandler&& o) noexcept = default;

  // Constructs the final error message from stored data and arguments and throws
  // an exception. Arguments should be printable types forming the error message
  template <typename... Args>
  [[noreturn]] void operator()(Args&&... args) const {
    std::ostringstream ss;
    write(ss);
    using expander = int[];
    (void) expander {0, (void(ss << std::forward<Args>(args)), 0)...};
    throw MAKE_EXCEPTION(ss.str());
  }
};

// Base case (no prefix) for XMLErrorHandler
template <typename MessageData>
class XMLErrorHandler<MessageData, void> {
  MessageData m_messageData;

  template <typename, typename> friend class XMLErrorHandler;

  void write(std::ostream& out) const {
    detail::writeMessageData(out, m_messageData);
  }
public:
  template <typename MessageData2>
  XMLErrorHandler(MessageData2&& messageData)
    : m_messageData(std::forward<MessageData2>(messageData))
  {}
  XMLErrorHandler(const XMLErrorHandler& o) = default;
  XMLErrorHandler(XMLErrorHandler&& o) noexcept = default;
  ~XMLErrorHandler() noexcept = default;

  XMLErrorHandler& operator=(const XMLErrorHandler& o) = default;
  XMLErrorHandler& operator=(XMLErrorHandler&& o) noexcept = default;

  // Constructs the final error message from stored data and arguments and throws
  // an exception. Arguments should be printable types forming the error message
  template <typename... Args>
  [[noreturn]] void operator()(Args&&... args) const {
    std::ostringstream ss;
    write(ss);
    using expander = int[];
    (void) expander {0, (void(ss << std::forward<Args>(args)), 0)...};
    throw MAKE_EXCEPTION(ss.str());
  }
};

// Constructs an error handler with another one serving as a prefix
// Arguments should be printable types forming the start of an error message.
template <typename PrefixData, typename PrefixPrefix, typename... Args>
XMLErrorHandler<std::tuple<const Args&...>, XMLErrorHandler<PrefixData, PrefixPrefix>>
makeErrorHandler(const XMLErrorHandler<PrefixData, PrefixPrefix>& prefix, const Args&... args) {
  return XMLErrorHandler<std::tuple<const Args&...>, XMLErrorHandler<PrefixData, PrefixPrefix>>(
    prefix, std::tuple<const Args&...>(args...));
}
// Constructs a base case error handler
// Arguments should be printable types forming the start of an error message
template <typename... Args>
XMLErrorHandler<std::tuple<const Args&...>>
makeErrorHandler(const Args&... args) {
  return XMLErrorHandler<std::tuple<const Args&...>>(
    std::tuple<const Args&...>(args...));
}

template <typename ErrorHandler>
void parse(
  xml_document<>& xmlDoc,
  char* text,
  std::size_t textSize,
  const ErrorHandler& errorHandler)
{
  try {
    xmlDoc.parse<parse_default>(text);
  } catch (parse_error& e) {
    auto pos = getLinePosition(text, text + textSize, e.where<char>());

    if (pos.first != -1)
      errorHandler("Parse error at line ", pos.first, ':', pos.second, ": ", e.what());
    else
      errorHandler("File is corrupt.");
  }
}

// xml_node::first_node
template <typename Ch>
const xml_node<Ch>* firstNode(
  const xml_node<Ch>& node,
  const std::basic_string<Ch>& name,
  bool caseSensitive = true)
{
  return node.first_node(name.data(), name.size(), caseSensitive);
}
// Pointer overload
template <typename Ch>
const xml_node<Ch>* firstNode(
  const xml_node<Ch>* node,
  const std::basic_string<Ch>& name,
  bool caseSensitive = true)
{
  ASSERT_MSG(node, "xml::firstNode() called with nullptr");
  return firstNode(*node, name, caseSensitive);
}

// xml_node::next_sibling
template <typename Ch>
const xml_node<Ch>* nextSibling(
  const xml_node<Ch>& node,
  const std::basic_string<Ch>& name,
  bool caseSensitive = true)
{
  return node.next_sibling(name.data(), name.size(), caseSensitive);
}
// Pointer overload
template <typename Ch>
const xml_node<Ch>* nextSibling(
  const xml_node<Ch>* node,
  const std::basic_string<Ch>& name,
  bool caseSensitive = true)
{
  ASSERT_MSG(node, "xml::nextSibling() called with nullptr");
  return nextSibling(*node, name, caseSensitive);
}

// xml_node::first_attribute
template <typename Ch>
const xml_attribute<Ch>* firstAttribute(
  const xml_node<Ch>& node,
  const std::basic_string<Ch>& name,
  bool caseSensitive = true)
{
  return node.first_attribute(name.data(), name.size(), caseSensitive);
}
// Pointer overload
template <typename Ch>
const xml_attribute<Ch>* firstAttribute(
  const xml_node<Ch>* node,
  const std::basic_string<Ch>& name,
  bool caseSensitive = true)
{
  ASSERT_MSG(node, "xml::firstAttribute() called with nullptr");
  return firstAttribute(*node, name, caseSensitive);
}

// Helper for tuple construction
template <typename T, typename U>
struct replace_with {
  using type = U;
};
template <typename T, typename U>
using replace_with_t = typename replace_with<T, U>::type;

// Attribute retrieval with error checking
template <typename Ch, typename ErrorHandler>
const xml_attribute<Ch>*
getAttribute(
  const xml_node<Ch>& node,
  const ErrorHandler& errorHandler,
  const std::basic_string<Ch>& name)
{
  auto attr = firstAttribute(node, name);
  if (!attr)
    errorHandler("missing attribute \"", name, "\"");
  return attr;
}
// Batch retrieval of attributes with error checking; args must be convertible to strings
template <typename Ch, typename ErrorHandler, typename... Args>
std::tuple<replace_with_t<Args, const xml_attribute<Ch>*>...>
getAttributes(
  const xml_node<Ch>& node,
  const ErrorHandler& errorHandler,
  const Args&... args) {
  return std::tuple<replace_with_t<Args, const xml_attribute<Ch>*>...>{
    getAttribute(node, errorHandler, args)...
  };
}

// xml_node::value
template <typename Ch>
std::basic_string<Ch> value(const xml_node<Ch>& node) {
  return {node.value(), node.value_size()};
}
// Pointer overload
template <typename Ch>
std::basic_string<Ch> value(const xml_node<Ch>* node) {
  ASSERT_MSG(node, "xml::value(node) called with nullptr");
  return {node->value(), node->value_size()};
}

// xml_attribute::value
template <typename Ch>
std::basic_string<Ch> value(const xml_attribute<Ch>& attr) {
  return {attr.value(), attr.value_size()};
}
// Pointer overload
template <typename Ch>
std::basic_string<Ch> value(const xml_attribute<Ch>* attr) {
  ASSERT_MSG(attr, "xml::value(attr) called with nullptr");
  return {attr->value(), attr->value_size()};
}

// Attribute value retrieval with error checking
template <typename Ch, typename ErrorHandler>
std::basic_string<Ch>
getAttributeValue(
  const xml_node<Ch>& node,
  const ErrorHandler& errorHandler,
  const std::basic_string<Ch>& name)
{
  return value(getAttribute(node, errorHandler, name));
}
// Batch attribute value retrieval with error checking; args must be convertible to strings
template <typename Ch, typename ErrorHandler, typename... Args>
std::tuple<replace_with_t<Args, std::basic_string<Ch>>...>
getAttributeValues(
  const xml_node<Ch>& node,
  const ErrorHandler& errorHandler,
  const Args&... args)
{
  return std::tuple<replace_with_t<Args, std::basic_string<Ch>>...>{
    getAttributeValue(node, errorHandler, args)...
  };
}

template <typename Ch>
xml_node<Ch>* allocateNode(
  xml_document<Ch>& xmlDoc,
  node_type type,
  const std::basic_string<Ch>& name)
{
  return xmlDoc.allocate_node(type, name.data(), nullptr, name.size());
}
template <typename Ch>
xml_node<Ch>* allocateNode(
  xml_document<Ch>& xmlDoc,
  node_type type,
  const std::basic_string<Ch>& name,
  const std::basic_string<Ch>& value)
{
  return xmlDoc.allocate_node(type, name.data(), value.data(), name.size(), value.size());
}

template <typename Ch>
xml_attribute<Ch>* allocateAttribute(
  xml_document<Ch>& xmlDoc,
  const std::basic_string<Ch>& name)
{
  return xmlDoc.allocate_attribute(name.data(), nullptr, name.size());
}
template <typename Ch>
xml_attribute<Ch>* allocateAttribute(
  xml_document<Ch>& xmlDoc,
  const std::basic_string<Ch>& name,
  const std::basic_string<Ch>& value)
{
  return xmlDoc.allocate_attribute(name.data(), value.data(), name.size(), value.size());
}

template <typename Ch>
Ch* allocateString(xml_document<Ch>& xmlDoc, const std::basic_string<Ch>& value) {
  return xmlDoc.allocate_string(value.data(), value.size());
}

// For cases where the attribute value will go out of scope
template <typename Ch>
xml_attribute<Ch>* allocateAttribute(
  xml_document<Ch>& xmlDoc,
  const std::basic_string<Ch>& name,
  std::basic_string<Ch>&& value)
{
  auto string = allocateString(xmlDoc, value);
  return xmlDoc.allocate_attribute(name.data(), string, name.size(), value.size());
}

} // namespace xml
} // namespace game

#endif // GAME_XML_HELPERS_H
