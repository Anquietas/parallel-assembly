#pragma once
#ifndef GAME_FACTORY_H
#define GAME_FACTORY_H


#include <boost/container/static_vector.hpp>

#include "LevelGrid.h"
#include "Worker.h"

namespace game {

class SimpleFactory final {
  using WorkerVec = boost::container::static_vector<Worker, Worker::maxWorkers>;

  LevelGrid m_grid;
  WorkerVec m_workers;
  
public:
  SimpleFactory(std::size_t width, std::size_t height, std::size_t numWorkers);
  SimpleFactory(const glm::ivec2& gridSize, std::size_t numWorkers);
  SimpleFactory(const SimpleFactory& o) = default;
  SimpleFactory(SimpleFactory&& o) noexcept = default;
  ~SimpleFactory() = default;

  SimpleFactory& operator=(const SimpleFactory& o) = default;
  SimpleFactory& operator=(SimpleFactory&& o) noexcept = default;

  auto& grid() noexcept { return m_grid; }
  const auto& grid() const noexcept { return m_grid; }

  std::size_t numWorkers() const noexcept { return m_workers.size(); }
  Worker& worker(std::size_t id) noexcept { return m_workers[id]; }
  const Worker& worker(std::size_t id) const noexcept { return m_workers[id]; }

  // Runs the given number of steps of the simulation for all workers
  void advance(std::size_t numSteps = 1);

  // Resets workers and the grid to their initial states
  void reset();
};

} // namespace game

#endif // GAME_FACTORY_H
