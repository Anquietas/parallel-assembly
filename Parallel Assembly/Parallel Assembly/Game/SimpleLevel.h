#pragma once
#ifndef GAME_SIMPLE_LEVEL_H
#define GAME_SIMPLE_LEVEL_H

#include "../GLMIncludes.h"

#include "Level.h"

namespace game {

class SimpleLevel : public Level {
  glm::ivec2 m_gridSize;
  std::size_t m_numWorkers;

  virtual void loadLevelImpl() override;
  virtual void unloadLevelImpl() override;
public:
  static const std::string levelTypeName;

  SimpleLevel(std::string name_, std::string id_, boost::filesystem::path directory_);
  SimpleLevel(SimpleLevel&& o) = default;
  virtual ~SimpleLevel() override = default;

  SimpleLevel& operator=(SimpleLevel&& o) = default;

  const glm::ivec2& gridSize() const noexcept { return m_gridSize; }
  std::size_t numWorkers() const noexcept { return m_numWorkers; }

  virtual const std::string& typeName() const noexcept override { return levelTypeName; }

  virtual const std::string& sceneName() const noexcept override;
};

} // namespace game

#endif // GAME_SIMPLE_LEVEL_H
