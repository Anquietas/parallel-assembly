#pragma once
#ifndef GAME_SOLUTION_FACTORY_MANAGER_H
#define GAME_SOLUTION_FACTORY_MANAGER_H

#include <memory>
#include <type_traits>

#include <boost/container/flat_map.hpp>

#include "LevelSolution.h"
#include "SolutionFactory.h"

namespace game {

class SolutionFactoryManager final {
  // factory name -> factory
  using FactoryMap = boost::container::flat_map<std::string, std::unique_ptr<LevelSolutionFactory>>;
  // level type name -> solution factory name
  using TypeNameMap = boost::container::flat_map<std::string, std::string>;

  FactoryMap m_solutionFactories;
  TypeNameMap m_levelSolutionFactoryMap;

  void registerSolutionTypes();

public:
  SolutionFactoryManager();
  SolutionFactoryManager(const SolutionFactoryManager&) = delete;
  SolutionFactoryManager(SolutionFactoryManager&&) = delete;
  ~SolutionFactoryManager() = default;

  SolutionFactoryManager& operator=(const SolutionFactoryManager&) = delete;
  SolutionFactoryManager& operator=(SolutionFactoryManager&& o) = delete;

  template <typename LevelType, typename SolutionFactoryType>
  std::enable_if_t<
    std::is_base_of<Level, LevelType>::value && 
    std::is_base_of<LevelSolutionFactory, SolutionFactoryType>::value
  > addSolutionFactory(std::string name);
  template <typename SolutionType>
  std::enable_if_t<std::is_base_of<LevelSolution, SolutionType>::value>
  addSolutionType();

  // Returns the factory that corresponds to the type of the level passed as an argument
  // Throws an exception if there isn't a factory registered for level's level type
  const LevelSolutionFactory& getSolutionFactory(const Level& level) const;
};

} // namespace game

#endif // GAME_SOLUTION_FACTORY_MANAGER_H
