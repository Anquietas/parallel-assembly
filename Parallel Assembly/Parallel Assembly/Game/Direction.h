#pragma once
#ifndef GAME_DIRECTION_H
#define GAME_DIRECTION_H

#include <string>

#include "../GLMIncludes.h"

namespace game {

enum class Direction {
  None = 0,
  Left,
  Right,
  Up,
  Down,
};

// Attempts to parse the given direction string.
// Valid values are "left", "right", "up", "down".
// Returns None on failure.
Direction parseDirection(const std::string& direction);
// Converts the direction to string; None will throw an exception.
const std::string& toString(Direction direction);

// Returns the offset vector to use for one step in the given direction
// Returns (0, 0) for Direction::None
glm::ivec2 getStepOffset(Direction direction);

// Returns the opposite direction to the one given
// Returns None if direction is None
Direction getOpposite(Direction direction);

// Returns the direction 90 degrees clockwise of the one given
// Returns None if direction is None
Direction rotateCW(Direction direction);

// Returns the direction 90 degrees counter-clockwise of the one given
// Returns None if direction is None
Direction rotateCCW(Direction direction);

// Returns true if otherDirection is perpendicular to direction, false otherwise
bool isPerpendicular(Direction direction, Direction otherDirection);

} // namespace game

#endif // GAME_DIRECTION_H
