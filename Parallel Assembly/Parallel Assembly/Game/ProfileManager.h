#pragma once
#ifndef GAME_PROFILE_MANAGER_H
#define GAME_PROFILE_MANAGER_H

#include <memory>

#include <boost/container/map.hpp>
#include <boost/filesystem/path.hpp>

#include "../Logger.h"

#include "Profile.h"

namespace game {

class ProfileManager final {
  mutable logging::Logger m_logger;

  boost::filesystem::path m_savesDirectory;
  boost::filesystem::path m_profilesFilePath;

  const LevelManager* m_levelManager;
  const SolutionFactoryManager* m_solutionFactoryManager;
  using ProfileMap = boost::container::map<std::string, Profile>;
  ProfileMap m_profiles;
  Profile* m_activeProfile;

  void loadProfiles();

public:
  ProfileManager(const LevelManager& levelManager,
                 const SolutionFactoryManager& solutionFactoryManager);
  ProfileManager(const ProfileManager&) = delete;
  ProfileManager(ProfileManager&&) = delete;
  ~ProfileManager() = default;

  ProfileManager& operator=(const ProfileManager&) = delete;
  ProfileManager& operator=(ProfileManager&&) = delete;

  static constexpr char* savesDirectoryName = "./Saves";
  static constexpr char* profilesFileName = "profiles.xml";

  void saveProfiles();
  
  // Creates a new profile with the given name.
  // The directory name will be computed from the profile name
  // Returns true if creation was successful, false otherwise (if it's a duplicate)
  bool createProfile(std::string name);
  // Renames the profile with the given name, if it exists, to the new name given.
  // If the profile does not exist, returns (false, _).
  // If the profile exists and the new name is a duplicate, returns (true, false)
  // If the profile exists and the new name is not a duplicate, returns (true, true).
  // The profile directory is left unchanged.
  std::pair<bool, bool> renameProfile(const std::string& name, std::string newName);
  // Removes the profile with the given name, if it exists.
  // This will delete the directory the profile corresponds to.
  // If the profile removed is the active one, then the active profile will be reset
  // to the default one.
  // Returns true if the profile existed and was removed, false otherwise
  bool removeProfile(const std::string& name);

  // Retrieves the profile with the given name, if it exists.
  // If the profile does not exist, returns nullptr
  Profile* lookupProfile(const std::string& name);
  const Profile* lookupProfile(const std::string& name) const;

  const auto& getProfiles() const noexcept { return m_profiles; }

  Profile* activeProfile() const noexcept { return m_activeProfile; }
  void setActiveProfile(Profile* profile);
};

} // namespace game

#endif // GAME_PROFILE_MANAGER_H
