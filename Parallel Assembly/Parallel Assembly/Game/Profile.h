#pragma once
#ifndef GAME_PROFILE_H
#define GAME_PROFILE_H

#include <string>
#include <memory>

#include <boost/filesystem/path.hpp>
#include <boost/container/flat_set.hpp>
#include <boost/container/flat_map.hpp>
#include <boost/optional.hpp>
// BOOST_UNLIKELY
#include <boost/config.hpp>

#include "Level.h"
#include "LevelSolution.h"

namespace game {

class LevelManager;
class SolutionFactoryManager;

class Profile final {
  friend class ProfileManager;
  friend class LevelSolution;

  const LevelManager* m_levelManager;
  const SolutionFactoryManager* m_solutionFactoryManager;

  std::string m_name;
  boost::filesystem::path m_directory;
  boost::filesystem::path m_profileInfoFilePath;
  bool m_loaded;

  void setLoaded() noexcept { m_loaded = true; }
  void clearLoaded() noexcept { m_loaded = false; }

  struct LevelPtrCompare {
    bool operator()(const Level* x, const Level* y) const {
      if (BOOST_UNLIKELY(!x)) return y != nullptr;  // nullptr < nullptr = false
      else if (BOOST_UNLIKELY(!y)) return false;
      else return x->name() < y->name();
    }
  };
  struct LevelSolutionPtrCompare {
    bool operator()(const LevelSolution* x, const LevelSolution* y) const {
      if (BOOST_UNLIKELY(!x)) return y != nullptr; // nullptr < nullptr = false
      else if (BOOST_UNLIKELY(!y)) return false;
      else return x->name() < y->name();
    }
    bool operator()(const std::unique_ptr<LevelSolution>& x,
                    const std::unique_ptr<LevelSolution>& y) const
    {
      return operator()(x.get(), y.get());
    }
  };
public:
  using CompletedLevelSet = boost::container::flat_set<const Level*, LevelPtrCompare>;
  // solution name -> solution map
  using SolutionMap = boost::container::flat_map<std::string, std::unique_ptr<LevelSolution>>;
  using SolutionIterator = SolutionMap::const_iterator;
  using SolutionRange = std::pair<SolutionIterator, SolutionIterator>;
  // Workaround for decorated name length exceeded.
  struct SolutionMapWrapper {
    SolutionMap map;
  };
  // level -> (solution name -> solution) map
  using LevelSolutionMap = boost::container::flat_map<const Level*, SolutionMapWrapper, LevelPtrCompare>;
private:
  CompletedLevelSet m_completedLevels;
  LevelSolutionMap m_levelSolutions;

  void deleteProfile();

  bool isLevelUnlocked(std::size_t levelID) const noexcept;
public:
  // Maximum profile name length in UTF32 code points
  static constexpr std::size_t maxNameUTF32Length = 20u;

  static constexpr char* profileInfoFileName = "profileInfo.xml";

  Profile(const LevelManager& levelManager,
          const SolutionFactoryManager& solutionFactoryManager,
          std::string name_,
          boost::filesystem::path directory_);
  Profile(const Profile& o) = default;
  Profile(Profile&& o) = default;
  ~Profile() = default;

  Profile& operator=(const Profile& o) = default;
  Profile& operator=(Profile&& o) = default;

  const std::string& name() const noexcept { return m_name; }
  void setName(std::string name_);

  const boost::filesystem::path& directory() const noexcept { return m_directory; }
  void setDirectory(boost::filesystem::path directory_);

  bool loaded() const noexcept { return m_loaded; }

  void loadProfile();
  void unloadProfile();
  void saveProfile() const;

  // Registers the given level as completed
  // If the level is already registered as completed, this will throw an exception
  void addCompletedLevel(const Level& level);
  // Queries if the given level has been registered as completed
  // Returns true if the level has been completed, false otherwise
  bool isLevelCompleted(const Level& level) const noexcept;
  // Queries if the given level is unlocked.
  // Returns true if the level is unlocked, false otherwise.
  bool isLevelUnlocked(const Level& level) const;

  // Creates a new LevelSolution for the given level with the given name
  // and stores it in the level solution map.
  // Returns true if creation was successful, false otherwise (if it's a duplicate)
  bool createSolution(Level& level, std::string name_);
  // Creates a copy of the LevelSolution for the given level with the given name
  // with copyName as the name of the copy. This will copy the solution's data file.
  // If the solution does not exist, returns (false, _).
  // If the solution exists and the copy name is a duplicate, returns (true, false).
  // If the solution exists and the copy name is not a duplicate, returns (true, true).
  std::pair<bool, bool> copySolution(const Level& level,
                                     const std::string& name_,
                                     std::string copyName);
  // Renames the LevelSolution for the given level with the given name, if it
  // exists, to the new name given.
  // If the solution does not exist, returns (false, _).
  // If the solution exists and the new name is a duplicate, returns (true, false).
  // If the solution exists and the new name is not a duplicate, returns (true, true).
  std::pair<bool, bool> renameSolution(const Level& level,
                                       const std::string& name_,
                                       std::string newName);
  // Removes the LevelSolution for the given level with the given name, if it exists.
  // Returns true if the solution existed and was removed, false otherwise.
  bool removeSolution(const Level& level, const std::string& name_);

  // Returns an iterator range for the solutions for a given level, if any exist.
  // Returns an empty optional if none exist.
  boost::optional<SolutionRange> getSolutions(const Level& level) const;

  // Queries the solution map for the a solution for the given level with the given
  // name and returns a pointer to it if it exists, or nullptr if it doesn't.
  LevelSolution* lookupSolution(const Level& level, const std::string& name_);
};

} // namespace game

#endif // GAME_PROFILE_H
