#include "SimpleLevelSolution.h"

#include <cstdlib>
#include <sstream>
#include <fstream>
#include <tuple>
#include <vector>

#include <boost/filesystem.hpp>

#include <rapidxml_print.hpp>

#include "../Assert.h"
#include "../Exception.h"
#include "../Logger.h"

#include "SimpleFactory.h"
#include "XMLHelpers.h"

namespace game {

namespace {
STATIC_LOGGER_DECLARE(logger);
}

using namespace std::string_literals;
namespace fs = boost::filesystem;

const std::string SimpleLevelSolution::solutionTypeName{"SimpleLevelSolution"s};

class SimpleLevelSolution::Impl {
  SimpleFactory m_factory;

public:
  Impl(const SimpleLevel& level) // level must be loaded!
    : m_factory(level.gridSize(), level.numWorkers())
  {
    ASSERT_MSG(level.loaded(), "SimpleLevelSolution::Impl constructed with unloaded level");
  }
  Impl(const Impl&) = delete;
  Impl(Impl&&) = delete;
  ~Impl() = default;

  Impl& operator=(const Impl&) = delete;
  Impl& operator=(Impl&&) = delete;

  SimpleFactory& factory() noexcept { return m_factory; }
  const SimpleFactory& factory() const noexcept { return m_factory; }
};

SimpleLevelSolution::SimpleLevelSolution(
  const Level& level_,
  Profile& profile_,
  std::string name_,
  boost::filesystem::path filepath_)
  : LevelSolution(profile_, std::move(name_), std::move(filepath_))
  , m_level(dynamic_cast<const SimpleLevel*>(&level_))
  , m_impl()
{
  if (!m_level) {
    std::ostringstream ss;
    ss << "Level parameter given to SimpleLevelSolution with name = \""
       << name() << "\" is not a SimpleLevel";
    throw MAKE_EXCEPTION(ss.str());
  }
}

void SimpleLevelSolution::initializeEmptySolutionImpl() {
  ASSERT(!loaded());
  ASSERT(!m_impl);
  ASSERT(m_level && m_level->loaded());

  m_impl = make_const_prop_ptr<Impl>(*m_level);
}

namespace {
const std::string solutionNodeName{"SimpleLevelSolution"s};
const std::string workerStateNodeName{"WorkerState"s};
const std::string positionNodeName{"Position"s};
const std::string directionNodeName{"Direction"s};
const std::string gridCellNodeName{"GridCell"s};
const std::string workerNodeName{"Worker"s};

const std::string idAttrName{"id"s};
const std::string xAttrName{"x"s};
const std::string yAttrName{"y"s};
const std::string valueAttrName{"value"s};
}

template <typename ErrorHandler>
glm::ivec2 loadWorkerPositionNode(
  const xml::xml_node<>& node,
  const glm::ivec2& gridSize,
  const ErrorHandler& errorHandler)
{
  const xml::xml_attribute<char>
    *xPosAttr = nullptr,
    *yPosAttr = nullptr;
  std::tie(xPosAttr, yPosAttr) = xml::getAttributes(node, errorHandler, xAttrName, yAttrName);

  glm::ivec2 result;
  char* end = nullptr;

  result.x = static_cast<int>(std::strtol(xPosAttr->value(), &end, 10));
  if (end == xPosAttr->value() || *end != '\0' ||
      result.x >= gridSize.x || result.x < 0)
    errorHandler(xAttrName, " attribute has an invalid value.\n",
      "Valid values are integers from 0 to ", gridSize.x - 1,
      " (level grid size is ", gridSize.x, " x ", gridSize.y, ").");

  result.y = static_cast<int>(std::strtol(yPosAttr->value(), &end, 10));
  if (end == yPosAttr->value() || *end != '\0' ||
      result.y >= gridSize.y || result.y < 0)
    errorHandler(yAttrName, " attribute has an invalid value.\n",
      "Valid values are integers from 0 to ", gridSize.y - 1,
      " (level grid size is ", gridSize.x, " x ", gridSize.y, ").");

  return result;
}

template <typename ErrorHandler>
Direction loadWorkerDirectionNode(
  const xml::xml_node<>& node,
  const ErrorHandler& errorHandler)
{
  auto valueAttr = xml::getAttribute(node, errorHandler, valueAttrName);

  auto direction = parseDirection(xml::value(valueAttr));
  if (direction == Direction::None)
    errorHandler(valueAttrName, " is invalid.\n",
      "Valid values are \"left\", \"right\", \"up\" or \"down\".");
  return direction;
}

template <typename ErrorHandler>
std::tuple<
  std::size_t, // Worker ID
  glm::ivec2,  // Worker position
  Direction    // Worker direction
> loadWorkerStateNode(
  const xml::xml_node<>& node,
  const glm::ivec2& gridSize,
  std::size_t numWorkers,
  const ErrorHandler& errorHandler)
{
  auto idAttr = xml::getAttribute(node, errorHandler, idAttrName);
  
  char* end = nullptr;
  std::size_t workerID = std::strtoul(idAttr->value(), &end, 10);
  if (end == idAttr->value() || *end != '\0' || workerID > numWorkers)
    errorHandler(idAttrName, " attribute is invalid.\n",
      "Valid values are integer IDs from 0 to ", numWorkers - 1,
      " (level has ", numWorkers, " workers).");

  auto positionNode = xml::firstNode(node, positionNodeName);
  if (!positionNode)
    errorHandler("missing ", positionNodeName, " node");
  auto position = loadWorkerPositionNode(*positionNode, gridSize,
    xml::makeErrorHandler(errorHandler, positionNodeName, " node: "));

  auto directionNode = xml::firstNode(node, directionNodeName);
  if (!directionNode)
    errorHandler("missing ", directionNodeName, " node");
  auto direction = loadWorkerDirectionNode(*directionNode,
    xml::makeErrorHandler(errorHandler, positionNodeName, " node: "));

  return std::make_tuple(workerID, position, direction);
}

template <typename ErrorHandler>
void loadWorkerInitialStates(
  const xml::xml_node<>& rootNode,
  const glm::ivec2& gridSize,
  SimpleFactory& factory,
  const ErrorHandler& errorHandler)
{
  ASSERT(0 < factory.numWorkers() && factory.numWorkers() <= Worker::maxWorkers);

  std::size_t workerStateID = 1u;
  for (auto workerNode = xml::firstNode(rootNode, workerStateNodeName);
       workerNode != nullptr;
       workerNode = xml::nextSibling(workerNode, workerStateNodeName), ++workerStateID)
  {
    auto workerStateErrHandler = xml::makeErrorHandler(errorHandler,
      workerStateNodeName, " node ", workerStateID, ": ");

    std::size_t workerID = 0u;
    glm::ivec2 workerPos;
    Direction workerDir = Direction::None;
    std::tie(workerID, workerPos, workerDir) =
      loadWorkerStateNode(*workerNode, gridSize, factory.numWorkers(), workerStateErrHandler);

    auto& worker = factory.worker(workerID);
    worker.setInitialPosition(workerPos);
    worker.setInitialDirection(workerDir);
  }

  // One or more workers are missing a state specification
  if (workerStateID < factory.numWorkers() + 1u) {
    ASSERT(workerStateID > 0u);
    errorHandler("missing one or more ", workerStateNodeName, " nodes; expected ",
      factory.numWorkers(), ", found ", workerStateID - 1u);
  }
}

template <typename ErrorHandler>
glm::ivec2 loadGridCellPosition(
  const xml::xml_node<>& gridCellNode,
  const glm::ivec2& gridSize,
  const ErrorHandler& errorHandler)
{
  const xml::xml_attribute<char>
    *xPosAttr = nullptr,
    *yPosAttr = nullptr;
  std::tie(xPosAttr, yPosAttr) =
    xml::getAttributes(gridCellNode, errorHandler, xAttrName, yAttrName);

  glm::ivec2 result;
  char* end = nullptr;
  result.x = static_cast<int>(std::strtol(xPosAttr->value(), &end, 10));
  if (end == xPosAttr->value() || *end != '\0' || result.x >= gridSize.x || result.x < 0)
    errorHandler(xAttrName, " attribute value is invalid.\n",
      "Valid values are integers from 0 to ", gridSize.x - 1,
      " (level grid size is ", gridSize.x, " x ", gridSize.y, ").");

  result.y = static_cast<int>(std::strtol(yPosAttr->value(), &end, 10));
  if (end == yPosAttr->value() || *end != '\0' || result.y >= gridSize.y || result.y < 0)
    errorHandler(yAttrName, " attribute value is invalid.\n",
      "Valid values are integers from 0 to ", gridSize.y - 1,
      " (level grid size is ", gridSize.x, " x ", gridSize.y, ").");

  return result;
}

template <typename ErrorHandler>
std::size_t loadWorkerNode(
  const xml::xml_node<>& node,
  std::size_t numWorkers,
  const ErrorHandler& errorHandler)
{
  auto idAttr = xml::getAttribute(node, errorHandler, idAttrName);

  char* end = nullptr;
  std::size_t workerID = std::strtoul(idAttr->value(), &end, 10);
  if (end == idAttr->value() || *end != '\0' || workerID > numWorkers)
    errorHandler("invalid ", idAttrName, ".\nValid values are integer ids from 0 to ",
      numWorkers - 1, " (level has ", numWorkers, " workers).");
  return workerID;
}

template <typename ErrorHandler>
Direction loadGridCellDirectionNode(
  const xml::xml_node<>& node,
  const ErrorHandler& errorHandler)
{
  auto valueAttr = xml::getAttribute(node, errorHandler, valueAttrName);
  auto direction = parseDirection(xml::value(valueAttr));
  if (direction == Direction::None)
    errorHandler(directionNodeName, " node: ", valueAttrName, " is invalid.\n",
      "Valid values are \"left\", \"right\", \"up\" or \"down\".");
  return direction;
}

template <typename ErrorHandler>
void loadGridCellNode(
  const xml::xml_node<>& node,
  std::size_t numWorkers,
  GridCell& cell,
  const ErrorHandler& errorHandler)
{
  std::size_t workerNodeID = 1u;
  for (auto workerNode = xml::firstNode(node, workerNodeName);
       workerNode != nullptr;
       workerNode = xml::nextSibling(workerNode, workerNodeName), ++workerNodeID)
  {
    auto workerErrHandler = xml::makeErrorHandler(errorHandler,
      workerNodeName, ' ', workerNodeID, ": ");

    auto workerID = loadWorkerNode(*workerNode, numWorkers, workerErrHandler);

    auto directionNode = xml::firstNode(workerNode, directionNodeName);
    if (directionNode) {
      if (cell.direction(workerID) != Direction::None)
        workerErrHandler("duplicate ", directionNodeName, " node");

      auto direction = loadGridCellDirectionNode(*directionNode, workerErrHandler);

      cell.setDirection(workerID, direction);
    }
  }

  // Empty GridCell node
  if (workerNodeID == 1u)
    errorHandler("empty ", gridCellNodeName, " node is not allowed");
}

template <typename ErrorHandler>
void loadGridCells(
  const xml::xml_node<>& rootNode,
  const glm::ivec2& gridSize,
  std::size_t numWorkers,
  SimpleFactory& factory,
  const ErrorHandler& errorHandler)
{
  std::size_t gridCellID = 1u;
  for (auto gridCellNode = xml::firstNode(rootNode, gridCellNodeName);
       gridCellNode != nullptr;
       gridCellNode = xml::nextSibling(gridCellNode, gridCellNodeName), ++gridCellID)
  {
    auto gridCellErrHandler = xml::makeErrorHandler(errorHandler,
      gridCellNodeName, " node ", gridCellID, ": ");

    auto position = loadGridCellPosition(*gridCellNode, gridSize, gridCellErrHandler);
    auto& gridCell = factory.grid()(position);
    loadGridCellNode(*gridCellNode, numWorkers, gridCell, gridCellErrHandler);
  }
}

void SimpleLevelSolution::loadSolutionImpl() {
  ASSERT(!loaded() && !m_impl);
  ASSERT(m_level && m_level->loaded());
  m_impl = make_const_prop_ptr<Impl>(*m_level);

  auto errorHandler = xml::makeErrorHandler("Solution \"", name(), "\" for Level \"",
    m_level->name(), "\" has an invalid save file (", filepath(), "):\n");

  std::ifstream solutionFile(filepath().string(),
                             std::ios::in | std::ios::binary | std::ios::ate);
  if (solutionFile) {
    auto fileSize = static_cast<std::size_t>(solutionFile.tellg());
    solutionFile.seekg(0);

    std::vector<char> fileBuffer(fileSize + 1, '\0');
    fileBuffer.assign(std::istreambuf_iterator<char>(solutionFile),
                      std::istreambuf_iterator<char>());
    ASSERT(fileBuffer.size() + 1 <= fileBuffer.capacity());
    fileBuffer.push_back('\0');
    solutionFile.close();

    xml::xml_document<> xmlDoc;
    xml::parse(xmlDoc, fileBuffer.data(), fileBuffer.size(), errorHandler);

    auto solutionNode = xml::firstNode(xmlDoc, solutionNodeName);
    if (!solutionNode)
      errorHandler(solutionNodeName, " node is missing");

    auto gridSize = m_impl->factory().grid().gridSize();
    ASSERT(glm::all(glm::greaterThan(gridSize, glm::ivec2())));

    loadWorkerInitialStates(*solutionNode, gridSize, m_impl->factory(), errorHandler);
    loadGridCells(*solutionNode, gridSize, m_impl->factory().numWorkers(),
      m_impl->factory(), errorHandler);

    // Ensure the factory is properly initialized
    m_impl->factory().reset();
  } else {
    LOG_SEVERITY(logger::get(), Warning)
      << "Couldn't open " << filepath() << " for Solution \""
      << name() << "\" for Level \"" << m_level->name() << "\"; creating empty solution";
    saveSolution();
  }
}

void saveWorkerInitialStates(
  xml::xml_document<>& xmlDoc,
  xml::xml_node<>& rootNode,
  const SimpleFactory& factory)
{
  const std::size_t numWorkers = factory.numWorkers();
  ASSERT(0 < numWorkers && numWorkers <= Worker::maxWorkers);

  for (std::size_t workerID = 0u; workerID < numWorkers; ++workerID) {
    auto workerNode = xml::allocateNode(xmlDoc, xml::node_element, workerStateNodeName);

    auto& worker = factory.worker(workerID);
    ASSERT(worker.id() == workerID);

    workerNode->append_attribute(
      xml::allocateAttribute(xmlDoc, idAttrName, std::to_string(workerID)));

    auto positionNode = xml::allocateNode(xmlDoc, xml::node_element, positionNodeName);
    auto& position = worker.initialPosition();
    positionNode->append_attribute(
      xml::allocateAttribute(xmlDoc, xAttrName, std::to_string(position.x)));
    positionNode->append_attribute(
      xml::allocateAttribute(xmlDoc, yAttrName, std::to_string(position.y)));
    workerNode->append_node(positionNode);

    auto directionNode = xml::allocateNode(xmlDoc, xml::node_element, directionNodeName);
    ASSERT(worker.initialDirection() != game::Direction::None);
    directionNode->append_attribute(
      xml::allocateAttribute(xmlDoc, valueAttrName, toString(worker.initialDirection())));
    workerNode->append_node(directionNode);

    rootNode.append_node(workerNode);
  }
}

void saveGridCells(
  xml::xml_document<>& xmlDoc,
  xml::xml_node<>& rootNode,
  const SimpleFactory& factory)
{
  const glm::ivec2 gridSize = factory.grid().gridSize();
  ASSERT(glm::all(glm::greaterThan(gridSize, glm::ivec2())));
  ASSERT(glm::all(glm::lessThanEqual(gridSize, LevelGrid::maxSize)));
  const auto width = static_cast<std::size_t>(gridSize.x);
  const auto height = static_cast<std::size_t>(gridSize.y);
  const std::size_t numWorkers = factory.numWorkers();

  for (std::size_t y = 0; y < height; ++y) {
    for (std::size_t x = 0; x < width; ++x) {
      auto& gridCell = factory.grid()(x, y);
      // Only bother writing the node if it has non-default values
      bool needsGridCellNode = false;
      for (std::size_t workerID = 0; workerID < numWorkers; ++workerID) {
        if (gridCell.direction(workerID) != Direction::None)
        {
          needsGridCellNode = true;
          break;
        }
      }
      if (!needsGridCellNode) continue;

      auto gridCellNode = xml::allocateNode(xmlDoc, xml::node_element, gridCellNodeName);
      gridCellNode->append_attribute(
        xml::allocateAttribute(xmlDoc, xAttrName, std::to_string(x)));
      gridCellNode->append_attribute(
        xml::allocateAttribute(xmlDoc, yAttrName, std::to_string(y)));

      for (std::size_t workerID = 0; workerID < numWorkers; ++workerID) {
        bool needsDirectionNode = gridCell.direction(workerID) != Direction::None;
        bool needsWorkerNode = needsDirectionNode;

        if (needsWorkerNode) {
          auto workerNode = xml::allocateNode(xmlDoc, xml::node_element, workerNodeName);
          workerNode->append_attribute(
            xml::allocateAttribute(xmlDoc, idAttrName, std::to_string(workerID)));

          if (needsDirectionNode) {
            auto directionNode = xml::allocateNode(xmlDoc, xml::node_element, directionNodeName);
            directionNode->append_attribute(
              xml::allocateAttribute(xmlDoc, valueAttrName, toString(gridCell.direction(workerID))));
            workerNode->append_node(directionNode);
          }

          gridCellNode->append_node(workerNode);
        }
      }

      rootNode.append_node(gridCellNode);
    }
  }
}

void SimpleLevelSolution::saveSolutionImpl() const {
  ASSERT(m_impl);
  ASSERT(fs::exists(filepath().parent_path()));

  // Write to temporary file first to avoid corrupting the original if something goes wrong
  auto tempFilePath = fs::path(filepath()).concat(".tmp");
  std::ofstream solutionFile(tempFilePath.string(),
                             std::ios::out | std::ios::binary | std::ios::trunc);
  if (!solutionFile) {
    std::ostringstream ss;
    ss << "Failed to open temporary file " << tempFilePath << " for Solution \"" << name()
       << "\" for Level \"" << m_level->name()
       << "\" in write mode.\nDid you set an existing one as read-only?";
    throw MAKE_EXCEPTION(ss.str());
  }
  xml::xml_document<> xmlDoc;
  
  auto solutionNode = xml::allocateNode(xmlDoc, xml::node_element, solutionNodeName);
  xmlDoc.append_node(solutionNode);

  saveWorkerInitialStates(xmlDoc, *solutionNode, m_impl->factory());
  saveGridCells(xmlDoc, *solutionNode, m_impl->factory());

  solutionFile << xmlDoc;
  // Close temp file to commit to disk
  solutionFile.close();

  boost::system::error_code error;
  if (fs::exists(filepath()) && !fs::remove(filepath(), error)) {
    std::ostringstream ss;
    ss << "Failed to replace save file " << filepath() << " for Solution \"" << name()
      << "\" for Level \"" << m_level->name() << "\": " << error.message()
      << "\nDid you set the file as read-only?\n\n"
      << "Your changes have been saved in " << tempFilePath
      << "; delete " << filepath().filename() << " and rename " << tempFilePath.filename()
      << " to " << filepath().filename() << " to update the file manually.";
    throw MAKE_EXCEPTION(ss.str());
  }
  // Rename temporary to original file name
  fs::rename(tempFilePath, filepath());
}

void SimpleLevelSolution::unloadSolutionImpl() {
  ASSERT(loaded() && m_impl);
  m_impl.reset();
}

SimpleFactory& SimpleLevelSolution::factory() {
  if (!loaded()) {
    std::ostringstream ss;
    ss << "Tried to access factory for unloaded SimpleLevel \"" << name() << "\"";
    throw MAKE_EXCEPTION(ss.str());
  }
  return m_impl->factory();
}
const SimpleFactory& SimpleLevelSolution::factory() const {
  if (!loaded()) {
    std::ostringstream ss;
    ss << "Tried to access factory for unloaded SimpleLevel \"" << name() << "\"";
    throw MAKE_EXCEPTION(ss.str());
  }
  return m_impl->factory();
}

namespace {
STATIC_LOGGER_INIT(logger) {
  logging::Logger lg;
  logging::setModuleName(lg, "SimpleLevelSolution");
  return lg;
}
}

} // namespace game
