#pragma once
#ifndef GAME_LEVEL_FACTORY_H
#define GAME_LEVEL_FACTORY_H

#include <memory>
#include <type_traits>

#include "Level.h"

namespace game {

class LevelFactory {
protected:
  LevelFactory() = default;
public:
  virtual ~LevelFactory() = default;

  virtual std::unique_ptr<Level> createLevel(
    std::string name, std::string id, boost::filesystem::path directory) const = 0;
};

template <
  typename LevelType,
  typename = std::enable_if_t<std::is_base_of<Level, LevelType>::value>
>
class TemplateLevelFactory : public LevelFactory {
public:
  TemplateLevelFactory() = default;
  virtual ~TemplateLevelFactory() override = default;

  virtual std::unique_ptr<Level> createLevel(
    std::string name, std::string id, boost::filesystem::path directory) const override {
    return std::make_unique<LevelType>(std::move(name), std::move(id), std::move(directory));
  }
};

} // namepsace game

#endif // GAME_LEVEL_FACTORY_H
