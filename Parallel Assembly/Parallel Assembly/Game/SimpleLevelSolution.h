#pragma once
#ifndef GAME_SIMPLE_LEVEL_SOLUTION_H
#define GAME_SIMPLE_LEVEL_SOLUTION_H

#include "../ConstPropPtr.h"

#include "LevelSolution.h"
#include "SimpleLevel.h"

namespace game {

class SimpleFactory;

class SimpleLevelSolution : public LevelSolution {
  class Impl;

  const SimpleLevel* m_level;
  const_prop_ptr<Impl> m_impl;

  virtual void initializeEmptySolutionImpl() override;
  virtual void loadSolutionImpl() override;
  virtual void unloadSolutionImpl() override;
  virtual void saveSolutionImpl() const override;

public:
  static const std::string solutionTypeName;
  using LevelType = SimpleLevel;

  SimpleLevelSolution(const Level& level_,
                      Profile& profile_,
                      std::string name_,
                      boost::filesystem::path filepath_);
  SimpleLevelSolution(SimpleLevelSolution&& o) = default;
  virtual ~SimpleLevelSolution() override = default;

  SimpleLevelSolution& operator=(SimpleLevelSolution&& o) = default;

  virtual const Level& level() const noexcept override { return *m_level; }

  // Accesses the internal factory object if the solution is loaded.
  // If it's not loaded, this will throw an exception
  SimpleFactory& factory();
  // Const overload of factory().
  const SimpleFactory& factory() const;
};

} // namespace game

#endif // GAME_SIMPLE_LEVEL_SOLUTION_H
