#include "LevelManager.inl"

#include <sstream>
#include <fstream>
#include <tuple>

#include <boost/filesystem.hpp>
#include <boost/core/ignore_unused.hpp>
#include <boost/graph/depth_first_search.hpp>

#include "../Assert.h"
#include "../Exception.h"
#include "../StringUtils.h"

#include "SimpleLevel.h"

#include "XMLHelpers.h"

namespace game {

using namespace std::string_literals;

namespace fs = boost::filesystem;

LevelManager::LevelManager()
  : m_logger()
  , m_levelFactories()
  , m_levelsDirectory(levelsDirectoryPath)
  , m_levelsInfoFilePath()
  , m_levels()
  , m_lookupMap()
  , m_dependencyGraph()
{
  logging::setModuleName(m_logger, "LevelManager");

  m_levelsDirectory.make_preferred();
  auto directoryStatus = fs::status(m_levelsDirectory);
  if (!fs::is_directory(directoryStatus)) {
    // levelsDirectory exists and is not a directory - can't safely proceed.
    if (fs::exists(directoryStatus)) {
      std::ostringstream ss;
      ss << "Levels directory " << m_levelsDirectory << " exists and is not a directory.\n";
      ss << "Please delete it and reinstall the game.";
      throw MAKE_EXCEPTION(ss.str());
    // levelsDirectory doesn't exist; there are no levels so we can't proceed.
    } else {
      std::ostringstream ss;
      ss << "Levels directory " << m_levelsDirectory << " is missing; please reinstall the game.";
      throw MAKE_EXCEPTION(ss.str());
    }
  }
  m_levelsInfoFilePath = m_levelsDirectory / levelsInfoFileName;

  registerLevelTypes();
  loadLevelsData();
}
LevelManager::~LevelManager() {}

namespace {

const std::string levelNodeName{"Level"s};
const std::string levelDepNodeName{"LevelDependency"s};

const std::string nameAttrName{"name"s};
const std::string typeAttrName{"type"s};
const std::string idAttrName{"id"s};
const std::string directoryAttrName{"directory"s};
const std::string levelNameAttrName{"levelName"s};
}

template <typename ErrorHandler>
void validateLevelNode(
  const std::string& levelName,
  const std::string& levelStrID,
  const LevelManager::LookupMap& lookupMap,
  const ErrorHandler& errorHandler)
{
  auto nameLength = utf32Length(levelName);
  if (nameLength == 0u)
    errorHandler("empty \"", nameAttrName, "\" attribute; level names can't be empty");
  else if (nameLength > Level::maxNameUTF32Length)
    errorHandler(nameAttrName, " (\"", levelName, "\") exceeds max length (",
      Level::maxNameUTF32Length, "): length = ", nameLength);
  else if (lookupMap.find(levelName) != lookupMap.end())
    errorHandler("duplicate ", levelNodeName, " definition (",
      nameAttrName, " = \"", levelName, "\")");

  auto idLength = utf32Length(levelStrID);
  if (idLength == 0u)
    errorHandler("empty \"", idAttrName, "\" attribute; level ids can't be empty");
  else if (idLength > Level::maxIDUTF32Length)
    errorHandler(idAttrName, " (\"", levelStrID, "\") exceeds max length (",
      Level::maxIDUTF32Length, "): length =", idLength);
}

template <typename ErrorHandler>
void loadLevels(
  const fs::path& levelsDirectory,
  const xml::xml_document<>& xmlDoc,
  const LevelManager::FactoryMap& levelFactories,
  std::vector<std::unique_ptr<Level>>& levels,
  LevelManager::LookupMap& lookupMap,
  const ErrorHandler& errorHandler)
{
  ASSERT_MSG(levels.empty(), "loadLevels() called with non-empty levels vector");
  ASSERT_MSG(lookupMap.empty(), "loadLevels() called with non-empty level lookup map");

  std::size_t levelID = 1u;
  for (auto levelNode = xml::firstNode(xmlDoc, levelNodeName);
       levelNode != nullptr;
       levelNode = xml::nextSibling(levelNode, levelNodeName), ++levelID)
  {
    auto levelErrHandler = xml::makeErrorHandler(errorHandler,
      levelNodeName, " node ", levelID, ": ");

    const xml::xml_attribute<>*
      nameAttr = nullptr,
      *typeAttr = nullptr,
      *idAttr = nullptr,
      *directoryAttr = nullptr;
    std::tie(nameAttr, typeAttr, idAttr, directoryAttr) =
      xml::getAttributes(*levelNode, levelErrHandler,
        nameAttrName, typeAttrName, idAttrName, directoryAttrName);

    std::string levelName{xml::value(nameAttr)};
    std::string levelType{xml::value(typeAttr)};
    std::string levelStrID{xml::value(idAttr)};
    auto levelDirectory = levelsDirectory / directoryAttr->value();

    validateLevelNode(levelName, levelStrID, lookupMap, levelErrHandler);

    auto factoryIter = levelFactories.find(levelType);
    if (factoryIter == levelFactories.end())
      levelErrHandler(typeAttrName, " (\"", levelType, "\") refers to unknown level type");
    auto factory = factoryIter->second.get();

    levels.emplace_back(
      factory->createLevel(levelName, std::move(levelStrID), std::move(levelDirectory)));
    auto result = lookupMap.emplace(
      std::piecewise_construct,
      std::forward_as_tuple(std::move(levelName)),
      std::forward_as_tuple(levels.size() - 1));
    ASSERT(result.second); boost::ignore_unused(result);
  }
  if (levels.empty())
    errorHandler("no levels defined");
  ASSERT(levels.size() == lookupMap.size());
}

template <typename ErrorHandler>
std::pair<std::size_t, LevelManager::LookupMap::const_iterator>
validateLevelDependencyName(
  const std::string& levelName,
  const LevelManager::LookupMap& lookupMap,
  const LevelManager::LevelGraph& depGraph,
  const ErrorHandler& errorHandler)
{
  auto levelIter = lookupMap.find(levelName);
  if (levelIter == lookupMap.end())
    errorHandler("refers to non-existent level \"", levelName, '\"');

  auto levelVertex = levelIter->second;
  ASSERT(levelVertex == boost::vertex(levelIter->second, depGraph));
  if (boost::out_degree(levelVertex, depGraph) != 0u)
    errorHandler("duplicate ", levelDepNodeName, " definition (",
      levelNameAttrName, " = \"", levelName, "\")");

  return std::make_pair(levelVertex, levelIter);
}

template <typename ErrorHandler>
std::size_t validateLevelDependencyLevelNode(
  LevelManager::LookupMap::const_iterator levelIter,
  const std::string& depLevelName,
  const LevelManager::LookupMap& lookupMap,
  const LevelManager::LevelGraph& depGraph,
  const ErrorHandler& errorHandler)
{
  auto depLevelIter = lookupMap.find(depLevelName);
  if (depLevelIter == lookupMap.end())
    errorHandler("refers to non-existent level \"", depLevelName, '\"');
  else if (depLevelIter == levelIter)
    errorHandler("refers to the same level (", nameAttrName, " = \"", depLevelName,
      "\") as its parent ", levelDepNodeName, "\nThis indicates the level depends on itself!");

  auto depLevelVertex = depLevelIter->second;
  ASSERT(depLevelVertex == boost::vertex(depLevelIter->second, depGraph));
  // Has edge levelVertex -> depLevelVertex already?
  if (boost::edge(levelIter->second, depLevelVertex, depGraph).second)
    errorHandler("duplicate ", levelNodeName, " node (",
      nameAttrName, " = \"", depLevelName, "\")");

  return depLevelVertex;
}

template <typename ErrorHandler>
void loadLevelDependencies(
  const xml::xml_document<>& xmlDoc,
  const LevelManager::LookupMap& lookupMap,
  LevelManager::LevelGraph& depGraph,
  const ErrorHandler& errorHandler)
{
  ASSERT_MSG(!lookupMap.empty(), "loadLevelDependencies called with empty level lookup map");
  ASSERT(boost::num_vertices(depGraph) == lookupMap.size());
  ASSERT(boost::num_edges(depGraph) == 0u);

  std::size_t dependencyID = 1u;
  for (auto levelDepNode = xml::firstNode(xmlDoc, levelDepNodeName);
       levelDepNode != nullptr;
       levelDepNode = xml::nextSibling(levelDepNode, levelDepNodeName), ++dependencyID)
  {
    auto levelDepErrHandler = xml::makeErrorHandler(errorHandler,
      levelDepNodeName, " node ", dependencyID, ": ");

    auto levelName = xml::getAttributeValue(*levelDepNode, levelDepErrHandler, levelNameAttrName);

    auto result = validateLevelDependencyName(levelName, lookupMap, depGraph, levelDepErrHandler);
    auto levelVertex = result.first;
    auto levelIter = result.second;

    std::size_t levelID = 1u;
    for (auto levelNode = xml::firstNode(levelDepNode, levelNodeName);
         levelNode != nullptr;
         levelNode = xml::nextSibling(levelNode, levelNodeName), ++levelID)
    {
      auto levelErrHandler = xml::makeErrorHandler(levelDepErrHandler,
        levelNodeName, " node ", levelID, ": ");

      auto depLevelName = xml::getAttributeValue(*levelNode, levelErrHandler, nameAttrName);

      auto depLevelVertex = validateLevelDependencyLevelNode(
        levelIter, depLevelName, lookupMap, depGraph, levelErrHandler);

      boost::add_edge(levelVertex, depLevelVertex, depGraph);
    }
  }
}

class CycleDetector : public boost::dfs_visitor<> {
  bool& m_hasCycle;

public:
  CycleDetector(bool& hasCycle) : m_hasCycle(hasCycle) {}

  template <typename Edge, typename Graph>
  void back_edge(const Edge&, Graph&) {
    m_hasCycle = true;
  }
};

template <typename ErrorHandler>
void validateLevelData(
  const LevelManager::LevelGraph& dependencyGraph,
  const ErrorHandler& errorHandler,
  logging::Logger& logger)
{
  LOG_SEVERITY(logger, Debug)
    << "Validating level dependency graph...";

  bool hasCycle = false;
  boost::depth_first_search(dependencyGraph, boost::visitor(CycleDetector(hasCycle)));
  if (hasCycle)
    errorHandler("cyclic dependencies exist between levels");

  LOG_SEVERITY(logger, Debug)
    << "Validation complete";
}

void LevelManager::loadLevelsData() {
  ASSERT_MSG(m_levels.empty() && m_lookupMap.empty() &&
             boost::num_vertices(m_dependencyGraph) == 0u &&
             boost::num_edges(m_dependencyGraph) == 0u,
             "LevelManager::loadLevelsData() called with level data already loaded");

  auto errorHandler = xml::makeErrorHandler(
    "Levels info file ", m_levelsInfoFilePath, " is invalid:\n");

  // Open levelsInfo at end to get file size
  std::ifstream levelsInfoFile(m_levelsInfoFilePath.string(),
                               std::ios::in | std::ios::binary | std::ios::ate);
  if (!levelsInfoFile) {
    std::ostringstream ss;
    ss << "Failed to open " << m_levelsInfoFilePath
       << "; please try reinstalling the game and contact support if the issue persists.";
    throw MAKE_EXCEPTION(ss.str());
  }

  auto fileSize = static_cast<std::size_t>(levelsInfoFile.tellg());
  levelsInfoFile.seekg(0);

  std::vector<char> fileBuffer(fileSize + 1, '\0');
  fileBuffer.assign(std::istreambuf_iterator<char>(levelsInfoFile),
                    std::istreambuf_iterator<char>());
  ASSERT(fileBuffer.size() + 1 <= fileBuffer.capacity());
  fileBuffer.push_back('\0');
  levelsInfoFile.close();

  xml::xml_document<> xmlDoc;
  xml::parse(xmlDoc, fileBuffer.data(), fileBuffer.size(), errorHandler);

  loadLevels(m_levelsDirectory, xmlDoc, m_levelFactories, m_levels, m_lookupMap, errorHandler);

  LOG_SEVERITY(m_logger, Debug)
    << "Loaded levels";

  // Allocate graph vertices
  {
    LevelGraph temp{m_levels.size()};
    std::swap(m_dependencyGraph, temp);
  }
  loadLevelDependencies(xmlDoc, m_lookupMap, m_dependencyGraph, errorHandler);

  LOG_SEVERITY(m_logger, Debug)
    << "Loaded level dependency graph";

  logLevelData();

  validateLevelData(m_dependencyGraph, errorHandler, m_logger);
}

void LevelManager::logLevelData() const {
  std::ostringstream buffer;
  buffer << "levels = {\n";
  std::size_t levelID = 0u;
  for (auto& level : m_levels) {
    buffer << "  [" << levelID << "] ";
    buffer << '{' << nameAttrName << "=\"" << level->name();
    buffer << "\", " << idAttrName << "=\"" << level->id();
    buffer << "\", " << directoryAttrName << '=' << level->directory() << "},\n";
    ++levelID;
  }
  buffer << "}\n\ndependencies = {\n";
  auto vertices = boost::vertices(m_dependencyGraph);
  for (auto vertIter = vertices.first; vertIter != vertices.second; ++vertIter) {
    auto source = *vertIter;

    buffer << "  " << source << " -> {";
    auto outEdges = boost::out_edges(source, m_dependencyGraph);
    for (auto edgeIter = outEdges.first; edgeIter != outEdges.second; ++edgeIter) {
      ASSERT(source == boost::source(*edgeIter, m_dependencyGraph));
      auto target = boost::target(*edgeIter, m_dependencyGraph);

      if (edgeIter != outEdges.first)
        buffer << ", ";
      buffer << target;
    }
    buffer << "},\n";
  }
  buffer << "}\n";

  LOG_SEVERITY(m_logger, Debug)
    << "Level Data:\n\n" << buffer.str();
}

void LevelManager::registerLevelTypes() {
  addLevelType<SimpleLevel>();
}

std::size_t LevelManager::lookupLevelID(const std::string& name) const {
  static_assert(static_cast<std::size_t>(-1) > 1'000'000'000, "DERP");
  auto iter = m_lookupMap.find(name);
  if (iter == m_lookupMap.end())
    return static_cast<std::size_t>(-1);
  else {
    ASSERT(iter->second < m_levels.size());
    return iter->second;
  }
}
Level* LevelManager::lookupLevelByName(const std::string& name) const {
  auto iter = m_lookupMap.find(name);
  if (iter == m_lookupMap.end())
    return nullptr;
  else {
    ASSERT(iter->second < m_levels.size());
    ASSERT(m_levels[iter->second]);
    return m_levels[iter->second].get();
  }
}
Level* LevelManager::getLevel(std::size_t index) const {
  ASSERT(index < m_levels.size());
  ASSERT(m_levels[index]);
  return m_levels[index].get();
}

} // namespace game
