#pragma once
#ifndef GAME_SOLUTION_FACTORY_MANAGER_INL
#define GAME_SOLUTION_FACTORY_MANAGER_INL

#include "SolutionFactoryManager.h"

#include <sstream>

#include <boost/core/ignore_unused.hpp>

#include "../Assert.h"
#include "../Exception.h"

namespace game {

template <typename LevelType, typename SolutionFactoryType>
std::enable_if_t<
  std::is_base_of<Level, LevelType>::value &&
  std::is_base_of<LevelSolutionFactory, SolutionFactoryType>::value
> SolutionFactoryManager::addSolutionFactory(std::string name) {
  auto& levelTypeName = LevelType::levelTypeName;

  auto levelFindResult = m_levelSolutionFactoryMap.find(levelTypeName);
  if (levelFindResult != m_levelSolutionFactoryMap.end()) {
    std::ostringstream ss;
    ss << "Failed to create level solution factory with name \"" << name
       << "\" for level type \"" << levelTypeName
       << "\":\na level solution factory already exists for that level type with name \""
       << levelFindResult->second << '\"';
    throw MAKE_EXCEPTION(ss.str());
  }
  if (m_solutionFactories.find(name) != m_solutionFactories.end()) {
    std::ostringstream ss;
    ss << "Failed to create level solution factory with name \"" << name
       << "\": a factory with that name already exists";
    throw MAKE_EXCEPTION(ss.str());
  }

  auto levelResult = m_levelSolutionFactoryMap.emplace(levelTypeName, name);
  ASSERT(levelResult.second); boost::ignore_unused(levelResult);

  auto solutionResult = m_solutionFactories.emplace(
    std::move(name),
    std::make_unique<SolutionFactoryType>()
  );
  ASSERT(solutionResult.second); boost::ignore_unused(solutionResult);
}
template <typename SolutionType>
std::enable_if_t<std::is_base_of<LevelSolution, SolutionType>::value>
SolutionFactoryManager::addSolutionType() {
  addSolutionFactory<
    typename SolutionType::LevelType,
    TemplateSolutionFactory<SolutionType>>(SolutionType::solutionTypeName);
}

} // namespace game

#endif // GAME_SOLUTION_FACTORY_MANAGER_INL
