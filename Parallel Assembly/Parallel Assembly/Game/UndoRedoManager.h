#pragma once
#ifndef GAME_UNDO_REDO_MANAGER_H
#define GAME_UNDO_REDO_MANAGER_H

#include <deque>

namespace game {

template <typename T, typename Allocator = std::allocator<T>>
class UndoRedoManager final {
  using Stack = std::deque<T, Allocator>;
  Stack m_undoStack;
  Stack m_redoStack;
  std::size_t m_capacity;

public:
  UndoRedoManager(std::size_t capacity = 100u);
  UndoRedoManager(const UndoRedoManager& o) = default;
  UndoRedoManager(UndoRedoManager&& o) = default;
  ~UndoRedoManager() = default;

  UndoRedoManager& operator=(const UndoRedoManager& o) = default;
  UndoRedoManager& operator=(UndoRedoManager&& o) = default;

  bool undoEmpty() const noexcept { return m_undoStack.empty(); }
  bool redoEmpty() const noexcept { return m_redoStack.empty(); }

  std::size_t undoSize() const noexcept { return m_undoStack.size(); }
  std::size_t redoSize() const noexcept { return m_redoStack.size(); }
  std::size_t capacity() const noexcept { return m_capacity; }

  // Clears the redo stack and adds state to the undo stack
  // If undo data is at capacity, this will erase the oldest item first
  void push(const T& state);

  // Adds the current state to the redo stack, sets the current state to
  // the top of the undo stack, if it's non-empty, and pops the undo stack
  // Returns true if successful, false if the undo stack was empty.
  // If the undo stack is empty neither the current state nor
  // either stack will be modified.
  // If the redo stack is at capacity, this will erase the oldest item first
  bool undo(T& currentState);
  // Adds the current state to the undo stack, sets the current state to
  // the top of the redo stack, if it's non-empty, and pops the redo stack
  // Returns true if successful, false if the redo stack was empty.
  // If the redo stack is empty neither the current state nor
  // either stack will be modified.
  // If the undo stack is at capacity, this will erase the oldest item first
  bool redo(T& currentState);
};

} // namespace game

#endif // GAME_UNDO_REDO_MANAGER_H
