#pragma once
#ifndef GAME_LEVEL_SOLUTION_H
#define GAME_LEVEL_SOLUTION_H

#include <string>

#include <boost/filesystem/path.hpp>

namespace game {

class Level;
class Profile;

class LevelSolution {
  friend class Profile;

  Profile* m_profile;
  std::string m_name;
  boost::filesystem::path m_filepath;
  bool m_loaded;

  void setName(std::string name_);

  void setLoaded() noexcept { m_loaded = true; }
  void clearLoaded() noexcept { m_loaded = false; }

  virtual void initializeEmptySolutionImpl() = 0;
  virtual void loadSolutionImpl() = 0;
  virtual void unloadSolutionImpl() = 0;
  virtual void saveSolutionImpl() const = 0;

  // Configures the solution as if loaded from an empty solution file
  // Note: should not actually load any files
  void initializeEmptySolution();
  // Helper function for Profile to save newly created solutions
  void saveNewlyCreatedSolution();

protected:
  LevelSolution(Profile& profile_,
                std::string name_,
                boost::filesystem::path filepath_);

public:
  static constexpr std::size_t maxNameUTF32Length = 20u;

  LevelSolution(const LevelSolution&) = delete;
  LevelSolution(LevelSolution&& o) = default;
  virtual ~LevelSolution() = default;

  LevelSolution& operator=(const LevelSolution&) = delete;
  LevelSolution& operator=(LevelSolution&& o) = default;

  const Profile& profile() const noexcept { return *m_profile; }
  // Profile association is immutable

  virtual const Level& level() const noexcept = 0;
  // Level association should be immutable

  const std::string& name() const noexcept { return m_name; }

  const boost::filesystem::path& filepath() const noexcept { return m_filepath; }
  // File path is immutable

  bool loaded() const noexcept { return m_loaded; }

  void loadSolution();
  void unloadSolution();
  void saveSolution() const;
};

} // namespace game

#endif // GAME_LEVEL_SOLUTION_H
