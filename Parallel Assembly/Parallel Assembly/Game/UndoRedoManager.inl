#pragma once
#ifndef GAME_UNDO_REDO_MANAGER_INL
#define GAME_UNDO_REDO_MANAGER_INL

#include "UndoRedoManager.h"

#include <utility>

#include "../Assert.h"

namespace game {

template <typename T, typename Allocator>
UndoRedoManager<T, Allocator>::UndoRedoManager(std::size_t capacity)
  : m_undoStack()
  , m_redoStack()
  , m_capacity(capacity)
{}

template <typename T, typename Allocator>
void UndoRedoManager<T, Allocator>::push(const T& state) {
  auto pushImpl = [this](const T& state) {
    m_undoStack.emplace_back(state);
  };

  // Back up the redo stack so we can revert on exception
  // This also "clears" the redo stack
  Stack redoStackBackup;
  std::swap(m_redoStack, redoStackBackup);
  try {
    ASSERT(m_undoStack.size() <= m_capacity);
    if (m_undoStack.size() == m_capacity) {
      // Back up the old bottom of the undo stack so we can revert on exception
      T oldBottom{std::move(m_undoStack.front())};
      m_undoStack.pop_front();
      try {
        pushImpl(state);
      } catch (...) {
        // Revert the old bottom of the undo stack
        // If this throws we're screwed anyway
        m_undoStack.emplace_front(std::move(oldBottom));
        throw;
      }
    } else
      pushImpl(state);
  } catch (...) {
    // Revert the redo stack
    std::swap(m_redoStack, redoStackBackup);
    throw;
  }
}

template <typename T, typename Allocator>
bool UndoRedoManager<T, Allocator>::undo(T& currentState) {
  if (m_undoStack.empty())
    return false;

  auto undoImpl = [this](T& currentState) {
    m_redoStack.emplace_back(std::move(currentState));
    try {
      currentState = std::move(m_undoStack.back());
      m_undoStack.pop_back();
    } catch (...) {
      // Revert the current state and redo stack
      currentState = std::move(m_redoStack.back());
      m_redoStack.pop_back();
      throw;
    }
  };

  ASSERT(m_redoStack.size() <= m_capacity);
  if (m_redoStack.size() == m_capacity) {
    T oldBottom{std::move(m_redoStack.front())};
    m_redoStack.pop_front();
    try {
      undoImpl(currentState);
    } catch (...) {
      // Revert the old bottom of the redo stack
      // If this throws we're screwed anyway
      m_redoStack.emplace_front(std::move(oldBottom));
      throw;
    }
  } else
    undoImpl(currentState);

  return true;
}
template <typename T, typename Allocator>
bool UndoRedoManager<T, Allocator>::redo(T& currentState) {
  if (m_redoStack.empty())
    return false;

  auto redoImpl = [this](T& currentState) {
    m_undoStack.emplace_back(std::move(currentState));
    try {
      currentState = std::move(m_redoStack.back());
      m_redoStack.pop_back();
    } catch (...) {
      // Revert the current state and undo stack
      currentState = std::move(m_undoStack.back());
      m_undoStack.pop_back();
      throw;
    }
  };

  ASSERT(m_undoStack.size() <= m_capacity);
  if (m_undoStack.size() == m_capacity) {
    T oldBottom{std::move(m_undoStack.front())};
    m_undoStack.pop_front();
    try {
      redoImpl(currentState);
    } catch (...) {
      // Revert the old bottom of the undo stack
      // If this throws we're screwed anyway
      m_undoStack.emplace_front(std::move(oldBottom));
    }
  } else
    redoImpl(currentState);

  return true;
}

} // namespace game

#endif // GAME_UNDO_REDO_MANAGER_INL
