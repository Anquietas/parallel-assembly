#include "GridCell.h"

#include "../Assert.h"

namespace game {

GridCell::GridCell()
  : m_directions()
{}

void GridCell::setNumWorkers(std::size_t numWorkers) {
  ASSERT(m_directions.empty());
  m_directions.resize(numWorkers, Direction::None);
}

Direction GridCell::direction(std::size_t workerID) const noexcept {
  ASSERT(workerID < Worker::maxWorkers);
  ASSERT(workerID < m_directions.size());

  return m_directions[workerID];
}
void GridCell::setDirection(std::size_t workerID, Direction direction) noexcept {
  ASSERT(workerID < Worker::maxWorkers);
  ASSERT(workerID < m_directions.size());
  
  m_directions[workerID] = direction;
}

void GridCell::execute(Worker& worker) {
  std::size_t workerID = worker.id();
  ASSERT(workerID < Worker::maxWorkers);
  ASSERT(workerID < m_directions.size());

  if (m_directions[workerID] != Direction::None)
    worker.setDirection(m_directions[workerID]);
}

void GridCell::reset() {
  // TODO: implement
}

} // namespace game
