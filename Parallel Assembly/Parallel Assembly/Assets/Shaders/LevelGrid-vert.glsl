#version 330 core

uniform mat4 mvpMatrix;
uniform float gridCellSize;
uniform int gridWidth;

in vec2 vertexPos;
in vec4 colour;

out VertexData {
  vec4 colour;
} vertexOut;

void main() {
  vec2 position = ivec2(
    gl_InstanceID % gridWidth,
    gl_InstanceID / gridWidth
  ) * gridCellSize;

  gl_Position = mvpMatrix * vec4(gridCellSize * vertexPos + position, 0.f, 1.f);
  vertexOut.colour = colour;
}
