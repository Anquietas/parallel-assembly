#version 330 core

uniform mat4 mvpMatrix;
uniform vec2 boxSize;
uniform vec2 boxPosition;
uniform float gridCellSize;

in vec2 vertexPos;

void main() {
  vec2 position = vertexPos * boxSize + boxPosition;
  gl_Position = mvpMatrix * vec4(gridCellSize * position, 0.f, 1.f);
}