#version 330 core

in VertexData {
  vec4 colour;
} fragIn;

out vec4 fragOut;

void main() {
  fragOut = fragIn.colour;
}
