#version 330 core

uniform sampler2D pathTexture;

uniform vec3 backgroundColour;
uniform vec3 foregroundColour;

in VertexData {
  vec2 texCoords;
} fragIn;

out vec4 fragOut;

void main() {
  vec2 texel = texture(pathTexture, fragIn.texCoords).rg;
  vec4 colour = vec4(texel.r * backgroundColour + texel.g * foregroundColour, 1.f);
  
  fragOut = colour;
}
