#version 330 core

uniform vec4 colour;

out vec4 fragOut;

void main() {
  fragOut = colour;
};