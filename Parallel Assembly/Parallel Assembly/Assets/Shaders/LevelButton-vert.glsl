#version 330 core

uniform mat4 mvpMatrix;

in vec3 position;
in vec2 texCoords;
in vec4 colour;

out VertexData {
  vec2 texCoords;
  vec4 colour;
} vertexOut;

void main() {
  gl_Position = mvpMatrix * vec4(position, 1.f);
  vertexOut.texCoords = texCoords;
  vertexOut.colour = colour;
}
