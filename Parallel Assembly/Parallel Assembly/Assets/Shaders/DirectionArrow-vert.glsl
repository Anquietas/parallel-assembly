#version 330 core

uniform mat4 mvpMatrix;
uniform float gridCellSize;

in vec2 vertexPos;
in vec2 texCoords;
in vec2 offset;
in int direction;
in int state;

out VertexData{
vec2 texCoords;
flat int state;
} vertexOut;

const mat2 rotations[4] = mat2[](
  // Left
  mat2(0.f, -1.f,
       1.f, 0.f),
  // Right
  mat2(0.f, 1.f,
       -1.f, 0.f),
  // Up
  mat2(1.f), // identity
  // Down
  mat2(-1.f) // -identity
);

void main() {
  vec2 position = rotations[direction] * vertexPos + offset;
  gl_Position = mvpMatrix * vec4(gridCellSize * position, 0.f, 1.f);
  vertexOut.texCoords = texCoords;
  vertexOut.state = state;
}
