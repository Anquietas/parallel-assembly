#version 330 core

uniform mat4 mvpMatrix;
uniform float gridCellSize;

in vec2 vertexPos;
in vec2 texCoords;

out VertexData{
  vec2 texCoords;
} vertexOut;

void main() {
  gl_Position = mvpMatrix * vec4(gridCellSize * vertexPos, 0.f, 1.f);
  vertexOut.texCoords = texCoords;
}
