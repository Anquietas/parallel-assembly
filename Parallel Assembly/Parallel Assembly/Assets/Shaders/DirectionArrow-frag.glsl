#version 330 core

uniform sampler2D arrowTexture;

uniform vec3 arrowColour;
uniform vec3 borderColour;

in VertexData{
vec2 texCoords;
flat int state;
} fragIn;

out vec4 fragOut;

const float arrowStateMixRatio[4] = float[](
  0.f,  // 0: normal
  0.35f,// 1: hover
  0.3f, // 2: selected
  0.4f  // 3: selected hover
);
const float borderStateMixRatio[4] = float[](
  0.f,  // 0: normal
  0.4f, // 1: hover
  1.f,  // 2: selected
  1.f   // 3: selected hover
);

void main() {
  vec4 texel = texture(arrowTexture, fragIn.texCoords);

  if (texel.a < 0.001) {
    discard;
    return;
  }

  vec3 colour =
    texel.r * mix(arrowColour, vec3(1.f), arrowStateMixRatio[fragIn.state]) +
    texel.g * mix(borderColour, vec3(1.f), borderStateMixRatio[fragIn.state]);

  fragOut = vec4(colour, texel.a);
}
