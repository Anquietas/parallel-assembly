#version 330 core

uniform sampler2D maskTexture;
uniform sampler2D imageTexture;

in VertexData {
  vec2 texCoords;
  vec4 colour;
} fragIn;

out vec4 fragOut;

void main() {
  float maskAlpha = texture(maskTexture, fragIn.texCoords).a;
  vec4 imageColour = texture(imageTexture, fragIn.texCoords);
  vec4 colourOut = vec4(imageColour.rgb, imageColour.a * maskAlpha) * fragIn.colour;
  
  if (colourOut.a < 0.001)
    discard;
  
  fragOut = colourOut;
}
