#include "StringUtils.h"

#include <codecvt>
#include <cuchar>

#include "Assert.h"

namespace {
struct UTF32Converter : std::codecvt<char32_t, char, std::mbstate_t> {} converter;
}

std::size_t utf32Length(const char* str, std::size_t utf8Size) {
  if (utf8Size == 0u)
    return 0u;

  std::mbstate_t state;
  return converter.length(state, str, str + utf8Size, utf8Size);
}

std::pair<std::size_t, std::size_t>
getLinePosition(const char* begin, const char* end, const char* where) {
  ASSERT(begin <= end);
  ASSERT(begin <= where && where <= end);

  if (begin == end || begin == where)
    return {1u, 0u};

  std::size_t line = 1u;
  std::size_t column = 0u;

  char32_t utf32 = U'\0';
  std::mbstate_t state{};

  const char* p = begin;
  std::int64_t result = std::mbrtoc32(&utf32, p, end - p, &state);
  while (p < where && p < end) {
    // Deliberately include null characters (result == 0)
    // RapidXML inserts nulls while parsing
    if (result >= 0) {
      if (utf32 == U'\n') {
        ++line;
        column = 0;
      } else
        ++column;

      p += result ? result : 1;
    } else {
      // This indicates file corruption; -1 = invalid unicode, -2 = incomplete unicode
      ASSERT(result < 0 && result >= -2);
      return {-1, -1};
    }
    result = std::mbrtoc32(&utf32, p, end - p, &state);
  }
  return {line, column};
}
