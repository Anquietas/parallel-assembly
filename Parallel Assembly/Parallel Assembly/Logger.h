#pragma once
#ifndef LOGGER_H
#define LOGGER_H

#include <boost/log/common.hpp>
#include <boost/log/sources/severity_logger.hpp>

namespace logging {

namespace expr = boost::log::expressions;
namespace sinks = boost::log::sinks;
namespace attr = boost::log::attributes;
namespace keywords = boost::log::keywords;

enum class Severity {
  Pedantic = 0,
  Profiling,
  Debug,
  Info,
  Normal,
  Warning,
  Error,
  Fatal,
};


// Global minimum for performance tuning
constexpr Severity minSeverity = Severity::Debug;
// Minimum severity for log file
constexpr Severity minLogFileSeverity = Severity::Pedantic;
// Minimum severity for console 
constexpr Severity minConsoleSeverity = Severity::Info;

// For one logger per thread
using Logger = boost::log::sources::severity_logger<Severity>;
// For logger shared between threads
using ThreadsafeLogger = boost::log::sources::severity_logger_mt<Severity>;

std::ostream& operator<<(std::ostream& out, Severity sev);

void initialize();
void setModuleName(Logger& logger, const char* name);
void setModuleName(ThreadsafeLogger& logger, const char* name);

void flush();

namespace detail {

struct DummyStream {};

// Discards all input
template <typename T>
inline constexpr const DummyStream& operator<<(const DummyStream& out, const T&) {
  return out;
}

template <typename LoggerT>
struct StreamWrapper {
  LoggerT& logger;
  boost::log::record record;
  // Hacky workaround to allow passing temporary around for operator<<
  mutable boost::log::record_ostream stream;

  StreamWrapper(LoggerT& logger_, Severity level)
    : logger(logger_)
    , record(logger.open_record(keywords::severity = level))
    , stream(record)
  {}
  StreamWrapper(const StreamWrapper&) = delete;
  StreamWrapper(StreamWrapper&& o)
    : logger(o.logger)
    , record(std::move(o.record))
    , stream(record)
  {}
  ~StreamWrapper() {
    stream.flush();
    logger.push_record(std::move(record));
  }

  StreamWrapper& operator=(const StreamWrapper&) = delete;
  // Can't assign a reference
  StreamWrapper& operator=(StreamWrapper&& o) = delete;
};

template <typename LoggerT, typename T>
inline const StreamWrapper<LoggerT>& operator<<(const StreamWrapper<LoggerT>& out, T&& val) {
  out.stream << std::forward<T>(val);
  return out;
}

// Helper to direct log to dummy stream if severity is too low
struct EnableLogger {

  template <Severity level, typename LoggerT>
  static inline constexpr DummyStream get(
    LoggerT&,
    std::enable_if_t<level < minSeverity>* = 0)
  {
    static_assert(level < minSeverity, "Derp");
    return DummyStream();
  }

  template <Severity level, typename LoggerT,
    typename = std::enable_if_t<level >= minSeverity>>
  static inline StreamWrapper<LoggerT> get(LoggerT& logger) {
    static_assert(level >= minSeverity, "Derp");
    return StreamWrapper<LoggerT>(logger, level);
  }

};

} // namespace detail

} // namespace logging

#define LOG_SEVERITY(logger, level) \
  logging::detail::EnableLogger::get<logging::Severity::level>(logger)

#define STATIC_LOGGER_DECLARE(name) \
  BOOST_LOG_GLOBAL_LOGGER(name, logging::Logger)
#define STATIC_LOGGER_INIT(name) \
  BOOST_LOG_GLOBAL_LOGGER_INIT(name, logging::Logger)

#define STATIC_THREADSAFE_LOGGER_DECLARE(name) \
  BOOST_LOG_GLOBAL_LOGGER(name, logging::ThreadsafeLogger)
#define STATIC_THREADSAFE_LOGGER_INIT(name) \
  BOOST_LOG_GLOBAL_LOGGER_INIT(name, logging::ThreadsafeLogger)

#endif // LOGGER_H
