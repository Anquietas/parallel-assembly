#pragma once
#ifndef ASSERT_H
#define ASSERT_H

#define BOOST_ENABLE_ASSERT_DEBUG_HANDLER
#include <boost/assert.hpp>

#if defined(ASSERT) || defined(ASSERT_MSG)
#error ASSERT or ASSERT_MSG already defined
#endif

namespace boost {
void assertion_failed(const char* expr, const char* function, const char* file, long line);
void assertion_failed_msg(const char* expr, const char* msg, const char* function, const char* file, long line);
}

#define ASSERT(x) BOOST_ASSERT(x)
#define ASSERT_MSG(x, msg) BOOST_ASSERT_MSG(x, msg)

#endif // ASSERT_H
