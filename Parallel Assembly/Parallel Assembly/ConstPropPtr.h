#pragma once
#ifndef CONST_PROP_PTR_H
#define CONST_PROP_PTR_H

#include <utility>

// Const-propagating version of unique_ptr, for use with PImpl
template <typename T>
class const_prop_ptr final {
public:
  using pointer = T*;
  using const_pointer = const T*;

private:
  pointer m_ptr;

public:
  constexpr const_prop_ptr() noexcept : m_ptr() {}
  constexpr const_prop_ptr(std::nullptr_t) noexcept : m_ptr(nullptr) {}
  explicit const_prop_ptr(pointer p) noexcept : m_ptr(p) {}

  const_prop_ptr(const const_prop_ptr&) = delete;
  const_prop_ptr(const_prop_ptr&& o) noexcept
    : m_ptr(std::exchange(o.m_ptr, nullptr))
  {}
  ~const_prop_ptr() {
    delete m_ptr; // no-op for nullptr
  }

  const_prop_ptr& operator=(std::nullptr_t) noexcept {
    delete m_ptr;
    m_ptr = nullptr;
    return *this;
  }
  const_prop_ptr& operator=(const const_prop_ptr&) = delete;
  const_prop_ptr& operator=(const_prop_ptr&& o) noexcept {
    delete m_ptr;
    m_ptr = std::exchange(o.m_ptr, nullptr);
    return *this;
  }

  pointer release() noexcept {
    return std::exchange(m_ptr, nullptr);
  }
  void reset(pointer ptr = pointer()) noexcept {
    delete m_ptr;
    m_ptr = ptr;
  }
  void swap(const_prop_ptr& o) noexcept {
    using std::swap;
    swap(m_ptr, o.m_ptr);
  }

  pointer get() noexcept { return m_ptr; }
  const_pointer get() const noexcept { return m_ptr; }

  explicit operator bool() const noexcept { return m_ptr != nullptr; }

  std::add_lvalue_reference_t<T> operator*() { return *m_ptr; }
  std::add_lvalue_reference_t<std::add_const_t<T>> operator*() const { return *m_ptr; }

  pointer operator->() { return m_ptr; }
  const_pointer operator->() const { return m_ptr; }
};

template <typename T, typename... Args>
const_prop_ptr<T> make_const_prop_ptr(Args&&... args) {
  return const_prop_ptr<T>(new T(std::forward<Args>(args)...));
}

template <typename T1, typename T2>
bool operator==(const const_prop_ptr<T1>& x, const const_prop_ptr<T2>& y) {
  return x.get() == y.get();
}
template <typename T1, typename T2>
bool operator!=(const const_prop_ptr<T1>& x, const const_prop_ptr<T2>& y) {
  return x.get() != y.get();
}
template <typename T1, typename T2>
bool operator<(const const_prop_ptr<T1>& x, const const_prop_ptr<T2>& y) {
  using CommonType = std::common_type_t<
    typename const_prop_ptr<T1>::pointer,
    typename const_prop_ptr<T2>::pointer>;

  return std::less<CommonType>()(x.get(), y.get());
}
template <typename T1, typename T2>
bool operator<=(const const_prop_ptr<T1>& x, const const_prop_ptr<T2>& y) {
  return !(y < x);
}
template <typename T1, typename T2>
bool operator>(const const_prop_ptr<T1>& x, const const_prop_ptr<T2>& y) {
  return y < x;
}
template <typename T1, typename T2>
bool operator>=(const const_prop_ptr<T1>& x, const const_prop_ptr<T2>& y) {
  return !(x < y);
}
template <typename T>
bool operator==(const const_prop_ptr<T>& x, std::nullptr_t) {
  return !x;
}
template <typename T>
bool operator==(std::nullptr_t, const const_prop_ptr<T>& y) {
  return !y;
}
template <typename T>
bool operator!=(const const_prop_ptr<T>& x, std::nullptr_t) {
  return bool(x);
}
template <typename T>
bool operator!=(std::nullptr_t, const const_prop_ptr<T>& y) {
  return bool(y);
}
template <typename T>
bool operator<(const const_prop_ptr<T>& x, std::nullptr_t) {
  return std::less<typename const_prop_ptr<T>::pointer>(x.get(), nullptr);
}
template <typename T>
bool operator<(std::nullptr_t, const const_prop_ptr<T>& y) {
  return std::less<typename const_prop_ptr<T>::pointer>(nullptr, y.get());
}
template <typename T>
bool operator<=(const const_prop_ptr<T>& x, std::nullptr_t) {
  return nullptr < x;
}
template <typename T>
bool operator<=(std::nullptr_t, const const_prop_ptr<T>& y) {
  return y < nullptr;
}
template <typename T>
bool operator>(const const_prop_ptr<T>& x, std::nullptr_t) {
  return nullptr < x;
}
template <typename T>
bool operator>(std::nullptr_t, const const_prop_ptr<T>& y) {
  return y < nullptr;
}
template <typename T>
bool operator>=(const const_prop_ptr<T>& x, std::nullptr_t) {
  return !(x < nullptr)
}
template <typename T>
bool operator>=(std::nullptr_t, const const_prop_ptr<T>& y) {
  return !(nullptr < y);
}

template <typename T>
void swap(const_prop_ptr<T>& lhs, const_prop_ptr<T>& rhs) {
  lhs.swap(rhs);
}

namespace std {
template <typename T>
struct hash<const_prop_ptr<T>> {
  using argument_type = const_prop_ptr<T>;
  using result_type = std::size_t;

  result_type operator()(const argument_type& ptr) {
    return std::hash<argument_type::pointer>{}(ptr.get());
  }
};
} // namespace std

#endif // CONST_PROP_PTR_H
