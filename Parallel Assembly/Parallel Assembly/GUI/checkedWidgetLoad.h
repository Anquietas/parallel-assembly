#pragma once
#ifndef GUI_CHECKED_WIDGET_LOAD_H
#define GUI_CHECKED_WIDGET_LOAD_H

#include <type_traits>
#include <string>

#include <CEGUI/Window.h>

#include "../Assert.h"
#include "../Exception.h"

#include "stringConvert.h"

namespace gui {

// TODO: add a detection check for ExpectedType::WidgetTypeName
template <typename ExpectedType>
std::enable_if_t<
  std::is_base_of<CEGUI::Window, std::decay_t<ExpectedType>>::value,
  std::decay_t<ExpectedType>*>
  checkedWidgetLoad(CEGUI::Window* root, const std::string& sceneName, const char* widgetName)
{
  ASSERT_MSG(root, "checkedWidgetLoad() called with null root");
  ASSERT_MSG(widgetName, "checkedWidgetLoad() called with null widget name");

  using ResultType = std::decay_t<ExpectedType>*;
  auto widgetNameStr = fromStdString(widgetName);
  if (widgetNameStr.empty()) {
    auto widget = dynamic_cast<ResultType>(root);
    if (!widget) {
      std::ostringstream ss;
      ss << "Failed to load root widget for " << sceneName << ": root widget is not of type "
         << std::decay_t<ExpectedType>::WidgetTypeName;
      throw MAKE_EXCEPTION(ss.str());
    }
    return widget;
  } else {
    auto child = dynamic_cast<ResultType>(root->getChild(widgetNameStr));
    if (!child) {
      std::ostringstream ss;
      ss << "Failed to load widget " << widgetName << " for " << sceneName << ": "
         << widgetName << " is not of type " << std::decay_t<ExpectedType>::WidgetTypeName;
      throw MAKE_EXCEPTION(ss.str());
    }
    return child;
  }
}

} // namespace gui

#endif // GUI_CHECKED_WIDGET_LOAD_H
