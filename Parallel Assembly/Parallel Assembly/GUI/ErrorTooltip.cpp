#include "ErrorTooltip.h"

#include <memory>
#include <functional>

#include <boost/scope_exit.hpp>

#include <CEGUI/CoordConverter.h>
#include <CEGUI/WindowManager.h>
#include <CEGUI/Window.h>
#include <CEGUI/falagard/WidgetLookFeel.h>

#include "../Exception.h"

namespace gui {

const CEGUI::String ErrorTooltipWindowRenderer::TypeName("PAGUI/ErrorTooltip");

const CEGUI::String ErrorTooltip::WidgetTypeName("PAGUI/ErrorTooltip");
const CEGUI::String ErrorTooltip::EventNamespace("ErrorTooltip");
const CEGUI::String ErrorTooltip::EventDisplayTimeChanged("DisplayTimeChanged");
const CEGUI::String ErrorTooltip::EventHorzOffsetChanged("HorzOffsetChanged");
const CEGUI::String ErrorTooltip::EventTooltipActive("TooltipActive");
const CEGUI::String ErrorTooltip::EventTooltipInactive("TooltipInactive");
const CEGUI::String ErrorTooltip::EventTooltipTransition("TooltipTransition");

void ErrorTooltipWindowRenderer::render() {
  auto& look = getLookNFeel();
  auto& imagery = look.getStateImagery(
    d_window->isEffectiveDisabled() ? "Disabled" : "Enabled");
  imagery.render(*d_window);
}

CEGUI::Sizef ErrorTooltipWindowRenderer::getTextSize() const {
  ErrorTooltip* tooltip = dynamic_cast<ErrorTooltip*>(d_window);
  CEGUI::Sizef size(tooltip->getTextSizeImpl());

  auto& look = getLookNFeel();
  const CEGUI::Rectf textArea(
    look.getNamedArea("TextArea").getArea().getPixelRect(*tooltip));
  const CEGUI::Rectf windowArea(CEGUI::CoordConverter::asAbsolute(
    tooltip->getArea(), tooltip->getParentPixelSize()));

  size.d_width = CEGUI::CoordConverter::alignToPixels(
    size.d_width + windowArea.getWidth() - textArea.getWidth());
  size.d_height = CEGUI::CoordConverter::alignToPixels(
    size.d_height + windowArea.getHeight() - textArea.getHeight());
  return size;
}

ErrorTooltip::ErrorTooltip(const CEGUI::String& type, const CEGUI::String& name)
  : Window(type, name)
  , m_active(false)
  , m_elapsed(0.f)
  , m_target(nullptr)
  , m_displayTime(5.f)
  , m_offset(3.f, 3.f)
  , m_inUpdatePosition(false)
{
  addProperties();

  setClippedByParent(false);
  setDestroyedByParent(true);
  setAlwaysOnTop(true);
  setMousePassThroughEnabled(true);

  setUpdateMode(CEGUI::WUM_VISIBLE);

  switchToInactiveState();
  hide();
}

void ErrorTooltip::addProperties() {
  const CEGUI::String& propertyOrigin = WidgetTypeName;

  CEGUI_DEFINE_PROPERTY(
    ErrorTooltip, float, "DisplayTime",
    "Property to get/set the display timeout in seconds. Value is a float",
    &ErrorTooltip::setDisplayTime, &ErrorTooltip::getDisplayTime, 5.f);

  CEGUI_DEFINE_PROPERTY(
    ErrorTooltip, CEGUI::Vector2f, "Offset",
    "Property to get/set the offset for the tooltip in pixels. Value is a Vector2f",
    &ErrorTooltip::setOffset, &ErrorTooltip::getOffset, CEGUI::Vector2f(3.f, 3.f));
}

bool ErrorTooltip::validateWindowRenderer(const CEGUI::WindowRenderer* renderer) const {
  return dynamic_cast<const ErrorTooltipWindowRenderer*>(renderer) != nullptr;
}

WidgetPtr<ErrorTooltip> ErrorTooltip::create(const CEGUI::String& name) {
  return checkedCreateWidget<ErrorTooltip>("PAGUI/ErrorTooltip", name);
}

void ErrorTooltip::updateSelf(float elapsed) {
  Window::updateSelf(elapsed);

  if (m_active)
    doActiveState(elapsed);
}
void ErrorTooltip::onTextChanged(CEGUI::WindowEventArgs& e) {
  Window::onTextChanged(e);

  updateSize();
  updatePosition();
}

void ErrorTooltip::doActiveState(float elapsed) {
  m_elapsed += elapsed;

  // If no target, switch immediately to inactive state
  if (!m_target) {
    hide();

    switchToInactiveState();
  }
  // Else see if display timeout expired
  else if (m_displayTime > 0.f && m_elapsed >= m_displayTime)
    switchToInactiveState();

  // Update position in case the widget we're attached to has moved
  updatePosition();
}

void ErrorTooltip::switchToActiveState() {
  // If the tooltip is already visible we can skip the event trigger
  if (m_active) {
    updatePosition();
    resetTimer();
  } else {
    updatePosition();
    show();

    m_active = true;
    m_elapsed = 0.f;

    CEGUI::WindowEventArgs args(this);
    onTooltipActive(args);
  }
}
void ErrorTooltip::switchToInactiveState() {
  m_active = false;
  m_elapsed = 0.f;

  CEGUI::WindowEventArgs args(this);
  onTooltipInactive(args);
}

void ErrorTooltip::onDisplayTimeChanged(CEGUI::WindowEventArgs& e) {
  fireEvent(EventDisplayTimeChanged, e, EventNamespace);
}
void ErrorTooltip::onOffsetChanged(CEGUI::WindowEventArgs& e) {
  updatePosition();

  fireEvent(EventHorzOffsetChanged, e, EventNamespace);
}
void ErrorTooltip::onTooltipActive(CEGUI::WindowEventArgs& e) {
  fireEvent(EventTooltipActive, e, EventNamespace);
}
void ErrorTooltip::onTooltipInactive(CEGUI::WindowEventArgs& e) {
  fireEvent(EventTooltipInactive, e, EventNamespace);
}
void ErrorTooltip::onTooltipTransition(CEGUI::WindowEventArgs& e) {
  fireEvent(EventTooltipTransition, e, EventNamespace);
}

void ErrorTooltip::updatePosition() {
  if (m_inUpdatePosition)
    return;
  m_inUpdatePosition = true;
  BOOST_SCOPE_EXIT(&m_inUpdatePosition) { m_inUpdatePosition = false; } BOOST_SCOPE_EXIT_END

  CEGUI::Vector2f targetPos(0.f, 0.f);
  CEGUI::Sizef targetSize(0.f, 0.f);
  if (m_target) {
    targetPos = m_target->getPixelPosition();
    targetSize = m_target->getPixelSize();
  }
  auto pos = targetPos + CEGUI::Vector2f(targetSize.d_width, 0.f) + m_offset;
  auto position = CEGUI::UVector2(cegui_absdim(pos.d_x), cegui_absdim(pos.d_y));

  setPosition(position);
}
void ErrorTooltip::updateSize() {
  CEGUI::Sizef textSize(getTextSize());

  setSize(CEGUI::USize(cegui_absdim(textSize.d_width),
                       cegui_absdim(textSize.d_height)));
}

void ErrorTooltip::resetTimer() {
  m_elapsed = 0.f;
}

void ErrorTooltip::setTargetWindow(const CEGUI::Window* window) {
  if (!window)
    m_target = window;
  else if (window != this) {
    if (m_target != window) {
      m_target = window;
      if (m_active) {
        CEGUI::WindowEventArgs args(this);
        onTooltipTransition(args);
      }
    }
  }

  resetTimer();
}

void ErrorTooltip::setDisplayTime(float seconds) {
  if (m_displayTime != seconds) {
    m_displayTime = seconds;
    
    CEGUI::WindowEventArgs args(this);
    onDisplayTimeChanged(args);
  }
}

void ErrorTooltip::setOffset(const CEGUI::Vector2f& offset) {
  if (m_offset != offset) {
    m_offset = offset;

    CEGUI::WindowEventArgs args(this);
    onOffsetChanged(args);
  }
}

CEGUI::Sizef ErrorTooltip::getTextSizeImpl() const {
  const CEGUI::RenderedString& renderedString(getRenderedString());
  CEGUI::Sizef size(0.f, 0.f);

  const auto lineCount = renderedString.getLineCount();
  for (std::size_t i = 0; i < lineCount; ++i) {
    const CEGUI::Sizef lineSize(renderedString.getPixelSize(this, i));
    size.d_height += lineSize.d_height;

    if (lineSize.d_width > size.d_width)
      size.d_width = lineSize.d_width;
  }

  return size;
}
CEGUI::Sizef ErrorTooltip::getTextSize() const {
  if (d_windowRenderer) {
    auto renderer = static_cast<const ErrorTooltipWindowRenderer*>(d_windowRenderer);
    return renderer->getTextSize();
  } else
    return getTextSizeImpl();
}

void ErrorTooltip::showAtRightEdge(const CEGUI::Window* targetWindow,
                                   const CEGUI::String& text)
{
  setTargetWindow(targetWindow);
  setText(text);
  switchToActiveState();
}
void ErrorTooltip::showAtRightEdge(const CEGUI::Window* targetWindow,
                                   const CEGUI::String& text,
                                   const CEGUI::Vector2f& offset)
{
  setOffset(offset);
  showAtRightEdge(targetWindow, text);
}

void ErrorTooltip::hideTooltip() {
  switchToInactiveState();
}

} // namespace gui
