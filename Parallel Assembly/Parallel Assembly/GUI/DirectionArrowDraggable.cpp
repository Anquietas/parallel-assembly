#include "DirectionArrowDraggable.h"

#include <algorithm>
#include <limits>

#include <CEGUI/CoordConverter.h>
#include <CEGUI/RenderingContext.h>
#include <CEGUI/RenderingSurface.h>
#include <CEGUI/RenderTarget.h>

#include "../Game/Worker.h"

#include "../Assert.h"
#include "../Exception.h"
#include "../Logger.h"

#include "DirectionArrowGeometryBuffer.h"

namespace gui {

const CEGUI::String DirectionArrowDraggable::WidgetTypeName("PAGUI/DirectionArrowDraggable");
const CEGUI::String DirectionArrowDraggable::EventNamespace("DirectionArrowDraggable");

DirectionArrowDraggable::DirectionArrowDraggable(
  const CEGUI::String& type, const CEGUI::String& name)
  : BaseType(type, name)
  , m_geometryBuffer(new DirectionArrowGeometryBuffer())
  , m_direction(game::Direction::Left)
  , m_workerSelection(0u)
{
  ASSERT(m_direction != game::Direction::None);
  ASSERT(m_workerSelection < game::Worker::maxWorkers);
  m_geometryBuffer->setDirection(m_direction);
  m_geometryBuffer->setWorkerSelection(m_workerSelection);

  setUsingFixedDragOffset(true);
  auto size = getPixelSize();
  // For some reason (0.75f, 0) is not equivalent to this :(
  setFixedDragOffset(CEGUI::UVector2{
    cegui_absdim(0.75f * size.d_width), cegui_absdim(0.75f * size.d_height)
  });
  setPixelDragThreshold(0.75f * std::min(size.d_width, size.d_height));
}

DirectionArrowDraggable::~DirectionArrowDraggable() = default;

void DirectionArrowDraggable::onMoved(CEGUI::ElementEventArgs& e) {
  BaseType::onMoved(e);
  invalidate();
}
void DirectionArrowDraggable::onSized(CEGUI::ElementEventArgs& e) {
  BaseType::onSized(e);
  auto size = getPixelSize();
  setFixedDragOffset(CEGUI::UVector2{
    cegui_absdim(0.75f * size.d_width), cegui_absdim(0.75f * size.d_height)
  });
  setPixelDragThreshold(0.75f * std::min(size.d_width, size.d_height));
  invalidate();
}
void DirectionArrowDraggable::drawSelf(const CEGUI::RenderingContext& ctx) {
  const bool needsRedraw = d_needsRedraw;
  BaseType::drawSelf(ctx);

  if (needsRedraw) {
    auto position = getPixelPosition();
    auto size = getPixelSize();
    CEGUI::Rectf area{position, size};

    m_geometryBuffer->setDirection(m_direction);
    m_geometryBuffer->setWorkerSelection(m_workerSelection);
    m_geometryBuffer->setRenderTargetArea(ctx.surface->getRenderTarget().getArea());
    m_geometryBuffer->setRenderArea(area);
    m_geometryBuffer->updateRenderData();
  }

  ctx.surface->addGeometryBuffer(ctx.queue, *m_geometryBuffer);
}

CEGUI::Vector2f DirectionArrowDraggable::getAbsoluteDragPosition() const {
  static_assert(std::numeric_limits<float>::is_iec559, "float is not IEEE 754");

  constexpr float negInfinity = -std::numeric_limits<float>::infinity();
  if (isBeingDragged()) {
    if (isUsingFixedDragOffset())
      return getPixelPosition() + CEGUI::CoordConverter::asAbsolute(d_fixedDragOffset, getPixelSize());
    else
      return getPixelPosition() + CEGUI::CoordConverter::asAbsolute(d_dragPoint, getPixelSize());
  } else
    return {negInfinity, negInfinity};
}

void DirectionArrowDraggable::setDirection(game::Direction direction) {
  ASSERT(direction != game::Direction::None);
  if (m_direction != direction) {
    m_direction = direction;
    invalidate();
  }
}
game::Direction DirectionArrowDraggable::getDirection() const {
  return m_direction;
}

void DirectionArrowDraggable::setWorkerSelection(std::size_t workerSelection) {
  ASSERT(workerSelection < game::Worker::maxWorkers);
  if (m_workerSelection != workerSelection) {
    m_workerSelection = workerSelection;
    invalidate();
  }
}
std::size_t DirectionArrowDraggable::getWorkerSelection() const {
  return m_workerSelection;
}

} // namespace gui
