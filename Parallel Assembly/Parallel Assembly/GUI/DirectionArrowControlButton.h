#pragma once
#ifndef GUI_DIRECTION_ARROW_CONTROL_BUTTON_H
#define GUI_DIRECTION_ARROW_CONTROL_BUTTON_H

#include <memory>

#include <CEGUI/WindowRendererSets/Core/Button.h>
#include <CEGUI/widgets/PushButton.h>

#include "../Game/Direction.h"

namespace gui {

class DirectionArrowControlButton;
class DirectionArrowDraggable;
class DirectionArrowGeometryBuffer;

class DirectionArrowControlButtonRenderer : public CEGUI::FalagardButton {
  using BaseType = FalagardButton;

  DirectionArrowControlButton* m_window;

protected:
  virtual void onAttach() override;
  virtual void onDetach() override;

public:
  static const CEGUI::String TypeName;

  DirectionArrowControlButtonRenderer(const CEGUI::String& type);

  CEGUI::Rectf getRenderArea() const;

  // Don't need to override render()
};

class DirectionArrowControlButton : public CEGUI::PushButton {
  using BaseType = PushButton;

  std::unique_ptr<DirectionArrowGeometryBuffer> m_geometryBuffer;

  DirectionArrowDraggable* m_draggable;

  game::Direction m_direction;
  std::size_t m_workerSelection;

protected:
  virtual bool validateWindowRenderer(const CEGUI::WindowRenderer* renderer) const override;

  virtual void onMouseButtonDown(CEGUI::MouseEventArgs& e) override;
  virtual void onMouseButtonUp(CEGUI::MouseEventArgs& e) override;
  virtual void onMouseMove(CEGUI::MouseEventArgs& e) override;
  virtual void onCaptureLost(CEGUI::WindowEventArgs& e) override;

  virtual void drawSelf(const CEGUI::RenderingContext& ctx) override;

public:
  static const CEGUI::String WidgetTypeName;
  static const CEGUI::String EventNamespace;

  static const CEGUI::String DraggableName;

  DirectionArrowControlButton(const CEGUI::String& type, const CEGUI::String& name);
  virtual ~DirectionArrowControlButton() override;

  virtual void initialiseComponents() override;

  DirectionArrowDraggable* getDraggable() const;

  // Sets the direction for the arrow rendered on the button
  // Note: Direction::None is not a valid setting.
  void setDirection(game::Direction direction);
  game::Direction getDirection() const;

  void setWorkerSelection(std::size_t workerSelection);
  std::size_t getWorkerSelection() const;
};

} // namespace gui

#endif // GUI_DIRECTION_ARROW_CONTROL_BUTTON_H

