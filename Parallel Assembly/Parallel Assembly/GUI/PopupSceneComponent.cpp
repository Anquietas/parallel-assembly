#include "PopupSceneComponent.h"

#include <CEGUI/System.h>
#include <CEGUI/GUIContext.h>

namespace gui {

PopupSceneComponent::PopupSceneComponent(
  std::string name_,
  const CEGUI::String& layoutFileName_)
  : SceneComponent(std::move(name_), layoutFileName_)
  , m_previousModal(nullptr)
{}

PopupSceneComponent::~PopupSceneComponent() {
  if (loaded() && root()->getModalState())
    clearModal();
}

void PopupSceneComponent::setModal() {
  if (loaded()) {
    m_previousModal = CEGUI::System::getSingleton().getDefaultGUIContext().getModalWindow();
    root()->setModalState(true);
  }
}
void PopupSceneComponent::clearModal() {
  if (loaded()) {
    root()->setModalState(false);
    if (m_previousModal && m_previousModal != root())
      m_previousModal->setModalState(true);
  }
}

} // namespace gui
