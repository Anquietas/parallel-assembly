#pragma once
#ifndef GUI_MAIN_MENU_SCENE_H
#define GUI_MAIN_MENU_SCENE_H

#include "Scene.h"

#include "../Logger.h"

namespace game {
class ProfileManager;
}

namespace gui {

class MainMenuScene : public Scene {
  logging::Logger m_logger;

  const game::ProfileManager* m_profileManager;

protected:
  virtual void initializeComponents() override;
public:
  MainMenuScene(const game::ProfileManager& profileManager);
  virtual ~MainMenuScene() override = default;
};

} // namespace gui

#endif // GUI_MAIN_MENU_SCENE_H
