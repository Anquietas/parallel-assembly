#pragma once
#ifndef GUI_UI_SIMPLE_LEVEL_GRID_H
#define GUI_UI_SIMPLE_LEVEL_GRID_H

#include "../Game/Direction.h"
#include "../Game/SimpleFactory.h"
#include "../Game/UndoRedoManager.h"

#include "../ConstPropPtr.h"
#include "../GLMIncludes.h"
#include "../LazyEval.h"

#include "LevelGridSelections.h"
#include "RenderableWindow.h"

namespace game {
class SimpleFactory;
}

namespace graphics {
class LevelGridRenderer;
}

namespace gui {

class UISimpleLevelGrid : public RenderableWindow {
  using BaseType = RenderableWindow;

  CEGUI::Scrollbar* m_verticalScrollbar;
  CEGUI::Scrollbar* m_horizontalScrollbar;
  CEGUI::PushButton* m_undoButton;
  CEGUI::PushButton* m_redoButton;

  game::SimpleFactory* m_factory;
  const_prop_ptr<graphics::LevelGridRenderer> m_renderer;

  LazyEval<glm::vec2> m_viewSize;
  LazyEval<float> m_gridCellSize;
  glm::vec2 m_relOffset; // grid offset from (0,0) to (1,1)
  LazyEval<glm::vec2> m_renderOffset;
  float m_zoomLevel;

  Flag m_moveDragging;
  Flag m_leftMouseButtonPressed;
  Flag m_selectDragging;
  glm::vec2 m_dragStartPos;

  std::size_t m_selectedWorker;

  LevelGridHoverSelection m_hoverSelection;
  LevelGridSelection m_selection;
  MultiSelection m_boxSelection; // current (temporary) box selection

  game::UndoRedoManager<game::SimpleFactory> m_undoRedoManager;

  void layoutUndoRedoButtons();

  glm::vec2 getGridPositionFromMouse(const CEGUI::Vector2f& mousePos);

  // Tests if the given grid position is inside the grid boundaries
  bool isInsideGrid(const glm::ivec2& gridPos) const;
  // Tests if the given grid position is inside the grid boundaries
  bool isInsideGrid(const glm::vec2& gridPos) const;

  bool clearHoverSelection();
  bool clearSelection();

  bool setHoveredGridCell(const glm::ivec2& gridCell);
  bool setSelectedGridCell(const glm::ivec2& gridCell);

  bool setHoveredDirectionArrow(const gui::DirectionArrowSelection& newHovered);
  bool setSelectedDirectionArrow(const gui::DirectionArrowSelection& newSelected);

  bool toggleSelection(const gui::DirectionArrowSelection& arrow);

  void beginBoxSelection(bool keepOldSelection);
  void updateBoxSelection(
    const glm::vec2& startPos,
    const glm::vec2& currentPos);
  void endBoxSelection();
  void cancelBoxSelection();

  void handleMouseHover(const CEGUI::MouseEventArgs& event);
  void handleMouseClick(const CEGUI::MouseEventArgs& event);
  void handleMouseWheel(const CEGUI::MouseEventArgs& event);
  void beginMoveDrag(const CEGUI::MouseEventArgs& event);
  void handleMouseMoveDrag(const CEGUI::MouseEventArgs& event);
  void endMoveDrag();
  void beginSelectDrag(const CEGUI::MouseEventArgs& event);
  void handleMouseSelectDrag(const CEGUI::MouseEventArgs& event);
  void endSelectDrag(bool cancel = false);

  void deleteSelection();

  void pushUndo();
  void undo();
  void redo();

  glm::vec2 computeViewSize() const;
  glm::vec2 computeGridRenderSize() const;
  float computeGridCellSize() const;
  // Converts relative offset to absolute offset
  glm::vec2 convertRelativeOffset(const glm::vec2& relOffset) const;
  // Converts absolute offset to relative offset
  glm::vec2 convertAbsoluteOffset(const glm::vec2& absOffset) const;

  virtual void renderWindow() override;

protected:
  virtual void onSized(CEGUI::ElementEventArgs& e) override;

  virtual void onMouseMove(CEGUI::MouseEventArgs& e) override;
  virtual void onMouseButtonDown(CEGUI::MouseEventArgs& e) override;
  virtual void onMouseButtonUp(CEGUI::MouseEventArgs& e) override;
  virtual void onMouseWheel(CEGUI::MouseEventArgs& e) override;
  virtual void onMouseEntersArea(CEGUI::MouseEventArgs& e) override;
  virtual void onMouseLeavesArea(CEGUI::MouseEventArgs& e) override;

  virtual void onCaptureLost(CEGUI::WindowEventArgs& e) override;

  virtual void onDragDropItemDropped(CEGUI::DragDropEventArgs& e) override;

public:
  static const CEGUI::String WidgetTypeName;
  static const CEGUI::String EventNamespace;

  static const CEGUI::String VerticalScrollbarName;
  static const CEGUI::String HorizontalScrollbarName;
  static const CEGUI::String UndoButtonName;
  static const CEGUI::String RedoButtonName;

  static const float minZoomLevel; // = 1.f
  static const float maxZoomLevel; // = 4.f

  static const float dragSelectThreshold; // = 5.f

  UISimpleLevelGrid(const CEGUI::String& type, const CEGUI::String& name);
  virtual ~UISimpleLevelGrid() override;

  virtual void initialiseComponents() override;

  void clearSelections();

  void handleKeyDown(const CEGUI::KeyEventArgs& event);

  void setFactory(game::SimpleFactory& factory);

  std::size_t selectedWorker() const { return m_selectedWorker; }
  void setSelectedWorker(std::size_t worker);

  // Sets the direction for the given worker to the specified value in the currently
  // selected cell, if there is one.
  // Returns true if successful, false if there was no selected cell.
  bool setSelectedCellDirection(std::size_t worker, game::Direction direction);
};

} // namespace gui

#endif // GUI_UI_SIMPLE_LEVEL_GRID_H
