#pragma once
#ifndef GUI_ERROR_TOOLTIP_H
#define GUI_ERROR_TOOLTIP_H

#include <CEGUI/WindowRenderer.h>
#include <CEGUI/Window.h>

#include "checkedCreateWidget.h"

namespace gui {

class ErrorTooltipWindowRenderer : public CEGUI::WindowRenderer {
public:
  static const CEGUI::String TypeName;

  using WindowRenderer::WindowRenderer;

  virtual void render() override;
  virtual CEGUI::Sizef getTextSize() const;
};

class ErrorTooltip : public CEGUI::Window {
  friend class ErrorTooltipWindowRenderer;

  void addProperties();
  void updatePosition();
  void updateSize();
protected:
  bool m_active;            // ErrorTooltip is active?
  float m_elapsed;          // Time the ErrorTooltip has been visible in seconds
  const Window* m_target;   // Current target window for the ErrorTooltip
  float m_displayTime;      // Time to show the tooltip for in seconds (0: show forever)
  CEGUI::Vector2f m_offset; // Offset in pixels to display the tooltip at from the target window
  bool m_inUpdatePosition;  // Flag to prevent infinite recursion for updatePosition

  virtual bool validateWindowRenderer(const CEGUI::WindowRenderer* renderer) const override;

  virtual void updateSelf(float elapsed) override;
  virtual void onTextChanged(CEGUI::WindowEventArgs& e) override;

  virtual void doActiveState(float elapsed);

  void switchToActiveState();
  void switchToInactiveState();

  virtual CEGUI::Sizef getTextSizeImpl() const;

  // Event trigger called when the display timeout gets changed
  virtual void onDisplayTimeChanged(CEGUI::WindowEventArgs& e);
  // Event trigger called when the offset gets changed
  virtual void onOffsetChanged(CEGUI::WindowEventArgs& e);
  // Event trigger called just before the tooltip becomes active
  virtual void onTooltipActive(CEGUI::WindowEventArgs& e);
  // Event trigger called just after the tooltip becomes inactive
  virtual void onTooltipInactive(CEGUI::WindowEventArgs& e);
  // Event trigger called just after the tooltip changed target window but remained active
  virtual void onTooltipTransition(CEGUI::WindowEventArgs& e);
public:
  static const CEGUI::String WidgetTypeName;
  static const CEGUI::String EventNamespace;

  // Event fired when the display timeout for the tool tip gets changed.
  static const CEGUI::String EventDisplayTimeChanged;
  // Event fired when the horizontal offset for the tooltip is changed.
  static const CEGUI::String EventHorzOffsetChanged;
  // Event fired when the tooltip is about to get activated.
  static const CEGUI::String EventTooltipActive;
  // Event fired when the tooltip has been deactivated.
  static const CEGUI::String EventTooltipInactive;
  // Event fired when the tooltip changes target window but stays active.
  static const CEGUI::String EventTooltipTransition;

  ErrorTooltip(const CEGUI::String& type, const CEGUI::String& name);
  virtual ~ErrorTooltip() override = default;

  // Helper function to take care of creation and validating type
  static WidgetPtr<ErrorTooltip> create(const CEGUI::String& name = "ErrorTooltip");

  // Resets the timer on the tooltip
  void resetTimer();

  // Sets the target window for the tooltip.
  // This is used to position the tooltip next to the widget it's showing an error for
  void setTargetWindow(const CEGUI::Window* window);
  const CEGUI::Window* getTargetWindow() const noexcept { return m_target; }

  // Sets the time to show the tooltip for in seconds before it automatically
  // deactivates itself. 0 indicates it should never time out and deactivate.
  void setDisplayTime(float seconds);
  float getDisplayTime() const noexcept { return m_displayTime; }

  // Sets the offset for the tooltip in pixels.
  void setOffset(const CEGUI::Vector2f& offset);
  const CEGUI::Vector2f& getOffset() const noexcept { return m_offset; }

  CEGUI::Sizef getTextSize() const;

  // Helper to simplify usage
  void showAtRightEdge(const CEGUI::Window* targetWindow, const CEGUI::String& text);
  // Helper to simplify usage when a different offset is required
  void showAtRightEdge(const CEGUI::Window* targetWindow, const CEGUI::String& text, const CEGUI::Vector2f& offset);

  void hideTooltip();
};

} // namespace gui

#endif // GUI_ERROR_TOOLTIP_H
