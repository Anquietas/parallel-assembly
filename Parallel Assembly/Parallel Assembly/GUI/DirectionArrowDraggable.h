#pragma once
#ifndef GUI_DIRECTION_ARROW_DRAG_CONTAINER_H
#define GUI_DIRECTION_ARROW_DRAG_CONTAINER_H

#include <memory>

#include <CEGUI/widgets/DragContainer.h>

#include "../Game/Direction.h"

namespace gui {

class DirectionArrowGeometryBuffer;

class DirectionArrowDraggable : public CEGUI::DragContainer {
  friend class DirectionArrowControlButton;

  using BaseType = DragContainer;

  std::unique_ptr<DirectionArrowGeometryBuffer> m_geometryBuffer;

  game::Direction m_direction;
  std::size_t m_workerSelection;

protected:
  virtual void onMoved(CEGUI::ElementEventArgs& e) override;
  virtual void onSized(CEGUI::ElementEventArgs& e) override;
  virtual void drawSelf(const CEGUI::RenderingContext& ctx) override;

public:
  static const CEGUI::String WidgetTypeName;
  static const CEGUI::String EventNamespace;

  DirectionArrowDraggable(const CEGUI::String& type, const CEGUI::String& name);
  virtual ~DirectionArrowDraggable() override;

  // Returns the position the widget is being dragged from
  // in screen coordinates
  // Returns (-inf, -inf) if widget is not currently being dragged
  CEGUI::Vector2f getAbsoluteDragPosition() const;

  // Sets the direction for the arrow rendered in the widget
  // Note: Direction::None is not a valid setting.
  void setDirection(game::Direction direction);
  game::Direction getDirection() const;

  void setWorkerSelection(std::size_t workerSelection);
  std::size_t getWorkerSelection() const;
};

} // namespace gui

#endif // GUI_DIRECTION_ARROW_DRAG_CONTAINER_H
