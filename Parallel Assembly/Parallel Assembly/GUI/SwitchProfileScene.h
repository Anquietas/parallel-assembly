#pragma once
#ifndef GUI_SWITCH_PROFILE_SCENE_H
#define GUI_SWITCH_PROFILE_SCENE_H

#include "Scene.h"
#include "NameEditboxPopup.h"
#include "PopupSceneComponent.h"

#include "../Logger.h"

namespace game {
class ProfileManager;
}

namespace gui {

class AnimatedFrameWindow;
class ErrorTooltip;

class SwitchProfileScene : public Scene {
  logging::Logger m_logger;

  game::ProfileManager* m_profileManager;

  CEGUI::DefaultWindow* m_currentProfileLabel;
  CEGUI::ItemListbox* m_profileList;

  NameEditboxPopup m_nameEditboxPopup;
  bool m_isEditing;

  PopupSceneComponent m_confirmDeletePopup;
  AnimatedFrameWindow* m_confirmDeleteWindow;
  CEGUI::DefaultWindow* m_confirmDeleteProfileLabel;

  ErrorTooltip* m_errorTooltip;

  CEGUI::ItemEntry* createListboxItem();
  void initializeProfilesList();

  // Shows the window for creating/editing profiles
  // Returns true if the window was shown, false otherwise.
  bool showNewEditProfileWindow(bool edit);
  // Hides the window for creating/editing profiles
  void hideNewEditProfileWindow();

  // Shows the window for confirming delete on a profile
  // Returns true if the window was shown, false otherwise.
  bool showConfirmDeleteWindow();
  // Hides the window for confirming delete on a profile
  void hideConfirmDeleteWindow();

  bool doCreateProfile();
  bool doEditProfile();
  void doDeleteProfile();
  bool doSelectProfile();

protected:
  virtual void initializeComponents() override;
  virtual void cleanupComponents() override;
public:
  SwitchProfileScene(game::ProfileManager& profileManager);
  virtual ~SwitchProfileScene() override = default;
};

} // namespace gui

#endif // GUI_SWITCH_PROFILE_SCENE_H
