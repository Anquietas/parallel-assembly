#pragma once
#ifndef GUI_PERFORMANCE_INFO_COMPONENT_H
#define GUI_PERFORMANCE_INFO_COMPONENT_H

#include <boost/accumulators/framework/accumulator_set.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/rolling_mean.hpp>
#include <boost/accumulators/statistics/rolling_variance.hpp>

#include "../Graphics/OpenGL/Types.h"

#include "SceneComponent.h"

namespace graphics {
class Renderer;
}

namespace gui {

namespace acc = boost::accumulators;

class AnimatedFrameWindow;

class PerformanceInfoComponent final : public SceneComponent {
  template <typename T>
  using AccumulatorSet = acc::accumulator_set<T,
    acc::stats<
      acc::tag::min,
      acc::tag::max,
      acc::tag::rolling_mean,
      acc::tag::rolling_variance>>;
  static constexpr std::size_t windowSize = 1000;

  graphics::Renderer* m_renderer;
  AccumulatorSet<float> m_fpsAccumulator;
  AccumulatorSet<float> m_primitivesGenAccumulator;
  AccumulatorSet<double> m_samplesPassedAccumulator;
  AccumulatorSet<double> m_gpuTimeAccumulator;

  AnimatedFrameWindow* m_window;

  CEGUI::DefaultWindow* m_currentFPSLabel;
  CEGUI::DefaultWindow* m_currentPrimitivesGenLabel;
  CEGUI::DefaultWindow* m_currentSamplesPassedLabel;
  CEGUI::DefaultWindow* m_currentGPUTimeLabel;

  CEGUI::DefaultWindow* m_minFPSLabel;
  CEGUI::DefaultWindow* m_minPrimitivesGenLabel;
  CEGUI::DefaultWindow* m_minSamplesPassedLabel;
  CEGUI::DefaultWindow* m_minGPUTimeLabel;

  CEGUI::DefaultWindow* m_maxFPSLabel;
  CEGUI::DefaultWindow* m_maxPrimitivesGenLabel;
  CEGUI::DefaultWindow* m_maxSamplesPassedLabel;
  CEGUI::DefaultWindow* m_maxGPUTimeLabel;

  CEGUI::DefaultWindow* m_meanFPSLabel;
  CEGUI::DefaultWindow* m_meanPrimitivesGenLabel;
  CEGUI::DefaultWindow* m_meanSamplesPassedLabel;
  CEGUI::DefaultWindow* m_meanGPUTimeLabel;

  CEGUI::DefaultWindow* m_stdDevFPSLabel;
  CEGUI::DefaultWindow* m_stdDevPrimitivesGenLabel;
  CEGUI::DefaultWindow* m_stdDevSamplesPassedLabel;
  CEGUI::DefaultWindow* m_stdDevGPUTimeLabel;

  virtual void initializeComponents() override;

  virtual void onAttach() override;
  virtual void onDetach() override;

  void resetFPSAccumulator();
  void resetPrimitivesGenAccumulator();
  void resetSamplesPassedAccumulator();
  void resetGPUTimeAccumulator();
public:
  PerformanceInfoComponent(graphics::Renderer& renderer);
  virtual ~PerformanceInfoComponent() override = default;

  void show();
  void hide();
};

} // namespace gui

#endif // GUI_PERFORMANCE_INFO_COMPONENT_H
