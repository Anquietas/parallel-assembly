#include "SceneNames.h"

namespace gui {

using namespace std::string_literals;

const std::string loadingSceneName{"Loading"s};
const std::string mainMenuSceneName{"MainMenu"s};
const std::string levelSelectSceneName{"LevelSelect"s};
const std::string switchProfileSceneName{"SwitchProfile"s};
const std::string settingsSceneName{"Settings"s};

const std::string simpleLevelSceneName{"SimpleLevel"s};

} // namespace gui
