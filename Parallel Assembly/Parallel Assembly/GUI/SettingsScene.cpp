#include "SettingsScene.h"

#include <cmath>
#include <cstdlib>
#include <string>

#include <CEGUI/widgets/All.h>
#include <CEGUI/WindowManager.h>
#include <CEGUI/Window.h>

#include "../Assert.h"
#include "../GameSettings.h"

#include "parseResolution.h"
#include "stringConvert.h"

#include "ErrorTooltip.h"

#include "Scene.inl"
#include "SceneNames.h"

namespace gui {

SettingsScene::SettingsScene()
  : Scene(settingsSceneName, "settings.layout")
  , m_logger()
  , m_errorTooltip(nullptr)
  // Video tab
  , m_resolutionCombobox(nullptr)
  , m_displayModeWindowedRadio(nullptr)
  , m_displayModeBorderlessRadio(nullptr)
  , m_displayModeFullscreenRadio(nullptr)
  , m_antialiasCheckbox(nullptr)
  , m_antialiasCombobox(nullptr)
  , m_vsyncCheckbox(nullptr)
  // Audio tab
  , m_enableAudioCheckbox(nullptr)
  , m_masterVolumeLabel(nullptr)
  , m_masterVolumeSlider(nullptr)
  , m_masterVolumeEditbox(nullptr)
  , m_enableMusicLabel(nullptr)
  , m_enableMusicCheckbox(nullptr)
  , m_musicVolumeLabel(nullptr)
  , m_musicVolumeSlider(nullptr)
  , m_musicVolumeEditbox(nullptr)
  , m_enableSFXLabel(nullptr)
  , m_enableSFXCheckbox(nullptr)
  , m_sfxVolumeLabel(nullptr)
  , m_sfxVolumeSlider(nullptr)
  , m_sfxVolumeEditbox(nullptr)
{
  logging::setModuleName(m_logger, "SettingsScene");
}

void SettingsScene::initializeComponents() {
  auto frame = sceneRoot()->getChild("SettingsWindow");

  auto tabControl = getWidget<CEGUI::TabControl>("SettingsWindow/TabControl");

  initializeButtons();

  // Video tab

  auto videoTab = loadTabFromLayoutFile("settings-videoTab.layout");
  tabControl->addTab(videoTab);

  m_resolutionCombobox = checkedWidgetLoad<CEGUI::Combobox>(
    videoTab, name(), "__auto_container__/ResolutionCombobox");
  m_displayModeWindowedRadio = checkedWidgetLoad<CEGUI::RadioButton>(
    videoTab, name(), "__auto_container__/DisplayModeGroupBox/WindowedRadio");
  m_displayModeBorderlessRadio = checkedWidgetLoad<CEGUI::RadioButton>(
    videoTab, name(), "__auto_container__/DisplayModeGroupBox/BorderlessRadio");
  m_displayModeFullscreenRadio = checkedWidgetLoad<CEGUI::RadioButton>(
    videoTab, name(), "__auto_container__/DisplayModeGroupBox/FullscreenRadio");
  m_antialiasCheckbox = checkedWidgetLoad<CEGUI::ToggleButton>(
    videoTab, name(), "__auto_container__/AACheckbox");
  m_antialiasCombobox = checkedWidgetLoad<CEGUI::Combobox>(
    videoTab, name(), "__auto_container__/AACombobox");
  m_vsyncCheckbox = checkedWidgetLoad<CEGUI::ToggleButton>(
    videoTab, name(), "__auto_container__/VSyncCheckbox");

  initializeVideoTab();

  // Audio tab

  auto audioTab = loadTabFromLayoutFile("settings-audioTab.layout");
  tabControl->addTab(audioTab);

  m_enableAudioCheckbox = checkedWidgetLoad<CEGUI::ToggleButton>(
    audioTab, name(), "__auto_container__/EnableAudioCheckbox");
  m_masterVolumeLabel = checkedWidgetLoad<CEGUI::DefaultWindow>(
    audioTab, name(), "__auto_container__/MasterVolumeLabel");
  m_masterVolumeSlider = checkedWidgetLoad<CEGUI::Slider>(
    audioTab, name(), "__auto_container__/MasterVolumeSlider");
  m_masterVolumeEditbox = checkedWidgetLoad<CEGUI::Editbox>(
    audioTab, name(), "__auto_container__/MasterVolumeEditbox");
  m_enableMusicLabel = checkedWidgetLoad<CEGUI::DefaultWindow>(
    audioTab, name(), "__auto_container__/EnableMusicLabel");
  m_enableMusicCheckbox = checkedWidgetLoad<CEGUI::ToggleButton>(
    audioTab, name(), "__auto_container__/EnableMusicCheckbox");
  m_musicVolumeLabel = checkedWidgetLoad<CEGUI::DefaultWindow>(
    audioTab, name(), "__auto_container__/MusicVolumeLabel");
  m_musicVolumeSlider = checkedWidgetLoad<CEGUI::Slider>(
    audioTab, name(), "__auto_container__/MusicVolumeSlider");
  m_musicVolumeEditbox = checkedWidgetLoad<CEGUI::Editbox>(
    audioTab, name(), "__auto_container__/MusicVolumeEditbox");
  m_enableSFXLabel = checkedWidgetLoad<CEGUI::DefaultWindow>(
    audioTab, name(), "__auto_container__/EnableSFXLabel");
  m_enableSFXCheckbox = checkedWidgetLoad<CEGUI::ToggleButton>(
    audioTab, name(), "__auto_container__/EnableSFXCheckbox");
  m_sfxVolumeLabel = checkedWidgetLoad<CEGUI::DefaultWindow>(
    audioTab, name(), "__auto_container__/SFXVolumeLabel");
  m_sfxVolumeSlider = checkedWidgetLoad<CEGUI::Slider>(
    audioTab, name(), "__auto_container__/SFXVolumeSlider");
  m_sfxVolumeEditbox = checkedWidgetLoad<CEGUI::Editbox>(
    audioTab, name(), "__auto_container__/SFXVolumeEditbox");

  initializeAudioTab();

  auto errorTooltip = ErrorTooltip::create();
  m_errorTooltip = errorTooltip.get();
  sceneRoot()->addChild(m_errorTooltip);
  errorTooltip.release();

  frame->activate();

  LOG_SEVERITY(m_logger, Debug) << "Initialized Components";
}

CEGUI::ScrollablePane* SettingsScene::loadTabFromLayoutFile(const CEGUI::String& fileName) {
  auto tab = CEGUI::WindowManager::getSingleton().loadLayoutFromFile(fileName);
  ASSERT(tab); // CEGUI throws exception
  auto tabPane = dynamic_cast<CEGUI::ScrollablePane*>(tab);
  if (!tabPane) {
    std::ostringstream ss;
    ss << "Failed to load settings tab from file \"" << fileName << "\": tab root is not of type ScrollablePane";
    throw MAKE_EXCEPTION(ss.str());
  }
  return tabPane;
}

void SettingsScene::initializeButtons() {
  auto okButton = getWidget<CEGUI::PushButton>("SettingsWindow/ButtonContainer/OkButton");
  auto applyButton = getWidget<CEGUI::PushButton>("SettingsWindow/ButtonContainer/ApplyButton");
  auto cancelButton = getWidget<CEGUI::PushButton>("SettingsWindow/ButtonContainer/CancelButton");

  okButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        auto settings = GameSettings::get();

        if (setSettingsFromUI()) {
          settings->commit();
          switchToPreviousScene();
        }
        return true;
      }
    ));
  applyButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        auto settings = GameSettings::get();
        if (!settings->backupInUse())
          settings->backup();
        setSettingsFromUI();
        return true;
      }
    ));
  cancelButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        auto settings = GameSettings::get();
        if (settings->backupInUse())
          settings->revertToBackup();

        switchToPreviousScene();
        return true;
      }
    ));
}

void SettingsScene::initializeVideoTab() {
  auto settings = GameSettings::get();

  auto& currentResolution = settings->screenResolution();
  
  // Add resolutions to resolution combobox
  auto& resolutions = settings->getScreenResolutions();
  ASSERT(m_resolutionCombobox->getItemCount() == 0);
  for (auto& res : resolutions) {
    std::ostringstream text;
    text << res.x << " x " << res.y;
    CEGUI::ListboxTextItem* item = new CEGUI::ListboxTextItem(text.str());
    item->setTextColours(CEGUI::Colour{0xFFDFDFDF});
    m_resolutionCombobox->addItem(item);
    if (currentResolution == res)
      m_resolutionCombobox->setItemSelectState(item, true);
  }
  if (!m_resolutionCombobox->getSelectedItem()) {
    std::ostringstream text;
    text << currentResolution.x << " x " << currentResolution.y;
    m_resolutionCombobox->setText(fromStdString(text.str()));
  }
  m_resolutionCombobox->handleUpdatedListItemData();
  m_resolutionCombobox->subscribeEvent(
    CEGUI::Combobox::EventListSelectionAccepted, CEGUI::SubscriberSlot(
      [combobox = m_resolutionCombobox, &logger = m_logger]
      (const CEGUI::EventArgs&) -> bool {
        auto selection = combobox->getSelectedItem();
        ASSERT(selection);

        LOG_SEVERITY(logger, Debug) << "Selected resolution: " << selection->getText();
        // Prevent CEGUI from selecting the whole editbox by default
        combobox->setSelection(CEGUI::String::npos, CEGUI::String::npos);
        combobox->getEditbox()->setCaretIndex(CEGUI::String::npos);

        return true;
      }
    ));
    
  // Set display mode radio button selection
  switch (settings->displayMode()) {
  case DisplayMode::Windowed:
    m_displayModeWindowedRadio->setSelected(true);
    break;
  case DisplayMode::Borderless:
    m_displayModeBorderlessRadio->setSelected(true);
    break;
  case DisplayMode::Fullscreen:
    m_displayModeFullscreenRadio->setSelected(true);
    break;
  default:
    ASSERT(false);;
  }
  
  m_antialiasCheckbox->setSelected(settings->isAAEnabled());
  m_antialiasCheckbox->subscribeEvent(
    CEGUI::ToggleButton::EventSelectStateChanged, CEGUI::SubscriberSlot(
      [checkbox = m_antialiasCheckbox, combobox = m_antialiasCombobox]
      (const CEGUI::EventArgs&) -> bool {
        combobox->setEnabled(checkbox->isSelected());
        return true;
      }
    ));
  auto& currentAALevel = settings->aaLevel();
  // Add antialiasing levels to antialiasing level combobox
  auto& aaLevels = settings->getAALevels();
  ASSERT(m_antialiasCombobox->getItemCount() == 0);
  if (settings->isAAAvailable()) {
    for (auto iter = aaLevels.begin(), end = aaLevels.end(); iter != end; ++iter) {
      auto& level = *iter;
      std::ostringstream text;
      text << "MSAA x" << level.level;
      CEGUI::ListboxTextItem* item = new CEGUI::ListboxTextItem(fromStdString(text.str()));
      item->setTextColours(CEGUI::Colour{0xFFDFDFDF});
      m_antialiasLevelMap.emplace(item, iter);
      m_antialiasCombobox->addItem(item);
      if (currentAALevel.level == level.level) {
        m_antialiasCombobox->setItemSelectState(item, true);
      }
    }
    m_antialiasCombobox->handleUpdatedListItemData();
    m_antialiasCombobox->setEnabled(settings->isAAEnabled());
  } else {
    // Disable AA and prevent enabling if not available
    m_antialiasCheckbox->setSelected(false);
    m_antialiasCheckbox->disable();
    static const CEGUI::String tooltipText = "Anti-aliasing is not available on this platform";
    m_antialiasCheckbox->setTooltipText(tooltipText);

    m_antialiasCombobox->disable();
    m_antialiasCombobox->setText("Not Available");
    m_antialiasCombobox->setTooltipText(tooltipText);
  }

  m_vsyncCheckbox->setSelected(settings->isVSyncEnabled());
}

void SettingsScene::initializeAudioTab() {
  auto settings = GameSettings::get();

  // Set enabling/disabling of almost all widgets based on audio enable checkbox
  m_enableAudioCheckbox->subscribeEvent(
    CEGUI::ToggleButton::EventSelectStateChanged, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        bool selected = m_enableAudioCheckbox->isSelected();
        m_masterVolumeLabel->setEnabled(selected);
        m_masterVolumeSlider->setEnabled(selected);
        m_masterVolumeEditbox->setEnabled(selected);
        m_enableMusicLabel->setEnabled(selected);
        m_enableMusicCheckbox->setEnabled(selected);
        m_enableSFXLabel->setEnabled(selected);
        m_enableSFXCheckbox->setEnabled(selected);

        bool musicEnabled = selected && m_enableMusicCheckbox->isSelected();
        m_musicVolumeLabel->setEnabled(musicEnabled);
        m_musicVolumeSlider->setEnabled(musicEnabled);
        m_musicVolumeEditbox->setEnabled(musicEnabled);

        bool sfxEnabled = selected && m_enableSFXCheckbox->isSelected();
        m_sfxVolumeLabel->setEnabled(sfxEnabled);
        m_sfxVolumeSlider->setEnabled(sfxEnabled);
        m_sfxVolumeEditbox->setEnabled(sfxEnabled);
        return true;
      }
    ));
  bool audioEnabled = settings->isAudioEnabled();
  m_enableAudioCheckbox->setSelected(audioEnabled);
  m_masterVolumeLabel->setEnabled(audioEnabled);
  m_masterVolumeSlider->setEnabled(audioEnabled);
  m_masterVolumeEditbox->setEnabled(audioEnabled);
  m_enableMusicLabel->setEnabled(audioEnabled);
  m_enableMusicCheckbox->setEnabled(audioEnabled);
  m_enableSFXLabel->setEnabled(audioEnabled);
  m_enableSFXCheckbox->setEnabled(audioEnabled);

  // Set enabling/disabling of music volume widgets based on checkbox
  m_enableMusicCheckbox->subscribeEvent(
    CEGUI::ToggleButton::EventSelectStateChanged, CEGUI::SubscriberSlot(
      [ checkbox = m_enableMusicCheckbox,
        label = m_musicVolumeLabel,
        slider = m_musicVolumeSlider,
        editbox = m_musicVolumeEditbox ]
      (const CEGUI::EventArgs&) -> bool {
        bool selected = checkbox->isSelected();
        label->setEnabled(selected);
        slider->setEnabled(selected);
        editbox->setEnabled(selected);
        return true;
      }
    ));
  bool musicEnabled = audioEnabled && settings->isMusicEnabled();
  m_enableMusicCheckbox->setSelected(settings->isMusicEnabled());
  m_musicVolumeLabel->setEnabled(musicEnabled);
  m_musicVolumeSlider->setEnabled(musicEnabled);
  m_musicVolumeEditbox->setEnabled(musicEnabled);

  // Set enabling/disabling of sfx volume widgets based on checkbox
  m_enableSFXCheckbox->subscribeEvent(
    CEGUI::ToggleButton::EventSelectStateChanged, CEGUI::SubscriberSlot(
      [ checkbox = m_enableSFXCheckbox,
        label = m_sfxVolumeLabel,
        slider = m_sfxVolumeSlider,
        editbox = m_sfxVolumeEditbox ]
      (const CEGUI::EventArgs&) -> bool {
        bool selected = checkbox->isSelected();
        label->setEnabled(selected);
        slider->setEnabled(selected);
        editbox->setEnabled(selected);
        return true;
      }
    ));
  bool sfxEnabled = audioEnabled && settings->isSFXEnabled();
  m_enableSFXCheckbox->setSelected(settings->isSFXEnabled());
  m_sfxVolumeLabel->setEnabled(sfxEnabled);
  m_sfxVolumeSlider->setEnabled(sfxEnabled);
  m_sfxVolumeEditbox->setEnabled(sfxEnabled);

  // Flags to prevent potential infinite cycle of slider/editbox updates.
  // This assumes that the music and SFX widgets will not be modified
  // simultaneously (this should be single-threaded code!)
  static bool suppressEditboxHandler = false;
  static bool suppressSliderHandler = false;

  auto toIntPercent = [](float value) -> int {
    return static_cast<int>(std::round(value * 100.f));
  };

  auto setEditboxFromSlider =
    [toIntPercent](CEGUI::Slider* slider, CEGUI::Editbox* editbox) -> bool {
      if (suppressSliderHandler)
        return false;
      ASSERT(!suppressSliderHandler && !suppressEditboxHandler);

      float sliderValue = slider->getCurrentValue();
      ASSERT(0.f <= sliderValue && sliderValue <= 1.f);
      int sliderPercent = toIntPercent(sliderValue);
      ASSERT(0 <= sliderPercent && sliderPercent <= 100);

      suppressEditboxHandler = true;
      editbox->setText(fromStdString(std::to_string(sliderPercent)));
      editbox->setSelection(CEGUI::String::npos, CEGUI::String::npos);
      editbox->setCaretIndex(CEGUI::String::npos);
      suppressEditboxHandler = false;
      return true;
    };
  auto setSliderFromEditbox = 
    [](CEGUI::Slider* slider, CEGUI::Editbox* editbox) -> bool {
      if (suppressEditboxHandler)
        return false;
      ASSERT(!suppressSliderHandler && !suppressEditboxHandler);

      int editboxValue = std::atoi(toStdString(editbox->getText()).c_str());
      if (editboxValue < 0) {
        editboxValue = 0;
        suppressEditboxHandler = true;
        editbox->setText(fromStdString(std::to_string(0)));
        suppressEditboxHandler = false;
      } else if (editboxValue > 100) {
        editboxValue = 100;
        suppressEditboxHandler = true;
        editbox->setText(fromStdString(std::to_string(100)));
        suppressEditboxHandler = false;
      }
      float value = static_cast<float>(editboxValue) / 100.f;
      ASSERT(0.f <= value && value <= 1.f);
      suppressSliderHandler = true;
      slider->setCurrentValue(value);
      suppressSliderHandler = false;
      return true;
    };

  m_masterVolumeSlider->subscribeEvent(
    CEGUI::Slider::EventValueChanged, CEGUI::SubscriberSlot(
      [slider = m_masterVolumeSlider, editbox = m_masterVolumeEditbox, setEditboxFromSlider]
      (const CEGUI::EventArgs&) -> bool {
        return setEditboxFromSlider(slider, editbox);
      }
    ));
  ASSERT(0.f <= settings->masterVolume() && settings->masterVolume() <= 1.f);
  m_masterVolumeSlider->setCurrentValue(settings->masterVolume());
  m_masterVolumeEditbox->subscribeEvent(
    CEGUI::Slider::EventTextChanged, CEGUI::SubscriberSlot(
      [slider = m_masterVolumeSlider, editbox = m_masterVolumeEditbox, setSliderFromEditbox]
      (const CEGUI::EventArgs&) -> bool {
        return setSliderFromEditbox(slider, editbox);
      }
    ));
  m_masterVolumeEditbox->setText(fromStdString(std::to_string(toIntPercent(settings->masterVolume()))));

  m_musicVolumeSlider->subscribeEvent(
    CEGUI::Slider::EventValueChanged, CEGUI::SubscriberSlot(
      [slider = m_musicVolumeSlider, editbox = m_musicVolumeEditbox, setEditboxFromSlider]
      (const CEGUI::EventArgs&) -> bool {
        return setEditboxFromSlider(slider, editbox);
      }
    ));
  ASSERT(0.f <= settings->musicVolume() && settings->musicVolume() <= 1.f);
  m_musicVolumeSlider->setCurrentValue(settings->musicVolume());
  m_musicVolumeEditbox->subscribeEvent(
    CEGUI::Editbox::EventTextChanged, CEGUI::SubscriberSlot(
      [slider = m_musicVolumeSlider, editbox = m_musicVolumeEditbox, setSliderFromEditbox]
      (const CEGUI::EventArgs&) -> bool {
        return setSliderFromEditbox(slider, editbox);
      }
    ));
  m_musicVolumeEditbox->setText(fromStdString(std::to_string(toIntPercent(settings->musicVolume()))));

  m_sfxVolumeSlider->subscribeEvent(
    CEGUI::Slider::EventValueChanged, CEGUI::SubscriberSlot(
      [slider = m_sfxVolumeSlider, editbox = m_sfxVolumeEditbox, setEditboxFromSlider]
      (const CEGUI::EventArgs&) -> bool {
        return setEditboxFromSlider(slider, editbox);
      }
    ));
  ASSERT(0.f <= settings->sfxVolume() && settings->sfxVolume() <= 1.f);
  m_sfxVolumeSlider->setCurrentValue(settings->sfxVolume());
  m_sfxVolumeEditbox->subscribeEvent(
    CEGUI::Editbox::EventTextChanged, CEGUI::SubscriberSlot(
      [slider = m_sfxVolumeSlider, editbox = m_sfxVolumeEditbox, setSliderFromEditbox]
      (const CEGUI::EventArgs&) -> bool {
        return setSliderFromEditbox(slider, editbox);
      }
    ));
  m_sfxVolumeEditbox->setText(fromStdString(std::to_string(toIntPercent(settings->sfxVolume()))));
}

bool SettingsScene::setSettingsFromUI() {
  auto settings = GameSettings::get();

  // Video settings

  glm::ivec2 selectedResolution;
  static_assert(sizeof(char) == sizeof(CEGUI::utf8), "CEGUI::utf8 is a different size to char");
  auto resolutionText = toStdString(m_resolutionCombobox->getText());
  selectedResolution = parseResolution(resolutionText.data(), resolutionText.data() + resolutionText.size());
  if (selectedResolution == glm::ivec2{-1,-1} ||
      glm::any(glm::lessThan(selectedResolution, GameSettings::minResolution)))
  {
    static const CEGUI::String invalidResText =
      CEGUI::String(fromStdString("Please select or enter a valid resolution.\nMinimum resolution is " +
      std::to_string(GameSettings::minResolution.x) + " x " + std::to_string(GameSettings::minResolution.y) +
      "\nMaximum resolution is " +
      std::to_string(settings->maxResolution().x) + " x " + std::to_string(settings->maxResolution().y) +
      " (native)"));
    
    m_errorTooltip->showAtRightEdge(m_resolutionCombobox, invalidResText);

    return false;
  }
  settings->setScreenResolution(selectedResolution);

  DisplayMode selectedDisplayMode;
  auto selectedDMRadio = m_displayModeWindowedRadio->getSelectedButtonInGroup();
  ASSERT(selectedDMRadio);
  if (selectedDMRadio == m_displayModeWindowedRadio)
    selectedDisplayMode = DisplayMode::Windowed;
  else if (selectedDMRadio == m_displayModeBorderlessRadio)
    selectedDisplayMode = DisplayMode::Borderless;
  else if (selectedDMRadio == m_displayModeFullscreenRadio)
    selectedDisplayMode = DisplayMode::Fullscreen;
  else {
    ASSERT(false);
    std::ostringstream ss;
    ss << "Unrecognized display mode selection from radio button "
       << selectedDMRadio->getName() << " with text " << selectedDMRadio->getText();
    throw MAKE_EXCEPTION(ss.str());
  }
  settings->setDisplayMode(selectedDisplayMode);

  settings->setAAEnabled(m_antialiasCheckbox->isSelected());
  // Ignore selection if AA is not enabled - to prevent overwriting old setting if later re-enabled
  if (settings->isAAEnabled()) {
    auto aaSelection = m_antialiasCombobox->getSelectedItem();
    ASSERT(aaSelection);
    auto aaIter = m_antialiasLevelMap.find(aaSelection);
    if (aaIter == m_antialiasLevelMap.end()) {
      std::ostringstream ss;
      ss << "Selected antialiasing level " << aaSelection->getText()
         << " was not properly registered in antialiasing level map";
      throw MAKE_EXCEPTION(ss.str());
    }
    settings->setAALevel(*aaIter->second);
  }

  settings->setVSyncEnabled(m_vsyncCheckbox->isSelected());

  // Audio settings

  settings->setAudioEnabled(m_enableAudioCheckbox->isSelected());
  auto masterVolume = m_masterVolumeSlider->getCurrentValue();
  ASSERT(0 <= masterVolume && masterVolume <= 1.f);
  settings->setMasterVolume(masterVolume);

  settings->setMusicEnabled(m_enableMusicCheckbox->isSelected());
  auto musicVolume = m_musicVolumeSlider->getCurrentValue();
  ASSERT(0 <= musicVolume && musicVolume <= 1.f);
  settings->setMusicVolume(musicVolume);

  settings->setSFXEnabled(m_enableSFXCheckbox->isSelected());
  auto sfxVolume = m_sfxVolumeSlider->getCurrentValue();
  ASSERT(0 <= sfxVolume && sfxVolume <= 1.f);
  settings->setSFXVolume(sfxVolume);
  
  return true;
}

void SettingsScene::cleanupComponents() {
  m_antialiasLevelMap.clear();
}

} // namespace gui