#pragma once
#ifndef GUI_RENDERABLE_WIDGET_H
#define GUI_RENDERABLE_WIDGET_H

#include <CEGUI/BasicImage.h>
#include <CEGUI/RendererModules/OpenGL/Texture.h>
#include <CEGUI/RendererModules/OpenGL/GL3FBOTextureTarget.h>
#include <CEGUI/Window.h>
#include <CEGUI/WindowRendererSets/Core/Default.h>

#undef min
#undef max

#include "../Graphics/OpenGL/FrameBuffer.h"
#include "../Graphics/OpenGL/MultisampleTexture2D.h"
#include "../Graphics/OpenGL/RenderBuffer.h"

#include "../Flag.h"

namespace gui {

class RenderableWindow;

class RenderableWindowRenderer : public CEGUI::FalagardDefault {
  using BaseType = CEGUI::FalagardDefault;

  RenderableWindow* m_window;

protected:
  virtual void onAttach() override;
  virtual void onDetach() override;
public:
  static const CEGUI::String TypeName;

  RenderableWindowRenderer(const CEGUI::String& type);

  CEGUI::Rectf getRenderArea() const;

  // Don't need to override render()
};

class RenderableWindow : public CEGUI::Window {
  friend class RenderableWindowRenderer;
  using BaseType = CEGUI::Window;

  graphics::gl::FrameBuffer m_fbo;
  graphics::gl::ColourMultisampleTexture2D m_texture;
  graphics::gl::DepthStencilRenderBuffer m_depthBuffer;
  CEGUI::OpenGL3FBOTextureTarget* m_textureTarget;
  CEGUI::BasicImage* m_image;

  CEGUI::Rectf m_renderArea;
  Flag m_renderAreaInvalid;

  void setRenderArea(const CEGUI::Rectf& renderArea);
  void updateRenderArea();

  virtual void renderWindow() = 0;
protected:
  virtual void onMoved(CEGUI::ElementEventArgs& e) override;
  virtual void onSized(CEGUI::ElementEventArgs& e) override;

  virtual bool validateWindowRenderer(const CEGUI::WindowRenderer* renderer) const override;
  virtual void drawSelf(const CEGUI::RenderingContext& ctx) override;

  const CEGUI::Rectf& renderArea() const noexcept { return m_renderArea; }

  RenderableWindow(const CEGUI::String& type, const CEGUI::String& name);
public:
  static const CEGUI::String EventNamespace;

  virtual ~RenderableWindow() override;
};

} // namespace gui

#endif // GUI_RENDERABLE_WIDGET_H
