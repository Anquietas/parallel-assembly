#include "checkedCreateWidget.h"

namespace gui {

void destroyWidget(CEGUI::Window* widget) {
  CEGUI::WindowManager::getSingleton().destroyWindow(widget);
}

} // namespace gui
