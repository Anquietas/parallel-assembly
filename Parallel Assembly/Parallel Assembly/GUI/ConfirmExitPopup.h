#pragma once
#ifndef GUI_CONFIRM_EXIT_POPUP_H
#define GUI_CONFIRM_EXIT_POPUP_H

#include <CEGUI/Window.h>

#include "../Logger.h"

#include "PopupSceneComponent.h"
#include "AnimatedFrameWindow.h"

namespace gui {

class ConfirmExitPopup final : public PopupSceneComponent {
  logging::Logger m_logger;
  bool m_exitConfirmed;

  AnimatedFrameWindow* m_window;
  
  virtual void initializeComponents() override;
public:
  ConfirmExitPopup();
  virtual ~ConfirmExitPopup() override = default;

  void show();
  void hide();

  bool exitConfirmed() const { return m_exitConfirmed; }
};

} // namespace gui

#endif // GUI_CONFIRM_EXIT_POPUP_H
