#include "UISimpleLevelGrid.h"

#include <type_traits>

#include <CEGUI/widgets/Scrollbar.h>
#include <CEGUI/widgets/PushButton.h>

#undef min
#undef max

#include "../Game/SimpleFactory.h"
#include "../Game/UndoRedoManager.inl"

#include "../Graphics/LevelGridRenderConstants.h"
#include "../Graphics/LevelGridRenderer.h"

#include "../Assert.h"
#include "../Logger.h"

#include "GUIManager.h"
#include "DirectionArrowDraggable.h"

namespace gui {

namespace {
STATIC_LOGGER_DECLARE(logger);
}

const CEGUI::String UISimpleLevelGrid::WidgetTypeName("PAGUI/SimpleLevelGrid");
const CEGUI::String UISimpleLevelGrid::EventNamespace("SimpleLevelGrid");

const CEGUI::String UISimpleLevelGrid::VerticalScrollbarName("__auto_vscrollbar__");
const CEGUI::String UISimpleLevelGrid::HorizontalScrollbarName("__auto_hscrollbar__");
const CEGUI::String UISimpleLevelGrid::UndoButtonName("__auto_undo_button__");
const CEGUI::String UISimpleLevelGrid::RedoButtonName("__auto_redo_button__");

const float UISimpleLevelGrid::minZoomLevel(1.f);
const float UISimpleLevelGrid::maxZoomLevel(4.f);

const float UISimpleLevelGrid::dragSelectThreshold(5.f);

UISimpleLevelGrid::UISimpleLevelGrid(const CEGUI::String& type, const CEGUI::String& name)
  : RenderableWindow(type, name)
  , m_verticalScrollbar(nullptr)
  , m_horizontalScrollbar(nullptr)
  , m_undoButton(nullptr)
  , m_redoButton(nullptr)
  , m_factory(nullptr)
  , m_renderer()
  , m_viewSize(
    false,
    [this]() -> glm::vec2 { ASSERT(m_renderer); return m_renderer->viewSize(); },
    [this]() -> glm::vec2 { return computeViewSize(); },
    [this](const glm::vec2& viewSize) {
      m_renderer->setViewSize(viewSize);
      m_renderOffset.invalidate();
    })
  , m_gridCellSize(
    false,
    [this]() -> float { ASSERT(m_renderer); return m_renderer->gridCellSize(); },
    [this]() -> float { return computeGridCellSize(); },
    [this](float gridCellSize) {
      m_renderer->setGridCellSize(gridCellSize);
      m_renderOffset.invalidate();
    })
  , m_relOffset(0.5, 0.5)
  , m_renderOffset(
    false,
    [this]() -> glm::vec2 { ASSERT(m_renderer); return m_renderer->gridRenderOffset(); },
    [this]() -> glm::vec2 { return convertRelativeOffset(m_relOffset); },
    [this](const glm::vec2& renderOffset) { m_renderer->setGridRenderOffset(renderOffset); })
  , m_zoomLevel(1.f)
  , m_moveDragging(false)
  , m_leftMouseButtonPressed(false)
  , m_selectDragging(false)
  , m_dragStartPos()
  , m_selectedWorker(0u)
  , m_hoverSelection()
  , m_selection()
  , m_boxSelection()
  , m_undoRedoManager()
{}

UISimpleLevelGrid::~UISimpleLevelGrid() = default;

void UISimpleLevelGrid::initialiseComponents() {
  BaseType::initialiseComponents();

  m_verticalScrollbar = &dynamic_cast<CEGUI::Scrollbar&>(*getChild(VerticalScrollbarName));
  m_horizontalScrollbar = &dynamic_cast<CEGUI::Scrollbar&>(*getChild(HorizontalScrollbarName));
  m_undoButton = &dynamic_cast<CEGUI::PushButton&>(*getChild(UndoButtonName));
  m_redoButton = &dynamic_cast<CEGUI::PushButton&>(*getChild(RedoButtonName));

  m_verticalScrollbar->setPageSize(1.f);
  m_verticalScrollbar->setStepSize(1.f);
  m_verticalScrollbar->setOverlapSize(0.f);
  m_verticalScrollbar->setUnitIntervalScrollPosition(m_relOffset.y);
  m_verticalScrollbar->subscribeEvent(
    CEGUI::Scrollbar::EventScrollPositionChanged, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        auto scrollPosition = m_verticalScrollbar->getUnitIntervalScrollPosition();
        if (m_relOffset.y != scrollPosition) {
          m_relOffset.y = scrollPosition;
          m_renderOffset.invalidate();
          invalidate();
        }
        return true;
      }
    ));

  m_horizontalScrollbar->setPageSize(1.f);
  m_horizontalScrollbar->setStepSize(1.f);
  m_horizontalScrollbar->setOverlapSize(0.f);
  m_horizontalScrollbar->setUnitIntervalScrollPosition(m_relOffset.x);
  m_horizontalScrollbar->subscribeEvent(
    CEGUI::Scrollbar::EventScrollPositionChanged, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        auto scrollPosition = m_horizontalScrollbar->getUnitIntervalScrollPosition();
        if (m_relOffset.x != scrollPosition) {
          m_relOffset.x = scrollPosition;
          m_renderOffset.invalidate();
          invalidate();
        }
        return true;
      }
    ));
  m_horizontalScrollbar->subscribeEvent(
    CEGUI::Window::EventShown, CEGUI::SubscriberSlot(&UISimpleLevelGrid::layoutUndoRedoButtons, this));
  m_horizontalScrollbar->subscribeEvent(
    CEGUI::Window::EventHidden, CEGUI::SubscriberSlot(&UISimpleLevelGrid::layoutUndoRedoButtons, this));

  m_undoButton->setWantsMultiClickEvents(false);
  m_undoButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(&UISimpleLevelGrid::undo, this));
  m_redoButton->setWantsMultiClickEvents(false);
  m_redoButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(&UISimpleLevelGrid::redo, this));

  m_verticalScrollbar->hide();
  m_horizontalScrollbar->hide();
  m_undoButton->disable();
  m_redoButton->disable();

  layoutUndoRedoButtons();
}

void UISimpleLevelGrid::layoutUndoRedoButtons() {
  const auto& pixelSize = getPixelSize();
  const auto& buttonSize = m_undoButton->getPixelSize();
  const auto& horiScrollHeight = m_horizontalScrollbar->getPixelSize().d_height;
  auto buttonYPosition = pixelSize.d_height - buttonSize.d_height;
  if (m_horizontalScrollbar->isVisible())
    buttonYPosition -= horiScrollHeight;

  m_undoButton->setPosition({
    cegui_absdim(0),
    cegui_absdim(buttonYPosition)
  });
  m_redoButton->setPosition({
    cegui_absdim(buttonSize.d_width),
    cegui_absdim(buttonYPosition)
  });
}

glm::vec2 UISimpleLevelGrid::getGridPositionFromMouse(const CEGUI::Vector2f& mousePos) {
  ASSERT(m_renderer);

  const auto relativeMousePos_ = mousePos - getPixelPosition();
  const auto relativeMousePos = glm::vec2{relativeMousePos_.d_x, relativeMousePos_.d_y};
  const auto offsetMousePos = relativeMousePos - m_renderOffset.get();

  return offsetMousePos / m_gridCellSize.get();
}

bool UISimpleLevelGrid::isInsideGrid(const glm::ivec2& gridPos) const {
  ASSERT(m_renderer);
  return m_renderer->isInsideGrid(gridPos);
}
bool UISimpleLevelGrid::isInsideGrid(const glm::vec2& gridPos) const {
  ASSERT(m_renderer);
  return m_renderer->isInsideGrid(gridPos);
}

bool UISimpleLevelGrid::clearHoverSelection() {
  ASSERT(m_renderer);
  if (!m_hoverSelection.is<gui::NullSelection>()) {
    m_renderer->invalidateSelectionRenderData(m_selection);

    m_hoverSelection.clear();
    return true;
  }
  return false;
}
bool UISimpleLevelGrid::clearSelection() {
  ASSERT(m_renderer);
  if (!m_selection.is<gui::NullSelection>()) {
    m_renderer->invalidateSelectionRenderData(m_selection);

    m_selection.clear();
    return true;
  }
  return false;
}

bool UISimpleLevelGrid::setHoveredGridCell(const glm::ivec2& gridCell) {
  using namespace std::rel_ops;
  ASSERT(m_renderer);

  const gui::GridCellSelection newHovered{gridCell};

  // Hover selection is already a grid cell selection; update directly
  if (m_hoverSelection.is<gui::GridCellSelection>()) {
    auto& hovered = m_hoverSelection.as<gui::GridCellSelection>();
    if (hovered != newHovered) {
      hovered = newHovered;
      m_renderer->invalidateGridSelectionRenderData();
      return true;
    }
  } else {
    m_renderer->invalidateSelectionRenderData(m_hoverSelection);
    m_renderer->invalidateGridSelectionRenderData();

    m_hoverSelection.set(newHovered);
    return true;
  }
  return false;
}
bool UISimpleLevelGrid::setSelectedGridCell(const glm::ivec2& gridCell) {
  using namespace std::rel_ops;
  ASSERT(m_renderer);

  const gui::GridCellSelection newSelected{gridCell};

  // Selection is already a grid cell selection; update directly
  if (m_selection.is<gui::GridCellSelection>()) {
    auto& selected = m_selection.as<gui::GridCellSelection>();
    if (selected != newSelected) {
      selected = newSelected;
      m_renderer->invalidateGridSelectionRenderData();
      return true;
    }
  } else {
    m_renderer->invalidateSelectionRenderData(m_selection);
    m_renderer->invalidateGridSelectionRenderData();

    m_selection.set(newSelected);
    return true;
  }
  return false;
}

bool UISimpleLevelGrid::setHoveredDirectionArrow(const gui::DirectionArrowSelection& newHovered) {
  using namespace std::rel_ops;
  ASSERT(m_renderer);

  // Hover selection is already an arrow selection; update directly
  if (m_hoverSelection.is<gui::DirectionArrowSelection>()) {
    auto& hovered = m_hoverSelection.as<gui::DirectionArrowSelection>();
    if (hovered != newHovered) {
      m_renderer->invalidateSelectionRenderData(hovered);
      m_renderer->invalidateSelectionRenderData(newHovered);
      hovered = newHovered;
      return true;
    }
  } else {
    m_renderer->invalidateSelectionRenderData(m_hoverSelection);
    m_renderer->invalidateSelectionRenderData(newHovered);

    m_hoverSelection.set(newHovered);
    return true;
  }
  return false;
}
bool UISimpleLevelGrid::setSelectedDirectionArrow(const gui::DirectionArrowSelection& newSelected) {
  using namespace std::rel_ops;
  ASSERT(m_renderer);

  // Selection is already an arrow selection; update directly
  if (m_selection.is<gui::DirectionArrowSelection>()) {
    auto& selected = m_selection.as<gui::DirectionArrowSelection>();
    if (selected != newSelected) {
      m_renderer->invalidateSelectionRenderData(selected);
      m_renderer->invalidateSelectionRenderData(newSelected);
      selected = newSelected;
      return true;
    }
  } else {
    m_renderer->invalidateSelectionRenderData(m_selection);
    m_renderer->invalidateSelectionRenderData(newSelected);

    m_selection.set(newSelected);
    return true;
  }
  return false;
}

bool UISimpleLevelGrid::toggleSelection(const gui::DirectionArrowSelection& arrow) {
  ASSERT(m_factory && arrow.workerID < m_factory->numWorkers());

  // Current selection is an arrow; we're either going to clear the selection
  // if it's the same arrow, or convert to MultiSelection
  if (m_selection.is<gui::DirectionArrowSelection>()) {
    auto& selection = m_selection.as<gui::DirectionArrowSelection>();

    // Same arrow; clear selection
    if (selection == arrow)
      return clearSelection();
    // Different arrow; convert to MultiSelection
    else {
      gui::MultiSelection newSelection;
      bool result = newSelection.addArrowSelection(selection);
      result = result && newSelection.addArrowSelection(arrow);
      ASSERT(result);

      m_renderer->invalidateSelectionRenderData(selection);
      m_renderer->invalidateSelectionRenderData(arrow);

      m_selection.set(std::move(newSelection));

      return true;
    }
  }
  // Current selection is a MultiSelection; we're either going to add
  // the arrow to the selection or remove it. If we remove it and the
  // remaining selection is down to one item we need to convert to
  // that item's selection type
  else if (m_selection.is<gui::MultiSelection>()) {
    auto& selection = m_selection.as<gui::MultiSelection>();

    // Arrow is in current selection; remove it
    if (selection.contains(arrow)) {
      bool result = selection.removeArrowSelection(arrow);
      if (result) {
        m_renderer->invalidateSelectionRenderData(arrow);
        // We're down to one item; convert to individual selection
        if (selection.size() == 1u) {
          auto newSelection = *selection.beginArrows();
          m_selection.set(newSelection);
        }
      }
      return result;
    }
    // Arrow is not in the selection; add it
    else {
      bool result = selection.addArrowSelection(arrow);
      if (result)
        m_renderer->invalidateSelectionRenderData(arrow);
      return result;
    }
  }
  // Current selection is some other selection type; we don't care
  // about the old selection and set the current selection to
  // the arrow we wanted to add.
  else {
    m_renderer->invalidateSelectionRenderData(m_selection);
    m_renderer->invalidateSelectionRenderData(arrow);

    m_selection.set(arrow);
    return true;
  }
}

void UISimpleLevelGrid::beginBoxSelection(bool keepOldSelection) {
  ASSERT(m_renderer);
  ASSERT(m_boxSelection.empty());
  const auto numWorkers = m_factory->numWorkers();

  // Clear selection anyway if it's a grid cell selection
  if (!keepOldSelection || m_selection.is<gui::GridCellSelection>())
    clearSelection();

  m_renderer->beginBoxSelection();
}
void UISimpleLevelGrid::updateBoxSelection(
  const glm::vec2& startPos,
  const glm::vec2& currentPos)
{
  using namespace graphics::constants;
  ASSERT(m_factory && m_renderer);
  const auto numWorkers = m_factory->numWorkers();

  // Reset box selection in case the box shrunk since last time
  m_renderer->invalidateSelectionRenderData(m_boxSelection);
  m_boxSelection.clear();

  const auto size = m_factory->grid().gridSize();
  const graphics::AABoundingBox2D gridBounds{{0.f, 0.f}, glm::vec2{size}};

  const graphics::AABoundingBox2D selectionBox{startPos, currentPos};
  // If false, selection box does not intersect the grid: nothing is selected
  if (gridBounds.intersects(selectionBox)) {
    const glm::ivec2 boxMin{glm::floor(selectionBox.min())};
    const glm::ivec2 boxMax{glm::floor(selectionBox.max())};

    ASSERT(glm::all(glm::greaterThan(size, glm::ivec2{1, 1})));
    const auto searchMin = glm::clamp(boxMin, glm::ivec2{0, 0}, size - glm::ivec2{1, 1});
    const auto searchMax = glm::clamp(boxMax, glm::ivec2{0, 0}, size - glm::ivec2{1, 1});
    ASSERT(glm::all(glm::lessThanEqual(searchMin, searchMax)));

    const auto& grid = m_factory->grid();
    for (std::size_t worker = 0u; worker < numWorkers; ++worker) {
      for (int x = searchMin.x; x <= searchMax.x; ++x) {
        for (int y = searchMin.y; y <= searchMax.y; ++y) {
          glm::ivec2 gridPos{x, y};
          auto& gridCell = grid(gridPos);

          auto direction = gridCell.direction(worker);
          if (direction != game::Direction::None) {
            auto arrowBounds = lookupDirectionArrowBounds(worker, direction)
              + glm::vec2{gridPos};

            if (selectionBox.contains(arrowBounds)) {
              bool result = m_boxSelection.addArrowSelection({gridPos, worker});
              ASSERT(result); (void)(result);
            }
          }
        }
      }
    }
  }
  m_renderer->invalidateSelectionRenderData(m_boxSelection);
  m_renderer->updateBoxSelection(selectionBox);
}
void UISimpleLevelGrid::endBoxSelection() {
  ASSERT(m_renderer);
  // Don't need to bother invalidating anything since we're not actually
  // changing the selection, only how it's represented.

  m_renderer->endBoxSelection();

  // Don't need to do anything is the box selection is empty
  if (m_boxSelection.empty())
    return;

  // Nothing selected, so just directly transfer the selection
  if (m_selection.is<gui::NullSelection>()) {
    // Box selection has multiple elements, don't need to convert
    if (m_boxSelection.size() > 1u) {
      m_selection.set(std::move(m_boxSelection));
      m_boxSelection = gui::MultiSelection{};
    }
    // Box selection has exactly one element, need to convert
    else {
      auto& selection = *m_boxSelection.beginArrows();
      ASSERT(isInsideGrid(selection.gridCell) && m_factory &&
             selection.workerID < m_factory->numWorkers());
      m_selection.set(std::move(selection));
      m_boxSelection.clear();
    }
  }
  // A single arrow selected; add it to the box select and then transfer that
  else if (m_selection.is<gui::DirectionArrowSelection>()) {
    auto& selection = m_selection.as<gui::DirectionArrowSelection>();
    ASSERT(isInsideGrid(selection.gridCell) && m_factory &&
           selection.workerID < m_factory->numWorkers());
    m_boxSelection.addArrowSelection(selection);
    m_selection.set(std::move(m_boxSelection));
    m_boxSelection = gui::MultiSelection{};
  }
  // There is an existing multiselect: merge the box select into it
  else {
    // GridCellSelection handled already elsewhere.
    ASSERT(m_selection.is<gui::MultiSelection>());
    auto& selection = m_selection.as<gui::MultiSelection>();
    selection.mergeSelection(m_boxSelection);
  }

  ASSERT(m_boxSelection.empty());
}
void UISimpleLevelGrid::cancelBoxSelection() {
  ASSERT(m_renderer);
  m_renderer->invalidateSelectionRenderData(m_boxSelection);
  m_renderer->endBoxSelection();
  m_boxSelection.clear();
}

template <typename T>
struct hasSysKeyFieldImpl
  : std::integral_constant<bool,
      std::is_same<T, CEGUI::MouseEventArgs>::value ||
      std::is_same<T, CEGUI::KeyEventArgs>::value>
{};
template <typename T>
using hasSysKeyField = hasSysKeyFieldImpl<std::decay_t<T>>;

template <typename T>
std::enable_if_t<hasSysKeyField<T>::value, bool>
isShiftPressed(const T& event) {
  return (CEGUI::SystemKey::Shift & event.sysKeys) != 0;
}
template <typename T>
std::enable_if_t<hasSysKeyField<T>::value, bool>
isCtrlPressed(const T& event) {
  return (CEGUI::SystemKey::Control & event.sysKeys) != 0;
}

void UISimpleLevelGrid::handleMouseHover(const CEGUI::MouseEventArgs& event) {
  ASSERT(m_renderer);

  const auto gridPos = getGridPositionFromMouse(event.position);

  glm::ivec2 gridCell;
  std::size_t workerID = std::size_t(-1ll);
  bool isDirection = false;
  std::tie(gridCell, workerID, isDirection) = m_renderer->lookupGridPosition(gridPos);
  
  bool needsRedraw = false;
  if (isDirection) {
    ASSERT((gridCell != glm::ivec2{-1, -1} && workerID != std::size_t(-1ll)));

    LOG_SEVERITY(logger::get(), Pedantic)
      << "Mouse over direction arrow for worker " << workerID << " in grid cell ("
      << gridCell.x << ", " << gridCell.y << ")";

    needsRedraw = setHoveredDirectionArrow({gridCell, workerID});
  } else {
    LOG_SEVERITY(logger::get(), Pedantic)
      << "Mouse over grid position (" << gridPos.x << ", " << gridPos.y
      << "); grid cell (" << gridCell.x << ", " << gridCell.y << ")";

    if (gridCell != glm::ivec2{-1, -1})
      needsRedraw = setHoveredGridCell(gridCell);
    else {
      LOG_SEVERITY(logger::get(), Pedantic)
        << "Mouse outside level grid";

      needsRedraw = clearHoverSelection();
    }
  }
  if (needsRedraw)
    invalidate();
}
void UISimpleLevelGrid::handleMouseClick(const CEGUI::MouseEventArgs& event) {
  ASSERT(m_renderer);
  const auto gridPos = getGridPositionFromMouse(event.position);

  glm::ivec2 gridCell;
  std::size_t workerID = std::size_t(-1ll);
  bool isDirection = false;
  std::tie(gridCell, workerID, isDirection) = m_renderer->lookupGridPosition(gridPos);

  bool needsRedraw = false;
  if (isDirection) {
    ASSERT((gridCell != glm::ivec2{-1, -1} && workerID != std::size_t(-1ll)));

    LOG_SEVERITY(logger::get(), Pedantic)
      << "Mouse clicked on direction arrow for worker " << workerID << " in grid cell ("
      << gridCell.x << ", " << gridCell.y << ")";

    if (isShiftPressed(event))
      needsRedraw = toggleSelection({gridCell, workerID});
    else
      needsRedraw = setSelectedDirectionArrow({gridCell, workerID});
  } else if (!isShiftPressed(event)) {
    LOG_SEVERITY(logger::get(), Pedantic)
      << "Mouse clicked in grid position (" << gridPos.x << ", " << gridPos.y << ")"
      << "; grid cell (" << gridCell.x << ", " << gridCell.y << ")";

    if (gridCell != glm::ivec2{-1, -1})
      needsRedraw = setSelectedGridCell(gridCell);
    else {
      LOG_SEVERITY(logger::get(), Pedantic)
        << "Mouse outside level grid";

      needsRedraw = clearSelection();
    }
  }

  if (needsRedraw)
    invalidate();
}
void UISimpleLevelGrid::handleMouseWheel(const CEGUI::MouseEventArgs& event) {
  float newZoom = m_zoomLevel + m_zoomLevel / 8.f * event.wheelChange;
  newZoom = glm::clamp(newZoom, minZoomLevel, maxZoomLevel);
  if (m_zoomLevel != newZoom) {
    const auto oldMouseGridPosition = getGridPositionFromMouse(event.position);

    m_zoomLevel = newZoom;
    m_gridCellSize.invalidate();

    if (m_zoomLevel > 1.f) {
      auto baseViewSize = renderArea().getSize();
      const auto gridRenderSize = computeGridRenderSize();

      if (!m_verticalScrollbar->isVisible() && gridRenderSize.y > baseViewSize.d_height) {
        m_verticalScrollbar->show();
        m_viewSize.invalidate();
      } else if (m_verticalScrollbar->isVisible() && gridRenderSize.y <= baseViewSize.d_height) {
        m_verticalScrollbar->setUnitIntervalScrollPosition(0.5f);
        m_verticalScrollbar->hide();
        m_viewSize.invalidate();
      }

      if (m_verticalScrollbar->isVisible())
        baseViewSize.d_width -= m_verticalScrollbar->getPixelSize().d_width;

      if (!m_horizontalScrollbar->isVisible() && gridRenderSize.x > baseViewSize.d_width) {
        m_horizontalScrollbar->show();

        m_viewSize.invalidate();
      } else if (m_horizontalScrollbar->isVisible() && gridRenderSize.x <= baseViewSize.d_width) {
        m_horizontalScrollbar->setUnitIntervalScrollPosition(0.5f);
        m_horizontalScrollbar->hide();

        m_viewSize.invalidate();
      }
    } else {
      m_verticalScrollbar->setUnitIntervalScrollPosition(0.5f);
      m_horizontalScrollbar->setUnitIntervalScrollPosition(0.5f);

      if (m_moveDragging.get()) {
        LOG_SEVERITY(logger::get(), Debug)
          << "Zoomed out fully: move-dragging level grid stopped at ("
          << event.position.d_x << ", " << event.position.d_y << ')';

        endMoveDrag();
      }
      m_verticalScrollbar->hide();
      m_horizontalScrollbar->hide();
      m_viewSize.invalidate();
    }
    invalidate();

    // Skip alignment to mouse if neither scrollbar is visible
    if (!(m_verticalScrollbar->isVisible() || m_horizontalScrollbar->isVisible()))
      return;

    m_gridCellSize.invalidate();
    m_renderOffset.invalidate();
    const auto newMouseGridPosition = getGridPositionFromMouse(event.position);

    auto mouseDelta = newMouseGridPosition - oldMouseGridPosition;
    if (!m_verticalScrollbar->isVisible())
      mouseDelta.y = 0.f;
    if (!m_horizontalScrollbar->isVisible())
      mouseDelta.x = 0.f;

    const auto absOffset = convertRelativeOffset(m_relOffset);
    m_relOffset = convertAbsoluteOffset(absOffset + (mouseDelta * m_gridCellSize.get()));

    m_horizontalScrollbar->setUnitIntervalScrollPosition(m_relOffset.x);
    m_verticalScrollbar->setUnitIntervalScrollPosition(m_relOffset.y);
    m_renderOffset.invalidate();
  }
}
void UISimpleLevelGrid::beginMoveDrag(const CEGUI::MouseEventArgs& event) {
  ASSERT(!m_moveDragging.get());

  m_moveDragging.set();
  m_leftMouseButtonPressed.clear();
  m_dragStartPos = glm::vec2{event.position.d_x, event.position.d_y};

  captureInput();
  GUIManager::get()->lockMousePosition();

  LOG_SEVERITY(logger::get(), Debug)
    << "Move-dragging level grid started at ("
    << m_dragStartPos.x << ", " << m_dragStartPos.y << ')';
}
void UISimpleLevelGrid::handleMouseMoveDrag(const CEGUI::MouseEventArgs& event) {
  ASSERT(m_moveDragging.get());

  const glm::vec2 mousePosition{event.position.d_x, event.position.d_y};
  auto delta = mousePosition - m_dragStartPos;

  if (!m_verticalScrollbar->isVisible())
    delta.y = 0.f;
  if (!m_horizontalScrollbar->isVisible())
    delta.x = 0.f;

  const auto absOffset = convertRelativeOffset(m_relOffset);
  m_relOffset = convertAbsoluteOffset(absOffset + delta);

  LOG_SEVERITY(logger::get(), Pedantic)
    << "Move-dragging level grid: mouse at (" << mousePosition.x << ", " << mousePosition.y
    << "), delta is (" << delta.x << ", " << delta.y << ')';

  m_verticalScrollbar->setUnitIntervalScrollPosition(m_relOffset.y);
  m_horizontalScrollbar->setUnitIntervalScrollPosition(m_relOffset.x);
  m_renderOffset.invalidate();
  invalidate();
}
void UISimpleLevelGrid::endMoveDrag() {
  ASSERT(m_moveDragging.get());

  m_moveDragging.clear();
  releaseInput();
  GUIManager::get()->unlockMousePosition();
}
void UISimpleLevelGrid::beginSelectDrag(const CEGUI::MouseEventArgs& event) {
  ASSERT(!m_selectDragging.get());

  LOG_SEVERITY(logger::get(), Pedantic)
    << "Select drag started with start = ("
    << m_dragStartPos.x << ", " << m_dragStartPos.y
    << "), mouse at (" << event.position.d_x << ", " << event.position.d_y << ')';

  // Convert start to grid coordinates to lock the start position relative to the grid
  m_dragStartPos = getGridPositionFromMouse({m_dragStartPos.x, m_dragStartPos.y});

  captureInput();

  m_selectDragging.set();
  beginBoxSelection(isShiftPressed(event));
}
void UISimpleLevelGrid::handleMouseSelectDrag(const CEGUI::MouseEventArgs& event) {
  ASSERT(m_selectDragging.get());

  const auto& boxStart = m_dragStartPos;
  const auto boxEnd = getGridPositionFromMouse(event.position);

  LOG_SEVERITY(logger::get(), Pedantic)
    << "Select-dragging level grid: mouse at ("
    << event.position.d_x << ", " << event.position.d_y
    << "), selection box: {(" << boxStart.x << ", " << boxStart.y
    << "), (" << boxEnd.x << ", " << boxEnd.y << ")}";

  updateBoxSelection(boxStart, boxEnd);

  invalidate();
}
void UISimpleLevelGrid::endSelectDrag(bool cancel) {
  ASSERT(m_selectDragging.get());

  if (cancel)
    cancelBoxSelection();
  else
    endBoxSelection();
  m_selectDragging.clear();
  m_leftMouseButtonPressed.clear();
  releaseInput();
}

void UISimpleLevelGrid::deleteSelection() {
  if (m_selectDragging.get())
    return;

  if (m_selection.is<GridCellSelection>()) {
    auto& selectedCell = m_selection.as<GridCellSelection>();
    ASSERT(isInsideGrid(selectedCell.gridCell));

    pushUndo();
    m_factory->grid()(selectedCell.gridCell).setDirection(
      m_selectedWorker, game::Direction::None);
    m_renderer->invalidatePath(m_selectedWorker);
    invalidate();
  } else if (m_selection.is<DirectionArrowSelection>()) {
    auto& selectedArrow = m_selection.as<DirectionArrowSelection>();
    ASSERT(isInsideGrid(selectedArrow.gridCell) &&
      selectedArrow.workerID < m_factory->numWorkers());

    pushUndo();
    m_factory->grid()(selectedArrow.gridCell).setDirection(
      selectedArrow.workerID, game::Direction::None);
    m_renderer->invalidatePath(selectedArrow.workerID);
    clearSelection();
    invalidate();
  } else if (m_selection.is<MultiSelection>()) {
    auto& multiSelection = m_selection.as<MultiSelection>();
    ASSERT(multiSelection.size() >= 2u);
    const std::size_t numWorkers = m_factory->numWorkers();

    pushUndo();
    auto arrowBegin = multiSelection.beginArrows();
    auto arrowEnd = multiSelection.endArrows();
    for (auto iter = arrowBegin; iter != arrowEnd; ++iter) {
      auto& arrow = *iter;
      ASSERT(isInsideGrid(arrow.gridCell) && arrow.workerID < numWorkers);

      m_factory->grid()(arrow.gridCell).setDirection(
        arrow.workerID, game::Direction::None);
    }
    for (std::size_t worker = 0; worker < numWorkers; ++worker) {
      if (multiSelection.isIncluded(worker))
        m_renderer->invalidatePath(worker);
    }
    clearSelection();
    invalidate();
  }
}

void UISimpleLevelGrid::pushUndo() {
  ASSERT(m_factory);
  m_undoRedoManager.push(*m_factory);
  LOG_SEVERITY(logger::get(), Info)
    << "Pushed undo state: " << m_undoRedoManager.undoSize() << " states in undo stack";

  if (m_undoButton->isDisabled())
    m_undoButton->enable();
  m_redoButton->disable();
}
void UISimpleLevelGrid::undo() {
  ASSERT(m_factory);
  if (m_undoRedoManager.undo(*m_factory)) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Undo: " << m_undoRedoManager.undoSize() << " states left in undo stack, "
      << m_undoRedoManager.redoSize() << " states in redo stack.";
    m_renderer->invalidateGrid();
    invalidate();

    if (m_undoRedoManager.undoEmpty())
      m_undoButton->disable();
    if (m_redoButton->isDisabled())
      m_redoButton->enable();
  }
}
void UISimpleLevelGrid::redo() {
  ASSERT(m_factory);
  if (m_undoRedoManager.redo(*m_factory)) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Redo: " << m_undoRedoManager.undoSize() << " states in undo stack, "
      << m_undoRedoManager.redoSize() << " states left in redo stack.";
    m_renderer->invalidateGrid();
    invalidate();

    if (m_undoButton->isDisabled())
      m_undoButton->enable();
    if (m_undoRedoManager.redoEmpty())
      m_redoButton->disable();
  }
}

void UISimpleLevelGrid::onSized(CEGUI::ElementEventArgs& e) {
  BaseType::onSized(e);

  m_viewSize.invalidate();
  m_gridCellSize.invalidate();

  layoutUndoRedoButtons();
}

void UISimpleLevelGrid::onMouseMove(CEGUI::MouseEventArgs& event) {
  BaseType::onMouseMove(event);

  if (m_moveDragging.get())
    handleMouseMoveDrag(event);
  else if (m_selectDragging.get())
    handleMouseSelectDrag(event);
  else if (m_leftMouseButtonPressed.get()) {
    const glm::vec2 mousePos{event.position.d_x, event.position.d_y};
    if (glm::distance(m_dragStartPos, mousePos) > dragSelectThreshold) {
      beginSelectDrag(event);
      handleMouseSelectDrag(event);
    }
  }
  handleMouseHover(event);
}
void UISimpleLevelGrid::onMouseButtonDown(CEGUI::MouseEventArgs& event) {
  BaseType::onMouseButtonDown(event);

  if (event.button == CEGUI::MouseButton::LeftButton && !m_moveDragging.get()) {
    ASSERT(!m_leftMouseButtonPressed.get());
    m_leftMouseButtonPressed.set();
    m_dragStartPos = glm::vec2{event.position.d_x, event.position.d_y};

    LOG_SEVERITY(logger::get(), Pedantic)
      << "Left mouse button pressed  at ("
      << m_dragStartPos.x << ", " << m_dragStartPos.y << ')';
  } else if (m_zoomLevel > 1.f &&
             event.button == CEGUI::MouseButton::RightButton &&
             !m_selectDragging.get())
  {
    beginMoveDrag(event);
  }
}
void UISimpleLevelGrid::onMouseButtonUp(CEGUI::MouseEventArgs& event) {
  BaseType::onMouseButtonUp(event);

  if (event.button == CEGUI::MouseButton::LeftButton) {
    if (m_selectDragging.get()) {
      handleMouseSelectDrag(event);
      endSelectDrag();

      LOG_SEVERITY(logger::get(), Pedantic)
        << "Select-dragging level grid stopped at ("
        << event.position.d_x << ", " << event.position.d_y << ')';

    // Check left mouse button is still pressed to rule out cancelled box selection
    } else if (m_leftMouseButtonPressed.get())
      handleMouseClick(event);

    m_leftMouseButtonPressed.clear();
  } else if (m_moveDragging.get() && event.button == CEGUI::MouseButton::RightButton) {
    handleMouseMoveDrag(event);
    endMoveDrag();

    LOG_SEVERITY(logger::get(), Debug)
      << "Move-dragging level grid stopped at ("
      << event.position.d_x << ", " << event.position.d_y << ')';
  }
}
void UISimpleLevelGrid::onMouseWheel(CEGUI::MouseEventArgs& event) {
  ASSERT(m_renderer);
  BaseType::onMouseWheel(event);

  handleMouseWheel(event);
  handleMouseHover(event);
}
void UISimpleLevelGrid::onMouseEntersArea(CEGUI::MouseEventArgs& event) {
  ASSERT(m_renderer);
  BaseType::onMouseEntersArea(event);

  LOG_SEVERITY(logger::get(), Pedantic)
    << "Mouse entered level grid";

  handleMouseHover(event);
}
void UISimpleLevelGrid::onMouseLeavesArea(CEGUI::MouseEventArgs& event) {
  ASSERT(m_renderer);
  BaseType::onMouseLeavesArea(event);

  LOG_SEVERITY(logger::get(), Debug)
    << "Mouse left level grid";
  
  if (m_moveDragging.get()) {
    handleMouseMoveDrag(event);
    endMoveDrag();

    LOG_SEVERITY(logger::get(), Warning)
      << "Mouse left the level grid, move dragging stopped at ("
      << event.position.d_x << ", " << event.position.d_y << ')';
  }
  if (clearHoverSelection())
    invalidate();
}

void UISimpleLevelGrid::onCaptureLost(CEGUI::WindowEventArgs& event) {
  BaseType::onCaptureLost(event);

  if (m_selectDragging.get())
    endSelectDrag(true);
  if (m_moveDragging.get()) {
    LOG_SEVERITY(logger::get(), Warning)
      << "Lost mouse capture while move dragging level grid";
    endMoveDrag();
  }
}

void UISimpleLevelGrid::onDragDropItemDropped(CEGUI::DragDropEventArgs& event) {
  ASSERT(m_factory && m_renderer);
  BaseType::onDragDropItemDropped(event);

  if (auto arrowDraggable = dynamic_cast<DirectionArrowDraggable*>(event.dragDropItem)) {
    // Don't need to check position == (-inf, -inf) because getGridPositionFromMouse
    // will return (-inf, -inf) in that case and then isInsideGrid will be false.
    auto position = arrowDraggable->getAbsoluteDragPosition();
    auto gridPos = getGridPositionFromMouse(position);

    if (isInsideGrid(gridPos)) {
      const glm::ivec2 gridCell{glm::floor(gridPos)};
      ASSERT(isInsideGrid(gridCell));

      const auto direction = arrowDraggable->getDirection();
      const auto workerSelection = arrowDraggable->getWorkerSelection();
      ASSERT(direction != game::Direction::None);
      ASSERT(workerSelection < m_factory->numWorkers());

      pushUndo();
      m_factory->grid()(gridCell).setDirection(workerSelection, direction);
      setSelectedGridCell(gridCell);

      m_renderer->invalidatePath(workerSelection);
      invalidate();
    }
  }
}

game::Direction getDirectionFromKey(CEGUI::Key::Scan key) {
  using CEGUI::Key;

  switch (key) {
  case Key::Scan::W:
  case Key::Scan::ArrowUp:
    return game::Direction::Up;
  case Key::Scan::A:
  case Key::Scan::ArrowLeft:
    return game::Direction::Left;
  case Key::Scan::S:
  case Key::Scan::ArrowDown:
    return game::Direction::Down;
  case Key::Scan::D:
  case Key::Scan::ArrowRight:
    return game::Direction::Right;
  default:
    ASSERT(false);
    return game::Direction::None;
  }
}

void UISimpleLevelGrid::handleKeyDown(const CEGUI::KeyEventArgs& event) {
  using CEGUI::Key;

  switch (event.scancode) {
  case Key::Scan::W:
  case Key::Scan::A:
  case Key::Scan::S:
  case Key::Scan::D:
  case Key::Scan::ArrowUp:
  case Key::Scan::ArrowLeft:
  case Key::Scan::ArrowDown:
  case Key::Scan::ArrowRight: {
    bool pathChange = false;
    glm::ivec2 gridCell{-1, -1};
    std::size_t worker = m_selectedWorker;
    game::Direction direction = game::Direction::None;

    if (m_selection.is<GridCellSelection>()) {
      auto& selectedCell = m_selection.as<GridCellSelection>();
      ASSERT(isInsideGrid(selectedCell.gridCell));
      pathChange = true;
      gridCell = selectedCell.gridCell;
    } else if (m_selection.is<DirectionArrowSelection>()) {
      auto& selectedArrow = m_selection.as<DirectionArrowSelection>();
      ASSERT(isInsideGrid(selectedArrow.gridCell) &&
        selectedArrow.workerID < m_factory->numWorkers());
      pathChange = true;
      gridCell = selectedArrow.gridCell;
      worker = selectedArrow.workerID;
    }

    if (pathChange) {
      direction = getDirectionFromKey(event.scancode);

      if (m_factory->grid()(gridCell).direction(worker) != direction) {
        pushUndo();
        m_factory->grid()(gridCell).setDirection(worker, direction);
        m_renderer->invalidatePath(worker);
        invalidate();
      }
    }
  } break;
  case Key::Scan::Delete:
    deleteSelection();
    break;
  case Key::Scan::Z:
    if (isCtrlPressed(event) && !isShiftPressed(event))
      undo();
    break;
  case Key::Scan::Y:
    if (isCtrlPressed(event) && !isShiftPressed(event))
      redo();
    break;
  default:;
  }
}

void UISimpleLevelGrid::setFactory(game::SimpleFactory& factory) {
  ASSERT_MSG(!m_factory && !m_renderer,
             "UISimpleLevelGrid::setFactory called after factory already initialized");
  ASSERT_MSG(m_verticalScrollbar && m_horizontalScrollbar,
             "UISimpleLevelGrid::setFactory called before initialiseComponents");

  m_factory = &factory;
  m_renderer = make_const_prop_ptr<graphics::LevelGridRenderer>(factory);

  ASSERT(m_factory && m_renderer);
  m_gridCellSize.invalidate();
  m_renderOffset.invalidate();

  auto gridSize = m_factory->grid().gridSize();
  m_verticalScrollbar->setDocumentSize(float(gridSize.y));
  m_horizontalScrollbar->setDocumentSize(float(gridSize.x));

  layoutUndoRedoButtons();
}

void UISimpleLevelGrid::setSelectedWorker(std::size_t worker) {
  if (m_selectedWorker != worker) {
    m_selectedWorker = worker;
  }
}

bool UISimpleLevelGrid::setSelectedCellDirection(
  std::size_t worker, game::Direction direction)
{
  ASSERT(m_factory && m_renderer);
  ASSERT(worker < m_factory->numWorkers());

  if (m_selection.is<GridCellSelection>()) {
    auto& selectedCell = m_selection.as<GridCellSelection>();
    ASSERT(isInsideGrid(selectedCell.gridCell));
    pushUndo();
    m_factory->grid()(selectedCell.gridCell).setDirection(worker, direction);
    m_renderer->invalidatePath(worker);
    invalidate();
    return true;
  } else
    return false;
}

void UISimpleLevelGrid::clearSelections() {
  bool needsRedraw = false;

  if (m_selectDragging.get()) {
    endSelectDrag(true);
    needsRedraw = true;
  } else
    needsRedraw = clearSelection();

  if (needsRedraw)
    invalidate();
}

glm::vec2 UISimpleLevelGrid::computeViewSize() const {
  ASSERT(m_renderer);

  auto viewSize = renderArea().getSize();
  if (m_verticalScrollbar->isVisible())
    viewSize.d_width -= m_verticalScrollbar->getPixelSize().d_width;
  if (m_horizontalScrollbar->isVisible())
    viewSize.d_height -= m_horizontalScrollbar->getPixelSize().d_height;

  return glm::vec2{viewSize.d_width, viewSize.d_height};
}
glm::vec2 UISimpleLevelGrid::computeGridRenderSize() const {
  ASSERT(m_renderer && m_factory);

  return m_gridCellSize.get() * glm::vec2{m_factory->grid().gridSize()};
}
float UISimpleLevelGrid::computeGridCellSize() const {
  ASSERT(m_renderer && m_factory);

  const auto gridSize = m_factory->grid().gridSize();
  const auto baseViewSize_ = renderArea().getSize();
  const glm::vec2 baseViewSize{baseViewSize_.d_width, baseViewSize_.d_height};
  const auto gridCellBounds = (baseViewSize - glm::vec2{20.f}) / glm::vec2{gridSize};
  float baseGridCellSize = std::min(gridCellBounds.x, gridCellBounds.y);

  return baseGridCellSize * m_zoomLevel;
}
glm::vec2 UISimpleLevelGrid::convertRelativeOffset(const glm::vec2& relOffset) const {
  ASSERT(m_renderer);
  ASSERT(glm::all(glm::lessThanEqual({0.f, 0.f}, relOffset)) &&
         glm::all(glm::lessThanEqual(relOffset, {1.f, 1.f})));

  const auto renderSize = computeGridRenderSize();

  return (m_viewSize.get() - renderSize) * relOffset;
}
glm::vec2 UISimpleLevelGrid::convertAbsoluteOffset(const glm::vec2& absOffset) const {
  ASSERT(m_renderer);

  const auto viewSize = m_viewSize.get();
  const auto renderSize = computeGridRenderSize();

  ASSERT(glm::all(glm::greaterThan(viewSize, {0.f, 0.f})));
  ASSERT(glm::all(glm::greaterThan(renderSize, {0.f, 0.f})));

  auto relOffset = absOffset / (viewSize - renderSize);

  return glm::clamp(relOffset, {0.f, 0.f}, {1.f, 1.f});
}

void UISimpleLevelGrid::renderWindow() {
  ASSERT(m_renderer);

  m_viewSize.update();
  m_gridCellSize.update();
  m_renderOffset.update();

  m_renderer->render(m_hoverSelection, m_selection, m_boxSelection);
}

namespace {
STATIC_LOGGER_INIT(logger) {
  logging::Logger lg;
  logging::setModuleName(lg, "UISimpleLevelGrid");
  return lg;
}
}

} // namespace gui
