#include "WorkerSelectionControl.h"

#include <sstream>

#include <CEGUI/Colour.h>
#include <CEGUI/widgets/RadioButton.h>

#include "../Graphics/LevelGridRenderConstants.h"

#include "../Assert.h"
#include "../Exception.h"

namespace gui {

const CEGUI::String WorkerSelectionControl::WidgetTypeName("PAGUI/WorkerSelectionControl");
const CEGUI::String WorkerSelectionControl::EventNamespace("WorkerSelectionControl");

const CEGUI::String WorkerSelectionControl::EventWorkerSelectionChanged("WorkerSelectionChanged");

const CEGUI::String WorkerSelectionControl::TopLeftButtonName("__auto_topleft_button__");
const CEGUI::String WorkerSelectionControl::TopRightButtonName("__auto_topright_button__");
const CEGUI::String WorkerSelectionControl::BottomLeftButtonName("__auto_bottomleft_button__");
const CEGUI::String WorkerSelectionControl::BottomRightButtonName("__auto_bottomright_button__");

const CEGUI::String WorkerSelectionControl::ButtonColourPropertyName("ButtonColour");
const CEGUI::String WorkerSelectionControl::ButtonTextColourPropertyName("TextColour");

WorkerSelectionControl::WorkerSelectionControl(
  const CEGUI::String& type, const CEGUI::String& name)
  : BaseType(type, name)
  , m_worker(0u)
  , m_topLeftButton(nullptr)
  , m_topRightButton(nullptr)
  , m_bottomLeftButton(nullptr)
  , m_bottomRightButton(nullptr)
{}

void WorkerSelectionControl::initialiseComponents() {
  BaseType::initialiseComponents();

  using graphics::constants::pathBackColours;

  m_topLeftButton = &dynamic_cast<CEGUI::RadioButton&>(*getChild(TopLeftButtonName));
  m_topRightButton = &dynamic_cast<CEGUI::RadioButton&>(*getChild(TopRightButtonName));
  m_bottomLeftButton = &dynamic_cast<CEGUI::RadioButton&>(*getChild(BottomLeftButtonName));
  m_bottomRightButton = &dynamic_cast<CEGUI::RadioButton&>(*getChild(BottomRightButtonName));

  static const auto& EventSelectStateChanged = CEGUI::RadioButton::EventSelectStateChanged;

  static_assert(pathBackColours.size() == 4u,
                "Need to update WorkerSelectionControl colours");

  const CEGUI::Colour topLeftButtonColour{
    pathBackColours[0].r,
    pathBackColours[0].g,
    pathBackColours[0].b
  };
  const CEGUI::Colour topRightButtonColour{
    pathBackColours[1].r,
    pathBackColours[1].g,
    pathBackColours[1].b
  };
  const CEGUI::Colour bottomLeftButtonColour{
    pathBackColours[2].r,
    pathBackColours[2].g,
    pathBackColours[2].b
  };
  const CEGUI::Colour bottomRightButtonColour{
    pathBackColours[3].r,
    pathBackColours[3].g,
    pathBackColours[3].b
  };

  m_topLeftButton->setProperty<CEGUI::Colour>(ButtonColourPropertyName, topLeftButtonColour);
  m_topRightButton->setProperty<CEGUI::Colour>(ButtonColourPropertyName, topRightButtonColour);
  m_bottomLeftButton->setProperty<CEGUI::Colour>(ButtonColourPropertyName, bottomLeftButtonColour);
  m_bottomRightButton->setProperty<CEGUI::Colour>(ButtonColourPropertyName, bottomRightButtonColour);

  m_topLeftButton->subscribeEvent(
    EventSelectStateChanged, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        if (m_topLeftButton->isSelected())
          setWorkerSelection(0u);
        return true;
      }
    ));
  m_topRightButton->subscribeEvent(
    EventSelectStateChanged, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        if (m_topRightButton->isSelected())
          setWorkerSelection(1u);
        return true;
      }
    ));
  m_bottomLeftButton->subscribeEvent(
    EventSelectStateChanged, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        if (m_bottomLeftButton->isSelected())
          setWorkerSelection(2u);
        return true;
      }
    ));
  m_bottomRightButton->subscribeEvent(
    EventSelectStateChanged, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        if (m_bottomRightButton->isSelected())
          setWorkerSelection(3u);
        return true;
      }
    ));

  if (m_topLeftButton->isSelected())
    setWorkerSelection(0u);
  else if (m_topRightButton->isSelected())
    setWorkerSelection(1u);
  else if (m_bottomLeftButton->isSelected())
    setWorkerSelection(2u);
  else {
    ASSERT(m_bottomRightButton->isSelected());
    setWorkerSelection(3u);
  }
}

void WorkerSelectionControl::onWorkerSelectionChanged(CEGUI::WindowEventArgs& e) {
  switch (m_worker) {
  case 0u:
    m_topLeftButton->setSelected(true);
    break;
  case 1u:
    m_topRightButton->setSelected(true);
    break;
  case 2u:
    m_bottomLeftButton->setSelected(true);
    break;
  case 3u:
    m_bottomRightButton->setSelected(true);
    break;
  default:
    ASSERT(false);;
  }

  fireEvent(EventWorkerSelectionChanged, e, EventNamespace);
}

void WorkerSelectionControl::setWorkerSelection(std::size_t worker) {
  if (m_worker != worker) {
    m_worker = worker;

    CEGUI::WindowEventArgs args{this};
    onWorkerSelectionChanged(args);
  }
}
std::size_t WorkerSelectionControl::getWorkerSelection() const {
  return m_worker;
}

void WorkerSelectionControl::setNumWorkers(std::size_t numWorkers) {
  ASSERT(m_topLeftButton && m_topRightButton && m_bottomLeftButton && m_bottomRightButton);

  switch (numWorkers) {
  case 4u:
    m_topLeftButton->enable();
    m_topRightButton->enable();
    m_bottomLeftButton->enable();
    m_bottomRightButton->enable();
    break;
  case 3u:
    m_topLeftButton->enable();
    m_topRightButton->enable();
    m_bottomLeftButton->enable();
    m_bottomRightButton->disable();
    break;
  case 2u:
    m_topLeftButton->enable();
    m_topRightButton->enable();
    m_bottomLeftButton->disable();
    m_bottomRightButton->disable();
    break;
  case 1u:
    m_topLeftButton->enable();
    m_topRightButton->disable();
    m_bottomLeftButton->disable();
    m_bottomRightButton->disable();
    break;
  case 0u:
    m_topLeftButton->disable();
    m_topRightButton->disable();
    m_bottomLeftButton->disable();
    m_bottomRightButton->disable();
    break;
  default: {
    std::ostringstream ss;
    ss << "Tried to configure WorkerSelectionControl for invalid number of workers;"
       << " passed value was " << numWorkers << ", accepted values are 0-4.";
    throw MAKE_EXCEPTION(ss.str());
  }}
}

} // namespace gui
