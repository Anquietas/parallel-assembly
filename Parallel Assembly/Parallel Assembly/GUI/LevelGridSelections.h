#pragma once
#ifndef GUI_LEVEL_GRID_SELECTIONS_H
#define GUI_LEVEL_GRID_SELECTIONS_H

#include <array>
#include <type_traits>

#include <boost/container/flat_set.hpp>
#include <boost/variant/variant.hpp>

#include "../Game/Worker.h"

#include "../GLMIncludes.h"

namespace gui {

inline bool lessThan(const glm::ivec2& x, const glm::ivec2& y) noexcept {
  return x.x < y.x || (x.x == y.x && x.y < y.y);
}
inline bool lessThan(const glm::vec2& x, const glm::vec2& y) noexcept {
  return x.x < y.x || (x.x == y.x && x.y < y.y);
}

struct NullSelection final {};

inline bool operator==(const NullSelection&, const NullSelection&) noexcept {
  return true;
}
inline bool operator<(const NullSelection&, const NullSelection&) noexcept {
  return false;
}

struct GridCellSelection final {
  glm::ivec2 gridCell;
};

inline bool operator==(const GridCellSelection& x, const GridCellSelection& y) noexcept {
  return x.gridCell == y.gridCell;
}
inline bool operator<(const GridCellSelection& x, const GridCellSelection& y) noexcept {
  return lessThan(x.gridCell, y.gridCell);
}

struct DirectionArrowSelection final {
  glm::ivec2 gridCell;
  std::size_t workerID;
};

inline bool operator==(const DirectionArrowSelection& x, const DirectionArrowSelection& y) noexcept {
  return x.gridCell == y.gridCell && x.workerID == y.workerID;
}
inline bool operator<(const DirectionArrowSelection& x, const DirectionArrowSelection& y) noexcept {
  return lessThan(x.gridCell, y.gridCell) || (x.gridCell == y.gridCell && x.workerID < y.workerID);
}

class MultiSelection final {
  using DirectionArrowSelectionSet = boost::container::flat_set<DirectionArrowSelection>;

  DirectionArrowSelectionSet m_selectedArrows;
  std::array<bool, game::Worker::maxWorkers> m_arrowsWorkerIDsIncluded;

public:
  using DirectionArrowIter = DirectionArrowSelectionSet::const_iterator;

  MultiSelection();
  MultiSelection(const MultiSelection& o) = default;
  MultiSelection(MultiSelection&& o) = default;
  ~MultiSelection() = default;

  MultiSelection& operator=(const MultiSelection& o) = default;
  MultiSelection& operator=(MultiSelection&& o) = default;

  inline DirectionArrowIter beginArrows() const { return m_selectedArrows.begin(); }
  inline DirectionArrowIter endArrows() const { return m_selectedArrows.end(); }

  bool empty() const noexcept;

  void clear() noexcept;

  std::size_t size() const noexcept;

  // Adds the given arrow selection to the multiselection
  // Returns true if successful, false otherwise
  bool addArrowSelection(const DirectionArrowSelection& arrow);
  // Removes the given arrow selection from the multiselection
  // Returns true if successful, false otherwise
  bool removeArrowSelection(const DirectionArrowSelection& arrow);
  // Overload of removeArrowSelection for removing directly via iterator
  void removeArrowSelection(DirectionArrowIter iter);

  // Adds the contents of selection to this and leaves
  // selection in an empty but usable state.
  void mergeSelection(MultiSelection& selection);

  DirectionArrowIter findArrowSelection(const DirectionArrowSelection& arrow) const;
  bool isIncluded(std::size_t workerID) const noexcept;

  bool contains(const DirectionArrowSelection& arrow) const noexcept;
  bool contains(const MultiSelection& selection) const noexcept;
};

template <typename T>
struct isSelectionTypeImpl : std::false_type {};
template <typename T>
struct isHoverSelectionTypeImpl : std::false_type {};

#define REGISTER_SELECTION_TYPE(T) \
  template <> struct isSelectionTypeImpl<T> : std::true_type {}
#define REGISTER_HOVER_SELECTION_TYPE(T) \
  template <> struct isHoverSelectionTypeImpl<T> : std::true_type {}
#define REGISTER_SELECT_HOVER_TYPE(T) \
  REGISTER_SELECTION_TYPE(T); \
  REGISTER_HOVER_SELECTION_TYPE(T)

REGISTER_SELECT_HOVER_TYPE(NullSelection);
REGISTER_SELECT_HOVER_TYPE(GridCellSelection);
REGISTER_SELECT_HOVER_TYPE(DirectionArrowSelection);
REGISTER_SELECTION_TYPE(MultiSelection);

#undef REGISTER_SELECTION_TYPE
#undef REGISTER_HOVER_SELECTION_TYPE
#undef REGISTER_SELECT_HOVER_TYPE

template <typename T>
struct isSelectionType : isSelectionTypeImpl<std::decay_t<T>> {};
template <typename T>
struct isHoverSelectionType : isHoverSelectionTypeImpl<std::decay_t<T>> {};

class LevelGridSelection final {
  using SelectionVariant = boost::variant<
    NullSelection,
    GridCellSelection,
    DirectionArrowSelection,
    MultiSelection
  >;

  SelectionVariant m_selection;

public:
  LevelGridSelection() = default;
  LevelGridSelection(const LevelGridSelection& o) = default;
  LevelGridSelection(LevelGridSelection&& o) = default;
  ~LevelGridSelection() = default;

  LevelGridSelection& operator=(const LevelGridSelection& o) = default;
  LevelGridSelection& operator=(LevelGridSelection&& o) = default;

  void clear();

  template <typename T>
  std::enable_if_t<isSelectionType<T>::value, bool> is() const noexcept;

  template <typename T>
  std::enable_if_t<isSelectionType<T>::value,
    std::add_lvalue_reference_t<T>
  > as();
  template <typename T>
  std::enable_if_t<isSelectionType<T>::value,
    std::add_lvalue_reference_t<std::add_const_t<T>>
  > as() const;

  template <typename T>
  std::enable_if_t<
    isSelectionType<T>::value && !std::is_same<T, NullSelection>::value, bool
  > contains(const T& selection) const noexcept;

  template <typename T>
  std::enable_if_t<isSelectionType<T>::value> set(T&& selection) {
    m_selection = std::forward<T>(selection);
  }
};

class LevelGridHoverSelection final {
  using HoverVariant = boost::variant<
    NullSelection,
    GridCellSelection,
    DirectionArrowSelection
  >;

  HoverVariant m_hover;

public:
  LevelGridHoverSelection() = default;
  LevelGridHoverSelection(const LevelGridHoverSelection& o) = default;
  LevelGridHoverSelection(LevelGridHoverSelection&& o) = default;
  ~LevelGridHoverSelection() = default;

  LevelGridHoverSelection& operator=(const LevelGridHoverSelection& o) = default;
  LevelGridHoverSelection& operator=(LevelGridHoverSelection&& o) = default;

  void clear();

  template <typename T>
  std::enable_if_t<isHoverSelectionType<T>::value, bool> is() const noexcept;

  template <typename T>
  std::enable_if_t<isHoverSelectionType<T>::value,
    std::add_lvalue_reference_t<T>
  > as();
  template <typename T>
  std::enable_if_t<isHoverSelectionType<T>::value,
    std::add_lvalue_reference_t<std::add_const_t<T>>
  > as() const;

  template <typename T>
  std::enable_if_t<
    isHoverSelectionType<T>::value && !std::is_same<T, NullSelection>::value, bool
  > contains(const T& selection) const noexcept;

  template <typename T>
  std::enable_if_t<isHoverSelectionType<T>::value> set(T&& hoverSelection) {
    m_hover = std::forward<T>(hoverSelection);
  }
};

#define EXTERN_SELECTION_IS(T) \
  extern template std::enable_if_t<isSelectionType<T>::value, bool> \
  LevelGridSelection::is<T>() const noexcept
#define EXTERN_HOVER_SELECTION_IS(T) \
  extern template std::enable_if_t<isHoverSelectionType<T>::value, bool> \
  LevelGridHoverSelection::is<T>() const noexcept
#define EXTERN_SELECT_HOVER_IS(T) \
  EXTERN_SELECTION_IS(T); \
  EXTERN_HOVER_SELECTION_IS(T)

#define EXTERN_SELECTION_AS(T) \
  extern template std::enable_if_t<isSelectionType<T>::value, \
    std::add_lvalue_reference_t<T> \
  > LevelGridSelection::as<T>(); \
  extern template std::enable_if_t<isSelectionType<T>::value, \
    std::add_lvalue_reference_t<std::add_const_t<T>> \
  > LevelGridSelection::as<T>() const
#define EXTERN_HOVER_SELECTION_AS(T) \
  extern template std::enable_if_t<isHoverSelectionType<T>::value, \
    std::add_lvalue_reference_t<T> \
  > LevelGridHoverSelection::as<T>(); \
  extern template std::enable_if_t<isHoverSelectionType<T>::value, \
    std::add_lvalue_reference_t<std::add_const_t<T>> \
  > LevelGridHoverSelection::as<T>() const
#define EXTERN_SELECT_HOVER_AS(T) \
  EXTERN_SELECTION_AS(T); \
  EXTERN_HOVER_SELECTION_AS(T)

#define EXTERN_SELECTION_CONTAINS(T) \
  extern template std::enable_if_t< \
    isSelectionType<T>::value && !std::is_same<T, NullSelection>::value, bool \
  > LevelGridSelection::contains<T>(const T& selection) const noexcept
#define EXTERN_HOVER_SELECTION_CONTAINS(T) \
  extern template std::enable_if_t< \
    isHoverSelectionType<T>::value && !std::is_same<T, NullSelection>::value, bool \
  > LevelGridHoverSelection::contains<T>(const T& selection) const noexcept
#define EXTERN_SELECT_HOVER_CONTAINS(T) \
  EXTERN_SELECTION_CONTAINS(T); \
  EXTERN_HOVER_SELECTION_CONTAINS(T))

#define EXTERN_SELECTION_ALL(T) \
  EXTERN_SELECTION_IS(T); \
  EXTERN_SELECTION_AS(T); \
  EXTERN_SELECTION_CONTAINS(T)
#define EXTERN_HOVER_SELECTION_ALL(T) \
  EXTERN_HOVER_SELECTION_IS(T); \
  EXTERN_HOVER_SELECTION_AS(T); \
  EXTERN_HOVER_SELECTION_CONTAINS(T)
#define EXTERN_SELECT_HOVER_ALL(T) \
  EXTERN_SELECTION_ALL(T); \
  EXTERN_HOVER_SELECTION_ALL(T)

EXTERN_SELECT_HOVER_IS(NullSelection);
EXTERN_SELECT_HOVER_AS(NullSelection);
EXTERN_SELECT_HOVER_ALL(GridCellSelection);
EXTERN_SELECT_HOVER_ALL(DirectionArrowSelection);
EXTERN_SELECTION_ALL(MultiSelection);

#undef EXTERN_SELECTION_IS
#undef EXTERN_HOVER_SELECTION_IS
#undef EXTERN_SELECT_HOVER_IS

#undef EXTERN_SELECTION_AS
#undef EXTERN_HOVER_SELECTION_AS
#undef EXTERN_SELECT_HOVER_AS

#undef EXTERN_SELECTION_CONTAINS
#undef EXTERN_HOVER_SELECTION_CONTAINS
#undef EXTERN_SELECT_HOVER_CONTAINS

#undef EXTERN_SELECTION_ALL
#undef EXTERN_HOVER_SELECTION_ALL
#undef EXTERN_SELECT_HOVER_ALL

} // namespace gui

#endif // GUI_LEVEL_GRID_SELECTIONS_H
