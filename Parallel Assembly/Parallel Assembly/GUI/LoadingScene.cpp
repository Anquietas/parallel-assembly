#include "LoadingScene.h"

#include <CEGUI/widgets/ProgressBar.h>
#include <CEGUI/widgets/DefaultWindow.h>

#include "Scene.inl"
#include "SceneNames.h"

namespace gui {

LoadingScene::LoadingScene()
  : Scene(loadingSceneName, "loadScreen.layout")
  , m_logger()
  , m_loadingBar(nullptr)
  , m_loadingLabel(nullptr)
{
  logging::setModuleName(m_logger, "LoadingScene");
}

void LoadingScene::initializeComponents() {
  LOG_SEVERITY(m_logger, Debug) << "Initializing components";

  m_loadingBar = getWidget<CEGUI::ProgressBar>("LoadingBar");
  m_loadingLabel = getWidget<CEGUI::DefaultWindow>("ItemBeingLoadedLabel");
}

} // namespace gui
