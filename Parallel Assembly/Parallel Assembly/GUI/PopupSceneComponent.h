#pragma once
#ifndef GUI_POPUP_SCEBE_COMPONENT_H
#define GUI_POPUP_SCENE_COMPONENT_H

#include "SceneComponent.h"

namespace gui {

class PopupSceneComponent : public SceneComponent {
  CEGUI::Window* m_previousModal;
public:
  PopupSceneComponent(std::string name_, const CEGUI::String& layoutFileName_);
  using SceneComponent::SceneComponent;
  virtual ~PopupSceneComponent() override;

  using SceneComponent::operator=;

  void setModal();
  void clearModal();
};

} // namespace gui

#endif // GUI_POPUP_SCENE_COMPONENT_H
