#include "MainMenuScene.h"

#include <CEGUI/widgets/DefaultWindow.h>
#include <CEGUI/widgets/PushButton.h>

#include <SDL.h>

#include "../Assert.h"

#include "../Game/ProfileManager.h"

#include "Scene.inl"
#include "SceneNames.h"
#include "stringConvert.h"

namespace gui {

MainMenuScene::MainMenuScene(const game::ProfileManager& profileManager)
  : Scene(mainMenuSceneName, "mainMenu.layout")
  , m_logger()
  , m_profileManager(&profileManager)
{
  logging::setModuleName(m_logger, "MainMenuScene");
}

void MainMenuScene::initializeComponents() {
  ASSERT(m_profileManager);

  auto frame = sceneRoot()->getChild("MenuFrame");
  auto profileLabel = getWidget<CEGUI::DefaultWindow>("MenuFrame/ProfileLabel");
  auto startButton = getWidget<CEGUI::PushButton>("MenuFrame/StartButton");
  auto profilesButton = getWidget<CEGUI::PushButton>("MenuFrame/ProfilesButton");
  auto settingsButton = getWidget<CEGUI::PushButton>("MenuFrame/SettingsButton");
  auto exitButton = getWidget<CEGUI::PushButton>("MenuFrame/ExitButton");

  auto activeProfile = m_profileManager->activeProfile();
  if (activeProfile) {
    activeProfile->unloadProfile();
    profileLabel->setText(fromStdString(activeProfile->name()));
  } else
    profileLabel->setText("<Error:NoProfile>");
  ASSERT(profileLabel->getText().size() <= game::Profile::maxNameUTF32Length);

  startButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        switchToScene(levelSelectSceneName);
        return true;
      }));
  profilesButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        switchToScene(switchProfileSceneName);
        return true;
      }));
  settingsButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        switchToScene(settingsSceneName);
        return true;
      }));
  exitButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [](const CEGUI::EventArgs&) -> bool {
        SDL_Event quit;
        quit.type = SDL_QUIT;
        int result = SDL_PushEvent(&quit);
        switch (result) {
        case 1:
          return true;
        case 0:
          ASSERT(false);
          throw MAKE_EXCEPTION("Attempted to push quit event; event was filtered");
        case -1: {
          std::ostringstream ss;
          ss << "Failed to push quit event: " << SDL_GetError();
          throw MAKE_EXCEPTION(ss.str());
        }
        default:
          ASSERT(false);
          return false;
        }
      }));

  frame->activate();
  
  LOG_SEVERITY(m_logger, Debug) << "Initialized components";
}

} // namespace gui
