#include "SimpleLevelScene.h"

#include <sstream>

#include <CEGUI/widgets/PushButton.h>
#include <CEGUI/widgets/ScrollablePane.h>
#include <CEGUI/widgets/Scrollbar.h>

#include "../Game/SimpleLevel.h"
#include "../Game/SimpleLevelSolution.h"
#include "../Game/SimpleFactory.h"

#include "../Assert.h"
#include "../Exception.h"

#include "DirectionArrowControl.h"
#include "DirectionArrowControlButton.h"
#include "Scene.inl"
#include "SceneNames.h"
#include "LevelSpeedControl.h"
#include "UISimpleLevelGrid.h"
#include "WorkerSelectionControl.h"

namespace gui {

SimpleLevelScene::SimpleLevelScene()
  : LevelScene(simpleLevelSceneName, "simpleLevel.layout")
  , m_logger()
  , m_level(nullptr)
  , m_solution(nullptr)
  , m_levelGrid(nullptr)
  , m_levelSpeedControl(nullptr)
  , m_workerSelectionControl(nullptr)
  , m_directionArrowControl(nullptr)
{
  logging::setModuleName(m_logger, "SimpleLevelScene");
}

void SimpleLevelScene::initializeComponents() {
  LevelScene::initializeComponents();
  ASSERT(m_level);
  ASSERT(m_solution);

  auto backButton = getWidget<CEGUI::PushButton>("BottomPanel/BackButton");
  m_levelGrid = getWidget<UISimpleLevelGrid>("LevelGrid");
  m_levelSpeedControl = getWidget<LevelSpeedControl>("BottomPanel/LevelSpeedControl");
  m_workerSelectionControl = getWidget<WorkerSelectionControl>("BottomPanel/WorkerSelectionControl");
  m_directionArrowControl = getWidget<DirectionArrowControl>("BottomPanel/DirectionArrowControl");

  m_levelGrid->setFactory(m_solution->factory());
  m_workerSelectionControl->setNumWorkers(m_solution->factory().numWorkers());

  backButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        switchToPreviousScene();
        return true;
      }
    ));

  m_workerSelectionControl->subscribeEvent(
    WorkerSelectionControl::EventWorkerSelectionChanged, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        auto selectedWorker = m_workerSelectionControl->getWorkerSelection();
        m_levelGrid->setSelectedWorker(selectedWorker);
        m_directionArrowControl->setWorkerSelection(selectedWorker);
        return true;
      }
    ));

  m_directionArrowControl->getLeftArrowButton()->subscribeEvent(
    DirectionArrowControlButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        m_levelGrid->setSelectedCellDirection(
          m_workerSelectionControl->getWorkerSelection(), game::Direction::Left);
        return true;
      }
    ));
  m_directionArrowControl->getUpArrowButton()->subscribeEvent(
    DirectionArrowControlButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        m_levelGrid->setSelectedCellDirection(
          m_workerSelectionControl->getWorkerSelection(), game::Direction::Up);
        return true;
      }
    ));
  m_directionArrowControl->getRightArrowButton()->subscribeEvent(
    DirectionArrowControlButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        m_levelGrid->setSelectedCellDirection(
          m_workerSelectionControl->getWorkerSelection(), game::Direction::Right);
        return true;
      }
    ));
  m_directionArrowControl->getDownArrowButton()->subscribeEvent(
    DirectionArrowControlButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        m_levelGrid->setSelectedCellDirection(
          m_workerSelectionControl->getWorkerSelection(), game::Direction::Down);
        return true;
      }
    ));

  sceneRoot()->subscribeEvent(CEGUI::Window::EventKeyDown,
    CEGUI::SubscriberSlot(&SimpleLevelScene::handleKeyDown, this));

  m_levelGrid->setSelectedWorker(m_workerSelectionControl->getWorkerSelection());

  sceneRoot()->activate();
}

void SimpleLevelScene::setLevelAndSolution(
  game::Level* level, game::LevelSolution* solution)
{
  m_level = dynamic_cast<game::SimpleLevel*>(level);
  m_solution = dynamic_cast<game::SimpleLevelSolution*>(solution);

  if (level && !m_level) {
    std::ostringstream ss;
    ss << "SimpleLevelScene: tried to set level to \"" << level->name()
       << "\" which is of unsupported type " << level->typeName() << " (expected SimpleLevel)";
    throw MAKE_EXCEPTION(ss.str());
  }
  if (solution && !m_solution) {
    std::ostringstream ss;
    ss << "SimpleLevelScene: tried to set solution to \"" << solution->name()
       << "\" which is of unsupported type (expected SimpleLevelSolution)";
    throw MAKE_EXCEPTION(ss.str());
  }
}

bool SimpleLevelScene::handleKeyDown(const CEGUI::EventArgs& e) {
  using CEGUI::Key;
  auto& event = static_cast<const CEGUI::KeyEventArgs&>(e);

  switch (event.scancode) {
  case Key::Scan::W:
  case Key::Scan::A:
  case Key::Scan::S:
  case Key::Scan::D:
  case Key::Scan::ArrowUp:
  case Key::Scan::ArrowLeft:
  case Key::Scan::ArrowDown:
  case Key::Scan::ArrowRight:
  case Key::Scan::Delete:
  case Key::Scan::Z:
  case Key::Scan::Y:
    m_levelGrid->handleKeyDown(event);
    return true;
  case Key::Scan::One:
    if (m_solution->factory().numWorkers() >= 1u) {
      m_workerSelectionControl->setWorkerSelection(0u);
      return true;
    } else
      return false;
  case Key::Scan::Two:
    if (m_solution->factory().numWorkers() >= 2u) {
      m_workerSelectionControl->setWorkerSelection(1u);
      return true;
    } else
      return false;
  case Key::Scan::Three:
    if (m_solution->factory().numWorkers() >= 3u) {
      m_workerSelectionControl->setWorkerSelection(2u);
      return true;
    } else
      return false;
  case Key::Scan::Four:
    if (m_solution->factory().numWorkers() == 4u) {
      m_workerSelectionControl->setWorkerSelection(3u);
      return true;
    } else
      return false;
  case Key::Scan::Escape:
    m_levelGrid->clearSelections();
    return true;
  case Key::Scan::Tab:
    m_workerSelectionControl->setWorkerSelection(
      (m_workerSelectionControl->getWorkerSelection() + 1) % m_solution->factory().numWorkers());
    return true;
  default:
    return false;
  }
}

game::Level* SimpleLevelScene::level() noexcept { return m_level; }
game::LevelSolution* SimpleLevelScene::solution() noexcept { return m_solution; }

} // namespace gui
