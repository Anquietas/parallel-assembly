#include "DirectionArrowControlButton.h"

#include <CEGUI/CoordConverter.h>
#include <CEGUI/falagard/WidgetLookFeel.h>
#include <CEGUI/RenderingContext.h>
#include <CEGUI/RenderingSurface.h>
#include <CEGUI/RenderTarget.h>

#include "../Game/Worker.h"

#include "../Assert.h"
#include "../Exception.h"

#include "DirectionArrowDraggable.h"
#include "DirectionArrowGeometryBuffer.h"

namespace gui {

const CEGUI::String DirectionArrowControlButtonRenderer::TypeName("PAGUI/DirectionArrowControlButton");

const CEGUI::String DirectionArrowControlButton::WidgetTypeName("PAGUI/DirectionArrowControlButton");
const CEGUI::String DirectionArrowControlButton::EventNamespace("DirectionArrowControlButton");

const CEGUI::String DirectionArrowControlButton::DraggableName("__auto_draggable__");

DirectionArrowControlButtonRenderer::DirectionArrowControlButtonRenderer(const CEGUI::String& type)
  : BaseType(type)
{}

void DirectionArrowControlButtonRenderer::onAttach() {
  m_window = dynamic_cast<DirectionArrowControlButton*>(d_window);
  if (!m_window)
    throw MAKE_EXCEPTION("Window attached to DirectionArrowControlButtonRenderer is not a DirectionArrowControlButton");
}
void DirectionArrowControlButtonRenderer::onDetach() {
  m_window = nullptr;
}

CEGUI::Rectf DirectionArrowControlButtonRenderer::getRenderArea() const {
  ASSERT(m_window);

  auto& wlf = getLookNFeel();

  return wlf.getNamedArea("RenderArea").getArea().getPixelRect(*m_window);
}

DirectionArrowControlButton::DirectionArrowControlButton(
  const CEGUI::String& type, const CEGUI::String& name)
  : BaseType(type, name)
  , m_geometryBuffer(new DirectionArrowGeometryBuffer())
  , m_draggable(nullptr)
  , m_direction(game::Direction::Left)
  , m_workerSelection(0u)
{
  ASSERT(m_direction != game::Direction::None);
  ASSERT(m_workerSelection < game::Worker::maxWorkers);
  m_geometryBuffer->setDirection(m_direction);
  m_geometryBuffer->setWorkerSelection(m_workerSelection);
}

DirectionArrowControlButton::~DirectionArrowControlButton() = default;

void DirectionArrowControlButton::initialiseComponents() {
  BaseType::initialiseComponents();

  m_draggable = &dynamic_cast<DirectionArrowDraggable&>(*getChild(DraggableName));

  m_draggable->setDirection(m_direction);
  m_draggable->setWorkerSelection(m_workerSelection);
}

DirectionArrowDraggable* DirectionArrowControlButton::getDraggable() const {
  ASSERT(m_draggable);

  return m_draggable;
}

bool DirectionArrowControlButton::validateWindowRenderer(const CEGUI::WindowRenderer* renderer) const {
  return dynamic_cast<const DirectionArrowControlButtonRenderer*>(renderer) != nullptr;
}

void DirectionArrowControlButton::onMouseButtonDown(CEGUI::MouseEventArgs& e) {
  ASSERT(m_draggable);
  BaseType::onMouseButtonDown(e);

  // Adapted from CEGUI::DragContainer::onMouseButtonDown() to make the
  // child DragContainer play nicely with the button behaviour

  if (e.button == CEGUI::MouseButton::LeftButton) {
    if (captureInput()) {
      auto localPos = CEGUI::CoordConverter::screenToWindow(*m_draggable, e.position);

      m_draggable->d_dragPoint.d_x = cegui_absdim(localPos.d_x);
      m_draggable->d_dragPoint.d_y = cegui_absdim(localPos.d_y);
      m_draggable->d_leftMouseDown = true;
    }

    ++e.handled;
  }
}
void DirectionArrowControlButton::onMouseButtonUp(CEGUI::MouseEventArgs& e) {
  ASSERT(m_draggable);
  m_draggable->onMouseButtonUp(e);

  BaseType::onMouseButtonUp(e);
}
void DirectionArrowControlButton::onMouseMove(CEGUI::MouseEventArgs& e) {
  ASSERT(m_draggable);
  BaseType::onMouseMove(e);

  // Adapted from CEGUI::DragContainer::onMouseMove() to make the
  // child DragContainer play nicely with the button behaviour

  auto localMousePos = CEGUI::CoordConverter::screenToWindow(*m_draggable, e.position);

  if (m_draggable->d_dragging)
    m_draggable->doDragging(localMousePos);
  else {
    if (m_draggable->d_leftMouseDown) {
      if (m_draggable->isDraggingThresholdExceeded(localMousePos)) {
        CEGUI::WindowEventArgs args(m_draggable);
        m_draggable->onDragStarted(args);
      }
    }
  }
}
void DirectionArrowControlButton::onCaptureLost(CEGUI::WindowEventArgs& e) {
  ASSERT(m_draggable);
  BaseType::onCaptureLost(e);

  m_draggable->onCaptureLost(e);
}

void DirectionArrowControlButton::drawSelf(const CEGUI::RenderingContext& ctx) {
  ASSERT(d_windowRenderer);

  const bool needsRedraw = d_needsRedraw;
  BaseType::drawSelf(ctx);

  if (needsRedraw) {
    auto renderer = static_cast<DirectionArrowControlButtonRenderer*>(d_windowRenderer);
    auto renderArea = renderer->getRenderArea();
    renderArea.setPosition(renderArea.d_min + getPixelPosition());

    m_geometryBuffer->setDirection(m_direction);
    m_geometryBuffer->setWorkerSelection(m_workerSelection);
    m_geometryBuffer->setRenderTargetArea(ctx.surface->getRenderTarget().getArea());
    m_geometryBuffer->setRenderArea(renderArea);
    m_geometryBuffer->updateRenderData();
  }

  ctx.surface->addGeometryBuffer(ctx.queue, *m_geometryBuffer);
}

void DirectionArrowControlButton::setDirection(game::Direction direction) {
  ASSERT(m_draggable);
  ASSERT(direction != game::Direction::None);
  if (m_direction != direction) {
    m_direction = direction;
    m_draggable->setDirection(m_direction);
    invalidate();
  }
}
game::Direction DirectionArrowControlButton::getDirection() const {
  return m_direction;
}

void DirectionArrowControlButton::setWorkerSelection(std::size_t workerSelection) {
  ASSERT(m_draggable);
  ASSERT(workerSelection < game::Worker::maxWorkers);
  if (m_workerSelection != workerSelection) {
    m_workerSelection = workerSelection;
    m_draggable->setWorkerSelection(m_workerSelection);
    invalidate();
  }
}
std::size_t DirectionArrowControlButton::getWorkerSelection() const {
  return m_workerSelection;
}

} // namespace gui
