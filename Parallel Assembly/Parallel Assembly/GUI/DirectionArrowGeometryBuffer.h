#pragma once
#ifndef GUI_DIRECTION_ARROW_GEOMETRY_BUFFER_H
#define GUI_DIRECTION_ARROW_GEOMETRY_BUFFER_H

#include <CEGUI/GeometryBuffer.h>

#include "../Game/Direction.h"

#include "../Graphics/OpenGL/VertexArrayObject.h"
#include "../Graphics/OpenGL/VertexBuffer.h"

namespace graphics {
class ShaderManager;
class DirectionArrowShaderProgram;
}

namespace gui {

class DirectionArrowGeometryBuffer : public CEGUI::GeometryBuffer {
  static graphics::DirectionArrowShaderProgram* directionArrowShader;

  game::Direction m_direction;
  std::size_t m_workerSelection;

  glm::vec2 m_screenSize;
  glm::vec2 m_renderPosition;
  graphics::gl::float_t m_gridCellSize;

  // Mutable to allow binding in draw()
  mutable graphics::gl::VertexArrayObject m_vao;
  graphics::gl::VertexBuffer m_meshVBO;
  graphics::gl::VertexBuffer m_dataVBO;

public:
  DirectionArrowGeometryBuffer();
  virtual ~DirectionArrowGeometryBuffer() override = default;

  static void setupShader(graphics::ShaderManager& shaderManager);

  virtual void draw() const override;
  virtual void setTranslation(const CEGUI::Vector3f&) override {}
  virtual void setRotation(const CEGUI::Quaternion&) override {}
  virtual void setPivot(const CEGUI::Vector3f&) override {}
  virtual void setClippingRegion(const CEGUI::Rectf&) override {}
  virtual void appendVertex(const CEGUI::Vertex&) override {}
  virtual void appendGeometry(const CEGUI::Vertex* const, CEGUI::uint) override {}
  virtual void setActiveTexture(CEGUI::Texture*) override {}
  virtual void reset() override {}
  virtual CEGUI::Texture* getActiveTexture() const override { return nullptr; }
  virtual CEGUI::uint getVertexCount() const override { return 0u; }
  virtual CEGUI::uint getBatchCount() const override { return 0u; }
  virtual void setRenderEffect(CEGUI::RenderEffect*) override {}
  virtual CEGUI::RenderEffect* getRenderEffect() override { return nullptr; }
  virtual void setClippingActive(const bool) override {}
  virtual bool isClippingActive() const override { return false; }

  void setDirection(game::Direction direction);
  game::Direction getDirection() const;

  void setWorkerSelection(std::size_t workerSelection);
  std::size_t getWorkerSelection() const;

  void updateRenderData();
  void setRenderTargetArea(const CEGUI::Rectf& renderTargetArea);
  void setRenderArea(const CEGUI::Rectf& renderArea);
};

} // namespace gui

#endif // GUI_DIRECTION_ARROW_GEOMETRY_BUFFER_H
