#include "parseResolution.h"

#include <boost/spirit/home/x3.hpp>
#include <boost/fusion/adapted/struct.hpp>

BOOST_FUSION_ADAPT_STRUCT(glm::ivec2, (int, x)(int, y))

namespace gui {

namespace spirit = boost::spirit;
namespace x3 = spirit::x3;

glm::ivec2 parseResolution(const char* begin, const char* end) {
  auto grammar = x3::uint_ > 'x' > x3::uint_;

  glm::ivec2 result;
  try {
    bool succeeded = x3::phrase_parse(begin, end, grammar, x3::ascii::space, result);

    // Fail if parse fails or we don't get a full match
    if (!succeeded || begin != end)
      return{-1, -1};
    else
      return result;
  } catch (const x3::expectation_failure<const char*>&) {
    // ignore the exception and return failure
    return{-1, -1};
  }
}

} // namespace gui
