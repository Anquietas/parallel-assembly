#pragma once
#ifndef GUI_SCENE_COMPONENT_H
#define GUI_SCENE_COMPONENT_H

#include <string>
#include <type_traits>

#include <CEGUI/Window.h>

#include "../Logger.h"

namespace gui {

class Scene;

class SceneComponent {
  friend class Scene;
  STATIC_LOGGER_DECLARE(logger);

  std::string m_name;
  CEGUI::String m_layoutFileName;
  CEGUI::Window* m_root; // non-owning
  Scene* m_target; // non-owning
  const bool m_keepLoaded; // flag to specify the component should stay loaded after scene unload
  const bool m_transferable; // flag to specify the component should be transferred on scene switch

  // Called just after the SceneComponent is loaded from its layout file
  // Default implementation does nothing
  virtual void initializeComponents();
  // Called just before the SceneComponent is unloaded
  // Default implementation does nothing
  virtual void cleanupComponents();

  void attach(Scene& scene);
  void detach();

  // Called just after the SceneComponent is attached to a scene
  // Default implementation does nothing
  virtual void onAttach();
  // Called just before the SceneComponent is detached from a scene
  // Default implementation does nothing
  virtual void onDetach();
public:
  // Constructor
  // name should be non-empty and layoutFileName must be a valid file name
  SceneComponent(std::string name_,
                 const CEGUI::String& layoutFileName_,
                 bool keepLoaded_ = false,
                 bool transferable_ = false);
  SceneComponent(const SceneComponent&) = delete;
  SceneComponent(SceneComponent&&) = delete;
  virtual ~SceneComponent();

  SceneComponent& operator=(const SceneComponent&) = delete;
  SceneComponent& operator=(SceneComponent&&) = delete;

  const std::string& name() const { return m_name; }
  const CEGUI::String& layoutFileName() const { return m_layoutFileName; }

  CEGUI::Window* root() { return m_root; }
  const CEGUI::Window* root() const { return m_root; }

  bool keepLoaded() const noexcept { return m_keepLoaded; }

  bool loaded() const noexcept { return m_root != nullptr; }
  // Loads the SceneComponent from its layout file if it isn't already loaded
  void load();
  // Unloads the SceneComponent if it is loaded after calling cleanupComponents; this will destroy child widgets
  void unload();

  template <typename ExpectedType>
  std::enable_if_t<
    std::is_base_of<CEGUI::Window, std::decay_t<ExpectedType>>::value,
    std::decay_t<ExpectedType>*
  > getWidget(const char* widgetName);

  bool transferable() const noexcept { return m_transferable; }

  // Attaches the SceneComponent to the given Scene.
  // Note: make sure the scene outlives the component.
  void attachTo(Scene& scene);
  // Attaches the SceneComponent to the currently active Scene.
  // Throws an exception if there is no active scene
  void attachToActiveScene();
  // Detaches the SceneComponent from the Scene it was previously attached to,
  // and calls onDetach if the scene is loaded
  // Throws an exception if it was not previously attached
  void detachFromTarget();

  bool isAttached() const { return m_target != nullptr; }
};

} // namespace gui

#endif // GUI_SCENE_COMPONENT
