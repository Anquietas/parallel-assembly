#include "LevelGridSelections.h"

#include <iterator>
#include <utility>

#include <boost/variant/get.hpp>
#include <boost/variant/apply_visitor.hpp>

#include "../Assert.h"

namespace gui {

MultiSelection::MultiSelection()
  : m_selectedArrows()
  , m_arrowsWorkerIDsIncluded()
{
  m_arrowsWorkerIDsIncluded.fill(false);
}

bool MultiSelection::empty() const noexcept {
  return m_selectedArrows.empty();
}

void MultiSelection::clear() noexcept {
  m_selectedArrows.clear();
  m_arrowsWorkerIDsIncluded.fill(false);
}

std::size_t MultiSelection::size() const noexcept {
  return m_selectedArrows.size();
}

bool MultiSelection::addArrowSelection(const DirectionArrowSelection& arrow) {
  ASSERT(arrow.workerID < game::Worker::maxWorkers);

  auto result = m_selectedArrows.insert(arrow);
  m_arrowsWorkerIDsIncluded[arrow.workerID] = true;

  return result.second;
}
bool MultiSelection::removeArrowSelection(const DirectionArrowSelection& arrow) {
  auto iter = m_selectedArrows.find(arrow);
  if (iter != m_selectedArrows.end()) {
    m_selectedArrows.erase(iter);

    // Not worth figuring out if a previously included worker ID is no longer included
    return true;
  }
  return false;
}
void MultiSelection::removeArrowSelection(DirectionArrowIter iter) {
  ASSERT(iter != m_selectedArrows.end());
  m_selectedArrows.erase(iter);
}

void MultiSelection::mergeSelection(MultiSelection& selection) {
  m_selectedArrows.merge(selection.m_selectedArrows);

  for (std::size_t worker = 0u; worker < game::Worker::maxWorkers; ++worker)
    m_arrowsWorkerIDsIncluded[worker] =
      m_arrowsWorkerIDsIncluded[worker] || selection.m_arrowsWorkerIDsIncluded[worker];

  selection.clear();
}

MultiSelection::DirectionArrowIter MultiSelection::findArrowSelection(const DirectionArrowSelection& arrow) const {
  return m_selectedArrows.find(arrow);
}
bool MultiSelection::isIncluded(std::size_t workerID) const noexcept {
  ASSERT(workerID < game::Worker::maxWorkers);

  return m_arrowsWorkerIDsIncluded[workerID];
}

bool MultiSelection::contains(const DirectionArrowSelection& arrow) const noexcept {
  return m_selectedArrows.find(arrow) != m_selectedArrows.end();
}

template <typename Iter>
using is_bidirectional = std::disjunction<
  std::is_same<
    std::bidirectional_iterator_tag,
    typename std::iterator_traits<Iter>::iterator_category
  >,
  std::is_same<
    std::random_access_iterator_tag,
    typename std::iterator_traits<Iter>::iterator_category
  >>;
template <typename Iter>
using is_random_access = std::is_same<
  std::random_access_iterator_tag,
  typename std::iterator_traits<Iter>::iterator_category>;

template <typename Iter, typename Iter2>
std::enable_if_t<
  is_random_access<Iter>::value && is_bidirectional<Iter2>::value, bool
> fastIncludes(Iter first1, Iter last1, Iter2 first2, Iter2 last2) noexcept {
  // [first2, last2) empty: always true
  if (first2 == last2)
    return true;
  // [first1, last1) empty, [first2, last2) non-empty: always false
  if (first1 == last1)
    return false;

  // Refine "outer" bounds to a minimal range in O(logN) time
  first1 = std::lower_bound(first1, last1, *first2);
  last1 = std::upper_bound(first1, last1, *std::prev(last2));

  return std::includes(first1, last1, first2, last2);
}
template <typename Iter, typename Iter2>
std::enable_if_t<
  !is_random_access<Iter>::value && is_bidirectional<Iter2>::value, bool
> fastIncludes(Iter first1, Iter last1, Iter2 first2, Iter2 last2) noexcept {
  // The above optimization is not an improvement if Iter is not random access
  return std::includes(first1, last1, first2, last2);
}

bool MultiSelection::contains(const MultiSelection& selection) const noexcept {
  return fastIncludes(
    m_selectedArrows.begin(), m_selectedArrows.end(),
    selection.m_selectedArrows.begin(), selection.m_selectedArrows.end());
}

void LevelGridSelection::clear() {
  set(NullSelection{});
}

void LevelGridHoverSelection::clear() {
  set(NullSelection{});
}

template <typename T>
struct IsTypeVisitor : boost::static_visitor<bool> {
  bool operator()(const T&) const noexcept {
    return true;
  }
  template <typename U>
  bool operator()(const U&) const noexcept {
    return false;
  }
};

template <typename T>
std::enable_if_t<isSelectionType<T>::value, bool>
LevelGridSelection::is() const noexcept {
  return boost::apply_visitor(IsTypeVisitor<T>{}, m_selection);
}
template <typename T>
std::enable_if_t<isHoverSelectionType<T>::value, bool>
LevelGridHoverSelection::is() const noexcept {
  return boost::apply_visitor(IsTypeVisitor<T>{}, m_hover);
}

template <typename T>
std::enable_if_t<isSelectionType<T>::value,
  std::add_lvalue_reference_t<T>
> LevelGridSelection::as() {
  return boost::get<T>(m_selection);
}
template <typename T>
std::enable_if_t<isSelectionType<T>::value,
  std::add_lvalue_reference_t<std::add_const_t<T>>
> LevelGridSelection::as() const {
  return boost::get<T>(m_selection);
}
template <typename T>
std::enable_if_t<isHoverSelectionType<T>::value,
  std::add_lvalue_reference_t<T>
> LevelGridHoverSelection::as() {
  return boost::get<T>(m_hover);
}
template <typename T>
std::enable_if_t<isHoverSelectionType<T>::value,
  std::add_lvalue_reference_t<std::add_const_t<T>>
> LevelGridHoverSelection::as() const {
  return boost::get<T>(m_hover);
}

template <typename T, typename U>
struct ContainsImpl {
  bool operator()(const T&, const U&) const noexcept {
    return false;
  }
};
template <typename T>
struct ContainsImpl<T, T> {
  bool operator()(const T& selection, const T& querySelection) noexcept {
    return selection == querySelection;
  }
};
template <>
struct ContainsImpl<MultiSelection, DirectionArrowSelection> {
  bool operator()(const MultiSelection& selection, const DirectionArrowSelection& querySelection) noexcept {
    return selection.contains(querySelection);
  }
};
template <>
struct ContainsImpl<MultiSelection, MultiSelection> {
  bool operator()(const MultiSelection& selection, const MultiSelection& querySelection) noexcept {
    return selection.contains(querySelection);
  }
};

template <typename T, typename U>
using Contains = ContainsImpl<std::decay_t<T>, std::decay_t<U>>;

template <typename T>
std::enable_if_t<
  isSelectionType<T>::value && !std::is_same<T, NullSelection>::value, bool
> LevelGridSelection::contains(const T& querySelection) const noexcept {
  return boost::apply_visitor(
    [&querySelection](const auto& selection) -> bool {
      return Contains<decltype(selection), T>{}(selection, querySelection);
    }, m_selection);
}
template <typename T>
std::enable_if_t<
  isHoverSelectionType<T>::value && !std::is_same<T, NullSelection>::value, bool
> LevelGridHoverSelection::contains(const T& querySelection) const noexcept {
  return boost::apply_visitor(
    [&querySelection](const auto& selection) -> bool {
      return Contains<decltype(selection), T>{}(selection, querySelection);
    }, m_hover);
}

#define INSTANTIATE_SELECTION_IS(T) \
  template std::enable_if_t<isSelectionType<T>::value, bool> \
  LevelGridSelection::is<T>() const noexcept
#define INSTANTIATE_HOVER_SELECTION_IS(T) \
  template std::enable_if_t<isHoverSelectionType<T>::value, bool> \
  LevelGridHoverSelection::is<T>() const noexcept
#define INSTANTIATE_SELECT_HOVER_IS(T) \
  INSTANTIATE_SELECTION_IS(T); \
  INSTANTIATE_HOVER_SELECTION_IS(T)

#define INSTANTIATE_SELECTION_AS(T) \
  template std::enable_if_t<isSelectionType<T>::value, \
    std::add_lvalue_reference_t<T> \
  > LevelGridSelection::as<T>(); \
  template std::enable_if_t<isSelectionType<T>::value, \
    std::add_lvalue_reference_t<std::add_const_t<T>> \
  > LevelGridSelection::as<T>() const
#define INSTANTIATE_HOVER_SELECTION_AS(T) \
  template std::enable_if_t<isHoverSelectionType<T>::value, \
    std::add_lvalue_reference_t<T> \
  > LevelGridHoverSelection::as<T>(); \
  template std::enable_if_t<isHoverSelectionType<T>::value, \
    std::add_lvalue_reference_t<std::add_const_t<T>> \
  > LevelGridHoverSelection::as<T>() const
#define INSTANTIATE_SELECT_HOVER_AS(T) \
  INSTANTIATE_SELECTION_AS(T); \
  INSTANTIATE_HOVER_SELECTION_AS(T)

#define INSTANTIATE_SELECTION_CONTAINS(T) \
  template std::enable_if_t< \
    isSelectionType<T>::value && !std::is_same<T, NullSelection>::value, bool \
  > LevelGridSelection::contains<T>(const T& selection) const noexcept
#define INSTANTIATE_HOVER_SELECTION_CONTAINS(T) \
  template std::enable_if_t< \
    isHoverSelectionType<T>::value && !std::is_same<T, NullSelection>::value, bool \
  > LevelGridHoverSelection::contains<T>(const T& selection) const noexcept
#define INSTANTIATE_SELECT_HOVER_CONTAINS(T) \
  INSTANTIATE_SELECTION_CONTAINS(T); \
  INSTANTIATE_HOVER_SELECTION_CONTAINS(T)

#define INSTANTIATE_SELECTION_ALL(T) \
  INSTANTIATE_SELECTION_IS(T); \
  INSTANTIATE_SELECTION_AS(T); \
  INSTANTIATE_SELECTION_CONTAINS(T)
#define INSTANTIATE_HOVER_SELECTION_ALL(T) \
  INSTANTIATE_HOVER_SELECTION_IS(T); \
  INSTANTIATE_HOVER_SELECTION_AS(T); \
  INSTANTIATE_HOVER_SELECTION_CONTAINS(T)
#define INSTANTIATE_SELECT_HOVER_ALL(T) \
  INSTANTIATE_SELECTION_ALL(T); \
  INSTANTIATE_HOVER_SELECTION_ALL(T)

INSTANTIATE_SELECT_HOVER_IS(NullSelection);
INSTANTIATE_SELECT_HOVER_AS(NullSelection);
INSTANTIATE_SELECT_HOVER_ALL(GridCellSelection);
INSTANTIATE_SELECT_HOVER_ALL(DirectionArrowSelection);
INSTANTIATE_SELECTION_ALL(MultiSelection);

#undef INSTANTIATE_SELECTION_IS
#undef INSTANTIATE_HOVER_SELECTION_IS
#undef INSTANTIATE_SELECT_HOVER_IS

#undef INSTANTIATE_SELECTION_AS
#undef INSTANTIATE_HOVER_SELECTION_AS
#undef INSTANTIATE_SELECT_HOVER_AS

#undef INSTANTIATE_SELECTION_CONTAINS
#undef INSTANTIATE_HOVER_SELECTION_CONTAINS
#undef INSTANTIATE_SELECT_HOVER_CONTAINS

#undef INSTANTIATE_SELECTION_ALL
#undef INSTANTIATE_HOVER_SELECTION_ALL
#undef INSTANTIATE_SELECT_HOVER_ALL

} // namespace gyi
