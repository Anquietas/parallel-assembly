#include "RenderableWindow.h"

#include <sstream>

#include <CEGUI/System.h>
#include <CEGUI/ImageManager.h>
#include <CEGUI/falagard/WidgetLookFeel.h>
#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>
#include <CEGUI/RendererModules/OpenGL/Shader.h>
#include <CEGUI/RenderingContext.h>
#include <CEGUI/System.h>

#include "../Graphics/OpenGL/checkError.h"

#include "../Assert.h"
#include "../Exception.h"
#include "../GameSettings.h"

namespace gui {

const CEGUI::String RenderableWindowRenderer::TypeName("PAGUI/RenderableWindow");

const CEGUI::String RenderableWindow::EventNamespace("RenderableWindow");

RenderableWindowRenderer::RenderableWindowRenderer(const CEGUI::String& type)
  : FalagardDefault(type)
  , m_window(nullptr)
{}

void RenderableWindowRenderer::onAttach() {
  m_window = dynamic_cast<RenderableWindow*>(d_window);
  if (!m_window)
    throw MAKE_EXCEPTION("Window attached to RenderableWindowRenderer is not a RenderableWindow");
}
void RenderableWindowRenderer::onDetach() {
  m_window = nullptr;
}

CEGUI::Rectf RenderableWindowRenderer::getRenderArea() const {
  ASSERT(m_window);

  auto& wlf = getLookNFeel();

  return wlf.getNamedArea("RenderArea").getArea().getPixelRect(*m_window);
}

RenderableWindow::RenderableWindow(const CEGUI::String& type, const CEGUI::String& name)
  : Window(type, name)
  , m_fbo(nullptr)
  , m_texture(nullptr)
  , m_depthBuffer(nullptr)
  , m_textureTarget(nullptr)
  , m_image(nullptr)
  , m_renderArea()
  , m_renderAreaInvalid()
{
  using namespace graphics::gl;

  auto renderer = CEGUI::System::getSingleton().getRenderer();
  auto imageManager = CEGUI::ImageManager::getSingletonPtr();
  try {
    auto size = getPixelSize();
    if (size == CEGUI::Sizef{0.f, 0.f})
      size = CEGUI::Sizef{10.f, 10.f};
    m_renderArea = CEGUI::Rectf{{}, size};

    auto gameSettings = GameSettings::get();
    
    bool aaEnabled = gameSettings->isAAEnabled();
    if (aaEnabled) {
      GLsizei samples = gameSettings->aaLevel().level;

      GLuint oldFBOBinding = 0;
      glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, reinterpret_cast<GLint*>(&oldFBOBinding));

      m_fbo.init();
      m_fbo.bind();
      CHECK_GL_ERROR_PEDANTIC();

      GLsizei width = static_cast<GLsizei>(size.d_width);
      GLsizei height = static_cast<GLsizei>(size.d_height);

      m_texture.init();
      m_texture.bind();
      m_texture.allocate(internal_format::Colour::RGBA_8, width, height, samples, true);

      GLuint oldRenderBufferBinding = 0;
      glGetIntegerv(GL_RENDERBUFFER_BINDING, reinterpret_cast<GLint*>(&oldRenderBufferBinding));

      m_depthBuffer.init();
      m_depthBuffer.bind();
      m_depthBuffer.allocate(internal_format::DepthStencil::Depth_24_Stencil_8,
                             width, height, samples);
      CHECK_GL_ERROR_PEDANTIC();

      FrameBuffer::attach(FrameBuffer::Target::Draw,
                          FrameBuffer::ColourAttachment{0},
                          m_texture);
      FrameBuffer::attach(FrameBuffer::Target::Draw,
                          FrameBuffer::DepthStencilAttachment::DepthStencil,
                          m_depthBuffer);
      CHECK_GL_ERROR_PEDANTIC();

      if (!FrameBuffer::isComplete()) {
        std::ostringstream ss;
        ss << "MSAA FrameBuffer for RenderableWindow " << name << " of type " << type
           << " not complete after construction.";
        throw MAKE_EXCEPTION(ss.str());
      }

      // Restore old bindings
      ColourMultisampleTexture2D::unbind();
      glBindRenderbuffer(GL_RENDERBUFFER, oldRenderBufferBinding);
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, oldFBOBinding);

      CHECK_GL_ERROR();
    }

    m_textureTarget = static_cast<CEGUI::OpenGL3FBOTextureTarget*>(renderer->createTextureTarget());
    m_textureTarget->declareRenderSize(size);

    auto image = &imageManager->create("BasicImage", name + "__render_image__");
    m_image = dynamic_cast<CEGUI::BasicImage*>(image);
    if (!m_image)
      throw MAKE_EXCEPTION("image type \"BasicImage\" is not of type BasicImage");

    m_image->setAutoScaled(CEGUI::ASM_Disabled);
    m_image->setTexture(&m_textureTarget->getTexture());
  } catch (...) {
    if (m_textureTarget)
      renderer->destroyTextureTarget(m_textureTarget);
    if (m_image)
      imageManager->destroy(*m_image);

    throw;
  }
}

RenderableWindow::~RenderableWindow() {
  if (m_textureTarget)
    CEGUI::System::getSingleton().getRenderer()->destroyTextureTarget(m_textureTarget);
  if (m_image)
    CEGUI::ImageManager::getSingleton().destroy(*m_image);
}

void RenderableWindow::setRenderArea(const CEGUI::Rectf& renderArea) {
  if (m_renderArea != renderArea) {
    m_renderArea = renderArea;
    m_renderAreaInvalid.set();
  }
}
void RenderableWindow::updateRenderArea() {
  using namespace graphics::gl;

  auto size = m_renderArea.getSize();
  auto gameSettings = GameSettings::get();
  bool aaEnabled = gameSettings->isAAEnabled();
  if (aaEnabled) {
    ASSERT(m_fbo.valid());
    ASSERT(m_texture.valid());
    ASSERT(m_depthBuffer.valid());

    GLsizei width = static_cast<GLsizei>(size.d_width);
    GLsizei height = static_cast<GLsizei>(size.d_height);
    GLsizei samples = gameSettings->aaLevel().level;

    // Update texture and render buffer sizes
    GLuint oldRenderBufferBinding = 0;
    glGetIntegerv(GL_RENDERBUFFER_BINDING, reinterpret_cast<GLint*>(&oldRenderBufferBinding));

    ASSERT(width > 0 && height > 0);
    m_texture.bind();
    m_texture.allocate(internal_format::Colour::RGBA_8, width, height, samples, true);

    m_depthBuffer.bind();
    m_depthBuffer.allocate(internal_format::DepthStencil::Depth_24_Stencil_8,
                           width, height, samples);

    // Restore old bindings
    ColourMultisampleTexture2D::unbind();
    glBindRenderbuffer(GL_RENDERBUFFER, oldRenderBufferBinding);

    CHECK_GL_ERROR_PEDANTIC();
  }

  m_textureTarget->declareRenderSize(size);

  m_image->setArea({0.f, 0.f, size.d_width, size.d_height});

  m_renderAreaInvalid.clear();
}

void RenderableWindow::onMoved(CEGUI::ElementEventArgs& e) {
  if (d_windowRenderer) {
    auto renderer = static_cast<const RenderableWindowRenderer*>(d_windowRenderer);
    setRenderArea(renderer->getRenderArea());
    ++e.handled;
  }
  BaseType::onMoved(e);
}
void RenderableWindow::onSized(CEGUI::ElementEventArgs& e) {
  if (d_windowRenderer) {
    auto renderer = static_cast<const RenderableWindowRenderer*>(d_windowRenderer);
    setRenderArea(renderer->getRenderArea());
    ++e.handled;
  }
  BaseType::onSized(e);
}

bool RenderableWindow::validateWindowRenderer(const CEGUI::WindowRenderer* renderer) const {
  return dynamic_cast<const RenderableWindowRenderer*>(renderer) != nullptr;
}

void RenderableWindow::drawSelf(const CEGUI::RenderingContext& ctx) {
  using namespace graphics::gl;
  bool redraw = false;
  if (d_needsRedraw) {
    auto gameSettings = GameSettings::get();

    if (m_renderAreaInvalid.get())
      updateRenderArea();
    
    bool aaEnabled = gameSettings->isAAEnabled();
    GLuint oldFBOBinding = 0;
    auto size = m_renderArea.getSize();
    GLint width = static_cast<GLint>(size.d_width);
    GLint height = static_cast<GLint>(size.d_height);

    if (aaEnabled) {
      glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, reinterpret_cast<GLint*>(&oldFBOBinding));

      m_fbo.bind();
      glViewport(0, 0, width, height);
    } else
      m_textureTarget->activate();

    GLint scissorBox[4]{0, 0, 0, 0};
    glGetIntegerv(GL_SCISSOR_BOX, scissorBox);

    auto clipRect = getClipRect();
    auto offset = getPixelPosition();
    auto scissorMin = clipRect.d_min - offset;
    auto scissorMax = clipRect.d_max - offset;
    glScissor(GLint(std::floor(scissorMin.d_x)), GLint(std::floor(scissorMin.d_y)),
              GLint(std::ceil(scissorMax.d_x - scissorMin.d_x)),
              GLint(std::ceil(scissorMax.d_y - scissorMin.d_y)));
    
    renderWindow();

    if (aaEnabled) {
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, oldFBOBinding);

      m_textureTarget->activate();
      m_fbo.bind(FrameBuffer::Target::Read);

      glViewport(0, 0, width, height);
      glBlitFramebuffer(0, 0, width, height,
                        0, 0, width, height,
                        GL_COLOR_BUFFER_BIT, GL_NEAREST);

      FrameBuffer::unbind(FrameBuffer::Target::Read);
    }

    glScissor(scissorBox[0], scissorBox[1], scissorBox[2], scissorBox[3]);

    m_textureTarget->deactivate();

    // Restore CEGUI standard shader
    auto renderer = static_cast<CEGUI::OpenGL3Renderer*>(CEGUI::System::getSingleton().getRenderer());
    renderer->getShaderStandard()->bind();

    redraw = true;
  }

  BaseType::drawSelf(ctx);
  if (redraw)
    m_image->Image::render(getGeometryBuffer(), m_renderArea.getPosition());
}

} // namespace gui
