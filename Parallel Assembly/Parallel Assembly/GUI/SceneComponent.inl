#pragma once
#ifndef GUI_SCENE_COMPONENT_INL
#define GUI_SCENE_COMPONENT_INL

#include "checkedWidgetLoad.h"
#include "SceneComponent.h"

#include "../Assert.h"

namespace gui {

template <typename ExpectedType>
std::enable_if_t<
  std::is_base_of<CEGUI::Window, std::decay_t<ExpectedType>>::value,
  std::decay_t<ExpectedType>*
> SceneComponent::getWidget(const char* widgetName) {
  ASSERT_MSG(root(), "SceneComponent::getWidget() called before loading");
  LOG_SEVERITY(logger::get(), Debug)
    << "SceneComponent \"" << name() << "\" loading child widget \"" << widgetName << '\"';
  return checkedWidgetLoad<ExpectedType>(root(), name(), widgetName);
}

} // namespace gui

#endif // GUI_SCENE_COMPONENT_INL
