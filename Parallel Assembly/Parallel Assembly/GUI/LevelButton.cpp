#include "LevelButton.h"

#include <CEGUI/RenderEffectManager.h>
#include <CEGUI/RenderingContext.h>
#include <CEGUI/GeometryBuffer.h>
#include <CEGUI/BasicImage.h>
#include <CEGUI/ImageManager.h>

#include "../Graphics/ShaderManager.inl"
#include "../Graphics/LevelButtonShaderProgram.h"

#include "../Assert.h"

namespace gui {

const CEGUI::String LevelButton::WidgetTypeName("PAGUI/LevelButton");
const CEGUI::String LevelButton::EventNamespace("LevelButton");

const CEGUI::String LevelButton::EventNumTextChanged("EventNumTextChanged");
const CEGUI::String LevelButton::EventImageChanged("EventImageChanged");

const CEGUI::String LevelButton::CheckMarkName("__auto_checkmark__");

const CEGUI::String LevelButtonRenderEffect::EffectName("LevelButtonRenderEffect");

LevelButton::LevelButton(const CEGUI::String& type, const CEGUI::String& name)
  : RadioButton(type, name)
  , m_numText("00")
  , m_image(getFallbackImage())
  , m_checkMark(nullptr)
  , m_geometryBuffer(nullptr)
  , m_effect(nullptr)
{
  addProperties();

  setMouseInputPropagationEnabled(true);

  auto renderer = CEGUI::System::getSingleton().getRenderer();
  auto effectManager = CEGUI::RenderEffectManager::getSingletonPtr();

  try {
    m_geometryBuffer = &renderer->createGeometryBuffer();
    m_effect = &effectManager->create(LevelButtonRenderEffect::EffectName, this);

    m_geometryBuffer->setRenderEffect(m_effect);
  } catch (...) {
    if (m_geometryBuffer)
      renderer->destroyGeometryBuffer(*m_geometryBuffer);
    if (m_effect)
      effectManager->destroy(*m_effect);

    throw;
  }
}

LevelButton::~LevelButton() {
  ASSERT(m_geometryBuffer); 
  ASSERT(m_effect);

  auto renderer = CEGUI::System::getSingleton().getRenderer();
  auto effectManager = CEGUI::RenderEffectManager::getSingletonPtr();

  renderer->destroyGeometryBuffer(*m_geometryBuffer);
  effectManager->destroy(*m_effect);
}

void LevelButton::initialiseComponents() {
  BaseType::initialiseComponents();

  m_checkMark = getChild(CheckMarkName);
}

const CEGUI::Image* LevelButton::getFallbackImage() {
  return &CEGUI::ImageManager::getSingleton().get("LevelButton-Fallback/Image");
}

WidgetPtr<LevelButton> LevelButton::create(const CEGUI::String& name) {
  return checkedCreateWidget<LevelButton>("PAGUI/LevelButton", name);
}

void LevelButton::addProperties() {
  const CEGUI::String& propertyOrigin = WidgetTypeName;

  CEGUI_DEFINE_PROPERTY(
    LevelButton, CEGUI::String, "NumText", "Level number text area contents",
    &LevelButton::setNumText, &LevelButton::getNumText, "00");

  CEGUI_DEFINE_PROPERTY(
    LevelButton, CEGUI::Image*, "Image", "Level image",
    &LevelButton::setImage, &LevelButton::getImage, getFallbackImage());
}

void LevelButton::onNumTextChanged(CEGUI::WindowEventArgs& e) {
  fireEvent(EventNumTextChanged, e, EventNamespace);
}
void LevelButton::onImageChanged(CEGUI::WindowEventArgs& e) {
  fireEvent(EventImageChanged, e, EventNamespace);
}

void LevelButton::drawSelf(const CEGUI::RenderingContext& ctx) {
  const bool needsRedraw = d_needsRedraw;
  BaseType::drawSelf(ctx);

  if (!m_image) {
    std::ostringstream ss;
    ss << "LevelButton " << getName() << " with text " << getText() << " doesn't have an image set";
    throw MAKE_EXCEPTION(ss.str());
  }
  ASSERT(m_geometryBuffer);

  if (needsRedraw) {
    m_geometryBuffer->reset();

    m_image->render(*m_geometryBuffer, getPixelPosition() - ctx.offset);
  }

  ctx.surface->addGeometryBuffer(ctx.queue, *m_geometryBuffer);
}

void LevelButton::setNumText(const CEGUI::String& numText) {
  if (m_numText != numText) {
    m_numText = numText;

    CEGUI::WindowEventArgs args(this);
    onNumTextChanged(args);

    invalidate();
  }
}
const CEGUI::String& LevelButton::getNumText() const {
  return m_numText;
}

void LevelButton::setImage(const CEGUI::Image* image) {
  if (m_image != image) {
    if (image)
      m_image = image;
    else {
      auto fallback = getFallbackImage();
      // Skip event trigger if image is already set to fallback
      if (m_image == fallback)
        return;
      m_image = fallback;
    }

    CEGUI::WindowEventArgs args(this);
    onImageChanged(args);

    invalidate();
  }
}

const CEGUI::Image* LevelButton::getImage() const {
  return m_image;
}

void LevelButton::setCheckMarkState(bool enabled) {
  ASSERT(m_checkMark);
  m_checkMark->setVisible(enabled);
}

void LevelButton::notifyScreenAreaChanged(bool recursive) {
  BaseType::notifyScreenAreaChanged(recursive);

  CEGUI::RenderingContext ctx;
  getRenderingContext(ctx);

  auto position = getUnclippedOuterRect().get().d_min - ctx.offset;
  // Button owns its rendering surface; no offset required
  if (ctx.surface->isRenderingWindow() && ctx.owner == this) {
    m_geometryBuffer->setTranslation({0.f, 0.f, 0.f});
    m_geometryBuffer->setClippingRegion({{0.f, 0.f}, d_pixelSize});
  } else {
    m_geometryBuffer->setTranslation(
      CEGUI::Vector3f(position.d_x, position.d_y, 0.0f));

    CEGUI::Rectf clipRect{getOuterRectClipper()};

    if (clipRect.getWidth() != 0.0f && clipRect.getHeight() != 0.0f)
      clipRect.offset({-ctx.offset.d_x, -ctx.offset.d_y});

    m_geometryBuffer->setClippingRegion(clipRect);

    invalidate();
  }
}

graphics::LevelButtonShaderProgram* LevelButtonRenderEffect::shader{nullptr};
const CEGUI::OpenGLTexture* LevelButtonRenderEffect::maskTexture{nullptr};

void LevelButtonRenderEffect::setupShader(graphics::ShaderManager& shaderManager) {
  shader = shaderManager.createShader<graphics::LevelButtonShaderProgram>();
  ASSERT(shader);
  shader->initialize();

  auto renderer = CEGUI::System::getSingleton().getRenderer();
  maskTexture = static_cast<CEGUI::OpenGLTexture*>(&renderer->getTexture("LevelButton-Mask"));
}

LevelButtonRenderEffect::LevelButtonRenderEffect(CEGUI::Window* window)
  : RenderEffect()
  , m_button(dynamic_cast<LevelButton*>(window))
{
  if (!m_button)
    throw MAKE_EXCEPTION("LevelButtonRenderEffect is only compatible with LevelButton");

  ASSERT(shader);
}

int LevelButtonRenderEffect::getPassCount() const {
  return 1;
}
void LevelButtonRenderEffect::performPreRenderFunctions(const int /*pass*/) {
  shader->preRender();

  // Bind mask texture to GL_TEXTURE1
  shader->getMaskTexUnit().setActive();
  glBindTexture(GL_TEXTURE_2D, maskTexture->getOpenGLTexture());
  
  // Set active texture back to GL_TEXTURE0
  glActiveTexture(GL_TEXTURE0);
}
void LevelButtonRenderEffect::performPostRenderFunctions() {
  shader->postRender();
}
bool LevelButtonRenderEffect::realiseGeometry(CEGUI::RenderingWindow&,
                                              CEGUI::GeometryBuffer&)
{
  return false;
}
bool LevelButtonRenderEffect::update(const float /*elapsed*/,
                                     CEGUI::RenderingWindow&)
{
  return false;
}

} // namespace gui
