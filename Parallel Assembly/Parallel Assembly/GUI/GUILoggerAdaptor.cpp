#include "GUILoggerAdaptor.h"

#include <boost/log/sources/severity_logger.hpp>

#include "../Assert.h"

namespace gui {

GUILoggerAdaptor::GUILoggerAdaptor()
  : Logger()
  , m_logger()
{
  logging::setModuleName(m_logger, "CEGUI");
}

logging::Severity convertLoggingLevel(CEGUI::LoggingLevel level) {
  switch (level) {
  case CEGUI::LoggingLevel::Errors:
    return logging::Severity::Error;
  case CEGUI::LoggingLevel::Warnings:
    return logging::Severity::Warning;
  case CEGUI::LoggingLevel::Standard:
    return logging::Severity::Normal;
  case CEGUI::LoggingLevel::Informative:
    return logging::Severity::Info;
  case CEGUI::LoggingLevel::Insane:
    return logging::Severity::Debug;
  default:
    ASSERT_MSG(false, "Unhandled CEGUI logging level");
    return logging::Severity::Fatal;
  }
}

void GUILoggerAdaptor::logEvent(const CEGUI::String& message,
                                CEGUI::LoggingLevel level)
{
  if (getLoggingLevel() >= level)
    BOOST_LOG_SEV(m_logger, convertLoggingLevel(level)) << message;
}
void GUILoggerAdaptor::setLogFilename(const CEGUI::String&, bool) {}

} // namespace gui
