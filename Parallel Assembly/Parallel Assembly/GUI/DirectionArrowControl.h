#pragma once
#ifndef GUI_DIRECTION_ARROW_CONTROL_H
#define GUI_DIRECTION_ARROW_CONTROL_H

#include <CEGUI/widgets/DefaultWindow.h>

#undef min
#undef max

namespace gui {

class DirectionArrowControlButton;

class DirectionArrowControl : public CEGUI::DefaultWindow {
  using BaseType = DefaultWindow;

  std::size_t m_workerSelection;

  DirectionArrowControlButton* m_leftArrowButton;
  DirectionArrowControlButton* m_upArrowButton;
  DirectionArrowControlButton* m_rightArrowButton;
  DirectionArrowControlButton* m_downArrowButton;

public:
  static const CEGUI::String WidgetTypeName;
  static const CEGUI::String EventNamespace;

  static const CEGUI::String LeftArrowButtonName;
  static const CEGUI::String UpArrowButtonName;
  static const CEGUI::String RightArrowButtonName;
  static const CEGUI::String DownArrowButtonName;

  DirectionArrowControl(const CEGUI::String& type, const CEGUI::String& name);
  virtual ~DirectionArrowControl() override = default;

  virtual void initialiseComponents() override;

  DirectionArrowControlButton* getLeftArrowButton() const;
  DirectionArrowControlButton* getUpArrowButton() const;
  DirectionArrowControlButton* getRightArrowButton() const;
  DirectionArrowControlButton* getDownArrowButton() const;

  void setWorkerSelection(std::size_t worker);
  std::size_t getWorkerSelection() const;
};

} // namespace gui

#endif // GUI_DIRECTION_ARROW_CONTROL_H
