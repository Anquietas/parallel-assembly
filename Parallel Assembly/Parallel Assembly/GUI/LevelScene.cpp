#include "LevelScene.h"

#include "../Exception.h"

namespace gui {

LevelScene::LevelScene(const std::string& name_, const CEGUI::String& layoutFileName_)
  : Scene(name_, layoutFileName_)
{}

void LevelScene::initializeComponents() {
  if (!(level() && solution()))
    throw MAKE_EXCEPTION("Tried to initialize LevelScene before setting level and solution");
  level()->loadLevel();
  solution()->loadSolution();
}
void LevelScene::postUnloadCleanup() {
  solution()->unloadSolution();
  level()->unloadLevel();
  setLevelAndSolution(nullptr, nullptr);
}

void LevelScene::setLevelAndSolution(
  game::Level& level,
  game::LevelSolution& solution)
{
  setLevelAndSolution(&level, &solution);
}

} // namespace gui
