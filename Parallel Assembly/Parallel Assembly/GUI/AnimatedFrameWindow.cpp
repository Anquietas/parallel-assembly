#include "AnimatedFrameWindow.h"

#include <CEGUI/AnimationManager.h>
#include <CEGUI/AnimationInstance.h>

namespace gui {

const CEGUI::String AnimatedFrameWindow::WidgetTypeName("PAGUI/AnimatedFrameWindow");
const CEGUI::String AnimatedFrameWindow::EventNamespace("AnimatedFrameWindow");

const CEGUI::String AnimatedFrameWindow::EventBeginShow("BeginShow");
const CEGUI::String AnimatedFrameWindow::EventBeginHide("BeginHide");

AnimatedFrameWindow::AnimatedFrameWindow(const CEGUI::String& type, const CEGUI::String& name)
  : FrameWindow(type, name)
  , m_showInstance(nullptr)
  , m_hideInstance(nullptr)
{
  auto& animationManager = CEGUI::AnimationManager::getSingleton();
  try {
    m_showInstance = animationManager.instantiateAnimation("AnimatedFrameWindowFadeIn");
    m_hideInstance = animationManager.instantiateAnimation("AnimatedFrameWindowFadeOut");

    m_showInstance->setTargetWindow(this);
    m_hideInstance->setTargetWindow(this);
  } catch (...) {
    if (m_showInstance)
      animationManager.destroyAnimationInstance(m_showInstance);
    if (m_hideInstance)
      animationManager.destroyAnimationInstance(m_hideInstance);
    throw;
  }
}

AnimatedFrameWindow::~AnimatedFrameWindow() {
  auto& animationManager = CEGUI::AnimationManager::getSingleton();
  if (m_showInstance)
    animationManager.destroyAnimationInstance(m_showInstance);
  if (m_hideInstance)
    animationManager.destroyAnimationInstance(m_hideInstance);
}

void AnimatedFrameWindow::onBeginShow(CEGUI::WindowEventArgs& e) {
  fireEvent(EventBeginShow, e, EventNamespace);
}
void AnimatedFrameWindow::onBeginHide(CEGUI::WindowEventArgs& e) {
  fireEvent(EventBeginHide, e, EventNamespace);
}

void AnimatedFrameWindow::beginShow() {
  FrameWindow::show();
  CEGUI::WindowEventArgs args(this);
  onBeginShow(args);
}
void AnimatedFrameWindow::beginHide() {
  CEGUI::WindowEventArgs args(this);
  onBeginHide(args);
  deactivate();
}

} // namespace gui
