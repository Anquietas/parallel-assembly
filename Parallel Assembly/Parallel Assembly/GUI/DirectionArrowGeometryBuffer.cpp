#include "DirectionArrowGeometryBuffer.h"

#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>
#include <CEGUI/RendererModules/OpenGL/Shader.h>
#include <CEGUI/System.h>

#include "../Game/Worker.h"

#include "../Graphics/DirectionArrowShaderProgram.h"
#include "../Graphics/LevelGridRenderConstants.h"
#include "../Graphics/ShaderManager.h"

#include "../Graphics/OpenGL/checkError.h"
#include "../Graphics/OpenGL/OpenGLIncludes.h"
#include "../Graphics/OpenGL/VertexBuffer.inl"

#include "../Assert.h"

namespace gui {

graphics::DirectionArrowShaderProgram* DirectionArrowGeometryBuffer::directionArrowShader = nullptr;

DirectionArrowGeometryBuffer::DirectionArrowGeometryBuffer()
  : GeometryBuffer()
  , m_direction(game::Direction::Left)
  , m_workerSelection(0u)
  , m_screenSize()
  , m_renderPosition()
  , m_gridCellSize(1.f)
  , m_vao()
  , m_meshVBO()
  , m_dataVBO() {
  using namespace graphics::constants;

  ASSERT_MSG(directionArrowShader,
             "DirectionArrowGeometryBuffer constructed before calling setupShader()");

  auto& vertexPosAttribID = directionArrowShader->vertexPosAttribID();
  auto& texCoordsAttribID = directionArrowShader->texCoordsAttribID();
  auto& offsetAttribID = directionArrowShader->offsetAttribID();
  auto& directionAttribID = directionArrowShader->directionAttribID();
  auto& stateAttribID = directionArrowShader->stateAttribID();

  m_vao.bind();
  m_vao.enableAttribute(vertexPosAttribID);
  m_vao.enableAttribute(texCoordsAttribID);
  m_vao.enableAttribute(offsetAttribID);
  m_vao.enableAttribute(directionAttribID);
  m_vao.enableAttribute(stateAttribID);
  m_vao.setAttributeDivisor(offsetAttribID, 1u);
  m_vao.setAttributeDivisor(directionAttribID, 1u);
  m_vao.setAttributeDivisor(stateAttribID, 1u);

  graphics::gl::VertexBuffer::InterleavedFormat meshBufferFormat;
  meshBufferFormat.addAttribute<glm::vec2>(
    vertexPosAttribID, false, sizeof(DirectionArrowGLVertex), offsetof(DirectionArrowGLVertex, vertexPos));
  meshBufferFormat.addAttribute<glm::vec2>(
    texCoordsAttribID, false, sizeof(DirectionArrowGLVertex), offsetof(DirectionArrowGLVertex, texCoords));

  m_meshVBO.bind();
  m_meshVBO.bindToAttributes(meshBufferFormat);
  m_meshVBO.allocate(directionArrowMesh.size(), directionArrowMesh.data());

  CHECK_GL_ERROR();

  graphics::gl::VertexBuffer::InterleavedFormat dataBufferFormat;
  dataBufferFormat.addAttribute<glm::vec2>(
    offsetAttribID, false, sizeof(DirectionArrowGLData), offsetof(DirectionArrowGLData, offset));
  dataBufferFormat.addAttribute<graphics::gl::int_t>(
    directionAttribID, false, sizeof(DirectionArrowGLData), offsetof(DirectionArrowGLData, direction));
  dataBufferFormat.addAttribute<graphics::gl::int_t>(
    stateAttribID, false, sizeof(DirectionArrowGLData), offsetof(DirectionArrowGLData, state));

  DirectionArrowGLData defaults{{0.f, 0.f}, 0, 0};

  m_dataVBO.bind();
  m_dataVBO.bindToAttributes(dataBufferFormat);
  m_dataVBO.allocate(1, &defaults, graphics::gl::VertexBuffer::Usage::DynamicDraw);

  CHECK_GL_ERROR();

  graphics::gl::VertexBuffer::unbind();
  graphics::gl::VertexArrayObject::unbind();
}

void DirectionArrowGeometryBuffer::setupShader(graphics::ShaderManager& shaderManager) {
  ASSERT_MSG(!directionArrowShader,
             "DirectionArrowGeometryBuffer::setupShader() called after shader was already initialized");

  using namespace std::string_literals;

  if (auto shader = shaderManager.lookupShader("DirectionArrowShader"s)) {
    directionArrowShader = static_cast<graphics::DirectionArrowShaderProgram*>(shader);
  } else {
    directionArrowShader = shaderManager.createShader<graphics::DirectionArrowShaderProgram>();
    directionArrowShader->initialize();
  }

  ASSERT_MSG(directionArrowShader,
             "Shader not initialized properly after calling DirectionArrowGeometryBuffer::setupShader()");
}

void DirectionArrowGeometryBuffer::draw() const {
  using namespace graphics::constants;

  ASSERT(m_workerSelection < game::Worker::maxWorkers);

  glDisable(GL_STENCIL_TEST);

  m_vao.bind();

  directionArrowShader->preRender();

  auto mvpMatrix = glm::ortho(0.f, m_screenSize.x, m_screenSize.y, 0.f);
  mvpMatrix = glm::translate(mvpMatrix, glm::vec3{m_renderPosition, 0.f});

  directionArrowShader->setMVPMatrix(mvpMatrix);
  directionArrowShader->setGridCellSize(m_gridCellSize);

  directionArrowShader->setArrowColour(directionArrowColours[m_workerSelection]);
  directionArrowShader->setBorderColour(directionArrowBorderColours[m_workerSelection]);

  glDrawArraysInstanced(GL_TRIANGLES, 0, graphics::gl::sizei_t(directionArrowMesh.size()), 1);

  directionArrowShader->postRender();

  graphics::gl::VertexArrayObject::unbind();

  auto renderer = static_cast<CEGUI::OpenGL3Renderer*>(CEGUI::System::getSingleton().getRenderer());
  renderer->getShaderStandard()->bind();
}

void DirectionArrowGeometryBuffer::updateRenderData() {
  using namespace graphics::constants;

  m_dataVBO.bind();

  DirectionArrowGLData arrowData{{0, 0}, 0, 0};

  switch (m_direction) {
  case game::Direction::Left:
    arrowData.offset = glm::vec2{arrowHeight * 1.5f, arrowWidth};
    arrowData.direction = 0;
    break;
  case game::Direction::Right:
    arrowData.offset = glm::vec2{arrowHeight / 2.f, 0.f};
    arrowData.direction = 1;
    break;
  case game::Direction::Up:
    arrowData.offset = glm::vec2{0.f, arrowHeight * 1.5f};
    arrowData.direction = 2;
    break;
  case game::Direction::Down:
    arrowData.offset = glm::vec2{arrowWidth, arrowHeight / 2.f};
    arrowData.direction = 3;
    break;
  default:
    ASSERT(false);;
  }
  m_dataVBO.update(0, 1, &arrowData);

  graphics::gl::VertexBuffer::unbind();
}

void DirectionArrowGeometryBuffer::setDirection(game::Direction direction) {
  ASSERT(direction != game::Direction::None);
  m_direction = direction;
}
game::Direction DirectionArrowGeometryBuffer::getDirection() const {
  return m_direction;
}

void DirectionArrowGeometryBuffer::setWorkerSelection(std::size_t workerSelection) {
  ASSERT(workerSelection < game::Worker::maxWorkers);
  m_workerSelection = workerSelection;
}
std::size_t DirectionArrowGeometryBuffer::getWorkerSelection() const {
  return m_workerSelection;
}

void DirectionArrowGeometryBuffer::setRenderTargetArea(const CEGUI::Rectf& renderTargetArea) {
  auto screenSize = renderTargetArea.getSize();
  m_screenSize = glm::vec2{screenSize.d_width, screenSize.d_height};
}

void DirectionArrowGeometryBuffer::setRenderArea(const CEGUI::Rectf& renderArea) {
  using namespace graphics::constants;

  auto renderPosition = renderArea.getPosition();
  m_renderPosition = glm::vec2{renderPosition.d_x, renderPosition.d_y};

  m_gridCellSize = renderArea.getWidth() / arrowWidth;
}

} // namespace gui
