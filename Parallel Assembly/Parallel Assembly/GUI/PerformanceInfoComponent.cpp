#include "PerformanceInfoComponent.h"

#include <cmath>
#include <iomanip>
#include <sstream>
#include <type_traits>

#include <CEGUI/widgets/DefaultWindow.h>
#include <CEGUI/widgets/PushButton.h>

#include "../Graphics/Renderer.h"

#include "AnimatedFrameWindow.h"
#include "SceneComponent.inl"
#include "stringConvert.h"

namespace gui {

PerformanceInfoComponent::PerformanceInfoComponent(graphics::Renderer& renderer)
  : SceneComponent("PerformanceInfo", "performanceInfo.layout", true, true)
  , m_renderer(&renderer)
  , m_fpsAccumulator(acc::tag::rolling_window::window_size = windowSize)
  , m_primitivesGenAccumulator(acc::tag::rolling_window::window_size = windowSize)
  , m_samplesPassedAccumulator(acc::tag::rolling_window::window_size = windowSize)
  , m_gpuTimeAccumulator(acc::tag::rolling_window::window_size = windowSize)
  , m_window(nullptr)
  , m_currentFPSLabel(nullptr)
  , m_currentPrimitivesGenLabel(nullptr)
  , m_currentSamplesPassedLabel(nullptr)
  , m_currentGPUTimeLabel(nullptr)
  , m_minFPSLabel(nullptr)
  , m_minPrimitivesGenLabel(nullptr)
  , m_minSamplesPassedLabel(nullptr)
  , m_minGPUTimeLabel(nullptr)
  , m_maxFPSLabel(nullptr)
  , m_maxPrimitivesGenLabel(nullptr)
  , m_maxSamplesPassedLabel(nullptr)
  , m_maxGPUTimeLabel(nullptr)
  , m_meanFPSLabel(nullptr)
  , m_meanPrimitivesGenLabel(nullptr)
  , m_meanSamplesPassedLabel(nullptr)
  , m_meanGPUTimeLabel(nullptr)
  , m_stdDevFPSLabel(nullptr)
  , m_stdDevPrimitivesGenLabel(nullptr)
  , m_stdDevSamplesPassedLabel(nullptr)
  , m_stdDevGPUTimeLabel(nullptr)
{
  load();
}

template <typename T>
std::enable_if_t<std::is_floating_point<T>::value, std::string>
formatFP(T value, int decimalPlaces = 2) {
  std::ostringstream ss;
  ss << std::fixed << std::setprecision(decimalPlaces) << value;
  return ss.str();
}

void PerformanceInfoComponent::initializeComponents() {
  m_window = dynamic_cast<AnimatedFrameWindow*>(root());
  if (!m_window) {
    std::ostringstream ss;
    ss << name() << " failed to load: SceneComponent root is not of type " << AnimatedFrameWindow::WidgetTypeName;
    throw MAKE_EXCEPTION(ss.str());
  }

  auto closeButton = getWidget<CEGUI::PushButton>("__auto_closebutton__");
  m_currentFPSLabel = getWidget<CEGUI::DefaultWindow>("CurrentFPSLabel");
  m_currentPrimitivesGenLabel = getWidget<CEGUI::DefaultWindow>("CurrentPrimitivesGenLabel");
  m_currentSamplesPassedLabel = getWidget<CEGUI::DefaultWindow>("CurrentSamplesPassedLabel");
  m_currentGPUTimeLabel = getWidget<CEGUI::DefaultWindow>("CurrentGPUTimeLabel");

  m_minFPSLabel = getWidget<CEGUI::DefaultWindow>("MinFPSLabel");
  m_minPrimitivesGenLabel = getWidget<CEGUI::DefaultWindow>("MinPrimitivesGenLabel");
  m_minSamplesPassedLabel = getWidget<CEGUI::DefaultWindow>("MinSamplesPassedLabel");
  m_minGPUTimeLabel = getWidget<CEGUI::DefaultWindow>("MinGPUTimeLabel");

  m_maxFPSLabel = getWidget<CEGUI::DefaultWindow>("MaxFPSLabel");
  m_maxPrimitivesGenLabel = getWidget<CEGUI::DefaultWindow>("MaxPrimitivesGenLabel");
  m_maxSamplesPassedLabel = getWidget<CEGUI::DefaultWindow>("MaxSamplesPassedLabel");
  m_maxGPUTimeLabel = getWidget<CEGUI::DefaultWindow>("MaxGPUTimeLabel");

  m_meanFPSLabel = getWidget<CEGUI::DefaultWindow>("MeanFPSLabel");
  m_meanPrimitivesGenLabel = getWidget<CEGUI::DefaultWindow>("MeanPrimitivesGenLabel");
  m_meanSamplesPassedLabel = getWidget<CEGUI::DefaultWindow>("MeanSamplesPassedLabel");
  m_meanGPUTimeLabel = getWidget<CEGUI::DefaultWindow>("MeanGPUTimeLabel");

  m_stdDevFPSLabel = getWidget<CEGUI::DefaultWindow>("StdDevFPSLabel");
  m_stdDevPrimitivesGenLabel = getWidget<CEGUI::DefaultWindow>("StdDevPrimitivesGenLabel");
  m_stdDevSamplesPassedLabel = getWidget<CEGUI::DefaultWindow>("StdDevSamplesPassedLabel");
  m_stdDevGPUTimeLabel = getWidget<CEGUI::DefaultWindow>("StdDevGPUTimeLabel");

  auto resetFPSButton = getWidget<CEGUI::PushButton>("ResetFPSButton");
  auto resetPrimitivesGenButton = getWidget<CEGUI::PushButton>("ResetPrimitivesGenButton");
  auto resetSamplesPassedButton = getWidget<CEGUI::PushButton>("ResetSamplesPassedButton");
  auto resetGPUTimeButton = getWidget<CEGUI::PushButton>("ResetGPUTimeButton");

  m_window->subscribeEvent(
    AnimatedFrameWindow::EventUpdated, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        const float currentFPS = m_renderer->computeFPS();
        const auto currentPrimGen = m_renderer->primitivesGenerated();
        const auto currentSamples = m_renderer->samplesPassed();
        const auto currentGPUTime = m_renderer->timeElapsed();

        m_fpsAccumulator(currentFPS);
        m_primitivesGenAccumulator(float(currentPrimGen));
        m_samplesPassedAccumulator(double(currentSamples));
        m_gpuTimeAccumulator(double(currentGPUTime));

        const auto minFPS = acc::min(m_fpsAccumulator);
        const auto minPrimGen = acc::min(m_primitivesGenAccumulator);
        const auto minSamples = acc::min(m_samplesPassedAccumulator);
        const auto minGPUTime = acc::min(m_gpuTimeAccumulator);

        const auto maxFPS = acc::max(m_fpsAccumulator);
        const auto maxPrimGen = acc::max(m_primitivesGenAccumulator);
        const auto maxSamples = acc::max(m_samplesPassedAccumulator);
        const auto maxGPUTime = acc::max(m_gpuTimeAccumulator);

        const auto meanFPS = acc::rolling_mean(m_fpsAccumulator);
        const auto meanPrimGen = acc::rolling_mean(m_primitivesGenAccumulator);
        const auto meanSamples = acc::rolling_mean(m_samplesPassedAccumulator);
        const auto meanGPUTime = acc::rolling_mean(m_gpuTimeAccumulator);

        const auto stdDevFPS = std::sqrt(acc::rolling_variance(m_fpsAccumulator));
        const auto stdDevPrimGen = std::sqrt(acc::rolling_variance(m_primitivesGenAccumulator));
        const auto stdDevSamples = std::sqrt(acc::rolling_variance(m_samplesPassedAccumulator));
        const auto stdDevGPUTime = std::sqrt(acc::rolling_variance(m_gpuTimeAccumulator));

        m_currentFPSLabel->setText(fromStdString(formatFP(currentFPS)));
        m_currentPrimitivesGenLabel->setText(fromStdString(std::to_string(currentPrimGen)));
        m_currentSamplesPassedLabel->setText(fromStdString(std::to_string(currentSamples)));
        m_currentGPUTimeLabel->setText(fromStdString(std::to_string(currentGPUTime)));

        m_minFPSLabel->setText(fromStdString(formatFP(minFPS)));
        m_minPrimitivesGenLabel->setText(fromStdString(formatFP(minPrimGen, 0)));
        m_minSamplesPassedLabel->setText(fromStdString(formatFP(minSamples, 0)));
        m_minGPUTimeLabel->setText(fromStdString(formatFP(minGPUTime, 0)));

        m_maxFPSLabel->setText(fromStdString(formatFP(maxFPS)));
        m_maxPrimitivesGenLabel->setText(fromStdString(formatFP(maxPrimGen, 0)));
        m_maxSamplesPassedLabel->setText(fromStdString(formatFP(maxSamples, 0)));
        m_maxGPUTimeLabel->setText(fromStdString(formatFP(maxGPUTime, 0)));

        m_meanFPSLabel->setText(fromStdString(formatFP(meanFPS)));
        m_meanPrimitivesGenLabel->setText(fromStdString(formatFP(meanPrimGen)));
        m_meanSamplesPassedLabel->setText(fromStdString(formatFP(meanSamples)));
        m_meanGPUTimeLabel->setText(fromStdString(formatFP(meanGPUTime)));

        m_stdDevFPSLabel->setText(fromStdString(formatFP(stdDevFPS)));
        m_stdDevPrimitivesGenLabel->setText(fromStdString(formatFP(stdDevPrimGen)));
        m_stdDevSamplesPassedLabel->setText(fromStdString(formatFP(stdDevSamples)));
        m_stdDevGPUTimeLabel->setText(fromStdString(formatFP(stdDevGPUTime)));

        return true;
      }
    ));
  m_window->subscribeEvent(
    AnimatedFrameWindow::EventHidden, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        detachFromTarget();
        return true;
      }
    ));
  closeButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        hide();
        return true;
      }
    ));

  resetFPSButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        resetFPSAccumulator();
        return true;
      }
    ));
  resetPrimitivesGenButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        resetPrimitivesGenAccumulator();
        return true;
      }
    ));
  resetSamplesPassedButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        resetSamplesPassedAccumulator();
        return true;
      }
    ));
  resetGPUTimeButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        resetGPUTimeAccumulator();
        return true;
      }
    ));
}

void PerformanceInfoComponent::onAttach() {
  m_renderer->enablePerfTracking();
}
void PerformanceInfoComponent::onDetach() {
  m_renderer->disablePerfTracking();
  resetFPSAccumulator();
  resetPrimitivesGenAccumulator();
  resetSamplesPassedAccumulator();
  resetGPUTimeAccumulator();
}

void PerformanceInfoComponent::show() {
  if (!isAttached())
    attachToActiveScene();
  m_window->show();
  m_window->activate();
}
void PerformanceInfoComponent::hide() {
  m_window->hide();
}

void PerformanceInfoComponent::resetFPSAccumulator() {
  m_fpsAccumulator = AccumulatorSet<float>{
    acc::tag::rolling_window::window_size = windowSize};
}
void PerformanceInfoComponent::resetPrimitivesGenAccumulator() {
  m_primitivesGenAccumulator = AccumulatorSet<float>{
    acc::tag::rolling_window::window_size = windowSize};
}
void PerformanceInfoComponent::resetSamplesPassedAccumulator() {
  m_samplesPassedAccumulator = AccumulatorSet<double>{
    acc::tag::rolling_window::window_size = windowSize};
}
void PerformanceInfoComponent::resetGPUTimeAccumulator() {
  m_gpuTimeAccumulator = AccumulatorSet<double>{
    acc::tag::rolling_window::window_size = windowSize};
}

} // namespace gui
