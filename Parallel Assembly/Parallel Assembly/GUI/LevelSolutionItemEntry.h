#pragma once
#ifndef GUI_LEVEL_SOLUTION_ITEM_ENTRY_H
#define GUI_LEVEL_SOLUTION_ITEM_ENTRY_H

#include <CEGUI/widgets/ItemEntry.h>

#include "checkedCreateWidget.h"
namespace gui {

class LevelSolutionItemEntry : public CEGUI::ItemEntry {
  CEGUI::String m_stat1Text;
  CEGUI::String m_stat2Text;
  CEGUI::String m_stat3Text;

  void addProperties();

  virtual void onStat1TextChanged(CEGUI::WindowEventArgs& e);
  virtual void onStat2TextChanged(CEGUI::WindowEventArgs& e);
  virtual void onStat3TextChanged(CEGUI::WindowEventArgs& e);
public:
  static const CEGUI::String WidgetTypeName;
  static const CEGUI::String EventNamespace;

  static const CEGUI::String EventStat1TextChanged;
  static const CEGUI::String EventStat2TextChanged;
  static const CEGUI::String EventStat3TextChanged;

  LevelSolutionItemEntry(const CEGUI::String& type,
                         const CEGUI::String& name);
  virtual ~LevelSolutionItemEntry() override = default;

  static WidgetPtr<LevelSolutionItemEntry>
    create(const CEGUI::String& name = "");

  // Sets the text for the left-most field of the item
  // CEGUI doesn't provide move constructor
  void setStat1Text(const CEGUI::String& stat1Text);
  const CEGUI::String& getStat1Text() const;

  // Sets the text for the central field of the item
  void setStat2Text(const CEGUI::String& stat2Text);
  const CEGUI::String& getStat2Text() const;

  // Sets the text for the right-most field of the item
  void setStat3Text(const CEGUI::String& stat3Text);
  const CEGUI::String& getStat3Text() const;

  // Helper to set all 3 fields compactly
  void setStatTextFields(const CEGUI::String& stat1Text,
                         const CEGUI::String& stat2Text,
                         const CEGUI::String& stat3Text);
};

} // namespace gui

#endif // GUI_LEVEL_SOLUTION_ITEM_ENTRY_H
