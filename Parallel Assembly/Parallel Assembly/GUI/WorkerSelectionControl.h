#pragma once
#ifndef GUI_WORKER_SELECTION_CONTROL_H
#define GUI_WORKER_SELECTION_CONTROL_H

#include <CEGUI/widgets/DefaultWindow.h>

#undef min
#undef max

namespace gui {

class WorkerSelectionControl : public CEGUI::DefaultWindow {
  using BaseType = DefaultWindow;

  std::size_t m_worker;

  CEGUI::RadioButton* m_topLeftButton;
  CEGUI::RadioButton* m_topRightButton;
  CEGUI::RadioButton* m_bottomLeftButton;
  CEGUI::RadioButton* m_bottomRightButton;

  virtual void onWorkerSelectionChanged(CEGUI::WindowEventArgs& e);
public:
  static const CEGUI::String WidgetTypeName;
  static const CEGUI::String EventNamespace;

  static const CEGUI::String EventWorkerSelectionChanged;

  static const CEGUI::String TopLeftButtonName;
  static const CEGUI::String TopRightButtonName;
  static const CEGUI::String BottomLeftButtonName;
  static const CEGUI::String BottomRightButtonName;

  static const CEGUI::String ButtonColourPropertyName;
  static const CEGUI::String ButtonTextColourPropertyName;

  WorkerSelectionControl(const CEGUI::String& type, const CEGUI::String& name);
  virtual ~WorkerSelectionControl() override = default;

  virtual void initialiseComponents() override;

  void setWorkerSelection(std::size_t worker);
  std::size_t getWorkerSelection() const;

  void setNumWorkers(std::size_t numWorkers);
};

} // namespace gui

#endif // GUI_WORKER_SELECTION_CONTROL_H
