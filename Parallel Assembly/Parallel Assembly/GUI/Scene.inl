#pragma once
#ifndef GUI_SCENE_INL
#define GUI_SCENE_INL

#include "checkedWidgetLoad.h"
#include "Scene.h"

namespace gui {

template <typename ExpectedType>
std::enable_if_t<
  std::is_base_of<CEGUI::Window, std::decay_t<ExpectedType>>::value,
  std::decay_t<ExpectedType>*
> Scene::getWidget(const char* widgetName) {
  if (loaded()) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Scene \"" << name() << "\" loading child widget \"" << widgetName << '\"';
    return checkedWidgetLoad<ExpectedType>(sceneRoot(), name(), widgetName);
  } else {
    std::ostringstream ss;
    ss << "Tried to load child widget for unloaded scene \"" << name() << '\"';
    throw MAKE_EXCEPTION(ss.str());
  }
}

} // namespace gui

#endif // GUI_SCENE_INL
