#pragma once
#ifndef GUI_SCENE_H
#define GUI_SCENE_H

#include <type_traits>

#include <boost/container/small_vector.hpp>

#include <CEGUI/Window.h>

#include "../Exception.h"
#include "../Logger.h"

namespace gui {

class GUIManager;
class SceneComponent;

class Scene {
  friend class GUIManager;
  STATIC_LOGGER_DECLARE(logger);

  const std::string& m_name;
  CEGUI::String m_layoutFileName;
  CEGUI::Window* m_sceneRoot;
  bool m_active;

  using ComponentsVector = boost::container::small_vector<SceneComponent*, 8>;
  ComponentsVector m_components;

  void setActive() noexcept { m_active = true; }
  void clearActive() noexcept { m_active = false; }

protected:
  CEGUI::Window* sceneRoot() noexcept { return m_sceneRoot; }
  const CEGUI::Window* sceneRoot() const noexcept { return m_sceneRoot; }

  void setSceneRoot(CEGUI::Window* sceneRoot) noexcept { m_sceneRoot = sceneRoot; }

  virtual void initializeComponents();
  virtual void cleanupComponents();
  // For handling cleanup that must happen after the scene has been fully unloaded.
  virtual void postUnloadCleanup();

  void detachComponent(ComponentsVector::iterator componentIter);

  // Transfers all transferable components to the given scene
  void transferTransferableComponents(Scene& scene);

  // Constructor for loading a scene from a layout file (via load()/unload())
  Scene(const std::string& name_, const CEGUI::String& layoutFileName_);
public:
  Scene(const Scene&) = delete;
  Scene(Scene&&) = delete;
  virtual ~Scene();

  Scene& operator=(const Scene&) = delete;
  Scene& operator=(Scene&&) = delete;

  const std::string& name() const noexcept { return m_name; }
  const CEGUI::String& layoutFileName() const noexcept { return m_layoutFileName; }

  bool loaded() const noexcept { return m_sceneRoot != nullptr; }
  // Loads the scene from its layout file if it isn't already loaded and m_layoutFileName is nonempty
  void load();
  // Unloads the scene if it is loaded after calling cleanupComponents; this will destroy child widgets
  void unload();

  template <typename ExpectedType>
  std::enable_if_t<
    std::is_base_of<CEGUI::Window, std::decay_t<ExpectedType>>::value,
    std::decay_t<ExpectedType>*
  > getWidget(const char* widgetName);

  bool active() const noexcept { return m_active; }

  // Attaches the given scene component to the scene
  // - If the component is already attached, throws an exception
  void attach(SceneComponent& component);
  // Detaches the given scene component from the scene
  // - If the component is not attached, throws an exception
  void detach(SceneComponent& component);

  // Attaches the given window to the root window of the scene
  // - Useful for popup windows
  // - If the root window is null, throws an exception
  void attachWindow(CEGUI::Window& window);
  // Detatches the given window from the root window of the scene
  // - If the root window is null, throws an exception
  // - If the widget is not attached, throws an exception
  void detachWindow(CEGUI::Window& window);

  // Switch to the scene with the given name, if it exists.
  // - If a scene with the given name does not exist, throws an exception.
  void switchToScene(const std::string& sceneName);
  // Switch to the given scene
  void switchToScene(Scene& scene);
  // Switch to the previous scene in the scene stack, or to the main menu if there isn't one.
  void switchToPreviousScene();
};

} // namespace gui

#endif // GUI_SCENE
