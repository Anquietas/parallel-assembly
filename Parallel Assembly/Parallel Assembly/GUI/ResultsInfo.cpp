#include "ResultsInfo.h"

#include "../Exception.h"

namespace gui {

const CEGUI::String ResultsInfo::WidgetTypeName("PAGUI/ResultsInfo");
const CEGUI::String ResultsInfo::EventNamespace("ResultsInfo");

ResultsInfo::ResultsInfo(const CEGUI::String& type, const CEGUI::String& name)
  : RenderableWindow(type, name)
{}

ResultsInfo::~ResultsInfo() = default;

void ResultsInfo::renderWindow() {
  glClearColor(1.f, 0.f, 0.f, 1.f);
  glClear(GL_COLOR_BUFFER_BIT |
          GL_DEPTH_BUFFER_BIT);
}

} // namespace gui
