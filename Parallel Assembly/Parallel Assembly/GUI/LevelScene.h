#pragma once
#ifndef GUI_LEVEL_SCENE_H
#define GUI_LEVEL_SCENE_H

#include "Scene.h"

#include "../Game/Level.h"
#include "../Game/LevelSolution.h"

namespace gui {

class LevelScene : public Scene {
  // Sets internal storage in derived classes for level and solution
  // (expected to use pointer types derived from Level and LevelSolution)
  // Implementation should allow level = nullptr, solution = nullptr for resetting
  virtual void setLevelAndSolution(
    game::Level* level, game::LevelSolution* solution) = 0;
protected:
  // Validates level and solution non-null; set before scene load.
  // Derived implementations should call this before performing their own initialization
  virtual void initializeComponents() override;
  // Resets level and solution to null
  virtual void postUnloadCleanup() override;

  LevelScene(const std::string& name_, const CEGUI::String& layoutFileName_);
public:
  virtual ~LevelScene() override = default;

  // Sets the scene's level and solution to those specified
  // Public API forbids setting nullptr
  void setLevelAndSolution(game::Level& level, game::LevelSolution& solution);

  virtual game::Level* level() noexcept = 0;
  virtual game::LevelSolution* solution() noexcept = 0;
};

} // namespace gui

#endif // GUI_LEVEL_SCENE_H
