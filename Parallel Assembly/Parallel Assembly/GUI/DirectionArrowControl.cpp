#include "DirectionArrowControl.h"

#include "../Game/Worker.h"

#include "../Assert.h"

#include "DirectionArrowControlButton.h"

namespace gui {

const CEGUI::String DirectionArrowControl::WidgetTypeName("PAGUI/DirectionArrowControl");
const CEGUI::String DirectionArrowControl::EventNamespace("DirectionArrowControl");

const CEGUI::String DirectionArrowControl::LeftArrowButtonName("__auto_topleft_button__");
const CEGUI::String DirectionArrowControl::UpArrowButtonName("__auto_topright_button__");
const CEGUI::String DirectionArrowControl::RightArrowButtonName("__auto_bottomright_button__");
const CEGUI::String DirectionArrowControl::DownArrowButtonName("__auto_bottomleft_button__");

DirectionArrowControl::DirectionArrowControl(const CEGUI::String& type, const CEGUI::String& name)
  : BaseType(type, name)
  , m_workerSelection(0u)
  , m_leftArrowButton(nullptr)
  , m_upArrowButton(nullptr)
  , m_rightArrowButton(nullptr)
  , m_downArrowButton(nullptr)
{}

void DirectionArrowControl::initialiseComponents() {
  BaseType::initialiseComponents();

  m_leftArrowButton = &dynamic_cast<DirectionArrowControlButton&>(*getChild(LeftArrowButtonName));
  m_upArrowButton = &dynamic_cast<DirectionArrowControlButton&>(*getChild(UpArrowButtonName));
  m_rightArrowButton = &dynamic_cast<DirectionArrowControlButton&>(*getChild(RightArrowButtonName));
  m_downArrowButton = &dynamic_cast<DirectionArrowControlButton&>(*getChild(DownArrowButtonName));

  m_leftArrowButton->setDirection(game::Direction::Left);
  m_upArrowButton->setDirection(game::Direction::Up);
  m_rightArrowButton->setDirection(game::Direction::Right);
  m_downArrowButton->setDirection(game::Direction::Down);
}

DirectionArrowControlButton* DirectionArrowControl::getLeftArrowButton() const {
  ASSERT(m_leftArrowButton);
  return m_leftArrowButton;
}
DirectionArrowControlButton* DirectionArrowControl::getUpArrowButton() const {
  ASSERT(m_upArrowButton);
  return m_upArrowButton;
}
DirectionArrowControlButton* DirectionArrowControl::getRightArrowButton() const {
  ASSERT(m_rightArrowButton);
  return m_rightArrowButton;
}
DirectionArrowControlButton* DirectionArrowControl::getDownArrowButton() const {
  ASSERT(m_downArrowButton);
  return m_downArrowButton;
}

void DirectionArrowControl::setWorkerSelection(std::size_t worker) {
  ASSERT(worker < game::Worker::maxWorkers);
  ASSERT(m_leftArrowButton && m_upArrowButton && m_rightArrowButton && m_downArrowButton);

  if (m_workerSelection != worker) {
    m_workerSelection = worker;

    m_leftArrowButton->setWorkerSelection(worker);
    m_upArrowButton->setWorkerSelection(worker);
    m_rightArrowButton->setWorkerSelection(worker);
    m_downArrowButton->setWorkerSelection(worker);
  }
}
std::size_t DirectionArrowControl::getWorkerSelection() const {
  return m_workerSelection;
}

} // namespace gui
