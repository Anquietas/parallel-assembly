#pragma once
#ifndef GUI_SETTINGS_SCENE_H
#define GUI_SETTINGS_SCENE_H

#include <map>

#include "Scene.h"

#include "../Logger.h"

struct AntiAliasingLevel;

namespace gui {

class ErrorTooltip;

class SettingsScene : public Scene {
  logging::Logger m_logger;

  ErrorTooltip* m_errorTooltip;

  using AALevelList = std::vector<AntiAliasingLevel>;
  using AALevelMap = std::map<CEGUI::ListboxItem*, AALevelList::const_iterator>;

  // Video tab

  CEGUI::Combobox* m_resolutionCombobox;
  CEGUI::RadioButton* m_displayModeWindowedRadio;
  CEGUI::RadioButton* m_displayModeBorderlessRadio;
  CEGUI::RadioButton* m_displayModeFullscreenRadio;
  CEGUI::ToggleButton* m_antialiasCheckbox;
  CEGUI::Combobox* m_antialiasCombobox;
  AALevelMap m_antialiasLevelMap;
  CEGUI::ToggleButton* m_vsyncCheckbox;

  // Audio tab

  CEGUI::ToggleButton* m_enableAudioCheckbox;
  CEGUI::DefaultWindow* m_masterVolumeLabel;
  CEGUI::Slider* m_masterVolumeSlider;
  CEGUI::Editbox* m_masterVolumeEditbox;
  
  CEGUI::DefaultWindow* m_enableMusicLabel;
  CEGUI::ToggleButton* m_enableMusicCheckbox;
  CEGUI::DefaultWindow* m_musicVolumeLabel;
  CEGUI::Slider* m_musicVolumeSlider;
  CEGUI::Editbox* m_musicVolumeEditbox;

  CEGUI::DefaultWindow* m_enableSFXLabel;
  CEGUI::ToggleButton* m_enableSFXCheckbox;
  CEGUI::DefaultWindow* m_sfxVolumeLabel;
  CEGUI::Slider* m_sfxVolumeSlider;
  CEGUI::Editbox* m_sfxVolumeEditbox;

  void initializeButtons();
  CEGUI::ScrollablePane* loadTabFromLayoutFile(const CEGUI::String& fileName);
  void initializeVideoTab();
  void initializeAudioTab();
  // Returns true on success, false on failure
  bool setSettingsFromUI();

protected:
  virtual void initializeComponents() override;
  virtual void cleanupComponents() override;
public:
  SettingsScene();
  virtual ~SettingsScene() override = default;
};

} // namespace gui

#endif // GUI_SETTINGS_SCENE_H
