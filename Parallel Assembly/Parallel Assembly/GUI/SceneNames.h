#pragma once
#ifndef GUI_SCENE_NAMES_H
#define GUI_SCENE_NAMES_H

#include <string>

namespace gui {

extern const std::string loadingSceneName;
extern const std::string mainMenuSceneName;
extern const std::string levelSelectSceneName;
extern const std::string switchProfileSceneName;
extern const std::string settingsSceneName;

extern const std::string simpleLevelSceneName;

} // namespace gui

#endif // GUI_SCENE_NAMES_H
