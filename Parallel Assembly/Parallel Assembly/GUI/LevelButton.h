#pragma once
#ifndef GUI_LEVEL_BUTTON_H
#define GUI_LEVEL_BUTTON_H

#include <CEGUI/widgets/RadioButton.h>
#include <CEGUI/RenderEffect.h>
#include <CEGUI/RendererModules/OpenGL/Texture.h>

#include "checkedCreateWidget.h"

namespace graphics {
class ShaderManager;
class LevelButtonShaderProgram;
}

namespace gui {

class LevelButton : public CEGUI::RadioButton {
  using BaseType = RadioButton;

  CEGUI::String m_numText;
  const CEGUI::Image* m_image;

  CEGUI::Window* m_checkMark;

  CEGUI::GeometryBuffer* m_geometryBuffer;
  CEGUI::RenderEffect* m_effect;

  void addProperties();

  virtual void onNumTextChanged(CEGUI::WindowEventArgs& e);
  virtual void onImageChanged(CEGUI::WindowEventArgs& e);

protected:
  virtual void drawSelf(const CEGUI::RenderingContext& ctx) override;

  static const CEGUI::Image* getFallbackImage();

public:
  static const CEGUI::String WidgetTypeName;
  static const CEGUI::String EventNamespace;

  static const CEGUI::String EventNumTextChanged;
  static const CEGUI::String EventImageChanged;

  static const CEGUI::String CheckMarkName;

  LevelButton(const CEGUI::String& type, const CEGUI::String& name);
  virtual ~LevelButton() override;

  static WidgetPtr<LevelButton> create(const CEGUI::String& name = "");

  virtual void initialiseComponents() override;

  // Sets the text for the level number field of the button
  void setNumText(const CEGUI::String& numText);
  const CEGUI::String& getNumText() const;

  // Sets the image for the button
  // If image is nullptr, the button will use the fallback image instead
  void setImage(const CEGUI::Image* image);
  const CEGUI::Image* getImage() const;

  void setCheckMarkState(bool enabled);

  virtual void notifyScreenAreaChanged(bool recursive = true) override;
};

class LevelButtonRenderEffect : public CEGUI::RenderEffect {
  LevelButton* m_button;
  static graphics::LevelButtonShaderProgram* shader;
  static const CEGUI::OpenGLTexture* maskTexture;
public:
  static const CEGUI::String EffectName;

  static void setupShader(graphics::ShaderManager& shaderManager);

  LevelButtonRenderEffect(CEGUI::Window* window);
  virtual ~LevelButtonRenderEffect() override = default;

  virtual int getPassCount() const override;
  virtual void performPreRenderFunctions(const int pass) override;
  virtual void performPostRenderFunctions() override;
  virtual bool realiseGeometry(CEGUI::RenderingWindow& window,
                               CEGUI::GeometryBuffer& geometry) override;
  virtual bool update(const float elapsed,
                      CEGUI::RenderingWindow& window) override;
};

} // namespace gui

#endif // GUI_LEVEL_BUTTON_H
