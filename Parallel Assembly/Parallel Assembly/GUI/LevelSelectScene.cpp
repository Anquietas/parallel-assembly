#include "LevelSelectScene.h"

#include <chrono>

#include <CEGUI/widgets/All.h>

#include "LevelSolutionItemEntry.h"

#include "stringConvert.h"

#include "../Assert.h"

#include "../Game/Level.h"
#include "../Game/Profile.h"
#include "../Game/LevelManager.h"
#include "../Game/ProfileManager.h"

#include "AnimatedFrameWindow.h"
#include "ErrorTooltip.h"
#include "GUIManager.h"
#include "LevelButton.h"
#include "LevelScene.h"
#include "Scene.inl"
#include "SceneComponent.inl"
#include "SceneNames.h"

using Clock = std::chrono::steady_clock;
using std::chrono::duration_cast;
using std::chrono::nanoseconds;

namespace gui {

const CEGUI::Vector2f LevelSelectScene::levelButtonSize{150.f, 115.f};
const float LevelSelectScene::minLevelButtonMargin = 10.f;

LevelSelectScene::LevelSelectScene(
  const game::LevelManager& levelManager,
  const game::ProfileManager& profileManager)
  : Scene(levelSelectSceneName, "levelSelect.layout")
  , m_logger()
  , m_levelManager(&levelManager)
  , m_profileManager(&profileManager)
  , m_levelContainer(nullptr)
  , m_solutionList(nullptr)
  , m_startButton(nullptr)
  , m_newButton(nullptr)
  , m_copyButton(nullptr)
  , m_renameButton(nullptr)
  , m_deleteButton(nullptr)
  , m_nameEditboxPopup()
  , m_nameEditboxState(NameEditboxState::Inactive)
  , m_confirmDeletePopup("LevelSelect.ConfirmDeleteWindow", "levelSelect-confirmDelete.layout")
  , m_confirmDeleteWindow(nullptr)
  , m_confirmDeleteSolutionLabel(nullptr)
  , m_errorTooltip(nullptr)
  , m_activeProfile(nullptr)
  , m_selectedLevel(nullptr)
  , m_levelButtonMap()
{
  logging::setModuleName(m_logger, "LevelSelectScene");
  attach(m_nameEditboxPopup);
  attach(m_confirmDeletePopup);
}

void LevelSelectScene::initializeComponents() {
  ASSERT(m_levelManager);
  ASSERT(m_profileManager);

  m_levelContainer = getWidget<CEGUI::ScrollablePane>("LevelContainer/ScrollPane");
  m_solutionList = getWidget<CEGUI::ItemListbox>("LevelPanel/SolutionList");
  m_startButton = getWidget<CEGUI::PushButton>("LevelPanel/StartButton");
  m_newButton = getWidget<CEGUI::PushButton>("LevelPanel/NewButton");
  m_copyButton = getWidget<CEGUI::PushButton>("LevelPanel/CopyButton");
  m_renameButton = getWidget<CEGUI::PushButton>("LevelPanel/RenameButton");
  m_deleteButton = getWidget<CEGUI::PushButton>("LevelPanel/DeleteButton");
  auto backButton = getWidget<CEGUI::PushButton>("LevelPanel/BackButton");

  m_nameEditboxPopup.load();
  auto nameEditboxOkButton = m_nameEditboxPopup.getWidget<CEGUI::PushButton>("ButtonContainer/OkButton");
  auto nameEditboxCancelButton = m_nameEditboxPopup.getWidget<CEGUI::PushButton>("ButtonContainer/CancelButton");
  auto nameEditboxCloseButton = m_nameEditboxPopup.getWidget<CEGUI::PushButton>("__auto_closebutton__");

  m_confirmDeletePopup.load();
  m_confirmDeleteWindow = m_confirmDeletePopup.getWidget<AnimatedFrameWindow>("");
  m_confirmDeleteSolutionLabel = m_confirmDeletePopup.getWidget<CEGUI::DefaultWindow>("SolutionLabel");
  auto confirmDeleteYesButton = m_confirmDeletePopup.getWidget<CEGUI::PushButton>("ButtonContainer/YesButton");
  auto confirmDeleteCancelButton = m_confirmDeletePopup.getWidget<CEGUI::PushButton>("ButtonContainer/CancelButton");
  auto confirmDeleteCloseButton = m_confirmDeletePopup.getWidget<CEGUI::PushButton>("__auto_closebutton__");

  auto errorTooltip = ErrorTooltip::create();
  m_errorTooltip = errorTooltip.get();
  sceneRoot()->addChild(m_errorTooltip);
  errorTooltip.release();

  m_activeProfile = m_profileManager->activeProfile();
  ASSERT(m_activeProfile);
  m_activeProfile->loadProfile();
  m_selectedLevel = nullptr; // Reset selection in case of multiple loads

  // Disable level-specific widgets until something is selected
  m_solutionList->setDisabled(true);
  m_startButton->setDisabled(true);
  m_newButton->setDisabled(true);
  m_copyButton->setDisabled(true);
  m_renameButton->setDisabled(true);
  m_deleteButton->setDisabled(true);

  m_solutionList->subscribeEvent(
    CEGUI::ItemListbox::EventSelectionChanged, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        auto selection = m_solutionList->getFirstSelectedItem();
        bool disable = !selection;

        m_startButton->setDisabled(disable);
        m_copyButton->setDisabled(disable);
        m_renameButton->setDisabled(disable);
        m_deleteButton->setDisabled(disable);

        return true;
      }
    ));

  m_startButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        return doStart();
      }
    ));
  m_newButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        showCreateSolutionWindow();
        return true;
      }
    ));
  m_copyButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        return showCopySolutionWindow();
      }
    ));
  m_renameButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        return showRenameSolutionWindow();
      }
    ));
  m_deleteButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        return showConfirmDeleteWindow();
      }
    ));
  backButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        switchToPreviousScene();
        return true;
      }
    ));

  nameEditboxOkButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        bool result = false;
        switch (m_nameEditboxState) {
        case NameEditboxState::CreateSolution:
          result = doCreateSolution();
          break;
        case NameEditboxState::CopySolution:
          result = doCopySolution();
          break;
        case NameEditboxState::RenameSolution:
          result = doRenameSolution();
          break;
        default:
          ASSERT(false);;
        }
        if (result)
          hideNameEditboxPopup();
        return result;
      }
    ));
  auto nameEditboxCancelHandler = [this](const CEGUI::EventArgs&) -> bool {
    hideNameEditboxPopup();
    return true;
  };
  nameEditboxCancelButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(nameEditboxCancelHandler));
  nameEditboxCloseButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(nameEditboxCancelHandler));

  confirmDeleteYesButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        doDeleteSolution();
        hideConfirmDeleteWindow();
        return true;
      }
    ));
  auto confirmDeleteCancelHandler = [this](const CEGUI::EventArgs&) -> bool {
    hideConfirmDeleteWindow();
    return true;
  };
  confirmDeleteCancelButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(confirmDeleteCancelHandler));
  confirmDeleteCloseButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(confirmDeleteCancelHandler));

  initializeLevelButtons();

  LOG_SEVERITY(m_logger, Debug) << "Initialized components";
}

std::size_t LevelSelectScene::maxLevelButtonRowsWithoutScrollbar() const {
  auto& levelArea = m_levelContainer->getClientChildContentArea().get();
  const auto levelAreaHeight = levelArea.getHeight();

  static const auto heightPerButton = levelButtonSize.d_y + 2.f * minLevelButtonMargin;

  auto maxRowsWithoutScrollbar =
    static_cast<std::size_t>(std::floor(levelAreaHeight / heightPerButton));
  ASSERT(maxRowsWithoutScrollbar > 1);

  return maxRowsWithoutScrollbar;
}

std::tuple<std::size_t, std::size_t, float>
LevelSelectScene::computeLevelButtonLayoutInfo(std::size_t numLevels) const {
  static const auto reservedWidthPerButton = levelButtonSize.d_x + 2.f * minLevelButtonMargin;
  auto& levelArea = m_levelContainer->getClientChildContentArea().get();
  auto levelAreaWidth = levelArea.getWidth();
  const auto scrollbarWidth = m_levelContainer->getVertScrollbar()->getPixelSize().d_width;

  // Leave room for scrollbar unconditionally so results are consistent and
  // the horizontal scrollbar is never needed
  auto levelButtonsPerRow = (levelAreaWidth - scrollbarWidth) / reservedWidthPerButton;
  ASSERT(levelButtonsPerRow > 0.f);

  const auto levelsPerRow = static_cast<std::size_t>(std::floor(levelButtonsPerRow));
  ASSERT(levelsPerRow > 0);
  std::size_t rowsRequired = numLevels / levelsPerRow;
  // Incomplete last row
  if (numLevels % levelsPerRow > 0)
    // Need an extra row
    ++rowsRequired;

  // Is scrollbar required?
  if (rowsRequired > maxLevelButtonRowsWithoutScrollbar())
    // If so, factor out its width from the available space
    levelAreaWidth -= scrollbarWidth;

  auto widthUsedByButtons = levelsPerRow * levelButtonSize.d_x;
  auto horizontalMargin = (levelAreaWidth - widthUsedByButtons) / (levelsPerRow + 1);
  ASSERT(horizontalMargin >= minLevelButtonMargin);

  return std::make_tuple(levelsPerRow, rowsRequired, horizontalMargin);
}

void LevelSelectScene::initializeLevelButtons() {
  ASSERT(m_levelManager);
  ASSERT(m_levelButtonMap.empty());

  auto layoutContainerWindow = m_levelContainer->createChild("GridLayoutContainer", "LevelGrid");
  auto layoutContainer = dynamic_cast<CEGUI::GridLayoutContainer*>(layoutContainerWindow);
  if (!layoutContainer)
    throw MAKE_EXCEPTION("Failed to create GridLayoutContainer: GridLayoutContainer is not of type GridLayoutContainer");
  
  static const float verticalMargin = 2.f * minLevelButtonMargin;

  const auto numLevels = m_levelManager->numLevels();

  std::size_t levelsPerRow = 0;
  std::size_t rowsRequired = 0;
  float horizontalMargin = 0.f;

  std::tie(levelsPerRow, rowsRequired, horizontalMargin)
    = computeLevelButtonLayoutInfo(numLevels);

  layoutContainer->setGridDimensions(levelsPerRow, rowsRequired);
  layoutContainer->setMouseInputPropagationEnabled(true);
  layoutContainer->setMousePassThroughEnabled(true);

  m_levelButtonMap.reserve(numLevels);

  for (std::size_t i = 0; i < numLevels; ++i) {
    auto levelButton = LevelButton::create();

    CEGUI::UBox margin{
      // Only add top margin to first row
      cegui_absdim(i < levelsPerRow ? verticalMargin : 0.f),
      cegui_absdim(horizontalMargin),
      cegui_absdim(verticalMargin),
      cegui_absdim(0.f)
    };
    levelButton->setMargin(margin);

    auto level = m_levelManager->getLevel(i);
    levelButton->setNumText(fromStdString(level->id()));
    levelButton->setText(fromStdString(level->name()));
    levelButton->subscribeEvent(
      LevelButton::EventSelectStateChanged, CEGUI::SubscriberSlot(
        [button = levelButton.get(), level, this](const CEGUI::EventArgs&) -> bool {
          if (button->isSelected()) {
            ASSERT(level != m_selectedLevel);
            setSelectedLevel(level);
            return true;
          }
          return false;
        }
      ));

    layoutContainer->addChild(levelButton.get());
    m_levelButtonMap.emplace(level, levelButton.get());
    levelButton.release();
  }

  setLevelButtonStates();
}

void LevelSelectScene::cleanupComponents() {
  ASSERT(!m_nameEditboxPopup.loaded());
  ASSERT(!m_confirmDeletePopup.loaded());

  m_levelButtonMap.clear();
}

void LevelSelectScene::setLevelButtonStates() {
  ASSERT(m_levelManager);
  ASSERT(m_activeProfile);

  const std::size_t numLevels = m_levelManager->numLevels();
  for (std::size_t i = 0; i < numLevels; ++i) {
    auto level = m_levelManager->getLevel(i);
    auto levelButton = m_levelButtonMap.at(level);

    levelButton->setEnabled(m_activeProfile->isLevelUnlocked(*level));
    levelButton->setCheckMarkState(m_activeProfile->isLevelCompleted(*level));
  }
}

void LevelSelectScene::showCreateSolutionWindow() {
  static const CEGUI::String caption = "Create Solution";
  
  LOG_SEVERITY(m_logger, Debug) << "Showing create solution window";

  m_nameEditboxPopup.setCaptionText(caption);
  m_nameEditboxPopup.setEditboxText(CEGUI::String());
  m_nameEditboxPopup.show();
  m_nameEditboxState = NameEditboxState::CreateSolution;
}
bool LevelSelectScene::showCopySolutionWindow() {
  static const CEGUI::String caption = "Copy Solution";

  auto selection = m_solutionList->getFirstSelectedItem();
  if (!selection)
    return false;

  LOG_SEVERITY(m_logger, Debug)
    << "Showing copy solution window for solution \"" << selection->getText() << '\"';
  m_nameEditboxPopup.setCaptionText(caption);
  m_nameEditboxPopup.setEditboxText(selection->getText());
  m_nameEditboxPopup.show();
  m_nameEditboxState = NameEditboxState::CopySolution;

  return false;
}
bool LevelSelectScene::showRenameSolutionWindow() {
  static const CEGUI::String caption = "Rename Solution";
  
  auto selection = m_solutionList->getFirstSelectedItem();
  if (!selection)
    return false;

  LOG_SEVERITY(m_logger, Debug)
    << "Showing rename solution window for solution \"" << selection->getText() << '\"';
  m_nameEditboxPopup.setCaptionText(caption);
  m_nameEditboxPopup.setEditboxText(selection->getText());
  m_nameEditboxPopup.show();
  m_nameEditboxState = NameEditboxState::RenameSolution;

  return true;
}
void LevelSelectScene::hideNameEditboxPopup() {
  m_nameEditboxPopup.hide();
  m_nameEditboxState = NameEditboxState::Inactive;
  if (m_errorTooltip->isVisible())
    m_errorTooltip->hideTooltip();
}

bool LevelSelectScene::showConfirmDeleteWindow() {
  ASSERT(!m_confirmDeleteWindow->isVisible());

  auto selection = m_solutionList->getFirstSelectedItem();
  if (!selection)
    return false;

  m_confirmDeleteSolutionLabel->setText(selection->getText());
  m_confirmDeleteWindow->show();
  m_confirmDeleteWindow->activate();
  m_confirmDeletePopup.setModal();
  return true;
}
void LevelSelectScene::hideConfirmDeleteWindow() {
  m_confirmDeletePopup.clearModal();
  m_confirmDeleteWindow->hide();
}

void LevelSelectScene::setSelectedLevel(game::Level* level) {
  ASSERT(level);
  LOG_SEVERITY(m_logger, Debug)
    << "Selected level changed to \"" << level->name() << "\"";

  auto totalStart = Clock::now();

  m_selectedLevel = level;

  m_solutionList->resetList();
  m_solutionList->clearAllSelections();

  auto solutions = m_activeProfile->getSolutions(*level);
  if (solutions) {
    std::size_t i = 0;
    for (auto iter = solutions->first; iter != solutions->second; ++iter, ++i) {
      const auto solution = iter->second.get();
      ASSERT(&solution->profile() == m_activeProfile);
      ASSERT(&solution->level() == level);

      auto createStart = Clock::now();

      auto itemPtr = LevelSolutionItemEntry::create();

      auto createElapsed = Clock::now() - createStart;
      LOG_SEVERITY(m_logger, Profiling)
        << "Creating solution " << i << " took "
        << duration_cast<nanoseconds>(createElapsed).count() << " ns";

      itemPtr->setText(fromStdString(solution->name()));
      m_solutionList->addItem(itemPtr.get());
      itemPtr.release();
    }
  }
  m_solutionList->handleUpdatedItemData(true);

  // Level selected, so enable solution list and new solution button.
  // Others require a solution to be selected to have meaning
  m_solutionList->setDisabled(false);
  m_newButton->setDisabled(false);

  auto totalElapsed = Clock::now() - totalStart;
  LOG_SEVERITY(m_logger, Profiling)
    << "setSelectedLevel() took "
    << duration_cast<nanoseconds>(totalElapsed).count() << " ns";
}
bool LevelSelectScene::doStart() {
  ASSERT(m_activeProfile);
  ASSERT(m_selectedLevel);

  auto selection = m_solutionList->getFirstSelectedItem();
  if (!selection)
    return false;

  auto& solutionName = selection->getText();

  LOG_SEVERITY(m_logger, Debug)
    << "Starting solution \"" << solutionName << '\"';

  auto solution = m_activeProfile->lookupSolution(
    *m_selectedLevel, toStdString(solutionName));
  if (!solution) {
    std::ostringstream ss;
    ss << "Error starting solution: soklution \"" << solutionName << "\" present in UI solution list,\n";
    ss << "but not present in active profile (\"" << m_activeProfile->name() << "\")";
    throw MAKE_EXCEPTION(ss.str());
  }

  auto guiManager = GUIManager::get();
  auto scene = guiManager->lookupScene(m_selectedLevel->sceneName());
  if (scene) {
    auto levelScene = dynamic_cast<LevelScene*>(scene);
    if (!levelScene) {
      std::ostringstream ss;
      ss << "UI scene associated with level \"" << m_selectedLevel->name() << "\" is not a LevelScene";
      throw MAKE_EXCEPTION(ss.str());
    }
    levelScene->setLevelAndSolution(*m_selectedLevel, *solution);
    switchToScene(*levelScene);
  } else {
    std::ostringstream ss;
    ss << "Tried to switch to level scene " << m_selectedLevel->sceneName() << ": scene does not exist";
    throw MAKE_EXCEPTION(ss.str());
  }

  return false;
}
bool LevelSelectScene::doCreateSolution() {
  ASSERT(m_nameEditboxState == NameEditboxState::CreateSolution);
  ASSERT(m_activeProfile);
  ASSERT(m_selectedLevel);

  auto& solutionName = m_nameEditboxPopup.getEditboxText();
  LOG_SEVERITY(m_logger, Debug)
    << "Creating solution \"" << solutionName << '\"';
  if (solutionName.empty() || solutionName == "") {
    static const CEGUI::String errorText = "A solution name can't be empty";
    m_errorTooltip->showAtRightEdge(m_nameEditboxPopup.getEditbox(), errorText);
    return false;
  }

  bool result = m_activeProfile->createSolution(*m_selectedLevel, toStdString(solutionName));
  if (result) {
    auto itemPtr = LevelSolutionItemEntry::create();
    itemPtr->setText(solutionName);
    m_solutionList->addItem(itemPtr.get());
    itemPtr->setSelected(true);
    itemPtr.release();

    m_solutionList->handleUpdatedItemData(true);
  } else {
    static const CEGUI::String errorText =
      "A solution with that name already exists";
    m_errorTooltip->showAtRightEdge(m_nameEditboxPopup.getEditbox(), errorText);
  }

  return result;
}
bool LevelSelectScene::doCopySolution() {
  ASSERT(m_nameEditboxState == NameEditboxState::CopySolution);
  ASSERT(m_activeProfile);
  ASSERT(m_selectedLevel);

  auto selection = m_solutionList->getFirstSelectedItem();
  ASSERT(selection);
  
  auto& origName = selection->getText();
  auto& copyName = m_nameEditboxPopup.getEditboxText();

  LOG_SEVERITY(m_logger, Debug)
    << "Copying solution \"" << origName << "\" as \"" << copyName << '\"';

  if (copyName.empty() || copyName == "") {
    static const CEGUI::String errorText = "A solution name can't be empty";
    m_errorTooltip->showAtRightEdge(m_nameEditboxPopup.getEditbox(), errorText);
    return false;
  }

  auto result = m_activeProfile->copySolution(
    *m_selectedLevel, toStdString(origName), toStdString(copyName));

  if (!result.first) {
    std::ostringstream ss;
    ss << "Error copying solution: solution \"" << origName << "\" present in UI solution list,\n";
    ss << "but not present in active profile (\"" << m_activeProfile->name() << "\")";
    throw MAKE_EXCEPTION(ss.str());
  } else if (!result.second) {
    static const CEGUI::String errorText =
      "A solution with that name already exists";
    m_errorTooltip->showAtRightEdge(m_nameEditboxPopup.getEditbox(), errorText);
  } else {
    ASSERT (result.first && result.second);

    auto itemPtr = LevelSolutionItemEntry::create();
    itemPtr->setText(copyName);
    m_solutionList->addItem(itemPtr.get());
    itemPtr->setSelected(true);
    itemPtr.release();

    m_solutionList->handleUpdatedItemData(true);
  }

  return result.second;
}
bool LevelSelectScene::doRenameSolution() {
  ASSERT(m_nameEditboxState == NameEditboxState::RenameSolution);
  ASSERT(m_activeProfile);
  ASSERT(m_selectedLevel);

  auto selection = m_solutionList->getFirstSelectedItem();
  ASSERT(selection);

  auto& oldName = selection->getText();
  auto& newName = m_nameEditboxPopup.getEditboxText();

  LOG_SEVERITY(m_logger, Debug)
    << "Renaming solution \"" << oldName << "\" to \"" << newName << '\"';

  if (newName.empty() || newName == "") {
    static const CEGUI::String errorText = "A solution name can't be empty";
    m_errorTooltip->showAtRightEdge(m_nameEditboxPopup.getEditbox(), errorText);
    return false;
  }

  auto result = m_activeProfile->renameSolution(
    *m_selectedLevel, toStdString(oldName), toStdString(newName));

  if (!result.first) {
    std::ostringstream ss;
    ss << "Error renaming solution: solution \"" << oldName << "\" present in UI solution list,\n";
    ss << "but not present in active profile (\"" << m_activeProfile->name() << "\")";
    throw MAKE_EXCEPTION(ss.str());
  } else if (!result.second) {
    static const CEGUI::String errorText =
      "A solution with that name already exists";
    m_errorTooltip->showAtRightEdge(m_nameEditboxPopup.getEditbox(), errorText);
  } else {
    ASSERT(result.first && result.second);

    selection->setText(newName);
    m_solutionList->handleUpdatedItemData(true);
  }

  return result.second;
}
void LevelSelectScene::doDeleteSolution() {
  ASSERT(m_confirmDeleteWindow->isVisible());
  ASSERT(m_activeProfile);
  ASSERT(m_selectedLevel);

  auto selection = m_solutionList->getFirstSelectedItem();
  ASSERT(selection);

  auto& solutionName = selection->getText();

  LOG_SEVERITY(m_logger, Debug)
    << "Deleting solution \"" << solutionName << '\"';

  if (!m_activeProfile->removeSolution(*m_selectedLevel, toStdString(solutionName))) {
    std::ostringstream ss;
    ss << "Error deleting solution: solution \"" << solutionName << "\" present in UI solution list,\n";
    ss << "but not present in active profile (\"" << m_activeProfile->name() << "\")";
    throw MAKE_EXCEPTION(ss.str());
  }

  // Trigger selection change event handler to reset button state
  ASSERT(selection->isSelected());
  selection->setSelected(false);
  // Remove the deleted solution from the list
  m_solutionList->removeItem(selection);
  // Don't need to re-sort since deletion won't affect order
  m_solutionList->handleUpdatedItemData(false);
}

} // namespace gui
