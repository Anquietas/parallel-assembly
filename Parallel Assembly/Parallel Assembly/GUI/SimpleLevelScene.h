#pragma once
#ifndef GUI_SIMPLE_LEVEL_SCENE_H
#define GUI_SIMPLE_LEVEL_SCENE_H

#include "LevelScene.h"

#include "../GLMIncludes.h"
#include "../Logger.h"

namespace game {
class SimpleLevel;
class SimpleLevelSolution;
}

namespace gui {

class LevelSpeedControl;
class UISimpleLevelGrid;
class WorkerSelectionControl;
class DirectionArrowControl;

class SimpleLevelScene : public LevelScene {
  logging::Logger m_logger;

  game::SimpleLevel* m_level;
  game::SimpleLevelSolution* m_solution;

  UISimpleLevelGrid* m_levelGrid;
  LevelSpeedControl* m_levelSpeedControl;
  WorkerSelectionControl* m_workerSelectionControl;
  DirectionArrowControl* m_directionArrowControl;

  virtual void setLevelAndSolution(
    game::Level* level, game::LevelSolution* solution) override;

  bool handleKeyDown(const CEGUI::EventArgs& e);

protected:
  virtual void initializeComponents() override;
public:
  SimpleLevelScene();
  virtual ~SimpleLevelScene() override = default;

  virtual game::Level* level() noexcept override;
  virtual game::LevelSolution* solution() noexcept override;
};

} // namespace gui

#endif // GUI_SIMPLE_LEVEL_SCENE_H
