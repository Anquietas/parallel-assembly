#include "GUIManager.inl"

#include <chrono>
#include <mutex>
#include <cuchar>

#include <boost/container/flat_map.hpp>

#include <CEGUI/GlobalEventSet.h>
#include <CEGUI/ImageManager.h>
#include <CEGUI/Font.h>
#include <CEGUI/Scheme.h>
#include <CEGUI/falagard/WidgetLookManager.h>
#include <CEGUI/WindowManager.h>
#include <CEGUI/WindowFactoryManager.h>
#include <CEGUI/WindowRendererManager.h>
#include <CEGUI/XMLParser.h>
#include <CEGUI/widgets/Editbox.h>
#include <CEGUI/widgets/MultiLineEditbox.h>
#include <CEGUI/Clipboard.h>
#include <CEGUI/AnimationManager.h>
#include <CEGUI/RenderEffectManager.h>

#ifndef NDEBUG
#include <CEGUI/XMLParserModules/Xerces/XMLParser.h>
#else
#include <CEGUI/XMLParserModules/Expat/XMLParser.h>
#endif

#include "../Assert.h"
#include "../Exception.h"
#include "../PhoenixSingleton.inl"

#include "Scene.h"

#include "AnimatedFrameWindow.h"
#include "DirectionArrowControl.h"
#include "DirectionArrowControlButton.h"
#include "DirectionArrowDraggable.h"
#include "DirectionArrowGeometryBuffer.h"
#include "ErrorTooltip.h"
#include "LevelButton.h"
#include "LevelSolutionItemEntry.h"
#include "LevelSpeedControl.h"
#include "RenderableWindow.h"
#include "ResultsInfo.h"
#include "UISimpleLevelGrid.h"
#include "WorkerSelectionControl.h"

#include "LoadingScene.h"
#include "MainMenuScene.h"
#include "LevelSelectScene.h"
#include "SwitchProfileScene.h"
#include "SettingsScene.h"
#include "SimpleLevelScene.h"

PHOENIX_SINGLETON_TEMPLATE_INSTANCE(gui::GUIManager);

namespace gui {

GUIManager::GUIManager()
  : enable_shared_from_this()
  , PhoenixSingleton()
  , m_logger()
  , m_lastTime(Clock::now())
  , m_guiLogger()
  , m_renderer(nullptr)
  , m_resProvider()
#ifndef NDEBUG
  , m_xmlParser(new CEGUI::XercesParser())
#else
  , m_xmlParser(new CEGUI::ExpatParser())
#endif
  , m_system(nullptr)
  , m_schemeManager(nullptr)
  , m_windowManager(nullptr)
  , m_animationManager(nullptr)
  , m_context(nullptr)
  , m_scenes()
  , m_sceneStack()
  , m_origMousePosition()
  , m_mousePositionLocked(false)
  , m_ignoreNextMouseMotion(false)
  , m_numMouseMotionsToIgnore(0)
{
  logging::setModuleName(m_logger, "GUIManager");
  LOG_SEVERITY(m_logger, Debug) << "Creating GUIManager";
  try {
    CEGUI::Exception::setStdErrEnabled(false);

    m_guiLogger.setLoggingLevel(CEGUI::LoggingLevel::Warnings);

    m_renderer = &CEGUI::OpenGL3Renderer::create();
    m_system = &CEGUI::System::create(*m_renderer, &m_resProvider, m_xmlParser.get());

    LOG_SEVERITY(m_logger, Debug) << "Initialized CEGUI system";

    m_resProvider.setResourceGroupDirectory("schemes", "Assets/GUI/schemes/");
    m_resProvider.setResourceGroupDirectory("imagesets", "Assets/GUI/imagesets/");
    m_resProvider.setResourceGroupDirectory("fonts", "Assets/GUI/fonts/");
    m_resProvider.setResourceGroupDirectory("layouts", "Assets/GUI/layouts/");
    m_resProvider.setResourceGroupDirectory("looknfeels", "Assets/GUI/looknfeel/");
    m_resProvider.setResourceGroupDirectory("xml_schemas", "Assets/GUI/xml_schemas/");
    m_resProvider.setResourceGroupDirectory("animations", "Assets/GUI/animations/");

    CEGUI::ImageManager::setImagesetDefaultResourceGroup("imagesets");
    CEGUI::Font::setDefaultResourceGroup("fonts");
    CEGUI::Scheme::setDefaultResourceGroup("schemes");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("looknfeels");
    CEGUI::WindowManager::setDefaultResourceGroup("layouts");
    CEGUI::AnimationManager::setDefaultResourceGroup("animations");

    CEGUI::XMLParser* parser = m_system->getXMLParser();
    if (parser->isPropertyPresent("SchemaDefaultResourceGroup"))
      parser->setProperty("SchemaDefaultResourceGroup", "xml_schemas");

    LOG_SEVERITY(m_logger, Debug) << "Set CEGUI resource groups";

    m_context = &m_system->getDefaultGUIContext();

    m_schemeManager = CEGUI::SchemeManager::getSingletonPtr();
    m_windowManager = CEGUI::WindowManager::getSingletonPtr();
    m_animationManager = CEGUI::AnimationManager::getSingletonPtr();

    registerCustomWidgetTypes();

    LOG_SEVERITY(m_logger, Debug) << "Registered custom widget types";

    m_schemeManager->createFromFile("PAGUI.scheme");

    LOG_SEVERITY(m_logger, Debug) << "Loaded PAGUI.scheme";

    m_context->setDefaultFont("Arial-12");
    m_context->setDefaultTooltipType("PAGUI/Tooltip");

    LOG_SEVERITY(m_logger, Debug) << "Set default font and tooltip";

    m_context->getMouseCursor().setDefaultImage("PAGUI/Cursor-Arrow");

    LOG_SEVERITY(m_logger, Debug) << "Set default cursor image to Cursor-Arrow/Cursor";
  } catch (...) {
    if (m_system)
      CEGUI::System::destroy();
    if (m_renderer)
      CEGUI::OpenGL3Renderer::destroy(*m_renderer);
    throw;
  }
}

GUIManager::~GUIManager() {
  LOG_SEVERITY(m_logger, Debug) << "Destroying GUIManager";
  m_sceneStack.clear();
  m_scenes.clear();
  if (m_system)
    CEGUI::System::destroy();
  if (m_renderer)
    CEGUI::OpenGL3Renderer::destroy(*m_renderer);
}

void GUIManager::registerCustomWidgetTypes() {
  using FactoryManager = CEGUI::WindowFactoryManager;
  using RendererManager = CEGUI::WindowRendererManager;
  auto& renderEffectManager = CEGUI::RenderEffectManager::getSingleton();

  FactoryManager::addWindowType<ErrorTooltip>();
  RendererManager::addWindowRendererType<ErrorTooltipWindowRenderer>();

  m_animationManager->loadAnimationsFromXML("AnimatedFrameWindow.anims");
  FactoryManager::addWindowType<AnimatedFrameWindow>();

  FactoryManager::addWindowType<LevelSolutionItemEntry>();

  FactoryManager::addWindowType<LevelButton>();
  renderEffectManager.addEffect<LevelButtonRenderEffect>(LevelButtonRenderEffect::EffectName);

  RendererManager::addWindowRendererType<RenderableWindowRenderer>();

  FactoryManager::addWindowType<ResultsInfo>();

  FactoryManager::addWindowType<LevelSpeedControl>();

  FactoryManager::addWindowType<UISimpleLevelGrid>();

  FactoryManager::addWindowType<WorkerSelectionControl>();

  FactoryManager::addWindowType<DirectionArrowControlButton>();
  RendererManager::addWindowRendererType<DirectionArrowControlButtonRenderer>();

  FactoryManager::addWindowType<DirectionArrowControl>();

  FactoryManager::addWindowType<DirectionArrowDraggable>();
}

void GUIManager::initializeCustomWidgetData(graphics::ShaderManager& shaderManager) {
  LevelButtonRenderEffect::setupShader(shaderManager);
  DirectionArrowGeometryBuffer::setupShader(shaderManager);

  LOG_SEVERITY(m_logger, Debug) << "Initialized custom widget data";
}
void GUIManager::registerScenes(
  const game::LevelManager& levelManager,
  game::ProfileManager& profileManager)
{
  addScene<LoadingScene>();
  addScene<MainMenuScene>(profileManager);
  addScene<LevelSelectScene>(levelManager, profileManager);
  addScene<SwitchProfileScene>(profileManager);
  addScene<SettingsScene>();

  addScene<SimpleLevelScene>();
}

CEGUI::MouseButton getMouseButton(Uint8 sdlMouseButton) noexcept;
CEGUI::Key::Scan getKeyScanCode(const SDL_Keysym& keysym) noexcept;

CEGUI::Window* GUIManager::getInputFocusedWindow() const {
  auto root = m_context->getRootWindow();
  return root ? root->getActiveChild() : nullptr;
}

void GUIManager::setupInputHandlers(sdl::EventHandler& eventHandler) {
  auto guiManager = shared_from_this();
  // Mouse events

  eventHandler.addEventHandler(
    SDL_MOUSEBUTTONDOWN,
    [guiManager](const SDL_Event& e, sdl::GLWindow& window) {
      auto& event = e.button;
      if (event.windowID == window.getID()) {
        // Workaround for large mouse motion on unlocking mouse motion
        if (guiManager->m_ignoreNextMouseMotion.get()) {
          LOG_SEVERITY(guiManager->m_logger, Debug)
            << "Ignoring mouse motion in button down event...";
        } else if (!guiManager->isMousePositionLocked())
          guiManager->m_context->injectMousePosition(float(event.x), float(event.y));
        guiManager->m_context->injectMouseButtonDown(getMouseButton(event.button));
      }
    });
  eventHandler.addEventHandler(
    SDL_MOUSEBUTTONUP,
    [guiManager](const SDL_Event& e, sdl::GLWindow& window) {
      auto& event = e.button;
      if (event.windowID == window.getID()) {
        // Workaround for large mouse motion on unlocking mouse motion
        if (guiManager->m_ignoreNextMouseMotion.get()) {
          LOG_SEVERITY(guiManager->m_logger, Debug)
            << "Ignoring mouse motion in button up event...";
        } else if (!guiManager->isMousePositionLocked())
          guiManager->m_context->injectMousePosition(float(event.x), float(event.y));
        guiManager->m_context->injectMouseButtonUp(getMouseButton(event.button));
      }
    });
  eventHandler.addEventHandler(
    SDL_MOUSEMOTION,
    [guiManager](const SDL_Event& e, sdl::GLWindow& window) {
      auto& event = e.motion;
      if (event.windowID == window.getID()) {
        // Workaround for large mouse motion on unlocking mouse motion
        if (guiManager->m_ignoreNextMouseMotion.get()) {
          LOG_SEVERITY(guiManager->m_logger, Debug)
            << "Ignoring mouse motion after unlock...";
          if (--guiManager->m_numMouseMotionsToIgnore == 0) {
            guiManager->m_ignoreNextMouseMotion.clear();
            LOG_SEVERITY(guiManager->m_logger, Debug)
              << "Mouse unlock complete; no longer ignoring mouse motion";
          }
          return;
        } else if (guiManager->isMousePositionLocked()) {
          auto& mouseCursor = guiManager->m_context->getMouseCursor();
          auto position = mouseCursor.getPosition();
          guiManager->m_context->injectMouseMove(float(event.xrel), float(event.yrel));
          mouseCursor.setPosition(position);
        } else
          guiManager->m_context->injectMousePosition(float(event.x), float(event.y));
      }
    });
  // Set initial mouse position
  glm::ivec2 mousePos;
  SDL_GetMouseState(&mousePos.x, &mousePos.y);
  m_context->injectMousePosition(float(mousePos.x), float(mousePos.y));

  eventHandler.addEventHandler(
    SDL_MOUSEWHEEL,
    [guiManager](const SDL_Event& e, sdl::GLWindow& window) {
      auto& event = e.wheel;
      if (event.windowID == window.getID()) {
        float wheelChange = float(event.y);
        if (event.direction == SDL_MOUSEWHEEL_FLIPPED)
          wheelChange = -wheelChange;
        guiManager->m_context->injectMouseWheelChange(wheelChange);
      }
    });
  eventHandler.addWindowEventHandler(
    SDL_WINDOWEVENT_LEAVE,
    [guiManager](const SDL_WindowEvent&, sdl::GLWindow&) {
      guiManager->m_context->injectMousePosition(-1.f, -1.f);
      guiManager->m_context->injectMouseLeaves();
      guiManager->hideCursor();
      LOG_SEVERITY(guiManager->m_logger, Debug) << "Hid mouse cursor";
    });
  eventHandler.addWindowEventHandler(
    SDL_WINDOWEVENT_ENTER,
    [guiManager](const SDL_WindowEvent&, sdl::GLWindow&) {
      guiManager->showCursor();
      LOG_SEVERITY(guiManager->m_logger, Debug) << "Unhid mouse cursor";
    });

  // Keyboard events

  eventHandler.addEventHandler(
    SDL_KEYDOWN,
    [guiManager](const SDL_Event& e, sdl::GLWindow& window) {
      auto& event = e.key;
      if (event.windowID == window.getID()) {
        // either control key is held down, and no other modifiers.
        if ((event.keysym.mod & KMOD_CTRL) &&
            !(event.keysym.mod & (KMOD_SHIFT | KMOD_ALT | KMOD_GUI)))
        {
          // Detect Ctrl+A/X/C/V
          switch (event.keysym.sym) {
          // Ctrl+A selection for editboxes
          case SDLK_a: {
            auto inputWindow = guiManager->getInputFocusedWindow();
            if (inputWindow) {
              auto& windowType = inputWindow->getType();
              // TODO: make this case insensitive
              if (windowType.find("MultiLineEditbox") != CEGUI::String::npos) {
                auto editbox = dynamic_cast<CEGUI::MultiLineEditbox*>(inputWindow);
                if (editbox && editbox->hasInputFocus()) {
                  LOG_SEVERITY(guiManager->m_logger, Debug) << "Ctrl+A: MultilineEditbox";
                  // 1 more than length because you can select the end of the string
                  editbox->setSelection(0, editbox->getText().length());
                }
              } else if (windowType.find("Editbox") != CEGUI::String::npos) {
                auto editbox = dynamic_cast<CEGUI::Editbox*>(inputWindow);
                if (editbox && editbox->hasInputFocus()) {
                  LOG_SEVERITY(guiManager->m_logger, Debug) << "Ctrl+A: Editbox";
                  // 1 more than length because you can select the end of the string
                  editbox->setSelection(0, editbox->getText().length());
                }
              }
            }
          } break;
          case SDLK_x: {
            LOG_SEVERITY(guiManager->m_logger, Debug) << "Ctrl+X";
            bool result = guiManager->m_context->injectCutRequest();
            if (result) {
              auto clipboard = guiManager->m_system->getClipboard();
              if (SDL_SetClipboardText(clipboard->getText().c_str()) < 0) {
                std::ostringstream ss;
                ss << "Failed to cut to clipboard: " << SDL_GetError();
                throw MAKE_EXCEPTION(ss.str());
              }
            }
          } break;
          case SDLK_c: {
            LOG_SEVERITY(guiManager->m_logger, Debug) << "Ctrl+C";
            bool result = guiManager->m_context->injectCopyRequest();
            if (result) {
              auto clipboard = guiManager->m_system->getClipboard();
              if (SDL_SetClipboardText(clipboard->getText().c_str()) < 0) {
                std::ostringstream ss;
                ss << "Failed to cut to clipboard: " << SDL_GetError();
                throw MAKE_EXCEPTION(ss.str());
              }
            }
          } break;
          case SDLK_v:
            LOG_SEVERITY(guiManager->m_logger, Debug) << "Ctrl+V";
            if (SDL_HasClipboardText() == SDL_TRUE) {
              std::unique_ptr<char, void(*)(void*)>
                clipboardText{ SDL_GetClipboardText(), SDL_free };
              auto clipboard = guiManager->m_system->getClipboard();

              // Needed to get CEGUI to recognize char* as UTF8
              static_assert(sizeof(char) == sizeof(CEGUI::utf8),
                            "char and CEGUI::utf8 have different sizes");

              clipboard->setText(reinterpret_cast<CEGUI::utf8*>(clipboardText.get()));
              guiManager->m_context->injectPasteRequest();
            }
            break;
          default:
            break;
          }
        }
        auto scancode = getKeyScanCode(event.keysym);
        if (scancode != CEGUI::Key::Scan::Unknown)
          guiManager->m_context->injectKeyDown(scancode);
      }
    });
  eventHandler.addEventHandler(
    SDL_KEYUP,
    [guiManager](const SDL_Event& e, sdl::GLWindow& window) {
      auto& event = e.key;
      if (event.windowID == window.getID()) {
        auto scancode = getKeyScanCode(event.keysym);
        if (scancode != CEGUI::Key::Scan::Unknown)
          guiManager->m_context->injectKeyUp(scancode);
      }
    });
  eventHandler.addEventHandler(
    SDL_TEXTINPUT,
    [guiManager](const SDL_Event& e, sdl::GLWindow& window) {
      auto& event = e.text;
      if (event.windowID == window.getID()) {
        std::mbstate_t state{};
        ASSERT(std::mbsinit(&state));
        char32_t utf32 = U'\0';
        auto result = std::mbrtoc32(&utf32, event.text, 31, &state);
        if (result > 0)
          guiManager->m_context->injectChar(static_cast<CEGUI::utf32>(utf32));
        else {
          switch (result) {
          case 0:
            throw MAKE_EXCEPTION("Text input event generated null character");
            break;
          case -1:
            throw MAKE_EXCEPTION("Text input event generated an invalid character");
            break;
          case -2:
            throw MAKE_EXCEPTION("Text input event generated an incomplete character");
            break;
          case -3:
            throw MAKE_EXCEPTION("Text input event generated a multi-character UTF32 character");
            break;
          default:
            ASSERT(false);
            throw MAKE_EXCEPTION("std::mbrtoc32 returned an invalid value");
          }
        }
      }
    });

  // Fix for paste etc exceeding editbox max text length
  auto& globalEventSet = CEGUI::GlobalEventSet::getSingleton();
  globalEventSet.subscribeEvent(
    CEGUI::Window::EventNamespace + "/" + CEGUI::Window::EventTextChanged,
    CEGUI::SubscriberSlot(
      [](const CEGUI::EventArgs& args) -> bool {
        auto& e = dynamic_cast<const CEGUI::WindowEventArgs&>(args);
        if (auto editbox = dynamic_cast<CEGUI::Editbox*>(e.window)) {
          if (editbox->getText().size() > editbox->getMaxTextLength()) {
            editbox->setText(editbox->getText().substr(0, editbox->getMaxTextLength()));
            return true;
          }
        } else if (auto mlEditbox = dynamic_cast<CEGUI::MultiLineEditbox*>(e.window)) {
          if (mlEditbox->getText().size() > mlEditbox->getMaxTextLength()) {
            mlEditbox->setText(mlEditbox->getText().substr(0, mlEditbox->getMaxTextLength()));
            return true;
          }
        }
        return false;
      }
    ));
}

void GUIManager::addScene(std::unique_ptr<Scene> scene) {
  LOG_SEVERITY(m_logger, Debug) << "Adding scene: \"" << scene->name() << '\"';
  auto result = m_scenes.emplace(scene->name(), std::move(scene));
  if (!result.second) {
    std::ostringstream ss;
    ss << "Duplicate attempt to register a scene with the name \"" << scene->name() << '\"';
    throw MAKE_EXCEPTION(ss.str());
  }
}
void GUIManager::removeScene(const std::string& name) {
  LOG_SEVERITY(m_logger, Debug) << "Unregistering scene: \"" << name << '\"';
  
  auto iter = m_scenes.find(name);
  if (iter != m_scenes.end()) {
    std::ostringstream ss;
    ss << "Tried to remove scene \"" << name << "\" that wasn't previously added.";
    throw MAKE_EXCEPTION(ss.str());
  }
  if (iter->second->active()) {
    ASSERT(!sceneStackEmpty());
    popSceneStack();
  }
  m_scenes.erase(iter);
}

Scene* GUIManager::lookupScene(const std::string& name) {
  auto iter = m_scenes.find(name);
  if (iter != m_scenes.end())
    return iter->second.get();
  else
    return nullptr;
}

void GUIManager::pushSceneStack(Scene& scene) {
  LOG_SEVERITY(m_logger, Debug) << "Pushing scene: \"" << scene.name() << '\"';
  if (!m_sceneStack.empty()) {
    m_sceneStack.back()->clearActive();
    m_sceneStack.back()->unload();
  }
  scene.load();
  m_context->setRootWindow(scene.sceneRoot());
  scene.initializeComponents();
  scene.setActive();
  m_sceneStack.push_back(&scene);
}
Scene& GUIManager::swapSceneStackTop(Scene& scene) {
  if (m_sceneStack.empty())
    throw MAKE_EXCEPTION("Tried to swap top of scene stack while empty");
  ASSERT(m_sceneStack.back());

  LOG_SEVERITY(m_logger, Debug)
    << "Swapping active scene \"" << m_sceneStack.back()->name()
    << "\" with scene \"" << scene.name() << '\"';

  m_sceneStack.back()->clearActive();
  m_sceneStack.back()->unload();
  scene.load();
  // Skip the expensive operation if root doesn't actually change
  // (e.g. in move constructor/assignment)
  if (m_context->getRootWindow() != scene.sceneRoot())
    m_context->setRootWindow(scene.sceneRoot());
  scene.initializeComponents();
  scene.setActive();

  auto oldScene = &scene;
  std::swap(m_sceneStack.back(), oldScene);

  ASSERT(m_sceneStack.back() == &scene && oldScene != &scene);
  return *oldScene;
}
bool GUIManager::popSceneStack() {
  if (m_sceneStack.empty())
    throw MAKE_EXCEPTION("Tried to pop scene stack while empty");
  ASSERT(m_sceneStack.back());

  LOG_SEVERITY(m_logger, Debug) << "Popping scene: \"" << m_sceneStack.back()->name() << '\"';

  m_sceneStack.back()->clearActive();
  m_sceneStack.back()->unload();
  m_sceneStack.pop_back();

  if (m_sceneStack.empty()) {
    LOG_SEVERITY(m_logger, Debug) << "Scene stack empty after popping";
    m_context->setRootWindow(nullptr);
    return false;
  } else {
    ASSERT(m_sceneStack.back());
    LOG_SEVERITY(m_logger, Debug)
      << "Scene stack non-empty after popping; setting \"" << m_sceneStack.back()->name() << "\" active";
    m_sceneStack.back()->load();
    m_context->setRootWindow(m_sceneStack.back()->sceneRoot());
    m_sceneStack.back()->initializeComponents();
    m_sceneStack.back()->setActive();
    return true;
  }
}
void GUIManager::clearSceneStack() {
  LOG_SEVERITY(m_logger, Debug) << "Clearing scene stack";
  if (!m_sceneStack.empty()) {
    ASSERT(m_sceneStack.back());
    m_sceneStack.back()->clearActive();
    m_sceneStack.back()->unload();
    m_context->setRootWindow(nullptr);
  }
  m_sceneStack.clear();
}
Scene& GUIManager::activeScene() const {
  if (m_sceneStack.empty())
    throw MAKE_EXCEPTION("Tried to get active scene while scene stack is empty");
  ASSERT(m_sceneStack.back());
  return *m_sceneStack.back();
}

void GUIManager::showCursor() {
  m_context->getMouseCursor().show();
}
void GUIManager::hideCursor() {
  m_context->getMouseCursor().hide();
}

void GUIManager::lockMousePosition() {
  if (!m_mousePositionLocked.get()) {
    m_mousePositionLocked.set();
    SDL_GetMouseState(&m_origMousePosition.x, &m_origMousePosition.y);
    SDL_SetRelativeMouseMode(SDL_TRUE);

    LOG_SEVERITY(m_logger, Debug)
      << "Locked mouse position at ("
      << m_origMousePosition.x << ", " << m_origMousePosition.y << ')';
  }
}
void GUIManager::unlockMousePosition() {
  if (m_mousePositionLocked.get()) {
    m_mousePositionLocked.clear();

    m_numMouseMotionsToIgnore = 1;
    m_ignoreNextMouseMotion.set();
    SDL_SetRelativeMouseMode(SDL_FALSE);
    SDL_WarpMouseInWindow(nullptr, m_origMousePosition.x, m_origMousePosition.y);

    LOG_SEVERITY(m_logger, Debug)
      << "Unlocked mouse position; restored to ("
      << m_origMousePosition.x << ", " << m_origMousePosition.y
      << ')';
    if (m_numMouseMotionsToIgnore == 1)
      LOG_SEVERITY(m_logger, Debug)
        << "Ignoring next mouse motion to avoid spurious large motion";
    else
      LOG_SEVERITY(m_logger, Debug)
        << "Ignoring next " << m_numMouseMotionsToIgnore
        << " mouse motions to avoid spurious large motion";
  }
}

void GUIManager::injectTimePulse() {
  using std::chrono::duration_cast;

  auto now = Clock::now();
  float elapsed = duration_cast<FloatSeconds>(now - m_lastTime).count();
  m_lastTime = now;

  m_system->injectTimePulse(elapsed);
  m_context->injectTimePulse(elapsed);
}

CEGUI::MouseButton getMouseButton(Uint8 sdlMouseButton) noexcept {
  switch (sdlMouseButton) {
  case SDL_BUTTON_LEFT:
    return CEGUI::MouseButton::LeftButton;
  case SDL_BUTTON_MIDDLE:
    return CEGUI::MouseButton::MiddleButton;
  case SDL_BUTTON_RIGHT:
    return CEGUI::MouseButton::RightButton;
  case SDL_BUTTON_X1:
    return CEGUI::MouseButton::X1Button;
  case SDL_BUTTON_X2:
    return CEGUI::MouseButton::X2Button;
  default:
    ASSERT(false);
    return CEGUI::MouseButton::NoButton;
  }
}

CEGUI::Key::Scan getKeyScanCode(const SDL_Keysym& keysym) noexcept {
  using boost::container::flat_map;
  using CEGUI::Key;
  static const flat_map<SDL_Scancode, CEGUI::Key::Scan> scancodeMap{
    {SDL_SCANCODE_Q, Key::Q},
    {SDL_SCANCODE_W, Key::W},
    {SDL_SCANCODE_E, Key::E},
    {SDL_SCANCODE_R, Key::R},
    {SDL_SCANCODE_T, Key::T},
    {SDL_SCANCODE_Y, Key::Y},
    {SDL_SCANCODE_U, Key::U},
    {SDL_SCANCODE_I, Key::I},
    {SDL_SCANCODE_O, Key::O},
    {SDL_SCANCODE_P, Key::P},

    {SDL_SCANCODE_A, Key::A},
    {SDL_SCANCODE_S, Key::S},
    {SDL_SCANCODE_D, Key::D},
    {SDL_SCANCODE_F, Key::F},
    {SDL_SCANCODE_G, Key::G},
    {SDL_SCANCODE_H, Key::H},
    {SDL_SCANCODE_J, Key::J},
    {SDL_SCANCODE_K, Key::K},
    {SDL_SCANCODE_L, Key::L},

    {SDL_SCANCODE_Z, Key::Z},
    {SDL_SCANCODE_X, Key::X},
    {SDL_SCANCODE_C, Key::C},
    {SDL_SCANCODE_V, Key::V},
    {SDL_SCANCODE_B, Key::B},
    {SDL_SCANCODE_N, Key::N},
    {SDL_SCANCODE_M, Key::M},
  };
  static const flat_map<SDL_Keycode, CEGUI::Key::Scan> keycodeMap{
    {SDLK_ESCAPE, Key::Escape},
    {SDLK_F1, Key::F1},
    {SDLK_F2, Key::F2},
    {SDLK_F3, Key::F3},
    {SDLK_F4, Key::F4},
    {SDLK_F5, Key::F5},
    {SDLK_F6, Key::F6},
    {SDLK_F7, Key::F7},
    {SDLK_F8, Key::F8},
    {SDLK_F9, Key::F9},
    {SDLK_F10, Key::F10},
    {SDLK_F11, Key::F11},
    {SDLK_F12, Key::F12},
    {SDLK_SCROLLLOCK, Key::ScrollLock},
    {SDLK_PAUSE, Key::Pause},

    {SDLK_BACKQUOTE, Key::Grave},
    {SDLK_1, Key::One},
    {SDLK_2, Key::Two},
    {SDLK_3, Key::Three},
    {SDLK_4, Key::Four},
    {SDLK_5, Key::Five},
    {SDLK_6, Key::Six},
    {SDLK_7, Key::Seven},
    {SDLK_8, Key::Eight},
    {SDLK_9, Key::Nine},
    {SDLK_0, Key::Zero},
    {SDLK_MINUS, Key::Minus},
    {SDLK_EQUALS, Key::Equals},
    {SDLK_BACKSPACE, Key::Backspace},

    {SDLK_TAB, Key::Tab},
    {SDLK_LEFTBRACKET, Key::LeftBracket},
    {SDLK_RIGHTBRACKET, Key::RightBracket},
    {SDLK_RETURN, Key::Return},

    {SDLK_CAPSLOCK, Key::Capital},
    {SDLK_SEMICOLON, Key::Semicolon},
    {SDLK_QUOTE, Key::Apostrophe},

    {SDLK_LSHIFT, Key::LeftShift},
    {SDLK_BACKSLASH, Key::Backslash},
    {SDLK_COMMA, Key::Comma},
    {SDLK_PERIOD, Key::Period},
    {SDLK_SLASH, Key::Slash},
    {SDLK_RSHIFT, Key::RightShift},
    {SDLK_LCTRL, Key::LeftControl},
    {SDLK_LGUI, Key::LeftWindows},
    {SDLK_LALT, Key::LeftAlt},
    {SDLK_RALT, Key::RightAlt},
    {SDLK_RGUI, Key::RightWindows},
    {SDLK_MENU, Key::AppMenu},
    {SDLK_RCTRL, Key::RightControl},

    {SDLK_INSERT, Key::Insert},
    {SDLK_DELETE, Key::Delete},
    {SDLK_HOME, Key::Home},
    {SDLK_END, Key::End},
    {SDLK_PAGEUP, Key::PageUp},
    {SDLK_PAGEDOWN, Key::PageDown},

    {SDLK_LEFT, Key::ArrowLeft},
    {SDLK_RIGHT, Key::ArrowRight},
    {SDLK_UP, Key::ArrowUp},
    {SDLK_DOWN, Key::ArrowDown},

    {SDLK_NUMLOCKCLEAR, Key::NumLock},
    {SDLK_KP_DIVIDE, Key::Divide},
    {SDLK_KP_MULTIPLY, Key::Multiply},
    {SDLK_KP_MINUS, Key::Subtract},
    {SDLK_KP_ENTER, Key::NumpadEnter},
    {SDLK_KP_DECIMAL, Key::Decimal},
    {SDLK_KP_PERIOD, Key::Decimal},
    {SDLK_KP_0, Key::Numpad0},
    {SDLK_KP_1, Key::Numpad1},
    {SDLK_KP_2, Key::Numpad2},
    {SDLK_KP_3, Key::Numpad3},
    {SDLK_KP_4, Key::Numpad4},
    {SDLK_KP_5, Key::Numpad5},
    {SDLK_KP_6, Key::Numpad6},
    {SDLK_KP_7, Key::Numpad7},
    {SDLK_KP_8, Key::Numpad8},
    {SDLK_KP_9, Key::Numpad9},
  };

  auto scancodeIter = scancodeMap.find(keysym.scancode);
  if (scancodeIter != scancodeMap.end())
    return scancodeIter->second;

  auto keycodeIter = keycodeMap.find(keysym.sym);
  if (keycodeIter != keycodeMap.end())
    return keycodeIter->second;

  return Key::Scan::Unknown;
}

} // namespace gui
