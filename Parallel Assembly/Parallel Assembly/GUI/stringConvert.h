#pragma once
#ifndef GUI_STRING_CONVERT_H
#define GUI_STRING_CONVERT_H

#include <string>
#include <CEGUI/String.h>

namespace gui {

std::string toStdString(const CEGUI::String& str);
CEGUI::String fromStdString(const std::string& str);

}

#endif // GUI_STRING_CONVERT_H
