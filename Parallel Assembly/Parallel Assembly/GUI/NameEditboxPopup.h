#pragma once
#ifndef GUI_NAME_EDITBOX_POPUP
#define GUI_NAME_EDITBOX_POPUP

#include "PopupSceneComponent.h"
#include "AnimatedFrameWindow.h"

namespace gui {

class NameEditboxPopup final : public PopupSceneComponent {
  logging::Logger m_logger;

  AnimatedFrameWindow* m_window;
  CEGUI::Editbox* m_nameEditbox;

  virtual void initializeComponents() override;
public:
  NameEditboxPopup();
  virtual ~NameEditboxPopup() override = default;

  void setCaptionText(const CEGUI::String& text);

  const CEGUI::Editbox* getEditbox() const;

  void setEditboxText(const CEGUI::String& text);
  const CEGUI::String& getEditboxText() const;

  void setEditboxMaxTextLength(std::size_t maxLength);

  void show();
  void hide();
};

} // namespace gui

#endif // GUI_NAME_EDITBOX_POPUP
