#pragma once
#ifndef GUI_GUIMANAGER_H
#define GUI_GUIMANAGER_H

#include <memory>
#include <map>
#include <string>
#include <chrono>
#include <vector>

#include <boost/container/flat_map.hpp>

#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>
#include <CEGUI/DefaultResourceProvider.h>
#include <CEGUI/System.h>

#include "GUILoggerAdaptor.h"

#include "../SDL/EventHandler.h"

#include "../Flag.h"
#include "../Logger.h"
#include "../PhoenixSingleton.h"

namespace game {
class LevelManager;
class ProfileManager;
}

namespace graphics {
class ShaderManager;
}

namespace gui {
class GUIManager;
}

PHOENIX_SINGLETON_EXTERN_TEMPLATE(gui::GUIManager);

namespace gui {

using GUIManagerPtr = std::shared_ptr<GUIManager>;

class Scene;

class GUIManager final
  : public std::enable_shared_from_this<GUIManager>
  , public PhoenixSingleton<GUIManager>
{
  friend class PhoenixSingleton<GUIManager>;
  friend class Scene; // workaround to give Scene access to m_scenes.end()

  using Clock = std::chrono::steady_clock;
  using FloatSeconds = std::chrono::duration<float, std::chrono::seconds::period>;

  logging::Logger m_logger;

  Clock::time_point m_lastTime;

  GUILoggerAdaptor m_guiLogger;
  CEGUI::OpenGL3Renderer* m_renderer; // non-owning
  CEGUI::DefaultResourceProvider m_resProvider;
  std::unique_ptr<CEGUI::XMLParser> m_xmlParser;
  CEGUI::System* m_system; // non-owning
  CEGUI::SchemeManager* m_schemeManager; // non-owning
  CEGUI::WindowManager* m_windowManager; // non-owning
  CEGUI::AnimationManager* m_animationManager; // non-owning

  CEGUI::GUIContext* m_context; // non-owning

  using SceneMap = boost::container::flat_map<std::string, std::unique_ptr<Scene>>;
  SceneMap m_scenes; // non-owning
  std::vector<Scene*> m_sceneStack; // non-owning

  glm::ivec2 m_origMousePosition;
  Flag m_mousePositionLocked;
  Flag m_ignoreNextMouseMotion;
  int m_numMouseMotionsToIgnore;

  void registerCustomWidgetTypes();

  CEGUI::Window* getInputFocusedWindow() const;

  GUIManager();
public:
  GUIManager(const GUIManager&) = delete;
  GUIManager(GUIManager&&) = delete;

  GUIManager& operator=(const GUIManager&) = delete;
  GUIManager& operator=(GUIManager&&) = delete;
  ~GUIManager();

  void setupInputHandlers(sdl::EventHandler& eventHandler);
  void initializeCustomWidgetData(graphics::ShaderManager& shaderManager);

  // Adds a scene to m_scenes keyed by scene->name()
  void addScene(std::unique_ptr<Scene> scene);
  // Constructs and adds a scene to m_scenes
  template <typename SceneType, typename... Args>
  std::enable_if_t<std::is_base_of<Scene, SceneType>::value>
  addScene(Args&&... args);
  // Removes the scene from m_scenes with the given name if it exists
  // If the scene does not exist, throws an exception
  void removeScene(const std::string& name);

  // Registers default scenes
  void registerScenes(const game::LevelManager& levelManager,
                      game::ProfileManager& profileManager);

  Scene* lookupScene(const std::string& name);

  bool sceneStackEmpty() const noexcept { return m_sceneStack.empty(); }
  // Pushes the given scene onto the scene stack and makes it active
  // Note: the lifetime of the scene object must exceed the time between
  // pushing the scene and later popping it.
  void pushSceneStack(Scene& scene);
  // Swaps the scene at the top of the stack with the one provided
  // Note: the lifetime of the given scene object must exceed the time between
  // pushing the scene and later popping it.
  // If the stack is empty when this is called, throws an exception
  Scene& swapSceneStackTop(Scene& scene);
  // Attempts to remove the scene at the top of the scene stack and sets
  // the next one as active, if it exists.
  // If the scene stack is empty when this is called, throws an exception
  // If the scene stack is empty after popping the top scene, returns false
  // Returns true otherwise
  bool popSceneStack();
  void clearSceneStack();
  Scene& activeScene() const;

  void showCursor();
  void hideCursor();

  bool isMousePositionLocked() const noexcept { return m_mousePositionLocked.get(); }
  void lockMousePosition();
  void unlockMousePosition();

  void injectTimePulse();
};

} // namespace gui

#endif // GUI_GUIMANAGER_H
