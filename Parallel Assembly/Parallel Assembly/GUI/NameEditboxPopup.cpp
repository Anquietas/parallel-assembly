#include "NameEditboxPopup.h"

#include <CEGUI/widgets/Editbox.h>
#include <CEGUI/widgets/PushButton.h>

#include "SceneComponent.inl"

namespace gui {

NameEditboxPopup::NameEditboxPopup()
  : PopupSceneComponent("NameEditboxPopup", "nameEditboxPopup.layout")
  , m_logger()
  , m_window(nullptr)
  , m_nameEditbox(nullptr)
{
  logging::setModuleName(m_logger, "NameEditboxPopup");
}

void NameEditboxPopup::initializeComponents() {
  m_window = dynamic_cast<AnimatedFrameWindow*>(root());
  if (!m_window) {
    std::ostringstream ss;
    ss << name() << " failed to load: SceneComponent root is not of type " << AnimatedFrameWindow::WidgetTypeName;
    throw MAKE_EXCEPTION(ss.str());
  }
  auto okButton = getWidget<CEGUI::PushButton>("ButtonContainer/OkButton");
  auto cancelButton = getWidget<CEGUI::PushButton>("ButtonContainer/CancelButton");
  m_nameEditbox = getWidget<CEGUI::Editbox>("NameEditbox");
  m_nameEditbox->subscribeEvent(
    CEGUI::Editbox::EventKeyDown, CEGUI::SubscriberSlot(
      [okButton, cancelButton](const CEGUI::EventArgs& e) -> bool {
        auto& args = static_cast<const CEGUI::KeyEventArgs&>(e);
        switch (args.scancode) {
        case CEGUI::Key::Scan::Return:
        case CEGUI::Key::Scan::NumpadEnter: {
          CEGUI::WindowEventArgs buttonArgs{okButton};
          okButton->fireEvent(CEGUI::PushButton::EventClicked, buttonArgs, CEGUI::PushButton::EventNamespace);
        } return true;
        case CEGUI::Key::Scan::Escape: {
          CEGUI::WindowEventArgs buttonArgs{cancelButton};
          cancelButton->fireEvent(CEGUI::PushButton::EventClicked, buttonArgs, CEGUI::PushButton::EventNamespace);
        } return true;
        default:
          return false;
        }
      }
    ));
}

void NameEditboxPopup::setCaptionText(const CEGUI::String& text) {
  if (loaded())
    m_window->setProperty<CEGUI::String>("Caption", text);
  else {
    std::ostringstream ss;
    ss << "Tried to set caption text for unloaded SceneComponent " << name();
    throw MAKE_EXCEPTION(ss.str());
  }
}

const CEGUI::Editbox* NameEditboxPopup::getEditbox() const {
  if (loaded())
    return m_nameEditbox;
  else {
    std::ostringstream ss;
    ss << "Tried to get editbox for unloaded SceneComponent " << name();
    throw MAKE_EXCEPTION(ss.str());
  }
}

void NameEditboxPopup::setEditboxText(const CEGUI::String& text) {
  if (loaded())
    m_nameEditbox->setText(text);
  else {
    std::ostringstream ss;
    ss << "Tried to set editbox text for unloaded SceneComponent " << name();
    throw MAKE_EXCEPTION(ss.str());
  }
}
const CEGUI::String& NameEditboxPopup::getEditboxText() const {
  if (loaded())
    return m_nameEditbox->getText();
  else {
    std::ostringstream ss;
    ss << "Tried to get editbox text for unloaded SceneComponent " << name();
    throw MAKE_EXCEPTION(ss.str());
  }
}

void NameEditboxPopup::setEditboxMaxTextLength(std::size_t maxLength) {
  if (loaded())
    m_nameEditbox->setMaxTextLength(maxLength);
  else {
    std::ostringstream ss;
    ss << "Tried to set editbox max text length for unloaded SceneComponent " << name();
    throw MAKE_EXCEPTION(ss.str());
  }
}

void NameEditboxPopup::show() {
  if (loaded()) {
    m_window->setPosition({{0.f,0.f},{0.f,0.f}});
    m_window->show();
    setModal();
    m_nameEditbox->activate();
    m_nameEditbox->setCaretIndex(CEGUI::String::npos);
  } else {
    std::ostringstream ss;
    ss << "Tried to show unloaded SceneComponent " << name();
    throw MAKE_EXCEPTION(ss.str());
  }
}
void NameEditboxPopup::hide() {
  if (loaded()) {
    clearModal();
    m_window->hide();
  } else {
    std::ostringstream ss;
    ss << "Tried to hide unloaded SceneComponent " << name();
    throw MAKE_EXCEPTION(ss.str());
  }
}

}  // namespace gui
