#pragma once
#ifndef GUI_ANIMATED_FRAME_WINDOW
#define GUI_ANIMATED_FRAME_WINDOW

#include <CEGUI/widgets/FrameWindow.h>

namespace gui {

// A variant of FrameWindow designed to support show/hide animations correctly
// The hide animation for the FrameWindow must set visible to false at the end
// of the animation.
class AnimatedFrameWindow : public CEGUI::FrameWindow {
  CEGUI::AnimationInstance* m_showInstance;
  CEGUI::AnimationInstance* m_hideInstance;
protected:
  void onBeginShow(CEGUI::WindowEventArgs& e);
  void onBeginHide(CEGUI::WindowEventArgs& e);
public:
  static const CEGUI::String WidgetTypeName;
  static const CEGUI::String EventNamespace;

  // Event fired on calling beginShow to trigger show animation
  static const CEGUI::String EventBeginShow;
  // Event fired on calling beginHide to trigger hide animation
  static const CEGUI::String EventBeginHide;

  AnimatedFrameWindow(const CEGUI::String& type,
                      const CEGUI::String& name);
  virtual ~AnimatedFrameWindow() override;

  // Begins animated show
  void beginShow();
  // Begins animated hide
  void beginHide();

  // Note: these will only work if accessed via AnimationFrameWindow!

  // Replacement for Window::show
  void show() { beginShow(); }
  // Replacement for Window::hide
  void hide() { beginHide(); }
};

} // namespace gui

#endif // GUI_ANIMATED_FRAME_WINDOW
