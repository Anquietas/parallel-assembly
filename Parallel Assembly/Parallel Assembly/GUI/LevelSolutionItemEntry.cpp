#include "LevelSolutionItemEntry.h"

#include <CEGUI/WindowManager.h>

#include "../Exception.h"

namespace gui {

const CEGUI::String LevelSolutionItemEntry::WidgetTypeName("PAGUI/LevelSolutionItemEntry");
const CEGUI::String LevelSolutionItemEntry::EventNamespace("LevelSolutionItemEntry");

const CEGUI::String LevelSolutionItemEntry::EventStat1TextChanged("EventStat1TextChanged");
const CEGUI::String LevelSolutionItemEntry::EventStat2TextChanged("EventStat2TextChanged");
const CEGUI::String LevelSolutionItemEntry::EventStat3TextChanged("EventStat3TextChanged");

LevelSolutionItemEntry::LevelSolutionItemEntry(
  const CEGUI::String& type,
  const CEGUI::String& name)
  : ItemEntry(type, name)
  , m_stat1Text("---")
  , m_stat2Text("---")
  , m_stat3Text("---")
{
  addProperties();
}

WidgetPtr<LevelSolutionItemEntry> LevelSolutionItemEntry::create(const CEGUI::String& name) {
  return checkedCreateWidget<LevelSolutionItemEntry>("PAGUI/LevelSolutionListboxItem", name);
}

void LevelSolutionItemEntry::addProperties() {
  const CEGUI::String& propertyOrigin = WidgetTypeName;

  CEGUI_DEFINE_PROPERTY(
    LevelSolutionItemEntry, CEGUI::String, "Stat1Text",
    "Left-most text area contents",
    &LevelSolutionItemEntry::setStat1Text, &LevelSolutionItemEntry::getStat1Text, "---");

  CEGUI_DEFINE_PROPERTY(
    LevelSolutionItemEntry, CEGUI::String, "Stat2Text",
    "Central text area contents",
    &LevelSolutionItemEntry::setStat2Text, &LevelSolutionItemEntry::getStat2Text, "---");

  CEGUI_DEFINE_PROPERTY(
    LevelSolutionItemEntry, CEGUI::String, "Stat3Text",
    "Right-most text area contents",
    &LevelSolutionItemEntry::setStat3Text, &LevelSolutionItemEntry::getStat3Text, "---");
}

void LevelSolutionItemEntry::onStat1TextChanged(CEGUI::WindowEventArgs& e) {
  fireEvent(EventStat1TextChanged, e, EventNamespace);
}
void LevelSolutionItemEntry::onStat2TextChanged(CEGUI::WindowEventArgs& e) {
  fireEvent(EventStat2TextChanged, e, EventNamespace);
}
void LevelSolutionItemEntry::onStat3TextChanged(CEGUI::WindowEventArgs& e) {
  fireEvent(EventStat3TextChanged, e, EventNamespace);
}

void LevelSolutionItemEntry::setStat1Text(const CEGUI::String& stat1Text) {
  if (m_stat1Text != stat1Text) {
    m_stat1Text = stat1Text;

    CEGUI::WindowEventArgs args(this);
    onStat1TextChanged(args);

    invalidate();
  }
}
const CEGUI::String& LevelSolutionItemEntry::getStat1Text() const {
  return m_stat1Text;
}

void LevelSolutionItemEntry::setStat2Text(const CEGUI::String& stat2Text) {
  if (m_stat2Text != stat2Text) {
    m_stat2Text = stat2Text;

    CEGUI::WindowEventArgs args(this);
    onStat2TextChanged(args);

    invalidate();
  }
}
const CEGUI::String& LevelSolutionItemEntry::getStat2Text() const {
  return m_stat2Text;
}

void LevelSolutionItemEntry::setStat3Text(const CEGUI::String& stat3Text) {
  if (m_stat3Text != stat3Text) {
    m_stat3Text = stat3Text;

    CEGUI::WindowEventArgs args(this);
    onStat3TextChanged(args);

    invalidate();
  }
}
const CEGUI::String& LevelSolutionItemEntry::getStat3Text() const {
  return m_stat3Text;
}

void LevelSolutionItemEntry::setStatTextFields(
  const CEGUI::String& stat1Text,
  const CEGUI::String& stat2Text,
  const CEGUI::String& stat3Text)
{
  setStat1Text(stat1Text);
  setStat2Text(stat2Text);
  setStat3Text(stat3Text);
}

} // namespace gui