#pragma once
#ifndef GUI_LOADING_SCENE_H
#define GUI_LOADING_SCENE_H

#include "Scene.h"

#include "../Logger.h"

namespace gui {

class LoadingScene : public Scene {
  logging::Logger m_logger;

  CEGUI::ProgressBar* m_loadingBar;
  CEGUI::DefaultWindow* m_loadingLabel;

protected:
  virtual void initializeComponents() override;
public:
  LoadingScene();
  virtual ~LoadingScene() override = default;
};

} // namespace gui

#endif // GUI_LOADING_SCENE_H
