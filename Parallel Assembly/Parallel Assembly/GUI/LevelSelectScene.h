#pragma once
#ifndef GUI_LEVEL_SELECT_SCENE_H
#define GUI_LEVEL_SELECT_SCENE_H

#include <tuple>

#include <boost/container/flat_map.hpp>

#include "Scene.h"

#include "../Logger.h"

#include "NameEditboxPopup.h"
#include "PopupSceneComponent.h"

namespace game {
class LevelManager;
class ProfileManager;
class Level;
class Profile;
}

namespace gui {

class AnimatedFrameWindow;
class ErrorTooltip;
class LevelButton;

class LevelSelectScene : public Scene {
  logging::Logger m_logger;

  const game::LevelManager* m_levelManager;
  const game::ProfileManager* m_profileManager;

  static const CEGUI::Vector2f levelButtonSize;
  static const float minLevelButtonMargin;

  CEGUI::ScrollablePane* m_levelContainer;
  CEGUI::ItemListbox* m_solutionList;
  CEGUI::PushButton* m_startButton;
  CEGUI::PushButton* m_newButton;
  CEGUI::PushButton* m_copyButton;
  CEGUI::PushButton* m_renameButton;
  CEGUI::PushButton* m_deleteButton;

  NameEditboxPopup m_nameEditboxPopup;
  enum class NameEditboxState {
    Inactive = 0,
    CreateSolution,
    CopySolution,
    RenameSolution,
  } m_nameEditboxState;

  PopupSceneComponent m_confirmDeletePopup;
  AnimatedFrameWindow* m_confirmDeleteWindow;
  CEGUI::DefaultWindow* m_confirmDeleteSolutionLabel;

  ErrorTooltip* m_errorTooltip;

  game::Profile* m_activeProfile;
  game::Level* m_selectedLevel;
  boost::container::flat_map<game::Level*, LevelButton*> m_levelButtonMap;

  inline std::size_t maxLevelButtonRowsWithoutScrollbar() const;
  inline std::tuple<
    std::size_t, // levels per row
    std::size_t, // rows required
    float        // horizontal margin
  > computeLevelButtonLayoutInfo(std::size_t numLevels) const;
  void initializeLevelButtons();

  void setLevelButtonStates();

  void showCreateSolutionWindow();
  bool showCopySolutionWindow();
  bool showRenameSolutionWindow();
  void hideNameEditboxPopup();

  bool showConfirmDeleteWindow();
  void hideConfirmDeleteWindow();

  void setSelectedLevel(game::Level* level);
  bool doStart();
  bool doCreateSolution();
  bool doCopySolution();
  bool doRenameSolution();
  void doDeleteSolution();

protected:
  virtual void initializeComponents() override;
  virtual void cleanupComponents() override;
public:
  LevelSelectScene(const game::LevelManager& levelManager,
                   const game::ProfileManager& profileManager);
  virtual ~LevelSelectScene() override = default;
};

} // namespace gui

#endif // GUI_LEVEL_SELECT_SCENE_H
