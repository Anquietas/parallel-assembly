#include "SceneComponent.inl"

#include <sstream>

#include <CEGUI/WindowManager.h>

#include "../Assert.h"
#include "../Exception.h"
#include "../Logger.h"

#include "GUIManager.h"
#include "Scene.h"
#include "stringConvert.h"

namespace gui {

SceneComponent::SceneComponent(
  std::string name_,
  const CEGUI::String& layoutFileName_,
  bool keepLoaded_,
  bool transferable_)
  : m_name(std::move(name_))
  , m_layoutFileName(layoutFileName_)
  , m_root(nullptr)
  , m_target(nullptr)
  , m_keepLoaded(keepLoaded_)
  , m_transferable(transferable_)
{}

SceneComponent::~SceneComponent() {
  if (isAttached())
    m_target->detach(*this);
  unload();
}

void SceneComponent::initializeComponents() {}
void SceneComponent::cleanupComponents() {}

void SceneComponent::load() {
  if (!loaded()) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Loading SceneComponent " << name() << " from file \"" << layoutFileName() << '\"';
    m_root = CEGUI::WindowManager::getSingleton().loadLayoutFromFile(m_layoutFileName);
    ASSERT_MSG(m_root, "CEGUI::WindowManager::loadLayoutFromFile returned nullptr");

    m_root->setDestroyedByParent(false);
    m_root->subscribeEvent(
      CEGUI::Window::EventDestroyedByParentChanged, CEGUI::SubscriberSlot(
        [this](const CEGUI::EventArgs&) -> bool {
          if (m_root->isDestroyedByParent()) {
            LOG_SEVERITY(logger::get(), Warning)
              << "Tried to set property DestroyedByParent of root widget of SceneComponent " << name()
              << " to true: this will cause crashes. Reverting to false";
            m_root->setDestroyedByParent(false);
          }
          return true;
        }
      ));

    initializeComponents();
  }
}
void SceneComponent::unload() {
  if (loaded()) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Unloading SceneComponent " << name();

    // Should normally be handled externally, but provided in case
    // component's unload() is called directly
    if (m_root->getParent()) {
      LOG_SEVERITY(logger::get(), Warning)
        << "SceneComponent " << name() << " root still has a parent on unloading; it will be detached first.";

      auto parent = m_root->getParent();
      parent->removeChild(m_root);
    }
    cleanupComponents();
    CEGUI::WindowManager::getSingleton().destroyWindow(m_root);
    m_root = nullptr;
  }
}

void SceneComponent::onAttach() {}
void SceneComponent::onDetach() {}

void SceneComponent::attach(Scene& scene) {
  if (isAttached()) {
    std::ostringstream ss;
    ss << "Tried to attach SceneComponent " << name() << " to Scene " << scene.name();
    ss << " while already attached to scene " << m_target->name();
    throw MAKE_EXCEPTION(ss.str());
  }

  m_target = &scene;

  LOG_SEVERITY(logger::get(), Debug)
    << "Attached SceneComponent " << name() << " to Scene " << m_target->name();

  onAttach();
}
void SceneComponent::detach() {
  if (isAttached()) {
    m_target = nullptr;
    onDetach();
  } else {
    std::ostringstream ss;
    ss << "Tried to detach SceneComponent " << name() << ", which wasn't previously attached to any Scene.";
    throw MAKE_EXCEPTION(ss.str());
  }
}

void SceneComponent::attachTo(Scene& scene) {
  scene.attach(*this);
}
void SceneComponent::attachToActiveScene() {
  auto guiManager = GUIManager::get();
  if (!guiManager->sceneStackEmpty())
    guiManager->activeScene().attach(*this);
  else {
    std::ostringstream ss;
    ss << "Tried to attach SceneComponent " << name() << " to the active Scene while there is no active Scene!";
    throw MAKE_EXCEPTION(ss.str());
  }
}
void SceneComponent::detachFromTarget() {
  if (isAttached())
    m_target->detach(*this);
  else {
    std::ostringstream ss;
    ss << "Tried to detach SceneComponent " << name() << ", which wasn't previously attached to any Scene.";
    throw MAKE_EXCEPTION(ss.str());
  }
}

STATIC_LOGGER_INIT(SceneComponent::logger) {
  logging::Logger lg;
  logging::setModuleName(lg, "SceneComponent");
  return lg;
}

} // namespace gui
