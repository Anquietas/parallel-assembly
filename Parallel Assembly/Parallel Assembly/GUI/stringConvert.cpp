#include "stringConvert.h"

#include <codecvt>
#include <sstream>

#include <CEGUI/String.h>

#include "../Exception.h"

#include "../StringUtils.h"

namespace gui {

namespace {
struct UTF32Converter : std::codecvt<char32_t, char, std::mbstate_t> {} converter;
}

static_assert(sizeof(CEGUI::utf32) == sizeof(char32_t), "CEGUI has weird size for UTF32 characters");

std::string toStdString(const CEGUI::String& str) {
  if (str.empty())
    return std::string();

  const CEGUI::utf32* utf32CEGUI = str.ptr();
  const char32_t* utf32Begin = reinterpret_cast<const char32_t*>(utf32CEGUI);
  const char32_t* utf32End = utf32Begin + str.size();
  
  std::string result(str.utf8_stream_len(), '\0');
  auto resultBegin = &result[0];
  auto resultEnd = resultBegin + result.size();

  std::mbstate_t state;
  const char32_t* fromNext = nullptr;
  char* toNext = nullptr;
  auto convResult = converter.out(state, utf32Begin, utf32End, fromNext, resultBegin, resultEnd, toNext);

  if (convResult != std::codecvt_base::ok) {
    std::ostringstream ss;
    ss << "Failed to convert UTF32 string " << str << " to UTF8";
    throw MAKE_EXCEPTION(ss.str());
  }

  return result;
}

CEGUI::String fromStdString(const std::string& str) {
  if (str.empty())
    return CEGUI::String();

  const char* utf8Begin = &str[0];
  const char* utf8End = utf8Begin + str.size();

  CEGUI::String result(utf32Length(str), static_cast<CEGUI::utf32>(U'\0'));
  auto resultBegin = reinterpret_cast<char32_t*>(result.ptr());
  auto resultEnd = resultBegin + result.size();

  std::mbstate_t state;
  const char* fromNext = nullptr;
  char32_t* toNext = nullptr;
  auto convResult = converter.in(state, utf8Begin, utf8End, fromNext, resultBegin, resultEnd, toNext);

  if (convResult != std::codecvt_base::ok) {
    std::ostringstream ss;
    ss << "Failed to convert UTF8 string " << str << " to UTF32";
    throw MAKE_EXCEPTION(ss.str());
  }

  return result;
}

} // namespace gui
