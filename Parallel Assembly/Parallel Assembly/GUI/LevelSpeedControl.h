#pragma once
#ifndef GUI_LEVEL_SPEED_CONTROL_H
#define GUI_LEVEL_SPEED_CONTROL_H

#include <CEGUI/widgets/DefaultWindow.h>

#include "../Game/LevelSpeed.h"

namespace gui {

class LevelSpeedControl : public CEGUI::DefaultWindow {
  game::LevelSpeed m_speed;

  CEGUI::RadioButton* m_pauseButton;
  CEGUI::RadioButton* m_stepButton;
  CEGUI::RadioButton* m_playButton;
  CEGUI::RadioButton* m_fastButton;
  CEGUI::RadioButton* m_veryFastButton;

  virtual void onLevelSpeedChanged(CEGUI::WindowEventArgs& e);
public:
  static const CEGUI::String WidgetTypeName;
  static const CEGUI::String EventNamespace;

  static const CEGUI::String EventLevelSpeedChanged;

  static const CEGUI::String PauseButtonName;
  static const CEGUI::String StepButtonName;
  static const CEGUI::String PlayButtonName;
  static const CEGUI::String FastButtonName;
  static const CEGUI::String VeryFastButtonName;

  LevelSpeedControl(const CEGUI::String& type, const CEGUI::String& name);
  virtual ~LevelSpeedControl() override = default;

  virtual void initialiseComponents() override;

  void setLevelSpeed(game::LevelSpeed speed);
  game::LevelSpeed getLevelSpeed() const;
};

} // namespace gui

#endif // GUI_LEVEL_SPEED_CONTROL_H
