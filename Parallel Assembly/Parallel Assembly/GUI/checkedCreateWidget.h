#pragma once
#ifndef GUI_CHECKED_CREATE_WIDGET_H
#define GUI_CHECKED_CREATE_WIDGET_H

#include <type_traits>
#include <memory>

#include <CEGUI/Window.h>
#include <CEGUI/WindowManager.h>

#include "../Assert.h"
#include "../Exception.h"

namespace gui {

void destroyWidget(CEGUI::Window* widget);

template <typename T>
using WidgetPtr = std::unique_ptr<T, decltype(&destroyWidget)>;

// TODO: add a detection check for ExpectedType::WidgetTypeName
template <typename ExpectedType>
std::enable_if_t<
  std::is_base_of<CEGUI::Window, std::decay_t<ExpectedType>>::value,
  WidgetPtr<std::decay_t<ExpectedType>>>
  checkedCreateWidget(const CEGUI::String& type, const CEGUI::String& name = "")
{
  using ResultType = std::decay_t<ExpectedType>*;

  auto& windowManager = CEGUI::WindowManager::getSingleton();

  WidgetPtr<CEGUI::Window>
    window{windowManager.createWindow(type, name), &destroyWidget};
  ASSERT_MSG(window, "CEGUI::WindowManager::createWindow returned nullptr");
  auto widget = dynamic_cast<ResultType>(window.get());
  if (!widget) {
    std::ostringstream ss;
    ss << "Failed to create widget " << window->getName() << " of type ";
    ss << type << ": type " << type << " does not correspond to expected type ";
    ss << std::decay_t<ExpectedType>::WidgetTypeName;
    throw MAKE_EXCEPTION(ss.str());
  }
  return {(window.release(), widget), &destroyWidget};
}

} // namespace gui

#endif // GUI_CHECKED_CREATE_WIDGET_H
