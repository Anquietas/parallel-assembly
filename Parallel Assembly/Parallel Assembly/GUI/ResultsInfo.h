#pragma once
#ifndef GUI_RESULTS_INFO_H
#define GUI_RESULTS_INFO_H

#include "RenderableWindow.h"

namespace gui {

class ResultsInfo : public RenderableWindow {
  using BaseType = RenderableWindow;

  virtual void renderWindow() override;
public:
  static const CEGUI::String WidgetTypeName;
  static const CEGUI::String EventNamespace;

  ResultsInfo(const CEGUI::String& type, const CEGUI::String& name);
  virtual ~ResultsInfo() override;
};

} // namespace gui

#endif // GUI_RESULTS_INFO
