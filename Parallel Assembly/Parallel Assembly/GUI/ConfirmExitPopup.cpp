#include "ConfirmExitPopup.h"

#include <utility>
#include <string>

#include <CEGUI/WindowManager.h>
#include <CEGUI/widgets/PushButton.h>

#include "GUIManager.h"
#include "Scene.h"
#include "SceneComponent.inl"

#include "../Assert.h"

namespace gui {

using namespace std::string_literals;

ConfirmExitPopup::ConfirmExitPopup()
  : PopupSceneComponent("ConfirmExitPopup"s, "confirmExit.layout", true)
  , m_logger()
  , m_exitConfirmed(false)
  , m_window(nullptr)
{
  logging::setModuleName(m_logger, "ConfirmExitPopup");
  load(); // ConfirmExitPopup is always loaded
}

void ConfirmExitPopup::initializeComponents() {
  m_window = dynamic_cast<AnimatedFrameWindow*>(root());
  if (!m_window) {
    std::ostringstream ss;
    ss << name() << " failed to load: SceneComponent root is not of type " << AnimatedFrameWindow::WidgetTypeName;
    throw MAKE_EXCEPTION(ss.str());
  }
  auto exitButton = getWidget<CEGUI::PushButton>("ExitButton");
  auto cancelButton = getWidget<CEGUI::PushButton>("CancelButton");
  auto closeButton = getWidget<CEGUI::PushButton>("__auto_closebutton__");

  exitButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [&exitConfirmed = m_exitConfirmed](const CEGUI::EventArgs&) -> bool {
        exitConfirmed = true;
        return true;
      }
    ));
  auto cancelHandler = [this](const CEGUI::EventArgs&) -> bool {
    hide();
    return true;
  };
  cancelButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(cancelHandler));
  closeButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(cancelHandler));

  m_window->subscribeEvent(
    CEGUI::Window::EventHidden, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        detachFromTarget();
        return true;
      }
    ));
  m_window->subscribeEvent(
    CEGUI::Window::EventKeyDown, CEGUI::SubscriberSlot(
      [exitButton, cancelButton](const CEGUI::EventArgs& e) -> bool {
        auto& args = static_cast<const CEGUI::KeyEventArgs&>(e);
        switch (args.scancode) {
        case CEGUI::Key::Scan::Return:
        case CEGUI::Key::Scan::NumpadEnter: {
          CEGUI::WindowEventArgs buttonArgs{exitButton};
          exitButton->fireEvent(CEGUI::PushButton::EventClicked, buttonArgs, CEGUI::PushButton::EventNamespace);
        } return true;
        case CEGUI::Key::Scan::Escape: {
          CEGUI::WindowEventArgs buttonArgs{cancelButton};
          cancelButton->fireEvent(CEGUI::PushButton::EventClicked, buttonArgs, CEGUI::PushButton::EventNamespace);
        } return true;
        default:
          return false;
        }
      }
    ));

  LOG_SEVERITY(m_logger, Debug) << "Initialized ConfirmExitPopup components";
}

void ConfirmExitPopup::show() {
  ASSERT(m_window);
  m_window->setPosition({{0.f,0.f},{0.f,0.f}});
  m_window->show();
  m_window->activate();
  setModal();
}
void ConfirmExitPopup::hide() {
  ASSERT(m_window);
  clearModal();
  m_window->hide();
}

} // namespace gui
