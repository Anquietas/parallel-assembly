#pragma once
#ifndef GUI_GUI_LOGGER_ADAPTOR_H
#define GUI_GUI_LOGGER_ADAPTOR_H

#include <CEGUI/Logger.h>

#include "../Logger.h"

namespace gui {

// Adaptor to redirect all CEGUI logging to main application logs
class GUILoggerAdaptor final : public CEGUI::Logger {
  logging::ThreadsafeLogger m_logger;

public:
  GUILoggerAdaptor();
  virtual ~GUILoggerAdaptor() override = default;

  virtual void logEvent(const CEGUI::String& message,
                        CEGUI::LoggingLevel level = CEGUI::Standard) override;
  // No-op since this is not useful to us
  virtual void setLogFilename(const CEGUI::String& filename,
                              bool append=false) override;
};

} // namespace gui

#endif // GUI_GUI_LOGGER_ADAPTOR_H
