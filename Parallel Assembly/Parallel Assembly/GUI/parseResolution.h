#pragma once

#include "../GLMIncludes.h"

namespace gui {

// Tries to parse a string representing a resolution of the form wwww x hhhh
// Returns (-1, -1) on failure
glm::ivec2 parseResolution(const char* begin, const char* end);

} // namespace gui
