#include "SwitchProfileScene.h"

#include <CEGUI/widgets/ItemListbox.h>
#include <CEGUI/widgets/DefaultWindow.h>
#include <CEGUI/widgets/PushButton.h>
#include <CEGUI/widgets/FrameWindow.h>
#include <CEGUI/widgets/Editbox.h>
#include <CEGUI/widgets/ItemEntry.h>
#include <CEGUI/WindowManager.h>

#include "../Assert.h"

#include "../Game/ProfileManager.h"

#include "stringConvert.h"

#include "AnimatedFrameWindow.h"
#include "ErrorTooltip.h"

#include "Scene.inl"
#include "SceneNames.h"

namespace gui {

static const CEGUI::String noCurrentProfileText("No profile selected");

SwitchProfileScene::SwitchProfileScene(game::ProfileManager& profileManager)
  : Scene(switchProfileSceneName, "switchProfile.layout")
  , m_logger()
  , m_profileManager(&profileManager)
  , m_currentProfileLabel(nullptr)
  , m_profileList(nullptr)
  , m_nameEditboxPopup()
  , m_isEditing(false)
  , m_confirmDeletePopup("SwitchProfile.ConfirmDeleteWindow", "switchProfile-confirmDelete.layout")
  , m_confirmDeleteWindow(nullptr)
  , m_confirmDeleteProfileLabel(nullptr)
  , m_errorTooltip(nullptr)
{
  logging::setModuleName(m_logger, "SwitchProfileScene");
  attach(m_nameEditboxPopup);
  attach(m_confirmDeletePopup);
}

void SwitchProfileScene::initializeComponents() {
  ASSERT(m_profileManager);

  auto frame = sceneRoot()->getChild("SelectProfileFrame");

  m_currentProfileLabel = getWidget<CEGUI::DefaultWindow>("SelectProfileFrame/CurrentProfileLabel");
  m_profileList = getWidget<CEGUI::ItemListbox>("SelectProfileFrame/ProfileList");
  auto newButton = getWidget<CEGUI::PushButton>("SelectProfileFrame/NewButton");
  auto editButton = getWidget<CEGUI::PushButton>("SelectProfileFrame/EditButton");
  auto deleteButton = getWidget<CEGUI::PushButton>("SelectProfileFrame/DeleteButton");
  auto okButton = getWidget<CEGUI::PushButton>("SelectProfileFrame/OkButton");
  auto cancelButton = getWidget<CEGUI::PushButton>("SelectProfileFrame/CancelButton");

  ASSERT(m_nameEditboxPopup.loaded());
  auto nameEditboxOkButton = m_nameEditboxPopup.getWidget<CEGUI::PushButton>("ButtonContainer/OkButton");
  auto nameEditboxCancelButton = m_nameEditboxPopup.getWidget<CEGUI::PushButton>("ButtonContainer/CancelButton");
  auto nameEditboxCloseButton = m_nameEditboxPopup.getWidget<CEGUI::PushButton>("__auto_closebutton__");

  ASSERT(m_confirmDeletePopup.loaded());
  m_confirmDeleteWindow = m_confirmDeletePopup.getWidget<AnimatedFrameWindow>("");
  m_confirmDeleteProfileLabel = m_confirmDeletePopup.getWidget<CEGUI::DefaultWindow>("ProfileLabel");
  auto confirmDeleteYesButton = m_confirmDeletePopup.getWidget<CEGUI::PushButton>("ButtonContainer/YesButton");
  auto confirmDeleteCancelButton = m_confirmDeletePopup.getWidget<CEGUI::PushButton>("ButtonContainer/CancelButton");
  auto confirmDeleteCloseButton = m_confirmDeletePopup.getWidget<CEGUI::PushButton>("__auto_closebutton__");

  auto errorTooltip = ErrorTooltip::create();
  m_errorTooltip = errorTooltip.get();
  sceneRoot()->addChild(m_errorTooltip);
  errorTooltip.release();

  initializeProfilesList();

  newButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        return showNewEditProfileWindow(false);
      }
    ));
  editButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        return showNewEditProfileWindow(true);
      }
    ));
  deleteButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        return showConfirmDeleteWindow();
      }
    ));
  okButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        ASSERT(m_profileManager);
        bool result = doSelectProfile();
        if (result) {
          ASSERT(m_profileManager->activeProfile());
          switchToPreviousScene();
        } else
          m_errorTooltip->showAtRightEdge(m_profileList, "You must select a profile to continue");
        return result;
      }
    ));
  cancelButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        ASSERT(m_profileManager);
        if (m_profileManager->activeProfile())
          switchToPreviousScene();
        else
          m_errorTooltip->showAtRightEdge(m_profileList, "You must select a profile to continue");
        return true;
      }
    ));
  
  nameEditboxOkButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        bool result = m_isEditing ? doEditProfile() : doCreateProfile();
        if (result)
          hideNewEditProfileWindow();
        return result;
      }
    ));
  auto newEditCancelHandler = [this](const CEGUI::EventArgs&) -> bool {
    hideNewEditProfileWindow();
    return true;
  };
  nameEditboxCancelButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(newEditCancelHandler));
  nameEditboxCloseButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(newEditCancelHandler));

  m_nameEditboxPopup.setEditboxMaxTextLength(game::Profile::maxNameUTF32Length);

  confirmDeleteYesButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        doDeleteProfile();
        hideConfirmDeleteWindow();
        return true;
      }
    ));
  auto confirmDeleteCancelHandler = [this](const CEGUI::EventArgs&) -> bool {
    hideConfirmDeleteWindow();
    return true;
  };
  confirmDeleteCancelButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(confirmDeleteCancelHandler));
  confirmDeleteCloseButton->subscribeEvent(
    CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(confirmDeleteCancelHandler));

  frame->activate();

  LOG_SEVERITY(m_logger, Debug) << "Initialized components";
}
void SwitchProfileScene::cleanupComponents() {
  ASSERT(!m_nameEditboxPopup.loaded());
  ASSERT(!m_confirmDeletePopup.loaded());
}

CEGUI::ItemEntry* SwitchProfileScene::createListboxItem() {
  auto itemWindow = CEGUI::WindowManager::getSingleton().createWindow("PAGUI/ListboxItem");
  if (!itemWindow)
    throw MAKE_EXCEPTION("Failed to create ListboxItem: PAGUI/ListboxItem is not defined");
  auto item = dynamic_cast<CEGUI::ItemEntry*>(itemWindow);
  if (!item)
    throw MAKE_EXCEPTION("Failed to create ListboxItem: PAGUI/ListboxItem is not an ItemEntry widget");
  return item;
}

void SwitchProfileScene::initializeProfilesList() {
  ASSERT(m_profileManager);
  auto& profilesMap = m_profileManager->getProfiles();
  auto activeProfile = m_profileManager->activeProfile();

  if (activeProfile)
    m_currentProfileLabel->setText(fromStdString(activeProfile->name()));
  else
    m_currentProfileLabel->setText(noCurrentProfileText);

  for (auto& pair : profilesMap) {
    auto listItem = createListboxItem();
    ASSERT(pair.first == pair.second.name());
    listItem->setText(fromStdString(pair.first));
    m_profileList->addItem(listItem);

    if (&pair.second == activeProfile)
      listItem->setSelected(true);
  }
  m_profileList->handleUpdatedItemData(true);
}

bool SwitchProfileScene::showNewEditProfileWindow(bool edit) {
  CEGUI::String caption;
  if (edit) {
    auto selection = m_profileList->getFirstSelectedItem();
    if (!selection)
      return false;

    LOG_SEVERITY(m_logger, Debug)
      << "Showing edit profile window for profile \"" << selection->getText() << '\"';
    caption = "Edit Profile";
    m_nameEditboxPopup.setEditboxText(selection->getText());
  } else {
    LOG_SEVERITY(m_logger, Debug) << "Showing create profile window";
    caption = "Create Profile";
    m_nameEditboxPopup.setEditboxText(CEGUI::String());
  }
  m_isEditing = edit;
  m_nameEditboxPopup.setCaptionText(caption);
  m_nameEditboxPopup.show();
  return true;
}
void SwitchProfileScene::hideNewEditProfileWindow() {
  m_nameEditboxPopup.hide();
  if (m_errorTooltip->isVisible())
    m_errorTooltip->hideTooltip();
}

bool SwitchProfileScene::showConfirmDeleteWindow() {
  ASSERT(!m_confirmDeleteWindow->isVisible());

  auto selection = m_profileList->getFirstSelectedItem();
  if (!selection)
    return false;

  m_confirmDeleteProfileLabel->setText(selection->getText());
  m_confirmDeleteWindow->show();
  m_confirmDeleteWindow->activate();
  m_confirmDeletePopup.setModal();
  return true;
}
void SwitchProfileScene::hideConfirmDeleteWindow() {
  m_confirmDeletePopup.clearModal();
  m_confirmDeleteWindow->hide();
}

bool SwitchProfileScene::doCreateProfile() {
  ASSERT(m_profileManager);
  ASSERT(!m_isEditing);
  
  auto& profileName = m_nameEditboxPopup.getEditboxText();
  LOG_SEVERITY(m_logger, Debug)
    << "Creating profile \"" << profileName << '\"';
  if (profileName.empty() || profileName == "") {
    static const CEGUI::String errorText = "A profile name can't be empty";
    m_errorTooltip->showAtRightEdge(m_nameEditboxPopup.getEditbox(), errorText);
    return false;
  }

  bool result = m_profileManager->createProfile(toStdString(profileName));
  if (result) {
    auto item = createListboxItem();
    item->setText(profileName);
    m_profileList->addItem(item);
    m_profileList->handleUpdatedItemData(true);

    m_profileManager->saveProfiles();
  } else {
    static const CEGUI::String errorText =
      "A profile with that name already exists";
    m_errorTooltip->showAtRightEdge(m_nameEditboxPopup.getEditbox(), errorText);
  }

  return result;
}
bool SwitchProfileScene::doEditProfile() {
  ASSERT(m_profileManager);
  ASSERT(m_isEditing);

  auto selection = m_profileList->getFirstSelectedItem();
  ASSERT(selection);

  auto& oldName = selection->getText();
  auto& newName = m_nameEditboxPopup.getEditboxText();

  LOG_SEVERITY(m_logger, Debug)
    << "Editing profile \"" << oldName << "\" to \"" << newName << '\"';

  if (newName.empty() || newName == "") {
    static const CEGUI::String errorText = "A profile name can't be empty";
    m_errorTooltip->showAtRightEdge(m_nameEditboxPopup.getEditbox(), errorText);
    return false;
  }

  auto result = m_profileManager->renameProfile(toStdString(oldName), toStdString(newName));

  if (!result.first) {
    std::ostringstream ss;
    ss << "Error renaming profile: profile \"" << oldName << "\" present in UI profile list,\n";
    ss << "but not present in ProfileManager";
    throw MAKE_EXCEPTION(ss.str());
  } else if (!result.second) {
    static const CEGUI::String errorText =
      "A profile with that name already exists";
    m_errorTooltip->showAtRightEdge(m_nameEditboxPopup.getEditbox(), errorText);
  } else {
    ASSERT(result.first && result.second);

    selection->setText(newName);
    m_profileList->handleUpdatedItemData(true);

    m_profileManager->saveProfiles();
  }

  return result.second;
}
void SwitchProfileScene::doDeleteProfile() {
  ASSERT(m_profileManager);
  ASSERT(m_confirmDeleteWindow->isVisible());

  auto selection = m_profileList->getFirstSelectedItem();
  ASSERT(selection);

  auto& profileName = selection->getText();

  LOG_SEVERITY(m_logger, Debug)
    << "Deleting profile \"" << profileName << '\"';

  if (!m_profileManager->removeProfile(toStdString(profileName))) {
    std::ostringstream ss;
    ss << "Error deleting profile: profile \"" << profileName
        << "\" present in UI profile list,\nbut not present in ProfileManager";
    throw MAKE_EXCEPTION(ss.str());
  }
  m_profileManager->saveProfiles();

  m_profileList->removeItem(selection);
  // Don't need to re-sort since deletion won't affect order
  m_profileList->handleUpdatedItemData(false);

  // Set list selection back to currently active profile if there is one
  auto activeProfile = m_profileManager->activeProfile();
  if (activeProfile) {
    auto activeProfileName = fromStdString(activeProfile->name());
    std::size_t numItems = m_profileList->getItemCount();
    for (std::size_t index = 0; index < numItems; ++index) {
      auto item = m_profileList->getItemFromIndex(index);
      if (item->getText() == activeProfileName) {
        item->setSelected(true);
        break;
      }
    }
  } else
    m_currentProfileLabel->setText(noCurrentProfileText);
}
bool SwitchProfileScene::doSelectProfile() {
  ASSERT(m_profileManager);

  auto selection = m_profileList->getFirstSelectedItem();
  if (!selection)
    return false;

  auto& profileName = selection->getText();

  auto selected = m_profileManager->lookupProfile(toStdString(profileName));
  if (!selected) {
    std::ostringstream ss;
    ss << "Error selecting profile: profile \"" << profileName
       << "\" present in UI profile list,\nbut not present in ProfileManager";
    throw MAKE_EXCEPTION(ss.str());
  }
  m_profileManager->setActiveProfile(selected);
  m_profileManager->saveProfiles();

  return true;
}

} // namespace gui
