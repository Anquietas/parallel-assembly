#include "Scene.inl"

#include <algorithm>

#include <boost/core/ignore_unused.hpp>

#include <CEGUI/WindowManager.h>

#include "../Assert.h"

#include "GUIManager.h"
#include "SceneComponent.h"
#include "SceneNames.h"

namespace gui {

Scene::Scene(const std::string& name_, const CEGUI::String& layoutFileName_)
  : m_name(name_)
  , m_layoutFileName(layoutFileName_)
  , m_sceneRoot(nullptr) // Loaded via load() when needed!
  , m_active(false)
  , m_components()
{
  LOG_SEVERITY(logger::get(), Debug)
    << "Created scene \"" << name_ << "\" from layout \"" << m_layoutFileName << '\"';
}
Scene::~Scene() {
  LOG_SEVERITY(logger::get(), Debug)
    << "Destroying scene \"" << name() << '\"';
  while (!m_components.empty()) {
    ASSERT(m_components.back());
    detach(*m_components.back());
  }
  unload();
}

void Scene::initializeComponents() {
  LOG_SEVERITY(logger::get(), Warning)
    << "Scene::initializeComponents() called; this suggests a derived class forgot to override!";
}
void Scene::cleanupComponents() {
  // Do nothing
}
void Scene::postUnloadCleanup() {
  // Do nothing
}

void Scene::load() {
  if (!loaded() && !m_layoutFileName.empty()) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Loading scene \"" << name() << "\" from file \"" << layoutFileName() << '\"';

    auto& windowManager = CEGUI::WindowManager::getSingleton();
    m_sceneRoot = windowManager.loadLayoutFromFile(m_layoutFileName);
    ASSERT(m_sceneRoot);

    for (auto component : m_components) {
      ASSERT(component);
      if (!component->loaded())
        component->load();
      ASSERT(component->loaded());

      attachWindow(*component->root());
    }
  }
}
void Scene::unload() {
  if (loaded()) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Unloading scene \"" << name() << '\"';

    for (auto component : m_components) {
      ASSERT(component);
      ASSERT(component->loaded());
      detachWindow(*component->root());
      if (!component->keepLoaded())
        component->unload();
    }

    cleanupComponents();
    CEGUI::WindowManager::getSingleton().destroyWindow(m_sceneRoot);
    m_sceneRoot = nullptr;
    postUnloadCleanup();
  }
}

void Scene::attach(SceneComponent& component) {
  ASSERT(!component.isAttached());
  auto iter = std::find(m_components.begin(), m_components.end(), &component);
  if (iter != m_components.end()) {
    std::ostringstream ss;
    ss << "Tried to attach component " << component.name() << " to scene " << name()
       << ": component already attached";
    throw MAKE_EXCEPTION(ss.str());
  }
  m_components.push_back(&component);
  component.attach(*this);
  if (loaded()) {
    if (!component.loaded())
      component.load();
    ASSERT(component.root());

    attachWindow(*component.root());
  }
}
void Scene::detachComponent(ComponentsVector::iterator componentIter) {
  ASSERT(m_components.begin() <= componentIter && componentIter < m_components.end());
  
  auto component = *componentIter;
  ASSERT(component);
  component->detach();
  if (loaded()) {
    ASSERT(component->loaded());

    detachWindow(*component->root());
    if (!component->keepLoaded())
      component->unload();
  }
}
void Scene::detach(SceneComponent& component) {
  auto iter = std::find(m_components.begin(), m_components.end(), &component);
  if (!component.isAttached() || iter == m_components.end()) {
    std::ostringstream ss;
    ss << "Tried to detach component " << component.name() << " from scene " << name()
       << ": component was not previously attached";
    throw MAKE_EXCEPTION(ss.str());
  }
  detachComponent(iter);
  m_components.erase(iter);
}

void Scene::attachWindow(CEGUI::Window& window) {
  if (!loaded()) {
    std::ostringstream ss;
    ss << "Tried to attach a window to unloaded scene \"" << name() << '\"';
    throw MAKE_EXCEPTION(ss.str());
  }
  
  LOG_SEVERITY(logger::get(), Debug)
    << "Attaching window \"" << window.getName() << "\" to scene \"" << name() << '\"';

  m_sceneRoot->addChild(&window);
}
void Scene::detachWindow(CEGUI::Window& window) {
  if (!loaded()) {
    std::ostringstream ss;
    ss << "Tried to detach a window from unloaded scene \"" << name() << '\"';
    throw MAKE_EXCEPTION(ss.str());
  }
  if (!m_sceneRoot->isChild(&window)) {
    std::ostringstream ss;
    ss << "Tried to detach a window from scene \"" << name() << "\" that was not previously attached";
    throw MAKE_EXCEPTION(ss.str());
  }

  LOG_SEVERITY(logger::get(), Debug)
    << "Detaching window \"" << window.getName() << "\" from scene \"" << name() << '\"';

  m_sceneRoot->removeChild(&window);
}

void Scene::transferTransferableComponents(Scene& scene) {
  // Partition the components vector to make this easier
  auto end = m_components.end();
  auto transferableBegin = std::partition(
    m_components.begin(), end,
    [](const SceneComponent* component) -> bool {
      ASSERT(component);
      // Return negation to put the transferrable components at the END of the vector
      return !component->transferable();
    });
  // Nothing to do if there are no transferable components
  if (transferableBegin == m_components.end())
    return;
  // Actually transfer the transferrable components;
  for (auto iter = transferableBegin; iter != end; ++iter) {
    auto component = *iter;
    ASSERT(component);
    detachComponent(iter); // skips the find/erase steps
    scene.attach(*component);
  }
  // Remove the transferred components
  m_components.erase(transferableBegin, end);
}

void Scene::switchToScene(const std::string& sceneName) {
  auto guiManager = GUIManager::get();
  ASSERT(!guiManager->sceneStackEmpty());
  ASSERT(&guiManager->activeScene() == this);
  auto scene = guiManager->lookupScene(sceneName);
  if (scene)
    switchToScene(*scene);
  else {
    std::ostringstream ss;
    ss << "Tried to switch to scene " << sceneName << ": scene does not exist";
    throw MAKE_EXCEPTION(ss.str());
  }
}
void Scene::switchToScene(Scene& scene) {
  ASSERT(&scene != this);

  transferTransferableComponents(scene);

  auto guiManager = GUIManager::get();
  guiManager->pushSceneStack(scene);
}
void Scene::switchToPreviousScene() {
  auto guiManager = GUIManager::get();
  ASSERT(!guiManager->sceneStackEmpty());
  if (!guiManager->popSceneStack()) {
    auto scene = guiManager->lookupScene(mainMenuSceneName);
    if (scene)
      guiManager->pushSceneStack(*scene);
  }
  transferTransferableComponents(guiManager->activeScene());
}

STATIC_LOGGER_INIT(Scene::logger) {
  logging::Logger lg;
  logging::setModuleName(lg, "Scene");
  return lg;
}

} // namespace gui
