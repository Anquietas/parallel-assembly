#pragma once
#ifndef GUI_GUIMANAGER_INL
#define GUI_GUIMANAGER_INL

#include "GUIManager.h"

#include <type_traits>

namespace gui {

template <typename SceneType, typename... Args>
std::enable_if_t<std::is_base_of<Scene, SceneType>::value>
GUIManager::addScene(Args&&... args) {
  addScene(std::make_unique<SceneType>(std::forward<Args>(args)...));
}

} // namespace gui

#endif // GUI_GUIMANAGER_INL
