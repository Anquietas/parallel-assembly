#include "LevelSpeedControl.h"

#include <CEGUI/widgets/RadioButton.h>

#include "../Assert.h"

namespace gui {

const CEGUI::String LevelSpeedControl::WidgetTypeName("PAGUI/LevelSpeedControl");
const CEGUI::String LevelSpeedControl::EventNamespace("LevelSpeedControl");

const CEGUI::String LevelSpeedControl::EventLevelSpeedChanged("LevelSpeedChanged");

const CEGUI::String LevelSpeedControl::PauseButtonName("__auto_pausebutton__");
const CEGUI::String LevelSpeedControl::StepButtonName("__auto_stepbutton__");
const CEGUI::String LevelSpeedControl::PlayButtonName("__auto_playbutton__");
const CEGUI::String LevelSpeedControl::FastButtonName("__auto_fastbutton__");
const CEGUI::String LevelSpeedControl::VeryFastButtonName("__auto_veryfastbutton__");

LevelSpeedControl::LevelSpeedControl(
  const CEGUI::String& type, const CEGUI::String& name)
  : DefaultWindow(type, name)
  , m_speed(game::LevelSpeed::Pause)
  , m_pauseButton(nullptr)
  , m_stepButton(nullptr)
  , m_playButton(nullptr)
  , m_fastButton(nullptr)
  , m_veryFastButton(nullptr)
{}

void LevelSpeedControl::initialiseComponents() {
  using game::LevelSpeed;

  m_pauseButton = &dynamic_cast<CEGUI::RadioButton&>(*getChild(PauseButtonName));
  m_stepButton = &dynamic_cast<CEGUI::RadioButton&>(*getChild(StepButtonName));
  m_playButton = &dynamic_cast<CEGUI::RadioButton&>(*getChild(PlayButtonName));
  m_fastButton = &dynamic_cast<CEGUI::RadioButton&>(*getChild(FastButtonName));
  m_veryFastButton = &dynamic_cast<CEGUI::RadioButton&>(*getChild(VeryFastButtonName));
  
  static const auto& EventSelectStateChanged = CEGUI::RadioButton::EventSelectStateChanged;

  m_pauseButton->subscribeEvent(
    EventSelectStateChanged, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        if (m_pauseButton->isSelected())
          setLevelSpeed(LevelSpeed::Pause);
        return true;
      }
    ));
  m_stepButton->subscribeEvent(
    EventSelectStateChanged, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        if (m_stepButton->isSelected())
          setLevelSpeed(LevelSpeed::Step);
        return true;
      }
    ));
  m_playButton->subscribeEvent(
    EventSelectStateChanged, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        if (m_playButton->isSelected())
          setLevelSpeed(LevelSpeed::Play);
        return true;
      }
    ));
  m_fastButton->subscribeEvent(
    EventSelectStateChanged, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        if (m_fastButton->isSelected())
          setLevelSpeed(LevelSpeed::Fast);
        return true;
      }
    ));
  m_veryFastButton->subscribeEvent(
    EventSelectStateChanged, CEGUI::SubscriberSlot(
      [this](const CEGUI::EventArgs&) -> bool {
        if (m_veryFastButton->isSelected())
          setLevelSpeed(LevelSpeed::VeryFast);
        return true;
      }
    ));

  if (m_pauseButton->isSelected())
    setLevelSpeed(LevelSpeed::Pause);
  else if (m_stepButton->isSelected())
    setLevelSpeed(LevelSpeed::Step);
  else if (m_playButton->isSelected())
    setLevelSpeed(LevelSpeed::Play);
  else if (m_fastButton->isSelected())
    setLevelSpeed(LevelSpeed::Fast);
  else {
    ASSERT(m_veryFastButton->isSelected());
    setLevelSpeed(LevelSpeed::VeryFast);
  }
}

void LevelSpeedControl::onLevelSpeedChanged(CEGUI::WindowEventArgs& e) {
  using game::LevelSpeed;

  switch (m_speed) {
  case LevelSpeed::Pause:
    m_pauseButton->setSelected(true);
    break;
  case LevelSpeed::Step:
    m_stepButton->setSelected(true);
    break;
  case LevelSpeed::Play:
    m_playButton->setSelected(true);
    break;
  case LevelSpeed::Fast:
    m_fastButton->setSelected(true);
    break;
  case LevelSpeed::VeryFast:
    m_veryFastButton->setSelected(true);
    break;
  default:
    ASSERT(false);;
  }

  fireEvent(EventLevelSpeedChanged, e, EventNamespace);
}

void LevelSpeedControl::setLevelSpeed(game::LevelSpeed speed) {
  if (m_speed != speed) {
    m_speed = speed;

    CEGUI::WindowEventArgs args{this};
    onLevelSpeedChanged(args);
  }
}
game::LevelSpeed LevelSpeedControl::getLevelSpeed() const {
  return m_speed;
}

} // namespace gui
