#include "UnitTests.h"

#include "testImageFormatConversion.h"

namespace test {

void runAllUnitTests() {
#ifndef NDEBUG
  testImageFormatConversion();
#endif
}

}
