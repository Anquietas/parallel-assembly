#include "../Assert.h"

#include "../Graphics/OpenGL/ImageFormat.h"

namespace test {

void testImageFormatConversion() {
  using namespace graphics::gl;
  // Check Colour format mappings
  for (enum_t i = 0; i <= static_cast<enum_t>(internal_format::Colour::RGB_32ui); ++i) {
    auto format = static_cast<internal_format::Colour>(i);
    ASSERT(format == getColourFormat(getFormatCode(format)));
  }

  // Check DepthStencil format mappings
  for (enum_t i = 0; i <= static_cast<enum_t>(internal_format::DepthStencil::Depth_32f_Stencil_8); ++i) {
    auto format = static_cast<internal_format::DepthStencil>(i);
    ASSERT(format == getDepthStencilFormat(getFormatCode(format)));
  }
}

}
