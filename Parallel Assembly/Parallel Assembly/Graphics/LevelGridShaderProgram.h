#pragma once
#ifndef GUI_LEVEL_GRID_SHADER_PROGRAM
#define GUI_LEVEL_GRID_SHADER_PROGRAM

#include "ShaderProgramBase.h"

#include "OpenGL/ShaderProgram.h"
#include "OpenGL/VertexArrayObject.h"
#include "OpenGL/VertexBuffer.h"

namespace graphics {

class LevelGridShaderProgram : public ShaderProgramBase {
  gl::ShaderProgram m_shader;
  gl::AttributeID m_vertexPosLoc;
  gl::AttributeID m_colourLoc;
  gl::UniformID m_mvpMatrixUniformLoc;
  gl::UniformID m_gridCellSizeUniformLoc;
  gl::UniformID m_gridWidthUniformLoc;

public:
  LevelGridShaderProgram();
  LevelGridShaderProgram(LevelGridShaderProgram&& o) = default;
  virtual ~LevelGridShaderProgram() override = default;

  LevelGridShaderProgram& operator=(LevelGridShaderProgram&& o) = default;

  virtual void initialize() override;
  virtual void cleanup() override;

  virtual void preRender() override;
  virtual void postRender() override;

  // Only valid after initialization

  const gl::AttributeID& vertexPosAttribID() const noexcept { return m_vertexPosLoc; }
  const gl::AttributeID& colourAttribID() const noexcept { return m_colourLoc; }

  // The following functions should only be called between preRender() and postRender()

  // Sets the shader's mvpMatrix uniform to the given value
  void setMVPMatrix(const glm::mat4& mvpMatrix);
  // Sets the shader's gridCellSize uniform to the given value
  void setGridCellSize(gl::float_t gridCellSize);
  // Sets the shader's gridSize uniform to the given value
  void setGridSize(const glm::ivec2& gridSize);
};

} // namespace graphics

#endif // GUI_LEVEL_GRID_SHADER_PROGRAM
