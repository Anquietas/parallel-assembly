#pragma once
#ifndef RENDERER_H
#define RENDERER_H

#include <vector>
#include <memory>

#include "OpenGL/Query.h"

#include "FPSLimiter.h"
#include "RenderStage.h"

#include "../Logger.h"

namespace graphics {

class Renderer final {
  glm::ivec2 m_screenSize;
  std::vector<std::unique_ptr<RenderStage>> m_stages;
  FPSLimiter m_fpsLimiter;

  struct QuerySet {
    gl::NumPrimitivesGeneratedQuery primitivesGenerated;
    gl::NumSamplesPassedQuery samplesPassed;
    gl::TimeElapsedQuery timeElapsed;

    QuerySet()
      : primitivesGenerated(nullptr)
      , samplesPassed(nullptr)
      , timeElapsed(nullptr)
    {}
  } m_queries;
  bool m_perfTrackingEnabled;
  gl::uint_t m_primitivesGenerated;
  gl::uint64_t m_samplesPassed;
  gl::uint64_t m_timeElapsed;

  logging::Logger m_logger;

public:
  Renderer(const glm::ivec2& screenSize);
  Renderer(const Renderer&) = delete;
  Renderer(Renderer&& o) = default;
  ~Renderer() = default;

  Renderer& operator=(const Renderer&) = delete;
  Renderer& operator=(Renderer&& o) = default;

  void initialize();
  // Call to control when resources are released
  void destroy();

  // Adds the given render stage at the end of the sequence.
  void addRenderStage(std::unique_ptr<RenderStage> stage);
  // Adds the given render stage after the first stage with the given name.
  // Return true on success, false if no such name was found
  bool addRenderStageAfter(const char* name, std::unique_ptr<RenderStage> stage);
  // Retrieves the render stage with the given name
  // Returns the render stage or null if it doesn't exist.
  RenderStage* lookupRenderStage(const char* name);
  // Removes the first render stage matching the name; names should be unique
  // Returns true on success, false if no such name was found
  bool removeRenderStage(const char* name);
  // Removes the first render stage matching the name and replaces it with the given stage
  // Returns true on success, false if no such name was found
  bool replaceRenderStage(const char* name, std::unique_ptr<RenderStage> stage);

  void enablePerfTracking() noexcept { m_perfTrackingEnabled = true; }
  void disablePerfTracking() noexcept { m_perfTrackingEnabled = false; }
  bool perfTrackingEnabled() const noexcept { return m_perfTrackingEnabled; }

  void draw();

  float computeFPS() const;
  gl::uint_t primitivesGenerated() const noexcept { return m_primitivesGenerated; }
  gl::uint64_t samplesPassed() const noexcept { return m_samplesPassed; }
  gl::uint64_t timeElapsed() const noexcept { return m_timeElapsed; }

  // Call to signal the OpenGL context is about to be destroyed and re-created
  // (e.g. because settings changed)
  // This will trigger any cleanup required by render stages
  void notifyContextRebuildImminent();
  // Call to signal the OpenGL context has just been destroyed and re-created
  // (e.g. because settings changed)
  // This will trigger re-initialization of render stages
  void notifyContextRebuildComplete();

  void onResize(const glm::ivec2& newSize);
};

}

#endif // RENDERER_H
