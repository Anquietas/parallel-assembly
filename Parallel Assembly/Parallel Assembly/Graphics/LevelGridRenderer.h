#pragma once
#ifndef GRAPHICS_LEVEL_GRID_RENDERER_H
#define GRAPHICS_LEVEL_GRID_RENDERER_H

#include <vector>
#include <tuple>
#include <type_traits>

#include <boost/container/static_vector.hpp>
#include <boost/graph/adjacency_list.hpp>

#include "../Game/Direction.h"
#include "../Game/Worker.h"

#include "../Flag.h"
#include "../GLMIncludes.h"

#include "OpenGL/VertexArrayObject.h"
#include "OpenGL/VertexBuffer.h"

#include "LevelGridRenderConstants.h"

namespace game {
class SimpleFactory;
}

namespace gui {
struct DirectionArrowSelection;
class MultiSelection;
class LevelGridHoverSelection;
class LevelGridSelection;
}

namespace graphics {

class DirectionArrowShaderProgram;
class LevelGridShaderProgram;
class LevelWorkerPathShaderProgram;
class SelectionBoxShaderProgram;
class ShaderManager;

class LevelGridRenderer final {
  const game::SimpleFactory* m_factory;
  float m_gridCellSize;
  glm::vec2 m_gridRenderOffset;
  glm::vec2 m_viewSize;
  glm::mat4 m_mvpMatrix;
  Flag m_mvpMatrixInvalid;

  std::vector<glm::vec4> m_gridCellColours;
  Flag m_gridCellColoursInvalid;

  gl::VertexArrayObject m_gridVAO;
  gl::VertexBuffer m_gridMeshVBO;
  gl::VertexBuffer m_gridColourVBO;

  // Worker path rendering graph
  struct VertexProperty {
    glm::ivec2 position;
    bool multipleIntersect;
  };
  struct EdgeProperty {
    int length;
    game::Direction direction;
  };
  using PathRenderGraph =
    boost::adjacency_list<
    boost::vecS, boost::vecS, boost::directedS,
    VertexProperty, EdgeProperty, boost::no_property, boost::vecS>;
  using PathRenderGraphVec = boost::container::static_vector<
    PathRenderGraph, game::Worker::maxWorkers>;
  PathRenderGraphVec m_pathRenderGraphs;

  struct PathGLVertex {
    glm::vec2 vertexPos;
    glm::vec2 texCoords;
  };
  static_assert(std::is_standard_layout<PathGLVertex>::value, "PathGLVertex is not standard layout :(");
  using PathMesh = std::vector<PathGLVertex>;
  using PathMeshVec = boost::container::static_vector<PathMesh, game::Worker::maxWorkers>;
  PathMeshVec m_pathMeshes;
  
  using FlagVec = boost::container::static_vector<Flag, game::Worker::maxWorkers>;
  FlagVec m_pathInvalid;

  gl::VertexArrayObject m_pathVAO;
  gl::VertexBuffer m_pathVBO;

  using DirectionArrowData = std::vector<constants::DirectionArrowGLData>;
  using DirectionArrowDataVec = boost::container::static_vector<DirectionArrowData, game::Worker::maxWorkers>;
  DirectionArrowDataVec m_arrowData;
  FlagVec m_directionArrowDataInvalid;

  gl::VertexArrayObject m_arrowVAO;
  gl::VertexBuffer m_arrowMeshVBO;
  gl::VertexBuffer m_arrowDataVBO;

  Flag m_renderSelectionBox;
  AABoundingBox2D m_selectionBox;
  gl::VertexArrayObject m_selectionBoxVAO;
  // Holds mesh for interior AND frame at different offsets
  gl::VertexBuffer m_selectionBoxVBO;

  // Shared by all instances of UISimpleLevelGrid; should only be one at a time and
  // UI code is serial anyway.
  static LevelGridShaderProgram* levelGridShader;
  static LevelWorkerPathShaderProgram* levelWorkerPathShader;
  static DirectionArrowShaderProgram* directionArrowShader;
  static SelectionBoxShaderProgram* selectionBoxShader;

  std::size_t getGridCellColourIndex(const glm::ivec2& gridCell);

  void updateGridCellColours(
    const gui::LevelGridHoverSelection& hoverSelection,
    const gui::LevelGridSelection& selection);
  void renderLevelGrid(
    const gui::LevelGridHoverSelection& hoverSelection,
    const gui::LevelGridSelection& selection);

  void generateWorkerPathGraph(std::size_t worker);
  void generateWorkerPathMesh(std::size_t worker);
  void renderWorkerPaths();

  void generateDirectionArrowRenderData(
    std::size_t worker,
    const gui::LevelGridHoverSelection& hoverSelection,
    const gui::LevelGridSelection& selection,
    const gui::MultiSelection& boxSelection);
  void renderDirectionArrows(
    const gui::LevelGridHoverSelection& hoverSelection,
    const gui::LevelGridSelection& selection,
    const gui::MultiSelection& boxSelection);

  void renderSelectionBox();

public:
  static void setupShaders(ShaderManager& shaderManager);

  LevelGridRenderer(const game::SimpleFactory& factory);
  ~LevelGridRenderer();

  glm::ivec2 gridSize() const;

  const glm::vec2& gridRenderOffset() const noexcept { return m_gridRenderOffset; }
  void setGridRenderOffset(const glm::vec2& offset);

  const glm::vec2& viewSize() const noexcept { return m_viewSize; }
  void setViewSize(const glm::vec2& size);

  float gridCellSize() const noexcept { return m_gridCellSize; }
  void setGridCellSize(float gridCellSize);

  void invalidateGrid();

  void invalidateGridSelectionRenderData();
  void invalidateSelectionRenderData(const gui::DirectionArrowSelection& selection);
  void invalidateSelectionRenderData(const gui::MultiSelection& selection);
  void invalidateSelectionRenderData(const gui::LevelGridHoverSelection& selection);
  void invalidateSelectionRenderData(const gui::LevelGridSelection& selection);

  void beginBoxSelection();
  void updateBoxSelection(const AABoundingBox2D& selectionBox);
  void endBoxSelection();

  void invalidatePath(std::size_t worker);

  // Tests if the given grid position is inside the grid boundaries
  bool isInsideGrid(const glm::ivec2& position) const;
  // Tests if the given grid position is inside the grid boundaries
  bool isInsideGrid(const glm::vec2& position) const;
  
  // Determines what's in the given grid position if it's inside the grid
  // Returns the grid cell at the given position, the worker ID for what is
  // at the position (if applicable; returns -1 if not), and whether the
  // position contains a direction marker
  // Returns {{-1,-1}, _, _} if the position is outside the grid.
  std::tuple<
    glm::ivec2,  // grid cell
    std::size_t, // worker ID
    bool         // direction?
  > lookupGridPosition(const glm::vec2& gridPosition) const;

  void render(
    const gui::LevelGridHoverSelection& hoverSelection,
    const gui::LevelGridSelection& selection,
    const gui::MultiSelection& boxSelection);
};

} // namespace graphics

#endif // GRAPHICS_LEVEL_GRID_RENDERER_H
