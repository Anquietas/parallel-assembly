#include "RenderStage.h"

namespace graphics {

RenderStage::RenderStage(const char* name) : m_name(name) {}

void RenderStage::notifyContextRebuildImminent() {}
void RenderStage::notifyContextRebuildComplete() {}

} // namespace graphics

