#pragma once
#ifndef GRAPHICS_AA_BOUNDING_BOX_H
#define GRAPHICS_AA_BOUNDING_BOX_H

#include "../GLMIncludes.h"

#undef min
#undef max

namespace graphics {

// Axis-aligned bounding box
class AABoundingBox2D final {
  glm::vec2 m_min;
  glm::vec2 m_max;

public:
  // Constructs an AABB between min and max.
  // This will swap min and max's components if needed to ensure min <= max
  AABoundingBox2D(const glm::vec2& min = {}, const glm::vec2& max = {}) noexcept;
  AABoundingBox2D(const AABoundingBox2D& o) noexcept = default;
  AABoundingBox2D(AABoundingBox2D&& o) noexcept = default;
  ~AABoundingBox2D() = default;

  AABoundingBox2D& operator=(const AABoundingBox2D& o) noexcept = default;
  AABoundingBox2D& operator=(AABoundingBox2D&& o) noexcept = default;

  const glm::vec2& min() const noexcept { return m_min; }
  // Updates min; this will also update max if necessary to ensure min <= max
  void setMin(const glm::vec2& min) noexcept;

  const glm::vec2& max() const noexcept { return m_max; }
  // Updates max; this will also update min if necessary to ensure min <= max
  void setMax(const glm::vec2& max) noexcept;

  const glm::vec2& position() const noexcept { return m_min; }
  glm::vec2 size() const noexcept { return m_max - m_min; }

  AABoundingBox2D unionWith(const AABoundingBox2D& box) const noexcept;

  AABoundingBox2D& operator+=(const glm::vec2& offset) noexcept;
  AABoundingBox2D& operator-=(const glm::vec2& offset) noexcept;

  bool contains(const AABoundingBox2D& box) const noexcept;
  bool contains(const glm::vec2& position) const noexcept;

  bool intersects(const AABoundingBox2D& box) const noexcept;
};

AABoundingBox2D operator+(AABoundingBox2D box, const glm::vec2& offset) noexcept;
AABoundingBox2D operator-(AABoundingBox2D box, const glm::vec2& offset) noexcept;

} // namespace graphics

#endif // GRAPHICS_AA_BOUNDING_BOX_H
