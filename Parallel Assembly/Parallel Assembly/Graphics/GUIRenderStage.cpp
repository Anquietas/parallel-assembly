#include "GUIRenderStage.h"

#include "OpenGL/OpenGLIncludes.h"
#include "OpenGL/checkError.h"

#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>

#include "../Assert.h"
#include "../GLMIncludes.h"
#include "../GLMStreamOps.h"

namespace graphics {

GUIRenderStage::GUIRenderStage(const glm::ivec2& screenSize)
  : RenderStage("GUI")
  , m_guiSystem(CEGUI::System::getSingletonPtr())
{
  m_guiSystem->notifyDisplaySizeChanged({float(screenSize.x), float(screenSize.y)});
}

void GUIRenderStage::draw() {
  // Make sure no shaders are bound, active texture is GL_TEXTURE0 and no texture is bound
  glUseProgram(0);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, 0);

  m_guiSystem->renderAllGUIContexts();
}

void GUIRenderStage::notifyContextRebuildImminent() {
  auto renderer = dynamic_cast<CEGUI::OpenGL3Renderer*>(m_guiSystem->getRenderer());
  ASSERT(renderer);
  renderer->grabTextures();
  CHECK_GL_ERROR();
}
// TODO: figure out why this isn't enough to avoid opengl errors in CEGUI
void GUIRenderStage::notifyContextRebuildComplete() {
  auto renderer = dynamic_cast<CEGUI::OpenGL3Renderer*>(m_guiSystem->getRenderer());
  ASSERT(renderer);
  renderer->restoreTextures();
  m_guiSystem->invalidateAllCachedRendering();
  CHECK_GL_ERROR();
}

void GUIRenderStage::onResize(const glm::ivec2& newSize) {
  m_guiSystem->notifyDisplaySizeChanged({float(newSize.x), float(newSize.y)});
}

}