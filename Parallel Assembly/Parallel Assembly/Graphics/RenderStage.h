#pragma once
#ifndef RENDERSTAGE_H
#define RENDERSTAGE_H

#include <string>

#include "../GLMIncludes.h"

namespace graphics {

class RenderStage {
  std::string m_name;
public:
  RenderStage(const char* name);
  RenderStage(const RenderStage&) = delete;
  RenderStage(RenderStage&& o) = default;
  virtual ~RenderStage() = default;

  RenderStage& operator=(const RenderStage&) = delete;
  RenderStage& operator=(RenderStage&&) = default;

  const std::string& name() const { return m_name; }

  virtual void draw() = 0;

  // Called to signal the OpenGL context is about to be destroyed and re-created
  // (e.g. because settings changed)
  // Default implementation does nothing
  virtual void notifyContextRebuildImminent();
  // Called to signal the OpenGL context has just been destroyed and re-created
  // (e.g. because settings changed)
  // Default implementation does nothing
  virtual void notifyContextRebuildComplete();

  virtual void onResize(const glm::ivec2& newSize) = 0;
};

} // namespace graphics

#endif // RENDERSTAGE_H
