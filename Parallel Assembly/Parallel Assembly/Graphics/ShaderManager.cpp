#define SHADER_MANAGER_SINGLETON_INSTANCE
#include "ShaderManager.inl"

#include <sstream>

#include "../Assert.h"
#include "../Exception.h"

namespace graphics {

ShaderManager::ShaderManager()
  : m_logger()
  , m_shaders()
{
  logging::setModuleName(m_logger, "ShaderManager");
}

ShaderManager::~ShaderManager() {
  for (auto& shader : m_shaders)
    if (shader.second->isInitialized())
      shader.second->cleanup();
}

ShaderProgramBase* ShaderManager::addShader(std::unique_ptr<ShaderProgramBase>&& shader) {
  auto result = m_shaders.emplace(shader->name(), std::move(shader));
  if (!result.second) {
    std::ostringstream ss;
    ss << "Failed to create shader " << result.first->second->name() << ": shader already exists";
    throw MAKE_EXCEPTION(ss.str());
  }
  LOG_SEVERITY(m_logger, Debug)
    << "Created shader " << result.first->first;
  ASSERT(result.first->second);
  return result.first->second.get();
}

ShaderProgramBase* ShaderManager::lookupShader(const std::string& name) noexcept {
  auto iter = m_shaders.find(name);
  if (iter == m_shaders.end())
    return nullptr;
  else
    return iter->second.get();
}

void ShaderManager::destroyShader(const std::string& name) {
  auto iter = m_shaders.find(name);
  if (iter == m_shaders.end()) {
    std::ostringstream ss;
    ss << "Tried to destroy shader " << name << " which is not stored in ShaderManager";
    throw MAKE_EXCEPTION(ss.str());
  }
  if (iter->second->isInitialized())
    iter->second->cleanup();
  m_shaders.erase(iter);
}
void ShaderManager::destroyShader(ShaderProgramBase& shader) {
  destroyShader(shader.name());
}

} // namespace graphics
