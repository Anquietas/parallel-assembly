#include "LevelButtonShaderProgram.h"

#include <sstream>

#include <CEGUI/System.h>
#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>
#include <CEGUI/RendererModules/OpenGL/Shader.h>
#include <CEGUI/RendererModules/OpenGL/GlmPimpl.h>

#include "OpenGL/checkError.h"

#include "../Assert.h"
#include "../Exception.h"

namespace graphics {

using namespace std::string_literals;

LevelButtonShaderProgram::LevelButtonShaderProgram()
  : ShaderProgramBase("LevelButtonShader"s)
  , m_shader()
  , m_positionLoc()
  , m_texCoordsLoc()
  , m_colourLoc()
  , m_matrixUniformLoc()
  , m_maskSamplerLoc()
  , m_imageSamplerLoc()
  , m_maskTexUnit(1)
  , m_imageTexUnit(0)
{}

namespace {
const std::string vertexShaderFileName{"Assets/Shaders/LevelButton-vert.glsl"s};
const std::string fragmentShaderFileName{"Assets/Shaders/LevelButton-frag.glsl"s};
}

void LevelButtonShaderProgram::initialize() {
  using graphics::gl::Shader;
  ShaderProgramBase::initialize();

  auto rendererBase = CEGUI::System::getSingleton().getRenderer();
  ASSERT(dynamic_cast<CEGUI::OpenGL3Renderer*>(rendererBase) != nullptr);
  auto renderer = static_cast<CEGUI::OpenGL3Renderer*>(rendererBase);

  m_positionLoc.id = renderer->getShaderStandardPositionLoc();
  m_texCoordsLoc.id = renderer->getShaderStandardTexCoordLoc();
  m_colourLoc.id = renderer->getShaderStandardColourLoc();
  
  m_shader.checkedAddShadersFromFiles(vertexShaderFileName, fragmentShaderFileName);

  m_shader.createProgramObject();

  m_shader.bindAttributeLocation("position", m_positionLoc);
  m_shader.bindAttributeLocation("texCoords", m_texCoordsLoc);
  m_shader.bindAttributeLocation("colour", m_colourLoc);

  m_shader.checkedLink(name());
  m_shader.dumpShaders();

  ASSERT(m_positionLoc == m_shader.getAttributeLocation("position"));
  ASSERT(m_texCoordsLoc == m_shader.getAttributeLocation("texCoords"));
  ASSERT(m_colourLoc == m_shader.getAttributeLocation("colour"));
  m_matrixUniformLoc = m_shader.getUniformLocation("mvpMatrix");
  m_maskSamplerLoc = m_shader.getUniformLocation("maskTexture");
  m_imageSamplerLoc = m_shader.getUniformLocation("imageTexture");

  m_shader.bind();
  m_shader.setUniform(m_maskSamplerLoc, m_maskTexUnit);
  m_shader.setUniform(m_imageSamplerLoc, m_imageTexUnit);
  m_shader.unbind();

  CHECK_GL_ERROR();
}
void LevelButtonShaderProgram::cleanup() {
  ShaderProgramBase::cleanup();
  m_shader.destroy();

  CHECK_GL_ERROR();
}

void LevelButtonShaderProgram::preRender() {
  auto rendererBase = CEGUI::System::getSingleton().getRenderer();
  ASSERT(dynamic_cast<CEGUI::OpenGL3Renderer*>(rendererBase) != nullptr);
  auto renderer = static_cast<CEGUI::OpenGL3Renderer*>(rendererBase);

  // Bind custom shader
  //renderer->getShaderStandard()->unbind();
  m_shader.bind();

  auto mvpMatrix = renderer->getViewProjectionMatrix();
  m_shader.setUniform(m_matrixUniformLoc, mvpMatrix->d_matrix);

  CHECK_GL_ERROR_PEDANTIC();
}
void LevelButtonShaderProgram::postRender() {
  // Restore CEGUI standard shader
  //m_shader.unbind();
  auto renderer = static_cast<CEGUI::OpenGL3Renderer*>(CEGUI::System::getSingleton().getRenderer());
  renderer->getShaderStandard()->bind();

  CHECK_GL_ERROR_PEDANTIC();
}

} // namespace graphics
