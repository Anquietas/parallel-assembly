#include "Renderer.h"

#include <algorithm>
#include <sstream>

#include "OpenGL/checkError.h"
#include "OpenGL/OpenGLIncludes.h"

#include "../Assert.h"

namespace graphics {

Renderer::Renderer(const glm::ivec2& screenSize)
  : m_screenSize(screenSize)
  , m_stages()
  , m_fpsLimiter(480.f)
  , m_queries()
  , m_perfTrackingEnabled(false)
  , m_primitivesGenerated(0u)
  , m_samplesPassed(0u)
  , m_timeElapsed(0u)
  , m_logger()
{
  logging::setModuleName(m_logger, "Renderer");
}

void Renderer::initialize() {
  m_queries.primitivesGenerated.init();
  m_queries.samplesPassed.init();
  m_queries.timeElapsed.init();
}

void Renderer::destroy() {
  m_stages.clear();
  LOG_SEVERITY(m_logger, Debug) << "Erased all render stages";
  m_queries.primitivesGenerated.destroy();
  m_queries.samplesPassed.destroy();
  m_queries.timeElapsed.destroy();
}

void Renderer::addRenderStage(std::unique_ptr<RenderStage> stage) {
  LOG_SEVERITY(m_logger, Debug) << "Added render stage " << stage->name();
  m_stages.emplace_back(std::move(stage));
}
bool Renderer::addRenderStageAfter(const char* name, std::unique_ptr<RenderStage> stage) {
  auto iter = std::find_if(m_stages.begin(), m_stages.end(),
    [name](const auto& stage) -> bool {
      return name == stage->name();
    });
  if (iter == m_stages.end()) {
    LOG_SEVERITY(m_logger, Warning) << "Failed to find render stage " << name;
    return false;
  }
  ++iter;
  iter = m_stages.emplace(iter, std::move(stage));
  LOG_SEVERITY(m_logger, Debug)
    << "Added render stage " << (*iter)->name() << " after " << name;
  return true;
}
RenderStage* Renderer::lookupRenderStage(const char* name) {
  auto iter = std::find_if(m_stages.begin(), m_stages.end(),
    [name](const auto& stage) -> bool {
      return name == stage->name();
    });
  if (iter == m_stages.end()) return nullptr;
  return iter->get();
}
bool Renderer::removeRenderStage(const char* name) {
  auto iter = std::find_if(m_stages.begin(), m_stages.end(),
    [name](const auto& stage) -> bool {
      return name == stage->name();
    });
  if (iter == m_stages.end()) {
    LOG_SEVERITY(m_logger, Warning) << "Failed to remove render stage " << name;
    return false;
  }
  m_stages.erase(iter);
  LOG_SEVERITY(m_logger, Debug) << "Removed render stage " << name;
  return true;
}
bool Renderer::replaceRenderStage(const char* name, std::unique_ptr<RenderStage> stage) {
  auto iter = std::find_if(m_stages.begin(), m_stages.end(),
    [name](const auto& stage) -> bool {
      return name == stage->name();
    });
  if (iter == m_stages.end()) {
    LOG_SEVERITY(m_logger, Warning) << "Failed to find render stage " << name << " for replacement";
    return false;
  }
  *iter = std::move(stage);
  LOG_SEVERITY(m_logger, Debug) << "Replaced render stage " << name << " with " << (*iter)->name();
  return true;
}

void Renderer::draw() {
  if (perfTrackingEnabled()) {
    m_queries.primitivesGenerated.beginQuery();
    m_queries.samplesPassed.beginQuery();
    m_queries.timeElapsed.beginQuery();
  }

  glClearColor(0.25f, 0.25f, 0.25f, 0.0f);
  glClear(GL_COLOR_BUFFER_BIT |
          GL_DEPTH_BUFFER_BIT);
  
  for (auto& stage : m_stages) {
    LOG_SEVERITY(m_logger, Pedantic) << "Rendering stage " << stage->name();
    stage->draw();
    CHECK_GL_ERROR_PEDANTIC();
  }
  // Only log if we rendered something
  if (!m_stages.empty())
    LOG_SEVERITY(m_logger, Pedantic) << "Rendering complete";

  if (perfTrackingEnabled()) {
    m_queries.timeElapsed.endQuery();
    m_queries.samplesPassed.endQuery();
    m_queries.primitivesGenerated.endQuery();
  }

  glFinish();

  m_fpsLimiter.onFrame();

  if (perfTrackingEnabled()) {
    m_primitivesGenerated = m_queries.primitivesGenerated.result();
    m_samplesPassed = m_queries.samplesPassed.result();
    m_timeElapsed = m_queries.timeElapsed.result();
  }
}

float Renderer::computeFPS() const {
  return m_fpsLimiter.computeFPS();
}

void Renderer::notifyContextRebuildImminent() {
  for (auto& stage : m_stages)
    stage->notifyContextRebuildImminent();
}
void Renderer::notifyContextRebuildComplete() {
  for (auto& stage : m_stages)
    stage->notifyContextRebuildComplete();
}

void Renderer::onResize(const glm::ivec2& newSize) {
  if (m_screenSize != newSize) {
    for (auto& stage : m_stages)
      stage->onResize(newSize);
    m_screenSize = newSize;
  }
}

}
