#include "SelectionBoxShaderProgram.h"

#include <sstream>
#include <string>

#include "OpenGL/OpenGLIncludes.h"

#include "../Exception.h"

#undef min
#undef max

namespace graphics {

using namespace std::string_literals;

SelectionBoxShaderProgram::SelectionBoxShaderProgram()
  : ShaderProgramBase("SelectionBoxShaderProgram"s)
  , m_shader()
  , m_vertexPosLoc()
  , m_mvpMatrixUniformLoc()
  , m_boxSizeUniformLoc()
  , m_boxPositionUniformLoc()
  , m_gridCellSizeUniformLoc()
  , m_colourUniformLoc()
{}

namespace {
const std::string vertexShaderFileName{"Assets/Shaders/SelectionBox-vert.glsl"s};
const std::string fragmentShaderFileName{"Assets/Shaders/SelectionBox-frag.glsl"s};
}

void SelectionBoxShaderProgram::initialize() {
  ShaderProgramBase::initialize();

  m_shader.checkedAddShadersFromFiles(vertexShaderFileName, fragmentShaderFileName);
  m_shader.checkedLink(name());
  m_shader.dumpShaders();

  m_vertexPosLoc = m_shader.getAttributeLocation("vertexPos");
  m_mvpMatrixUniformLoc = m_shader.getUniformLocation("mvpMatrix");
  m_boxSizeUniformLoc = m_shader.getUniformLocation("boxSize");
  m_boxPositionUniformLoc = m_shader.getUniformLocation("boxPosition");
  m_gridCellSizeUniformLoc = m_shader.getUniformLocation("gridCellSize");
  m_colourUniformLoc = m_shader.getUniformLocation("colour");
}
void SelectionBoxShaderProgram::cleanup() {
  ShaderProgramBase::cleanup();
  m_shader.destroy();
}

void SelectionBoxShaderProgram::preRender() {
  m_shader.bind();
}
void SelectionBoxShaderProgram::postRender() {
  m_shader.unbind();
}

void SelectionBoxShaderProgram::setMVPMatrix(const glm::mat4& mvpMatrix) {
  m_shader.setUniform(m_mvpMatrixUniformLoc, mvpMatrix);
}
void SelectionBoxShaderProgram::setBoxSize(const glm::vec2& boxSize) {
  m_shader.setUniform(m_boxSizeUniformLoc, boxSize);
}
void SelectionBoxShaderProgram::setBoxPosition(const glm::vec2& boxPosition) {
  m_shader.setUniform(m_boxPositionUniformLoc, boxPosition);
}
void SelectionBoxShaderProgram::setGridCellSize(gl::float_t gridCellSize) {
  m_shader.setUniform(m_gridCellSizeUniformLoc, gridCellSize);
}
void SelectionBoxShaderProgram::setColour(const glm::vec4& colour) {
  m_shader.setUniform(m_colourUniformLoc, colour);
}

} // namespace graphics
