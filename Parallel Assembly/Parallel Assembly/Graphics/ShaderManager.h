#pragma once
#ifndef GRAPHICS_SHADER_MANAGER_H
#define GRAPHICS_SHADER_MANAGER_H

#include <memory>
#include <string>
#include <map>
#include <type_traits>

#include "../Logger.h"

#include "ShaderProgramBase.h"

namespace graphics {

class ShaderManager final {
  logging::Logger m_logger;

  using ShaderMap = std::map<std::string, std::unique_ptr<ShaderProgramBase>>;
  ShaderMap m_shaders;

  // Helper function for createShader
  ShaderProgramBase* addShader(std::unique_ptr<ShaderProgramBase>&& shader);

public:
  ShaderManager();
  ShaderManager(const ShaderManager&) = delete;
  ShaderManager(ShaderManager&&) = delete;
  ~ShaderManager();

  ShaderManager& operator=(const ShaderManager&) = delete;
  ShaderManager& operator=(ShaderManager&&) = delete;

  // Creates a shader of the requested type using the given arguments
  // and registers it in m_shaders using the result of name() as the key.
  // If a shader with that name already exists, throws an exception.
  template <typename ShaderType, typename... Args>
  std::enable_if_t<
    std::is_base_of<ShaderProgramBase, std::decay_t<ShaderType>>::value,
    std::decay_t<ShaderType>*
  > createShader(Args&&... args);

  // Looks up the given shader name and returns the shader if it exists,
  // or nullptr if it doesn't.
  ShaderProgramBase* lookupShader(const std::string& name) noexcept;

  // Destroys the shader with the given name, if it exists.
  // If it does not exist, throws an exception
  void destroyShader(const std::string& name);
  // Destroys the given shader if it's stored in m_shaders.
  // If it isn't, throws an exception.
  void destroyShader(ShaderProgramBase& shader);
};

} // namespace graphics

#endif // GRAPHICS_SHADER_MANAGER_H
