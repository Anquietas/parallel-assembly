#include "LevelGridRenderConstants.h"

#include <initializer_list>

#include <boost/container/flat_map.hpp>

#include "../Assert.h"

namespace graphics {
namespace constants {

// Grid cell mesh
const std::array<glm::vec2, 8> gridCellMesh{
  glm::vec2{0.125f, 0.025f},
  {0.875f, 0.025f},
  {0.025f, 0.125f},
  {0.975f, 0.125f},
  {0.025f, 0.875f},
  {0.975f, 0.875f},
  {0.125f, 0.975f},
  {0.875f, 0.975f},
};

using DirectionToVec2Map = boost::container::flat_map<game::Direction, glm::vec2>;

// Offsets specific to the mesh for correcting transforms
const boost::container::flat_map<game::Direction, glm::vec2> baseDirectionArrowMeshOffsetMap{
  {game::Direction::Left,   {arrowHeight, arrowWidth}},
  {game::Direction::Right,  {0.f, 0.f}},
  {game::Direction::Up,     {0.f, arrowHeight}},
  {game::Direction::Down,   {arrowWidth, 0.f}},
};

const DirectionToVec2Map baseDirectionArrowOffsetMap {
  {game::Direction::Left,   {0.f, baseOffset}},
  {game::Direction::Right,  {1.f - arrowHeight, baseOffset}},
  {game::Direction::Up,     {baseOffset, 0.f}},
  {game::Direction::Down,   {baseOffset, 1.f - arrowHeight}},
};
const DirectionToVec2Map perWorkerDirectionArrowOffsetMap {
  {game::Direction::Left,   {0.f, arrowWidth}},
  {game::Direction::Right,  {0.f, arrowWidth}},
  {game::Direction::Up,     {arrowWidth, 0.f}},
  {game::Direction::Down,   {arrowWidth, 0.f}},
};

glm::vec2 computeDirectionArrowOffset(
  std::size_t worker, game::Direction direction) noexcept
{
  ASSERT(worker < game::Worker::maxWorkers);
  ASSERT(direction != game::Direction::None);
  ASSERT_MSG(baseDirectionArrowOffsetMap.count(direction) == 1u &&
    perWorkerDirectionArrowOffsetMap.count(direction) == 1u,
    "Unhandled direction in computeDirectionArrowOffset()");

  return baseDirectionArrowOffsetMap.find(direction)->second +
    perWorkerDirectionArrowOffsetMap.find(direction)->second * float(worker);
}
const std::array<DirectionToVec2Map, game::Worker::maxWorkers> directionArrowOffsetMap {
  std::initializer_list<DirectionToVec2Map::value_type>{
    // Worker 0
    {game::Direction::Left,   computeDirectionArrowOffset(0u, game::Direction::Left)},
    {game::Direction::Right,  computeDirectionArrowOffset(0u, game::Direction::Right)},
    {game::Direction::Up,     computeDirectionArrowOffset(0u, game::Direction::Up)},
    {game::Direction::Down,   computeDirectionArrowOffset(0u, game::Direction::Down)},
  }, {
    // Worker 1
    {game::Direction::Left,   computeDirectionArrowOffset(1u, game::Direction::Left)},
    {game::Direction::Right,  computeDirectionArrowOffset(1u, game::Direction::Right)},
    {game::Direction::Up,     computeDirectionArrowOffset(1u, game::Direction::Up)},
    {game::Direction::Down,   computeDirectionArrowOffset(1u, game::Direction::Down)},
  }, {
    // Worker 2
    {game::Direction::Left,   computeDirectionArrowOffset(2u, game::Direction::Left)},
    {game::Direction::Right,  computeDirectionArrowOffset(2u, game::Direction::Right)},
    {game::Direction::Up,     computeDirectionArrowOffset(2u, game::Direction::Up)},
    {game::Direction::Down,   computeDirectionArrowOffset(2u, game::Direction::Down)},
  }, {
    // Worker 3
    {game::Direction::Left,   computeDirectionArrowOffset(3u, game::Direction::Left)},
    {game::Direction::Right,  computeDirectionArrowOffset(3u, game::Direction::Right)},
    {game::Direction::Up,     computeDirectionArrowOffset(3u, game::Direction::Up)},
    {game::Direction::Down,   computeDirectionArrowOffset(3u, game::Direction::Down)},
  },
};
const glm::vec2& lookupDirectionArrowOffset(
  std::size_t worker, game::Direction direction) noexcept
{
  ASSERT(worker < game::Worker::maxWorkers);
  ASSERT(direction != game::Direction::None);
  ASSERT_MSG(directionArrowOffsetMap[worker].count(direction) == 1u,
    "Unhandled direction in directionArrowOffsetMap");

  return directionArrowOffsetMap[worker].find(direction)->second;
}

glm::vec2 computeDirectionArrowMeshOffset(
  std::size_t worker, game::Direction direction) noexcept
{
  ASSERT(direction != game::Direction::None);
  ASSERT_MSG(baseDirectionArrowMeshOffsetMap.count(direction) == 1u,
    "Unhandled direction in baseDirectionArrowMeshOffsetMap");

  return lookupDirectionArrowOffset(worker, direction) +
    baseDirectionArrowMeshOffsetMap.find(direction)->second;
}
const std::array<DirectionToVec2Map, game::Worker::maxWorkers> directionArrowMeshOffsetMap {
  std::initializer_list<DirectionToVec2Map::value_type>{
    // Worker 0
    {game::Direction::Left,   computeDirectionArrowMeshOffset(0u, game::Direction::Left)},
    {game::Direction::Right,  computeDirectionArrowMeshOffset(0u, game::Direction::Right)},
    {game::Direction::Up,     computeDirectionArrowMeshOffset(0u, game::Direction::Up)},
    {game::Direction::Down,   computeDirectionArrowMeshOffset(0u, game::Direction::Down)},
  }, {
    // Worker 1
    {game::Direction::Left,   computeDirectionArrowMeshOffset(1u, game::Direction::Left)},
    {game::Direction::Right,  computeDirectionArrowMeshOffset(1u, game::Direction::Right)},
    {game::Direction::Up,     computeDirectionArrowMeshOffset(1u, game::Direction::Up)},
    {game::Direction::Down,   computeDirectionArrowMeshOffset(1u, game::Direction::Down)},
  }, {
    // Worker 2
    {game::Direction::Left,   computeDirectionArrowMeshOffset(2u, game::Direction::Left)},
    {game::Direction::Right,  computeDirectionArrowMeshOffset(2u, game::Direction::Right)},
    {game::Direction::Up,     computeDirectionArrowMeshOffset(2u, game::Direction::Up)},
    {game::Direction::Down,   computeDirectionArrowMeshOffset(2u, game::Direction::Down)},
  }, {
    // Worker 3
    {game::Direction::Left,   computeDirectionArrowMeshOffset(3u, game::Direction::Left)},
    {game::Direction::Right,  computeDirectionArrowMeshOffset(3u, game::Direction::Right)},
    {game::Direction::Up,     computeDirectionArrowMeshOffset(3u, game::Direction::Up)},
    {game::Direction::Down,   computeDirectionArrowMeshOffset(3u, game::Direction::Down)},
  },
};
const glm::vec2& lookupDirectionArrowMeshOffset(
  std::size_t worker, game::Direction direction) noexcept
{
  ASSERT(worker < game::Worker::maxWorkers);
  ASSERT(direction != game::Direction::None);
  ASSERT_MSG(directionArrowMeshOffsetMap[worker].count(direction) == 1u,
    "Unhandled direction in directionArrowMeshOffsetMap");

  return directionArrowMeshOffsetMap[worker].find(direction)->second;
}

const glm::vec2 zero{0.f, 0.f};
const glm::vec2 leftRightArrowSize{arrowHeight, arrowWidth};
const glm::vec2 upDownArrowSize{arrowWidth, arrowHeight};

using DirectionToAABBMap = boost::container::flat_map<game::Direction, AABoundingBox2D>;

const DirectionToAABBMap baseDirectionBoundMap{
  {game::Direction::Left,   {zero, leftRightArrowSize}},
  {game::Direction::Right,  {zero, leftRightArrowSize}},
  {game::Direction::Up,     {zero, upDownArrowSize}},
  {game::Direction::Down,   {zero, upDownArrowSize}},
};

AABoundingBox2D computeDirectionArrowBounds(
  std::size_t worker, game::Direction direction) noexcept
{
  ASSERT(worker < game::Worker::maxWorkers);
  ASSERT(direction != game::Direction::None);
  ASSERT_MSG(baseDirectionBoundMap.count(direction) == 1u,
    "Unhandled direction in baseDirectionBoundMap");

  return baseDirectionBoundMap.find(direction)->second +
    lookupDirectionArrowOffset(worker, direction);
}
// Use worker ID for array index
extern const std::array<DirectionToAABBMap, game::Worker::maxWorkers> directionArrowBoundMap{
  std::initializer_list<DirectionToAABBMap::value_type>{
    // Worker 0
    {game::Direction::Left,   computeDirectionArrowBounds(0u, game::Direction::Left)},
    {game::Direction::Right,  computeDirectionArrowBounds(0u, game::Direction::Right)},
    {game::Direction::Up,     computeDirectionArrowBounds(0u, game::Direction::Up)},
    {game::Direction::Down,   computeDirectionArrowBounds(0u, game::Direction::Down)},
  }, {
    // Worker 1
    {game::Direction::Left,   computeDirectionArrowBounds(1u, game::Direction::Left)},
    {game::Direction::Right,  computeDirectionArrowBounds(1u, game::Direction::Right)},
    {game::Direction::Up,     computeDirectionArrowBounds(1u, game::Direction::Up)},
    {game::Direction::Down,   computeDirectionArrowBounds(1u, game::Direction::Down)},
  }, {
    // Worker 2
    {game::Direction::Left,   computeDirectionArrowBounds(2u, game::Direction::Left)},
    {game::Direction::Right,  computeDirectionArrowBounds(2u, game::Direction::Right)},
    {game::Direction::Up,     computeDirectionArrowBounds(2u, game::Direction::Up)},
    {game::Direction::Down,   computeDirectionArrowBounds(2u, game::Direction::Down)},
  }, {
    // Worker 3
    {game::Direction::Left,   computeDirectionArrowBounds(3u, game::Direction::Left)},
    {game::Direction::Right,  computeDirectionArrowBounds(3u, game::Direction::Right)},
    {game::Direction::Up,     computeDirectionArrowBounds(3u, game::Direction::Up)},
    {game::Direction::Down,   computeDirectionArrowBounds(3u, game::Direction::Down)},
  },
};
const AABoundingBox2D& lookupDirectionArrowBounds(
  std::size_t worker, game::Direction direction) noexcept
{
  ASSERT(worker < game::Worker::maxWorkers);
  ASSERT(direction != game::Direction::None);
  ASSERT_MSG(directionArrowBoundMap[worker].count(direction) == 1u,
    "Unhandled direction in directionArrowBoundMap");

  return directionArrowBoundMap[worker].find(direction)->second;
}

// Direction arrow mesh
const std::array<DirectionArrowGLVertex, 3> directionArrowMesh{
  DirectionArrowGLVertex
  {{arrowWidth, 0.f},{1.f, 0.f}},
  {{0.f, 0.f}, {0.f, 0.f}},
  {{arrowWidth / 2.f, -arrowHeight}, {0.5f, 1.f}},
};

// Selection box meshes

const std::array<glm::vec2, 8> selectionBoxMesh{
  glm::vec2
  // Interior
  {0.f, 0.f},
  {0.f, 1.f},
  {1.f, 0.f},
  {1.f, 1.f},
  // Border
  {0.f, 0.f},
  {0.f, 1.f},
  {1.f, 1.f},
  {1.f, 0.f},
};

// Grid colours

const glm::vec4 gridBackColour{0.25, 0.25f, 0.25f, 1.f};
const glm::vec4 gridCellColour{0.1f, 0.1f, 0.1f, 1.f};
const glm::vec4 gridCellHoverColour{0.15f, 0.15f, 0.15f, 1.f};
const glm::vec4 gridCellSelectedColour{0.2f, 0.2f, 0.2f, 1.f};
const glm::vec4 gridCellSelectedHoverColour{0.225f, 0.225f, 0.225f, 1.f};

// Path colours

const std::array<glm::vec3, game::Worker::maxWorkers> pathBackColours{
  glm::vec3{1.f, 0.1f, 0.1f},
  {0.2f, 0.2f, 0.8f},
  {0.f, 0.553f, 0.169f},
  {0.65f, 0.7f, 0.f},
};
const std::array<glm::vec3, game::Worker::maxWorkers> pathArrowColours{
  pathBackColours[0] * 0.5f,
  pathBackColours[1] * 0.5f,
  pathBackColours[2] * 0.5f,
  pathBackColours[3] * 0.5f,
};

// Arrow colours

const std::array<glm::vec3, game::Worker::maxWorkers> directionArrowColours{
  glm::clamp(pathBackColours[0] * 4.f,{},{1.f}),
  glm::clamp(pathBackColours[1] * 2.5f,{},{1.f}),
  glm::clamp(pathBackColours[2] * 1.5f,{},{1.f}),
  glm::clamp(pathBackColours[3] * 2.f,{},{1.f}),
};
const std::array<glm::vec3, game::Worker::maxWorkers> directionArrowBorderColours{
  directionArrowColours[0] * 0.5f,
  directionArrowColours[1] * 0.5f,
  directionArrowColours[2] * 0.5f,
  directionArrowColours[3] * 0.5f,
};

// Selection box colours
const glm::vec4 selectionBoxColour{0.75f, 0.75f, 0.75f, 0.1f};
const glm::vec4 selectionBoxBorderColour{0.75f, 0.75f, 0.75f, 0.75f};

} // namespace constants
} // namespace graphics
