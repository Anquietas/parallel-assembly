#pragma once
#ifndef GRAPHICS_LEVEL_GRID_RENDER_CONSTANTS_H
#define GRAPHICS_LEVEL_GRID_RENDER_CONSTANTS_H

#include <array>
#include <type_traits>

#include "../Game/Worker.h"

#include "../GLMIncludes.h"

#include "OpenGL/Types.h"

#include "AABoundingBox.h"

namespace graphics {
namespace constants {

// Grid cell mesh
extern const std::array<glm::vec2, 8> gridCellMesh;

// Constants for path/arrow rendering

static_assert(game::Worker::maxWorkers == 4u,
              "Need to update render constants; maxWorkers is not 4");

constexpr float baseOffset = 1.f / 8.f;
constexpr float workingSpace = 3.f / 4.f;
constexpr float pathRatio = 1.f / 2.f; // 2 / 4
constexpr float arrowWidth = workingSpace / float(game::Worker::maxWorkers);
constexpr float arrowHeight = arrowWidth / 2.f;
constexpr float fullPathWidth = pathRatio / float(game::Worker::maxWorkers);
constexpr float halfPathWidth = fullPathWidth / 2.f;
constexpr float basePathOffset = 1.f / float(8 * game::Worker::maxWorkers);
constexpr float workerPathOffset(std::size_t worker) {
  return baseOffset + worker * arrowWidth + basePathOffset;
}
constexpr float pathTextureMult = 1.f / fullPathWidth;

// Used to look up the offset vector for direction arrows
const glm::vec2& lookupDirectionArrowOffset(
  std::size_t worker, game::Direction direction) noexcept;
// Used to look up the offset for direction arrows with mesh offsets
const glm::vec2& lookupDirectionArrowMeshOffset(
  std::size_t worker, game::Direction direction) noexcept;
// Used to look up the bounding box for direction arrows
const AABoundingBox2D& lookupDirectionArrowBounds(
  std::size_t worker, game::Direction direction) noexcept;

// Direction arrow mesh

struct DirectionArrowGLVertex {
  glm::vec2 vertexPos;
  glm::vec2 texCoords;
};
static_assert(std::is_standard_layout<DirectionArrowGLVertex>::value, "DirectionArrowGLVertex is not standard layout :(");
extern const std::array<DirectionArrowGLVertex, 3> directionArrowMesh;

// Direction arrow shader inputs
struct DirectionArrowGLData {
  glm::vec2 offset;
  gl::int_t direction;  // 0: left, 1: right, 2: up, 3: down
  gl::int_t state;      // 0: normal , 1: hover, 2: selected, 3: selected and hovered
};
static_assert(std::is_standard_layout<DirectionArrowGLData>::value, "DirectionArrowGLData is not standard layout :(");

// Selection box meshes

// First 4: interior mesh: tri strip
// Second 4: border mesh: line loop
extern const std::array<glm::vec2, 8> selectionBoxMesh;
constexpr graphics::gl::int_t selectionBoxInteriorOffset = 0;
constexpr graphics::gl::sizei_t selectionBoxInteriorSize = 4;
constexpr graphics::gl::int_t selectionBoxBorderOffset = 4;
constexpr graphics::gl::sizei_t selectionBoxBorderSize = 4;

// Grid colours

extern const glm::vec4 gridBackColour;
extern const glm::vec4 gridCellColour;
extern const glm::vec4 gridCellHoverColour;
extern const glm::vec4 gridCellSelectedColour;
extern const glm::vec4 gridCellSelectedHoverColour;

// Path colours

extern const std::array<glm::vec3, game::Worker::maxWorkers> pathBackColours;
extern const std::array<glm::vec3, game::Worker::maxWorkers> pathArrowColours;

// Arrow colours

extern const std::array<glm::vec3, game::Worker::maxWorkers> directionArrowColours;
extern const std::array<glm::vec3, game::Worker::maxWorkers> directionArrowBorderColours;

// Selection box colours

extern const glm::vec4 selectionBoxColour;
extern const glm::vec4 selectionBoxBorderColour;

} // namespace constants
} // namespace graphics

#endif // GRAPHICS_LEVEL_GRID_RENDER_CONSTANTS_H
