#pragma once
#ifndef GRAPHICS_SELECTION_BOX_SHADER_PROGRAM_H
#define GRAPHICS_SELECTION_BXO_SHADER_PROGRAM_H

#include "OpenGL\ShaderProgram.h"

#include "ShaderProgramBase.h"

namespace graphics {

class SelectionBoxShaderProgram : public ShaderProgramBase {
  gl::ShaderProgram m_shader;
  gl::AttributeID m_vertexPosLoc;
  gl::UniformID m_mvpMatrixUniformLoc;
  gl::UniformID m_boxSizeUniformLoc;
  gl::UniformID m_boxPositionUniformLoc;
  gl::UniformID m_gridCellSizeUniformLoc;
  gl::UniformID m_colourUniformLoc;
public:
  SelectionBoxShaderProgram();
  SelectionBoxShaderProgram(SelectionBoxShaderProgram&& o) = default;
  virtual ~SelectionBoxShaderProgram() override = default;

  SelectionBoxShaderProgram& operator=(SelectionBoxShaderProgram&& o) = default;

  virtual void initialize() override;
  virtual void cleanup() override;

  virtual void preRender() override;
  virtual void postRender() override;

  // Only valid after initialization

  const gl::AttributeID& vertexPosAttriBID() const noexcept { return m_vertexPosLoc; }

  // The following functions should only be called between preRender() and postRender()

  // Sets the shader's mvpMatrix uniform to the given value
  void setMVPMatrix(const glm::mat4& mvpMatrix);
  // Sets the shader's boxSize uniform to the given value
  void setBoxSize(const glm::vec2& boxSize);
  // Sets the shader's boxPosition uniform to the given value
  void setBoxPosition(const glm::vec2& boxPosition);
  // Sets the shader's gridCellSize uniform to the given value
  void setGridCellSize(gl::float_t gridCellSize);
  // Sets the shader's colour uniform to the given value
  void setColour(const glm::vec4& colour);
};

} // namespace graphics

#endif // GRAPHICS_SELECTION_BOX_SHADER_PROGRAM_H
