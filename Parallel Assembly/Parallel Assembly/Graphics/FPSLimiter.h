#pragma once
#ifndef GRAPHICS_FPS_LIMITER_H
#define GRAPHICS_FPS_LIMITER_H

#include <chrono>

#include <boost/container/static_vector.hpp>

namespace graphics {

class FPSLimiter final {
  using Clock = std::chrono::steady_clock;

  template <typename T, std::size_t N>
  using static_vector = boost::container::static_vector<T, N>;

  static constexpr std::size_t windowSize = 5;

  float m_targetFPS;
  Clock::duration m_expectedGap;
  Clock::time_point m_lastFrame;
  static_vector<Clock::duration, windowSize> m_frameGaps;
  std::size_t m_gapIndex;
public:
  FPSLimiter(float targetFPS_);
  FPSLimiter(const FPSLimiter& o) = default;
  FPSLimiter(FPSLimiter&& o) = default;
  ~FPSLimiter() = default;

  void setTargetFPS(float targetFPS_);
  float targetFPS() const noexcept { return m_targetFPS; }

  float computeFPS() const noexcept;
  void onFrame();
};

} // namespace graphics

#endif // GRAPHICS_FPS_LIMITER_H
