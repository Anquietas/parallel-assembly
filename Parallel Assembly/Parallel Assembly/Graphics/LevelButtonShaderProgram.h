#pragma once
#ifndef GUI_LEVEL_BUTTON_SHADER_PROGRAM
#define GUI_LEVEL_BUTTON_SHADER_PROGRAM

#include "ShaderProgramBase.h"
#include "OpenGL/ShaderProgram.h"

namespace graphics {

class LevelButtonShaderProgram : public ShaderProgramBase {
  gl::ShaderProgram m_shader;
  gl::AttributeID m_positionLoc;
  gl::AttributeID m_texCoordsLoc;
  gl::AttributeID m_colourLoc;
  gl::UniformID m_matrixUniformLoc;
  gl::UniformID m_maskSamplerLoc;
  gl::UniformID m_imageSamplerLoc;
  gl::TextureUnit m_maskTexUnit;
  gl::TextureUnit m_imageTexUnit;

public:
  LevelButtonShaderProgram();
  LevelButtonShaderProgram(LevelButtonShaderProgram&& o) = default;
  virtual ~LevelButtonShaderProgram() override = default;

  LevelButtonShaderProgram& operator=(LevelButtonShaderProgram&& o) = default;

  virtual void initialize() override;
  virtual void cleanup() override;

  virtual void preRender() override;
  virtual void postRender() override;

  gl::TextureUnit& getMaskTexUnit() noexcept { return m_maskTexUnit; }
};

} // namespace graphics

#endif // GUI_LEVEL_BUTTON_SHADER_PROGRAM
