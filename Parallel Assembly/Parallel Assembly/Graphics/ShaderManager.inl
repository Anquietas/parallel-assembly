#pragma once
#ifndef GRAPHICS_SHADER_MANAGER_INL
#define GRAPHICS_SHADER_MANAGER_INL

#include "ShaderManager.h"

namespace graphics {

template <typename ShaderType, typename... Args>
std::enable_if_t<
  std::is_base_of<ShaderProgramBase, std::decay_t<ShaderType>>::value,
  std::decay_t<ShaderType>*
> ShaderManager::createShader(Args&&... args) {
  using DecayedType = std::decay_t<ShaderType>;
  auto shaderPtr = std::make_unique<DecayedType>(std::forward<Args>(args)...);
  auto shader = addShader(std::move(shaderPtr));
  return static_cast<DecayedType*>(shader);
}

} // namespace graphics

#endif // GRAPHICS_SHADER_MANAGER_INL
