#pragma once
#ifndef GRAPHICS_DIRECTION_ARROW_SHADER_PROGRAM_H
#define GRAPHICS_DIRECTION_ARROW_SHADER_PROGRAM_H

#include "ShaderProgramBase.h"

#include "OpenGL/ShaderProgram.h"
#include "OpenGL/Texture2D.h"
#include "OpenGL/Sampler.h"

namespace graphics {

class DirectionArrowShaderProgram : public ShaderProgramBase {
  gl::ShaderProgram m_shader;
  gl::AttributeID m_vertexPosLoc;
  gl::AttributeID m_texCoordsLoc;
  gl::AttributeID m_offsetLoc;
  gl::AttributeID m_directionLoc;
  gl::AttributeID m_stateLoc;
  gl::UniformID m_mvpMatrixUniformLoc;
  gl::UniformID m_gridCellSizeUniformLoc;
  gl::UniformID m_arrowSamplerUniformLoc;
  gl::UniformID m_arrowColourUniformLoc;
  gl::UniformID m_borderColourUniformLoc;

  gl::TextureUnit m_arrowTexUnit;
  gl::ColourTexture2D m_arrowTexture;
  gl::Sampler m_arrowSampler;
public:
  DirectionArrowShaderProgram();
  DirectionArrowShaderProgram(DirectionArrowShaderProgram&& o) = default;
  virtual ~DirectionArrowShaderProgram() override = default;

  DirectionArrowShaderProgram& operator=(DirectionArrowShaderProgram&& o) = default;

  virtual void initialize() override;
  virtual void cleanup() override;
  
  virtual void preRender() override;
  virtual void postRender() override;

  // Only valid after initialization

  const gl::AttributeID& vertexPosAttribID() const noexcept { return m_vertexPosLoc; }
  const gl::AttributeID& texCoordsAttribID() const noexcept { return m_texCoordsLoc; }
  const gl::AttributeID& offsetAttribID() const noexcept { return m_offsetLoc; }
  const gl::AttributeID& directionAttribID() const noexcept { return m_directionLoc; }
  const gl::AttributeID& stateAttribID() const noexcept { return m_stateLoc; }

  // The following functions should only be called between preRender() and postRender()

  // Sets the shader's mvpMatrix uniform to the given value
  void setMVPMatrix(const glm::mat4& mvpMatrix);
  // Sets the shader's gridCellSize uniform to the given value
  void setGridCellSize(gl::float_t gridCellSize);
  // Sets the shader's arrowColour uniform to the given value
  void setArrowColour(const glm::vec3& colour);
  // Setts the shader's borderColour uniform to the given value
  void setBorderColour(const glm::vec3& colour);
};

} // namespace graphics

#endif // GRAPHICS_DIRECTION_ARROW_SHADER_PROGRAM_H
