#include "AABoundingBox.h"

#include <tuple>
#include <algorithm>

namespace graphics {

AABoundingBox2D::AABoundingBox2D(const glm::vec2& min, const glm::vec2& max) noexcept
  : m_min(min)
  , m_max(max)
{
  std::tie(m_min.x, m_max.x) = std::minmax(min.x, max.x);
  std::tie(m_min.y, m_max.y) = std::minmax(min.y, max.y);
}

void AABoundingBox2D::setMin(const glm::vec2& min) noexcept {
  m_min = min;
  if (m_max.x < m_min.x)
    m_max.x = m_min.x;
  if (m_max.y < m_min.y)
    m_max.y = m_min.y;
}

void AABoundingBox2D::setMax(const glm::vec2& max) noexcept{
  m_max = max;
  if (m_min.x > m_max.x)
    m_min.x = m_max.x;
  if (m_min.y > m_max.y)
    m_min.y = m_max.y;
}

AABoundingBox2D AABoundingBox2D::unionWith(const AABoundingBox2D& box) const noexcept {
  return {glm::min(m_min, box.m_min), glm::max(m_max, box.m_max)};
}

AABoundingBox2D& AABoundingBox2D::operator+=(const glm::vec2& offset) noexcept {
  m_min += offset;
  m_max += offset;
  return *this;
}
AABoundingBox2D& AABoundingBox2D::operator-=(const glm::vec2& offset) noexcept {
  m_min -= offset;
  m_max -= offset;
  return *this;
}

bool AABoundingBox2D::contains(const AABoundingBox2D& box) const noexcept {
  return glm::all(glm::lessThanEqual(m_min, box.m_min)) &&
         glm::all(glm::lessThanEqual(box.m_max, m_max));
}
bool AABoundingBox2D::contains(const glm::vec2& position) const noexcept {
  return glm::all(glm::lessThanEqual(m_min, position)) &&
         // lessThan to guarantee boxes in contact with each other will not share points
         glm::all(glm::lessThan(position, m_max));
}

bool AABoundingBox2D::intersects(const AABoundingBox2D& box) const noexcept {
  // lessThan so non-overlapping boxes in contact with each other will not be counted
  // i.e. the boxes MUST overlap.
  return glm::all(glm::lessThan(m_min, box.m_max)) &&
         glm::all(glm::lessThan(box.m_min, m_max));
}

AABoundingBox2D operator+(AABoundingBox2D box, const glm::vec2& offset) noexcept {
  return box += offset;
}
AABoundingBox2D operator-(AABoundingBox2D box, const glm::vec2& offset) noexcept {
  return box -= offset;
}

} // namespace graphics
