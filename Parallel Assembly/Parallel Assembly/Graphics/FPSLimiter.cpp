#include "FPSLimiter.h"

#include <cmath>
#include <numeric>
#include <sstream>
#include <thread>

#include "../Assert.h"

namespace graphics {

using std::chrono::duration;
using std::chrono::seconds;
using std::chrono::duration_cast;
using namespace std::chrono_literals;

FPSLimiter::FPSLimiter(float targetFPS_)
  : m_targetFPS(targetFPS_)
  , m_expectedGap(duration_cast<Clock::duration>(duration<float>(1.f / targetFPS_)))
  , m_lastFrame(Clock::now())
  , m_frameGaps()
  , m_gapIndex(0ull)
{
  m_frameGaps.assign(windowSize, m_expectedGap);
  ASSERT(std::abs(m_targetFPS - computeFPS()) < m_targetFPS / 100'000.f);
}

void FPSLimiter::setTargetFPS(float targetFPS_) {
  m_targetFPS = targetFPS_;
  m_expectedGap = duration_cast<Clock::duration>(duration<float>(1.f / targetFPS_));
}

float FPSLimiter::computeFPS() const noexcept {
  auto meanGap = 
    duration<float>{
      std::accumulate(m_frameGaps.begin(), m_frameGaps.end(), Clock::duration())
    } / float(windowSize);
  return 1.f / meanGap.count();
}

void FPSLimiter::onFrame() {
  ASSERT(m_gapIndex < windowSize);

  auto frameTime = Clock::now();
  auto thisGap = frameTime - m_lastFrame;
  
  if (thisGap < m_expectedGap) {
    // Deliberately sleep 1.5ms less because sleep is imprecise
    auto timeToSleep = m_expectedGap - thisGap - 1500us;
    if (timeToSleep.count() > 0)
      std::this_thread::sleep_for(timeToSleep);
    do {
      frameTime = Clock::now();
      thisGap = frameTime - m_lastFrame;
    } while (thisGap < m_expectedGap);
  }

  m_lastFrame = frameTime;

  m_frameGaps[m_gapIndex] = thisGap;
  m_gapIndex = (m_gapIndex + 1) % windowSize;
}

} // namespace graphics
