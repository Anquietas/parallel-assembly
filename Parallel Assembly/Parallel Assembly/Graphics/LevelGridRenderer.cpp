#include "LevelGridRenderer.h"

#include <algorithm>
#include <array>
#include <utilitY>

#include <boost/container/flat_map.hpp>
#include <boost/graph/graph_utility.hpp>

#include "../Game/SimpleFactory.h"
#include "../Game/Worker.h"

#include "../GUI/LevelGridSelections.h"

#include "../Assert.h"
#include "../GLMIncludes.h"
#include "../Logger.h"

#include "OpenGL/checkError.h"
#include "OpenGL/VertexBuffer.inl"

#include "DirectionArrowShaderProgram.h"
#include "LevelGridShaderProgram.h"
#include "LevelWorkerPathShaderProgram.h"
#include "SelectionBoxShaderProgram.h"
#include "ShaderManager.inl"

namespace graphics {

namespace {
STATIC_LOGGER_DECLARE(logger);
}

LevelGridShaderProgram* LevelGridRenderer::levelGridShader = nullptr;
LevelWorkerPathShaderProgram* LevelGridRenderer::levelWorkerPathShader = nullptr;
DirectionArrowShaderProgram* LevelGridRenderer::directionArrowShader = nullptr;
SelectionBoxShaderProgram* LevelGridRenderer::selectionBoxShader = nullptr;

void LevelGridRenderer::setupShaders(ShaderManager& shaderManager) {
  ASSERT_MSG(!levelGridShader &&
             !levelWorkerPathShader &&
             !directionArrowShader &&
             !selectionBoxShader,
             "LevelGridRenderer::setupShaders() called after shaders were already initialized");

  using namespace std::string_literals;

  levelGridShader = shaderManager.createShader<LevelGridShaderProgram>();
  levelGridShader->initialize();

  levelWorkerPathShader = shaderManager.createShader<LevelWorkerPathShaderProgram>();
  levelWorkerPathShader->initialize();

  if (auto shader = shaderManager.lookupShader("DirectionArrowShader"s)) {
    directionArrowShader = static_cast<DirectionArrowShaderProgram*>(shader);
  } else {
    directionArrowShader = shaderManager.createShader<DirectionArrowShaderProgram>();
    directionArrowShader->initialize();
  }

  selectionBoxShader = shaderManager.createShader<SelectionBoxShaderProgram>();
  selectionBoxShader->initialize();

  ASSERT_MSG(levelGridShader &&
             levelWorkerPathShader &&
             directionArrowShader &&
             selectionBoxShader,
             "Shaders not initialized properly after calling LevelGridRenderer::setupShaders()");
}

LevelGridRenderer::LevelGridRenderer(const game::SimpleFactory& factory)
  : m_factory(&factory)
  , m_gridCellSize(0.f)
  , m_gridRenderOffset()
  , m_viewSize()
  , m_mvpMatrix(1.f) // Identity
  , m_mvpMatrixInvalid(true)
  , m_gridCellColours()
  , m_gridCellColoursInvalid(true)
  , m_gridVAO()
  , m_gridMeshVBO()
  , m_gridColourVBO()
  , m_pathRenderGraphs(factory.numWorkers())
  , m_pathMeshes(factory.numWorkers())
  , m_pathInvalid(factory.numWorkers(), Flag{true})
  , m_pathVAO()
  , m_pathVBO()
  , m_arrowData(factory.numWorkers())
  , m_directionArrowDataInvalid(factory.numWorkers(), Flag{true})
  , m_arrowVAO()
  , m_arrowMeshVBO()
  , m_arrowDataVBO()
  , m_renderSelectionBox(false)
  , m_selectionBox()
  , m_selectionBoxVAO()
  , m_selectionBoxVBO()
{
  using namespace constants;

  ASSERT(m_factory);
  ASSERT_MSG(levelGridShader && 
             levelWorkerPathShader && 
             directionArrowShader &&
             selectionBoxShader,
             "LevelGridRenderer constructed before calling setupShaders()");

  auto size = gridSize();
  ASSERT_MSG(glm::all(glm::greaterThan(size, {})), "LevelGridRenderer initialized with invalid configuration");

  auto& gridVertexPosAttribID = levelGridShader->vertexPosAttribID();
  auto& gridColourAttribID = levelGridShader->colourAttribID();

  m_gridVAO.bind();
  m_gridVAO.enableAttribute(gridVertexPosAttribID);
  m_gridVAO.enableAttribute(gridColourAttribID);
  m_gridVAO.setAttributeDivisor(gridColourAttribID, 1u);

  m_gridMeshVBO.bind();
  m_gridMeshVBO.allocate(gridCellMesh.size(), gridCellMesh.data());
  m_gridMeshVBO.bindToAttribute<glm::vec2>(gridVertexPosAttribID);

  m_gridColourVBO.bind();
  m_gridColourVBO.bindToAttribute<glm::vec4>(gridColourAttribID);

  m_gridCellColours.assign(size.x * size.y, gridCellColour);

  auto& pathVertexPosAttribID = levelWorkerPathShader->vertexPosAttribID();
  auto& pathTexCoordsAttribID = levelWorkerPathShader->texCoordsAttribID();

  m_pathVAO.bind();
  m_pathVAO.enableAttribute(pathVertexPosAttribID);
  m_pathVAO.enableAttribute(pathTexCoordsAttribID);

  gl::VertexBuffer::InterleavedFormat pathBufferFormat;
  pathBufferFormat.addAttribute<glm::vec2>(
    pathVertexPosAttribID, false, sizeof(PathGLVertex), offsetof(PathGLVertex, vertexPos));
  pathBufferFormat.addAttribute<glm::vec2>(
    pathTexCoordsAttribID, false, sizeof(PathGLVertex), offsetof(PathGLVertex, texCoords));

  m_pathVBO.bind();
  m_pathVBO.bindToAttributes(pathBufferFormat);

  CHECK_GL_ERROR();

  auto& arrowVertexPosAttribID = directionArrowShader->vertexPosAttribID();
  auto& arrowTexCoordsAttribID = directionArrowShader->texCoordsAttribID();
  auto& arrowOffsetAttribID = directionArrowShader->offsetAttribID();
  auto& arrowDirectionAttribID = directionArrowShader->directionAttribID();
  auto& arrowStateAttribID = directionArrowShader->stateAttribID();

  m_arrowVAO.bind();
  m_arrowVAO.enableAttribute(arrowVertexPosAttribID);
  m_arrowVAO.enableAttribute(arrowTexCoordsAttribID);
  m_arrowVAO.enableAttribute(arrowOffsetAttribID);
  m_arrowVAO.enableAttribute(arrowDirectionAttribID);
  m_arrowVAO.enableAttribute(arrowStateAttribID);
  m_arrowVAO.setAttributeDivisor(arrowOffsetAttribID, 1u);
  m_arrowVAO.setAttributeDivisor(arrowDirectionAttribID, 1u);
  m_arrowVAO.setAttributeDivisor(arrowStateAttribID, 1u);
  
  gl::VertexBuffer::InterleavedFormat arrowMeshBufferFormat;
  arrowMeshBufferFormat.addAttribute<glm::vec2>(
    arrowVertexPosAttribID, false, sizeof(DirectionArrowGLVertex), offsetof(DirectionArrowGLVertex, vertexPos));
  arrowMeshBufferFormat.addAttribute<glm::vec2>(
    arrowTexCoordsAttribID, false, sizeof(DirectionArrowGLVertex), offsetof(DirectionArrowGLVertex, texCoords));

  m_arrowMeshVBO.bind();
  m_arrowMeshVBO.bindToAttributes(arrowMeshBufferFormat);
  m_arrowMeshVBO.allocate(directionArrowMesh.size(), directionArrowMesh.data());

  CHECK_GL_ERROR();

  gl::VertexBuffer::InterleavedFormat arrowDataBufferFormat;
  arrowDataBufferFormat.addAttribute<glm::vec2>(
    arrowOffsetAttribID, false, sizeof(DirectionArrowGLData), offsetof(DirectionArrowGLData, offset));
  arrowDataBufferFormat.addAttribute<gl::int_t>(
    arrowDirectionAttribID, false, sizeof(DirectionArrowGLData), offsetof(DirectionArrowGLData, direction));
  arrowDataBufferFormat.addAttribute<gl::int_t>(
    arrowStateAttribID, false, sizeof(DirectionArrowGLData), offsetof(DirectionArrowGLData, state));

  m_arrowDataVBO.bind();
  m_arrowDataVBO.bindToAttributes(arrowDataBufferFormat);

  CHECK_GL_ERROR();

  auto& selectionBoxVertexPosAttribID = selectionBoxShader->vertexPosAttriBID();

  m_selectionBoxVAO.bind();
  m_selectionBoxVAO.enableAttribute(selectionBoxVertexPosAttribID);

  m_selectionBoxVBO.bind();
  m_selectionBoxVBO.allocate(selectionBoxMesh.size(), selectionBoxMesh.data());
  m_selectionBoxVBO.bindToAttribute<glm::vec2>(selectionBoxVertexPosAttribID, 0);

  CHECK_GL_ERROR();

  gl::VertexBuffer::unbind();
  gl::VertexArrayObject::unbind();
}

LevelGridRenderer::~LevelGridRenderer() = default;

void LevelGridRenderer::setGridRenderOffset(const glm::vec2& offset) {
  if (m_gridRenderOffset != offset) {
    m_gridRenderOffset = offset;
    m_mvpMatrixInvalid.set();
  }
}

void LevelGridRenderer::setViewSize(const glm::vec2& size) {
  ASSERT(glm::all(glm::greaterThan(gridSize(), {})));
  ASSERT_MSG(glm::all(glm::greaterThan(size, {})), "LevelGridRenderer::setViewSize() called with invalid size");
  if (m_viewSize != size) {
    m_viewSize = size;
    m_mvpMatrixInvalid.set();
  }
}

void LevelGridRenderer::setGridCellSize(float gridCellSize) {
  m_gridCellSize = gridCellSize;
}

glm::ivec2 LevelGridRenderer::gridSize() const {
  return m_factory->grid().gridSize();
}

void LevelGridRenderer::invalidateGrid() {
  m_gridCellColoursInvalid.set();
  for (auto& flag : m_pathInvalid)
    flag.set();
  for (auto& flag : m_directionArrowDataInvalid)
    flag.set();
}

void LevelGridRenderer::invalidateGridSelectionRenderData() {
  m_gridCellColoursInvalid.set();
}
void LevelGridRenderer::invalidateSelectionRenderData(
  const gui::DirectionArrowSelection& selection)
{
  ASSERT(m_factory && selection.workerID < m_factory->numWorkers());

  m_directionArrowDataInvalid[selection.workerID].set();
}
void LevelGridRenderer::invalidateSelectionRenderData(
  const gui::MultiSelection& selection)
{
  ASSERT(m_factory);
  const std::size_t numWorkers = m_factory->numWorkers();
  for (std::size_t worker = 0u; worker < numWorkers; ++worker) {
    if (selection.isIncluded(worker))
      m_directionArrowDataInvalid[worker].set();
  }
}
void LevelGridRenderer::invalidateSelectionRenderData(
  const gui::LevelGridHoverSelection& selection)
{
  if (selection.is<gui::GridCellSelection>())
    m_gridCellColoursInvalid.set();
  else if (selection.is<gui::DirectionArrowSelection>())
    invalidateSelectionRenderData(selection.as<gui::DirectionArrowSelection>());
  else {
    ASSERT(selection.is<gui::NullSelection>());
  }
}
void LevelGridRenderer::invalidateSelectionRenderData(
  const gui::LevelGridSelection& selection)
{
  if (selection.is<gui::GridCellSelection>())
    m_gridCellColoursInvalid.set();
  else if (selection.is<gui::DirectionArrowSelection>())
    invalidateSelectionRenderData(selection.as<gui::DirectionArrowSelection>());
  else if (selection.is<gui::MultiSelection>())
    invalidateSelectionRenderData(selection.as<gui::MultiSelection>());
  else {
    ASSERT(selection.is<gui::NullSelection>());
  }
}

void LevelGridRenderer::beginBoxSelection() {
  m_renderSelectionBox.set();
}
void LevelGridRenderer::updateBoxSelection(const AABoundingBox2D& selectionBox) {
  ASSERT(m_renderSelectionBox.get());
  m_selectionBox = selectionBox;
}
void LevelGridRenderer::endBoxSelection() {
  ASSERT(m_renderSelectionBox.get());
  m_renderSelectionBox.clear();
}

void LevelGridRenderer::invalidatePath(std::size_t worker) {
  ASSERT(worker < m_factory->numWorkers());

  m_pathInvalid[worker].set();
  m_directionArrowDataInvalid[worker].set();
}

bool LevelGridRenderer::isInsideGrid(const glm::ivec2& position) const {
  return glm::all(glm::greaterThanEqual(position, {})) &&
         glm::all(glm::lessThan(position, gridSize()));
}
bool LevelGridRenderer::isInsideGrid(const glm::vec2& position) const {
  return glm::all(glm::greaterThanEqual(position, {})) &&
         glm::all(glm::lessThan(position, glm::vec2{gridSize()}));
}

std::tuple<
  glm::ivec2,  // grid cell
  std::size_t, // worker ID
  bool         // direction?
> LevelGridRenderer::lookupGridPosition(const glm::vec2& gridPos) const {
  using namespace constants;

  ASSERT(m_factory);

  auto gridPosition = gridPos;

  if (!isInsideGrid(gridPosition))
    return {{-1, -1}, std::size_t(-1), false};

  const auto gridCellPos = glm::ivec2{glm::floor(gridPosition)};
  ASSERT(isInsideGrid(gridCellPos));
  const auto positionInCell = glm::mod(gridPosition, {1.f});
  ASSERT(glm::all(glm::greaterThanEqual(positionInCell, glm::vec2{})));
  ASSERT(glm::all(glm::lessThan(positionInCell, glm::vec2{1.f})));

  const auto numWorkers = m_factory->numWorkers();
  const auto& gridCell = m_factory->grid()(gridCellPos);

  for (std::size_t worker = 0u; worker < numWorkers; ++worker) {
    auto direction = gridCell.direction(worker);
    if (direction != game::Direction::None) {
      auto& bounds = lookupDirectionArrowBounds(worker, direction);
      if (bounds.contains(positionInCell))
        return {gridCellPos, worker, true};
    }
  }

  return {gridCellPos, std::size_t(-1), false};
}

std::size_t LevelGridRenderer::getGridCellColourIndex(const glm::ivec2& gridCell) {
  ASSERT(isInsideGrid(gridCell));

  return gridSize().x * gridCell.y + gridCell.x;
}

void LevelGridRenderer::updateGridCellColours(
  const gui::LevelGridHoverSelection& hoverSelection,
  const gui::LevelGridSelection& selection)
{
  using namespace constants;

  glm::ivec2 hoveredCell{-1, -1};
  glm::ivec2 selectedCell{-1, -1};

  if (hoverSelection.is<gui::GridCellSelection>()) {
    auto& hovered = hoverSelection.as<gui::GridCellSelection>();
    hoveredCell = hovered.gridCell;
    ASSERT((hoveredCell != glm::ivec2{-1, -1}));
  }
  if (selection.is<gui::GridCellSelection>()) {
    auto& selected = selection.as<gui::GridCellSelection>();
    selectedCell = selected.gridCell;
    ASSERT((selectedCell != glm::ivec2{-1, -1}));
  }

  const auto size = gridSize();
  
  for (int x = 0; x < size.x; ++x) {
    for (int y = 0; y < size.y; ++y) {
      const glm::ivec2 cell{x, y};
      const auto index = getGridCellColourIndex(cell);
      glm::vec4 cellColour = gridCellColour;

      if (cell == hoveredCell && cell == selectedCell)
        cellColour = gridCellSelectedHoverColour;
      else if (cell == hoveredCell)
        cellColour = gridCellHoverColour;
      else if (cell == selectedCell)
        cellColour = gridCellSelectedColour;

      m_gridCellColours[index] = cellColour;
    }
  }

}
void LevelGridRenderer::renderLevelGrid(
  const gui::LevelGridHoverSelection& hoverSelection,
  const gui::LevelGridSelection& selection)
{
  using namespace constants;

  ASSERT(m_gridCellColours.size() == gridSize().x * gridSize().y);

  m_gridVAO.bind();

  if (m_gridCellColoursInvalid.get()) {
    updateGridCellColours(hoverSelection, selection);

    m_gridColourVBO.bind();
    m_gridColourVBO.allocate(m_gridCellColours.size(), m_gridCellColours.data(), gl::VertexBuffer::Usage::DynamicDraw);

    m_gridCellColoursInvalid.clear();
  }

  levelGridShader->preRender();

  levelGridShader->setMVPMatrix(m_mvpMatrix);
  levelGridShader->setGridCellSize(m_gridCellSize);
  levelGridShader->setGridSize(gridSize());

  glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0,
                        GLsizei(gridCellMesh.size()),
                        GLsizei(m_gridCellColours.size()));

  levelGridShader->postRender();
}

// Computes distance on one axis; intended to be used with vectors that
// only differ in one axis
int singleAxisDistance(const glm::ivec2& x, const glm::ivec2& y) {
  auto offset = glm::abs(x - y);
  ASSERT_MSG(offset.x == 0 || offset.y == 0, "singleAxisDistance(): vectors differ in more than one axis");
  return std::max(offset.x, offset.y);
}

void LevelGridRenderer::generateWorkerPathGraph(std::size_t worker) {
  ASSERT(m_factory);
  ASSERT(m_pathRenderGraphs.size() == m_factory->numWorkers());

  using VertexDescriptor = boost::graph_traits<PathRenderGraph>::vertex_descriptor;
  struct PositionComp {
    bool operator()(const glm::ivec2& x, const glm::ivec2& y) const {
      return x.x < y.x || (x.x == y.x && x.y < y.y);
    }
  };
  using PastPositionMap = boost::container::flat_map<
    glm::ivec2, VertexDescriptor, PositionComp>;

  auto& graph = m_pathRenderGraphs[worker];
  graph.clear();

  auto vertexPropMap = boost::get(boost::vertex_bundle, graph);
  auto edgePropMap = boost::get(boost::edge_bundle, graph);

  PastPositionMap pastPositions;

  const auto initialPosition = m_factory->worker(worker).initialPosition();
  auto initialDirection = m_factory->worker(worker).initialDirection();
  auto position = initialPosition;
  auto direction = initialDirection;
  const auto& grid = m_factory->grid();
  auto gridCell = &grid(position);

  // Add start cell to graph
  const VertexDescriptor startVertex = boost::add_vertex({position, false}, graph);
  VertexDescriptor lastVertex = startVertex;
  // Start cell has a direction set?
  if (gridCell->direction(worker) != game::Direction::None) {
    initialDirection = gridCell->direction(worker);
    direction = initialDirection;
    pastPositions.emplace(position, lastVertex);
  }
  auto findIntersectCandidate =
    [&pastPositions, this, initialPosition, startVertex]
    (glm::ivec2& pos, game::Direction direction) -> std::pair<VertexDescriptor, bool> {
      const auto step = getStepOffset(direction);
      pos += step;
      while (isInsideGrid(pos)) {
        if (pos == initialPosition)
          return{startVertex, true};
        else {
          auto iter = pastPositions.find(pos);
          if (iter != pastPositions.end())
            return{iter->second, true};
          else
            pos += step;
        }
      }
      return{{0}, false};
    };
  auto splicePathEdge =
    [&graph](VertexDescriptor from, VertexDescriptor to, VertexDescriptor intersect,
              const glm::ivec2& fromPos, const glm::ivec2& toPos, const glm::ivec2& intersectPos,
              game::Direction edgeDirection)
    {
      ASSERT(edgeDirection != game::Direction::None);
      // Remove existing edge
      boost::remove_edge(from, to, graph);

      // Add edge for from -> intersect
      auto fromLength = singleAxisDistance(intersectPos, fromPos);
      boost::add_edge(from, intersect, {fromLength, edgeDirection}, graph);

      // Add edge for intersect -> to
      auto toLength = singleAxisDistance(intersectPos, toPos);
      boost::add_edge(intersect, to, {toLength, edgeDirection}, graph);
    };

  bool done = false;
  while (!done) {
    auto step = getStepOffset(direction);
    ASSERT(step != glm::ivec2{});
    auto nextPosition = position + step;
    // Find next grid cell that changes direction or the point where
    // the path hits the grid edge if there isn't a next grid cell
    while (isInsideGrid(nextPosition)) {
      gridCell = &grid(nextPosition);
      if (nextPosition != initialPosition &&
          gridCell->direction(worker) == game::Direction::None) {
        // Check for path self-intersection
        bool cwIntersect = false, ccwIntersect = false;
        glm::ivec2 cwPosition = nextPosition, ccwPosition = nextPosition;
        VertexDescriptor cwVertex{0}, ccwVertex{0};

        // Find existing graph vertex to the "right" of the current position
        std::tie(cwVertex, cwIntersect) = findIntersectCandidate(cwPosition, rotateCW(direction));
        // Find existing graph vertex to the "left" of the current position
        std::tie(ccwVertex, ccwIntersect) = findIntersectCandidate(ccwPosition, rotateCCW(direction));

        // vertices both left and right?
        if (cwIntersect && ccwIntersect) {
          // Either we have a new intersect for edge1/edge2 requiring splicing, we've been here before
          // and can stop after adding an edge, or we're between 2 graph vertices that don't have any
          // edges between them (e.g. corners  for other parts of the path) in which case there's 
          // nothing to do here.

          auto edge1 = boost::edge(cwVertex, ccwVertex, graph);
          auto edge1Direction = edge1.second ? edgePropMap[edge1.first].direction : game::Direction::None;
          auto edge2 = boost::edge(ccwVertex, cwVertex, graph);
          auto edge2Direction = edge2.second ? edgePropMap[edge2.first].direction : game::Direction::None;

          bool skipAddEdge = false;

          VertexDescriptor intersectVertex{0};
          // Edge exists in one or both directions; need to add a vertex to splice with
          if (edge1.second || edge2.second) {
            // Add vertex for the intersection point
            intersectVertex = boost::add_vertex({nextPosition, true}, graph);
            pastPositions.emplace(nextPosition, intersectVertex);
          } else {
            auto iter = pastPositions.find(nextPosition);
            // We've been in this position before; intersect is end of the path
            if (iter != pastPositions.end())
              intersectVertex = pastPositions.find(nextPosition)->second;
            // We're between 2 graph vertices that don't have any edges between them. nothing to do.
            else
              skipAddEdge = true;
          }

          if (!skipAddEdge) {
            auto length = singleAxisDistance(nextPosition, position);

            // Add edge from previous position to the intersection point
            boost::add_edge(lastVertex, intersectVertex, {length, direction}, graph);
            // Update lastVertex/position to prevent overlap
            lastVertex = intersectVertex;
            position = nextPosition;

            // cwVertex -> ccwVertex edge exists; splice it
            if (edge1.second)
              splicePathEdge(cwVertex, ccwVertex, lastVertex,
                             cwPosition, ccwPosition, nextPosition, edge1Direction);
            // ccwVertex -> cwVertex edge exists; splice it
            if (edge2.second)
              splicePathEdge(ccwVertex, cwVertex, lastVertex,
                             ccwPosition, cwPosition, nextPosition, edge2Direction);
          }
        }

        nextPosition += step;
      } else
        break;
    }
    VertexDescriptor nextVertex;
    // Left grid boundaries; path is incomplete
    if (!isInsideGrid(nextPosition)) {
      // Deliberately leave nextPosition sitting outside the grid so
      // path mesh will leave the bounds of the grid and be cropped to the grid

      // Add vertex marking the end of the path
      nextVertex = boost::add_vertex({nextPosition, false}, graph);
      // Don't need to bother adding position, vertex to past positions; we're done

      done = true;
    }
    // Path passes over start position again without changing direction
    else if (nextPosition == initialPosition &&
              gridCell->direction(worker) == game::Direction::None) {
      nextVertex = startVertex;
      vertexPropMap[nextVertex].multipleIntersect =
        isPerpendicular(initialDirection, direction);
      if (initialDirection == direction)
        done = true;
    }
    // Reached a grid cell with a direction entry
    else {
      ASSERT(gridCell->direction(worker) != game::Direction::None);
      ASSERT(&grid(nextPosition) == gridCell);
      bool sharpTurn = gridCell->direction(worker) == game::getOpposite(direction);
      auto iter = pastPositions.find(nextPosition);
      // Haven't been in this position before
      if (iter == pastPositions.end()) {
        // Add a vertex for the grid cell
        nextVertex = boost::add_vertex({nextPosition, sharpTurn}, graph);
        pastPositions.emplace(nextPosition, nextVertex);
      }
      // Already been to this position; we're done
      else {
        nextVertex = iter->second;
        if (sharpTurn)
          vertexPropMap[nextVertex].multipleIntersect = true;
        done = true;
      }
    }

    auto length = singleAxisDistance(nextPosition, position);
    // Add edge from lastVertex to nextVertex, storing length and direction
    boost::add_edge(lastVertex, nextVertex, {length, direction}, graph);

    if (!done) {
      position = nextPosition;
      if (gridCell->direction(worker) != game::Direction::None)
        direction = gridCell->direction(worker);
      lastVertex = nextVertex;
    }
  }
}
void LevelGridRenderer::generateWorkerPathMesh(std::size_t worker) {
  using namespace constants;

  ASSERT(m_factory);
  ASSERT(m_pathMeshes.size() == m_factory->numWorkers());

  using VertexDescriptor = boost::graph_traits<PathRenderGraph>::vertex_descriptor;
  using EdgeIterator = boost::graph_traits<PathRenderGraph>::edge_iterator;

  const auto& graph = m_pathRenderGraphs[worker];
  const auto vertexPropMap = boost::get(boost::vertex_bundle, graph);
  const auto edgePropMap = boost::get(boost::edge_bundle, graph);

  auto& mesh = m_pathMeshes[worker];
  mesh.clear();
  mesh.reserve(boost::num_edges(graph) * 6); // 2 triangles per graph edge

  const glm::vec2 pathOffset{workerPathOffset(worker)};

  EdgeIterator edgesIter, edgesEnd;
  std::tie(edgesIter, edgesEnd) = boost::edges(graph);
  for (; edgesIter != edgesEnd; ++edgesIter) {
    VertexDescriptor from, to;
    std::tie(from, to) = boost::incident(*edgesIter, graph);
    auto& fromProperties = boost::get(vertexPropMap, from);
    auto& toProperties = boost::get(vertexPropMap, to);
    auto& edgeProperties = boost::get(edgePropMap, *edgesIter);

    auto fromPosition = glm::vec2{fromProperties.position} + pathOffset;
    auto toPosition = glm::vec2{toProperties.position} + pathOffset;
    auto length = float(edgeProperties.length) * pathTextureMult;
    auto direction = edgeProperties.direction;
    bool bidiEdge = boost::edge(to, from, graph).second;

    const float pathWidth = bidiEdge ? halfPathWidth : fullPathWidth;

    PathGLVertex topLeft, topRight, bottomLeft, bottomRight;
    auto addRectToMesh = [&] {
      mesh.push_back(topLeft);
      mesh.push_back(topRight);
      mesh.push_back(bottomLeft);
      mesh.push_back(topRight);
      mesh.push_back(bottomRight);
      mesh.push_back(bottomLeft);
    };

    // If source vertex is marked as multiple intersect, draw the first segment
    // of the edge differently to the rest
    if (fromProperties.multipleIntersect) {
      topLeft     = {fromPosition,                                      {0.f, 0.f}};
      topRight    = {fromPosition + glm::vec2{fullPathWidth, 0.f},      {1.f, 0.f}};
      bottomLeft  = {fromPosition + glm::vec2{0.f, fullPathWidth},      {0.f, 0.5f}};
      bottomRight = {fromPosition + glm::vec2{fullPathWidth},           {1.f, 0.5f}};
      addRectToMesh();

      length -= 1.f; // need 1 less segment of the path
    }

    switch (direction) {
    case game::Direction::Left: {
      const float offset = bidiEdge ? pathWidth : 0.f;
      topLeft       = {toPosition + glm::vec2{fullPathWidth, offset},   {length, 1.f}};
      bottomLeft    = {topLeft.vertexPos + glm::vec2{0.f, pathWidth},   {length, 0.5f}};
      if (fromProperties.multipleIntersect || bidiEdge)
        topRight    = {fromPosition + glm::vec2{0.f, offset},           {0.f, 1.f}};
      else
        topRight    = {fromPosition + glm::vec2{fullPathWidth, offset}, {0.f, 1.f}};
      bottomRight   = {topRight.vertexPos + glm::vec2{0.f, pathWidth},  {0.f, 0.5f}};
    } break;
    case game::Direction::Right:
      if (fromProperties.multipleIntersect || bidiEdge)
        topLeft     = {fromPosition + glm::vec2{fullPathWidth, 0.f},    {0.f, 0.5f}};
      else
        topLeft     = {fromPosition,                                    {0.f, 0.5f}};
      bottomLeft    = {topLeft.vertexPos + glm::vec2{0.f, pathWidth},   {0.f, 1.f}};
      topRight      = {toPosition,                                      {length, 0.5f}};
      bottomRight   = {topRight.vertexPos + glm::vec2{0.f, pathWidth},  {length, 1.f}};
      break;
    case game::Direction::Up:
      topLeft       = {toPosition + glm::vec2{0.f, fullPathWidth},      {length, 0.5f}};
      topRight      = {topLeft.vertexPos + glm::vec2{pathWidth, 0.f},   {length, 1.f}};
      if (fromProperties.multipleIntersect || bidiEdge)
        bottomLeft  = {fromPosition,                                    {0.f, 0.5f}};
      else
        bottomLeft  = {fromPosition + glm::vec2{0.f, fullPathWidth},    {0.f, 0.5f}};
      bottomRight   = {bottomLeft.vertexPos + glm::vec2{pathWidth, 0.f},{0.f, 1.f}};
      break;
    case game::Direction::Down: {
      const float offset = bidiEdge ? pathWidth : 0.f;
      if (fromProperties.multipleIntersect || bidiEdge)
        topLeft     = {fromPosition + glm::vec2{offset, fullPathWidth}, {0.f, 0.5f}};
      else
        topLeft     = {fromPosition + glm::vec2{offset, 0.f},           {0.f, 0.5f}};
      topRight      = {topLeft.vertexPos + glm::vec2{pathWidth, 0.f},   {0.f, 1.f}};
      bottomLeft    = {toPosition + glm::vec2{offset, 0.f},             {length, 0.5f}};
      bottomRight   = {bottomLeft.vertexPos + glm::vec2{pathWidth, 0.f},{length, 1.f}};
    } break;
    default:
      ASSERT_MSG(false, "Unhandled direction in switch");;
    }
    addRectToMesh();
  }
}
void LevelGridRenderer::renderWorkerPaths() {
  using namespace constants;

  const std::size_t numWorkers = m_factory->numWorkers();
  for (std::size_t worker = 0u; worker < numWorkers; ++worker) {
    if (m_pathInvalid[worker].get()) {
      generateWorkerPathGraph(worker);
      generateWorkerPathMesh(worker);

      m_pathInvalid[worker].clear();
    }
  }

  m_pathVAO.bind();
  m_pathVBO.bind();

  levelWorkerPathShader->preRender();
  levelWorkerPathShader->setMVPMatrix(m_mvpMatrix);
  levelWorkerPathShader->setGridCellSize(m_gridCellSize);

  ASSERT(m_pathMeshes.size() == numWorkers);
  for (std::size_t worker = 0u; worker < numWorkers; ++worker) {
    const auto& mesh = m_pathMeshes[worker];

    levelWorkerPathShader->setBackgroundColour(pathBackColours[worker]);
    levelWorkerPathShader->setForegroundColour(pathArrowColours[worker]);

    m_pathVBO.allocate(mesh.size(), mesh.data(), gl::VertexBuffer::Usage::DynamicDraw);

    glDrawArrays(GL_TRIANGLES, 0, graphics::gl::sizei_t(mesh.size()));
  }

  levelWorkerPathShader->postRender();
}

void LevelGridRenderer::generateDirectionArrowRenderData(
  std::size_t worker,
  const gui::LevelGridHoverSelection& hoverSelection,
  const gui::LevelGridSelection& selection,
  const gui::MultiSelection& boxSelection)
{
  using namespace constants;

  ASSERT(m_factory);

  const auto numWorkers = m_factory->numWorkers();
  ASSERT(m_arrowData.size() == numWorkers);

  m_arrowData[worker].clear();

  auto getGLDirectionIndex =
    [](game::Direction direction) -> gl::int_t {
      ASSERT(direction != game::Direction::None);
      switch (direction) {
      case game::Direction::Left:
        return 0;
      case game::Direction::Right:
        return 1;
      case game::Direction::Up:
        return 2;
      case game::Direction::Down:
        return 3;
      default:
        ASSERT_MSG(false, "Unhandled direction in switch");
        return -1;
      }
    };
  auto getGLStateIndex =
    [&](const glm::ivec2& gridCellPos, std::size_t worker) -> gl::int_t {
      const gui::DirectionArrowSelection arrowSelection{gridCellPos, worker};
      
      const bool isHover = hoverSelection.contains(arrowSelection);
      const bool isSelected = selection.contains(arrowSelection) ||
        boxSelection.contains(arrowSelection);

      if (isHover && isSelected)
        return 3;
      else if (isSelected)
        return 2;
      else if (isHover)
        return 1;
      else
        return 0;
    };

  const auto size = gridSize();
  for (int x = 0; x < size.x; ++x) {
    for (int y = 0; y < size.y; ++y) {
      const glm::ivec2 gridCellPos{x, y};
      auto& gridCell = m_factory->grid()(gridCellPos);
      const glm::vec2 cellOffset{gridCellPos};

      const auto direction = gridCell.direction(worker);

      if (direction == game::Direction::None)
        continue;
      else
        m_arrowData[worker].push_back({
          cellOffset + lookupDirectionArrowMeshOffset(worker, direction),
          getGLDirectionIndex(direction),
          getGLStateIndex(gridCellPos, worker)
        });
    }
  }
}
void LevelGridRenderer::renderDirectionArrows(
  const gui::LevelGridHoverSelection& hoverSelection,
  const gui::LevelGridSelection& selection,
  const gui::MultiSelection& boxSelection)
{
  using namespace constants;

  const std::size_t numWorkers = m_factory->numWorkers();
  for (std::size_t worker = 0u; worker < numWorkers; ++worker) {
    if (m_directionArrowDataInvalid[worker].get()) {
      generateDirectionArrowRenderData(worker, hoverSelection, selection, boxSelection);
      
      m_directionArrowDataInvalid[worker].clear();
    }
  }

  m_arrowVAO.bind();
  m_arrowDataVBO.bind();

  directionArrowShader->preRender();
  directionArrowShader->setMVPMatrix(m_mvpMatrix);
  directionArrowShader->setGridCellSize(m_gridCellSize);

  ASSERT(m_arrowData.size() == numWorkers);
  for (std::size_t worker = 0u; worker < numWorkers; ++worker) {
    const auto& data = m_arrowData[worker];
    if (data.empty())
      continue;

    directionArrowShader->setArrowColour(directionArrowColours[worker]);
    directionArrowShader->setBorderColour(directionArrowBorderColours[worker]);
    
    m_arrowDataVBO.allocate(data.size(), data.data(), gl::VertexBuffer::Usage::DynamicDraw);

    glDrawArraysInstanced(GL_TRIANGLES, 0, gl::sizei_t(directionArrowMesh.size()), gl::sizei_t(data.size()));
  }

  directionArrowShader->postRender();
}

void LevelGridRenderer::renderSelectionBox() {
  using namespace constants;

  m_selectionBoxVAO.bind();

  selectionBoxShader->preRender();
  selectionBoxShader->setMVPMatrix(m_mvpMatrix);
  selectionBoxShader->setBoxSize(m_selectionBox.size());
  selectionBoxShader->setBoxPosition(m_selectionBox.position());
  selectionBoxShader->setGridCellSize(m_gridCellSize);

  selectionBoxShader->setColour(selectionBoxColour);
  glDrawArrays(GL_TRIANGLE_STRIP, selectionBoxInteriorOffset, selectionBoxInteriorSize);

  selectionBoxShader->setColour(selectionBoxBorderColour);
  glDrawArrays(GL_LINE_LOOP, selectionBoxBorderOffset, selectionBoxBorderSize);

  selectionBoxShader->postRender();
}

void LevelGridRenderer::render(
  const gui::LevelGridHoverSelection& hoverSelection,
  const gui::LevelGridSelection& selection,
  const gui::MultiSelection& boxSelection)
{
  using namespace constants;

  glClearColor(gridBackColour.r,
               gridBackColour.g,
               gridBackColour.b,
               gridBackColour.a);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glViewport(0, 0, GLsizei(m_viewSize.x), GLsizei(m_viewSize.y));
  if (m_mvpMatrixInvalid.get()) {
    auto min = -m_gridRenderOffset;
    auto max = min + m_viewSize;
    m_mvpMatrix = glm::ortho(min.x, max.x, min.y, max.y);

    m_mvpMatrixInvalid.clear();
  }

  GLint oldScissorBox[4]{0, 0, 0, 0};
  glGetIntegerv(GL_SCISSOR_BOX, oldScissorBox);

  auto gridOffset = glm::ivec2{glm::round(m_gridRenderOffset)};
  auto gridSize = glm::ivec2{glm::round(m_gridCellSize * glm::vec2{m_factory->grid().gridSize()})};
  glScissor(gridOffset.x, gridOffset.y, gridSize.x, gridSize.y);

  renderLevelGrid(hoverSelection, selection);
  CHECK_GL_ERROR_PEDANTIC();
  renderWorkerPaths();
  CHECK_GL_ERROR_PEDANTIC();
  renderDirectionArrows(hoverSelection, selection, boxSelection);
  CHECK_GL_ERROR_PEDANTIC();

  glScissor(oldScissorBox[0], oldScissorBox[1], oldScissorBox[2], oldScissorBox[3]);
  // Render after restoring scissor to allow selection box to appear outside the grid
  if (m_renderSelectionBox.get()) {
    renderSelectionBox();
    CHECK_GL_ERROR_PEDANTIC();
  }

  gl::VertexBuffer::unbind();
  gl::VertexArrayObject::unbind();
}

namespace {
STATIC_LOGGER_INIT(logger) {
  logging::Logger lg;
  logging::setModuleName(lg, "LevelGridRenderer");
  return lg;
}
}

} // namespace graphics
