#pragma once
#ifndef GRAPHICS_SHADER_PROGRAM_BASE_H
#define GRAPHICS_SHADER_PROGRAM_BASE_H

#include <string>

namespace graphics {

class ShaderProgramBase {
  std::string m_name;
  bool m_initialized;

protected:
  void setInitialized() noexcept { m_initialized = true; }
  void clearInitialized() noexcept { m_initialized = false; }

  ShaderProgramBase(std::string name_) noexcept;
public:
  ShaderProgramBase(const ShaderProgramBase&) = delete;
  ShaderProgramBase(ShaderProgramBase&& o) noexcept;
  virtual ~ShaderProgramBase();

  ShaderProgramBase& operator=(const ShaderProgramBase&) = delete;
  ShaderProgramBase& operator=(ShaderProgramBase&& o) noexcept;

  const std::string& name() const noexcept { return m_name; }

  // Perform any initialization required for the shader program
  // Default implementation only sets m_initialized to true
  virtual void initialize();
  // Perform any cleanup required for the shader program
  // Default implementation only sets m_initialized to false
  virtual void cleanup();

  bool isInitialized() const noexcept { return m_initialized; }

  // Perform any setup required just prior to rendering
  virtual void preRender() = 0;
  // Perform any cleanup required just after rendering
  virtual void postRender() = 0;
};

}

#endif // GRAPHICS_SHADER_PROGRAM_BASE_H
