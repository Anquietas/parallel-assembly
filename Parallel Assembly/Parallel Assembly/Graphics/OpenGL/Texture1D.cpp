#include "Texture1D.h"

#include "OpenGLIncludes.h"

#include "../../Assert.h"

namespace graphics {
namespace gl {

void Texture1DBase::bind() noexcept {
  glBindTexture(GL_TEXTURE_1D, m_id);
}
void Texture1DBase::unbind() noexcept {
  glBindTexture(GL_TEXTURE_1D, uint_t(0));
}

sizei_t Texture1DBase::width(int_t mipMapLevel) const noexcept {
  int_t width = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_1D, mipMapLevel, GL_TEXTURE_WIDTH, &width);
  return static_cast<sizei_t>(width);
}

bool Texture1DBase::isCompressed() const noexcept {
  int_t value = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_1D, 0, GL_TEXTURE_COMPRESSED, &value);
  return value == GL_TRUE;
}
sizei_t Texture1DBase::compressedSize() const noexcept {
  int_t size = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_1D, 0, GL_TEXTURE_COMPRESSED_IMAGE_SIZE, &size);
  return static_cast<sizei_t>(size);
}


void ColourTexture1D::allocate(int_t mipMapLevel, internal_format::Colour internalFormat,
                               sizei_t width, format::Colour pixelFormat,
                               ImageType pixelType, const void* pixelData) noexcept
{
  auto internalFormatCode = getFormatCode(internalFormat);
  auto formatCode = getFormatCode(pixelFormat);
  auto typeCode = getTypeCode(pixelType);
  glTexImage1D(GL_TEXTURE_1D, mipMapLevel, internalFormatCode,
               width, 0, formatCode, typeCode, pixelData);
}
void ColourTexture1D::update(int_t mipMapLevel, int_t offset, sizei_t width,
                             format::Colour pixelFormat, ImageType pixelType,
                             const void* pixelData) noexcept
{
  ASSERT(pixelData);
  auto formatCode = getFormatCode(pixelFormat);
  auto typeCode = getTypeCode(pixelType);
  glTexSubImage1D(GL_TEXTURE_1D, mipMapLevel, offset, width, formatCode, typeCode, pixelData);
}
void ColourTexture1D::generateMipMaps() noexcept {
  glGenerateMipmap(GL_TEXTURE_1D);
}

glm::ivec4 ColourTexture1D::rgbaSizes() const noexcept {
  static_assert(sizeof(glm::ivec4) == 4 * sizeof(int_t),
                "Size discrepancy between glm::ivec4 and int_t");
  glm::ivec4 sizes;
  glGetTexLevelParameteriv(GL_TEXTURE_1D, 0, GL_TEXTURE_RED_SIZE, &sizes.r);
  glGetTexLevelParameteriv(GL_TEXTURE_1D, 0, GL_TEXTURE_GREEN_SIZE, &sizes.g);
  glGetTexLevelParameteriv(GL_TEXTURE_1D, 0, GL_TEXTURE_BLUE_SIZE, &sizes.b);
  glGetTexLevelParameteriv(GL_TEXTURE_1D, 0, GL_TEXTURE_ALPHA_SIZE, &sizes.a);
  return sizes;
}

internal_format::Colour ColourTexture1D::internalFormat() const {
  int_t formatCode = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_1D, 0, GL_TEXTURE_INTERNAL_FORMAT, &formatCode);
  return getColourFormat(static_cast<enum_t>(formatCode));
}


void DepthStencilTexture1D::allocate(internal_format::DepthStencil internalFormat, sizei_t width,
                                     format::DepthStencil dataFormat, ImageType dataType,
                                     const void* data) noexcept
{
  auto internalFormatCode = getFormatCode(internalFormat);
  auto formatCode = getFormatCode(dataFormat);
  auto typeCode = getTypeCode(dataType);
  glTexImage1D(GL_TEXTURE_1D, 0, internalFormatCode, width, 0, formatCode, typeCode, data);
}
void DepthStencilTexture1D::update(int_t offset, sizei_t width, format::DepthStencil dataFormat,
                                   ImageType dataType, const void* data) noexcept
{
  ASSERT(data);
  auto formatCode = getFormatCode(dataFormat);
  auto typeCode = getTypeCode(dataType);
  glTexSubImage1D(GL_TEXTURE_1D, 0, offset, width, formatCode, typeCode, data);
}

sizei_t DepthStencilTexture1D::depthSize() const noexcept {
  int_t size = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_1D, 0, GL_TEXTURE_DEPTH_SIZE, &size);
  return static_cast<sizei_t>(size);
}
sizei_t DepthStencilTexture1D::stencilSize() const noexcept {
  int_t size = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_1D, 0, GL_TEXTURE_STENCIL_SIZE, &size);
  return static_cast<sizei_t>(size);
}

internal_format::DepthStencil DepthStencilTexture1D::internalFormat() const {
  int_t formatCode = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_1D, 0, GL_TEXTURE_INTERNAL_FORMAT, &formatCode);
  return getDepthStencilFormat(static_cast<enum_t>(formatCode));
}

} // namespace gl
} // namespace graphics
