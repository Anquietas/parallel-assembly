#include "Shader.h"

#include "OpenGLIncludes.h"

#include <type_traits>
#include <fstream>
#include <sstream>
#include <iterator>
#include <memory>

#include "../../Assert.h"
#include "../../Exception.h"

namespace graphics {
namespace gl {

// Helper functions

// Converts enum to OpenGL type code
GLenum typeCode(Shader::Type type) noexcept;
// Verifies complication was successful
bool checkCompile(uint_t id) noexcept;
// Exactly what it sounds like
void loadSourceFile(const std::string& fileName, std::string& output);

GLenum typeCode(Shader::Type type) noexcept {
  static constexpr const GLenum constants[] = {
    GL_VERTEX_SHADER,
    GL_FRAGMENT_SHADER,
    GL_GEOMETRY_SHADER,
  };
  auto typeBase = static_cast<std::underlying_type_t<Shader::Type>>(type);
  ASSERT(0 <= typeBase && typeBase < std::extent<decltype(constants)>::value);
  return constants[typeBase];
}

const char* Shader::typeString(Type shaderType) noexcept {
  static constexpr const char* const strings[] = {
    "vertex shader",
    "fragment shader",
    "geometry shader",
  };
  auto typeBase = static_cast<std::underlying_type_t<Shader::Type>>(shaderType);
  ASSERT(0 <= typeBase && typeBase < std::extent<decltype(strings)>::value);
  return strings[typeBase];
}

Shader::Shader(uint_t id) noexcept
  : m_id(id)
  , m_state((id == 0) ? State::Uninitialized : State::Compiled)
{
  ASSERT(m_id == 0 || glIsShader(m_id) == GL_TRUE);
}
Shader::Shader(Shader&& other) noexcept
  : m_id(std::exchange(other.m_id, 0))
  , m_state(std::exchange(other.m_state, State::Uninitialized))
{}
Shader::~Shader() {
  if (m_id != 0)
    glDeleteShader(m_id);
}

Shader& Shader::operator=(Shader&& other) noexcept {
  if (m_id != 0)
    glDeleteShader(m_id);
  m_id = std::exchange(other.m_id, 0);
  m_state = std::exchange(other.m_state, State::Uninitialized);
  return *this;
}

bool checkCompile(uint_t id) noexcept {
  GLint compileStatus = GL_FALSE;
  glGetShaderiv(id, GL_COMPILE_STATUS, &compileStatus);
  return compileStatus == GL_TRUE;
}

bool Shader::compile(const std::string& source, Type shaderType) {
  ASSERT(!source.empty());
  ASSERT(m_state == State::Uninitialized);
  ASSERT(source.length() < static_cast<std::size_t>(std::numeric_limits<int_t>::max()));
  m_id = glCreateShader(typeCode(shaderType));
  if (m_id == 0) {
    std::ostringstream error;
    error << "Failed to create " << Shader::typeString(shaderType);
    m_state = State::Error;
    throw MAKE_EXCEPTION(error.str());
  }
  // Save the pointer to ensure it persists long enough
  // for glShaderSource to finish using it.
  // (strange quirks of GCC's standard library)
  const char* s = source.c_str();
  int_t length = static_cast<int_t>(source.length());
  glShaderSource(m_id, 1, &s, &length);
  glCompileShader(m_id);
  if (!checkCompile(m_id)) {
    m_state = State::Error;
    // Deliberately leave shader allocated - it stores the error log
    return false;
  }
  m_state = State::Compiled;
  return true;
}

void loadSourceFile(const std::string& fileName, std::string& output) {
  ASSERT(output.empty());

  // Determine file size and reserve storage
  std::ifstream in(fileName.c_str(),
                   std::ios::in | std::ios::binary | std::ios::ate);
  if (!in)
    throw MAKE_EXCEPTION("Unable to open shader source file: " + fileName);
  std::size_t fileSize = static_cast<std::size_t>(in.tellg());

  output.reserve(fileSize);

  // Seek to start of file to read data
  in.seekg(0);

  // Read data from file
  output.assign(std::istreambuf_iterator<char>(in),
                std::istreambuf_iterator<char>());
}

bool Shader::compileFromFile(const std::string& fileName, Type shaderType) {
  std::string source;
  loadSourceFile(fileName, source);
  return compile(source, shaderType);
}

void Shader::clearError() noexcept {
  // If false, then no error occurred - do nothing
  if (m_state == State::Error) {
    m_state = State::Uninitialized;
    if (m_id != 0)
      glDeleteShader(m_id);
  }
}

std::string Shader::getCompileLog() const {
  ASSERT(m_state != State::Uninitialized);

  // Query log size
  GLint logSize = 0;
  glGetShaderiv(m_id, GL_INFO_LOG_LENGTH, &logSize);
  ASSERT(logSize >= 0);

  std::unique_ptr<char[]> logBuffer {new char[logSize]};
  GLsizei logLength = 0;
  glGetShaderInfoLog(m_id, logSize, &logLength, logBuffer.get());
  ASSERT((logSize == 0 && logLength == 0) || logLength == logSize - 1);

  return std::string(logBuffer.get(), logLength);
}

} // namespace gl
} // namespace graphics
