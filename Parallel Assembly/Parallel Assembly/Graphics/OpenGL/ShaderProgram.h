#ifndef GRAPHICS_GL_SHADERPROGRAM_H
#define GRAPHICS_GL_SHADERPROGRAM_H

#pragma once

#include "../../GLMIncludes.h"

#include "Types.h"
#include "Shader.h"

#include "AttributeID.h"
#include "UniformID.h"
#include "TextureUnit.h"

#include <string>
#include <vector>
#include <memory>
#include <map>
#include <utility>

namespace graphics {
namespace gl {

// Models an OpenGL shader program composed of shader objects
// - Note: not thread safe
class ShaderProgram final {
protected:
  uint_t m_id;

  typedef std::vector<std::shared_ptr<Shader>> ShaderGroup;
  ShaderGroup m_shaders;

  enum class State {
    Assembly,
    Linked,
    Error
  };
  State m_state;

  // Non-owning pointer
  Shader* m_lastCompiled;

public:
  // Creates an empty shader program if id = 0, and assumes the shader
  // has already been linked if given a non-zero id.
  ShaderProgram(uint_t id = 0) noexcept;
  ShaderProgram(const ShaderProgram&) = delete;
  ShaderProgram(ShaderProgram&&) noexcept;
  ~ShaderProgram();

  ShaderProgram& operator=(const ShaderProgram&) = delete;
  ShaderProgram& operator=(ShaderProgram&&) noexcept;

  // Destroys the program object and returns the program to its initial state
  void destroy() noexcept;

  uint_t id() const noexcept { return m_id; }

  // Adds an externally created shader to the program
  // - Note: make sure the shader is compiled before calling link()!
  void addShader(std::shared_ptr<Shader> shader);
  // Adds a shader from its source code and compiles it
  // - Returns true if the shader compiled successfully, false otherwise
  bool addShader(const std::string& source, Shader::Type shaderType);
  // Adds a shader directly from its source file and compiles it
  // - Returns true if the shader compiled successfully, false otherwise
  bool addShaderFromFile(const std::string& fileName,
                         Shader::Type shaderType);

  // Helper function for handling the common case of a shader program consisting of
  // a vertex and fragment shader. This effectively calls addShaderFromFile
  // with the appropriate arguments for each shader and verifies if they compiled.
  // If any shader does not compile, an exception will be thrown containing the
  // compile log from the first shader that failed to compile.
  // Additionally if the compile log is non-empty after a successful compilation,
  // a warning will be recorded in the program log.
  void checkedAddShadersFromFiles(const std::string& vertexShaderFileName,
                                  const std::string& fragmentShaderFileName);

  // Retrieves the compilation log for the most recently compiled shader
  // - Note: this should be called immediately after addShader()
  // - This will return an empty string if no shader was compiled
  std::string getCompileLog() const;
  // Removes all Shader objects from the ShaderProgram
  // Note: these are not required to keep using a linked shader program
  // Note 2: this is not done automatically after building, to allow
  //         shaders to be reused
  void dumpShaders() noexcept;

  // Creates a program object for the shader
  // - Provided to allow separating the creation of the program object from linking the program
  void createProgramObject();

  // Binds the given attribute to the given attribute index
  // This must be called after creating the program object but before linking the program
  void bindAttributeLocation(const char* name, const AttributeID& id) noexcept;

  // Links the shader into a usable program
  // - Creates a program object for the shader if one wasn't previously created.
  // - This can be called again if the first attempt failed, but note
  //   that if an exception is thrown, it probably won't work the second
  //   time either.
  // - Note: do not call again after a successful link.
  // - Throws std::runtime_error on failure to create the program object
  // - makeRetrievable controls whether to make the program binary
  //   retrievable e.g. to cache it for later use
  bool link(bool makeRetrievable = false,
            bool makeSeparable = false);

  // Helper function for linking the program and verifying it linked
  // This effectively calls link with the given arguments and verifies the
  // program linked successfully. If it failed to link, an exception will
  // be thrown containing the link log.
  // Additionally if the link log is non-empty after a successful link,
  // a warning will be recorded in the program log
  // - The name parameter is used to identify the particular shader in the
  //   exception message and log entry as appropriate.
  void checkedLink(const std::string& name,
                   bool makeRetrievable = false,
                   bool makeSeparable = false);

  // Retrieves the linked program binary e.g. for caching
  // - This should be used with extreme caution, and only after
  //   the shader has been used to draw a few frames.
  std::pair<std::vector<ubyte_t> /*binary*/, enum_t /*Format code (GLenum)*/>
      getBinary() const;
  // Creates a new shader program from its binary code
  // - This should be used with extreme caution as driver updates
  //   or other system changes may break them.
  // - Returns true if OpenGL accepts the binary, false otherwise.
  //   In the event false is returned, the shader should be recompiled
  //   from the source code instead.
  bool useBinary(const std::vector<ubyte_t>& binary, enum_t formatCode);

  // Retrieves the link log for the shader program
  // - Note: this should be called *immediately* after link()
  // - Do not call this on an unlinked program!
  std::string getLinkLog() const;
  inline bool isLinked() const { return m_state == State::Linked; }
  inline bool error() const { return m_state == State::Error; }

  // Clears error state allowing for object reuse
  // - This will also erase any compile or link logs
  // - Does nothing if no error occurred
  void clearError() noexcept; 

  // Only call these functions after a successful link!
  // Returns the number of vertex shader attributes used by the program
  int_t activeAttributes() const noexcept;
  // Returns the number of uniform variables used by the program
  int_t activeUniforms() const noexcept;

  // Retrieves the location of an attribute
  AttributeID getAttributeLocation(const char* name) const noexcept;
  // Retrieves the location of a uniform variable
  UniformID getUniformLocation(const char* name) const noexcept;

  // Set single uniform values
  template <typename T>
  void setUniform(const UniformID& id, const T& value) noexcept {
    static_assert(false, "Attempt to use setUniform with unspecialized type");
  }
  // Set uniform arrays
  // - Here, length is the length of the array.
  template <typename T>
  void setUniform(const UniformID& id, const T* values, sizei_t length) noexcept {
    static_assert(false, "Attempt to use setUniform with unspecialized type");
  }

  void bind() const noexcept;
  void unbind() const noexcept;
};

// Forward declarations; implementation in cpp file
#define UNIFORM_FWD_DEC(T) \
  template <> \
  void ShaderProgram::setUniform<T>(const UniformID& id, const T& value) noexcept
#define UNIFORM_FWD_DEC_ARRAY(T) \
  template <> \
  void ShaderProgram::setUniform<T>(const UniformID& id, const T* values, sizei_t length) noexcept

UNIFORM_FWD_DEC(int_t);
UNIFORM_FWD_DEC(uint_t);
UNIFORM_FWD_DEC(float_t);
UNIFORM_FWD_DEC(TextureUnit);
UNIFORM_FWD_DEC(glm::ivec2);
UNIFORM_FWD_DEC(glm::uvec2);
UNIFORM_FWD_DEC(glm::vec2);
UNIFORM_FWD_DEC(glm::ivec3);
UNIFORM_FWD_DEC(glm::uvec3);
UNIFORM_FWD_DEC(glm::vec3);
UNIFORM_FWD_DEC(glm::ivec4);
UNIFORM_FWD_DEC(glm::uvec4);
UNIFORM_FWD_DEC(glm::vec4);
UNIFORM_FWD_DEC(glm::quat);
UNIFORM_FWD_DEC(glm::mat2);
UNIFORM_FWD_DEC(glm::mat2x3);
UNIFORM_FWD_DEC(glm::mat2x4);
UNIFORM_FWD_DEC(glm::mat3);
UNIFORM_FWD_DEC(glm::mat3x2);
UNIFORM_FWD_DEC(glm::mat3x4);
UNIFORM_FWD_DEC(glm::mat4);
UNIFORM_FWD_DEC(glm::mat4x2);
UNIFORM_FWD_DEC(glm::mat4x3);
UNIFORM_FWD_DEC(glm::dualquat);

UNIFORM_FWD_DEC_ARRAY(int_t);
UNIFORM_FWD_DEC_ARRAY(uint_t);
UNIFORM_FWD_DEC_ARRAY(float_t);
UNIFORM_FWD_DEC_ARRAY(TextureUnit);
UNIFORM_FWD_DEC_ARRAY(glm::ivec2);
UNIFORM_FWD_DEC_ARRAY(glm::uvec2);
UNIFORM_FWD_DEC_ARRAY(glm::vec2);
UNIFORM_FWD_DEC_ARRAY(glm::ivec3);
UNIFORM_FWD_DEC_ARRAY(glm::uvec3);
UNIFORM_FWD_DEC_ARRAY(glm::vec3);
UNIFORM_FWD_DEC_ARRAY(glm::ivec4);
UNIFORM_FWD_DEC_ARRAY(glm::uvec4);
UNIFORM_FWD_DEC_ARRAY(glm::vec4);
UNIFORM_FWD_DEC_ARRAY(glm::quat);
UNIFORM_FWD_DEC_ARRAY(glm::mat2);
UNIFORM_FWD_DEC_ARRAY(glm::mat2x3);
UNIFORM_FWD_DEC_ARRAY(glm::mat2x4);
UNIFORM_FWD_DEC_ARRAY(glm::mat3);
UNIFORM_FWD_DEC_ARRAY(glm::mat3x2);
UNIFORM_FWD_DEC_ARRAY(glm::mat3x4);
UNIFORM_FWD_DEC_ARRAY(glm::mat4);
UNIFORM_FWD_DEC_ARRAY(glm::mat4x2);
UNIFORM_FWD_DEC_ARRAY(glm::mat4x3);
UNIFORM_FWD_DEC_ARRAY(glm::dualquat);

#undef UNIFORM_FWD_DEC
#undef UNIFORM_FWD_DEC_ARRAY

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_SHADERPROGRAM_H
