#ifndef GRAPHICS_GL_OPENGLINCLUDES_H
#define GRAPHICS_GL_OPENGLINCLUDES_H

#pragma once

#include <GL/glew.h>
#ifdef _WIN32
#include <Windows.h>
#include <GL/wglew.h>
#undef min
#undef max
#else
#include <GL/glxew.h>
#endif

#endif // GRAPHICS_GL_OPENGLINCLUDES_H
