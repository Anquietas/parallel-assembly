#pragma once
#ifndef GRAPHICS_GL_SAMPLER_H
#define GRAPHICS_GL_SAMPLER_H

#include "Types.h"
#include "GLObject.h"
#include "TextureUnit.h"
#include "../../GLMIncludes.h"

namespace graphics {
namespace gl {

class Sampler final : public GLObject<traits::Sampler> {
public:
  enum class WrapMode : enum_t {
    ClampToBorder = 0,      // GL_CLAMP_TO_BORDER
    ClampToEdge,            // GL_CLAMP_TO_EDGE
    Repeat,                 // GL_REPEAT
    RepeatMirrored,         // GL_MIRRORED_REPEAT
  };
  enum class MinifyingFilter : enum_t {
    Nearest = 0,            // GL_NEAREST
    Linear,                 // GL_LINEAR
    NearestMipmapNearest,   // GL_NEAREST_MIPMAP_NEAREST
    NearestMipmapLinear,    // GL_NEAREST_MIPMAP_LINEAR
    LinearMipmapNearest,    // GL_LINEAR_MIPMAP_NEAREST
    LinearMipmapLinear,     // GL_LINEAR_MIPMAP_LINEAR
  };
  enum class MagnifyingFilter : enum_t {
    Nearest = 0,            // GL_NEAREST
    Linear,                 // GL_LINEAR
  };
  enum class CompareFunc : enum_t {
    Never = 0,              // GL_NEVER
    Equal,                  // GL_EQUAL
    NotEqual,               // GL_NOTEQUAL
    Less,                   // GL_LESS
    LessEqual,              // GL_LEQUAL
    Greater,                // GL_GREATER
    GreaterEqual,           // GL_GEQUAL
    Always,                 // GL_ALWAYS
  };

  GLOBJECT_CONSTRUCTOR_HELPER(Sampler, GLObject)

  // Binds the Sampler to the specified TextureUnit
  // Note: this will unbind any Sampler previously bound to this TextureUnit
  void bind(const TextureUnit& texUnit) noexcept;
  // Unbinds the Sampler currently bound to the specified TextureUnit
  static void unbind(const TextureUnit& texUnit) noexcept;

  // None of these functions require the sampler to be bound to work

  // Sets the wrap mode for the S texture coordinate (default: Repeat)
  void setWrapModeS(WrapMode mode) noexcept;
  // Sets the wrap mode for the T texture coordinate (default: Repeat)
  void setWrapModeT(WrapMode mode) noexcept;
  // Sets the wrap mode for the R texture coordinate (default: Repeat)
  void setWrapModeR(WrapMode mode) noexcept;
  // Sets the wrap mode for S,T and R to the same value (default: Repeat)
  void setAllWrapModes(WrapMode mode) noexcept;

  WrapMode wrapModeS() const;
  WrapMode wrapModeT() const;
  WrapMode wrapModeR() const;

  // Sets the minimum value for the sampler's LOD parameter (default -1000)
  void setMinLOD(float_t minLOD) noexcept;
  // Sets the maximum value for the sampler's LOD parameter (default +1000)
  void setMaxLOD(float_t maxLOD) noexcept;

  float_t minLOD() const noexcept;
  float_t maxLOD() const noexcept;

  // Sets the border colour of the sampler (default: (0,0,0,0))
  void setBorderColour(glm::vec4 colour) noexcept;

  glm::vec4 borderColour() const noexcept;

  // Sets the minifying filter for the sampler (default: NearestMipmapLinear)
  void setMinFilter(MinifyingFilter filter) noexcept;
  // Sets the magnifying filter for the sampler (default: Linear)
  void setMagFilter(MagnifyingFilter filter) noexcept;

  MinifyingFilter minFilter() const;
  MagnifyingFilter magFilter() const;

  // Enables GL_COMPARE_REF_TO_TEXTURE
  void enableCompareRefToTexture() noexcept;
  // Disables GL_COMPARE_REF_TO_TEXTURE
  void disableCompareRefToTexture() noexcept;
  // Sets the function to use for GL_COMPARE_REF_TO_TEXTURE
  void setCompareFunc(CompareFunc func) noexcept;

  bool isCompareRefToTextureEnabled() const noexcept;
  CompareFunc compareFunc() const;
};

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_SAMPLER_H
