#include "FrameBuffer.h"

#include <type_traits>

#include "OpenGLIncludes.h"

#include "MultisampleTexture2D.h"
#include "RenderBuffer.h"
#include "SystemInfo.h"
#include "Texture2D.h"

#include "../../Assert.h"

namespace graphics {
namespace gl {

enum_t getFrameBufferTargetCode(FrameBuffer::Target target) noexcept {
  static constexpr const enum_t targetCodes[] = {
    GL_DRAW_FRAMEBUFFER,
    GL_READ_FRAMEBUFFER,
  };

  auto underlying = static_cast<std::underlying_type_t<FrameBuffer::Target>>(target);
  ASSERT(0 <= underlying && underlying < std::extent<decltype(targetCodes)>::value);
  return targetCodes[underlying];
}
enum_t getScreenAttachmentCode(ScreenAttachment attachment) noexcept {
  static constexpr const enum_t attachCodes[] = {
    GL_NONE,
    GL_FRONT,
    GL_BACK,
    GL_LEFT,
    GL_RIGHT,
    GL_FRONT_LEFT,
    GL_FRONT_RIGHT,
    GL_BACK_LEFT,
    GL_BACK_RIGHT,
    GL_FRONT_AND_BACK,
  };

  auto underlying = static_cast<std::underlying_type_t<ScreenAttachment>>(attachment);
  ASSERT(0 <= underlying && underlying < std::extent<decltype(attachCodes)>::value);
  return attachCodes[underlying];
}
enum_t getColourAttachmentCode(const FrameBuffer::ColourAttachment& attachment) noexcept {
  return GL_COLOR_ATTACHMENT0 + attachment.id;
}
enum_t getDepthStencilAttachmentCode(FrameBuffer::DepthStencilAttachment attachment) noexcept {
  static constexpr const enum_t attachCodes[] = {
    GL_DEPTH_ATTACHMENT,
    GL_STENCIL_ATTACHMENT,
    GL_DEPTH_STENCIL_ATTACHMENT,
  };

  auto underlying = static_cast<std::underlying_type_t<FrameBuffer::DepthStencilAttachment>>(attachment);
  ASSERT(0 <= underlying && underlying < std::extent<decltype(attachCodes)>::value);
  return attachCodes[underlying];
}

FrameBuffer* FrameBuffer::primaryFrameBuffer = nullptr;

void FrameBuffer::bind(Target target) noexcept {
  glBindFramebuffer(getFrameBufferTargetCode(target), m_id);
}
void FrameBuffer::unbind(Target target) noexcept {
  glBindFramebuffer(getFrameBufferTargetCode(target), uint_t(0));
}

void FrameBuffer::setPrimaryFrameBuffer(FrameBuffer* newBuffer) noexcept {
  primaryFrameBuffer = newBuffer;
}
FrameBuffer* FrameBuffer::getPrimaryFrameBuffer() noexcept {
  return primaryFrameBuffer;
}

void FrameBuffer::attach(Target target, const ColourAttachment& attachment,
                         const ColourRenderBuffer& renderbuffer) noexcept
{
  glFramebufferRenderbuffer(getFrameBufferTargetCode(target),
                            getColourAttachmentCode(attachment),
                            GL_RENDERBUFFER,
                            renderbuffer.id());
}
void FrameBuffer::attach(Target target, const DepthStencilAttachment& attachment,
                         const DepthStencilRenderBuffer& renderbuffer) noexcept
{
  glFramebufferRenderbuffer(getFrameBufferTargetCode(target),
                            getDepthStencilAttachmentCode(attachment),
                            GL_RENDERBUFFER,
                            renderbuffer.id());
}

void FrameBuffer::attach(Target target, const ColourAttachment& attachment,
                         const ColourTexture2D& texture, int_t mipMapLevel) noexcept
{
  glFramebufferTexture2D(getFrameBufferTargetCode(target),
                         getColourAttachmentCode(attachment),
                         GL_TEXTURE_2D,
                         texture.id(),
                         mipMapLevel);
}
void FrameBuffer::attach(Target target, const DepthStencilAttachment& attachment,
                         const DepthStencilTexture2D& texture) noexcept
{
  glFramebufferTexture2D(getFrameBufferTargetCode(target),
                         getDepthStencilAttachmentCode(attachment),
                         GL_TEXTURE_2D,
                         texture.id(), 0);
}

void FrameBuffer::attach(Target target, const ColourAttachment& attachment,
                         const ColourMultisampleTexture2D& texture) noexcept
{
  glFramebufferTexture2D(getFrameBufferTargetCode(target),
                         getColourAttachmentCode(attachment),
                         GL_TEXTURE_2D_MULTISAMPLE,
                         texture.id(), 0);
}
void FrameBuffer::attach(Target target, const DepthStencilAttachment& attachment,
                         const DepthStencilMultisampleTexture2D& texture) noexcept
{
  glFramebufferTexture2D(getFrameBufferTargetCode(target),
                         getDepthStencilAttachmentCode(attachment),
                         GL_TEXTURE_2D_MULTISAMPLE,
                         texture.id(), 0);
}

bool FrameBuffer::isComplete(Target target) noexcept {
  return GL_FRAMEBUFFER_COMPLETE == glCheckFramebufferStatus(getFrameBufferTargetCode(target));
}

void setReadBuffer(ScreenAttachment attachment) noexcept {
  glReadBuffer(getScreenAttachmentCode(attachment));
}
void setReadBuffer(const FrameBuffer::ColourAttachment& attachment) noexcept {
  glReadBuffer(getColourAttachmentCode(attachment));
}
void setReadBuffer(FrameBuffer::DepthStencilAttachment attachment) noexcept {
  glReadBuffer(getDepthStencilAttachmentCode(attachment));
}

void setDrawBuffer(ScreenAttachment attachment) noexcept {
  glDrawBuffer(getScreenAttachmentCode(attachment));
}
void setDrawBuffer(const FrameBuffer::ColourAttachment& attachment) noexcept {
  glDrawBuffer(getColourAttachmentCode(attachment));
}
void setDrawBuffer(FrameBuffer::DepthStencilAttachment attachment) noexcept {
  glDrawBuffer(getDepthStencilAttachmentCode(attachment));
}

bool FrameBuffer::ColourAttachment::valid() const noexcept {
  return this->id < SystemInfo::maxColourAttachments();
}

} // namespace gl
} // namespace graphics
