#pragma once
#ifndef GRAPHICS_GL_TEXTURE_2D_H
#define GRAPHICS_GL_TEXTURE_2D_H

#include "Types.h"
#include "GLObject.h"
#include "ImageFormat.h"
#include "../../GLMIncludes.h"

namespace graphics {
namespace gl {

class Texture2DBase : public GLObject<traits::Texture> {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(Texture2DBase, GLObject)

  // Binds the texture to GL_TEXTURE_2D
  void bind() noexcept;
  // Unbinds the texture currently bound to GL_TEXTURE_2D
  static void unbind() noexcept;

  // The following functions will only work after calling bind()!
  // The following functions will not return meaningful results until allocate has been called

  sizei_t width(int_t mipMapLevel = 0) const noexcept;
  sizei_t height(int_t mipMapLevel = 0) const noexcept;

  bool isCompressed() const noexcept;
  sizei_t compressedSize() const noexcept;
};

class ColourTexture2D final : public Texture2DBase {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(ColourTexture2D, Texture2DBase)

  // The following functions will only work after calling bind()!

  // Allocates the texture and fills it with the given pixel data. Passing a null
  // pointer for pixelData will leave the texture in an uninitialized state
  // e.g. for framebuffer usage
  void allocate(int_t mipMapLevel, internal_format::Colour internalFormat,
                sizei_t width, sizei_t height, format::Colour pixelFormat,
                ImageType pixelType, const void* pixelData) noexcept;
  // Updates a region of the texture between (xOffset, yOffset) and
  // (xOffset + width, yOffset + height) with the given pixel data.
  // pixelData must not be null.
  void update(int_t mipMapLevel, int_t xOffset, int_t yOffset,
              sizei_t width, sizei_t height, format::Colour pixelFormat,
              ImageType pixelType, const void* pixelData) noexcept;
  // Generates mipmaps for the texture
  // - don't call this before the texture has pixel data in it!
  void generateMipMaps() noexcept;

  // The following functions will not return meaningful results until allocate has been called

  glm::ivec4 rgbaSizes() const noexcept;

  internal_format::Colour internalFormat() const;
};

class DepthStencilTexture2D final : public Texture2DBase {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(DepthStencilTexture2D, Texture2DBase)

  // The following functions will only work after calling bind()!

  // Allocates the texture and fills it with the given depth/stencil data. Passing
  // a null pointer for data will leave the texture in an uninitialized state
  // e.g. for framebuffer usage
  void allocate(internal_format::DepthStencil internalFormat,
                sizei_t width, sizei_t height, format::DepthStencil dataFormat,
                ImageType dataType, const void* data) noexcept;
  // Updates a region of the texture between (xOffset, yOffset) and
  // (xOffset + width, yOffset + height) with the given depth/stencil data
  // data must not be null.
  void update(int_t xOffset, int_t yOffset, sizei_t width, sizei_t height,
              format::DepthStencil dataFormat, ImageType dataType,
              const void* data) noexcept;

  // The following functions will not return meaningful results until allocate has been called

  sizei_t depthSize() const noexcept;
  sizei_t stencilSize() const noexcept;

  internal_format::DepthStencil internalFormat() const;
};

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_TEXTURE_2D_H
