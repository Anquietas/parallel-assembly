#pragma once
#ifndef GRAPHICS_GL_CHECK_ERROR_H
#define GRAPHICS_GL_CHECK_ERROR_H

namespace graphics {
namespace gl {

// Throws an exception if an OpenGL error has occurred.
void checkError(const char* fileName, int lineNumber);

} // namespace gl
} // namespace grpahics

#define CHECK_GL_ERROR() graphics::gl::checkError(__FILE__, __LINE__)

#ifdef ENABLE_CHECK_GL_ERROR_PEDANTIC
#define CHECK_GL_ERROR_PEDANTIC() graphics::gl::checkError(__FILE__, __LINE__)
#else
#define CHECK_GL_ERROR_PEDANTIC() (void)0
#endif

#endif // GRAPHICS_GL_CHECK_ERROR_H
