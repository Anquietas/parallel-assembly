#include "SystemInfo.h"

#include <sstream>

#include "OpenGLIncludes.h"

#include "checkError.h"

#include "../../Exception.h"
#include "../../Logger.h"

namespace graphics {
namespace gl {

float_t SystemInfo::minAliasedLineWidth_ = 0.f;
float_t SystemInfo::maxAliasedLineWidth_ = 0.f;
float_t SystemInfo::minSmoothLineWidth_ = 0.f;
float_t SystemInfo::maxSmoothLineWidth_ = 0.f;
float_t SystemInfo::smoothLineWidthGranularity_ = 0.f;

float_t SystemInfo::minPointSize_ = 0.f;
float_t SystemInfo::maxPointSize_ = 0.f;
float_t SystemInfo::pointSizeGranularity_ = 0.f;

bool SystemInfo::stereoSupported_ = false;

uint_t SystemInfo::subpixelBits_ = 0;

sizei_t SystemInfo::maxRenderbufferSize_ = 0;
sizei_t SystemInfo::maxTextureSize_ = 0;
sizei_t SystemInfo::maxTextureBufferSize_ = 0;
sizei_t SystemInfo::maxRectangleTextureSize_ = 0;
sizei_t SystemInfo::max3DTextureSize_ = 0;
sizei_t SystemInfo::maxCubeMapTextureSize_ = 0;
sizei_t SystemInfo::maxArrayTextureLayers_ = 0;

uint_t SystemInfo::maxCombinedTextureImageUnits_ = 0;

int_t SystemInfo::minProgramTexelOffset_ = 0;
int_t SystemInfo::maxProgramTexelOffset_ = 0;
float_t SystemInfo::maxTextureLODBias_ = 0.f;

uint_t SystemInfo::maxClipDistances_ = 0;

sizei_t SystemInfo::maxElementsVertices_ = 0;
sizei_t SystemInfo::maxElementsIndices_ = 0;

uint_t SystemInfo::maxSampleMaskWords_ = 0;

uint_t SystemInfo::maxColourAttachments_ = 0;
uint_t SystemInfo::maxColourTextureSamples_ = 0;
uint_t SystemInfo::maxDepthStencilTextureSamples_ = 0;
uint_t SystemInfo::maxIntegerSamples_ = 0;

int64_t SystemInfo::maxServerWaitTimeout_ = 0;

sizei_t SystemInfo::maxViewportWidth_ = 0;
sizei_t SystemInfo::maxViewportHeight_ = 0;

uint_t SystemInfo::maxVaryingComponents_ = 0;
uint_t SystemInfo::maxVaryingFloats_ = 0;
uint_t SystemInfo::maxUniformBufferBindings_ = 0;
uint_t SystemInfo::maxUniformBlockSize_ = 0;
uint_t SystemInfo::minUniformBufferOffsetAlignment_ = 0;
uint_t SystemInfo::maxCombinedUniformBlocks_ = 0;

uint_t SystemInfo::maxVertexAttribs_ = 0;
uint_t SystemInfo::maxVertexTextureImageUnits_ = 0;
uint_t SystemInfo::maxVertexUniformComponents_ = 0;
uint_t SystemInfo::maxVertexUniformBlocks_ = 0;
uint_t SystemInfo::maxCombinedVertexUniformComponents_ = 0;
uint_t SystemInfo::maxVertexOutputComponents_ = 0;

uint_t SystemInfo::maxGeometryInputComponents_ = 0;
uint_t SystemInfo::maxGeometryTextureImageUnits_ = 0;
uint_t SystemInfo::maxGeometryUniformComponents_ = 0;
uint_t SystemInfo::maxGeometryUniformBlocks_ = 0;
uint_t SystemInfo::maxCombinedGeometryUniformComponents_ = 0;
uint_t SystemInfo::maxGeometryOutputComponents_ = 0;

uint_t SystemInfo::maxFragmentInputComponents_ = 0;
uint_t SystemInfo::maxTextureImageUnits_ = 0;
uint_t SystemInfo::maxFragmentUniformComponents_ = 0;
uint_t SystemInfo::maxFragmentUniformBlocks_ = 0;
uint_t SystemInfo::maxCombinedFragmentUniformComponents_ = 0;
uint_t SystemInfo::maxDrawBuffers_ = 0;
uint_t SystemInfo::maxDualSourceDrawBuffers_ = 0;

void SystemInfo::init() {
  CHECK_GL_ERROR_PEDANTIC();

  float_t floatValue = 0.f;
  float_t floatValues[2] = {0.f, 0.f};
  int_t intValue = 0;
  int_t intValues[2] = {0, 0};
  GLboolean boolValue = GL_FALSE;

  CHECK_GL_ERROR_PEDANTIC();

  glGetFloatv(GL_ALIASED_LINE_WIDTH_RANGE, floatValues);
  minAliasedLineWidth_ = floatValues[0];
  maxAliasedLineWidth_ = floatValues[1];
  glGetFloatv(GL_SMOOTH_LINE_WIDTH_RANGE, floatValues);
  minSmoothLineWidth_ = floatValues[0];
  maxSmoothLineWidth_ = floatValues[1];
  glGetFloatv(GL_SMOOTH_LINE_WIDTH_GRANULARITY, &smoothLineWidthGranularity_);

  CHECK_GL_ERROR_PEDANTIC(); // throws the exception

  glGetFloatv(GL_POINT_SIZE_RANGE, floatValues);
  minPointSize_ = floatValues[0];
  maxPointSize_ = floatValues[1];
  glGetFloatv(GL_POINT_SIZE_GRANULARITY, &pointSizeGranularity_);

  CHECK_GL_ERROR_PEDANTIC();

  glGetBooleanv(GL_STEREO, &boolValue);
  stereoSupported_ = boolValue == GL_TRUE;

  CHECK_GL_ERROR_PEDANTIC();

  glGetIntegerv(GL_SUBPIXEL_BITS, &intValue);
  subpixelBits_ = static_cast<uint_t>(intValue);

  CHECK_GL_ERROR_PEDANTIC();

  glGetIntegerv(GL_MAX_RENDERBUFFER_SIZE, &intValue);
  maxRenderbufferSize_ = intValue;
  glGetIntegerv(GL_MAX_TEXTURE_SIZE, &intValue);
  maxTextureSize_ = intValue;
  glGetIntegerv(GL_MAX_TEXTURE_BUFFER_SIZE, &intValue);
  maxTextureBufferSize_ = intValue;
  glGetIntegerv(GL_MAX_RECTANGLE_TEXTURE_SIZE, &intValue);
  maxRectangleTextureSize_ = intValue;
  glGetIntegerv(GL_MAX_3D_TEXTURE_SIZE, &intValue);
  max3DTextureSize_ = intValue;
  glGetIntegerv(GL_MAX_CUBE_MAP_TEXTURE_SIZE, &intValue);
  maxCubeMapTextureSize_ = intValue;
  glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &intValue);
  maxArrayTextureLayers_ = intValue;

  CHECK_GL_ERROR_PEDANTIC();

  glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &intValue);
  maxCombinedTextureImageUnits_ = static_cast<uint_t>(intValue);

  CHECK_GL_ERROR_PEDANTIC();

  glGetIntegerv(GL_MIN_PROGRAM_TEXEL_OFFSET, &intValue);
  minProgramTexelOffset_ = intValue;
  glGetIntegerv(GL_MAX_PROGRAM_TEXEL_OFFSET, &intValue);
  maxProgramTexelOffset_ = intValue;
  glGetFloatv(GL_MAX_TEXTURE_LOD_BIAS, &floatValue);
  maxTextureLODBias_ = floatValue;

  CHECK_GL_ERROR_PEDANTIC();

  glGetIntegerv(GL_MAX_CLIP_DISTANCES, &intValue);
  maxClipDistances_ = static_cast<uint_t>(intValue);

  CHECK_GL_ERROR_PEDANTIC();

  glGetIntegerv(GL_MAX_ELEMENTS_VERTICES, &intValue);
  maxElementsVertices_ = intValue;
  glGetIntegerv(GL_MAX_ELEMENTS_INDICES, &intValue);
  maxElementsIndices_ = intValue;

  CHECK_GL_ERROR_PEDANTIC();

  glGetIntegerv(GL_MAX_SAMPLE_MASK_WORDS, &intValue);
  maxSampleMaskWords_ = static_cast<uint_t>(intValue);

  CHECK_GL_ERROR_PEDANTIC();

  glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &intValue);
  maxColourAttachments_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_COLOR_TEXTURE_SAMPLES, &intValue);
  maxColourTextureSamples_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_DEPTH_TEXTURE_SAMPLES, &intValue);
  maxDepthStencilTextureSamples_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_INTEGER_SAMPLES, &intValue);
  maxIntegerSamples_ = static_cast<uint_t>(intValue);

  CHECK_GL_ERROR_PEDANTIC();

  glGetInteger64v(GL_MAX_SERVER_WAIT_TIMEOUT, &maxServerWaitTimeout_);

  CHECK_GL_ERROR_PEDANTIC();

  glGetIntegerv(GL_MAX_VIEWPORT_DIMS, intValues);
  maxViewportWidth_ = intValues[0];
  maxViewportHeight_ = intValues[1];

  CHECK_GL_ERROR_PEDANTIC();

  glGetIntegerv(GL_MAX_VARYING_COMPONENTS, &intValue);
  maxVaryingComponents_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_VARYING_FLOATS, &intValue);
  maxVaryingFloats_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_UNIFORM_BUFFER_BINDINGS, &intValue);
  maxUniformBufferBindings_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &intValue);
  maxUniformBlockSize_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &intValue);
  minUniformBufferOffsetAlignment_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_COMBINED_UNIFORM_BLOCKS, &intValue);
  maxCombinedUniformBlocks_ = static_cast<uint_t>(intValue);

  CHECK_GL_ERROR_PEDANTIC();

  glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &intValue);
  maxVertexAttribs_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, &intValue);
  maxVertexTextureImageUnits_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_VERTEX_UNIFORM_COMPONENTS, &intValue);
  maxVertexUniformComponents_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_VERTEX_UNIFORM_BLOCKS, &intValue);
  maxVertexUniformBlocks_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS, &intValue);
  maxCombinedVertexUniformComponents_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_VERTEX_OUTPUT_COMPONENTS, &intValue);
  maxVertexOutputComponents_ = static_cast<uint_t>(intValue);

  CHECK_GL_ERROR_PEDANTIC();

  glGetIntegerv(GL_MAX_GEOMETRY_INPUT_COMPONENTS, &intValue);
  maxGeometryInputComponents_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS, &intValue);
  maxGeometryTextureImageUnits_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_GEOMETRY_UNIFORM_COMPONENTS, &intValue);
  maxGeometryUniformComponents_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_GEOMETRY_UNIFORM_BLOCKS, &intValue);
  maxGeometryUniformBlocks_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS, &intValue);
  maxCombinedGeometryUniformComponents_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_GEOMETRY_OUTPUT_COMPONENTS, &intValue);
  maxGeometryOutputComponents_ = static_cast<uint_t>(intValue);

  CHECK_GL_ERROR_PEDANTIC();

  glGetIntegerv(GL_MAX_FRAGMENT_INPUT_COMPONENTS, &intValue);
  maxFragmentInputComponents_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &intValue);
  maxTextureImageUnits_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_COMPONENTS, &intValue);
  maxFragmentUniformComponents_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_BLOCKS, &intValue);
  maxFragmentUniformBlocks_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS, &intValue);
  maxCombinedFragmentUniformComponents_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_DRAW_BUFFERS, &intValue);
  maxDrawBuffers_ = static_cast<uint_t>(intValue);
  glGetIntegerv(GL_MAX_DUAL_SOURCE_DRAW_BUFFERS, &intValue);
  maxDualSourceDrawBuffers_ = static_cast<uint_t>(intValue);

  CHECK_GL_ERROR();

  dumpToLog();
  checkConfig();
}

void SystemInfo::dumpToLog() {
  std::ostringstream ss;
  ss << "OpenGL Constants:\n";
  ss << "GL_ALIASED_LINE_WIDTH_RANGE = " << minAliasedLineWidth_ << " .. " << maxAliasedLineWidth_ << '\n';
  ss << "GL_SMOOTH_LINE_WIDTH_RANGE = " << minSmoothLineWidth_ << " .. " << maxSmoothLineWidth_ << '\n';
  ss << "GL_SMOOTH_LINE_WIDTH_GRANULARITY = " << smoothLineWidthGranularity_ << '\n';
  ss << "GL_POINT_SIZE_RANGE = " << minPointSize_ << " .. " << maxPointSize_ << '\n';
  ss << "GL_POINT_SIZE_GRANULARITY = " << pointSizeGranularity_ << '\n';
  ss << "GL_STEREO = " << std::boolalpha << stereoSupported_ << '\n';
  ss << "GL_SUBPIXEL_BITS = " << subpixelBits_ << '\n';

  ss << "GL_MAX_RENDERBUFFER_SIZE = " << maxRenderbufferSize_ << '\n';
  ss << "GL_MAX_TEXTURE_SIZE = " << maxTextureSize_ << '\n';
  ss << "GL_MAX_TEXTURE_BUFFER_SIZE = " << maxTextureBufferSize_ << '\n';
  ss << "GL_MAX_RECTANGLE_TEXTURE_SIZE = " << maxRectangleTextureSize_ << '\n';
  ss << "GL_MAX_3D_TEXTURE_SIZE = " << max3DTextureSize_ << '\n';
  ss << "GL_MAX_CUBE_MAP_TEXTURE_SIZE = " << maxCubeMapTextureSize_ << '\n';
  ss << "GL_MAX_ARRAY_TEXTURE_LAYERS = " << maxArrayTextureLayers_ << '\n';

  ss << "GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS = " << maxCombinedFragmentUniformComponents_ << '\n';
  ss << "GL_MIN_PROGRAM_TEXEL_OFFSET = " << minProgramTexelOffset_ << '\n';
  ss << "GL_MAX_PROGRAM_TEXEL_OFFSET = " << maxProgramTexelOffset_ << '\n';
  ss << "GL_MAX_TEXTURE_LOD_BIAS = " << maxTextureLODBias_ << '\n';
  ss << "GL_MAX_CLIP_DISTANCES = " << maxClipDistances_ << '\n';
  ss << "GL_MAX_ELEMENTS_VERTICES = " << maxElementsVertices_ << '\n';
  ss << "GL_MAX_ELEMENTS_INDICES = " << maxElementsIndices_ << '\n';
  ss << "GL_MAX_SAMPLE_MASK_WORDS = " << maxSampleMaskWords_ << '\n';

  ss << "GL_MAX_COLOR_ATTACHMENTS = " << maxColourAttachments_ << '\n';
  ss << "GL_MAX_COLOR_TEXTURE_SAMPLES = " << maxColourTextureSamples_ << '\n';
  ss << "GL_MAX_DEPTH_TEXTURE_SAMPLES = " << maxDepthStencilTextureSamples_ << '\n';
  ss << "GL_MAX_INTEGER_SAMPLES = " << maxIntegerSamples_ << '\n';
  ss << "GL_MAX_SERVER_WAIT_TIMEOUT = " << maxServerWaitTimeout_ << '\n';
  ss << "GL_MAX_VIEWPORT_DIMS = " << maxViewportWidth_ << " x " << maxViewportHeight_ << '\n';

  ss << "GL_MAX_VARYING_COMPONENTS = " << maxVaryingComponents_ << '\n';
  ss << "GL_MAX_VARYING_FLOATS = " << maxVaryingFloats_ << '\n';
  ss << "GL_MAX_UNIFORM_BUFFER_BINDINGS = " << maxUniformBufferBindings_ << '\n';
  ss << "GL_MAX_UNIFORM_BLOCK_SIZE = " << maxUniformBlockSize_ << '\n';
  ss << "GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT = " << minUniformBufferOffsetAlignment_ << '\n';
  ss << "GL_MAX_COMBINED_UNIFORM_BLOCKS = " << maxCombinedFragmentUniformComponents_ << '\n';

  ss << "GL_MAX_VERTEX_ATTRIBS = " << maxVertexAttribs_ << '\n';
  ss << "GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS = " << maxVertexTextureImageUnits_ << '\n';
  ss << "GL_MAX_VERTEX_UNIFORM_COMPONENTS = " << maxVertexUniformComponents_ << '\n';
  ss << "GL_MAX_VERTEX_UNIFORM_BLOCKS = " << maxVertexUniformBlocks_ << '\n';
  ss << "GL_MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS = " << maxCombinedVertexUniformComponents_ << '\n';
  ss << "GL_MAX_VERTEX_OUTPUT_COMPONENTS = " << maxVertexOutputComponents_ << '\n';

  ss << "GL_MAX_GEOMETRY_INPUT_COMPONENTS = " << maxGeometryInputComponents_ << '\n';
  ss << "GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS = " << maxGeometryTextureImageUnits_ << '\n';
  ss << "GL_MAX_GEOMETRY_UNIFORM_COMPONENTS = " << maxGeometryUniformComponents_ << '\n';
  ss << "GL_MAX_GEOMETRY_UNIFORM_BLOCKS = " << maxGeometryUniformBlocks_ << '\n';
  ss << "GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS = " << maxCombinedGeometryUniformComponents_ << '\n';
  ss << "GL_MAX_GEOMETRY_OUTPUT_COMPONENTS = " << maxGeometryOutputComponents_ << '\n';

  ss << "GL_MAX_FRAGMENT_INPUT_COMPONENTS = " << maxFragmentInputComponents_ << '\n';
  ss << "GL_MAX_TEXTURE_IMAGE_UNITS = " << maxTextureImageUnits_ << '\n';
  ss << "GL_MAX_FRAGMENT_UNIFORM_COMPONENTS = " << maxFragmentInputComponents_ << '\n';
  ss << "GL_MAX_FRAGMENT_UNIFORM_BLOCKS = " << maxFragmentUniformBlocks_ << '\n';
  ss << "GL_MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS = " << maxCombinedFragmentUniformComponents_ << '\n';
  ss << "GL_MAX_DRAW_BUFFERS = " << maxDrawBuffers_ << '\n';
  ss << "GL_MAX_DUAL_SOURCE_DRAW_BUFFERS = " << maxDualSourceDrawBuffers_ << '\n';

  logging::Logger logger;
  logging::setModuleName(logger, "GLSystemInfo");
  LOG_SEVERITY(logger, Debug) << ss.str();
}

void SystemInfo::checkConfig() {
#define CHECK_VALUE_MIN(actual, expected, macroName) \
  if (SystemInfo::actual > expected) { \
    std::ostringstream ss; \
    ss << "Driver does not meet OpenGL 3.3 minimum spec for " << #macroName << ";\n"; \
    ss << "expected at most " << expected << ", got " << actual << '\n'; \
    ss << "This is most likely to be a bug with the game rather than the driver."; \
    throw MAKE_EXCEPTION(ss.str()); \
  }
#define CHECK_VALUE_MAX(actual, expected, macroName) \
  if (SystemInfo::actual < expected) { \
    std::ostringstream ss; \
    ss << "Driver does not meet OpenGL 3.3 minimum spec for " << #macroName << ";\n"; \
    ss << "expected at least " << expected << ", got " << actual << '\n'; \
    ss << "This is most likely to be a bug with the game rather than the driver."; \
    throw MAKE_EXCEPTION(ss.str()); \
  }

  CHECK_VALUE_MIN(minPointSize_, 1, GL_POINT_SIZE_RANGE);
  CHECK_VALUE_MAX(maxPointSize_, 1, GL_POINT_SIZE_RANGE);

  CHECK_VALUE_MAX(subpixelBits_, 8, GL_SUBPIXEL_BITS);
  
  CHECK_VALUE_MAX(maxTextureSize_, 1024, GL_MAX_TEXTURE_SIZE);
  CHECK_VALUE_MAX(maxTextureBufferSize_, 65536, GL_MAX_TEXTURE_BUFFER_SIZE);
  CHECK_VALUE_MAX(maxRectangleTextureSize_, 1024, GL_MAX_RECTANGLE_TEXTURE_SIZE);
  CHECK_VALUE_MAX(max3DTextureSize_, 64, GL_MAX_3D_TEXTURE_SIZE);
  CHECK_VALUE_MAX(maxCubeMapTextureSize_, 1024, GL_MAX_CUBE_MAP_TEXTURE_SIZE);
  CHECK_VALUE_MAX(maxArrayTextureLayers_, 256, GL_MAX_ARRAY_TEXTURE_LAYERS);

  CHECK_VALUE_MAX(maxCombinedTextureImageUnits_, 48, GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS);
  CHECK_VALUE_MIN(minProgramTexelOffset_, -8, GL_MIN_PROGRAM_TEXEL_OFFSET);
  CHECK_VALUE_MAX(maxProgramTexelOffset_, 7, GL_MAX_PROGRAM_TEXEL_OFFSET);
  CHECK_VALUE_MAX(maxTextureLODBias_, 2.f, GL_MAX_TEXTURE_LOD_BIAS);

  CHECK_VALUE_MAX(maxClipDistances_, 8, GL_MAX_CLIP_DISTANCES);
  CHECK_VALUE_MAX(maxColourAttachments_, 8, GL_MAX_COLOR_ATTACHMENTS);

  CHECK_VALUE_MAX(maxVaryingComponents_, 60, GL_MAX_VARYING_COMPONENTS);
  CHECK_VALUE_MAX(maxVaryingFloats_, 32, GL_MAX_VARYING_FLOATS);
  CHECK_VALUE_MAX(maxUniformBufferBindings_, 36, GL_MAX_UNIFORM_BUFFER_BINDINGS);
  CHECK_VALUE_MAX(maxUniformBlockSize_, 16384, GL_MAX_UNIFORM_BLOCK_SIZE);
  CHECK_VALUE_MAX(maxCombinedUniformBlocks_, 36, GL_MAX_COMBINED_UNIFORM_BLOCKS);

  CHECK_VALUE_MAX(maxVertexAttribs_, 16, GL_MAX_VERTEX_ATTRIBS);
  CHECK_VALUE_MAX(maxVertexTextureImageUnits_, 16, GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS);
  CHECK_VALUE_MAX(maxVertexUniformComponents_, 1024, GL_MAX_VERTEX_UNIFORM_COMPONENTS);
  CHECK_VALUE_MAX(maxVertexUniformBlocks_, 12, GL_MAX_VERTEX_UNIFORM_BLOCKS);
  CHECK_VALUE_MAX(maxCombinedVertexUniformComponents_, 1, GL_MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS);
  CHECK_VALUE_MAX(maxVertexOutputComponents_, 64, GL_MAX_VERTEX_OUTPUT_COMPONENTS);

  CHECK_VALUE_MAX(maxGeometryInputComponents_, 64, GL_MAX_GEOMETRY_INPUT_COMPONENTS);
  CHECK_VALUE_MAX(maxGeometryTextureImageUnits_, 16, GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS);
  CHECK_VALUE_MAX(maxGeometryUniformComponents_, 1024, GL_MAX_GEOMETRY_UNIFORM_COMPONENTS);
  CHECK_VALUE_MAX(maxGeometryUniformBlocks_, 12, GL_MAX_GEOMETRY_UNIFORM_BLOCKS);
  CHECK_VALUE_MAX(maxCombinedGeometryUniformComponents_, 1, GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS);
  CHECK_VALUE_MAX(maxGeometryOutputComponents_, 128, GL_MAX_GEOMETRY_OUTPUT_COMPONENTS);
  
  CHECK_VALUE_MAX(maxFragmentInputComponents_, 128, GL_MAX_FRAGMENT_INPUT_COMPONENTS);
  CHECK_VALUE_MAX(maxTextureImageUnits_, 16, GL_MAX_TEXTURE_IMAGE_UNITS);
  CHECK_VALUE_MAX(maxFragmentUniformComponents_, 1024, GL_MAX_FRAGMENT_UNIFORM_COMPONENTS);
  CHECK_VALUE_MAX(maxFragmentUniformBlocks_, 12, GL_MAX_FRAGMENT_UNIFORM_BLOCKS);
  CHECK_VALUE_MAX(maxCombinedFragmentUniformComponents_, 1, GL_MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS);
  CHECK_VALUE_MAX(maxDrawBuffers_, 8, GL_MAX_DRAW_BUFFERS);
  CHECK_VALUE_MAX(maxDualSourceDrawBuffers_, 1, GL_MAX_DUAL_SOURCE_DRAW_BUFFERS);

#undef CHECK_VALUE_MIN
#undef CHECK_VALUE_MAX
}

} // namespace gl
} // namespace graphics
