#pragma once
#ifndef GRAPHICS_GL_ATTRIBUTE_ID_H
#define GRAPHICS_GL_ATTRIBUTE_ID_H

#include "Types.h"

namespace graphics {
namespace gl {

struct AttributeID final {
  uint_t id;

  explicit AttributeID(uint_t id_ = static_cast<uint_t>(-1)) noexcept : id(id_) {}
  AttributeID(const AttributeID& o) noexcept = default;
  AttributeID(AttributeID&& o) noexcept = default;
  ~AttributeID() = default;

  AttributeID& operator=(const AttributeID& o) noexcept = default;
  AttributeID& operator=(AttributeID&& o) noexcept = default;

  bool valid() const noexcept { return id != static_cast<uint_t>(-1); }
};

inline bool operator==(const AttributeID& x, const AttributeID& y) noexcept {
  return x.id == y.id;
}
inline bool operator!=(const AttributeID& x, const AttributeID& y) noexcept {
  return x.id != y.id;
}

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_ATTRIBUTE_ID_H
