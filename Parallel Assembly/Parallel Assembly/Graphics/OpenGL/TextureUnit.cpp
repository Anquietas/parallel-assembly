#include "TextureUnit.h"

#include "OpenGLIncludes.h"

#include "SystemInfo.h"

namespace graphics {
namespace gl {

bool TextureUnit::valid() const noexcept {
  return this->id < SystemInfo::maxCombinedTextureImageUnits();
}

void TextureUnit::setActive() noexcept {
  glActiveTexture(GL_TEXTURE0 + this->id);
}

} // namespace gl
} // namespace graphics
