#include "Texture2D.h"

#include "OpenGLIncludes.h"

#include "../../Assert.h"

namespace graphics {
namespace gl {

void Texture2DBase::bind() noexcept {
  glBindTexture(GL_TEXTURE_2D, m_id);
}
void Texture2DBase::unbind() noexcept {
  glBindTexture(GL_TEXTURE_2D, uint_t(0));
}

sizei_t Texture2DBase::width(int_t mipMapLevel) const noexcept {
  int_t width = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, mipMapLevel, GL_TEXTURE_WIDTH, &width);
  return static_cast<sizei_t>(width);
}
sizei_t Texture2DBase::height(int_t mipMapLevel) const noexcept {
  int_t height = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, mipMapLevel, GL_TEXTURE_HEIGHT, &height);
  return static_cast<sizei_t>(height);
}

bool Texture2DBase::isCompressed() const noexcept {
  int_t value = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_COMPRESSED, &value);
  return value == GL_TRUE;
}
sizei_t Texture2DBase::compressedSize() const noexcept {
  int_t size = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_COMPRESSED_IMAGE_SIZE, &size);
  return static_cast<sizei_t>(size);
}


void ColourTexture2D::allocate(int_t mipMapLevel, internal_format::Colour internalFormat,
                               sizei_t width, sizei_t height, format::Colour pixelFormat,
                               ImageType pixelType, const void* pixelData) noexcept
{
  auto internalFormatCode = getFormatCode(internalFormat);
  auto formatCode = getFormatCode(pixelFormat);
  auto typeCode = getTypeCode(pixelType);
  glTexImage2D(GL_TEXTURE_2D, mipMapLevel, internalFormatCode,
               width, height, 0, formatCode, typeCode, pixelData);
}
void ColourTexture2D::update(int_t mipMapLevel, int_t xOffset, int_t yOffset,
                             sizei_t width, sizei_t height, format::Colour pixelFormat,
                             ImageType pixelType, const void* pixelData) noexcept
{
  ASSERT(pixelData);
  auto formatCode = getFormatCode(pixelFormat);
  auto typeCode = getTypeCode(pixelType);
  glTexSubImage2D(GL_TEXTURE_2D, mipMapLevel, xOffset, yOffset,
                  width, height, formatCode, typeCode, pixelData);
}
void ColourTexture2D::generateMipMaps() noexcept {
  glGenerateMipmap(GL_TEXTURE_2D);
}

glm::ivec4 ColourTexture2D::rgbaSizes() const noexcept {
  static_assert(sizeof(glm::ivec4) == 4 * sizeof(int_t),
                "Size discrepancy between glm::ivec4 and int_t");
  glm::ivec4 sizes;
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_RED_SIZE, &sizes.r);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_GREEN_SIZE, &sizes.g);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_BLUE_SIZE, &sizes.b);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_ALPHA_SIZE, &sizes.a);
  return sizes;
}

internal_format::Colour ColourTexture2D::internalFormat() const {
  int_t formatCode = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_INTERNAL_FORMAT, &formatCode);
  return getColourFormat(static_cast<enum_t>(formatCode));
}


void DepthStencilTexture2D::allocate(internal_format::DepthStencil internalFormat,
                                     sizei_t width, sizei_t height, format::DepthStencil dataFormat,
                                     ImageType dataType, const void* data) noexcept
{
  auto internalFormatCode = getFormatCode(internalFormat);
  auto formatCode = getFormatCode(dataFormat);
  auto typeCode = getTypeCode(dataType);
  glTexImage2D(GL_TEXTURE_2D, 0, internalFormatCode,
               width, height, 0, formatCode, typeCode, data);
}
void DepthStencilTexture2D::update(int_t xOffset, int_t yOffset, sizei_t width, sizei_t height,
                                   format::DepthStencil dataFormat, ImageType dataType,
                                   const void* data) noexcept
{
  ASSERT(data);
  auto formatCode = getFormatCode(dataFormat);
  auto typeCode = getTypeCode(dataType);
  glTexSubImage2D(GL_TEXTURE_2D, 0, xOffset, yOffset,
                  width, height, formatCode, typeCode, data);
}

sizei_t DepthStencilTexture2D::depthSize() const noexcept {
  int_t size = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_DEPTH_SIZE, &size);
  return static_cast<sizei_t>(size);
}
sizei_t DepthStencilTexture2D::stencilSize() const noexcept {
  int_t size = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_STENCIL_SIZE, &size);
  return static_cast<sizei_t>(size);
}

internal_format::DepthStencil DepthStencilTexture2D::internalFormat() const {
  int_t formatCode = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_INTERNAL_FORMAT, &formatCode);
  return getDepthStencilFormat(static_cast<enum_t>(formatCode));
}

} // namespace gl
} // namespace graphics
