#ifndef GRAPHICS_GL_GLTYPEINFO_H
#define GRAPHICS_GL_GLTYPEINFO_H

#pragma once

#include "Types.h"
#include "../../GLMIncludes.h"

#include <type_traits>

namespace graphics {
namespace gl {
namespace util {

template <enum_t typeIdentifier,
          int_t numComponents,
          bool intType,
          // Only applies for matrix types
          int numParts = 1,
          uint_t perPartOffset = 0>
struct GLTypeInfoBase {
  static constexpr enum_t typeID = typeIdentifier;
  static constexpr int_t components = numComponents;
  static constexpr bool isIntType = intType;
  static constexpr int parts = numParts;
  static constexpr uint_t partOffset = perPartOffset;
};

template <typename T, typename Enable = void>
struct GLTypeInfo;

template <typename T>
struct isMatrixType : std::false_type {};

// mat2
template <> struct isMatrixType<glm::mat2> : std::true_type {};
template <> struct isMatrixType<glm::dmat2> : std::true_type {};
// mat2x3
template <> struct isMatrixType<glm::mat2x3> : std::true_type {};
template <> struct isMatrixType<glm::dmat2x3> : std::true_type {};
// mat2x4
template <> struct isMatrixType<glm::mat2x4> : std::true_type {};
template <> struct isMatrixType<glm::dmat2x4> : std::true_type {};
// mat3
template <> struct isMatrixType<glm::mat3> : std::true_type {};
template <> struct isMatrixType<glm::dmat3> : std::true_type {};
// mat3x2
template <> struct isMatrixType<glm::mat3x2> : std::true_type {};
template <> struct isMatrixType<glm::dmat3x2> : std::true_type {};
// mat3x4
template <> struct isMatrixType<glm::mat3x4> : std::true_type {};
template <> struct isMatrixType<glm::dmat3x4> : std::true_type {};
// mat4
template <> struct isMatrixType<glm::mat4> : std::true_type {};
template <> struct isMatrixType<glm::dmat4> : std::true_type {};
// mat4x2
template <> struct isMatrixType<glm::mat4x2> : std::true_type {};
template <> struct isMatrixType<glm::dmat4x2> : std::true_type {};
// mat4x3
template <> struct isMatrixType<glm::mat4x3> : std::true_type {};
template <> struct isMatrixType<glm::dmat4x3> : std::true_type {};

// dualquat
template <> struct isMatrixType<glm::dualquat> : std::true_type {};
template <> struct isMatrixType<glm::ddualquat> : std::true_type {};

} // namespace util
} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_GLTYPEINFO_H
