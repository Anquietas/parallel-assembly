#include "Texture2DArray.h"

#include "OpenGLIncludes.h"

#include "../../Assert.h"

namespace graphics {
namespace gl {

void Texture2DArrayBase::bind() noexcept {
  glBindTexture(GL_TEXTURE_2D_ARRAY, m_id);
}
void Texture2DArrayBase::unbind() noexcept {
  glBindTexture(GL_TEXTURE_2D_ARRAY, uint_t(0));
}

sizei_t Texture2DArrayBase::width(int_t mipMapLevel) const noexcept {
  int_t width = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_ARRAY, mipMapLevel, GL_TEXTURE_WIDTH, &width);
  return static_cast<sizei_t>(width);
}
sizei_t Texture2DArrayBase::height(int_t mipMapLevel) const noexcept {
  int_t height = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_ARRAY, mipMapLevel, GL_TEXTURE_HEIGHT, &height);
  return static_cast<sizei_t>(height);
}
sizei_t Texture2DArrayBase::length() const noexcept {
  int_t length = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_ARRAY, 0, GL_TEXTURE_DEPTH, &length);
  return static_cast<sizei_t>(length);
}

bool Texture2DArrayBase::isCompressed() const noexcept {
  int_t value = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_ARRAY, 0, GL_TEXTURE_COMPRESSED, &value);
  return value == GL_TRUE;
}
sizei_t Texture2DArrayBase::compressedSize() const noexcept {
  int_t size = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_ARRAY, 0, GL_TEXTURE_COMPRESSED_IMAGE_SIZE, &size);
  return static_cast<sizei_t>(size);
}


void ColourTexture2DArray::allocate(
  int_t mipMapLevel, internal_format::Colour internalFormat,
  sizei_t width, sizei_t height, sizei_t length,
  format::Colour pixelFormat, ImageType pixelType, const void* pixelData) noexcept
{
  auto internalFormatCode = getFormatCode(internalFormat);
  auto formatCode = getFormatCode(pixelFormat);
  auto typeCode = getTypeCode(pixelType);
  glTexImage3D(GL_TEXTURE_2D_ARRAY, mipMapLevel, internalFormatCode,
               width, height, length, 0, formatCode, typeCode, pixelData);
}
void ColourTexture2DArray::update(
  int_t mipMapLevel, int_t xOffset, int_t yOffset, int_t index,
  sizei_t width, sizei_t height, sizei_t length,
  format::Colour pixelFormat, ImageType pixelType, const void* pixelData) noexcept
{
  ASSERT(pixelData);
  auto formatCode = getFormatCode(pixelFormat);
  auto typeCode = getTypeCode(pixelType);
  glTexSubImage3D(GL_TEXTURE_2D_ARRAY, mipMapLevel, xOffset, yOffset, index,
                  width, height, length, formatCode, typeCode, pixelData);
}
void ColourTexture2DArray::generateMipMaps() noexcept {
  glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
}

glm::ivec4 ColourTexture2DArray::rgbaSizes() const noexcept {
  static_assert(sizeof(glm::ivec4) == 4 * sizeof(int_t),
                "Size discrepancy between glm::ivec4 and int_t");
  glm::ivec4 sizes;
  glGetTexLevelParameteriv(GL_TEXTURE_2D_ARRAY, 0, GL_TEXTURE_RED_SIZE, &sizes.r);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_ARRAY, 0, GL_TEXTURE_GREEN_SIZE, &sizes.g);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_ARRAY, 0, GL_TEXTURE_BLUE_SIZE, &sizes.b);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_ARRAY, 0, GL_TEXTURE_ALPHA_SIZE, &sizes.a);
  return sizes;
}

internal_format::Colour ColourTexture2DArray::internalFormat() const {
  int_t formatCode = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_ARRAY, 0, GL_TEXTURE_INTERNAL_FORMAT, &formatCode);
  return getColourFormat(static_cast<enum_t>(formatCode));
}


void DepthStencilTexture2DArray::allocate(
  internal_format::DepthStencil internalFormat,
  sizei_t width, sizei_t height, sizei_t length,
  format::DepthStencil dataFormat, ImageType dataType, const void* data) noexcept
{
  auto internalFormatCode = getFormatCode(internalFormat);
  auto formatCode = getFormatCode(dataFormat);
  auto typeCode = getTypeCode(dataType);
  glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, internalFormatCode,
               width, height, length, 0, formatCode, typeCode, data);
}
void DepthStencilTexture2DArray::update(
  int_t xOffset, int_t yOffset, int_t index,
  sizei_t width, sizei_t height, sizei_t length,
  format::Colour dataFormat, ImageType dataType, const void* data) noexcept
{
  ASSERT(data);
  auto formatCode = getFormatCode(dataFormat);
  auto typeCode = getTypeCode(dataType);
  glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, xOffset, yOffset, index,
                  width, height, length, formatCode, typeCode, data);
}

sizei_t DepthStencilTexture2DArray::depthSize() const noexcept {
  int_t size = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_ARRAY, 0, GL_TEXTURE_DEPTH_SIZE, &size);
  return static_cast<sizei_t>(size);
}
sizei_t DepthStencilTexture2DArray::stencilSize() const noexcept {
  int_t size = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_ARRAY, 0, GL_TEXTURE_STENCIL_SIZE, &size);
  return static_cast<sizei_t>(size);
}

internal_format::DepthStencil DepthStencilTexture2DArray::internalFormat() const {
  int_t formatCode = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_ARRAY, 0, GL_TEXTURE_INTERNAL_FORMAT, &formatCode);
  return getDepthStencilFormat(static_cast<enum_t>(formatCode));
}

} // namespace gl
} // namesapce graphics
