#include "MultisampleTexture2D.h"

#include "OpenGLIncludes.h"

namespace graphics {
namespace gl {

void MultisampleTexture2DBase::bind() noexcept {
  glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, m_id);
}
void MultisampleTexture2DBase::unbind() noexcept {
  glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, uint_t(0));
}

sizei_t MultisampleTexture2DBase::width() const noexcept {
  int_t width = 0;
  glGetTexLevelParameteriv(GL_TEXTURE_2D_MULTISAMPLE, 0, GL_TEXTURE_WIDTH, &width);
  return static_cast<sizei_t>(width);
}
sizei_t MultisampleTexture2DBase::height() const noexcept {
  int_t height = 0;
  glGetTexLevelParameteriv(GL_TEXTURE_2D_MULTISAMPLE, 0, GL_TEXTURE_HEIGHT, &height);
  return static_cast<sizei_t>(height);
}
sizei_t MultisampleTexture2DBase::samples() const noexcept {
  int_t samples = 0;
  glGetTexLevelParameteriv(GL_TEXTURE_2D_MULTISAMPLE, 0, GL_TEXTURE_SAMPLES, &samples);
  return static_cast<sizei_t>(samples);
}

bool MultisampleTexture2DBase::isCompressed() const noexcept {
  int_t value = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_MULTISAMPLE, 0, GL_TEXTURE_COMPRESSED, &value);
  return value == GL_TRUE;
}
sizei_t MultisampleTexture2DBase::compressedSize() const noexcept {
  int_t size = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_MULTISAMPLE, 0, GL_TEXTURE_COMPRESSED_IMAGE_SIZE, &size);
  return static_cast<sizei_t>(size);
}


void ColourMultisampleTexture2D::allocate(internal_format::Colour colourFormat,
                                          sizei_t width, sizei_t height, sizei_t samples,
                                          bool fixedSampleLocations)
{
  auto internalFormatCode = getFormatCode(colourFormat);
  glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, internalFormatCode,
                          width, height, fixedSampleLocations ? GL_TRUE : GL_FALSE);
}

glm::ivec4 ColourMultisampleTexture2D::rgbaSizes() const noexcept {
  static_assert(sizeof(glm::ivec4) == 4 * sizeof(int_t),
                "Size discrepancy between glm::ivec4 and int_t");
  glm::ivec4 sizes;
  glGetTexLevelParameteriv(GL_TEXTURE_2D_MULTISAMPLE, 0, GL_TEXTURE_RED_SIZE, &sizes.r);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_MULTISAMPLE, 0, GL_TEXTURE_GREEN_SIZE, &sizes.g);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_MULTISAMPLE, 0, GL_TEXTURE_BLUE_SIZE, &sizes.b);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_MULTISAMPLE, 0, GL_TEXTURE_ALPHA_SIZE, &sizes.a);
  return sizes;
}

internal_format::Colour ColourMultisampleTexture2D::internalFormat() const {
  int_t formatCode = 0;
  glGetTexLevelParameteriv(GL_TEXTURE_2D_MULTISAMPLE, 0, GL_TEXTURE_INTERNAL_FORMAT, &formatCode);
  return getColourFormat(static_cast<enum_t>(formatCode));
}


void DepthStencilMultisampleTexture2D::allocate(internal_format::DepthStencil internalFormat,
                                                sizei_t width, sizei_t height, sizei_t samples,
                                                bool fixedSampleLocations)
{
  auto internalFormatCode = getFormatCode(internalFormat);
  glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, internalFormatCode,
                          width, height, fixedSampleLocations ? GL_TRUE : GL_FALSE);
}

sizei_t DepthStencilMultisampleTexture2D::depthSize() const noexcept {
  int_t size = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_MULTISAMPLE, 0, GL_TEXTURE_DEPTH_SIZE, &size);
  return static_cast<sizei_t>(size);
}
sizei_t DepthStencilMultisampleTexture2D::stencilSize() const noexcept {
  int_t size = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_MULTISAMPLE, 0, GL_TEXTURE_STENCIL_SIZE, &size);
  return static_cast<sizei_t>(size);
}

internal_format::DepthStencil DepthStencilMultisampleTexture2D::internalFormat() const {
  int_t formatCode = int_t(0);
  glGetTexLevelParameteriv(GL_TEXTURE_2D_MULTISAMPLE, 0, GL_TEXTURE_INTERNAL_FORMAT, &formatCode);
  return getDepthStencilFormat(static_cast<enum_t>(formatCode));
}

} // namespace gl
} // namespace graphics
