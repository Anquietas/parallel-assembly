#pragma once
#ifndef GRAPHICS_GL_IMAGEFORMAT_H
#define GRAPHICS_GL_IMAGEFORMAT_H

#include "Types.h"

namespace graphics {
namespace gl {
namespace internal_format {

// TODO at some point: compressed textures?
enum class Colour : enum_t {
  // Texture & Renderbuffer:
  // Single-colour formats
  R_8 = 0,      // GL_R8
  R_8i,         // GL_R8I
  R_8ui,        // GL_R8UI
  R_16,         // GL_R16
  R_16f,        // GL_R16F
  R_16i,        // GL_R16I
  R_16ui,       // GL_R16UI
  R_32f,        // GL_R32F
  R_32i,        // GL_R32I
  R_32ui,       // GL_R32UI

  // Two-colour formats
  RG_8,           // GL_RG8
  RG_8i,          // GL_RG8I
  RG_8ui,         // GL_RG8UI
  RG_16,          // GL_RG16
  RG_16f,         // GL_RG16F
  RG_16i,         // GL_RG16I
  RG_16ui,        // GL_RG16UI
  RG_32f,         // GL_RG32F
  RG_32i,         // GL_RG32I
  RG_32ui,        // GL_RG32UI

  // Four-colour formats
  RGBA_8,         // GL_RGBA8
  RGBA_8i,        // GL_RGBA8I
  RGBA_8ui,       // GL_RGBA8UI
  RGBA_16,        // GL_RGBA16
  RGBA_16f,       // GL_RGBA16F
  RGBA_16i,       // GL_RGBA16I
  RGBA_16ui,      // GL_RGBA16UI
  RGBA_32f,       // GL_RGBA32F
  RGBA_32i,       // GL_RGBA32I
  RGBA_32ui,      // GL_RGBA32UI
  
  // Other
  RGB_10_A_2,     // GL_RGB10_A2
  RGB_10_A_2ui,   // GL_RGB10_A2UI
  R11f_G11f_B10f, // GL_R11F_G11F_B10F
  SRGB_8,         // GL_SRGB8
  SRGB_8_A_8,     // GL_SRGB8_ALPHA8

  // Texture only
  // Signed-normalized formats
  R_8SNorm,       // GL_R8_SNORM
  R_16SNorm,      // GL_R16_SNORM
  RG_8SNorm,      // GL_RG8_SNORM
  RG_16SNorm,     // GL_RG16_SNORM
  RGB_8SNorm,     // GL_RGB8_SNORM
  RGB_16SNorm,    // GL_RGB16_SNORM
  RGBA_8SNorm,    // GL_RGBA8_SNORM
  RGBA_16SNorm,   // GL_RGBA16_SNORM

  // Three-colour formats
  RGB_8,          // GL_RGB8
  RGB_8i,         // GL_RGB8I
  RGB_8ui,        // GL_RGB8UI
  RGB_16,         // GL_RGB16
  RGB_16f,        // GL_RGB16F
  RGB_16i,        // GL_RGB16I
  RGB_16ui,       // GL_RGB16UI
  RGB_32f,        // GL_RGB32F
  RGB_32i,        // GL_RGB32I
  RGB_32ui,       // GL_RGB32UI
};

enum class DepthStencil : enum_t {
  Depth_16 = 0,         // GL_DEPTH_COMPONENT16
  Depth_24,             // GL_DEPTH_COMPONENT24
  Depth_32f,            // GL_DEPTH_COMPONENT32F
  Depth_24_Stencil_8,   // GL_DEPTH24_STENCIL8
  Depth_32f_Stencil_8,  // GL_DEPTH32F_STENCIL8
};

} // namespace internal_format

enum_t getFormatCode(internal_format::Colour colourFormat) noexcept;
enum_t getFormatCode(internal_format::DepthStencil depthFormat) noexcept;

internal_format::Colour getColourFormat(enum_t formatCode);
internal_format::DepthStencil getDepthStencilFormat(enum_t formatCode);

namespace format {

enum class Colour : enum_t {
  R = 0,          // GL_RED
  R_i,            // GL_RED_INTEGER
  RG,             // GL_RG
  RG_i,           // GL_RG_INTEGER
  RGB,            // GL_RGB
  RGB_i,          // GL_RGB_INTEGER
  BGR,            // GL_BGR
  BGR_i,          // GL_BGR_INTEGER
  RGBA,           // GL_RGBA
  RGBA_i,         // GL_RGBA_INTEGER
  BGRA,           // GL_BGRA
  BGRA_i,         // GL_BGRA_INTEGER
};

enum class DepthStencil : enum_t {
  Depth,          // GL_DEPTH_COMPONENT,
  DepthStencil,   // GL_DEPTH_STENCIL
};

} // namespace format

enum_t getFormatCode(format::Colour colourFormat) noexcept;
enum_t getFormatCode(format::DepthStencil depthFormat) noexcept;

enum class ImageType : enum_t {
  Byte = 0,             // GL_BYTE
  UByte,                // GL_UNSIGNED_BYTE
  Short,                // GL_SHORT
  UShort,               // GL_UNSIGNED_SHORT
  Int,                  // GL_INT
  UInt,                 // GL_UNSIGNED_INT
  Float,                // GL_FLOAT
  UByte_3_3_2,          // GL_UNSIGNED_BYTE_3_3_2
  UByte_2_3_3_rev,      // GL_UNSIGNED_BYTE_2_3_3_REV
  UShort_5_6_5,         // GL_UNSIGNED_SHORT_5_6_5
  UShort_5_6_5_rev,     // GL_UNSIGNED_SHORT_5_6_5_REV
  UShort_4_4_4_4,       // GL_UNSIGNED_SHORT_4_4_4_4
  UShort_4_4_4_4_rev,   // GL_UNSIGNED_SHORT_4_4_4_4_REV
  UShort_5_5_5_1,       // GL_UNSIGNED_SHORT_5_5_5_1
  UShort_1_5_5_5,       // GL_UNSIGNED_SHORT_1_5_5_5_REV
  UInt_8_8_8_8,         // GL_UNSIGNED_INT_8_8_8_8
  UInt_8_8_8_8_rev,     // GL_UNSIGNED_INT_8_8_8_8_REV
  UInt_10_10_10_2,      // GL_UNSIGNED_INT_10_10_10_2
  UInt_2_10_10_10_rev,  // GL_UNSIGNED_INT_2_10_10_10_REV
};

enum_t getTypeCode(ImageType type) noexcept;

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_IMAGEFORMAT_H
