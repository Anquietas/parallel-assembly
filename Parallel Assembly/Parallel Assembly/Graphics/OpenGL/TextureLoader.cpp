#include "TextureLoader.h"

#include <memory>
#include <sstream>

#include <FreeImage.h>

#include "../../Assert.h"
#include "../../Exception.h"

namespace graphics {
namespace gl {

using FIBITMAP_Ptr = std::unique_ptr<FIBITMAP, void(*)(FIBITMAP*)>;

namespace {
std::ostringstream errorBuffer;
}

void FreeImageErrorHandler(FREE_IMAGE_FORMAT format, const char* message) {
  if (format != FIF_UNKNOWN)
    errorBuffer << "Format: " << FreeImage_GetFormatFromFIF(format) << '\n';
  errorBuffer << message;
}

FIBITMAP_Ptr loadImage(const char* filePath) {
  // Reset error buffer
  errorBuffer.str(std::string());
  FreeImage_SetOutputMessage(&FreeImageErrorHandler);

  FREE_IMAGE_FORMAT format = FREE_IMAGE_FORMAT::FIF_UNKNOWN;
  format = FreeImage_GetFileType(filePath);
  if (format == FIF_UNKNOWN)
    format = FreeImage_GetFIFFromFilename(filePath);
  if (format == FIF_UNKNOWN) {
    std::ostringstream ss;
    ss << "Failed to identify image type for texture file \"" << filePath << '\"';
    throw MAKE_EXCEPTION(ss.str());
  }
  if (!FreeImage_FIFSupportsReading(format)) {
    std::ostringstream ss;
    ss << "Failed to load texture file \"" << filePath << "\": unsupported format";
    throw MAKE_EXCEPTION(ss.str());
  }

  FIBITMAP_Ptr image{FreeImage_Load(format, filePath), FreeImage_Unload};
  if (!image) {
    std::ostringstream ss;
    ss << "Failed to load texture file \"" << filePath << "\":\n" << errorBuffer.str();
    throw MAKE_EXCEPTION(ss.str());
  }

  FIBITMAP_Ptr image32{FreeImage_ConvertTo32Bits(image.get()), FreeImage_Unload};
  if (!image32) {
    std::ostringstream ss;
    ss << "Failed to convert image from texture file \"" << filePath
       << "\" to a format suitable for OpenGL: " << errorBuffer.str();
    throw MAKE_EXCEPTION(ss.str());
  }

  ASSERT(FreeImage_GetImageType(image32.get()) == FIT_BITMAP);

  return image32;
}

glm::ivec2 loadTexture2D(const std::string& filePath, ColourTexture2D& texture) {
  auto image = loadImage(filePath.c_str());

  glm::ivec2 imageSize{FreeImage_GetWidth(image.get()), FreeImage_GetHeight(image.get())};
  texture.allocate(0, internal_format::Colour::RGBA_8, imageSize.x, imageSize.y,
                   format::Colour::BGRA, ImageType::UByte,
                   FreeImage_GetBits(image.get()));

  return imageSize;
}

glm::ivec2 loadTexture2D(const std::string& filePath, ColourTexture2DArray& texArray, int_t index) {
  auto image = loadImage(filePath.c_str());

  glm::ivec2 imageSize{FreeImage_GetWidth(image.get()), FreeImage_GetHeight(image.get())};
  texArray.update(0, 0, 0, index, imageSize.x, imageSize.y, 1,
                  format::Colour::BGRA, ImageType::UByte,
                  FreeImage_GetBits(image.get()));

  return imageSize;
}

std::vector<glm::vec2> loadTexture2DArray(const std::vector<std::string>& filePaths,
                                          sizei_t width, sizei_t height,
                                          ColourTexture2DArray& texArray)
{
  sizei_t numImages = static_cast<sizei_t>(filePaths.size());

  std::vector<glm::vec2> texCoords;

  texArray.allocate(0, internal_format::Colour::RGBA_8, width, height,
                    numImages, format::Colour::BGRA, ImageType::UByte, nullptr);
  int_t index = 0;
  for (const auto& filePath : filePaths) {
    auto image = loadImage(filePath.c_str());
    glm::ivec2 imageSize{FreeImage_GetWidth(image.get()), FreeImage_GetHeight(image.get())};
    texCoords.emplace_back(glm::vec2(imageSize) / glm::vec2(width, height));

    texArray.update(0, 0, 0, index, imageSize.x, imageSize.y, 1,
                    format::Colour::BGRA, ImageType::UByte,
                    FreeImage_GetBits(image.get()));

    ++index;
  }

  return texCoords;
}

} // namespace gl
} // namespace graphics
