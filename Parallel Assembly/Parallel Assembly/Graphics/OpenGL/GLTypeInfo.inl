#ifndef GRAPHICS_GL_GLTYPEINFO_INL
#define GRAPHICS_GL_GLTYPEINFO_INL

#pragma once

#include <algorithm>

#include "GLTypeInfo.h"
#include "OpenGLIncludes.h"

namespace graphics {
namespace gl {
namespace util {

// Primative types
template <> struct GLTypeInfo<byte_t> : GLTypeInfoBase<GL_BYTE, 1, true> {};
template <> struct GLTypeInfo<ubyte_t> : GLTypeInfoBase<GL_UNSIGNED_BYTE, 1, true> {};
template <> struct GLTypeInfo<short_t> : GLTypeInfoBase<GL_SHORT, 1, true> {};
template <> struct GLTypeInfo<ushort_t> : GLTypeInfoBase<GL_UNSIGNED_SHORT, 1, true> {};
template <> struct GLTypeInfo<int_t> : GLTypeInfoBase<GL_INT, 1, true> {};
template <> struct GLTypeInfo<uint_t> : GLTypeInfoBase<GL_UNSIGNED_INT, 1, true> {};
template <> struct GLTypeInfo<float_t> : GLTypeInfoBase<GL_FLOAT, 1, false> {};
template <> struct GLTypeInfo<double_t> : GLTypeInfoBase<GL_DOUBLE, 1, false> {};

// GLM types
namespace detail {

template <typename T>
struct isGLMType : std::false_type {};

// vec2
template <> struct isGLMType<glm::vec2> : std::true_type {};
template <> struct isGLMType<glm::dvec2> : std::true_type {};
template <> struct isGLMType<glm::ivec2> : std::true_type {};
template <> struct isGLMType<glm::uvec2> : std::true_type {};
// vec3
template <> struct isGLMType<glm::vec3> : std::true_type {};
template <> struct isGLMType<glm::dvec3> : std::true_type {};
template <> struct isGLMType<glm::ivec3> : std::true_type {};
template <> struct isGLMType<glm::uvec3> : std::true_type {};
// vec4
template <> struct isGLMType<glm::vec4> : std::true_type {};
template <> struct isGLMType<glm::dvec4> : std::true_type {};
template <> struct isGLMType<glm::ivec4> : std::true_type {};
template <> struct isGLMType<glm::uvec4> : std::true_type {};

// mat2
template <> struct isGLMType<glm::mat2> : std::true_type {};
template <> struct isGLMType<glm::dmat2> : std::true_type {};
// mat2x3
template <> struct isGLMType<glm::mat2x3> : std::true_type {};
template <> struct isGLMType<glm::dmat2x3> : std::true_type {};
// mat2x4
template <> struct isGLMType<glm::mat2x4> : std::true_type {};
template <> struct isGLMType<glm::dmat2x4> : std::true_type {};
// mat3
template <> struct isGLMType<glm::mat3> : std::true_type {};
template <> struct isGLMType<glm::dmat3> : std::true_type {};
// mat3x2
template <> struct isGLMType<glm::mat3x2> : std::true_type {};
template <> struct isGLMType<glm::dmat3x2> : std::true_type {};
// mat3x4
template <> struct isGLMType<glm::mat3x4> : std::true_type {};
template <> struct isGLMType<glm::dmat3x4> : std::true_type {};
// mat4
template <> struct isGLMType<glm::mat4> : std::true_type {};
template <> struct isGLMType<glm::dmat4> : std::true_type {};
// mat4x2
template <> struct isGLMType<glm::mat4x2> : std::true_type {};
template <> struct isGLMType<glm::dmat4x2> : std::true_type {};
// mat4x3
template <> struct isGLMType<glm::mat4x3> : std::true_type {};
template <> struct isGLMType<glm::dmat4x3> : std::true_type {};

// quat
template <> struct isGLMType<glm::quat> : std::true_type {};
template <> struct isGLMType<glm::dquat> : std::true_type {};
// dualquat
template <> struct isGLMType<glm::dualquat> : std::true_type {};
template <> struct isGLMType<glm::ddualquat> : std::true_type {};

template <typename T>
struct glmComponents {
  static_assert(isGLMType<T>::value, "OOPS!");
  static constexpr int value = sizeof(T) / sizeof(typename T::value_type);
};

} // namespace detail

template <typename T>
struct GLTypeInfo<T, typename std::enable_if<detail::isGLMType<T>::value &&
                                             !isMatrixType<T>::value>::type>
  : GLTypeInfoBase<
    GLTypeInfo<typename T::value_type>::typeID,
    detail::glmComponents<T>::value,
    GLTypeInfo<typename T::value_type>::isIntType>
{};

// Workaround for compiler bug in VS2017
// TODO: replace when fixed
#define max(x, y) (x < y ? y : x)
#define min(x, y) (y < x ? y : x)

template <typename T>
struct GLTypeInfo<T, typename std::enable_if<detail::isGLMType<T>::value &&
                                             isMatrixType<T>::value>::type>
  : GLTypeInfoBase<
      GLTypeInfo<typename T::value_type>::typeID,
      // Handle mat2x4 and mat4x2 correctly
      max(detail::glmComponents<typename T::row_type>::value,
          detail::glmComponents<typename T::transpose_type::row_type>::value),
      GLTypeInfo<typename T::value_type>::isIntType,
      min(detail::glmComponents<typename T::row_type>::value,
          detail::glmComponents<typename T::transpose_type::row_type>::value),
      sizeof(typename T::value_type) *
        min(detail::glmComponents<typename T::row_type>::value,
            detail::glmComponents<typename T::transpose_type::row_type>::value)>
{};

#undef min
#undef max

// mat2 will fit in one 4-component value
template <>
struct GLTypeInfo<glm::mat2>
  : GLTypeInfoBase<GLTypeInfo<glm::mat2::value_type>::typeID, 4, false> {};

// dualquat uses a different typedef for some fucking stupid reason
template <>
struct GLTypeInfo<glm::dualquat>
  : GLTypeInfoBase<GLTypeInfo<glm::dualquat::part_type::value_type>::typeID,
                    4, false, 2, sizeof(glm::dualquat::part_type)>
{};
template <>
struct GLTypeInfo<glm::ddualquat>
  : GLTypeInfoBase<GLTypeInfo<glm::ddualquat::part_type::value_type>::typeID,
                    4, false, 2, sizeof(glm::ddualquat::part_type)>
{};

#ifndef NDEBUG
#define ASSERT_GLCP(T, comps, partsN) \
  GLTypeInfo<T>::components == comps && GLTypeInfo<T>::parts == partsN

// Assert optimal layouts
static_assert(ASSERT_GLCP(glm::mat2, 4, 1), "OOPS");
static_assert(ASSERT_GLCP(glm::mat2x3, 3, 2), "OOPS");
static_assert(ASSERT_GLCP(glm::mat2x4, 4, 2), "OOPS");
static_assert(ASSERT_GLCP(glm::mat3, 3, 3), "OOPS");
static_assert(ASSERT_GLCP(glm::mat3x2, 3, 2), "OOPS");
static_assert(ASSERT_GLCP(glm::mat3x4, 4, 3), "OOPS");
static_assert(ASSERT_GLCP(glm::mat4, 4, 4), "OOPS");
static_assert(ASSERT_GLCP(glm::mat4x2, 4, 2), "OOPS");
static_assert(ASSERT_GLCP(glm::mat4x3, 4, 3), "OOPS");
static_assert(ASSERT_GLCP(glm::quat, 4, 1), "OOPS");
static_assert(ASSERT_GLCP(glm::dualquat, 4, 2), "OOPS");

#undef ASSERT_GLCP
#endif // NDEBUG

} // namespace util
} // namespace gl
} // namespace graphics


#endif // GRAPHICS_GL_GLTYPEINFO_INL
