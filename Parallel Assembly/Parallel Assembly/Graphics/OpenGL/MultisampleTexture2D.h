#pragma once
#ifndef GRAPHICS_GL_MULTISAMPLE_TEXTURE_2D_H
#define GRAPHICS_GL_MULTISAMPLE_TEXTURE_2D_H

#include "Types.h"
#include "GLObject.h"
#include "ImageFormat.h"
#include "../../GLMIncludes.h"

namespace graphics {
namespace gl {

class MultisampleTexture2DBase : public GLObject<traits::Texture> {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(MultisampleTexture2DBase, GLObject)

  // Binds the texture to GL_TEXTURE_2D_MULTISAMPLE
  void bind() noexcept;
  // Unbinds the texture currently bound to GL_TEXTURE_2D_MULTISAMPLE
  static void unbind() noexcept;

  // The following functions will only work after calling bind()!
  // The following functions will not return meaningful results until allocate has been called

  sizei_t width() const noexcept;
  sizei_t height() const noexcept;
  sizei_t samples() const noexcept;

  bool isCompressed() const noexcept;
  sizei_t compressedSize() const noexcept;
};

class ColourMultisampleTexture2D final : public MultisampleTexture2DBase {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(ColourMultisampleTexture2D, MultisampleTexture2DBase)

  // The following functions will only work after calling bind()!

  // Allocates the texture with the specified format, width, height, number of samples
  // per texel and whether to use fixed sample locations for each texel.
  void allocate(internal_format::Colour colourFormat, sizei_t width, sizei_t height,
                sizei_t samples, bool fixedSampleLocations);

  // The following functions will not return meaningful results until allocate has been called

  glm::ivec4 rgbaSizes() const noexcept;

  internal_format::Colour internalFormat() const;
};

class DepthStencilMultisampleTexture2D final : public MultisampleTexture2DBase {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(DepthStencilMultisampleTexture2D, MultisampleTexture2DBase)

  // The following functions will only work after calling bind()!

  // Allocates the texture with the specified format, width, height, number of samples
  // per texel and whether to use fixed sample locations for each texel.
  void allocate(internal_format::DepthStencil internalFormat, sizei_t width, sizei_t height,
                sizei_t samples, bool fixedSampleLocations);

  // Thw following functions will not return meaningful results until allocate has been called

  sizei_t depthSize() const noexcept;
  sizei_t stencilSize() const noexcept;

  internal_format::DepthStencil internalFormat() const;
};

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_MULTISAMPLE_TEXTURE_2D_H
