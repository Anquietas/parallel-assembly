#ifndef GRAPHICS_GL_VERTEXBUFFER_H
#define GRAPHICS_GL_VERTEXBUFFER_H

#pragma once

#include "Types.h"
#include "GLTypeInfo.h"
#include "AttributeID.h"
#include "GLObject.h"

#include <vector>
#include <tuple>
#include <type_traits>

namespace graphics {
namespace gl {

class VertexBuffer final : public GLObject<traits::VBO> {
public:
  enum class Target : enum_t {
    // Note: Only one VertexBuffer may be bound to the Array target at a time
    Array,    // GL_ARRAY_BUFFER
    // Note: These targets may interfere with other buffer types bound to them,
    //       and vice versa. Hence they should really only be used short-term.
    CopyRead, // GL_COPY_READ_BUFFER
    CopyWrite // GL_COPY_WRITE_BUFFER
  };

  enum class Usage : enum_t {
    StaticDraw,   // GL_STATIC_DRAW
    StaticRead,   // GL_STATIC_READ
    StaticCopy,   // GL_STATIC_COPY
    DynamicDraw,  // GL_DYNAMIC_DRAW
    DynamicRead,  // GL_DYNAMIC_READ
    DynamicCopy,  // GL_DYNAMIC_COPY
    StreamDraw,   // GL_STREAM_DRAW
    StreamRead,   // GL_STREAM_READ
    StreamCopy    // GL_STREAM_COPY
  };

  enum class Access : enum_t {
    Read,     // GL_READ_ONLY  or GL_MAP_READ_BIT
    Write,    // GL_WRITE_ONLY or GL_MAP_WRITE_BIT
    ReadWrite // GL_READ_WRITE or (GL_MAP_READ_BIT | GL_MAP_WRITE_BIT)
  };

  // Used to specify the data format of an interleaved buffer
  class InterleavedFormat;

  GLOBJECT_CONSTRUCTOR_HELPER(VertexBuffer, GLObject)

  // Binds the VBO to the binding target.
  //  - Note: This will unbind any VBO previously bound to the target
  //  - Note: This will unbind any kind of buffer previously bound to the
  //          target if the target is not Target::Array.
  //  - Note: This must be called before any other operations are performed
  void bind(Target target = Target::Array) noexcept;
  // Unbinds the VBO bound at the specified target
  static void unbind(Target target = Target::Array) noexcept;

  // Allocates the buffer at the specified target without storing
  // any data in it, using the specified usage hint
  // - size here means the total size in bytes to allocate
  // - This will also erase any previous allocation of the buffer
  void allocate(sizeiptr_t size,
                Usage usage = Usage::StaticDraw,
                Target target = Target::Array) noexcept;
  // Allocates the buffer at the specified target, and also stores
  // the provided data in it (if the pointer is not null), using
  // the specified usage hint.
  // - size here means the length of the array.
  // - This will also erase any previous allocation of the buffer
  template <typename T>
  void allocate(sizeiptr_t size, const T* data,
                Usage usage = Usage::StaticDraw,
                Target target = Target::Array) noexcept;

  // Updates a portion of the buffer at the specified target with
  // the provided data, with offset and size defining the bounds
  // of the region to be updated.
  // - offset here should be the location within the buffer to begin
  //   the mapping at, in bytes
  // - size here should be the length of the data array.
  // - Note: offset and size must define a region *inside* the
  //   region previously allocated with "allocate". If this is not
  //   satisfied, OpenGL will raise the GL_INVALID_VALUE error.
  template <typename T>
  void update(intptr_t offset, std::size_t size, const T* data,
              Target target = Target::Array) noexcept;

  // Used to map all of the buffer at the specified target to RAM
  // without type safety
  static void* map(Target target = Target::Array,
                   Access access = Access::Write) noexcept;
  // Used to map a portion of the buffer at the specified target to RAM
  // without type safety
  // - offset here should be the location within the buffer to begin
  //   the mapping at, in bytes.
  // - size here should be the size of the mapped region, in bytes
  static void* map(intptr_t offset, sizeiptr_t length,
                   Target target = Target::Array,
                   Access access = Access::Write) noexcept;
  // Used to map all of the buffer at the specified target to RAM
  template <typename T>
  static T* mapAs(Target target = Target::Array,
                  Access access = Access::Write) noexcept;
  // Used to map a portion of the buffer at the specified target to RAM
  // - offset here should be the location within the buffer to begin
  //   the mapping at, in bytes
  // - size here should be the number of elements of type T to map
  template <typename T>
  static T* mapAs(intptr_t offset, std::size_t length,
                  Target target = Target::Array,
                  Access access = Access::Write) noexcept;
  // Const overload of mapAs(target, access)
  template <typename T>
  static const T* mapAsConst(Target target = Target::Array,
                             Access access = Access::Read) noexcept;
  template <typename T>
  // - offset here should be the location within the buffer to begin
  //   the mapping at, in bytes
  // - size here should be the number of elements of type T to map
  static const T* mapAsConst(intptr_t offset, std::size_t length,
                             Target target = Target::Array,
                             Access access = Access::Read) noexcept;
  static void unmap(Target target = Target::Array);

  // Used to bind the entire buffer to a single vertex attribute in
  // a shader program.
  template <typename AttribT>
  std::enable_if_t<!util::isMatrixType<AttribT>::value>
    bindToAttribute(const AttributeID& id, bool normalized = false, intptr_t offset = 0) noexcept;
  // This one includes dual quaternions!
  template <typename AttribT>
  std::enable_if_t<util::isMatrixType<AttribT>::value>
    bindToAttribute(const AttributeID& id, intptr_t offset = 0) noexcept;
  // Used to bind the buffer to several vertex attributes using an
  // interleaved format.
  void bindToAttributes(const InterleavedFormat& format) noexcept;
};

class VertexBuffer::InterleavedFormat final {
  friend class VertexBuffer;
public:
  using AttributeFormat = std::tuple<
      AttributeID,
      enum_t,   // Attribute OpenGL type
      int_t,    // Number of "components" in attribute (e.g. glm::vec3 has 3 components)
      bool,     // Is integer type? - Optimization
      bool,     // Normalized?
      sizei_t,  // Stride (offset between consecutive values of the attribute, in bytes)
                // - This is usually constant for all values in a given buffer,
                //   but is left in this format for flexibility
      intptr_t  // Offset to first value of the attribute in the buffer, in bytes
    >;
private:
  std::vector<AttributeFormat> m_formatData;

public:
  InterleavedFormat() = default;
  InterleavedFormat(std::initializer_list<AttributeFormat>);
  InterleavedFormat(const InterleavedFormat&) = default;
  InterleavedFormat(InterleavedFormat&&) noexcept = default;
  ~InterleavedFormat() = default;

  // Adds format information for a given attribute
  // - Here, stride and offset are in bytes
  template <typename AttribT>
  std::enable_if_t<!util::isMatrixType<AttribT>::value>
  addAttribute(const AttributeID& id, bool normalized,
               sizei_t stride, intptr_t offset);
  template <typename AttribT>
  std::enable_if_t<util::isMatrixType<AttribT>::value>
  addAttribute(const AttributeID& id, sizei_t stride, intptr_t offset);
};

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_VERTEXBUFFER_H
