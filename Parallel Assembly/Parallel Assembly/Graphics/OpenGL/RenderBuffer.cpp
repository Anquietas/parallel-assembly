#include "RenderBuffer.h"

#include "OpenGLIncludes.h"

#include "GLObject.inl"

#include "../../Assert.h"

namespace graphics {
namespace gl {

namespace detail {

void RenderBufferBase::bind() noexcept {
  glBindRenderbuffer(GL_RENDERBUFFER, m_id);
}
void RenderBufferBase::unbind() noexcept {
  glBindRenderbuffer(GL_RENDERBUFFER, value_type(0));
}

sizei_t RenderBufferBase::width() const noexcept {
  int_t value = int_t(0);
  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &value);
  return static_cast<sizei_t>(value);
}
sizei_t RenderBufferBase::height() const noexcept {
  int_t value = int_t(0);
  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &value);
  return static_cast<sizei_t>(value);
}
sizei_t RenderBufferBase::samples() const noexcept {
  int_t value = int_t(0);
  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_SAMPLES, &value);
  return static_cast<sizei_t>(value);
}

}


void ColourRenderBuffer::allocate(internal_format::Colour colourFormat,
                                  sizei_t width, sizei_t height) noexcept
{
  ASSERT(width > 0 && height > 0);
  auto formatCode = getFormatCode(colourFormat);
  glRenderbufferStorage(GL_RENDERBUFFER, formatCode, width, height);
}
void ColourRenderBuffer::allocate(internal_format::Colour colourFormat,
                                  sizei_t width, sizei_t height, sizei_t samples) noexcept
{
  ASSERT(width > 0 && height > 0 && samples >= 0);
  auto formatCode = getFormatCode(colourFormat);
  glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, formatCode, width, height);
}


glm::ivec4 ColourRenderBuffer::rgbaSizes() const noexcept {
  glm::ivec4 value;
  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_RED_SIZE, &value.r);
  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_GREEN_SIZE, &value.g);
  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_BLUE_SIZE, &value.b);
  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_ALPHA_SIZE, &value.a);
  return value;
}

internal_format::Colour ColourRenderBuffer::internalFormat() const {
  int_t formatCode = int_t(0);
  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_INTERNAL_FORMAT, &formatCode);
  return getColourFormat(static_cast<enum_t>(formatCode));
}


void DepthStencilRenderBuffer::allocate(internal_format::DepthStencil depthFormat,
                                        sizei_t width, sizei_t height) noexcept
{
  ASSERT(width > 0 && height > 0);
  auto formatCode = getFormatCode(depthFormat);
  glRenderbufferStorage(GL_RENDERBUFFER, formatCode, width, height);
}
void DepthStencilRenderBuffer::allocate(internal_format::DepthStencil depthFormat,
                                        sizei_t width, sizei_t height, sizei_t samples) noexcept
{
  ASSERT(width > 0 && height > 0 && samples >= 0);
  auto formatCode = getFormatCode(depthFormat);
  glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, formatCode, width, height);
}

sizei_t DepthStencilRenderBuffer::depthSize() const noexcept {
  int_t value = int_t(0);
  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_DEPTH_SIZE, &value);
  return static_cast<sizei_t>(value);
}
sizei_t DepthStencilRenderBuffer::stencilSize() const noexcept {
  int_t value = int_t(0);
  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_STENCIL_SIZE, &value);
  return static_cast<sizei_t>(value);
}

internal_format::DepthStencil DepthStencilRenderBuffer::internalFormat() const {
  int_t formatCode = int_t(0);
  glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_INTERNAL_FORMAT, &formatCode);
  return getDepthStencilFormat(static_cast<enum_t>(formatCode));
}

} // namespace gl
} // namespace graphics