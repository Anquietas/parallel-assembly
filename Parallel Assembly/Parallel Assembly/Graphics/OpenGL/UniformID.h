#pragma once
#ifndef GRAPHICS_GL_UNIFORM_ID_H
#define GRAPHICS_GL_UNIFORM_ID_H

#include "Types.h"

namespace graphics {
namespace gl {

struct UniformID final {
  uint_t id;

  explicit UniformID(uint_t id_ = static_cast<uint_t>(-1)) noexcept : id(id_) {}
  UniformID(const UniformID& o) noexcept = default;
  UniformID(UniformID&& o) noexcept = default;
  ~UniformID() noexcept = default;

  UniformID& operator=(const UniformID& o) noexcept = default;
  UniformID& operator=(UniformID&& o) noexcept = default;

  bool valid() const noexcept { return id != static_cast<uint_t>(-1); }
};

inline bool operator==(const UniformID& x, const UniformID& y) noexcept {
  return x.id == y.id;
}
inline bool operator!=(const UniformID& x, const UniformID& y) noexcept {
  return x.id != y.id;
}

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_UNIFORM_ID_H
