#pragma once
#ifndef GRAPHICS_GL_GLOBJECT_H
#define GRAPHICS_GL_GLOBJECT_H

#include "Types.h"
#include <type_traits>

namespace graphics {
namespace gl {

namespace traits {
struct VAO {};
struct VBO {};
struct Texture {};
struct Sampler {};
struct RenderBuffer {};
struct FrameBuffer {};
struct Query {};

template <typename TagT>
struct Traits {
  static_assert(sizeof(TagT) == -1, "traits instantiated for unknown tag type");
};

#define MAKE_GL_TRAITS(TagT) \
template <> struct Traits<TagT> { \
  using value_type = uint_t; \
\
  inline static value_type create() noexcept; \
  inline static void create(value_type* ids, sizei_t n) noexcept; \
\
  inline static void destroy(value_type id) noexcept; \
  inline static void destroy(const value_type* ids, sizei_t n) noexcept; \
\
  inline static bool checkID(value_type id) noexcept; \
}

MAKE_GL_TRAITS(VAO);
MAKE_GL_TRAITS(VBO);
MAKE_GL_TRAITS(Texture);
MAKE_GL_TRAITS(Sampler);
MAKE_GL_TRAITS(RenderBuffer);
MAKE_GL_TRAITS(FrameBuffer);
MAKE_GL_TRAITS(Query);

#undef MAKE_GL_TRAITS

} // namespace traits

template <typename TagT>
class GLObject {
protected:
  using value_type = typename traits::Traits<TagT>::value_type;

  value_type m_id;
public:
  // Constructs an uninitialized object. USed to create several objects at once via init
  explicit GLObject(std::nullptr_t) noexcept;
  // Constructs and initializes the corresponding object type if id is 0.
  // If id is not 0, then takes ownership of the object.
  explicit GLObject(value_type id = value_type(0)) noexcept;
  GLObject(const GLObject&) = delete;
  GLObject(GLObject&& o) noexcept;
  // Deliberately non-virtual; do not delete via base class pointer!
  ~GLObject(); 

  GLObject& operator=(const GLObject&) = delete;
  GLObject& operator=(GLObject&& o) noexcept;

  inline void init() noexcept;
  inline static void init(GLObject* objs, sizei_t n) noexcept;

  inline void destroy() noexcept;
  inline static void destroy(GLObject* objs, sizei_t n) noexcept;

  value_type id() const { return m_id; }

  bool valid() const noexcept;
};

#ifndef GRAPHICS_GL_GLOBJECT_NO_EXTERN_TEMPLATES

#define EXTERN_GLOBJECT(T) \
  extern template struct traits::Traits<T>; \
  extern template class GLObject<T>

EXTERN_GLOBJECT(traits::VAO);
EXTERN_GLOBJECT(traits::VBO);
EXTERN_GLOBJECT(traits::Texture);
EXTERN_GLOBJECT(traits::Sampler);
EXTERN_GLOBJECT(traits::RenderBuffer);
EXTERN_GLOBJECT(traits::FrameBuffer);
EXTERN_GLOBJECT(traits::Query);

#undef EXTERN_GLOBJECT

#endif // GRAPHICS_GL_GLOBJECT_NO_EXTERN_TEMPLATES

#define GLOBJECT_CONSTRUCTOR_HELPER(T, BaseT) \
  explicit T(std::nullptr_t) : BaseT(nullptr) {} \
  explicit T(value_type id = value_type(0)) : BaseT(id) {} \
  T(const T&) = delete; \
  T(T&& o) noexcept = default; \
  ~T() = default; \
  \
  T& operator=(const T&) = delete; \
  T& operator=(T&& o) noexcept = default;

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_GLOBJECT_H
