#pragma once
#ifndef GRAPHICS_GL_DEBUG_HANDLER_H
#define GRAPHICS_GL_DEBUG_HANDLER_H

namespace graphics {
namespace gl {

void setupDebugHandler();

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_DEBUG_HANDLER_H
