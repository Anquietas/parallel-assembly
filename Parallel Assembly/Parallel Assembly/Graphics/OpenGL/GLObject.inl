#pragma once
#ifndef GRAPHICS_GL_GLOBJECT_INL
#define GRAPHICS_GL_GLOBJECT_INL

#include "GLObject.h"

#include <utility>
#include <algorithm>

#include "../../Assert.h"

namespace graphics {
namespace gl {

template <typename TagT>
GLObject<TagT>::GLObject(std::nullptr_t) noexcept
  : m_id(value_type(0))
{}
template <typename TagT>
GLObject<TagT>::GLObject(value_type id) noexcept
  : m_id(id)
{
  if (m_id == value_type(0))
    init();
  else
    ASSERT_MSG(traits::Traits<TagT>::checkID(m_id),
               "GLObject constructed with invalid object id");
}
template <typename TagT>
GLObject<TagT>::GLObject(GLObject&& o) noexcept
  : m_id(std::exchange(o.m_id, value_type(0)))
{}
template <typename TagT>
GLObject<TagT>::~GLObject() {
  if (m_id != value_type(0))
    traits::Traits<TagT>::destroy(m_id);
}

template <typename TagT>
auto GLObject<TagT>::operator=(GLObject&& o) noexcept -> GLObject& {
  if (m_id != value_type(0))
    traits::Traits<TagT>::destroy(m_id);
  m_id = std::exchange(o.m_id, value_type(0));
  return *this;
}

template <typename TagT>
void GLObject<TagT>::init() noexcept {
  ASSERT_MSG(m_id == value_type(0), "GLObject::init() called with object already initialized");
  m_id = traits::Traits<TagT>::create();
  ASSERT_MSG(m_id != value_type(0), "GLObject::init() failed to initialize object");
}
template <typename TagT>
void GLObject<TagT>::init(GLObject* objs, sizei_t n) noexcept {
  static_assert(sizeof(value_type) == sizeof(GLObject),
                "Size discrepancy between value_tpye and GLObject");
  ASSERT(n > 0);
  auto ids = reinterpret_cast<value_type*>(objs);
  ASSERT(std::all_of(ids, ids + n, [](uint_t x) -> bool { return x == 0; }));
  traits::Traits<TagT>::create(ids, n);
  ASSERT(std::all_of(ids, ids + n, [](uint_t x) -> bool { return x != 0; }));
  ASSERT(std::equal(ids, ids + n, objs, objs + n,
                    [](uint_t id, const GLObject& obj) -> bool {
                      return id == obj.id();
                    }));
}

template <typename TagT>
void GLObject<TagT>::destroy() noexcept {
  if (m_id != value_type(0)) {
    traits::Traits<TagT>::destroy(m_id);
    m_id = value_type(0);
  }
}
template <typename TagT>
void GLObject<TagT>::destroy(GLObject* objs, sizei_t n) noexcept {
  static_assert(sizeof(value_type) == sizeof(GLObject),
                "Size discrepancy between value_tpye and GLObject");
  auto ids = reinterpret_cast<value_type*>(objs);
  traits::Traits<TagT>::destroy(ids, n);
  std::fill_n(ids, n, value_type(0));
}

template <typename TagT>
bool GLObject<TagT>::valid() const noexcept {
  return traits::Traits<TagT>::checkID(m_id);
}

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_GLOBJECT_INL
