#ifndef GRAPHICS_GL_TYPES_H
#define GRAPHICS_GL_TYPES_H

// Defines types to be used in place of OpenGL's types such as GLint,
// GLuint etc. This is to avoid needing to include OpenGL's headers
// to specify types of the correct size and format.

#include <cstdint>
#include <cstddef>

namespace graphics {
namespace gl {

namespace detail {
static_assert(sizeof(float) == 4, "float is not exactly 32 bits");
using float32_t = float;
static_assert(sizeof(double) == 8, "double is not exactly 64 bits");
using float64_t = double;
} // namespace detail

using byte_t = std::int8_t;
using ubyte_t = std::uint8_t;
using short_t = std::int16_t;
using ushort_t = std::uint16_t;
using int_t = std::int32_t;
using uint_t = std::uint32_t;
// Fixed-point number, stored as int by OpenGL
// Representation: Signed, 2's complement 16.16 integer
using fixed_t = std::int32_t;
using int64_t = std::int64_t;
using uint64_t = std::uint64_t;
// Should be non-negative, but GLEW uses "int"
using sizei_t = std::int32_t;
using enum_t = std::uint32_t;
using intptr_t = std::intptr_t;
// Specified as "Non negative binary integer size, for
// pointer offsets and ranges", GLEW uses ptrdiff_t
using sizeiptr_t = std::ptrdiff_t;
// Handle to a "Sync Object", GLEW defines this as __GLsync*
using sync_t = void*;
using bitfield = std::uint32_t;
// IEEE-754 16-bit floating point, GLEW uses "unsigned short"
// using half_t = std::uint16_t; // Causes problems
using float_t = detail::float32_t;
// float_t, but clamped to [0,1]
using clampf_t = detail::float32_t;
using double_t = detail::float64_t;
// double_t, but clamped to [0,1]
using clampd_t = detail::float64_t;
// GLchar excluded since it's unnecessary if GLchar == char

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_TYPES_H
