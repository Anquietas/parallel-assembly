#ifndef GRAPHICS_GL_VERTEXBUFFER_INL
#define GRAPHICS_GL_VERTEXBUFFER_INL

#pragma once

#include "VertexBuffer.h"
#include "OpenGLIncludes.h"
#include "GLTypeInfo.inl"

namespace graphics {
namespace gl {

namespace detail {

GLenum bindTargetCode(VertexBuffer::Target target) noexcept;
GLenum bufferUsageCode(VertexBuffer::Usage usage) noexcept;
GLenum bufferAccessCode(VertexBuffer::Access access) noexcept;
GLbitfield bufferAccessBitfield(VertexBuffer::Access access) noexcept;

} // namespace detail

template <typename T>
void VertexBuffer::allocate(sizeiptr_t size, const T* data,
                            Usage usage, Target target) noexcept
{
  glBufferData(detail::bindTargetCode(target),
               size * sizeof(T), data,
               detail::bufferUsageCode(usage));
}
template <typename T>
void VertexBuffer::update(intptr_t offset, std::size_t size, const T* data, Target target) noexcept {
  glBufferSubData(detail::bindTargetCode(target), offset,
                  static_cast<sizeiptr_t>(size) * sizeof(T), data);
}

template <bool isIntType = true>
void bindToAttributeNonMatrixImpl(
  const AttributeID& id, int_t components, enum_t typeID, bool normalized, intptr_t offset) noexcept
{
  if (normalized)
    glVertexAttribPointer(id.id, components, typeID, GL_TRUE, 0, reinterpret_cast<void*>(offset));
  else
    glVertexAttribIPointer(id.id, components, typeID, 0, reinterpret_cast<void*>(offset));
}
template <>
void bindToAttributeNonMatrixImpl<false>(
  const AttributeID& id, int_t components, enum_t typeID, bool /*normalized*/, intptr_t offset) noexcept;

template <typename AttribT>
std::enable_if_t<!util::isMatrixType<AttribT>::value>
VertexBuffer::bindToAttribute(const AttributeID& id, bool normalized, intptr_t offset) noexcept {
  using namespace util;
  static_assert(GLTypeInfo<AttribT>::components <= 4,
                "DERP! Forgot a matrix type");
  constexpr auto components = GLTypeInfo<AttribT>::components;
  constexpr auto typeID = GLTypeInfo<AttribT>::typeID;

  bindToAttributeNonMatrixImpl<GLTypeInfo<AttribT>::isIntType>(
    id, components, typeID, normalized, offset);
}
template <typename AttribT>
std::enable_if_t<util::isMatrixType<AttribT>::value>
VertexBuffer::bindToAttribute(const AttributeID& id, intptr_t offset) noexcept {
  using namespace util;
  constexpr auto components = GLTypeInfo<AttribT>::components;
  constexpr auto typeID = GLTypeInfo<AttribT>::typeID;
  constexpr auto partOffset = GLTypeInfo<AttribT>::partOffset;
  constexpr auto size = sizeof(AttribT);
  for (int i = 0; i < GLTypeInfo<AttribT>::parts; ++i) {
    intptr_t totalOffset = offset + i * partOffset;
    glVertexAttribPointer(id.id, components, typeID,
                          GL_FALSE, // matrices use floating point types
                          size, reinterpret_cast<void*>(totalOffset));
  }
}

template <typename T>
T* VertexBuffer::mapAs(Target target, Access access) noexcept {
  return reinterpret_cast<T*>(
        glMapBuffer(detail::bindTargetCode(target),
                    detail::bufferAccessCode(access)));
}
template <typename T>
T* VertexBuffer::mapAs(intptr_t offset, std::size_t length,
                       Target target, Access access) noexcept
{
  return reinterpret_cast<T*>(
        glMapBufferRange(detail::bindTargetCode(target), offset,
                         static_cast<sizeiptr_t>(length) * sizeof(T),
                         detail::bufferAccessBitfield(access)));
}
template <typename T>
const T* VertexBuffer::mapAsConst(Target target, Access access) noexcept {
  return reinterpret_cast<const T*>(
        glMapBuffer(detail::bindTargetCode(target),
                    detail::bufferAccessCode(access)));
}
template <typename T>
const T* VertexBuffer::mapAsConst(intptr_t offset, std::size_t length,
                                  Target target, Access access) noexcept
{
  return reinterpret_cast<const T*>(
        glMapBufferRange(detail::bindTargetCode(target), offset,
                         static_cast<sizeiptr_t>(length) * sizeof(T),
                         detail::bufferAccessBitfield(access)));
}

// Covers primatives and vectors
template <typename AttribT>
std::enable_if_t<!util::isMatrixType<AttribT>::value>
VertexBuffer::InterleavedFormat::addAttribute(
  const AttributeID& id, bool normalized, sizei_t stride, intptr_t offset)
{
  using namespace util;
  static_assert(GLTypeInfo<AttribT>::components <= 4 &&
                GLTypeInfo<AttribT>::parts == 1,
                "DERP! Forgot a matrix type");
  constexpr auto typeID     = GLTypeInfo<AttribT>::typeID;
  constexpr auto components = GLTypeInfo<AttribT>::components;
  constexpr auto isIntType  = GLTypeInfo<AttribT>::isIntType;
  m_formatData.emplace_back(id, typeID, components, isIntType,
                            // Disallow floats to be normalized
                            isIntType ? normalized : false, stride, offset);
}
// Matrices need to be handled specially
template <typename AttribT>
std::enable_if_t<util::isMatrixType<AttribT>::value>
VertexBuffer::InterleavedFormat::addAttribute(
  const AttributeID& id, sizei_t stride, intptr_t offset)
{
  using namespace util;
  constexpr auto typeID     = GLTypeInfo<AttribT>::typeID;
  constexpr auto components = GLTypeInfo<AttribT>::components;
  constexpr auto isIntType  = GLTypeInfo<AttribT>::isIntType;
  constexpr auto parts      = GLTypeInfo<AttribT>::parts;
  constexpr auto partOffset = GLTypeInfo<AttribT>::partOffset;
  for (int i = 0; i < parts; ++i) {
    m_formatData.emplace_back(id.id, typeID, components, isIntType,
                              false, // matrices use floating point types
                              stride, offset + i * partOffset);
  }
}

// allocate(): Prevent compiler generating a template at use site since .cpp file
// contains explicit instantiations
#define EXTERN_ALLOCATE(T) \
  extern template void VertexBuffer::allocate<T>(sizeiptr_t, const T*, Usage, Target) noexcept

// Primatives
EXTERN_ALLOCATE(byte_t);
EXTERN_ALLOCATE(ubyte_t);
EXTERN_ALLOCATE(short_t);
EXTERN_ALLOCATE(ushort_t);
EXTERN_ALLOCATE(int_t);
EXTERN_ALLOCATE(uint_t);
EXTERN_ALLOCATE(float_t);
EXTERN_ALLOCATE(double_t);
// GLM
// vec2
EXTERN_ALLOCATE(glm::vec2);
EXTERN_ALLOCATE(glm::dvec2);
EXTERN_ALLOCATE(glm::ivec2);
EXTERN_ALLOCATE(glm::uvec2);
// vec3
EXTERN_ALLOCATE(glm::vec3);
EXTERN_ALLOCATE(glm::dvec3);
EXTERN_ALLOCATE(glm::ivec3);
EXTERN_ALLOCATE(glm::uvec3);
// vec4
EXTERN_ALLOCATE(glm::vec4);
EXTERN_ALLOCATE(glm::dvec4);
EXTERN_ALLOCATE(glm::ivec4);
EXTERN_ALLOCATE(glm::uvec4);

// mat2
EXTERN_ALLOCATE(glm::mat2);
EXTERN_ALLOCATE(glm::dmat2);
// mat2x3
EXTERN_ALLOCATE(glm::mat2x3);
EXTERN_ALLOCATE(glm::dmat2x3);
// mat2x4
EXTERN_ALLOCATE(glm::mat2x4);
EXTERN_ALLOCATE(glm::dmat2x4);
// mat3
EXTERN_ALLOCATE(glm::mat3);
EXTERN_ALLOCATE(glm::dmat3);
// mat3x2
EXTERN_ALLOCATE(glm::mat3x2);
EXTERN_ALLOCATE(glm::dmat3x2);
// mat3x4
EXTERN_ALLOCATE(glm::mat3x4);
EXTERN_ALLOCATE(glm::dmat3x4);
// mat4
EXTERN_ALLOCATE(glm::mat4);
EXTERN_ALLOCATE(glm::dmat4);
// mat4x2
EXTERN_ALLOCATE(glm::mat4x2);
EXTERN_ALLOCATE(glm::dmat4x2);
// mat4x3
EXTERN_ALLOCATE(glm::mat4x3);
EXTERN_ALLOCATE(glm::dmat4x3);

// quat
EXTERN_ALLOCATE(glm::quat);
EXTERN_ALLOCATE(glm::dquat);
// dualquat
EXTERN_ALLOCATE(glm::dualquat);
EXTERN_ALLOCATE(glm::ddualquat);

#undef EXTERN_ALLOCATE

// update(): Prevent compiler generating a template at use site
#define EXTERN_UPDATE(T) \
  extern template void VertexBuffer::update<T>(intptr_t, std::size_t, const T*, Target) noexcept

// Primatives
EXTERN_UPDATE(byte_t);
EXTERN_UPDATE(ubyte_t);
EXTERN_UPDATE(short_t);
EXTERN_UPDATE(ushort_t);
EXTERN_UPDATE(int_t);
EXTERN_UPDATE(uint_t);
EXTERN_UPDATE(float_t);
EXTERN_UPDATE(double_t);
// GLM
// vec2
EXTERN_UPDATE(glm::vec2);
EXTERN_UPDATE(glm::dvec2);
EXTERN_UPDATE(glm::ivec2);
EXTERN_UPDATE(glm::uvec2);
// vec3
EXTERN_UPDATE(glm::vec3);
EXTERN_UPDATE(glm::dvec3);
EXTERN_UPDATE(glm::ivec3);
EXTERN_UPDATE(glm::uvec3);
// vec4
EXTERN_UPDATE(glm::vec4);
EXTERN_UPDATE(glm::dvec4);
EXTERN_UPDATE(glm::ivec4);
EXTERN_UPDATE(glm::uvec4);

// mat2
EXTERN_UPDATE(glm::mat2);
EXTERN_UPDATE(glm::dmat2);
// mat2x3
EXTERN_UPDATE(glm::mat2x3);
EXTERN_UPDATE(glm::dmat2x3);
// mat2x4
EXTERN_UPDATE(glm::mat2x4);
EXTERN_UPDATE(glm::dmat2x4);
// mat3
EXTERN_UPDATE(glm::mat3);
EXTERN_UPDATE(glm::dmat3);
// mat3x2
EXTERN_UPDATE(glm::mat3x2);
EXTERN_UPDATE(glm::dmat3x2);
// mat3x4
EXTERN_UPDATE(glm::mat3x4);
EXTERN_UPDATE(glm::dmat3x4);
// mat4
EXTERN_UPDATE(glm::mat4);
EXTERN_UPDATE(glm::dmat4);
// mat4x2
EXTERN_UPDATE(glm::mat4x2);
EXTERN_UPDATE(glm::dmat4x2);
// mat4x3
EXTERN_UPDATE(glm::mat4x3);
EXTERN_UPDATE(glm::dmat4x3);

// quat
EXTERN_UPDATE(glm::quat);
EXTERN_UPDATE(glm::dquat);
// dualquat
EXTERN_UPDATE(glm::dualquat);
EXTERN_UPDATE(glm::ddualquat);

#undef EXTERN_UPDATE

// bindToAttributeNonMatrixImpl: Prevent compiler generating a template at use site
extern template void bindToAttributeNonMatrixImpl<true>(const AttributeID&, int_t, enum_t, bool, intptr_t) noexcept;
extern template void bindToAttributeNonMatrixImpl<false>(const AttributeID&, int_t, enum_t, bool, intptr_t) noexcept;

// bindToAttribute(): Prevent compiler generating a template at use site (non-matrix)
// InterleavedFormat::addAttribute(): same
#define EXTERN_ATTRIB1(T) \
  extern template std::enable_if_t<!util::isMatrixType<T>::value> \
  VertexBuffer::bindToAttribute<T>(const AttributeID&, bool, intptr_t) noexcept; \
  extern template std::enable_if_t<!util::isMatrixType<T>::value> \
  VertexBuffer::InterleavedFormat::addAttribute<T>(const AttributeID&, bool, sizei_t, intptr_t)

// Primatives
EXTERN_ATTRIB1(byte_t);
EXTERN_ATTRIB1(ubyte_t);
EXTERN_ATTRIB1(short_t);
EXTERN_ATTRIB1(ushort_t);
EXTERN_ATTRIB1(int_t);
EXTERN_ATTRIB1(uint_t);
EXTERN_ATTRIB1(float_t);
EXTERN_ATTRIB1(double_t);
// GLM
// vec2
EXTERN_ATTRIB1(glm::vec2);
EXTERN_ATTRIB1(glm::dvec2);
EXTERN_ATTRIB1(glm::ivec2);
EXTERN_ATTRIB1(glm::uvec2);
// vec3
EXTERN_ATTRIB1(glm::vec3);
EXTERN_ATTRIB1(glm::dvec3);
EXTERN_ATTRIB1(glm::ivec3);
EXTERN_ATTRIB1(glm::uvec3);
// vec4
EXTERN_ATTRIB1(glm::vec4);
EXTERN_ATTRIB1(glm::dvec4);
EXTERN_ATTRIB1(glm::ivec4);
EXTERN_ATTRIB1(glm::uvec4);

// quat
EXTERN_ATTRIB1(glm::quat);
EXTERN_ATTRIB1(glm::dquat);

#undef EXTERN_ATTRIB1

// bindToAttribute(): Prevent compiler generating a template at use site (non-matrix)
// InterleavedFormat::addAttribute(): same
#define EXTERN_ATTRIB2(T) \
  extern template std::enable_if_t<util::isMatrixType<T>::value> \
  VertexBuffer::bindToAttribute<T>(const AttributeID&, intptr_t) noexcept; \
  extern template std::enable_if_t<util::isMatrixType<T>::value> \
  VertexBuffer::InterleavedFormat::addAttribute<T>(const AttributeID&, sizei_t, intptr_t)

// mat2
EXTERN_ATTRIB2(glm::mat2);
EXTERN_ATTRIB2(glm::dmat2);
// mat2x3
EXTERN_ATTRIB2(glm::mat2x3);
EXTERN_ATTRIB2(glm::dmat2x3);
// mat2x4
EXTERN_ATTRIB2(glm::mat2x4);
EXTERN_ATTRIB2(glm::dmat2x4);
// mat3
EXTERN_ATTRIB2(glm::mat3);
EXTERN_ATTRIB2(glm::dmat3);
// mat3x2
EXTERN_ATTRIB2(glm::mat3x2);
EXTERN_ATTRIB2(glm::dmat3x2);
// mat3x4
EXTERN_ATTRIB2(glm::mat3x4);
EXTERN_ATTRIB2(glm::dmat3x4);
// mat4
EXTERN_ATTRIB2(glm::mat4);
EXTERN_ATTRIB2(glm::dmat4);
// mat4x2
EXTERN_ATTRIB2(glm::mat4x2);
EXTERN_ATTRIB2(glm::dmat4x2);
// mat4x3
EXTERN_ATTRIB2(glm::mat4x3);
EXTERN_ATTRIB2(glm::dmat4x3);

// dualquat
EXTERN_ATTRIB2(glm::dualquat);
EXTERN_ATTRIB2(glm::ddualquat);

#undef EXTERN_ATTRIB2

// mapAs(): Prevent compiler generating a template at use site
#define EXTERN_MAPAS(T) \
  extern template T* VertexBuffer::mapAs<T>(Target, Access) noexcept; \
  extern template T* VertexBuffer::mapAs<T>(intptr_t, std::size_t, Target, Access) noexcept; \
  extern template const T* VertexBuffer::mapAsConst<T>(Target, Access) noexcept; \
  extern template const T* VertexBuffer::mapAsConst<T>(intptr_t, std::size_t, Target, Access) noexcept

// Primatives
EXTERN_MAPAS(byte_t);
EXTERN_MAPAS(ubyte_t);
EXTERN_MAPAS(short_t);
EXTERN_MAPAS(ushort_t);
EXTERN_MAPAS(int_t);
EXTERN_MAPAS(uint_t);
EXTERN_MAPAS(float_t);
EXTERN_MAPAS(double_t);
// GLM
// vec2
EXTERN_MAPAS(glm::vec2);
EXTERN_MAPAS(glm::dvec2);
EXTERN_MAPAS(glm::ivec2);
EXTERN_MAPAS(glm::uvec2);
// vec3
EXTERN_MAPAS(glm::vec3);
EXTERN_MAPAS(glm::dvec3);
EXTERN_MAPAS(glm::ivec3);
EXTERN_MAPAS(glm::uvec3);
// vec4
EXTERN_MAPAS(glm::vec4);
EXTERN_MAPAS(glm::dvec4);
EXTERN_MAPAS(glm::ivec4);
EXTERN_MAPAS(glm::uvec4);

// quat
EXTERN_MAPAS(glm::quat);
EXTERN_MAPAS(glm::dquat);
// dualquat
EXTERN_MAPAS(glm::dualquat);
EXTERN_MAPAS(glm::ddualquat);

// mat2
EXTERN_MAPAS(glm::mat2);
EXTERN_MAPAS(glm::dmat2);
// mat2x3
EXTERN_MAPAS(glm::mat2x3);
EXTERN_MAPAS(glm::dmat2x3);
// mat2x4
EXTERN_MAPAS(glm::mat2x4);
EXTERN_MAPAS(glm::dmat2x4);
// mat3
EXTERN_MAPAS(glm::mat3);
EXTERN_MAPAS(glm::dmat3);
// mat3x2
EXTERN_MAPAS(glm::mat3x2);
EXTERN_MAPAS(glm::dmat3x2);
// mat3x4
EXTERN_MAPAS(glm::mat3x4);
EXTERN_MAPAS(glm::dmat3x4);
// mat4
EXTERN_MAPAS(glm::mat4);
EXTERN_MAPAS(glm::dmat4);
// mat4x2
EXTERN_MAPAS(glm::mat4x2);
EXTERN_MAPAS(glm::dmat4x2);
// mat4x3
EXTERN_MAPAS(glm::mat4x3);
EXTERN_MAPAS(glm::dmat4x3);

#undef EXTERN_MAPAS

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_VERTEXBUFFER_INL
