#define GRAPHICS_GL_GLOBJECT_NO_EXTERN_TEMPLATES
#include "GLObject.inl"

#include "OpenGLIncludes.h"

namespace graphics {
namespace gl {

namespace traits {

#define MAKE_GL_TRAITS_IMPL_(TagT, createFunc, deleteFunc, isFunc) \
auto Traits<TagT>::create() noexcept -> value_type { \
  value_type id = value_type(0); \
  createFunc(1, &id); \
  return id; \
} \
void Traits<TagT>::create(value_type* ids, sizei_t n) noexcept { \
  createFunc(n, ids); \
} \
\
void Traits<TagT>::destroy(value_type id) noexcept { \
  deleteFunc(1, &id); \
} \
void Traits<TagT>::destroy(const value_type* ids, sizei_t n) noexcept { \
  deleteFunc(n, ids); \
} \
\
bool Traits<TagT>::checkID(value_type id) noexcept { \
  return isFunc(id) == GL_TRUE; \
}

#define MAKE_GL_TRAITS_IMPL(TagT, Singular, Plural) \
  MAKE_GL_TRAITS_IMPL_(TagT, glGen##Plural, glDelete##Plural, glIs##Singular)

MAKE_GL_TRAITS_IMPL(VAO, VertexArray, VertexArrays)
MAKE_GL_TRAITS_IMPL(VBO, Buffer, Buffers)
MAKE_GL_TRAITS_IMPL(Texture, Texture, Textures)
MAKE_GL_TRAITS_IMPL(Sampler, Sampler, Samplers)
MAKE_GL_TRAITS_IMPL(RenderBuffer, Renderbuffer, Renderbuffers)
MAKE_GL_TRAITS_IMPL(FrameBuffer, Framebuffer, Framebuffers)
MAKE_GL_TRAITS_IMPL(Query, Query, Queries)

#undef MAKE_GL_TRAITS_IMPL_
#undef MAKE_GL_TRAITS_IMPL

} // namespace traits

#define INST_GLOBJECT(T) \
  template struct traits::Traits<T>; \
  template class GLObject<T>

INST_GLOBJECT(traits::VAO);
INST_GLOBJECT(traits::VBO);
INST_GLOBJECT(traits::Texture);
INST_GLOBJECT(traits::Sampler);
INST_GLOBJECT(traits::RenderBuffer);
INST_GLOBJECT(traits::FrameBuffer);
INST_GLOBJECT(traits::Query);

#undef INST_GLOBJECT

} // namespace gl
} // namespace graphics
