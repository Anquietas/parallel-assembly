#include "ShaderProgram.h"

#include <sstream>
#include <type_traits>

#include <glm/gtc/type_ptr.hpp>

#include "OpenGLIncludes.h"

#include "GLTypeInfo.inl"

#include "../../Assert.h"
#include "../../Exception.h"
#include "../../Logger.h"

namespace graphics {
namespace gl {

namespace {
STATIC_LOGGER_DECLARE(logger)
}

ShaderProgram::ShaderProgram(uint_t id) noexcept
  : m_id(id)
  , m_shaders()
  , m_state((id == 0) ? State::Assembly : State::Linked)
  , m_lastCompiled(nullptr)
{
  ASSERT(m_id == 0 || glIsProgram(m_id) == GL_TRUE);
}
ShaderProgram::ShaderProgram(ShaderProgram && other) noexcept
  : m_id(std::exchange(other.m_id, 0))
  , m_shaders(other.m_shaders)
  , m_state(other.m_state)
  , m_lastCompiled(other.m_lastCompiled)
{
  ASSERT(other.m_shaders.empty());
}
ShaderProgram::~ShaderProgram() {
  if (m_state == State::Error && m_id != 0)
    for (const auto& shader : m_shaders)
      glDetachShader(m_id, shader->id());

  if (m_id != 0)
    glDeleteProgram(m_id);
}

ShaderProgram& ShaderProgram::operator=(ShaderProgram&& other) noexcept {
  if (m_state == State::Error && m_id != 0)
    for (const auto& shader : m_shaders)
      glDetachShader(m_id, shader->id());

  if (m_id != 0)
    glDeleteProgram(m_id);

  m_id = std::exchange(other.m_id, 0);

  m_shaders = std::move(other.m_shaders);
  m_state = other.m_state;
  m_lastCompiled = other.m_lastCompiled;

  return *this;
}

void ShaderProgram::destroy() noexcept {
  if (m_state == State::Error && m_id != 0)
    for (const auto& shader : m_shaders)
      glDetachShader(m_id, shader->id());

  if (m_id != 0) {
    glDeleteProgram(m_id);
    m_id = 0;
  }

  m_shaders.clear();
  m_state = State::Assembly;
  m_lastCompiled = nullptr;
}

void ShaderProgram::addShader(std::shared_ptr<Shader> shader) {
  ASSERT(m_state == State::Assembly);
  m_shaders.emplace_back(std::move(shader));
}
bool ShaderProgram::addShader(const std::string& source,
                              Shader::Type shaderType)
{
  ASSERT(m_state == State::Assembly);
  auto shader = std::make_shared<Shader>();
  m_shaders.push_back(shader);
  bool compileResult = shader->compile(source, shaderType);

  m_lastCompiled = shader.get();
  return compileResult;
}
bool ShaderProgram::addShaderFromFile(const std::string& fileName,
                                      Shader::Type shaderType)
{
  ASSERT(m_state == State::Assembly);
  auto shader = std::make_shared<Shader>();
  m_shaders.push_back(shader);
  bool compileResult = shader->compileFromFile(fileName, shaderType);

  m_lastCompiled = shader.get();
  return compileResult;
}

void ShaderProgram::checkedAddShadersFromFiles(
  const std::string& vertexShaderFileName,
  const std::string& fragmentShaderFileName)
{
  ASSERT(m_state == State::Assembly);
  bool compiled = addShaderFromFile(vertexShaderFileName, Shader::Type::Vertex);
  auto compileLog = getCompileLog();
  if (!compiled) {
    std::ostringstream ss;
    ss << "Failed to compile vertex shader \"" << vertexShaderFileName << "\":\n" << compileLog;
    throw MAKE_EXCEPTION(ss.str());
  } else if (!compileLog.empty())
    LOG_SEVERITY(logger::get(), Warning)
      << "Non-empty compile log for vertex shader \"" << vertexShaderFileName
      << "\":\n" << compileLog;

  compiled = addShaderFromFile(fragmentShaderFileName, Shader::Type::Fragment);
  compileLog = getCompileLog();
  if (!compiled) {
    std::ostringstream ss;
    ss << "Failed to compile fragment shader \"" << fragmentShaderFileName << "\":\n" << compileLog;
    throw MAKE_EXCEPTION(ss.str());
  } else if (!compileLog.empty())
    LOG_SEVERITY(logger::get(), Warning)
      << "Non-empty compile log for fragment shader \"" << fragmentShaderFileName
      << "\":\n" << compileLog;
}

std::string ShaderProgram::getCompileLog() const {
  if (m_lastCompiled)
    return m_lastCompiled->getCompileLog();
  else
    return "";
}
void ShaderProgram::dumpShaders() noexcept {
  m_shaders.clear();
  m_lastCompiled = nullptr;
}

void ShaderProgram::createProgramObject() {
  ASSERT(m_id == 0 && m_state == State::Assembly);
  m_id = glCreateProgram();
  if (m_id == 0) {
    m_state = State::Error;
    throw MAKE_EXCEPTION("Could not created shader program object");
  }
}

void ShaderProgram::bindAttributeLocation(const char* name, const AttributeID& id) noexcept {
  ASSERT(m_id != 0 && m_state == State::Assembly);
  glBindAttribLocation(m_id, id.id, name);
}

namespace {
inline bool checkLink(uint_t id) noexcept {
  GLint linkStatus = GL_FALSE;
  glGetProgramiv(id, GL_LINK_STATUS, &linkStatus);
  return linkStatus == GL_TRUE;
}
}

bool ShaderProgram::link(bool makeRetrievable, bool makeSeparable) {
  ASSERT(m_state == State::Assembly);
  if (m_id == 0)
    createProgramObject();

  for (const auto& shader : m_shaders) {
    ASSERT(shader->isCompiled());
    glAttachShader(m_id, shader->id());
  }
  if (makeRetrievable)
    glProgramParameteri(m_id, GL_PROGRAM_BINARY_RETRIEVABLE_HINT, GL_TRUE);
  if (makeSeparable)
    glProgramParameteri(m_id, GL_PROGRAM_SEPARABLE, GL_TRUE);
  glLinkProgram(m_id);

  if (!checkLink(m_id)) {
    m_state = State::Error;
    // Deliberately leave program allocated - it stores the error log
    return false;
  }
  m_state = State::Linked;
  // Don't need to keep the components attached after linking
  for (const auto& shader : m_shaders)
    glDetachShader(m_id, shader->id());
  // Leave shaders alone incase the program needs
  // to be reconfigured.
  return true;
}

void ShaderProgram::checkedLink(
  const std::string& name,
  bool makeRetrievable,
  bool makeSeparable)
{
  ASSERT(m_state == State::Assembly);
  
  bool linked = link(makeRetrievable, makeSeparable);
  auto linkLog = getLinkLog();
  if (!linked) {
    std::ostringstream ss;
    ss << "Failed to link shader " << name << ":\n" << linkLog;
    throw MAKE_EXCEPTION(ss.str());
  } else if (!linkLog.empty())
    LOG_SEVERITY(logger::get(), Warning)
      << "Non-empty link log for shader " << name << ":\n" << linkLog;
}

std::pair<std::vector<ubyte_t>, enum_t>
    ShaderProgram::getBinary() const
{
  ASSERT(GLEW_ARB_get_program_binary);
  ASSERT(m_state == State::Linked);
  GLint binaryLength = 0;
  glGetProgramiv(m_id, GL_PROGRAM_BINARY_LENGTH, &binaryLength);

  // Safe
  std::vector<ubyte_t> binary(binaryLength, ubyte_t(0));
  GLsizei binarySize = 0;
  GLenum formatCode = GL_NONE;
  // gl* are C functions and therefore can't throw exceptions :)
  glGetProgramBinary(m_id, binaryLength, &binarySize, &formatCode, binary.data());
  // unique_ptr doesn't help with this if it fails...
  ASSERT(binarySize == binaryLength);

  return {std::move(binary), static_cast<enum_t>(formatCode)};
}

bool ShaderProgram::useBinary(const std::vector<ubyte_t>& binary, enum_t formatCode) {
  if (GLEW_ARB_get_program_binary) {
    ASSERT(m_state == State::Assembly);
    if (m_id == 0)
      createProgramObject();

    glProgramBinary(m_id, formatCode, binary.data(), static_cast<sizei_t>(binary.size()));
    if (checkLink(m_id)) {
      m_state = State::Linked;
      return true;
    } else {
      glDeleteProgram(m_id);
      m_id = 0;
      // Don't set state = Error for convenience of loading
      // the shaders from source instead.
      return false;
    }
  } else
    return false;
}

std::string ShaderProgram::getLinkLog() const {
  ASSERT(m_state != State::Assembly);

  // Query log size
  GLint logSize = 0;
  glGetProgramiv(m_id, GL_INFO_LOG_LENGTH, &logSize);
  ASSERT(logSize >= 0);

  std::unique_ptr<char[]> logBuffer {new char[logSize]};
  GLsizei logLength = 0;
  glGetProgramInfoLog(m_id, logSize, &logLength, logBuffer.get());
  ASSERT((logLength == 0 && logSize == 0) || logLength == logSize - 1);

  return std::string(logBuffer.get(), logLength);
}

void ShaderProgram::clearError() noexcept {
  // Last error was a linker error
  if (m_state == State::Error) {
    m_state = State::Assembly;
    if (m_id != 0)
      glDeleteProgram(m_id);
  // Last error was a compiler error
  } else
    // If false, then trivially no error occured - do nothing
    if (m_lastCompiled)
      // Does nothing if no error occured
      m_lastCompiled->clearError();
}

int_t ShaderProgram::activeAttributes() const noexcept {
  ASSERT(m_state == State::Linked);
  GLint activeAttributes;
  glGetProgramiv(m_id, GL_ACTIVE_ATTRIBUTES, &activeAttributes);
  return activeAttributes;
}
int_t ShaderProgram::activeUniforms() const noexcept {
  ASSERT(m_state == State::Linked);
  GLint activeUniforms;
  glGetProgramiv(m_id, GL_ACTIVE_UNIFORMS, &activeUniforms);
  return activeUniforms;
}

AttributeID ShaderProgram::getAttributeLocation(const char *name) const noexcept {
  // glGetAttribLocation returns GLint, but all uses of the location
  // expect GLuint. \o/ design flaws
  return AttributeID(static_cast<uint_t>(glGetAttribLocation(m_id, name)));
}
UniformID ShaderProgram::getUniformLocation(const char *name) const noexcept {
  // Same as for glGetAttribLocation
  return UniformID(static_cast<uint_t>(glGetUniformLocation(m_id, name)));
}

void ShaderProgram::bind() const noexcept {
  if (m_state == State::Linked)
    glUseProgram(m_id);
}
void ShaderProgram::unbind() const noexcept {
  if (m_state == State::Linked)
    glUseProgram(0);
}

// Scalar / Vector types (incl. quaternions)
// 1-component single values
template <>
void ShaderProgram::setUniform<int_t>(const UniformID& id,
                                      const int_t& value) noexcept
{
  glUniform1i(id.id, value);
}
template <>
void ShaderProgram::setUniform<uint_t>(const UniformID& id,
                                       const uint_t& value) noexcept
{
  glUniform1ui(id.id, value);
}
template <>
void ShaderProgram::setUniform<float_t>(const UniformID& id,
                                        const float_t& value) noexcept
{
  glUniform1f(id.id, value);
}
template <>
void ShaderProgram::setUniform<TextureUnit>(const UniformID& id,
                                            const TextureUnit& texUnit) noexcept
{
  // Yay, more dodgy type choices for OpenGL APIs
  glUniform1i(id.id, static_cast<int_t>(texUnit.id));
}
// 2-component single values
template <>
void ShaderProgram::setUniform<glm::ivec2>(const UniformID& id,
                                           const glm::ivec2& value) noexcept
{
  glUniform2iv(id.id, 1, glm::value_ptr(value));
}
template <>
void ShaderProgram::setUniform<glm::uvec2>(const UniformID& id,
                                           const glm::uvec2& value) noexcept
{
  glUniform2uiv(id.id, 1, glm::value_ptr(value));
}
template <>
void ShaderProgram::setUniform<glm::vec2>(const UniformID& id,
                                          const glm::vec2& value) noexcept
{
  glUniform2fv(id.id, 1, glm::value_ptr(value));
}
// 3-component single values
template <>
void ShaderProgram::setUniform<glm::ivec3>(const UniformID& id,
                                           const glm::ivec3& value) noexcept
{
  glUniform3iv(id.id, 1, glm::value_ptr(value));
}
template <>
void ShaderProgram::setUniform<glm::uvec3>(const UniformID& id,
                                           const glm::uvec3& value) noexcept
{
  glUniform3uiv(id.id, 1, glm::value_ptr(value));
}
template <>
void ShaderProgram::setUniform<glm::vec3>(const UniformID& id,
                                          const glm::vec3& value) noexcept
{
  glUniform3fv(id.id, 1, glm::value_ptr(value));
}
// 4-component single values
template <>
void ShaderProgram::setUniform<glm::ivec4>(const UniformID& id,
                                           const glm::ivec4& value) noexcept
{
  glUniform4iv(id.id, 1, glm::value_ptr(value));
}
template <>
void ShaderProgram::setUniform<glm::uvec4>(const UniformID& id,
                                           const glm::uvec4& value) noexcept
{
  glUniform4uiv(id.id, 1, glm::value_ptr(value));
}
template <>
void ShaderProgram::setUniform<glm::vec4>(const UniformID& id,
                                          const glm::vec4& value) noexcept
{
  glUniform4fv(id.id, 1, glm::value_ptr(value));
}
template <>
void ShaderProgram::setUniform<glm::quat>(const UniformID& id,
                                          const glm::quat& value) noexcept
{
  glUniform4fv(id.id, 1, glm::value_ptr(value));
}

// 1-component arrays
template <>
void ShaderProgram::setUniform<int_t>(const UniformID& id,
                                      const int_t* values,
                                      sizei_t length) noexcept
{
  glUniform1iv(id.id, length, values);
}
template <>
void ShaderProgram::setUniform<uint_t>(const UniformID& id,
                                       const uint_t* values,
                                       sizei_t length) noexcept
{
  glUniform1uiv(id.id, length, values);
}
template <>
void ShaderProgram::setUniform<float_t>(const UniformID& id,
                                        const float_t* values,
                                        sizei_t length) noexcept
{
  glUniform1fv(id.id, length, values);
}
template <>
void ShaderProgram::setUniform<TextureUnit>(const UniformID& id,
                                            const TextureUnit* texUnits,
                                            sizei_t length) noexcept
{
  static_assert(sizeof(TextureUnit) == sizeof(int_t),
                "Size discrepancy between TextureUnit and type passed to glUniform1i (int_t)");
  // Yay, more dodgy type choices for OpenGL APIs
  glUniform1iv(id.id, length, reinterpret_cast<const int_t*>(texUnits));
}
// 2-component arrays
template <>
void ShaderProgram::setUniform<glm::ivec2>(const UniformID& id,
                                           const glm::ivec2* values,
                                           sizei_t length) noexcept
{
  static_assert(sizeof(glm::ivec2) == 2 * sizeof(int_t), "DERP!");
  glUniform2iv(id.id, length, reinterpret_cast<const int_t*>(values));
}
template <>
void ShaderProgram::setUniform<glm::uvec2>(const UniformID& id,
                                           const glm::uvec2* values,
                                           sizei_t length) noexcept
{
  static_assert(sizeof(glm::uvec2) == 2 * sizeof(uint_t), "DERP!");
  glUniform2uiv(id.id, length, reinterpret_cast<const uint_t*>(values));
}
template <>
void ShaderProgram::setUniform<glm::vec2>(const UniformID& id,
                                          const glm::vec2* values,
                                          sizei_t length) noexcept
{
  static_assert(sizeof(glm::vec2) == 2 * sizeof(float_t), "DERP!");
  glUniform2fv(id.id, length, reinterpret_cast<const float_t*>(values));
}
// 3-component arrays
template <>
void ShaderProgram::setUniform<glm::ivec3>(const UniformID& id,
                                           const glm::ivec3* values,
                                           sizei_t length) noexcept
{
  static_assert(sizeof(glm::ivec3) == 3 * sizeof(int_t), "DERP!");
  glUniform3iv(id.id, length, reinterpret_cast<const int_t*>(values));
}
template <>
void ShaderProgram::setUniform<glm::uvec3>(const UniformID& id,
                                           const glm::uvec3* values,
                                           sizei_t length) noexcept
{
  static_assert(sizeof(glm::uvec3) == 3 * sizeof(uint_t), "DERP!");
  glUniform3uiv(id.id, length, reinterpret_cast<const uint_t*>(values));
}
template <>
void ShaderProgram::setUniform<glm::vec3>(const UniformID& id,
                                          const glm::vec3* values,
                                          sizei_t length) noexcept
{
  static_assert(sizeof(glm::vec3) == 3 * sizeof(float_t), "DERP!");
  glUniform3fv(id.id, length, reinterpret_cast<const float_t*>(values));
}
// 4-component arrays
template <>
void ShaderProgram::setUniform<glm::ivec4>(const UniformID& id,
                                           const glm::ivec4* values,
                                           sizei_t length) noexcept
{
  static_assert(sizeof(glm::ivec4) == 4 * sizeof(int_t), "DERP!");
  glUniform4iv(id.id, length, reinterpret_cast<const int_t*>(values));
}
template <>
void ShaderProgram::setUniform<glm::uvec4>(const UniformID& id,
                                           const glm::uvec4* values,
                                           sizei_t length) noexcept
{
  static_assert(sizeof(glm::uvec4) == 4 * sizeof(uint_t), "DERP!");
  glUniform4uiv(id.id, length, reinterpret_cast<const uint_t*>(values));
}
template <>
void ShaderProgram::setUniform<glm::vec4>(const UniformID& id,
                                          const glm::vec4* values,
                                          sizei_t length) noexcept
{
  static_assert(sizeof(glm::vec4) == 4 * sizeof(float_t), "DERP!");
  glUniform4fv(id.id, length, reinterpret_cast<const float_t*>(values));
}
template <>
void ShaderProgram::setUniform<glm::quat>(const UniformID& id,
                                          const glm::quat* values,
                                          sizei_t length) noexcept
{
  static_assert(sizeof(glm::quat) == 4 * sizeof(float_t), "DERP!");
  glUniform4fv(id.id, length, reinterpret_cast<const float_t*>(values));
}

// Matrix types (incl. dual quaternions)
// single values
template <>
void ShaderProgram::setUniform<glm::mat2>(const UniformID& id,
                                          const glm::mat2& value) noexcept
{
  glUniformMatrix2fv(id.id, 1, GL_FALSE, glm::value_ptr(value));
}
template <>
void ShaderProgram::setUniform<glm::mat2x3>(const UniformID& id,
                                            const glm::mat2x3& value) noexcept
{
  glUniformMatrix2x3fv(id.id, 1, GL_FALSE, glm::value_ptr(value));
}
template <>
void ShaderProgram::setUniform<glm::mat2x4>(const UniformID& id,
                                            const glm::mat2x4& value) noexcept
{
  glUniformMatrix2x4fv(id.id, 1, GL_FALSE, glm::value_ptr(value));
}
template <>
void ShaderProgram::setUniform<glm::mat3>(const UniformID& id,
                                          const glm::mat3& value) noexcept
{
  glUniformMatrix3fv(id.id, 1, GL_FALSE, glm::value_ptr(value));
}
template <>
void ShaderProgram::setUniform<glm::mat3x2>(const UniformID& id,
                                            const glm::mat3x2& value) noexcept
{
  glUniformMatrix3x2fv(id.id, 1, GL_FALSE, glm::value_ptr(value));
}
template <>
void ShaderProgram::setUniform<glm::mat3x4>(const UniformID& id,
                                            const glm::mat3x4& value) noexcept
{
  glUniformMatrix3x4fv(id.id, 1, GL_FALSE, glm::value_ptr(value));
}
template <>
void ShaderProgram::setUniform<glm::mat4>(const UniformID& id,
                                          const glm::mat4& value) noexcept
{
  glUniformMatrix4fv(id.id, 1, GL_FALSE, glm::value_ptr(value));
}
template <>
void ShaderProgram::setUniform<glm::mat4x2>(const UniformID& id,
                                            const glm::mat4x2& value) noexcept
{
  glUniformMatrix4x2fv(id.id, 1, GL_FALSE, glm::value_ptr(value));
}
template <>
void ShaderProgram::setUniform<glm::mat4x3>(const UniformID& id,
                                            const glm::mat4x3& value) noexcept
{
  glUniformMatrix4x3fv(id.id, 1, GL_FALSE, glm::value_ptr(value));
}
template <>
void ShaderProgram::setUniform<glm::dualquat>(const UniformID& id,
                                              const glm::dualquat& value) noexcept
{
  static_assert(sizeof(glm::dualquat) == 8 * sizeof(float_t), "DERP!");
  glUniformMatrix2x4fv(id.id, 1, GL_FALSE,
                       reinterpret_cast<const float_t*>(&value));
}
// arrays
template <>
void ShaderProgram::setUniform<glm::mat2>(const UniformID& id,
                                          const glm::mat2* values,
                                          sizei_t length) noexcept
{
  static_assert(sizeof(glm::mat2) == 4 * sizeof(float_t), "DERP!");
  glUniformMatrix2fv(id.id, length, GL_FALSE,
                     reinterpret_cast<const float_t*>(values));
}
template <>
void ShaderProgram::setUniform<glm::mat2x3>(const UniformID& id,
                                            const glm::mat2x3* values,
                                            sizei_t length) noexcept
{
  static_assert(sizeof(glm::mat2x3) == 6 * sizeof(float_t), "DERP!");
  glUniformMatrix2x3fv(id.id, length, GL_FALSE,
                       reinterpret_cast<const float_t*>(values));
}
template <>
void ShaderProgram::setUniform<glm::mat2x4>(const UniformID& id,
                                            const glm::mat2x4* values,
                                            sizei_t length) noexcept
{
  static_assert(sizeof(glm::mat2x4) == 8 * sizeof(float_t), "DERP!");
  glUniformMatrix2x4fv(id.id, length, GL_FALSE,
                       reinterpret_cast<const float_t*>(values));
}
template <>
void ShaderProgram::setUniform<glm::mat3>(const UniformID& id,
                                          const glm::mat3* values,
                                          sizei_t length) noexcept
{
  static_assert(sizeof(glm::mat3) == 9 * sizeof(float_t), "DERP!");
  glUniformMatrix3fv(id.id, length, GL_FALSE,
                     reinterpret_cast<const float_t*>(values));
}
template <>
void ShaderProgram::setUniform<glm::mat3x2>(const UniformID& id,
                                            const glm::mat3x2* values,
                                            sizei_t length) noexcept
{
  static_assert(sizeof(glm::mat3x2) == 6 * sizeof(float_t), "DERP!");
  glUniformMatrix3x2fv(id.id, length, GL_FALSE,
                       reinterpret_cast<const float_t*>(values));
}
template <>
void ShaderProgram::setUniform<glm::mat3x4>(const UniformID& id,
                                            const glm::mat3x4* values,
                                            sizei_t length) noexcept
{
  static_assert(sizeof(glm::mat3x4) == 12 * sizeof(float_t), "DERP!");
  glUniformMatrix3x4fv(id.id, length, GL_FALSE,
                       reinterpret_cast<const float_t*>(values));
}
template <>
void ShaderProgram::setUniform<glm::mat4>(const UniformID& id,
                                          const glm::mat4* values,
                                          sizei_t length) noexcept
{
  static_assert(sizeof(glm::mat4) == 16 * sizeof(float_t), "DERP!");
  glUniformMatrix4fv(id.id, length, GL_FALSE,
                     reinterpret_cast<const float_t*>(values));
}
template <>
void ShaderProgram::setUniform<glm::mat4x2>(const UniformID& id,
                                            const glm::mat4x2* values,
                                            sizei_t length) noexcept
{
  static_assert(sizeof(glm::mat4x2) == 8 * sizeof(float_t), "DERP!");
  glUniformMatrix4x2fv(id.id, length, GL_FALSE,
                       reinterpret_cast<const float_t*>(values));
}
template <>
void ShaderProgram::setUniform<glm::mat4x3>(const UniformID& id,
                                            const glm::mat4x3* values,
                                            sizei_t length) noexcept
{
  static_assert(sizeof(glm::mat4x3) == 12 * sizeof(float_t), "DERP!");
  glUniformMatrix4x3fv(id.id, length, GL_FALSE,
                       reinterpret_cast<const float_t*>(values));
}
template <>
void ShaderProgram::setUniform<glm::dualquat>(const UniformID& id,
                                              const glm::dualquat* values,
                                              sizei_t length) noexcept
{
  static_assert(sizeof(glm::dualquat) == 8 * sizeof(float_t), "DERP!");
  glUniformMatrix2x4fv(id.id, length, GL_FALSE,
                       reinterpret_cast<const float_t*>(values));
}

namespace {
STATIC_LOGGER_INIT(logger) {
  logging::Logger lg;
  logging::setModuleName(lg, "gl::ShaderProgram");
  return lg;
}
}

} // namespace gl
} // namespace graphics
