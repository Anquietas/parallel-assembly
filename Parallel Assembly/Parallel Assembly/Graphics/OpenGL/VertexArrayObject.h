#ifndef GRAPHICS_GL_VERTEXARRAYOBJECT_H
#define GRAPHICS_GL_VERTEXARRAYOBJECT_H

#include "AttributeID.h"
#include "GLObject.h"

namespace graphics {
namespace gl {

class VertexArrayObject final : public GLObject<traits::VAO> {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(VertexArrayObject, GLObject)

  // Binds the VAO.
  // Note: this will unbind any previously bound VAO
  void bind() noexcept;
  // Unbinds the currently bound VAO
  static void unbind() noexcept;

  // Enables the given attribute in the VAO for rendering
  void enableAttribute(const AttributeID& id) noexcept;
  // Disables the given attribute in the VAO for rendering
  void disableAttribute(const AttributeID& id) noexcept;

  // Sets the divisor used for the attribute for instanced rendering
  // - A divisor of 0 indicates the attribute is per-vertex rather than per-instance
  void setAttributeDivisor(const AttributeID& id, uint_t divisor) noexcept;
};

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_VERTEXARRAYOBJECT_H
