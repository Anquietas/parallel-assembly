#define GRAPHICS_GL_INDEXBUFFER_NO_EXTERN_TEMPLATES
#include "IndexBuffer.inl"

#include <algorithm>

#include "../../Assert.h"

namespace graphics {
namespace gl {
namespace detail {

GLenum bindTargetCode(IndexBuffer::Target target) noexcept {
  GLenum bindTargets[] = {
    GL_ELEMENT_ARRAY_BUFFER,
    GL_COPY_READ_BUFFER,
    GL_COPY_WRITE_BUFFER,
  };
  auto targetBase = static_cast<std::underlying_type_t<IndexBuffer::Target>>(target);
  ASSERT(0 <= targetBase && targetBase < std::extent<decltype(bindTargets)>::value);
  return bindTargets[targetBase];
}

GLenum bufferUsageCode(IndexBuffer::Usage usage) noexcept {
  GLenum bufferUsages[] = {
    GL_STATIC_DRAW,
    GL_STATIC_READ,
    GL_STATIC_COPY,
    GL_DYNAMIC_DRAW,
    GL_DYNAMIC_READ,
    GL_DYNAMIC_COPY,
    GL_STREAM_DRAW,
    GL_STREAM_READ,
    GL_STREAM_COPY,
  };
  auto usageBase = static_cast<std::underlying_type_t<IndexBuffer::Target>>(usage);
  ASSERT(0 <= usageBase && usageBase < std::extent<decltype(bufferUsages)>::value);
  return bufferUsages[usageBase];
}

GLenum bufferAccessCode(IndexBuffer::Access access) noexcept {
  GLenum bufferAccessCodes[] = {
    GL_READ_ONLY,
    GL_WRITE_ONLY,
    GL_READ_WRITE,
  };
  auto accessBase = static_cast<std::underlying_type_t<IndexBuffer::Access>>(access);
  ASSERT(0 <= accessBase && accessBase < std::extent<decltype(bufferAccessCodes)>::value);
  return bufferAccessCodes[accessBase];
}
GLbitfield bufferAccessBitfield(IndexBuffer::Access access) noexcept {
  GLbitfield bitmasks[] = {
    GL_MAP_READ_BIT,
    GL_MAP_WRITE_BIT,
    GL_MAP_READ_BIT | GL_MAP_WRITE_BIT,
  };
  auto accessBase = static_cast<std::underlying_type_t<IndexBuffer::Access>>(access);
  ASSERT(0 <= accessBase && accessBase < std::extent<decltype(bitmasks)>::value);
  return bitmasks[accessBase];
}

} // namespace detail

void IndexBuffer::bind(Target target) noexcept {
  ASSERT(m_id != 0);
  glBindBuffer(detail::bindTargetCode(target), m_id);
}
void IndexBuffer::unbind(Target target) noexcept {
  glBindBuffer(detail::bindTargetCode(target), 0);
}

void IndexBuffer::allocate(sizeiptr_t size, Usage usage, Target target) noexcept {
  glBufferData(detail::bindTargetCode(target),
               size, nullptr,
               detail::bufferUsageCode(usage));
}

void IndexBuffer::unmap(Target target) noexcept {
  glUnmapBuffer(detail::bindTargetCode(target));
}

// allocate(): Force instantiation of known types
#define INST_ALLOCATE(T) \
  template std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value> \
  IndexBuffer::allocate<T>(sizeiptr_t, const T*, Usage, Target) noexcept

// Primatives
INST_ALLOCATE(ubyte_t);
INST_ALLOCATE(ushort_t);
INST_ALLOCATE(uint_t);

#undef INST_ALLOCATE

// update(): Force instantiation of known types
#define INST_UPDATE(T) \
  template std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value> \
  IndexBuffer::update<T>(intptr_t, std::size_t, const T*, Target) noexcept

// Primative
INST_UPDATE(ubyte_t);
INST_UPDATE(ushort_t);
INST_UPDATE(uint_t);

#undef INST_UPDATE

// mapAs(): Force instantiation of known types
#define INST_MAPAS(T) \
  template std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value, T*> \
  IndexBuffer::mapAs<T>(Target, Access) noexcept; \
  template std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value, T*> \
  IndexBuffer::mapAs<T>(intptr_t, std::size_t, Target, Access) noexcept; \
  template std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value, const T*> \
  IndexBuffer::mapAsConst<T>(Target, Access) noexcept; \
  template std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value, const T*> \
  IndexBuffer::mapAsConst<T>(intptr_t, std::size_t, Target, Access) noexcept

// Primatives
INST_MAPAS(ubyte_t);
INST_MAPAS(ushort_t);
INST_MAPAS(uint_t);

#undef INST_MAPAS

} // namespace gl
} // namespace graphics
