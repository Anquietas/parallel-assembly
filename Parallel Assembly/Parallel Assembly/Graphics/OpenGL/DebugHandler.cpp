#include "DebugHandler.h"

#include <iostream>
#include <mutex>
#include <sstream>

#include <SDL.h>

#include "OpenGLIncludes.h"

#include "../../Assert.h"
#include "../../Logger.h"

namespace graphics {
namespace gl {

extern "C"
void APIENTRY debugHandler(GLenum source,
                           GLenum type,
                           GLuint id,
                           GLenum severity,
                           GLsizei length,
                           const GLchar* message,
                           const void* userParam);

void setupDebugHandler() {
  logging::Logger logger;
  logging::setModuleName(logger, "SetupDebugHandler");

  if (GLEW_KHR_debug) {
    LOG_SEVERITY(logger, Debug) << "GL_KHR_debug is supported on this platform, enabling debug handler";

    glDebugMessageCallback(debugHandler, nullptr);

    // Useful for debugging but comes with a severe performance penalty
    //glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    // Causes ALL messages to be generated, including during render loop
    /*glDebugMessageControl(GL_DONT_CARE,
                            GL_DONT_CARE,
                            GL_DONT_CARE,
                            0,
                            nullptr,
                            GL_TRUE);*/
  } else
    LOG_SEVERITY(logger, Warning) << "GL_KHR_debug is not supported on this platform";
}

extern "C"
void APIENTRY debugHandler(GLenum source,
                           GLenum type,
                           GLuint id,
                           GLenum severity,
                           GLsizei length,
                           const GLchar* message,
                           const void* /*userParam*/)
{
  try {
    static logging::Logger logger;
    static std::once_flag loggerFlag;
    std::call_once(loggerFlag, []() { logging::setModuleName(logger, "OpenGL"); });

    std::ostringstream ss;
    ss << "Debug handler called with:\nsource = ";
    switch (source) {
    case GL_DEBUG_SOURCE_API:
      ss << "API\n";
      break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
      ss << "Window System\n";
      break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER:
      ss << "Shader Compiler\n";
      break;
    case GL_DEBUG_SOURCE_THIRD_PARTY:
      ss << "Third Party\n";
      break;
    case GL_DEBUG_SOURCE_APPLICATION:
      ss << "Application\n";
      break;
    case GL_DEBUG_SOURCE_OTHER:
      ss << "Other\n";
      break;
    default:
      ss << "<unknown>\n";
      ASSERT_MSG(false, "Unhandled OpenGL debug source");
    }

    ss << "type = ";
    switch (type) {
    case GL_DEBUG_TYPE_ERROR:
      ss << "Error\n";
      break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
      ss << "Deprecated Behaviour\n";
      break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
      ss << "Undefined Behaviour\n";
      break;
    case GL_DEBUG_TYPE_PORTABILITY:
      ss << "Portability\n";
      break;
    case GL_DEBUG_TYPE_PERFORMANCE:
      ss << "Performance\n";
      break;
    case GL_DEBUG_TYPE_OTHER:
      ss << "Other\n";
      break;
    case GL_DEBUG_TYPE_MARKER:
      ss << "Marker\n";
      break;
    case GL_DEBUG_TYPE_PUSH_GROUP:
      ss << "Push Group\n";
      break;
    case GL_DEBUG_TYPE_POP_GROUP:
      ss << "Pop Group\n";
      break;
    default:
      ss << "<unknown>\n";
      ASSERT_MSG(false, "Unhandled OpenGL debug type");
    }

    ss << "id = " << id << '\n';

    ss << "severity = ";
    switch (severity) {
    case GL_DEBUG_SEVERITY_HIGH:
      ss << "High\n";
      break;
    case GL_DEBUG_SEVERITY_MEDIUM:
      ss << "Medium\n";
      break;
    case GL_DEBUG_SEVERITY_LOW:
      ss << "Low\n";
      break;
    case GL_DEBUG_SEVERITY_NOTIFICATION:
      ss << "Notification\n";
      break;
    default:
      ss << "<unknown>\n";
      ASSERT_MSG(false, "Unhandled OpenGL debug severity");
    }

    ss << "message = \"";
    ss.write(message, static_cast<std::streamsize>(length)) << "\"";

    if (severity == GL_DEBUG_SEVERITY_NOTIFICATION)
      LOG_SEVERITY(logger, Debug) << ss.str();
    else {
      switch (type) {
      case GL_DEBUG_TYPE_ERROR:
      case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
        LOG_SEVERITY(logger, Error) << ss.str();
        break;
      case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
      case GL_DEBUG_TYPE_PORTABILITY:
      case GL_DEBUG_TYPE_PERFORMANCE:
        LOG_SEVERITY(logger, Warning) << ss.str();
        break;
      default:
        LOG_SEVERITY(logger, Info) << ss.str();
      }
    }
  } catch (const std::exception& e) {
    if (SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
                                 "Exception in OpenGL debug handler",
                                 e.what(), nullptr))
      std::cerr << "Exception thrown in OpenGL debug handler: " << e.what() << std::endl;
    std::exit(EXIT_FAILURE);
  } catch (...) {
    if (SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
                                 "Exception in OpenGL Debug handler",
                                 "Unknown error", nullptr))
      std::cerr << "Unknown exception thrown in OpenGL debug handler\n";
    std::exit(EXIT_FAILURE);
  }
}

} // namespace gl
} // namespace graphics
