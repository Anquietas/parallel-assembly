#pragma once
#ifndef GRAPHICS_GL_SHADER_H
#define GRAPHICS_GL_SHADER_H

#include "Types.h"
#include <string>

namespace graphics {
namespace gl {

// Models an OpenGL shader object
// - Note: not thread safe
class Shader final {
  uint_t m_id;

  enum class State {
    Uninitialized,
    Compiled,
    Error
  };
  State m_state;

public:
  enum class Type : enum_t {
    Vertex,// = GL_VERTEX_SHADER,
    Fragment,// = GL_FRAGMENT_SHADER,
    Geometry,// = GL_GEOMETRY_SHADER,
  };

  // Creates an empty shader if id = 0, and assumes the shader
  // has already been compiled if given a non-zero id
  Shader(uint_t id = 0) noexcept;
  Shader(const Shader&) = delete;
  Shader(Shader&& other) noexcept;
  ~Shader();

  // Converts a Type object to a string describing it in plain
  // english (Type::Vertex -> "vertex shader" etc)
  // - Doesn't require localization (only devs should see it)
  static const char* typeString(Type shaderType) noexcept;

  Shader& operator=(const Shader&) = delete;
  Shader& operator=(Shader&& other) noexcept;

  // Used to compile a shader from its source code, after
  // being constructed with the default constructor
  // - Note: do not call this on a compiled shader!
  // - Returns true if the shader compiled successfully, false otherwise
  // - Throws std::runtime_error on failure to create the
  //   shader object
  bool compile(const std::string& source, Type shaderType);
  // Used to compile a shader from its source file, after
  // being constructed with the default constructor
  // - Note: do not call this on a compiled shader!
  // - Returns true if the shader compiled successfully, false otherwise
  // - Throws std::runtime_error if the file does not exist
  // - Throws std::runtime_error on failure to create the
  //   shader object
  bool compileFromFile(const std::string& fileName, Type shaderType);

  inline bool isCompiled() const noexcept { return m_state == State::Compiled; }
  inline bool error() const noexcept { return m_state == State::Error; }

  // Clears error state allowing for object reuse
  // - This will also erase any compile logs
  // - Does nothing if no error occurred
  void clearError() noexcept;

  // Retrieves the compilation log for the shader
  // - Note: this should be called *immediately* after compile()
  // - Do not call this on an uninitialized shader!
  std::string getCompileLog() const;

  inline uint_t id() const { return m_id; }
};

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_SHADER_H
