#pragma once
#ifndef GRAPHICS_GL_TEXTURE_LOADER_H
#define GRAPHICS_GL_TEXTURE_LOADER_H

#include <vector>
#include <string>

#include "Texture2D.h"
#include "Texture2DArray.h"
#include "../../GLMIncludes.h"

namespace graphics {
namespace gl {

// Loads a texture from the specified file, if it exists, and stores it in
// the given texture. This assumes the texture is already bound.
// If the file doesn't exist or is invalid, throws an exception
// Returns the dimensions of the image loaded
glm::ivec2 loadTexture2D(const std::string& filePath, ColourTexture2D& texture);

// Loads a texture form the specified file, if it exists, and stores it in the
// given texture array at the specified index. This assumes the texture is already
// bound and allocated, and that index is within the bounds of the teture array.
// Also assumes the image file is small enough to fit in the texture array
// If the file doesn't exist or is invalid, throws an exception
// Returns the dimensions of the image loaded
glm::ivec2 loadTexture2D(const std::string& filePath, ColourTexture2DArray& texArray, int_t index);

// Loads an entire texture array from the specified list of files, if they exist,
// and stores them in the given texture array. This assumes the texture is already bound.
// If any files don't exist or are invalid, throws an exception. This will leave
// the texture array in an undefined state and should be reallocated before further use.
// Returns a vector containing the texture coordinates of the bottom right corner
// of each image.
std::vector<glm::vec2> loadTexture2DArray(const std::vector<std::string>& filePaths,
                                           sizei_t width, sizei_t height,
                                           ColourTexture2DArray& texArray);

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_TEXTURE_LOADER_H
