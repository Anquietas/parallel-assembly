#pragma once
#ifndef GRAPHICS_GL_SYSTEM_INFO_H
#define GRAPHICS_GL_SYSTEM_INFO_H

#include "Types.h"

namespace graphics {
namespace gl {

struct SystemInfo final {
  SystemInfo() = delete;  // non-instantiable

  // GL_ALIASED_LINE_WIDTH_RANGE
  static float_t minAliasedLineWidth() { return minAliasedLineWidth_; }
  static float_t maxAliasedLineWidth() { return maxAliasedLineWidth_; }
  // GL_SMOOTH_LINE_WIDTH_RANGE
  static float_t minSmoothLineWidth() { return minSmoothLineWidth_; }
  static float_t maxSmoothLineWidth() { return maxSmoothLineWidth_; }
  // GL_SMOOTH_LINE_WIDTH_GRANULARITY
  static float_t smoothLineWidthGranularity() { return smoothLineWidthGranularity_; }

  // GL_POINT_SIZE_RANGE
  static float_t minPointSize() { return minPointSize_; }
  static float_t maxPointSize() { return maxPointSize_; }
  // GL_POINT_SIZE_GRANULARITY
  static float_t pointSizeGranularity() { return pointSizeGranularity_; }

  // GL_STEREO
  static bool stereoSupported() { return stereoSupported_; }

  // GL_SUBPIXEL_BITS
  static uint_t subpixelBits() { return subpixelBits_; }

  // Texture size limits

  // GL_MAX_RENDERBUFFER_SIZE
  static sizei_t maxRenderbufferSize() { return maxRenderbufferSize_; }
  // GL_MAX_TEXTURE_SIZE
  static sizei_t maxTextureSize() { return maxTextureSize_; }
  // GL_MAX_TEXTURE_BUFFER_SIZE
  static sizei_t maxTextureBufferSize() { return maxTextureBufferSize_; }
  // GL_MAX_RECTANGLE_TEXTURE_SIZE
  static sizei_t maxRectangleTextureSize() { return maxRectangleTextureSize_; }
  // GL_MAX_3D_TEXTURE_SIZE
  static sizei_t max3DTextureSize() { return max3DTextureSize_; }
  // GL_MAX_CUBE_MAP_TEXTURE_SIZE
  static sizei_t maxCubeMapTextureSize() { return maxCubeMapTextureSize_; }
  // GL_MAX_ARRAY_TEXTURE_LAYERS
  static sizei_t maxArrayTextureLayers() { return maxArrayTextureLayers_; }

  // GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS
  static uint_t maxCombinedTextureImageUnits() { return maxCombinedTextureImageUnits_; }

  // GL_MIN_PROGRAM_TEXEL_OFFSET
  static int_t minProgramTexelOffset() { return minProgramTexelOffset_; }
  // GL_MAX_PROGRAM_TEXEL_OFFSET
  static int_t maxProgramTexelOffset() { return maxProgramTexelOffset_; }
  // GL_MAX_TEXTURE_LOD_BIAS
  static float_t maxTextureLODBias() { return maxTextureLODBias_; }

  // GL_MAX_CLIP_DISTANCES
  static uint_t maxClipDistances() { return maxClipDistances_; }

  // GL_MAX_ELEMENTS_VERTICES
  static sizei_t maxElementsVertices() { return maxElementsVertices_; }
  // GL_MAX_ELEMENTS_INDICES
  static sizei_t maxElementsIndices() { return maxElementsIndices_; }
  
  // GL_MAX_SAMPLE_MASK_WORDS
  static uint_t maxSampleMaskWords() { return maxSampleMaskWords_; }

  // GL_MAX_COLOR_ATTACHMENTS
  static uint_t maxColourAttachments() { return maxColourAttachments_; }
  // GL_MAX_COLOR_TEXTURE_SAMPLES
  static uint_t maxColourTextureSamples() { return maxColourTextureSamples_; }
  // GL_MAX_DEPTH_TEXTURE_SAMPLES
  static uint_t maxDepthStencilTextureSamples() { return maxDepthStencilTextureSamples_; }
  // GL_MAX_INTEGER_SAMPLES
  static uint_t maxIntegerSamples() { return maxIntegerSamples_; }
  
  // GL_MAX_SERVER_WAIT_TIMEOUT
  static int64_t maxServerWaitTimeout() { return maxServerWaitTimeout_; }

  // GL_MAX_VIEWPORT_DIMS
  static sizei_t maxViewportWidth() { return maxViewportWidth_; }
  static sizei_t maxViewportHeight() { return maxViewportHeight_; }

  ///// Shader variables /////

  // GL_MAX_VARYING_COMPONENTS
  static uint_t maxVaryingComponents() { return maxVaryingComponents_; }
  // GL_MAX_VARYING_FLOATS
  static uint_t maxVaryingFloats() { return maxVaryingFloats_; }
  // GL_MAX_UNIFORM_BUFFER_BINDINGS
  static uint_t maxUniformBufferBindings() { return maxUniformBufferBindings_; }
  // GL_MAX_UNIFORM_BLOCK_SIZE
  static uint_t maxUniformBlockSize() { return maxUniformBlockSize_; }
  // GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT
  static uint_t minUniformBufferOffsetAlignment() { return minUniformBufferOffsetAlignment_; }
  // GL_MAX_COMBINED_UNIFORM_BLOCKS
  static uint_t maxCombinedUniformBlocks() { return maxCombinedUniformBlocks_; }

  // Vertex shader variables

  // GL_MAX_VERTEX_ATTRIBS
  static uint_t maxVertexAttribs() { return maxVertexAttribs_; }
  // GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS
  static uint_t maxVertexTextureImageUnits() { return maxVertexTextureImageUnits_; }
  // GL_MAX_VERTEX_UNIFORM_COMPONENTS
  static uint_t maxVertexUniformComponents() { return maxVertexUniformComponents_; }
  // GL_MAX_VERTEX_UNIFORM_BLOCKS
  static uint_t maxVertexUniformBlocks() { return maxVertexUniformBlocks_; }
  // GL_MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS
  static uint_t maxCombinedVertexUniformComponents() { return maxCombinedVertexUniformComponents_; }
  // GL_MAX_VERTEX_OUTPUT_COMPONENTS
  static uint_t maxVertexOutputComponents() { return maxVertexOutputComponents_; }

  // Geometry shader variables

  // GL_MAX_GEOMETRY_INPUT_COMPONENTS
  static uint_t maxGeometryInputComponents() { return maxGeometryInputComponents_; }
  // GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS
  static uint_t maxGeometryTextureImageUnits() { return maxGeometryTextureImageUnits_; }
  // GL_MAX_GEOMETRY_UNIFORM_COMPONENTS
  static uint_t maxGeometryUniformComponents() { return maxGeometryUniformComponents_; }
  // GL_MAX_GEOMETRY_UNIFORM_BLOCKS
  static uint_t maxGeometryUniformBlocks() { return maxGeometryUniformBlocks_; }
  // GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS
  static uint_t maxCombinedGeometryUniformComponents() { return maxCombinedGeometryUniformComponents_; }
  // GL_MAX_GEOMETRY_OUTPUT_COMPONENTS
  static uint_t maxGeometryOutputComponents() { return maxGeometryOutputComponents_; }

  // Fragment shader variables

  // GL_MAX_FRAGMENT_INPUT_COMPONENTS
  static uint_t maxFragmentInputComponents() { return maxFragmentInputComponents_; }
  // GL_MAX_TEXTURE_IMAGE_UNITS
  static uint_t maxTextureImageUnits() { return maxTextureImageUnits_; }
  // GL_MAX_FRAGMENT_UNIFORM_COMPONENTS
  static uint_t maxFragmentUniformComponents() { return maxFragmentUniformComponents_; }
  // GL_MAX_FRAGMENT_UNIFORM_BLOCKS
  static uint_t maxFragmentUniformBlocks() { return maxFragmentUniformBlocks_; }
  // GL_MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS
  static uint_t maxCombinedFragmentUniformComponents() { return maxCombinedFragmentUniformComponents_; }
  // GL_MAX_DRAW_BUFFERS
  static uint_t maxDrawBuffers() { return maxDrawBuffers_; }
  // GL_MAX_DUALSOURCE_DRAW_BUFFERS
  static uint_t maxDualSourceDrawBuffers() { return maxDualSourceDrawBuffers_; }

  // Initializes the above variables; this should be called only after
  // a valid OpenGL context is established
  static void init();
private:
  // Checks the loaded values all meet the OpenGL specification requirements
  static void checkConfig();
  // Dumps the loaded values to the log system
  static void dumpToLog();

  static float_t minAliasedLineWidth_;
  static float_t maxAliasedLineWidth_;
  static float_t minSmoothLineWidth_;
  static float_t maxSmoothLineWidth_;
  static float_t smoothLineWidthGranularity_;

  static float_t minPointSize_;
  static float_t maxPointSize_;
  static float_t pointSizeGranularity_;
  static bool stereoSupported_;
  static uint_t subpixelBits_;

  static sizei_t maxRenderbufferSize_;
  static sizei_t maxTextureSize_;
  static sizei_t maxTextureBufferSize_;
  static sizei_t maxRectangleTextureSize_;
  static sizei_t max3DTextureSize_;
  static sizei_t maxCubeMapTextureSize_;
  static sizei_t maxArrayTextureLayers_;

  static uint_t maxCombinedTextureImageUnits_;
  static int_t minProgramTexelOffset_;
  static int_t maxProgramTexelOffset_;
  static float_t maxTextureLODBias_;
  static uint_t maxClipDistances_;

  static sizei_t maxElementsVertices_;
  static sizei_t maxElementsIndices_;
  static uint_t maxSampleMaskWords_;
  static uint_t maxColourAttachments_;
  static uint_t maxColourTextureSamples_;
  static uint_t maxDepthStencilTextureSamples_;
  static uint_t maxIntegerSamples_;

  static int64_t maxServerWaitTimeout_;

  static sizei_t maxViewportWidth_;
  static sizei_t maxViewportHeight_;

  static uint_t maxVaryingComponents_;
  static uint_t maxVaryingFloats_;
  static uint_t maxUniformBufferBindings_;
  static uint_t maxUniformBlockSize_;
  static uint_t minUniformBufferOffsetAlignment_;
  static uint_t maxCombinedUniformBlocks_;

  static uint_t maxVertexAttribs_;
  static uint_t maxVertexTextureImageUnits_;
  static uint_t maxVertexUniformComponents_;
  static uint_t maxVertexUniformBlocks_;
  static uint_t maxCombinedVertexUniformComponents_;
  static uint_t maxVertexOutputComponents_;

  static uint_t maxGeometryInputComponents_;
  static uint_t maxGeometryTextureImageUnits_;
  static uint_t maxGeometryUniformComponents_;
  static uint_t maxGeometryUniformBlocks_;
  static uint_t maxCombinedGeometryUniformComponents_;
  static uint_t maxGeometryOutputComponents_;

  static uint_t maxFragmentInputComponents_;
  static uint_t maxTextureImageUnits_;
  static uint_t maxFragmentUniformComponents_;
  static uint_t maxFragmentUniformBlocks_;
  static uint_t maxCombinedFragmentUniformComponents_;
  static uint_t maxDrawBuffers_;
  static uint_t maxDualSourceDrawBuffers_;
};

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_SYSTEM_INFO_H
