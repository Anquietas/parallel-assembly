#pragma once
#ifndef GRAPHICS_GL_QUERY_H
#define GRAPHICS_GL_QUERY_H

#include "GLObject.h"
#include "Types.h"

namespace graphics {
namespace gl {

class QueryBase : public GLObject<traits::Query> {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(QueryBase, GLObject)

  bool resultAvailable() const noexcept;
};

class NumSamplesPassedQuery final : public QueryBase {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(NumSamplesPassedQuery, QueryBase)

  void beginQuery() noexcept;
  void endQuery() noexcept;

  uint64_t result() const noexcept;
};

class AnySamplesPassedQuery final : public QueryBase {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(AnySamplesPassedQuery, QueryBase)

  void beginQuery() noexcept;
  void endQuery() noexcept;

  bool result() const noexcept;
};

class NumPrimitivesGeneratedQuery final : public QueryBase {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(NumPrimitivesGeneratedQuery, QueryBase)

  void beginQuery() noexcept;
  void endQuery() noexcept;

  uint_t result() const noexcept;
};

class NumTransformFeedbackPrimitivesQuery final : public QueryBase {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(NumTransformFeedbackPrimitivesQuery, QueryBase)

  void beginQuery() noexcept;
  void endQuery() noexcept;

  uint_t result() const noexcept;
};

class TimeElapsedQuery final : public QueryBase {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(TimeElapsedQuery, QueryBase)

  void beginQuery() noexcept;
  void endQuery() noexcept;

  uint64_t result() const noexcept;
};

class TimestampQuery final : public QueryBase {
  GLOBJECT_CONSTRUCTOR_HELPER(TimestampQuery, QueryBase)

  void markTimestamp() noexcept;

  uint64_t result() const noexcept;
};

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_QUERY_H
