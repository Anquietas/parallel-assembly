#ifndef GRAPHICS_GL_INDEXBUFFER_H
#define GRAPHICS_GL_INDEXBUFFER_H

#pragma once

#include "Types.h"
#include "GLObject.h"

#include <type_traits>

namespace graphics {
namespace gl {

class IndexBuffer final : public GLObject<traits::VBO> {
public:
  enum class Target : enum_t {
    // Note: Only one IndexBuffer may be bound to the Array target at a time
    IndexArray, // GL_ELEMENT_ARRAY_BUFFER
    // Note: These targets may interfere with other buffer types bound to them,
    //       and vice versa. Hence they should really only be used short-term.
    CopyRead,   // GL_COPY_READ_BUFFER
    CopyWrite   // GL_COPY_WRITE_BUFFER
  };

  enum class Usage : enum_t {
    StaticDraw,   // GL_STATIC_DRAW
    StaticRead,   // GL_STATIC_READ
    StaticCopy,   // GL_STATIC_COPY
    DynamicDraw,  // GL_DYNAMIC_DRAW
    DynamicRead,  // GL_DYNAMIC_READ
    DynamicCopy,  // GL_DYNAMIC_COPY
    StreamDraw,   // GL_STREAM_DRAW
    StreamRead,   // GL_STREAM_READ
    StreamCopy    // GL_STREAM_COPY
  };

  enum class Access : enum_t {
    Read,     // GL_READ_ONLY  or GL_MAP_READ_BIT
    Write,    // GL_WRITE_ONLY or GL_MAP_WRITE_BIT
    ReadWrite // GL_READ_WRITE or (GL_MAP_READ_BIT | GL_MAP_WRITE_BIT)
  };

  GLOBJECT_CONSTRUCTOR_HELPER(IndexBuffer, GLObject)

  // Binds the index buffer to the binding target
  //  - Note: This will unbind any index buffer previously bound to the target
  //  - Note: This will unbind any kind of buffer previously bound to the
  //          target if the target is not Target::IndexArray.
  //  - Note: This must be called before any other operations are performed
  void bind(Target target = Target::IndexArray) noexcept;
  // Unbinds the index buffer bound at the specified target
  static void unbind(Target target = Target::IndexArray) noexcept;

  // Allocates the buffer at the specifid target without storing
  // any data in it, using the specified usage hint
  // - size here means the total size in bytes to allocate
  // - This will also erase any previous allocation of the buffer
  void allocate(sizeiptr_t size,
                Usage usage = Usage::StaticDraw,
                Target target = Target::IndexArray) noexcept;
  // Allocates the buffer at the specified target, and also stores
  // the provided data in it (if the pointer is not null), using
  // the specified usage hint.
  // - size here means the length of the array.
  // - This will also erase any previous allocation of the buffer
  template <typename T>
  // Index buffers exclusively use integer types
  std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value>
  allocate(sizeiptr_t size, const T* data,
           Usage usage = Usage::StaticDraw,
           Target target = Target::IndexArray) noexcept;

  // Updates a portion of the buffer at the specified target with
  // the provided data, with offset and size defining the bounds
  // of the region to be updated.
  // - offset here should be the location within the buffer to begin
  //   the mapping at, in bytes
  // - size here should be the length of the data array.
  // - Note: offset and size must define a region *inside* the
  //   region previously allocated with "allocate". If this is not
  //   satisfied, OpenGL will raise the GL_INVALID_VALUE error.
  template <typename T>
  std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value>
  update(intptr_t offset, std::size_t size, const T* data,
         Target target = Target::IndexArray) noexcept;

  // Used to map all of the buffer at the specified target to RAM
  template <typename T>
  static std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value, T*>
  mapAs(Target target = Target::IndexArray,
        Access access = Access::Write) noexcept;
  // Used to map a portion of the buffer at the specified target to RAM
  // - offset here should be the location within the buffer to begin
  //   the mapping at, in bytes
  // - size here should be the number of elements of type T to map
  template <typename T>
  static std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value, T*>
  mapAs(intptr_t offset, std::size_t length,
        Target target = Target::IndexArray,
        Access access = Access::Write) noexcept;
  // Const overload of mapAs(target, access)
  template <typename T>
  static std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value, const T*>
  mapAsConst(Target target = Target::IndexArray,
             Access access = Access::Read) noexcept;
  // - offset here should be the location within the buffer to begin
  //   the mapping at, in bytes
  // - size here should be the number of elements of type T to map
  template <typename T>
  static std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value, const T*>
  mapAsConst(intptr_t offset, std::size_t length,
             Target target = Target::IndexArray,
             Access access = Access::Read) noexcept;
  static void unmap(Target target = Target::IndexArray) noexcept;
};

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_INDEXBUFFER_H
