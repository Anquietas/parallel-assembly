#ifndef GRAPHICS_GL_INDEXBUFFER_INL
#define GRAPHICS_GL_INDEXBUFFER_INL

#pragma once

#include "IndexBuffer.h"
#include "OpenGLIncludes.h"
#include "GLTypeInfo.inl"

namespace graphics {
namespace gl {

namespace detail {

GLenum bindTargetCode(IndexBuffer::Target target) noexcept;
GLenum bufferUsageCode(IndexBuffer::Usage usage) noexcept;
GLenum bufferAccessCode(IndexBuffer::Access access) noexcept;
GLbitfield bufferAccessBitfield(IndexBuffer::Access access) noexcept;

} // namespace detail

template <typename T>
std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value>
IndexBuffer::allocate(sizeiptr_t size, const T* data,
                      Usage usage, Target target) noexcept
{
  glBufferData(detail::bindTargetCode(target),
               size * sizeof(T), data,
               detail::bufferUsageCode(usage));
}
template <typename T>
std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value>
IndexBuffer::update(intptr_t offset, std::size_t size, const T* data, Target target) noexcept {
  glBufferSubData(detail::bindTargetCode(target), offset,
                  static_cast<sizeiptr_t>(size) * sizeof(T), data);
}

template <typename T>
static std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value, T*>
IndexBuffer::mapAs(Target target, Access access) noexcept {
  return reinterpret_cast<T*>(
    glMapBuffer(detail::bindTargetCode(target),
                detail::bufferAccessCode(access)));
}
template <typename T>
static std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value, T*>
IndexBuffer::mapAs(intptr_t offset, std::size_t length,
                       Target target, Access access) noexcept {
  return reinterpret_cast<T*>(
    glMapBufferRange(detail::bindTargetCode(target), offset,
                     static_cast<sizeiptr_t>(length) * sizeof(T),
                     detail::bufferAccessBitfield(access)));
}
template <typename T>
static std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value, const T*>
IndexBuffer::mapAsConst(Target target, Access access) noexcept {
  return reinterpret_cast<const T*>(
    glMapBuffer(detail::bindTargetCode(target),
                detail::bufferAccessCode(access)));
}
template <typename T>
static std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value, const T*>
IndexBuffer::mapAsConst(intptr_t offset, std::size_t length,
                        Target target, Access access) noexcept
{
  return reinterpret_cast<const T*>(
    glMapBufferRange(detail::bindTargetCode(target), offset,
                     static_cast<sizeiptr_t>(length) * sizeof(T),
                     detail::bufferAccessBitfield(access)));
}

#ifndef GRAPHICS_GL_INDEXBUFFER_NO_EXTERN_TEMPLATES

// allocate(): Prevent compiler generating a template at use site since .cpp file
// contains explicit instantiations
#define EXTERN_ALLOCATE(T) \
  extern template std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value> \
  IndexBuffer::allocate<T>(sizeiptr_t, const T*, Usage, Target) noexcept

EXTERN_ALLOCATE(ubyte_t);
EXTERN_ALLOCATE(ushort_t);
EXTERN_ALLOCATE(uint_t);

#undef EXTERN_ALLOCATE

#define EXTERN_UPDATE(T) \
  extern template std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value> \
  IndexBuffer::update<T>(intptr_t, std::size_t, const T*, Target) noexcept

EXTERN_UPDATE(ubyte_t);
EXTERN_UPDATE(ushort_t);
EXTERN_UPDATE(uint_t);

#define EXTERN_MAPAS(T) \
  extern template std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value, T*> \
  IndexBuffer::mapAs<T>(Target, Access) noexcept; \
  extern template std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value, T*> \
  IndexBuffer::mapAs<T>(intptr_t, std::size_t, Target, Access) noexcept; \
  extern template std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value, const T*> \
  IndexBuffer::mapAsConst<T>(Target, Access) noexcept; \
  extern template std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value, const T*> \
  IndexBuffer::mapAsConst<T>(intptr_t, std::size_t, Target, Access) noexcept

EXTERN_MAPAS(ubyte_t);
EXTERN_MAPAS(ushort_t);
EXTERN_MAPAS(uint_t);

#undef EXTERN_UPDATE

#endif // GRAPHICS_GL_INDEXBUFFER_NO_EXTERN_TEMPLATES

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_INDEXBUFFER_INL
