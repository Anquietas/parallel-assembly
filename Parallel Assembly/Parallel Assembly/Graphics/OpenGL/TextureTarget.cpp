#include "TextureTarget.h"

#include "OpenGLIncludes.h"

#include <type_traits>

#include "../../Assert.h"

namespace graphics {
namespace gl {

enum_t getTextureTargetCode(TextureTarget target) {
  static constexpr const enum_t targets[] = {
    GL_TEXTURE_1D,
    GL_TEXTURE_1D_ARRAY,
    GL_TEXTURE_2D,
    GL_TEXTURE_2D_MULTISAMPLE,
    GL_TEXTURE_2D_ARRAY,
    GL_TEXTURE_2D_MULTISAMPLE_ARRAY,
    GL_TEXTURE_RECTANGLE,
    GL_TEXTURE_CUBE_MAP,
    GL_TEXTURE_3D,
  };

  auto underlying = static_cast<std::underlying_type_t<TextureTarget>>(target);
  ASSERT(0 <= underlying && underlying < std::extent<decltype(targets)>::value);
  return targets[underlying];
}

} // namespace gl
} // namespace graphics
