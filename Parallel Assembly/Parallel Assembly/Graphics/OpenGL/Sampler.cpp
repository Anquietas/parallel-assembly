#include "Sampler.h"

#include "OpenGLIncludes.h"

#include <sstream>
#include <type_traits>

#include "../../Assert.h"
#include "../../Exception.h"

namespace graphics {
namespace gl {

int_t getWrapModeCode(Sampler::WrapMode mode) noexcept {
  static constexpr const int_t wrapCodes[] = {
    GL_CLAMP_TO_BORDER,
    GL_CLAMP_TO_EDGE,
    GL_REPEAT,
    GL_MIRRORED_REPEAT,
  };

  auto underlying = static_cast<std::underlying_type_t<Sampler::WrapMode>>(mode);
  ASSERT(0 <= underlying && underlying < std::extent<decltype(wrapCodes)>::value);
  return wrapCodes[underlying];
}
int_t getMinFilterCode(Sampler::MinifyingFilter filter) noexcept {
  static constexpr const int_t filterCodes[] = {
    GL_NEAREST,
    GL_LINEAR,
    GL_NEAREST_MIPMAP_NEAREST,
    GL_NEAREST_MIPMAP_LINEAR,
    GL_LINEAR_MIPMAP_NEAREST,
    GL_LINEAR_MIPMAP_LINEAR,
  };

  auto underlying = static_cast<std::underlying_type_t<Sampler::MinifyingFilter>>(filter);
  ASSERT(0 <= underlying && underlying < std::extent<decltype(filterCodes)>::value);
  return filterCodes[underlying];
}
int_t getMagFilterCode(Sampler::MagnifyingFilter filter) noexcept {
  static constexpr const int_t filterCodes[] = {
    GL_NEAREST,
    GL_LINEAR,
  };

  auto underlying = static_cast<std::underlying_type_t<Sampler::MagnifyingFilter>>(filter);
  ASSERT(0 <= underlying && underlying < std::extent<decltype(filterCodes)>::value);
  return filterCodes[underlying];
}
int_t getCompareFuncCode(Sampler::CompareFunc func) noexcept {
  static constexpr const int_t funcs[] = {
    GL_NEVER,
    GL_EQUAL,
    GL_NOTEQUAL,
    GL_LESS,
    GL_LEQUAL,
    GL_GREATER,
    GL_GEQUAL,
    GL_ALWAYS,
  };

  auto underlying = static_cast<std::underlying_type_t<Sampler::CompareFunc>>(func);
  ASSERT(0 <= underlying && underlying < std::extent<decltype(funcs)>::value);
  return funcs[underlying];
}

Sampler::WrapMode getWrapMode(int_t mode) {
  switch (mode) {
  case GL_CLAMP_TO_BORDER:
    return Sampler::WrapMode::ClampToBorder;
  case GL_CLAMP_TO_EDGE:
    return Sampler::WrapMode::ClampToEdge;
  case GL_REPEAT:
    return Sampler::WrapMode::Repeat;
  case GL_MIRRORED_REPEAT:
    return Sampler::WrapMode::RepeatMirrored;
  default: {
    std::ostringstream ss;
    ss << "Unknown sampler wrap mode code: " << std::hex << mode;
    throw MAKE_EXCEPTION(ss.str());
  }
  }
}
Sampler::MinifyingFilter getMinFilter(int_t filter) {
  switch (filter) {
  case GL_NEAREST:
    return Sampler::MinifyingFilter::Nearest;
  case GL_LINEAR:
    return Sampler::MinifyingFilter::Linear;
  case GL_NEAREST_MIPMAP_NEAREST:
    return Sampler::MinifyingFilter::NearestMipmapNearest;
  case GL_NEAREST_MIPMAP_LINEAR:
    return Sampler::MinifyingFilter::NearestMipmapLinear;
  case GL_LINEAR_MIPMAP_NEAREST:
    return Sampler::MinifyingFilter::LinearMipmapNearest;
  case GL_LINEAR_MIPMAP_LINEAR:
    return Sampler::MinifyingFilter::LinearMipmapLinear;
  default: {
    std::ostringstream ss;
    ss << "Unknown sampler minifying filter code: " << std::hex << filter;
    throw MAKE_EXCEPTION(ss.str());
  }
  }
}
Sampler::MagnifyingFilter getMagFilter(int_t filter) {
  switch (filter) {
  case GL_NEAREST:
    return Sampler::MagnifyingFilter::Nearest;
  case GL_LINEAR:
    return Sampler::MagnifyingFilter::Linear;
  default: {
    std::ostringstream ss;
    ss << "Unknown sampler magnifying filter code: " << std::hex << filter;
    throw MAKE_EXCEPTION(ss.str());
  }
  }
}
Sampler::CompareFunc getCompareFunc(int_t filter) {
  switch (filter) {
  case GL_NEVER:
    return Sampler::CompareFunc::Never;
  case GL_EQUAL:
    return Sampler::CompareFunc::Equal;
  case GL_NOTEQUAL:
    return Sampler::CompareFunc::NotEqual;
  case GL_LESS:
    return Sampler::CompareFunc::Less;
  case GL_LEQUAL:
    return Sampler::CompareFunc::LessEqual;
  case GL_GREATER:
    return Sampler::CompareFunc::Greater;
  case GL_GEQUAL:
    return Sampler::CompareFunc::GreaterEqual;
  case GL_ALWAYS:
    return Sampler::CompareFunc::Always;
  default: {
    std::ostringstream ss;
    ss << "Unknown sampler compare function code: " << std::hex << filter;
    throw MAKE_EXCEPTION(ss.str());
  }
  }
}

void Sampler::bind(const TextureUnit& texUnit) noexcept {
  glBindSampler(texUnit.id, m_id);
}
void Sampler::unbind(const TextureUnit& texUnit) noexcept {
  glBindSampler(texUnit.id, uint_t(0));
}

void Sampler::setWrapModeS(WrapMode mode) noexcept {
  glSamplerParameteri(m_id, GL_TEXTURE_WRAP_S, getWrapModeCode(mode));
}
void Sampler::setWrapModeT(WrapMode mode) noexcept {
  glSamplerParameteri(m_id, GL_TEXTURE_WRAP_T, getWrapModeCode(mode));
}
void Sampler::setWrapModeR(WrapMode mode) noexcept {
  glSamplerParameteri(m_id, GL_TEXTURE_WRAP_R, getWrapModeCode(mode));
}
void Sampler::setAllWrapModes(WrapMode mode) noexcept {
  auto wrapCode = getWrapModeCode(mode);
  glSamplerParameteri(m_id, GL_TEXTURE_WRAP_S, wrapCode);
  glSamplerParameteri(m_id, GL_TEXTURE_WRAP_T, wrapCode);
  glSamplerParameteri(m_id, GL_TEXTURE_WRAP_R, wrapCode);
}

Sampler::WrapMode Sampler::wrapModeS() const {
  int_t wrapCode = int_t(0);
  glGetSamplerParameteriv(m_id, GL_TEXTURE_WRAP_S, &wrapCode);
  return getWrapMode(wrapCode);
}
Sampler::WrapMode Sampler::wrapModeT() const {
  int_t wrapCode = int_t(0);
  glGetSamplerParameteriv(m_id, GL_TEXTURE_WRAP_T, &wrapCode);
  return getWrapMode(wrapCode);
}
Sampler::WrapMode Sampler::wrapModeR() const {
  int_t wrapCode = int_t(0);
  glGetSamplerParameteriv(m_id, GL_TEXTURE_WRAP_R, &wrapCode);
  return getWrapMode(wrapCode);
}

void Sampler::setMinLOD(float_t minLOD) noexcept {
  glSamplerParameterf(m_id, GL_TEXTURE_MIN_LOD, minLOD);
}
void Sampler::setMaxLOD(float_t maxLOD) noexcept {
  glSamplerParameterf(m_id, GL_TEXTURE_MAX_LOD, maxLOD);
}

float_t Sampler::minLOD() const noexcept {
  float_t minLOD = float_t(0);
  glGetSamplerParameterfv(m_id, GL_TEXTURE_MIN_LOD, &minLOD);
  return minLOD;
}
float_t Sampler::maxLOD() const noexcept {
  float_t maxLOD = float_t(0);
  glGetSamplerParameterfv(m_id, GL_TEXTURE_MAX_LOD, &maxLOD);
  return maxLOD;
}

void Sampler::setBorderColour(glm::vec4 colour) noexcept {
  static_assert(sizeof(glm::vec4) == 4 * sizeof(float_t),
                "Size discrepancy between glm::vec4 and its components");
  glSamplerParameterfv(m_id, GL_TEXTURE_BORDER_COLOR, reinterpret_cast<const float_t*>(&colour));
}
glm::vec4 Sampler::borderColour() const noexcept {
  static_assert(sizeof(glm::vec4) == 4 * sizeof(float_t),
                "Size discrepancy between glm::vec4 and its components");
  glm::vec4 colour;
  glGetSamplerParameterfv(m_id, GL_TEXTURE_BORDER_COLOR, reinterpret_cast<float_t*>(&colour));
  return colour;
}

void Sampler::setMinFilter(MinifyingFilter filter) noexcept {
  glSamplerParameteri(m_id, GL_TEXTURE_MIN_FILTER, getMinFilterCode(filter));
}
void Sampler::setMagFilter(MagnifyingFilter filter) noexcept {
  glSamplerParameteri(m_id, GL_TEXTURE_MAG_FILTER, getMagFilterCode(filter));
}

Sampler::MinifyingFilter Sampler::minFilter() const {
  int_t filter = int_t(0);
  glGetSamplerParameteriv(m_id, GL_TEXTURE_MIN_FILTER, &filter);
  return getMinFilter(filter);
}
Sampler::MagnifyingFilter Sampler::magFilter() const {
  int_t filter = int_t(0);
  glGetSamplerParameteriv(m_id, GL_TEXTURE_MAG_FILTER, &filter);
  return getMagFilter(filter);
}

void Sampler::enableCompareRefToTexture() noexcept {
  glSamplerParameteri(m_id, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
}
void Sampler::disableCompareRefToTexture() noexcept {
  glSamplerParameteri(m_id, GL_TEXTURE_COMPARE_MODE, GL_NONE);
}
void Sampler::setCompareFunc(CompareFunc func) noexcept {
  glSamplerParameteri(m_id, GL_TEXTURE_COMPARE_FUNC, getCompareFuncCode(func));
}

bool Sampler::isCompareRefToTextureEnabled() const noexcept {
  int_t mode = int_t(0);
  glGetSamplerParameteriv(m_id, GL_TEXTURE_COMPARE_MODE, &mode);
  return mode == GL_COMPARE_REF_TO_TEXTURE;
}
Sampler::CompareFunc Sampler::compareFunc() const {
  int_t func = int_t(0);
  glGetSamplerParameteriv(m_id, GL_TEXTURE_COMPARE_FUNC, &func);
  return getCompareFunc(func);
}

} // namespace gl
} // namespace graphics
