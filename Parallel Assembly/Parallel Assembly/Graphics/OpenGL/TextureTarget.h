#pragma once
#ifndef GRAPHICS_GL_TEXTURE_TARGET_H
#define GRAPHICS_GL_TEXTURE_TARGET_H

#include "Types.h"

namespace graphics {
namespace gl {

enum class TextureTarget : enum_t {
  Texture1D,                  // GL_TEXTURE_1D
  Texture1DArray,             // GL_TEXTURE_1D_ARRAY
  Texture2D,                  // GL_TEXTURE_2D
  Texture2DMultisample,       // GL_TEXTURE_2D_MULTISAMPLE
  Texture2DArray,             // GL_TEXTURE_2D_ARRAY
  Texture2DMultisampleArray,  // GL_TEXTURE_2D_MULTISAMPLE_ARRAY
  TextureRectangle,           // GL_TEXTURE_RECTANGLE
  TextureCubeMap,             // GL_TEXTURE_CUBE_MAP
  Texture3D,                  // GL_TEXTURE_3D
};

enum_t getTextureTargetCode(TextureTarget target);

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_TEXTURE_TARGET_H
