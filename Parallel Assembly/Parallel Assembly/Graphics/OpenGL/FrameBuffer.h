#pragma once
#ifndef GRAPHICS_GL_FRAMEBUFFER_H
#define GRAPHICS_GL_FRAMEBUFFER_H

#include "Types.h"
#include "GLObject.h"

namespace graphics {
namespace gl {

class ColourRenderBuffer;
class DepthStencilRenderBuffer;
class ColourTexture2D;
class DepthStencilTexture2D;
class ColourMultisampleTexture2D;
class DepthStencilMultisampleTexture2D;

// Attachments for the default framebuffer
enum class ScreenAttachment {
  None = 0,       // GL_NONE
  Front,          // GL_FRONT
  Back,           // GL_BACK
  Left,           // GL_LEFT
  Right,          // GL_RIGHT
  FrontLeft,      // GL_FRONT_LEFT
  FrontRight,     // GL_FRONT_RIGHT
  BackLeft,       // GL_BACK_LEFT
  BackRight,      // GL_BACK_RIGHT
  FrontAndBack,   // GL_FRONT_AND_BACK
};

class FrameBuffer final : public GLObject<traits::FrameBuffer> {
  static FrameBuffer* primaryFrameBuffer;
public:
  enum class Target : enum_t {
    Draw = 0,       // GL_DRAW_FRAMEBUFFER
    Read,           // GL_READ_FRAMEBUFFER
  };

  struct ColourAttachment;

  enum class DepthStencilAttachment {
    Depth = 0,      // GL_DEPTH_ATTACHMENT
    Stencil,        // GL_STENCIL_ATTACHMENT
    DepthStencil,   // GL_DEPTH_STENCIL_ATTACHMENT
  };

  GLOBJECT_CONSTRUCTOR_HELPER(FrameBuffer, GLObject)

  // Binds the FrameBuffer to the specified target
  // Note: this will unbind any FrameBuffer previously bound to the target
  void bind(Target target = Target::Draw) noexcept;
  // Unbinds the FrameBuffer currently bound to the specified target
  static void unbind(Target target = Target::Draw) noexcept;

  // Sets the primary FrameBuffer to the FrameBuffer provided.
  // If newBuffer is nullptr, this indicates the default framebuffer
  // should be primary.
  // The given FrameBuffer should be cleared before the end of the
  // FrameBuffer's lifespan.
  static void setPrimaryFrameBuffer(FrameBuffer* newBuffer) noexcept;
  // Returns a pointer to the primary FrameBuffer previously set with
  // setPrimaryFrameBuffer, or nullptr if the default FrameBuffer should
  // be primary.
  static FrameBuffer* getPrimaryFrameBuffer() noexcept;

  // The following functions will only work after calling bind!

  // Attaches the specified renderbuffer to the specified attachment point
  // of the FrameBuffer previously bound to target
  static void attach(Target target, const ColourAttachment& attachment,
                     const ColourRenderBuffer& renderbuffer) noexcept;
  // Attaches the specified renderbuffer to the specified attachment point
  // of the FrameBuffer previously bound to target
  static void attach(Target target, const DepthStencilAttachment& attachment,
                     const DepthStencilRenderBuffer& renderbuffer) noexcept;

  // Attaches the specified mipmap level of the specified 2D texture
  // to the specified attachment point of the FrameBuffer previously bound to target
  static void attach(Target target, const ColourAttachment& attachment,
                     const ColourTexture2D& texture, int_t mipMapLevel = 0) noexcept;
  // Attaches the specified 2D texture to the specified attachment point
  // of the FrameBuffer previously bound to target
  static void attach(Target target, const DepthStencilAttachment& attachment,
                     const DepthStencilTexture2D& texture) noexcept;

  // Attaches the specified multisample 2D texture to the specified
  // attachment point of the FrameBuffer previously bound to target
  static void attach(Target target, const ColourAttachment& attachment,
                     const ColourMultisampleTexture2D& texture) noexcept;
  // Attaches the specified multisample 2D texture to the specified
  // attachment point of the FrameBuffer previously bound to target
  static void attach(Target target, const DepthStencilAttachment& attachment,
                     const DepthStencilMultisampleTexture2D& texture) noexcept;

  // Checks the FrameBuffer bound to the specified target is in a complete (usable) state
  static bool isComplete(Target target = Target::Draw) noexcept;
};

void setReadBuffer(ScreenAttachment attachment) noexcept;
void setReadBuffer(const FrameBuffer::ColourAttachment& attachment) noexcept;
void setReadBuffer(FrameBuffer::DepthStencilAttachment attachment) noexcept;

void setDrawBuffer(ScreenAttachment attachment) noexcept;
void setDrawBuffer(const FrameBuffer::ColourAttachment& attachment) noexcept;
void setDrawBuffer(FrameBuffer::DepthStencilAttachment attachment) noexcept;

struct FrameBuffer::ColourAttachment {
  uint_t id;

  explicit ColourAttachment(uint_t id_ = static_cast<uint_t>(-1)) noexcept : id(id_) {}
  ColourAttachment(const ColourAttachment& o) noexcept = default;
  ColourAttachment(ColourAttachment&& o) noexcept = default;
  ~ColourAttachment() = default;

  ColourAttachment& operator=(const ColourAttachment& o) noexcept = default;
  ColourAttachment& operator=(ColourAttachment&& o) noexcept = default;

  bool valid() const noexcept;
};

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_FRAMEBUFFER_H
