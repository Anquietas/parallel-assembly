#pragma once
#ifndef GRAPHICS_GL_RENDERBUFFER_H
#define GRAPHICS_GL_RENDERBUFFER_H

#include "GLObject.h"
#include "ImageFormat.h"

#include "../../GLMIncludes.h"

namespace graphics {
namespace gl {

namespace detail {
class RenderBufferBase : public GLObject<traits::RenderBuffer> {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(RenderBufferBase, GLObject)

  // Binds the RenderBuffer to GL_RENDERBUFFER
  // Note: this will unbind any previously bound RenderBuffer
  void bind() noexcept;
  // Unbinds the currently bound RenderBuffer
  static void unbind() noexcept;

  // The following functions will only work after calling bind!

  // Returns the width of the RenderBuffer in pixels
  sizei_t width() const noexcept;
  sizei_t height() const noexcept;
  sizei_t samples() const noexcept;
};
} // namespace detail

class ColourRenderBuffer final : public detail::RenderBufferBase {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(ColourRenderBuffer, RenderBufferBase)

  // The following functions will only work after calling bind!

  // Allocates a RenderBuffer with the specified colour format, width and height,
  // but without any multisampling
  void allocate(internal_format::Colour colourFormat,
                sizei_t width, sizei_t height) noexcept;
  // Allocates a RenderBuffer with the specified colour format, width and height,
  // and with multisampling set to the specified level
  // If samples is 0, this is equivalent to calling the above function.
  void allocate(internal_format::Colour colourFormat,
                sizei_t width, sizei_t height, sizei_t samples) noexcept;

  glm::ivec4 rgbaSizes() const noexcept;

  internal_format::Colour internalFormat() const;
};

class DepthStencilRenderBuffer final : public detail::RenderBufferBase {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(DepthStencilRenderBuffer, RenderBufferBase)

  // The following functions will only work after calling bind!

  // Allocates a RenderBuffer with the specified depth/stencil format, width and height,
  // but without any multisampling
  void allocate(internal_format::DepthStencil depthFormat,
                sizei_t width, sizei_t height) noexcept;
  // Allocates a RenderBuffer with the specified depth/stencil format, width and height,
  // and with multisampling set to the specified level
  // If samples is 0, this is equivalent to calling the above function.
  void allocate(internal_format::DepthStencil depthFormat,
                sizei_t width, sizei_t height, sizei_t samples) noexcept;

  sizei_t depthSize() const noexcept;
  sizei_t stencilSize() const noexcept;

  internal_format::DepthStencil internalFormat() const;
};

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_RENDERBUFFER_H
