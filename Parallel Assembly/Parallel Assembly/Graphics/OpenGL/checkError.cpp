#include "checkError.h"

#include <string>
#include <sstream>

#include <boost/algorithm/string/case_conv.hpp>

#include "OpenGLIncludes.h"

#include "../../Exception.h"

namespace graphics {
namespace gl {

const char* getErrorString(GLenum errorCode) {
  switch (errorCode) {
  case GL_INVALID_ENUM:
    return "invalid enumeration";
  case GL_INVALID_VALUE:
    return "invalid value";
  case GL_INVALID_OPERATION:
    return "invalid operation";
  case GL_STACK_OVERFLOW:
    return "stack overflow";
  case GL_STACK_UNDERFLOW:
    return "stack underflow";
  case GL_OUT_OF_MEMORY:
    return "OpenGL out of memory";
  case GL_INVALID_FRAMEBUFFER_OPERATION:
    return "invalid framebuffer operation";
  case GL_CONTEXT_LOST:
    return "OpenGL context lost";
  default:
    return nullptr;
  }
}

void checkError(const char* fileName, int lineNumber) {
  GLenum error = glGetError();
  
  if (error != GL_NO_ERROR) {
    // Get relative file path
    std::string filePathStr(fileName);
    std::string filePathStrLower = boost::algorithm::to_lower_copy(filePathStr);
    auto pos = filePathStrLower.rfind("parallel assembly");
    if (pos == std::string::npos)
      pos = 0;

    std::ostringstream ss;
    ss << "OpenGL error detected in " << filePathStr.substr(pos) << " on line " << lineNumber << ":\n";
    do {
      auto errorStr = getErrorString(error);
      if (errorStr)
        ss << errorStr << '\n';
      else
        ss << "Warning! Did not recognize error code " << std::showbase << std::hex << error << '\n';
      error = glGetError();
    } while (error != GL_NO_ERROR);
    throw MAKE_EXCEPTION(ss.str());
  }
}

} // namespace gl
} // namespace graphics
