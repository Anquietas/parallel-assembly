#pragma once
#ifndef GRAPHICS_GL_TEXTURE_UNIT_H
#define GRAPHICS_GL_TEXTURE_UNIT_H

#include "Types.h"

namespace graphics {
namespace gl {

struct TextureUnit final {
  uint_t id;

  explicit TextureUnit(uint_t id_ = static_cast<uint_t>(-1)) noexcept : id(id_) {}
  TextureUnit(const TextureUnit& o) noexcept = default;
  TextureUnit(TextureUnit&& o) noexcept = default;
  ~TextureUnit() = default;

  TextureUnit& operator=(const TextureUnit& o) noexcept = default;
  TextureUnit& operator=(TextureUnit&& o) noexcept = default;

  bool valid() const noexcept;

  // Sets this texture unit to be the active one, via glActiveTexture
  void setActive() noexcept;
};

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_TEXTURE_UNIT_H
