#include "Query.h"

#include "OpenGLIncludes.h"

namespace graphics {
namespace gl {

bool QueryBase::resultAvailable() const noexcept {
  GLint available = 0;
  glGetQueryObjectiv(m_id, GL_QUERY_RESULT_AVAILABLE, &available);
  return available == GL_TRUE;
}


void NumSamplesPassedQuery::beginQuery() noexcept {
  glBeginQuery(GL_SAMPLES_PASSED, m_id);
}
void NumSamplesPassedQuery::endQuery() noexcept {
  glEndQuery(GL_SAMPLES_PASSED);
}

uint64_t NumSamplesPassedQuery::result() const noexcept {
  uint64_t result = 0;
  glGetQueryObjectui64v(m_id, GL_QUERY_RESULT, &result);
  return result;
}


void AnySamplesPassedQuery::beginQuery() noexcept {
  glBeginQuery(GL_ANY_SAMPLES_PASSED, m_id);
}
void AnySamplesPassedQuery::endQuery() noexcept {
  glEndQuery(GL_ANY_SAMPLES_PASSED);
}

bool AnySamplesPassedQuery::result() const noexcept {
  GLint result = GL_FALSE;
  glGetQueryObjectiv(m_id, GL_QUERY_RESULT, &result);
  return result == GL_TRUE;
}


void NumPrimitivesGeneratedQuery::beginQuery() noexcept {
  glBeginQuery(GL_PRIMITIVES_GENERATED, m_id);
}
void NumPrimitivesGeneratedQuery::endQuery() noexcept {
  glEndQuery(GL_PRIMITIVES_GENERATED);
}

uint_t NumPrimitivesGeneratedQuery::result() const noexcept {
  uint_t result = 0;
  glGetQueryObjectuiv(m_id, GL_QUERY_RESULT, &result);
  return result;
}


void NumTransformFeedbackPrimitivesQuery::beginQuery() noexcept {
  glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, m_id);
}
void NumTransformFeedbackPrimitivesQuery::endQuery() noexcept {
  glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
}

uint_t NumTransformFeedbackPrimitivesQuery::result() const noexcept {
  uint_t result = 0;
  glGetQueryObjectuiv(m_id, GL_QUERY_RESULT, &result);
  return result;
}


void TimeElapsedQuery::beginQuery() noexcept {
  glBeginQuery(GL_TIME_ELAPSED, m_id);
}
void TimeElapsedQuery::endQuery() noexcept {
  glEndQuery(GL_TIME_ELAPSED);
}

uint64_t TimeElapsedQuery::result() const noexcept {
  uint64_t result = 0;
  glGetQueryObjectui64v(m_id, GL_QUERY_RESULT, &result);
  return result;
}

void TimestampQuery::markTimestamp() noexcept {
  glQueryCounter(m_id, GL_TIMESTAMP);
}

uint64_t TimestampQuery::result() const noexcept {
  uint64_t result = 0;
  glGetQueryObjectui64v(m_id, GL_QUERY_RESULT, &result);
  return result;
}

} // namespace gl
} // namespace graphics
