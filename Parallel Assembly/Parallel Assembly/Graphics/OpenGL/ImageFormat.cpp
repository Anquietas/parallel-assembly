#include "ImageFormat.h"

#include "OpenGLIncludes.h"

#include <sstream>
#include <type_traits>

#include "../../Assert.h"
#include "../../Exception.h"

namespace graphics {
namespace gl {

enum_t getFormatCode(internal_format::Colour colourFormat) noexcept {
  static constexpr const enum_t colourCodes[] = {
    // Texture & Renderbuffer:
    // Single-colour formats
    GL_R8,
    GL_R8I,
    GL_R8UI,
    GL_R16,
    GL_R16F,
    GL_R16I,
    GL_R16UI,
    GL_R32F,
    GL_R32I,
    GL_R32UI,

    // Two-colour formats
    GL_RG8,
    GL_RG8I,
    GL_RG8UI,
    GL_RG16,
    GL_RG16F,
    GL_RG16I,
    GL_RG16UI,
    GL_RG32F,
    GL_RG32I,
    GL_RG32UI,

    // Four-colour formats
    GL_RGBA8,
    GL_RGBA8I,
    GL_RGBA8UI,
    GL_RGBA16,
    GL_RGBA16F,
    GL_RGBA16I,
    GL_RGBA16UI,
    GL_RGBA32F,
    GL_RGBA32I,
    GL_RGBA32UI,

    // Other
    GL_RGB10_A2,
    GL_RGB10_A2UI,
    GL_R11F_G11F_B10F,
    GL_SRGB8,
    GL_SRGB8_ALPHA8,

    // Texture only
    // Signed-normalized formats
    GL_R8_SNORM,
    GL_R16_SNORM,
    GL_RG8_SNORM,
    GL_RG16_SNORM,
    GL_RGB8_SNORM,
    GL_RGB16_SNORM,
    GL_RGBA8_SNORM,
    GL_RGBA16_SNORM,

    // Three-colour formats
    GL_RGB8,
    GL_RGB8I,
    GL_RGB8UI,
    GL_RGB16,
    GL_RGB16F,
    GL_RGB16I,
    GL_RGB16UI,
    GL_RGB32F,
    GL_RGB32I,
    GL_RGB32UI,
  };

  auto underlying = static_cast<std::underlying_type_t<internal_format::Colour>>(colourFormat);
  ASSERT(0 <= underlying && underlying < std::extent<decltype(colourCodes)>::value);
  return colourCodes[underlying];
}
enum_t getFormatCode(internal_format::DepthStencil depthFormat) noexcept {
  static constexpr const enum_t depthCodes[] = {
    GL_DEPTH_COMPONENT16,
    GL_DEPTH_COMPONENT24,
    GL_DEPTH_COMPONENT32F,
    GL_DEPTH24_STENCIL8,
    GL_DEPTH32F_STENCIL8,
  };

  auto underlying = static_cast<std::underlying_type_t<internal_format::DepthStencil>>(depthFormat);
  ASSERT(0 <= underlying && underlying < std::extent<decltype(depthCodes)>::value);
  return depthCodes[underlying];
}

internal_format::Colour getColourFormat(enum_t formatCode) {
  using internal_format::Colour;
  switch (formatCode) {
  // Texture & Renderbuffer:
  // Single-colour formats
  case GL_R8:
    return Colour::R_8;
  case GL_R8I:
    return Colour::R_8i;
  case GL_R8UI:
    return Colour::R_8ui;
  case GL_R16:
    return Colour::R_16;
  case GL_R16F:
    return Colour::R_16f;
  case GL_R16I:
    return Colour::R_16i;
  case GL_R16UI:
    return Colour::R_16ui;
  case GL_R32F:
    return Colour::R_32f;
  case GL_R32I:
    return Colour::R_32i;
  case GL_R32UI:
    return Colour::R_32ui;

  // Two-colour formats
  case GL_RG8:
    return Colour::RG_8;
  case GL_RG8I:
    return Colour::RG_8i;
  case GL_RG8UI:
    return Colour::RG_8ui;
  case GL_RG16:
    return Colour::RG_16;
  case GL_RG16F:
    return Colour::RG_16f;
  case GL_RG16I:
    return Colour::RG_16i;
  case GL_RG16UI:
    return Colour::RG_16ui;
  case GL_RG32F:
    return Colour::RG_32f;
  case GL_RG32I:
    return Colour::RG_32i;
  case GL_RG32UI:
    return Colour::RG_32ui;

  // Four-colour formats
  case GL_RGBA8:
    return Colour::RGBA_8;
  case GL_RGBA8I:
    return Colour::RGBA_8i;
  case GL_RGBA8UI:
    return Colour::RGBA_8ui;
  case GL_RGBA16:
    return Colour::RGBA_16;
  case GL_RGBA16F:
    return Colour::RGBA_16f;
  case GL_RGBA16I:
    return Colour::RGBA_16i;
  case GL_RGBA16UI:
    return Colour::RGBA_16ui;
  case GL_RGBA32F:
    return Colour::RGBA_32f;
  case GL_RGBA32I:
    return Colour::RGBA_32i;
  case GL_RGBA32UI:
    return Colour::RGBA_32ui;

  // Other
  case GL_RGB10_A2:
    return Colour::RGB_10_A_2;
  case GL_RGB10_A2UI:
    return Colour::RGB_10_A_2ui;
  case GL_R11F_G11F_B10F:
    return Colour::R11f_G11f_B10f;
  case GL_SRGB8:
    return Colour::SRGB_8;
  case GL_SRGB8_ALPHA8:
    return Colour::SRGB_8_A_8;

  // Texture only
  // Signed-normalized formats
  case GL_R8_SNORM:
    return Colour::R_8SNorm;
  case GL_R16_SNORM:
    return Colour::R_16SNorm;
  case GL_RG8_SNORM:
    return Colour::RG_8SNorm;
  case GL_RG16_SNORM:
    return Colour::RG_16SNorm;
  case GL_RGB8_SNORM:
    return Colour::RGB_8SNorm;
  case GL_RGB16_SNORM:
    return Colour::RGB_16SNorm;
  case GL_RGBA8_SNORM:
    return Colour::RGBA_8SNorm;
  case GL_RGBA16_SNORM:
    return Colour::RGBA_16SNorm;

  // Three-colour formats
  case GL_RGB8:
    return Colour::RGB_8;
  case GL_RGB8I:
    return Colour::RGB_8i;
  case GL_RGB8UI:
    return Colour::RGB_8ui;
  case GL_RGB16:
    return Colour::RGB_16;
  case GL_RGB16F:
    return Colour::RGB_16f;
  case GL_RGB16I:
    return Colour::RGB_16i;
  case GL_RGB16UI:
    return Colour::RGB_16ui;
  case GL_RGB32F:
    return Colour::RGB_32f;
  case GL_RGB32I:
    return Colour::RGB_32i;
  case GL_RGB32UI:
    return Colour::RGB_32ui;
  default: {
    std::ostringstream ss;
    ss << "Unknown colour internal format code: " << std::hex << formatCode;
    throw MAKE_EXCEPTION(ss.str());
  }
  }
}

internal_format::DepthStencil getDepthStencilFormat(enum_t formatCode) {
  using internal_format::DepthStencil;
  switch (formatCode) {
  case GL_DEPTH_COMPONENT16:
    return DepthStencil::Depth_16;
  case GL_DEPTH_COMPONENT24:
    return DepthStencil::Depth_24;
  case GL_DEPTH_COMPONENT32F:
    return DepthStencil::Depth_32f;
  case GL_DEPTH24_STENCIL8:
    return DepthStencil::Depth_24_Stencil_8;
  case GL_DEPTH32F_STENCIL8:
    return DepthStencil::Depth_32f_Stencil_8;
  default: {
    std::ostringstream ss;
    ss << "Unknown depth/stencil internal format code: " << std::hex << formatCode;
    throw MAKE_EXCEPTION(ss.str());
  }
  }
}

enum_t getFormatCode(format::Colour colourFormat) noexcept {
  static constexpr const enum_t colourCodes[] = {
    GL_RED,
    GL_RED_INTEGER,
    GL_RG,
    GL_RG_INTEGER,
    GL_RGB,
    GL_RGB_INTEGER,
    GL_BGR,
    GL_BGR_INTEGER,
    GL_RGBA,
    GL_RGBA_INTEGER,
    GL_BGRA,
    GL_BGRA_INTEGER,
  };

  auto underlying = static_cast<std::underlying_type_t<format::Colour>>(colourFormat);
  ASSERT(0 <= underlying && underlying < std::extent<decltype(colourCodes)>::value);
  return colourCodes[underlying];
}

enum_t getFormatCode(format::DepthStencil depthFormat) noexcept {
  static constexpr const enum_t depthCodes[] = {
    GL_DEPTH_COMPONENT,
    GL_DEPTH_STENCIL,
  };

  auto underlying = static_cast<std::underlying_type_t<format::DepthStencil>>(depthFormat);
  ASSERT(0 <= underlying && underlying < std::extent<decltype(depthCodes)>::value);
  return depthCodes[underlying];
}

enum_t getTypeCode(ImageType type) noexcept {
  static constexpr const enum_t typeCodes[] = {
    GL_BYTE,
    GL_UNSIGNED_BYTE,
    GL_SHORT,
    GL_UNSIGNED_SHORT,
    GL_INT,
    GL_UNSIGNED_INT,
    GL_FLOAT,
    GL_UNSIGNED_BYTE_3_3_2,
    GL_UNSIGNED_BYTE_2_3_3_REV,
    GL_UNSIGNED_SHORT_5_6_5,
    GL_UNSIGNED_SHORT_5_6_5_REV,
    GL_UNSIGNED_SHORT_4_4_4_4,
    GL_UNSIGNED_SHORT_4_4_4_4_REV,
    GL_UNSIGNED_SHORT_5_5_5_1,
    GL_UNSIGNED_SHORT_1_5_5_5_REV,
    GL_UNSIGNED_INT_8_8_8_8,
    GL_UNSIGNED_INT_8_8_8_8_REV,
    GL_UNSIGNED_INT_10_10_10_2,
    GL_UNSIGNED_INT_2_10_10_10_REV,
  };

  auto underlying = static_cast<std::underlying_type_t<ImageType>>(type);
  ASSERT(0 <= underlying && underlying < std::extent<decltype(typeCodes)>::value);
  return typeCodes[underlying];
}

} // namespace gl
} // namespace graphics
