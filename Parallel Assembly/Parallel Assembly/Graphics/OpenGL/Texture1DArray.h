#pragma once
#ifndef GRAPHICS_GL_TEXTURE_1D_ARRAY_H
#define GRAPHICS_GL_TEXTURE_1D_ARRAY_H

#include "Types.h"
#include "GLObject.h"
#include "ImageFormat.h"
#include "../../GLMIncludes.h"

namespace graphics {
namespace gl {

class Texture1DArrayBase : public GLObject<traits::Texture> {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(Texture1DArrayBase, GLObject)

  // Binds the texture to GL_TEXTURE_1D_ARRAY
  void bind() noexcept;
  // Unbinds the texture currently bound to GL_TEXTURE_1D_ARRAY
  static void unbind() noexcept;

  // The following functions will only work after calling bind()!
  // The following functions will not return meaningful results until allocate has been called

  sizei_t width(int_t mipMapLevel = 0) const noexcept;
  sizei_t length() const noexcept;

  bool isCompressed() const noexcept;
  sizei_t compressedSize() const noexcept;
};

class ColourTexture1DArray final : public Texture1DArrayBase {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(ColourTexture1DArray, Texture1DArrayBase)

  // The following functions will only work after calling bind()!

  // Allocates the texture and fills it with the given pixel data. Passing a null
  // pointer for pixelData will leave the texture in an uninitialized state
  void allocate(int_t mipMapLevel, internal_format::Colour internalFormat,
                sizei_t width, sizei_t length, format::Colour pixelFormat,
                ImageType pixelType, const void* pixelData) noexcept;
  // Updates a region of the texture between (offset, index) and
  // (offset + width, index + length) with the given
  // pixel data. pixelData must not be null.
  void update(int_t mipMapLevel, int_t offset, int_t index,
              sizei_t width, sizei_t length, format::Colour pixelFormat,
              ImageType pixelType, const void* pixelData) noexcept;
  // Generates mipmaps for the texture
  // - don't call this before the texture has pixel data in it for all levels of the array!
  void generateMipMaps() noexcept;

  // Thw following functions will not return meaningful results until allocate has been called

  glm::ivec4 rgbaSizes() const noexcept;

  internal_format::Colour internalFormat() const;
};

class DepthStencilTexture1DArray final : public Texture1DArrayBase {
public:
  GLOBJECT_CONSTRUCTOR_HELPER(DepthStencilTexture1DArray, Texture1DArrayBase)

  // The following functions will only work after calling bind()!

  // Allocates the texture and fills it with the given depth/stencil data. Passing
  // a null pointer for data will leave the texture in an uninitialized state
  void allocate(internal_format::DepthStencil internalFormat,
                sizei_t width, sizei_t length, format::DepthStencil dataFormat,
                ImageType dataType, const void* data) noexcept;
  // Updates a region of the texture between (offset, index) and
  // (offset + width, index + length) with the given depth/stencil data.
  // data must not be null.
  void update(int_t offset, int_t index, sizei_t width, sizei_t length,
              format::Colour dataFormat, ImageType dataType,
              const void* data) noexcept;

  // The following functions will not return meaningful results until allocate has been called

  sizei_t depthSize() const noexcept;
  sizei_t stencilSize() const noexcept;

  internal_format::DepthStencil internalFormat() const;
};

} // namespace gl
} // namespace graphics

#endif // GRAPHICS_GL_TEXTURE_1D_ARRAY_H
