#include "VertexBuffer.inl"
#include "../../GLMIncludes.h"

#include <algorithm>
#include <sstream>
#include <tuple>

#include "../../Assert.h"
#include "../../Exception.h"

namespace graphics {
namespace gl {
namespace detail {

GLenum bindTargetCode(VertexBuffer::Target target) noexcept {
  GLenum bindTargets[] = {
    GL_ARRAY_BUFFER,
    GL_COPY_READ_BUFFER,
    GL_COPY_WRITE_BUFFER,
  };
  auto targetBase = static_cast<std::underlying_type_t<VertexBuffer::Target>>(target);
  ASSERT(0 <= targetBase && targetBase < std::extent<decltype(bindTargets)>::value);
  return bindTargets[targetBase];
}

GLenum bufferUsageCode(VertexBuffer::Usage usage) noexcept {
  GLenum bufferUsages[] = {
    GL_STATIC_DRAW,
    GL_STATIC_READ,
    GL_STATIC_COPY,
    GL_DYNAMIC_DRAW,
    GL_DYNAMIC_READ,
    GL_DYNAMIC_COPY,
    GL_STREAM_DRAW,
    GL_STREAM_READ,
    GL_STREAM_COPY,
  };
  auto usageBase = static_cast<std::underlying_type_t<VertexBuffer::Target>>(usage);
  ASSERT(0 <= usageBase && usageBase < std::extent<decltype(bufferUsages)>::value);
  return bufferUsages[usageBase];
}

GLenum bufferAccessCode(VertexBuffer::Access access) noexcept {
  GLenum bufferAccessCodes[] = {
    GL_READ_ONLY,
    GL_WRITE_ONLY,
    GL_READ_WRITE,
  };
  auto accessBase = static_cast<std::underlying_type_t<VertexBuffer::Access>>(access);
  ASSERT(0 <= accessBase && accessBase < std::extent<decltype(bufferAccessCodes)>::value);
  return bufferAccessCodes[accessBase];
}
GLbitfield bufferAccessBitfield(VertexBuffer::Access access) noexcept {
  GLbitfield bitmasks[] = {
    GL_MAP_READ_BIT,
    GL_MAP_WRITE_BIT,
    GL_MAP_READ_BIT | GL_MAP_WRITE_BIT,
  };
  auto accessBase = static_cast<std::underlying_type_t<VertexBuffer::Access>>(access);
  ASSERT(0 <= accessBase && accessBase < std::extent<decltype(bitmasks)>::value);
  return bitmasks[accessBase];
}

} // namespace detail

void VertexBuffer::bind(Target target) noexcept {
  ASSERT(m_id != 0);
  glBindBuffer(detail::bindTargetCode(target), m_id);
}
void VertexBuffer::unbind(Target target) noexcept {
  glBindBuffer(detail::bindTargetCode(target), 0);
}

void VertexBuffer::allocate(sizeiptr_t size, Usage usage, Target target) noexcept {
  glBufferData(detail::bindTargetCode(target),
               size, nullptr,
               detail::bufferUsageCode(usage));
}

template <>
void bindToAttributeNonMatrixImpl<false>(
  const AttributeID& id, int_t components, enum_t typeID, bool /*normalized*/, intptr_t offset) noexcept
{
  // Disallow floats to be normalized
  glVertexAttribPointer(id.id, components, typeID, GL_FALSE, 0, reinterpret_cast<void*>(offset));
}

void VertexBuffer::bindToAttributes(const InterleavedFormat& format) noexcept {
  for (const auto& t : format.m_formatData) {
    AttributeID attribID;
    enum_t glType = static_cast<enum_t>(0);
    int_t components = static_cast<int_t>(0);
    bool isIntType = false;
    bool normalized = false;
    sizei_t stride = static_cast<sizei_t>(0);
    intptr_t offset = static_cast<intptr_t>(0);

    std::tie(attribID, glType, components, isIntType, normalized, stride, offset) = t;
    if (isIntType && !normalized) {
      glVertexAttribIPointer(attribID.id, components, glType, stride,
                             reinterpret_cast<void*>(offset));
    } else {
      glVertexAttribPointer(attribID.id, components, glType,
                            normalized ? GL_TRUE : GL_FALSE, stride,
                            reinterpret_cast<void*>(offset));
    }
  }
}

void* VertexBuffer::map(Target target, Access access) noexcept {
  return glMapBuffer(detail::bindTargetCode(target),
                     detail::bufferAccessCode(access));
}
void* VertexBuffer::map(intptr_t offset, sizeiptr_t length,
                        Target target, Access access) noexcept
{
  return glMapBufferRange(detail::bindTargetCode(target),
                          offset, length,
                          detail::bufferAccessBitfield(access));
}
void VertexBuffer::unmap(Target target) {
  auto result = glUnmapBuffer(detail::bindTargetCode(target));
  if (result != GL_TRUE) {
    std::ostringstream ss;
    ss << "Failed to transfer mapped data to OpenGL!";
    throw MAKE_EXCEPTION(ss.str());
  }
}

VertexBuffer::InterleavedFormat::InterleavedFormat(
    std::initializer_list<AttributeFormat> init)
  : m_formatData(init)
{}

// allocate(): Force instantiation of known types
#define INST_ALLOCATE(T) \
  template void VertexBuffer::allocate<T>(sizeiptr_t, const T*, Usage, Target) noexcept

// Primatives
INST_ALLOCATE(byte_t);
INST_ALLOCATE(ubyte_t);
INST_ALLOCATE(short_t);
INST_ALLOCATE(ushort_t);
INST_ALLOCATE(int_t);
INST_ALLOCATE(uint_t);
INST_ALLOCATE(float_t);
INST_ALLOCATE(double_t);
// GLM
// vec2
INST_ALLOCATE(glm::vec2);
INST_ALLOCATE(glm::dvec2);
INST_ALLOCATE(glm::ivec2);
INST_ALLOCATE(glm::uvec2);
// vec3
INST_ALLOCATE(glm::vec3);
INST_ALLOCATE(glm::dvec3);
INST_ALLOCATE(glm::ivec3);
INST_ALLOCATE(glm::uvec3);
// vec4
INST_ALLOCATE(glm::vec4);
INST_ALLOCATE(glm::dvec4);
INST_ALLOCATE(glm::ivec4);
INST_ALLOCATE(glm::uvec4);

// mat2
INST_ALLOCATE(glm::mat2);
INST_ALLOCATE(glm::dmat2);
// mat2x3
INST_ALLOCATE(glm::mat2x3);
INST_ALLOCATE(glm::dmat2x3);
// mat2x4
INST_ALLOCATE(glm::mat2x4);
INST_ALLOCATE(glm::dmat2x4);
// mat3
INST_ALLOCATE(glm::mat3);
INST_ALLOCATE(glm::dmat3);
// mat3x2
INST_ALLOCATE(glm::mat3x2);
INST_ALLOCATE(glm::dmat3x2);
// mat3x4
INST_ALLOCATE(glm::mat3x4);
INST_ALLOCATE(glm::dmat3x4);
// mat4
INST_ALLOCATE(glm::mat4);
INST_ALLOCATE(glm::dmat4);
// mat4x2
INST_ALLOCATE(glm::mat4x2);
INST_ALLOCATE(glm::dmat4x2);
// mat4x3
INST_ALLOCATE(glm::mat4x3);
INST_ALLOCATE(glm::dmat4x3);

// quat
INST_ALLOCATE(glm::quat);
INST_ALLOCATE(glm::dquat);
// dualquat
INST_ALLOCATE(glm::dualquat);
INST_ALLOCATE(glm::ddualquat);

#undef INST_ALLOCATE

// update(): Force instantiation of known types
#define INST_UPDATE(T) \
  template void VertexBuffer::update<T>(intptr_t, std::size_t, const T*, Target) noexcept

// Primatives
INST_UPDATE(byte_t);
INST_UPDATE(ubyte_t);
INST_UPDATE(short_t);
INST_UPDATE(ushort_t);
INST_UPDATE(int_t);
INST_UPDATE(uint_t);
INST_UPDATE(float_t);
INST_UPDATE(double_t);
// GLM
// vec2
INST_UPDATE(glm::vec2);
INST_UPDATE(glm::dvec2);
INST_UPDATE(glm::ivec2);
INST_UPDATE(glm::uvec2);
// vec3
INST_UPDATE(glm::vec3);
INST_UPDATE(glm::dvec3);
INST_UPDATE(glm::ivec3);
INST_UPDATE(glm::uvec3);
// vec4
INST_UPDATE(glm::vec4);
INST_UPDATE(glm::dvec4);
INST_UPDATE(glm::ivec4);
INST_UPDATE(glm::uvec4);

// mat2
INST_UPDATE(glm::mat2);
INST_UPDATE(glm::dmat2);
// mat2x3
INST_UPDATE(glm::mat2x3);
INST_UPDATE(glm::dmat2x3);
// mat2x4
INST_UPDATE(glm::mat2x4);
INST_UPDATE(glm::dmat2x4);
// mat3
INST_UPDATE(glm::mat3);
INST_UPDATE(glm::dmat3);
// mat3x2
INST_UPDATE(glm::mat3x2);
INST_UPDATE(glm::dmat3x2);
// mat3x4
INST_UPDATE(glm::mat3x4);
INST_UPDATE(glm::dmat3x4);
// mat4
INST_UPDATE(glm::mat4);
INST_UPDATE(glm::dmat4);
// mat4x2
INST_UPDATE(glm::mat4x2);
INST_UPDATE(glm::dmat4x2);
// mat4x3
INST_UPDATE(glm::mat4x3);
INST_UPDATE(glm::dmat4x3);

// quat
INST_UPDATE(glm::quat);
INST_UPDATE(glm::dquat);
// dualquat
INST_UPDATE(glm::dualquat);
INST_UPDATE(glm::ddualquat);

#undef INST_UPDATE

// bindToAttributeNonMatrixImpl(): Force instantiation for both versions
template void bindToAttributeNonMatrixImpl<true>(const AttributeID&, int_t, enum_t, bool, intptr_t) noexcept;
template void bindToAttributeNonMatrixImpl<false>(const AttributeID&, int_t, enum_t, bool, intptr_t) noexcept;

// bindToAttribute(): Force instantiation of known non-matrix types
// InterleavedFormat::addAttribute(): same
#define INST_ATTRIB1(T) \
  template std::enable_if_t<!util::isMatrixType<T>::value> \
  VertexBuffer::bindToAttribute<T>(const AttributeID&, bool, intptr_t) noexcept; \
  template std::enable_if_t<!util::isMatrixType<T>::value> \
  VertexBuffer::InterleavedFormat::addAttribute<T>(const AttributeID&, bool, sizei_t, intptr_t)

// Primatives
INST_ATTRIB1(byte_t);
INST_ATTRIB1(ubyte_t);
INST_ATTRIB1(short_t);
INST_ATTRIB1(ushort_t);
INST_ATTRIB1(int_t);
INST_ATTRIB1(uint_t);
INST_ATTRIB1(float_t);
INST_ATTRIB1(double_t);
// GLM
// vec2
INST_ATTRIB1(glm::vec2);
INST_ATTRIB1(glm::dvec2);
INST_ATTRIB1(glm::ivec2);
INST_ATTRIB1(glm::uvec2);
// vec3
INST_ATTRIB1(glm::vec3);
INST_ATTRIB1(glm::dvec3);
INST_ATTRIB1(glm::ivec3);
INST_ATTRIB1(glm::uvec3);
// vec4
INST_ATTRIB1(glm::vec4);
INST_ATTRIB1(glm::dvec4);
INST_ATTRIB1(glm::ivec4);
INST_ATTRIB1(glm::uvec4);

// quat
INST_ATTRIB1(glm::quat);
INST_ATTRIB1(glm::dquat);

#undef INST_ATTRIB1

// bindToAttribute(): Force instantiation of known matrix types
// InterleavedFormat::addAttribute(): same
#define INST_ATTRIB2(T) \
  template std::enable_if_t<util::isMatrixType<T>::value> \
  VertexBuffer::bindToAttribute<T>(const AttributeID&, intptr_t) noexcept; \
  template std::enable_if_t<util::isMatrixType<T>::value> \
  VertexBuffer::InterleavedFormat::addAttribute<T>(const AttributeID&, sizei_t, intptr_t)

// mat2
INST_ATTRIB2(glm::mat2);
INST_ATTRIB2(glm::dmat2);
// mat2x3
INST_ATTRIB2(glm::mat2x3);
INST_ATTRIB2(glm::dmat2x3);
// mat2x4
INST_ATTRIB2(glm::mat2x4);
INST_ATTRIB2(glm::dmat2x4);
// mat3
INST_ATTRIB2(glm::mat3);
INST_ATTRIB2(glm::dmat3);
// mat3x2
INST_ATTRIB2(glm::mat3x2);
INST_ATTRIB2(glm::dmat3x2);
// mat3x4
INST_ATTRIB2(glm::mat3x4);
INST_ATTRIB2(glm::dmat3x4);
// mat4
INST_ATTRIB2(glm::mat4);
INST_ATTRIB2(glm::dmat4);
// mat4x2
INST_ATTRIB2(glm::mat4x2);
INST_ATTRIB2(glm::dmat4x2);
// mat4x3
INST_ATTRIB2(glm::mat4x3);
INST_ATTRIB2(glm::dmat4x3);

// dualquat
INST_ATTRIB2(glm::dualquat);
INST_ATTRIB2(glm::ddualquat);

#undef INST_ATTRIB2

// mapAs(): Force instantiation of known types
#define INST_MAPAS(T) \
  template T* VertexBuffer::mapAs<T>(Target, Access) noexcept; \
  template T* VertexBuffer::mapAs<T>(intptr_t, std::size_t, Target, Access) noexcept; \
  template const T* VertexBuffer::mapAsConst<T>(Target, Access) noexcept; \
  template const T* VertexBuffer::mapAsConst<T>(intptr_t, std::size_t, Target, Access) noexcept

// Primatives
INST_MAPAS(byte_t);
INST_MAPAS(ubyte_t);
INST_MAPAS(short_t);
INST_MAPAS(ushort_t);
INST_MAPAS(int_t);
INST_MAPAS(uint_t);
INST_MAPAS(float_t);
INST_MAPAS(double_t);
// GLM
// vec2
INST_MAPAS(glm::vec2);
INST_MAPAS(glm::dvec2);
INST_MAPAS(glm::ivec2);
INST_MAPAS(glm::uvec2);
// vec3
INST_MAPAS(glm::vec3);
INST_MAPAS(glm::dvec3);
INST_MAPAS(glm::ivec3);
INST_MAPAS(glm::uvec3);
// vec4
INST_MAPAS(glm::vec4);
INST_MAPAS(glm::dvec4);
INST_MAPAS(glm::ivec4);
INST_MAPAS(glm::uvec4);

// quat
INST_MAPAS(glm::quat);
INST_MAPAS(glm::dquat);
// dualquat
INST_MAPAS(glm::dualquat);
INST_MAPAS(glm::ddualquat);

// mat2
INST_MAPAS(glm::mat2);
INST_MAPAS(glm::dmat2);
// mat2x3
INST_MAPAS(glm::mat2x3);
INST_MAPAS(glm::dmat2x3);
// mat2x4
INST_MAPAS(glm::mat2x4);
INST_MAPAS(glm::dmat2x4);
// mat3
INST_MAPAS(glm::mat3);
INST_MAPAS(glm::dmat3);
// mat3x2
INST_MAPAS(glm::mat3x2);
INST_MAPAS(glm::dmat3x2);
// mat3x4
INST_MAPAS(glm::mat3x4);
INST_MAPAS(glm::dmat3x4);
// mat4
INST_MAPAS(glm::mat4);
INST_MAPAS(glm::dmat4);
// mat4x2
INST_MAPAS(glm::mat4x2);
INST_MAPAS(glm::dmat4x2);
// mat4x3
INST_MAPAS(glm::mat4x3);
INST_MAPAS(glm::dmat4x3);

#undef INST_MAPAS

} // namespace gl
} // namespace graphics
