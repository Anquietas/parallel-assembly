#include "VertexArrayObject.h"
#include "OpenGLIncludes.h"

#include "GLObject.inl"

#include "../../Assert.h"

namespace graphics {
namespace gl {

void VertexArrayObject::bind() noexcept {
  ASSERT(m_id != 0);
  glBindVertexArray(m_id);
}
void VertexArrayObject::unbind() noexcept {
  glBindVertexArray(0);
}

void VertexArrayObject::enableAttribute(const AttributeID& id) noexcept {
  glEnableVertexAttribArray(id.id);
}
void VertexArrayObject::disableAttribute(const AttributeID& id) noexcept {
  glDisableVertexAttribArray(id.id);
}

void VertexArrayObject::setAttributeDivisor(const AttributeID& id, uint_t divisor) noexcept {
  glVertexAttribDivisor(id.id, divisor);
}

} // namespace gl
} // namespace graphics
