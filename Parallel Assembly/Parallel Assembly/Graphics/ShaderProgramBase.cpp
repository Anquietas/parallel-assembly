#include "ShaderProgramBase.h"

#include <utility>

#include "../Logger.h"

namespace graphics {

namespace {
STATIC_LOGGER_DECLARE(logger);
}

ShaderProgramBase::ShaderProgramBase(std::string name_) noexcept
  : m_name(std::move(name_))
  , m_initialized(false)
{}

ShaderProgramBase::ShaderProgramBase(ShaderProgramBase&& o) noexcept
  : m_name(std::move(o.m_name))
  , m_initialized(std::exchange(o.m_initialized, false))
{}

ShaderProgramBase::~ShaderProgramBase() {
  if (isInitialized()) {
    LOG_SEVERITY(logger::get(), Warning)
      << "Shader appears to have not been cleaned up properly at destruction: " << name();
  }
}

ShaderProgramBase& ShaderProgramBase::operator=(ShaderProgramBase&& o) noexcept {
  if (isInitialized())
    cleanup();

  m_name = std::move(o.m_name);
  m_initialized = std::exchange(o.m_initialized, false);

  return *this;
}

void ShaderProgramBase::initialize() {
  setInitialized();
}

void ShaderProgramBase::cleanup() {
  clearInitialized();
}

namespace {
STATIC_LOGGER_INIT(logger) {
  logging::Logger lg;
  logging::setModuleName(lg, "ShaderProgramBase");
  return lg;
}
}

} // namespace graphics
