#include "LevelGridShaderProgram.h"

#include <sstream>
#include <array>

#include "OpenGL/checkError.h"
#include "OpenGL/OpenGLIncludes.h"

#include "../Exception.h"

namespace graphics {

using namespace std::string_literals;

LevelGridShaderProgram::LevelGridShaderProgram()
  : ShaderProgramBase("LevelGridShader"s)
  , m_shader()
  , m_vertexPosLoc()
  , m_colourLoc()
  , m_mvpMatrixUniformLoc()
  , m_gridCellSizeUniformLoc()
  , m_gridWidthUniformLoc()
{}

namespace {
const std::string vertexShaderFileName{"Assets/Shaders/LevelGrid-vert.glsl"s};
const std::string fragmentShaderFileName{"Assets/Shaders/LevelGrid-frag.glsl"s};
}

void LevelGridShaderProgram::initialize() {
  ShaderProgramBase::initialize();

  m_shader.checkedAddShadersFromFiles(vertexShaderFileName, fragmentShaderFileName);
  m_shader.checkedLink(name());
  m_shader.dumpShaders();

  m_vertexPosLoc = m_shader.getAttributeLocation("vertexPos");
  m_colourLoc = m_shader.getAttributeLocation("colour");
  m_mvpMatrixUniformLoc = m_shader.getUniformLocation("mvpMatrix");
  m_gridCellSizeUniformLoc = m_shader.getUniformLocation("gridCellSize");
  m_gridWidthUniformLoc = m_shader.getUniformLocation("gridWidth");

  CHECK_GL_ERROR();
}
void LevelGridShaderProgram::cleanup() {
  ShaderProgramBase::cleanup();
  m_shader.destroy();

  CHECK_GL_ERROR();
}

void LevelGridShaderProgram::preRender() {
  m_shader.bind();
}
void LevelGridShaderProgram::postRender() {
  m_shader.unbind();
}

void LevelGridShaderProgram::setMVPMatrix(const glm::mat4& mvpMatrix) {
  m_shader.setUniform(m_mvpMatrixUniformLoc, mvpMatrix);
}
void LevelGridShaderProgram::setGridCellSize(gl::float_t gridCellSize) {
  m_shader.setUniform(m_gridCellSizeUniformLoc, gridCellSize);
}
void LevelGridShaderProgram::setGridSize(const glm::ivec2& gridSize) {
  m_shader.setUniform(m_gridWidthUniformLoc, gridSize.x);
}

} // namespace graphics
