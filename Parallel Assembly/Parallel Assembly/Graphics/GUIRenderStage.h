#pragma once
#ifndef GRAPHICS_GUI_RENDERSTAGE_H
#define GRAPHICS_GUI_RENDERSTAGE_H

#include <CEGUI/System.h>

#include "RenderStage.h"

namespace graphics {

class GUIRenderStage : public RenderStage {
  CEGUI::System* m_guiSystem; // non-owning
public:
  GUIRenderStage(const glm::ivec2& screenSize);
  GUIRenderStage(const GUIRenderStage&) = delete;
  GUIRenderStage(GUIRenderStage&& o) = default;
  virtual ~GUIRenderStage() override = default;

  GUIRenderStage& operator=(const GUIRenderStage&) = delete;
  GUIRenderStage& operator=(GUIRenderStage&& o) = default;

  virtual void draw() override;

  virtual void notifyContextRebuildImminent() override;
  virtual void notifyContextRebuildComplete() override;

  virtual void onResize(const glm::ivec2& newSize) override;
};

}

#endif // GRAPHICS_GUI_RENDERSTAGE_H
