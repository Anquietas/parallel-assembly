#pragma once
#ifndef GRAPHICS_LEVEL_WORKER_PATH_SHADER_PROGRAM_H
#define GRAPHICS_LEVEL_WORKER_PATH_SHADER_PROGRAM_H

#include "ShaderProgramBase.h"

#include "OpenGL/ShaderProgram.h"
#include "OpenGL/Texture2D.h"
#include "OpenGL/Sampler.h"

namespace graphics {

class LevelWorkerPathShaderProgram : public ShaderProgramBase {
  gl::ShaderProgram m_shader;
  gl::AttributeID m_vertexPosLoc;
  gl::AttributeID m_texCoordsLoc;
  gl::UniformID m_mvpMatrixUniformLoc;
  gl::UniformID m_gridCellSizeUniformLoc;
  gl::UniformID m_pathSamplerUniformLoc;
  gl::UniformID m_backgroundColourUniformLoc;
  gl::UniformID m_foregroundColourUniformLoc;

  gl::TextureUnit m_pathTexUnit;
  gl::ColourTexture2D m_pathTexture;
  gl::Sampler m_pathSampler;
public:
  LevelWorkerPathShaderProgram();
  LevelWorkerPathShaderProgram(LevelWorkerPathShaderProgram&& o) = default;
  virtual ~LevelWorkerPathShaderProgram() override = default;

  LevelWorkerPathShaderProgram& operator=(LevelWorkerPathShaderProgram&& o) = default;

  virtual void initialize() override;
  virtual void cleanup() override;

  virtual void preRender() override;
  virtual void postRender() override;

  // Only valid after initialization

  const gl::AttributeID& vertexPosAttribID() const noexcept { return m_vertexPosLoc; }
  const gl::AttributeID& texCoordsAttribID() const noexcept { return m_texCoordsLoc; }

  // The following functions should only be called between preRender() and postRender()

  // Sets the shader's mvpMatrix uniform to the given value
  void setMVPMatrix(const glm::mat4& mvpMatrix);
  // Sets the shader's gridCellSize uniform to the given value
  void setGridCellSize(gl::float_t gridCellSize);
  // Sets the shader's backgroundColour uniform to the given value
  void setBackgroundColour(const glm::vec3& colour);
  // Sets the shader's foregroundColour uniform to the given value
  void setForegroundColour(const glm::vec3& colour);
};

} // namespace graphics

#endif // GRAPHICS_LEVEL_WORKER_PATH_SHADER_PROGRAM_H
