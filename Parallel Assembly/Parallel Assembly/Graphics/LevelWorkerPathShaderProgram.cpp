#include "LevelWorkerPathShaderProgram.h"

#include <sstream>

#include "OpenGL/OpenGLIncludes.h"
#include "OpenGL/TextureLoader.h"

#include "../Assert.h"
#include "../Exception.h"

namespace graphics {

using namespace std::string_literals;

LevelWorkerPathShaderProgram::LevelWorkerPathShaderProgram()
  : ShaderProgramBase("LevelWorkerPathShader"s)
  , m_shader()
  , m_vertexPosLoc()
  , m_texCoordsLoc()
  , m_mvpMatrixUniformLoc()
  , m_gridCellSizeUniformLoc()
  , m_pathSamplerUniformLoc()
  , m_backgroundColourUniformLoc()
  , m_foregroundColourUniformLoc()
  , m_pathTexUnit(1)
  , m_pathTexture(nullptr)
  , m_pathSampler(nullptr)
{}

namespace {
const std::string vertexShaderFileName{"Assets/Shaders/WorkerPath-vert.glsl"s};
const std::string fragmentShaderFileName{"Assets/Shaders/WorkerPath-frag.glsl"s};

const std::string textureFileName{"Assets/Textures/WorkerPath.png"s};
}

void LevelWorkerPathShaderProgram::initialize() {
  ShaderProgramBase::initialize();

  m_shader.checkedAddShadersFromFiles(vertexShaderFileName, fragmentShaderFileName);
  m_shader.checkedLink(name());
  m_shader.dumpShaders();

  m_vertexPosLoc = m_shader.getAttributeLocation("vertexPos");
  m_texCoordsLoc = m_shader.getAttributeLocation("texCoords");
  m_mvpMatrixUniformLoc = m_shader.getUniformLocation("mvpMatrix");
  m_gridCellSizeUniformLoc = m_shader.getUniformLocation("gridCellSize");
  m_pathSamplerUniformLoc = m_shader.getUniformLocation("pathTexture");
  m_backgroundColourUniformLoc = m_shader.getUniformLocation("backgroundColour");
  m_foregroundColourUniformLoc = m_shader.getUniformLocation("foregroundColour");

  m_pathTexUnit.setActive();
  m_pathTexture.init();
  m_pathTexture.bind();
  gl::loadTexture2D(textureFileName, m_pathTexture);
  m_pathTexture.generateMipMaps();
  
  m_pathSampler.init();
  ASSERT(m_pathSampler.wrapModeS() == gl::Sampler::WrapMode::Repeat);
  ASSERT(m_pathSampler.wrapModeT() == gl::Sampler::WrapMode::Repeat);
  m_pathSampler.setMinFilter(gl::Sampler::MinifyingFilter::LinearMipmapLinear);
  m_pathSampler.setMagFilter(gl::Sampler::MagnifyingFilter::Linear);

  glActiveTexture(GL_TEXTURE0);
}
void LevelWorkerPathShaderProgram::cleanup() {
  ShaderProgramBase::cleanup();
  m_shader.destroy();

  m_pathTexture.destroy();
  m_pathSampler.destroy();
}

void LevelWorkerPathShaderProgram::preRender() {
  m_shader.bind();

  m_pathTexUnit.setActive();
  m_pathTexture.bind();

  m_pathSampler.bind(m_pathTexUnit);

  m_shader.setUniform(m_pathSamplerUniformLoc, m_pathTexUnit);
}
void LevelWorkerPathShaderProgram::postRender() {
  gl::Sampler::unbind(m_pathTexUnit);

  m_shader.unbind();

  glActiveTexture(GL_TEXTURE0);
}

void LevelWorkerPathShaderProgram::setMVPMatrix(const glm::mat4& mvpMatrix) {
  m_shader.setUniform(m_mvpMatrixUniformLoc, mvpMatrix);
}
void LevelWorkerPathShaderProgram::setGridCellSize(gl::float_t gridCellSize) {
  m_shader.setUniform(m_gridCellSizeUniformLoc, gridCellSize);
}
void LevelWorkerPathShaderProgram::setBackgroundColour(const glm::vec3& colour) {
  m_shader.setUniform(m_backgroundColourUniformLoc, colour);
}
void LevelWorkerPathShaderProgram::setForegroundColour(const glm::vec3& colour) {
  m_shader.setUniform(m_foregroundColourUniformLoc, colour);
}

} // namespace graphics
