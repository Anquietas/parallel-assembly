#include "DirectionArrowShaderProgram.h"

#include <sstream>

#include "OpenGL/OpenGLIncludes.h"
#include "OpenGL/TextureLoader.h"

#include "../Exception.h"

namespace graphics {

using namespace std::string_literals;

DirectionArrowShaderProgram::DirectionArrowShaderProgram()
  : ShaderProgramBase("DirectionArrowShader"s)
  , m_shader()
  , m_vertexPosLoc()
  , m_texCoordsLoc()
  , m_offsetLoc()
  , m_directionLoc()
  , m_stateLoc()
  , m_mvpMatrixUniformLoc()
  , m_gridCellSizeUniformLoc()
  , m_arrowSamplerUniformLoc()
  , m_arrowColourUniformLoc()
  , m_borderColourUniformLoc()
  , m_arrowTexUnit(2)
  , m_arrowTexture(nullptr)
  , m_arrowSampler(nullptr)
{}

namespace {
const std::string vertexShaderFileName{"Assets/Shaders/DirectionArrow-vert.glsl"s};
const std::string fragmentShaderFileName{"Assets/Shaders/DirectionArrow-frag.glsl"s};

const std::string textureFileName{"Assets/Textures/DirectionArrow.png"s};
}

void DirectionArrowShaderProgram::initialize() {
  ShaderProgramBase::initialize();

  m_shader.checkedAddShadersFromFiles(vertexShaderFileName, fragmentShaderFileName);
  m_shader.checkedLink(name());
  m_shader.dumpShaders();

  m_vertexPosLoc = m_shader.getAttributeLocation("vertexPos");
  m_texCoordsLoc = m_shader.getAttributeLocation("texCoords");
  m_offsetLoc = m_shader.getAttributeLocation("offset");
  m_directionLoc = m_shader.getAttributeLocation("direction");
  m_stateLoc = m_shader.getAttributeLocation("state");
  m_mvpMatrixUniformLoc = m_shader.getUniformLocation("mvpMatrix");
  m_gridCellSizeUniformLoc = m_shader.getUniformLocation("gridCellSize");
  m_arrowSamplerUniformLoc = m_shader.getUniformLocation("arrowTexture");
  m_arrowColourUniformLoc = m_shader.getUniformLocation("arrowColour");
  m_borderColourUniformLoc = m_shader.getUniformLocation("borderColour");

  m_arrowTexUnit.setActive();
  m_arrowTexture.init();
  m_arrowTexture.bind();
  gl::loadTexture2D(textureFileName, m_arrowTexture);
  m_arrowTexture.generateMipMaps();

  m_arrowSampler.init();
  m_arrowSampler.setAllWrapModes(gl::Sampler::WrapMode::ClampToEdge);
  m_arrowSampler.setMinFilter(gl::Sampler::MinifyingFilter::LinearMipmapLinear);
  m_arrowSampler.setMagFilter(gl::Sampler::MagnifyingFilter::Linear);

  glActiveTexture(GL_TEXTURE0);
}
void DirectionArrowShaderProgram::cleanup() {
  ShaderProgramBase::cleanup();
  m_shader.destroy();

  m_arrowTexture.destroy();
  m_arrowSampler.destroy();
}

void DirectionArrowShaderProgram::preRender() {
  m_shader.bind();

  m_arrowTexUnit.setActive();
  m_arrowTexture.bind();

  m_arrowSampler.bind(m_arrowTexUnit);

  m_shader.setUniform(m_arrowSamplerUniformLoc, m_arrowTexUnit);
}
void DirectionArrowShaderProgram::postRender() {
  gl::Sampler::unbind(m_arrowTexUnit);

  m_shader.unbind();

  glActiveTexture(GL_TEXTURE0);
}

void DirectionArrowShaderProgram::setMVPMatrix(const glm::mat4& mvpMatrix) {
  m_shader.setUniform(m_mvpMatrixUniformLoc, mvpMatrix);
}
void DirectionArrowShaderProgram::setGridCellSize(gl::float_t gridCellSize) {
  m_shader.setUniform(m_gridCellSizeUniformLoc, gridCellSize);
}
void DirectionArrowShaderProgram::setArrowColour(const glm::vec3& colour) {
  m_shader.setUniform(m_arrowColourUniformLoc, colour);
}
void DirectionArrowShaderProgram::setBorderColour(const glm::vec3& colour) {
  m_shader.setUniform(m_borderColourUniformLoc, colour);
}

} // namespace graphics
