#include "PlatformInfo.h"

#include <sstream>
#include <vector>

#include <SDL.h>

#include <boost/core/ignore_unused.hpp>

#include "Graphics/OpenGL/checkError.h"
#include "Graphics/OpenGL/OpenGLIncludes.h"
#include "Graphics/OpenGL/Types.h"

#include "Assert.h"
#include "Exception.h"
#include "Logger.h"

void logPlatformInfo() {
  logging::Logger logger;
  logging::setModuleName(logger, "PlatformInfo");

  std::ostringstream ss;
  ss << "Platform Information:\n";
  ss << "Platform: " << SDL_GetPlatform() << '\n';
  ss << "CPU Cores (logical): " << SDL_GetCPUCount() << '\n';
  ss << "CPU Cache line: " << SDL_GetCPUCacheLineSize() << " KB\n";
  ss << "SSE: " << (SDL_HasSSE() ? "yes" : "no") << '\n';
  ss << "SSE2: " << (SDL_HasSSE2() ? "yes" : "no") << '\n';
  ss << "SSE3: " << (SDL_HasSSE3() ? "yes" : "no") << '\n';
  ss << "SSE4.1: " << (SDL_HasSSE41() ? "yes" : "no") << '\n';
  ss << "SSE4.2: " << (SDL_HasSSE42() ? "yes" : "no") << '\n';
  ss << "AVX: " << (SDL_HasAVX() ? "yes" : "no") << '\n';
  ss << "AVX2: " << (SDL_HasAVX2() ? "yes" : "no") << '\n';
  ss << "RAM: " << SDL_GetSystemRAM() << " MB\n";
  int numDisplays = SDL_GetNumVideoDisplays();
  ss << "Detected " << numDisplays << " display devices\n";
  for (int i = 0; i < numDisplays; ++i) {
    ss << "Display " << i << ":\n";
    auto displayName = SDL_GetDisplayName(i);
    if (!displayName) {
      std::ostringstream error;
      error << "Failed to get display name for display " << i << ": " << SDL_GetError();
      throw MAKE_EXCEPTION(error.str());
    }
    ss << "  " << displayName << '\n';
    SDL_DisplayMode displayMode;
    if (SDL_GetCurrentDisplayMode(i, &displayMode)) {
      std::ostringstream error;
      error << "Failed to get display mode for display " << i << ": " << SDL_GetError();
      throw MAKE_EXCEPTION(error.str());
    }
    ss << "  " << displayMode.w << "x" << displayMode.h << " @ ";
    ss << displayMode.refresh_rate << " Hz\n";
  }
  int numDrivers = SDL_GetNumVideoDrivers();
  ss << "Detected " << numDrivers << " display drivers\n";
  for (int i = 0; i < numDrivers; ++i) {
    ss << "Driver " << i << ":\n";
    auto driverName = SDL_GetVideoDriver(i);
    if (!driverName) {
      std::ostringstream error;
      error << "Failed to get driver name for driver " << i << ": " << SDL_GetError();
      throw MAKE_EXCEPTION(error.str());
    }
    ss << "  " << driverName << '\n';
  }

  if (GLEW_NVX_gpu_memory_info) {
    ss << "<NVIDIA specific>\n";
    graphics::gl::int_t gpuMemoryKB = 0;
    glGetIntegerv(GL_GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX, &gpuMemoryKB);
    ss << "Video RAM: " << (gpuMemoryKB / 1024) << " MB\n";
    glGetIntegerv(GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX, &gpuMemoryKB);
    ss << "Total Available: " << (gpuMemoryKB / 1024) << " MB\n";
    glGetIntegerv(GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX, &gpuMemoryKB);
    ss << "Currently available: " << (gpuMemoryKB / 1024) << " MB\n";
  }
#ifdef _WIN32
  else if (WGLEW_AMD_gpu_association) {
    ss << "<Windows + AMD specific>\n";
    UINT numGPUs = wglGetGPUIDsAMD(0, nullptr);
    ASSERT(numGPUs > 0);  // OpenGL context creation would probably fail otherwise
    std::vector<UINT> gpuIDs(numGPUs);
    UINT result = wglGetGPUIDsAMD(numGPUs, gpuIDs.data());
    ASSERT(result == numGPUs); boost::ignore_unused(result);
    ss << "Detected " << numGPUs << " GPUs\n";
    for (UINT i = 0; i < numGPUs; ++i) {
      UINT gpuMemoryMB = 0;
      INT result2 = wglGetGPUInfoAMD(gpuIDs[i], WGL_GPU_RAM_AMD, GL_UNSIGNED_INT, 1, &gpuMemoryMB);
      if (result2 != 1) {
        std::ostringstream error;
        error << "Failed to read VRAM amount for GPU ID " << i << "[" << gpuIDs[i];
        error << "], return value was " << result2;
        throw MAKE_EXCEPTION(error.str());
      }
      ss << "GPU " << i << "[" << gpuIDs[i] << "] Video RAM: " << gpuMemoryMB << " MB\n";
    }
  }
#else
  // A reminder to my future self.
#error UNTESTED
  else if (GLXEW_AMD_gpu_association) {
    ss << "<Unix + AMD specific>\n";
    unsigned int numGPUs = glXGetGPUIDsAMD(0, nullptr);
    ASSERT(numGPUs > 0);
    std::vector<unsigned int> gpuIDs(numGPUs);
    unsigned int result = glXGetGPUIDsAMD(numGPUs, gpuIDs.data());
    ASSERT(result == numGPUs);
    ss << "Detected " << numGPUs << " GPUs\n";
    for (unsigned int i = 0; i < numGPUs; ++i) {
      unsigned int gpuMemoryMB = 0;
      int result2 = glXGetGPUInfoAMD(gpuIDs[i], GLX_GPU_RAM_AMD, GL_UNSIGNED_INT, 1, &gpuMemoryMB);
      if (result != 1) {
        std::ostringstream error;
        error << "Failed to read VRAM amount for GPU ID " << i << "[" << gpuIDs[i];
        error << "], return value was " << result2;
        throw MAKE_EXCEPTION(error.str());
      }
      ss << "GPU " << i << "[" << gpuIDs[i] << " Video RAM: " << gpuMemoryMB << " MB\n";
    }
  }
#endif
  ss << "\nOpenGL System Info:\n";
  ss << "Vendor: " << glGetString(GL_VENDOR) << '\n';
  ss << "Renderer: " << glGetString(GL_RENDERER) << '\n';
  ss << "Version: " << glGetString(GL_VERSION) << '\n';
  ss << "GLSL Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << '\n';
  graphics::gl::int_t numExtensions = 0;
  glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);
  ss << "\nGL_NUM_EXTENSIONS = " << numExtensions << '\n';
  ss << "Available Extensions:\n";
  for (graphics::gl::int_t i = 0; i < numExtensions - 1; ++i)
    ss << glGetStringi(GL_EXTENSIONS, i) << '\n';
  if (numExtensions > 0)
    ss << glGetStringi(GL_EXTENSIONS, numExtensions - 1);

  CHECK_GL_ERROR();

  LOG_SEVERITY(logger, Debug) << ss.str();
}
