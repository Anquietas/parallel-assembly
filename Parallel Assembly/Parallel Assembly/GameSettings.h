#pragma once
#ifndef GAME_SETTINGS_H
#define GAME_SETTINGS_H

#include <string>
#include <vector>
#include <memory>
#include <boost/program_options/parsers.hpp>

#include "Graphics/OpenGL/Types.h"

#include "GLMIncludes.h"

#include "PhoenixSingleton.h"

enum class DisplayMode {
  Windowed,
  Borderless,
  Fullscreen,
};

struct AntiAliasingLevel {
  // TODO: maybe add a type flag if/when FXAA/SMAA implemented?
  graphics::gl::uint_t level;
};

namespace detail {
namespace po = boost::program_options;
} // namespace detail

class GameSettings;
PHOENIX_SINGLETON_EXTERN_TEMPLATE(GameSettings);

class GameSettings final : public PhoenixSingleton<GameSettings> {
  friend class PhoenixSingleton<GameSettings>;

  std::string m_settingsFileName;
  bool m_settingsFileDirty;
  bool isSettingsFileDirty() const noexcept { return m_settingsFileDirty; }
  void setSettingsFileDirty() noexcept { m_settingsFileDirty = true; }
  void clearSettingsFileDirty() noexcept { m_settingsFileDirty = false; }

  // Video settings

  bool m_videoDirty;
  void setVideoDirty(bool fromBackup = false) noexcept {
    m_videoDirty = true;
    if (!fromBackup)
      setSettingsFileDirty();
  }

  glm::ivec2 m_screenResolution;
  std::vector<glm::ivec2> m_screenResolutions;
  glm::ivec2 m_nativeResolution;
  
  DisplayMode m_displayMode;

  bool m_aaEnabled;
  AntiAliasingLevel m_aaLevel;
  std::vector<AntiAliasingLevel> m_aaLevels;

  bool m_vsyncEnabled;

  // Audio settings

  bool m_audioDirty;
  void setAudioDirty(bool fromBackup = false) noexcept {
    m_audioDirty = true;
    if (!fromBackup)
      setSettingsFileDirty();
  }

  bool m_audioEnabled;
  float m_masterVolume;

  bool m_musicEnabled;
  float m_musicVolume;  // [0, 1]
  
  bool m_sfxEnabled;
  float m_sfxVolume;    // [0, 1]

  void populateScreenResolutions();
  void populateAALevels();
  void setDefaultValues();

  bool initializeImpl(detail::po::command_line_parser& cmdParser, const char* settingsFileName);
  void writeSettingsFile() const;

  GameSettings();
  GameSettings(const GameSettings& o) = default;
  GameSettings(GameSettings&&) = delete;

  GameSettings& operator=(const GameSettings&) = default;
  GameSettings& operator=(GameSettings&&) = delete;

  static bool settingsBackupInUse;
  static GameSettings settingsBackup; // For reverting from an apply
public:
  ~GameSettings() = default;

  static const glm::ivec2 minResolution;
  
  // Initialize from command line and settings file (windows version)
  // Returns true if the program should terminate after this returns
  // (e.g. because --help was an argument)
  bool initialize(const char* lpCmdLine, const char* settingsFileName);
  // Initialize from command line and settings file (unix version)
  // Returns true if the program should terminate after this returns
  // (e.g. because --help was an argument)
  bool initialize(int argc, const char* argv[], const char* settingsFileName);

  bool isVideoDirty() const noexcept { return m_videoDirty; }
  void clearVideoDirty() noexcept { m_videoDirty = false; }

  const glm::ivec2& screenResolution() const noexcept { return m_screenResolution; }
  bool setScreenResolution(const glm::ivec2& screenResolution);
  const std::vector<glm::ivec2>& getScreenResolutions() const noexcept { return m_screenResolutions; }

  const glm::ivec2& maxResolution() const noexcept { return m_nativeResolution; }

  DisplayMode displayMode() const noexcept { return m_displayMode; }
  void setDisplayMode(DisplayMode mode);

  bool isAAAvailable() const noexcept { return !m_aaLevels.empty(); }
  bool isAAEnabled() const noexcept { return m_aaEnabled; }
  bool setAAEnabled(bool enabled);
  const AntiAliasingLevel& aaLevel() const noexcept { return m_aaLevel; }
  bool setAALevel(const AntiAliasingLevel& level);
  const std::vector<AntiAliasingLevel>& getAALevels() const noexcept { return m_aaLevels; }

  bool isVSyncEnabled() const noexcept { return m_vsyncEnabled; }
  void setVSyncEnabled(bool enabled);

  bool isAudioDirty() const noexcept { return m_audioDirty; }
  void clearAudioDirty() noexcept { m_audioDirty = false; }

  bool isAudioEnabled() const noexcept { return m_audioEnabled; }
  void setAudioEnabled(bool enabled);
  float masterVolume() const noexcept { return m_masterVolume; }
  void setMasterVolume(float volume);

  bool isMusicEnabled() const noexcept { return m_musicEnabled; }
  void setMusicEnabled(bool enabled);
  float musicVolume() const noexcept { return m_musicVolume; }
  void setMusicVolume(float volume);

  bool isSFXEnabled() const noexcept { return m_sfxEnabled; }
  void setSFXEnabled(bool enabled);
  float sfxVolume() const noexcept { return m_sfxVolume; }
  void setSFXVolume(float volume);

  bool backupInUse() const noexcept { return settingsBackupInUse; }
  // Saves the current settings to a temporary backup to allow changing settings
  // and then reverting to the original.
  // - will throw an exception if backup is already in use
  void backup();
  // Commits to using the new settings and removes the backup
  // - will throw an exception if backup is not in use
  void commit();
  // Reverts to the backed up settings
  // - will throw an exception if backup is not in use
  void revertToBackup();
};

#endif // GAME_SETTINGS_H
