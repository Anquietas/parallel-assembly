#pragma warning(disable : 4503) // name length exceeded, caused by boost.log
#include "Logger.h"

#include <cstdlib>

#include <iostream>
#include <string>
#include <fstream>
#include <type_traits>

#pragma warning(disable : 4714) // __forceinline not inlined
#include <boost/date_time/posix_time/posix_time_types.hpp>

#include <boost/log/common.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/support/date_time.hpp>

#include "Assert.h"

namespace logging {

BOOST_LOG_ATTRIBUTE_KEYWORD(lineID, "LineID", unsigned int)
BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", Severity)
BOOST_LOG_ATTRIBUTE_KEYWORD(moduleName, "Module", std::string)

std::ostream& operator<<(std::ostream& out, Severity sev) {
  static constexpr const char* const strReps[] = {
    "Pedantic",
    "Profiling",
    "Debug",
    "Info",
    "Normal",
    "Warning",
    "Error",
    "Fatal",
  };

  auto underlying = static_cast<std::underlying_type_t<Severity>>(sev);
  ASSERT(0 <= underlying && underlying < std::extent<decltype(strReps)>::value);
  return out << strReps[underlying];
}

using file_sink_backend = sinks::text_file_backend;
using file_sink = sinks::synchronous_sink<file_sink_backend>;

using console_sink_backend = sinks::text_ostream_backend;
using console_sink = sinks::synchronous_sink<console_sink_backend>;

boost::shared_ptr<file_sink> fileSink;
boost::shared_ptr<console_sink> consoleSink;

void cleanUp() {
  try {
    logging::flush();
  } catch (...) {
    std::cerr << "Error: exception thrown by flushing logs";
  }
  fileSink.reset();
  consoleSink.reset();
}

void initialize() {

  // File log (logs everything)
  // Format: 000000 <TimeStamp> [Module]: [Severity]  Message
  boost::log::formatter fileFormatter = expr::stream
    << std::setw(6) << std::setfill('0') << lineID << std::setfill(' ')
    << " <" << expr::format_date_time<boost::posix_time::ptime>("TimeStamp", "%Y-%m-%d %H:%M:%S.%f") << '>'
    << expr::if_(expr::has_attr(moduleName)) [expr::stream << " [" << moduleName << "]"]
    << "\t[Thread " << expr::attr<attr::current_thread_id::value_type>("ThreadID") << "]"
    << ":\t[" << severity << "]\t" << expr::message;

  auto backend = boost::make_shared<file_sink_backend>(
    keywords::file_name = "logs/output_%Y-%m-%d_%H-%M-%S.log",
    keywords::auto_flush = true);
  auto collector = sinks::file::make_collector(
      keywords::target = "logs",
      keywords::max_files = 5
    );
  // Needed for the cleanup to actually work
  collector->scan_for_files(
    sinks::file::scan_matching,
    "output_%Y-%m-%d_%H-%M-%S.log",
    nullptr);
  fileSink = boost::make_shared<file_sink>(backend);
  fileSink->locked_backend()->set_file_collector(collector);

  fileSink->set_formatter(fileFormatter);
  fileSink->set_filter(severity >= minLogFileSeverity);

  boost::log::core::get()->add_sink(fileSink);

  // Console log
  boost::log::formatter consoleFormatter = expr::stream
    << expr::format_date_time<boost::posix_time::ptime>("TimeStamp", "%H:%M:%S.%f")
    << " " << expr::if_(expr::has_attr(moduleName))[expr::stream << " [" << moduleName << "]"]
    << ": [" << severity << "]\t" << expr::message;
  consoleSink = boost::log::add_console_log(std::cout);
  consoleSink->set_formatter(consoleFormatter);
  consoleSink->set_filter(severity >= minConsoleSeverity);

  boost::log::add_common_attributes();

  // Set a handlers to flush logs on program termination
  std::atexit(cleanUp);
  std::at_quick_exit(cleanUp);
  std::set_terminate([]() noexcept {
    cleanUp();
    std::abort();
  });

  // Set global filter for performance tuning
  boost::log::core::get()->set_filter(severity >= minSeverity);
}

void setModuleName(Logger& logger, const char* name) {
  logger.add_attribute("Module", attr::constant<std::string>(name));
}
void setModuleName(ThreadsafeLogger& logger, const char* name) {
  logger.add_attribute("Module", attr::constant<std::string>(name));
}

void flush() {
  boost::log::core::get()->flush();       // flush core to sinks
  if (fileSink) {
    fileSink->flush();                    // flush sink frontend
    fileSink->locked_backend()->flush();  // flush sink backend
  }
  if (consoleSink)
    consoleSink->flush();
}

} // namespace logging
