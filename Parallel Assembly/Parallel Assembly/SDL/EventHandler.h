#pragma once
#ifndef EVENT_HANDLER_H
#define EVENT_HANDLER_H

#include <map>
#include <functional>

#include <SDL.h>

#include "GLWindow.h"
#include "../Logger.h"

namespace sdl {

// Handlers need to check window ID for events that have them
using EventHandlerFunc = std::function<void(const SDL_Event&, sdl::GLWindow&)>;
// Window ID already checked before being called
using WindowEventHandlerFunc = std::function<void(const SDL_WindowEvent&, sdl::GLWindow&)>;

class EventHandler {
  sdl::GLWindow* m_window; // non-owning
  logging::Logger m_logger;

  // Uint32 == SDL_EventType
  std::multimap<Uint32, EventHandlerFunc> m_handlers;
  // Uint8 == SDL_WindowEventID
  std::multimap<Uint8, WindowEventHandlerFunc> m_windowHandlers;
public:
  EventHandler(sdl::GLWindow& window);
  EventHandler(const EventHandler& o) = default;
  EventHandler(EventHandler&& o) = default;
  ~EventHandler() = default;

  Uint32 allocateUserEvent();

  // Returns true on quit event
  bool dispatch(const SDL_Event& event);
  void addEventHandler(Uint32 eventType, EventHandlerFunc func);
  void addWindowEventHandler(SDL_WindowEventID eventType, WindowEventHandlerFunc func);

  // Attaches logging handlers for events we care about
  void attachLoggingHandlers();

protected:
  void handleEvent(const SDL_WindowEvent& event);
};

}

#endif // EVENT_HANDLER_H
