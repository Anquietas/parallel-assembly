#pragma once
#ifndef SDL_SDL_MIXER_H
#define SDL_SDL_MIXER_H

#include <SDL.h>

namespace sdl {

class SDLMixer final {
public:
  SDLMixer();
  SDLMixer(const SDLMixer&) = delete;
  SDLMixer(SDLMixer&&) = delete;
  ~SDLMixer();

  SDLMixer& operator=(const SDLMixer&) = delete;
  SDLMixer& operator=(SDLMixer&&) = delete;
};

}

#endif // SDL_SDL_MIXER_H
