#include "SDLMixer.h"

#include <sstream>

#include <SDL_mixer.h>

#include "../Exception.h"

namespace sdl {

SDLMixer::SDLMixer() {
  int flags = MIX_INIT_MP3 | MIX_INIT_OGG;
  int initialized = Mix_Init(flags);
  if ((initialized & flags) != flags) {
    std::ostringstream ss;
    ss << "Failed to initialize mp3 and ogg support: " << Mix_GetError();
    Mix_Quit();
    throw MAKE_EXCEPTION(ss.str());
  }
}
SDLMixer::~SDLMixer() {
  Mix_Quit();
}

}
