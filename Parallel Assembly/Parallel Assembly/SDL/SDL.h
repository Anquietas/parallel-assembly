#pragma once
#ifndef SDL_SDL_H
#define SDL_SDL_H

#include <SDL.h>

namespace sdl {

class SDL final {
public:
  SDL(Uint32 flags);
  SDL(const SDL&) = delete;
  SDL(SDL&&) = delete;
  ~SDL();

  SDL& operator=(const SDL&) = delete;
  SDL& operator=(SDL&&) = delete;
};

}

#endif // SDL_SDL_H
