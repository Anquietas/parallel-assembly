#include "SDL.h"

#include <sstream>

#include "../Exception.h"

namespace sdl {

SDL::SDL(Uint32 flags) {
  SDL_SetMainReady();
  if (SDL_Init(flags)) {
    std::ostringstream ss;
    ss << "Failed to initialize SDL: " << SDL_GetError();
    throw MAKE_EXCEPTION(ss.str());
  }
}
SDL::~SDL() {
  SDL_Quit();
}

} // namespace sdl
