#pragma once
#ifndef GL_WINDOW_H
#define GL_WINDOW_H

#include <sstream>

#include <SDL.h>

#include "../GLMIncludes.h"

#include "../Graphics/Renderer.h"
#include "../Logger.h"

namespace sdl {

struct GLConfig {
  glm::ivec4 rgbaSizes;
  int depthBufferSize;
  int stencilBufferSize;
  bool doubleBuffering;
  bool hardwareAcceleration;
  glm::ivec2 openGLVersion;
  int openGLContextFlags;
  int openGLProfileMask;
  bool sRGBCapable;
};

class GLWindow {
  SDL_Window* m_window;
  SDL_GLContext m_glContext;  // really void*
  graphics::Renderer m_renderer;
  bool m_inDestructor; // workaround

  logging::Logger m_logger;

  void setGLAttributes(const GLConfig& config);
  void checkGLAttributes(const GLConfig& config);

public:
  GLWindow(const char* title,
           const glm::ivec2& size);
  GLWindow(const char* title,
           const glm::ivec2& size,
           const GLConfig& config);
  GLWindow(const GLWindow&) = delete;
  GLWindow(GLWindow&&) = delete;
  ~GLWindow();

  GLWindow& operator=(const GLWindow&) = delete;
  GLWindow& operator=(GLWindow&&) = delete;

  void show();
  void hide();

  Uint32 getID() noexcept;

  graphics::Renderer* renderer() noexcept;

  glm::ivec2 getSize() const noexcept;

  void draw();
  void swapBuffers() noexcept;

  void updateDisplaySettings();
};

}

#endif // GL_WINDOW_H
