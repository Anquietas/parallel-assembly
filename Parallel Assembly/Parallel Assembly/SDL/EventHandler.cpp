#include "EventHandler.h"

#include <iostream>

#include <chrono>

#include "../Assert.h"
#include "../Exception.h"

namespace sdl {

EventHandler::EventHandler(sdl::GLWindow& window)
  : m_window(&window)
  , m_logger()
{
  logging::setModuleName(m_logger, "EventHandler");
}

Uint32 EventHandler::allocateUserEvent() {
  Uint32 result = SDL_RegisterEvents(1);
  if (result == static_cast<Uint32>(-1))
    throw MAKE_EXCEPTION("SDL ran out of slots for user events");
  return result;
}

bool EventHandler::dispatch(const SDL_Event& event) {
  bool handled = false;
  auto handlers = m_handlers.equal_range(event.type);
  for (auto iter = handlers.first; iter != handlers.second; ++iter)
    iter->second(event, *m_window);
  handled = handlers.first != handlers.second;

  switch (event.type) {
  case SDL_QUIT:
    return true;
    break;
  case SDL_WINDOWEVENT:
    handleEvent(event.window);
    handled = true;
    break;
  case SDL_CLIPBOARDUPDATE:
    // Don't care about clipboard updates but suppress the warning anyway
    handled = true;
    break;
  default:
    // Don't care
    break;
  }
  if (!handled)
    LOG_SEVERITY(m_logger, Warning) << "Unhandled event: " << event.type;
  return false;
}
void EventHandler::addEventHandler(Uint32 eventType, EventHandlerFunc func) {
  m_handlers.emplace(eventType, func);
}
void EventHandler::addWindowEventHandler(SDL_WindowEventID eventType, WindowEventHandlerFunc func) {
  m_windowHandlers.emplace(Uint8(eventType), func);
}

void EventHandler::attachLoggingHandlers() {
  auto audioDeviceLogger = [this](const SDL_Event& e, sdl::GLWindow&) {
    std::ostringstream ss;
    auto& event = e.adevice;
    switch (event.type) {
    case SDL_AUDIODEVICEADDED:
      ss << "Audio device added: ";
      break;
    case SDL_AUDIODEVICEREMOVED:
      ss << "Audio device removed: ";
      break;
    default:
      ASSERT(false);
      return;
    }
    ss << "which = " << event.which << "; " << (event.iscapture ? "input" : "output");
    LOG_SEVERITY(m_logger, Debug) << ss.str();
  };
  addEventHandler(SDL_AUDIODEVICEADDED, audioDeviceLogger);
  addEventHandler(SDL_AUDIODEVICEREMOVED, audioDeviceLogger);

  auto keyLogger = [this](const SDL_Event& e, sdl::GLWindow& window) {
    auto& event = e.key;
    auto windowID = window.getID();
    if (windowID == event.windowID) {
      std::ostringstream ss;
      switch (event.type) {
      case SDL_KEYDOWN:
        ss << "Key down: ";
        break;
      case SDL_KEYUP:
        ss << "Key up: ";
        break;
      default:
        ASSERT(false);
        return;
      }
      ss << SDL_GetKeyName(event.keysym.sym) << (event.repeat ? " repeat" : "");
      ss << ", mod = {";
      if (event.keysym.mod & KMOD_CTRL)
        ss << "ctrl, ";
      if (event.keysym.mod & KMOD_ALT)
        ss << "alt, ";
      if (event.keysym.mod & KMOD_MODE)
        ss << "alt-gr, ";
      if (event.keysym.mod & KMOD_SHIFT)
        ss << "shift, ";
      if (event.keysym.mod & KMOD_GUI)
        ss << "gui, ";
      if (event.keysym.mod & KMOD_CAPS)
        ss << "capslock, ";
      if (event.keysym.mod & KMOD_NUM)
        ss << "numlock, ";
      ss << '}';
      LOG_SEVERITY(m_logger, Pedantic) << ss.str();
    }
  };
  addEventHandler(SDL_KEYDOWN, keyLogger);
  addEventHandler(SDL_KEYUP, keyLogger);

  addEventHandler(
    SDL_TEXTEDITING,
    [this](const SDL_Event& e, sdl::GLWindow& window) {
      auto& event = e.edit;
      auto windowID = window.getID();
      if (windowID == event.windowID) {
        std::ostringstream ss;
        ss << "TextEdit: text = \"" << event.text
           << "\", start = " << event.start << ", length = " << event.length
           << ", timestamp = " << event.timestamp;
        LOG_SEVERITY(m_logger, Pedantic) << ss.str();
      }
    });
  addEventHandler(
    SDL_TEXTINPUT,
    [this](const SDL_Event& e, sdl::GLWindow& window) {
      auto& event = e.text;
      auto windowID = window.getID();
      if (windowID == event.windowID) {
        std::ostringstream ss;
        ss << "TextInput: text = \"" << event.text << "\", timestamp = " << event.timestamp;
        LOG_SEVERITY(m_logger, Pedantic) << ss.str();
      }
    });

  addEventHandler(
    SDL_MOUSEMOTION,
    [this](const SDL_Event& e, sdl::GLWindow& window) {
      auto& event = e.motion;
      auto windowID = window.getID();
      if (windowID == event.windowID) {
        std::ostringstream ss;
        ss << "Mouse motion: (";
        ss << event.x << " (" << (event.xrel >= 0 ? "+" : "") << event.xrel << "), ";
        ss << event.y << " (" << (event.yrel >= 0 ? "+" : "") << event.yrel << ")); {";
        if (event.state & SDL_BUTTON_LMASK)
          ss << "left, ";
        if (event.state & SDL_BUTTON_MMASK)
          ss << "middle, ";
        if (event.state & SDL_BUTTON_RMASK)
          ss << "right, ";
        if (event.state & SDL_BUTTON_X1MASK)
          ss << "x1, ";
        if (event.state & SDL_BUTTON_X2MASK)
          ss << "x2, ";
        ss << "}; which = " << event.which;
        LOG_SEVERITY(m_logger, Pedantic) << ss.str();
      }
    });

  auto mouseButtonLogger = [this](const SDL_Event& e, sdl::GLWindow& window) {
    auto& event = e.button;
    auto windowID = window.getID();
    if (windowID == event.windowID) {
      std::ostringstream ss;
      switch (event.type) {
      case SDL_MOUSEBUTTONDOWN:
        ss << "Mouse button down: ";
        ASSERT(event.state == SDL_PRESSED);
        break;
      case SDL_MOUSEBUTTONUP:
        ss << "Mouse button up: ";
        ASSERT(event.state == SDL_RELEASED);
        break;
      }
      ss << "button=";
      switch (event.button) {
      case SDL_BUTTON_LEFT:
        ss << "left";
        break;
      case SDL_BUTTON_MIDDLE:
        ss << "middle";
        break;
      case SDL_BUTTON_RIGHT:
        ss << "right";
        break;
      case SDL_BUTTON_X1:
        ss << "x1";
        break;
      case SDL_BUTTON_X2:
        ss << "x2";
        break;
      default:
        ASSERT(false);
      }
      ss << "; (" << event.x << ", " << event.y << "); which = " << event.which;
      ss << "; clicks = " << static_cast<int>(event.clicks);
      LOG_SEVERITY(m_logger, Pedantic) << ss.str();
    }
  };
  addEventHandler(SDL_MOUSEBUTTONDOWN, mouseButtonLogger);
  addEventHandler(SDL_MOUSEBUTTONUP, mouseButtonLogger);

  addEventHandler(
    SDL_MOUSEWHEEL,
    [this](const SDL_Event& e, sdl::GLWindow& window) {
      auto& event = e.wheel;
      auto windowID = window.getID();
      if (windowID == event.windowID) {
        LOG_SEVERITY(m_logger, Pedantic)
          << "Mouse wheel event: (" << event.x << ", " << event.y << ") direction = "
          << (event.direction == SDL_MOUSEWHEEL_NORMAL ? "normal" : "flipped")
          << "; which = " << event.which;
      }
    });

  addEventHandler(
    SDL_QUIT,
    [this](const SDL_Event& e, sdl::GLWindow&) {
      auto& event = e.quit;
      LOG_SEVERITY(m_logger, Debug) << "Quit event at timestamp " << event.timestamp;
    });

  addWindowEventHandler(
    SDL_WINDOWEVENT_SHOWN,
    [this](const SDL_WindowEvent&, sdl::GLWindow&) {
      LOG_SEVERITY(m_logger, Debug) << "Window event: shown";
    });
  addWindowEventHandler(
    SDL_WINDOWEVENT_HIDDEN,
    [this](const SDL_WindowEvent&, sdl::GLWindow&) {
      LOG_SEVERITY(m_logger, Debug) << "Window event: hidden";
    });
  addWindowEventHandler(
    SDL_WINDOWEVENT_EXPOSED,
    [this](const SDL_WindowEvent&, sdl::GLWindow&) {
      LOG_SEVERITY(m_logger, Debug) << "Window event: exposed";
    });
  addWindowEventHandler(
    SDL_WINDOWEVENT_MOVED,
    [this](const SDL_WindowEvent& event, sdl::GLWindow&) {
      LOG_SEVERITY(m_logger, Debug)
        << "Window event: moved to (" << event.data1 << ", " << event.data2 << ')';
    });
  addWindowEventHandler(
    SDL_WINDOWEVENT_RESIZED,
    [this](const SDL_WindowEvent& event, sdl::GLWindow&) {
      LOG_SEVERITY(m_logger, Debug)
        << "Window event: resized to " << event.data1 << " x " << event.data2;
    });
  addWindowEventHandler(
    SDL_WINDOWEVENT_SIZE_CHANGED,
    [this](const SDL_WindowEvent& event, sdl::GLWindow&) {
      LOG_SEVERITY(m_logger, Debug)
        << "Window event: size changed to " << event.data1 << " x " << event.data2;
    });
  addWindowEventHandler(
    SDL_WINDOWEVENT_MINIMIZED,
    [this](const SDL_WindowEvent&, sdl::GLWindow&) {
      LOG_SEVERITY(m_logger, Debug) << "Window event: minimized";
    });
  addWindowEventHandler(
    SDL_WINDOWEVENT_MAXIMIZED,
    [this](const SDL_WindowEvent&, sdl::GLWindow&) {
      LOG_SEVERITY(m_logger, Debug) << "Window event: maximized";
    });
  addWindowEventHandler(
    SDL_WINDOWEVENT_RESTORED,
    [this](const SDL_WindowEvent&, sdl::GLWindow&) {
      LOG_SEVERITY(m_logger, Debug) << "Window event: restored";
    });
  addWindowEventHandler(
    SDL_WINDOWEVENT_ENTER,
    [this](const SDL_WindowEvent&, sdl::GLWindow&) {
      LOG_SEVERITY(m_logger, Debug) << "Window event: mouse entered window";
    });
  addWindowEventHandler(
    SDL_WINDOWEVENT_LEAVE,
    [this](const SDL_WindowEvent&, sdl::GLWindow&) {
      LOG_SEVERITY(m_logger, Debug) << "Window event: mouse left window";
    });
  addWindowEventHandler(
    SDL_WINDOWEVENT_FOCUS_GAINED,
    [this](const SDL_WindowEvent&, sdl::GLWindow&) {
      LOG_SEVERITY(m_logger, Debug) << "Window event: keyboard focus gained";
    });
  addWindowEventHandler(
    SDL_WINDOWEVENT_FOCUS_LOST,
    [this](const SDL_WindowEvent&, sdl::GLWindow&) {
      LOG_SEVERITY(m_logger, Debug) << "Window event: keyboard focus lost";
    });
  addWindowEventHandler(
    SDL_WINDOWEVENT_CLOSE,
    [this](const SDL_WindowEvent&, sdl::GLWindow&) {
      LOG_SEVERITY(m_logger, Debug) << "Window event: close";
    });
}

void EventHandler::handleEvent(const SDL_WindowEvent& event) {
  auto windowID = m_window->getID();
  if (windowID == event.windowID) {
    bool handled = false;
    auto handlers = m_windowHandlers.equal_range(event.event);
    for (auto iter = handlers.first; iter != handlers.second; ++iter)
      iter->second(event, *m_window);
    handled = handlers.first != handlers.second;

    switch (event.event) {
    case SDL_WINDOWEVENT_SIZE_CHANGED:
      // Ignore event; resized'll work just fine.
      handled = true;
      break;
    default:
      // Don't care
      break;
    }
    if (!handled)
      LOG_SEVERITY(m_logger, Warning) << "Unhandled window event: " << static_cast<int>(event.event);
  }
}

}
