#include "GLWindow.h"

#include <iostream>

#include "../Graphics/OpenGL/OpenGLIncludes.h"
#include "../Graphics/OpenGL/DebugHandler.h"
#include "../Graphics/OpenGL/SystemInfo.h"
#include "../Graphics/OpenGL/checkError.h"
#include "../Graphics/OpenGL/FrameBuffer.h"

#include "../Assert.h"
#include "../Exception.h"
#include "../GameSettings.h"
#include "../PlatformInfo.h"

namespace sdl {

GLWindow::GLWindow(const char* title,
                   const glm::ivec2& size)
  : GLWindow(
      title, size, {
        {8, 8, 8, 0}, // rgba sizes
        16,           // depth buffer
        0,            // stencil buffer
        true,         // double buffering
        true,         // hardware acceleration
        {3, 3},       // OpenGL version
        // OpenGL Context Flags
        0
#ifndef NDEBUG
      | SDL_GL_CONTEXT_DEBUG_FLAG               // Debug mode
#endif
        ,
        // OpenGL Profile Mask
        SDL_GL_CONTEXT_PROFILE_CORE,
        true          // sRGBCapable
      })
{}
GLWindow::GLWindow(const char* title, 
                   const glm::ivec2& size,
                   const GLConfig& config)
  : m_window(nullptr)
  , m_glContext(nullptr)
  , m_renderer(size)
  , m_inDestructor(false)
  , m_logger()
{
  try {
    logging::setModuleName(m_logger, "GLWindow");

    m_window = SDL_CreateWindow(
      title,
      SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
      size.x, size.y,
      SDL_WINDOW_OPENGL |
      SDL_WINDOW_HIDDEN);
    if (!m_window) {
      std::ostringstream ss;
      ss << "Failed to create SDL_Window: " << SDL_GetError();
      throw MAKE_EXCEPTION(ss.str());
    }

    setGLAttributes(config);

    m_glContext = SDL_GL_CreateContext(m_window);
    if (!m_glContext) {
      std::ostringstream ss;
      ss << "Failed to create OpenGL Context: " << SDL_GetError();
      throw MAKE_EXCEPTION(ss.str());
    }

    checkGLAttributes(config);

    GLenum error = glewInit();
    if (error != GLEW_OK) {
      std::ostringstream ss;
      ss << "Failed to initialize GLEW: " << glewGetErrorString(error);
      throw MAKE_EXCEPTION(ss.str());
    }
    if (!GLEW_VERSION_3_3) {
      std::ostringstream ss;
      ss << "GLEW failed to detect OpenGL 3.3 support; check drivers";
      throw MAKE_EXCEPTION(ss.str());
    }

    logPlatformInfo();
    graphics::gl::setupDebugHandler();
    graphics::gl::SystemInfo::init();
    m_renderer.initialize();
  } catch (...) {
    m_renderer.destroy();
    if (m_glContext)
      SDL_GL_DeleteContext(m_glContext);
    if (m_window)
      SDL_DestroyWindow(m_window);
    throw;
  }
}

GLWindow::~GLWindow() {
  m_inDestructor = true;
  m_renderer.destroy();
  glFinish();
  if (m_glContext)
    SDL_GL_DeleteContext(m_glContext);
  if (m_window)
    SDL_DestroyWindow(m_window);
}

void GLWindow::setGLAttributes(const GLConfig& config) {
  // Set up an RGBA 32 bit window with 24 bit z-buffer and 8 bit stencil buffer
  int error = 0;
  error = error || SDL_GL_SetAttribute(SDL_GL_RED_SIZE, config.rgbaSizes.r);
  error = error || SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, config.rgbaSizes.g);
  error = error || SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, config.rgbaSizes.b);
  error = error || SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, config.rgbaSizes.a);
  error = error || SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, config.rgbaSizes.r + config.rgbaSizes.g + config.rgbaSizes.b + config.rgbaSizes.a);
  error = error || SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, config.depthBufferSize);
  error = error || SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, config.stencilBufferSize);
  error = error || SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, config.doubleBuffering ? 1 : 0);
  error = error || SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, config.hardwareAcceleration ? 1 : 0);
  error = error || SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, config.openGLVersion[0]);
  error = error || SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, config.openGLVersion[1]);
  error = error || SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, config.openGLContextFlags);
  error = error || SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, config.openGLProfileMask);
  error = error || SDL_GL_SetAttribute(SDL_GL_FRAMEBUFFER_SRGB_CAPABLE, config.sRGBCapable);

  if (error != 0) {
    std::ostringstream ss;
    ss << "Error setting OpenGL attributes: " << SDL_GetError();
    throw MAKE_EXCEPTION(ss.str());
  }
}

void GLWindow::checkGLAttributes(const GLConfig& config) {
  glm::ivec4 rgbaSizes{0, 0, 0, 0};
  int bufferSize = 0, depthSize = 0, stencilSize = 0;
  int isDoubleBuffered = 0, hardwareAccelEnabled = 0;
  glm::ivec2 openGLVersion{0, 0};
  int contextFlags = 0, profileMask = 0, srgbCapable = 0;

  // Retrieve config values post context creation
  int error = 0;
  error = error || SDL_GL_GetAttribute(SDL_GL_RED_SIZE, &rgbaSizes.r);
  error = error || SDL_GL_GetAttribute(SDL_GL_GREEN_SIZE, &rgbaSizes.g);
  error = error || SDL_GL_GetAttribute(SDL_GL_BLUE_SIZE, &rgbaSizes.b);
  error = error || SDL_GL_GetAttribute(SDL_GL_ALPHA_SIZE, &rgbaSizes.a);
  error = error || SDL_GL_GetAttribute(SDL_GL_BUFFER_SIZE, &bufferSize);
  error = error || SDL_GL_GetAttribute(SDL_GL_DEPTH_SIZE, &depthSize);
  error = error || SDL_GL_GetAttribute(SDL_GL_STENCIL_SIZE, &stencilSize);
  // Double buffering
  error = error || SDL_GL_GetAttribute(SDL_GL_DOUBLEBUFFER, &isDoubleBuffered);
  // hardware acceleration
  error = error || SDL_GL_GetAttribute(SDL_GL_ACCELERATED_VISUAL, &hardwareAccelEnabled);
  // OpenGL version
  error = error || SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &openGLVersion[0]);
  error = error || SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &openGLVersion[1]);
  // Context flags
  error = error || SDL_GL_GetAttribute(SDL_GL_CONTEXT_FLAGS, &contextFlags);
  // OpenGL Profile
  error = error || SDL_GL_GetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, &profileMask);
  // sRGB support
  error = error || SDL_GL_GetAttribute(SDL_GL_FRAMEBUFFER_SRGB_CAPABLE, &srgbCapable);

  if (error != 0) {
    std::ostringstream ss;
    ss << "Error reading OpenGL attributes after context creation: " << SDL_GetError();
    throw MAKE_EXCEPTION(ss.str());
  }
  // Print to console for debugging
  std::ostringstream windowInfo;
  windowInfo << "Window Information:\n";
  windowInfo << "RGBA: (" << rgbaSizes.r << ", " << rgbaSizes.g << ", " << rgbaSizes.b << ", " << rgbaSizes.a << ")\n";
  windowInfo << "Buffer size: " << bufferSize << " bits\n";
  windowInfo << "Depth buffer: " << depthSize << " bits\n";
  windowInfo << "Stencil buffer: " << stencilSize << " bits\n";
  windowInfo << "Double buffering: " << (isDoubleBuffered ? "enabled" : "disabled") << '\n';
  windowInfo << "Hardware acceleration: " << (hardwareAccelEnabled ? "enabled" : "disabled") << '\n';
  windowInfo << "OpenGL Version: " << openGLVersion[0] << '.' << openGLVersion[1] << '\n';
  windowInfo << "OpenGL context flags:\n";
  windowInfo << "  debug: " << (contextFlags & SDL_GL_CONTEXT_DEBUG_FLAG ? "enabled" : "disabled");
  windowInfo << "\n  forward compatible: " << (contextFlags & SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG ? "enabled" : "disabled");
  windowInfo << "\n  robust access: " << (contextFlags & SDL_GL_CONTEXT_ROBUST_ACCESS_FLAG ? "enabled" : "disabled");
  windowInfo << "\n  reset isolation: " << (contextFlags & SDL_GL_CONTEXT_RESET_ISOLATION_FLAG ? "enabled" : "disabled");
  windowInfo << "\nOpenGL profile mask: ";
  switch (profileMask) {
  case SDL_GL_CONTEXT_PROFILE_CORE:
    windowInfo << "core";
    break;
  case SDL_GL_CONTEXT_PROFILE_COMPATIBILITY:
    windowInfo << "compatibility";
    break;
  default:
    // Don't need to check ES since we're not working on an embedded platform
    ASSERT(false);
  }
  windowInfo << '\n';
  windowInfo << "sRGB capable: " << (srgbCapable ? "yes" : "no");
  LOG_SEVERITY(m_logger, Debug) << windowInfo.str();

  // Now check loaded values match or exceed request
  bool rgbaOK = true;
  int rgbaTotal = 0;
  for (int i = 0; i < 4; ++i) {
    rgbaOK = rgbaOK && rgbaSizes[i] >= config.rgbaSizes[i];
    rgbaTotal += rgbaSizes[i];
  }
  if (!rgbaOK)
    throw MAKE_EXCEPTION("SDL returned window with insufficient colour depth");
  ASSERT(rgbaTotal <= bufferSize);
  ASSERT(rgbaTotal >= config.rgbaSizes.r + config.rgbaSizes.g + config.rgbaSizes.b + config.rgbaSizes.a);
  if (depthSize < config.depthBufferSize)
    throw MAKE_EXCEPTION("SDL returned window with insufficient depth buffer");
  if (stencilSize < config.stencilBufferSize)
    throw MAKE_EXCEPTION("SDL returned window with insufficient stencil buffer");
  if (!isDoubleBuffered && config.doubleBuffering)
    throw MAKE_EXCEPTION("Double buffering requested, SDL returned single buffered window");
  if (!hardwareAccelEnabled && config.hardwareAcceleration)
    throw MAKE_EXCEPTION("Hardware acceleration requested, SDL returned software OpenGL context");
  if (openGLVersion[0] < config.openGLVersion[0] || (openGLVersion[0] == config.openGLVersion[0] && openGLVersion[1] < config.openGLVersion[1]))
    throw MAKE_EXCEPTION("SDL failed to provide requested OpenGL version or higher");
  if (config.openGLContextFlags != 0 && !(contextFlags & config.openGLContextFlags))
    throw MAKE_EXCEPTION("SDL returned OpenGL context missing one or more requested features");
  if (profileMask != config.openGLProfileMask)
    throw MAKE_EXCEPTION("SDL returned OpenGL context with different profile than requested");
  if (!srgbCapable && config.sRGBCapable)
    throw MAKE_EXCEPTION("sRGB capable window requested, SDL failed to provide one");
}

void GLWindow::show() {
  ASSERT(m_window);
  SDL_ShowWindow(m_window);
}
void GLWindow::hide() {
  ASSERT(m_window);
  SDL_HideWindow(m_window);
}

Uint32 GLWindow::getID() noexcept {
  return SDL_GetWindowID(m_window);
}

graphics::Renderer* GLWindow::renderer() noexcept {
  return &m_renderer;
}

glm::ivec2 GLWindow::getSize() const noexcept {
  glm::ivec2 result;
  SDL_GetWindowSize(m_window, &result.x, &result.y);
  return result;
}

void GLWindow::draw() {
  if (!m_inDestructor) {
    m_renderer.draw();
    CHECK_GL_ERROR();
    swapBuffers();
  }
}
void GLWindow::swapBuffers() noexcept {
  SDL_GL_SwapWindow(m_window);
}

void GLWindow::updateDisplaySettings() {
  LOG_SEVERITY(m_logger, Debug)
    << "Updating display settings...";

  auto settings = GameSettings::get();
  auto& resolution = settings->screenResolution();
  auto mode = settings->displayMode();

  // Must update display mode before resolution to avoid clamping window size to native resolution
  int result = 0;
  switch (mode) {
  case DisplayMode::Windowed:
    result = SDL_SetWindowFullscreen(m_window, 0);
    SDL_SetWindowBordered(m_window, SDL_TRUE);
    LOG_SEVERITY(m_logger, Debug) << "Updated display mode to windowed";
    break;
  case DisplayMode::Borderless:
    result = SDL_SetWindowFullscreen(m_window, 0);
    SDL_SetWindowBordered(m_window, SDL_FALSE);
    LOG_SEVERITY(m_logger, Debug) << "Updated display mode to borderless";
    break;
  case DisplayMode::Fullscreen:
    result = SDL_SetWindowFullscreen(m_window, SDL_WINDOW_FULLSCREEN);
    LOG_SEVERITY(m_logger, Debug) << "Updated display mode to fullscreen";
    break;
  default:
    ASSERT(false);
    return;
  }
  if (result != 0) {
    std::ostringstream ss;
    ss << "Error setting display mode: " << SDL_GetError();
    throw MAKE_EXCEPTION(ss.str());
  }

  glm::ivec2 oldResolution;
  SDL_GetWindowSize(m_window, &oldResolution.x, &oldResolution.y);
  if (oldResolution != resolution) {
    SDL_SetWindowSize(m_window, resolution.x, resolution.y);
    LOG_SEVERITY(m_logger, Debug)
      << "Updated screen resolution from " << oldResolution.x << " x " << oldResolution.y
      << " to " << resolution.x << " x " << resolution.y;
    SDL_SetWindowPosition(m_window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
  }

  // SDL_GL_GetSwapInterval is too unreliable to optimize this here :(
  // Unbind any active framebuffer before updating VSync (doesn't work otherwise)
  auto activeFBO = graphics::gl::FrameBuffer::getPrimaryFrameBuffer();
  if (activeFBO) {
    LOG_SEVERITY(m_logger, Debug) << "Unbinding active FBO for VSync update";
    graphics::gl::FrameBuffer::unbind();
  }

  bool failedToUpdateVSync = false;
  if (settings->isVSyncEnabled()) {
    LOG_SEVERITY(m_logger, Debug)
      << "Enabling VSync";
    // Try to enable late swap tearing first, or v-sync if it fails.
    // If both fail, error.
    // (ClearError here clears the error from the first GL_SetSwapInterval call)
    if (SDL_GL_SetSwapInterval(-1) != 0) {
      LOG_SEVERITY(m_logger, Warning)
        << "Failed to enable late swap tearing VSync: " << SDL_GetError()
        << ", falling back to standard VSync.";
      if (SDL_GL_SetSwapInterval(1))
        failedToUpdateVSync = true;
    }
  } else {
    LOG_SEVERITY(m_logger, Debug)
      << "Disabling VSync";
    if (SDL_GL_SetSwapInterval(0) != 0)
      failedToUpdateVSync = true;
  }
  if (failedToUpdateVSync) {
    std::ostringstream ss;
    ss << "Failed to update VSync setting: " << SDL_GetError();
    throw MAKE_EXCEPTION(ss.str());
  }

  if (activeFBO) {
    LOG_SEVERITY(m_logger, Debug) << "Rebinding previously active FBO after VSync update";
    activeFBO->bind();
  }
}

} // namespace sdl
