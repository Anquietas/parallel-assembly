#pragma once
#ifndef PHOENIX_SINGLETON_H
#define PHOENIX_SINGLETON_H

#include <memory>
#include <mutex>

template <typename T>
class PhoenixSingleton {
public:
  static std::shared_ptr<T> get();
};

#define PHOENIX_SINGLETON_EXTERN_TEMPLATE(T) \
  extern template std::shared_ptr<T> PhoenixSingleton<T>::get()
#define PHOENIX_SINGLETON_TEMPLATE_INSTANCE(T) \
  template std::shared_ptr<T> PhoenixSingleton<T>::get()

#endif // PHOENIX_SINGLETON_H
