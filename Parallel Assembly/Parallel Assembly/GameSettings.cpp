#include "GameSettings.h"

#include <cmath>
#include <mutex>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/algorithm/string/case_conv.hpp>

#include <SDL.h>

#include "Graphics/OpenGL/SystemInfo.h"

#include "Assert.h"
#include "Exception.h"
#include "Logger.h"
#include "PhoenixSingleton.inl"

#define PA_VERSION_STRING "prealpha"

PHOENIX_SINGLETON_TEMPLATE_INSTANCE(GameSettings);

namespace {
STATIC_LOGGER_DECLARE(logger);
}

namespace po = detail::po;
namespace fs = boost::filesystem;

std::istream& operator>>(std::istream& in, DisplayMode& mode) {
  std::string token;
  in >> token;

  boost::to_lower(token);

  if (token == "windowed")
    mode = DisplayMode::Windowed;
  else if (token == "borderless")
    mode = DisplayMode::Borderless;
  else if (token == "fullscreen")
    mode = DisplayMode::Fullscreen;
  else
    throw po::validation_error(po::validation_error::invalid_option_value);

  return in;
}
std::ostream& operator<<(std::ostream& out, const DisplayMode& mode) {
  switch (mode) {
  case DisplayMode::Windowed:
    out << "windowed";
    break;
  case DisplayMode::Borderless:
    out << "borderless";
    break;
  case DisplayMode::Fullscreen:
    out << "fullscreen";
    break;
  default:
    ASSERT(false);;
  }
  return out;
}

bool operator==(const AntiAliasingLevel& x, const AntiAliasingLevel& y) {
  return x.level == y.level;
}
bool operator!=(const AntiAliasingLevel& x, const AntiAliasingLevel& y) {
  return x.level != y.level;
}

bool GameSettings::settingsBackupInUse = false;
GameSettings GameSettings::settingsBackup;
const glm::ivec2 GameSettings::minResolution{800, 600};

GameSettings::GameSettings()
  : PhoenixSingleton()
  , m_settingsFileName()
  , m_settingsFileDirty(false)
  , m_videoDirty(false)
  , m_screenResolution()
  , m_nativeResolution()
  , m_screenResolutions()
  , m_displayMode(DisplayMode::Windowed)
  , m_aaEnabled(false)
  , m_aaLevel({0u})
  , m_aaLevels()
  , m_vsyncEnabled(false)
  , m_audioDirty(false)
  , m_audioEnabled(false)
  , m_masterVolume(0.f)
  , m_musicEnabled(false)
  , m_musicVolume(0.f)
  , m_sfxEnabled(false)
  , m_sfxVolume(0.f)
{}

void GameSettings::populateScreenResolutions() {
  /*
  SDL_DisplayMode desktopDisplayMode;
  if (SDL_GetDesktopDisplayMode(0, &desktopDisplayMode) != 0) {
    std::ostringstream ss;
    ss << "Failed to fetch native display mode: " << SDL_GetError();
    throw MAKE_EXCEPTION(ss.str());
  }
  m_nativeResolution = glm::ivec2(desktopDisplayMode.w, desktopDisplayMode.h);
  LOG_SEVERITY(logger::get(), Debug)
    << "Native resolution: " << m_nativeResolution.x << 'x' << m_nativeResolution.y;
  */

  SDL_Rect displayBounds;
  if (SDL_GetDisplayBounds(0, &displayBounds) != 0) {
    std::ostringstream ss;
    ss << "Failed to fetch display bounds: " << SDL_GetError();
    throw MAKE_EXCEPTION(ss.str());
  }
  m_nativeResolution = glm::ivec2(displayBounds.w, displayBounds.h);
  LOG_SEVERITY(logger::get(), Info)
    << "Native resolution: " << m_nativeResolution.x << 'x' << m_nativeResolution.y;

  m_screenResolutions.assign({
    {800, 600},
    {1024, 768},
    {1280, 720},
    {1280, 768},
    {1280, 800},
    {1280, 1024},
    {1360, 768},
    {1366, 768},
    {1440, 900},
    {1600, 900},
    {1600, 1200},
    {1680, 1050},
    {1920, 1080},
    {1920, 1200},
    {2560, 1440},
    {3840, 2160},
    {7680, 4320},
  });

  auto newEnd = std::remove_if(
    m_screenResolutions.begin(), m_screenResolutions.end(),
    [nativeRes = m_nativeResolution](const glm::ivec2& res) -> bool {
      return glm::any(glm::greaterThan(res, nativeRes));
    });
  m_screenResolutions.erase(newEnd, m_screenResolutions.end());

  // Check there's available resolutions
  if (m_screenResolutions.empty()) {
    std::stringstream ss;
    ss << "Desktop resolutions smaller than " << minResolution.x << 'x' << minResolution.y
      << " are not supported, sorry.\n";
    ss << "Your desktop resolution appears to be " << m_nativeResolution.x << 'x' << m_nativeResolution.y << '\n';
    ss << "Please contact support if this is not correct.";
    throw MAKE_EXCEPTION(ss.str());
  }

  std::ostringstream ss;
  ss << "Available resolution options:\n";
  for (auto& res : m_screenResolutions)
    ss << res.x << " x " << res.y << '\n';
  LOG_SEVERITY(logger::get(), Debug) << ss.str();
}
void GameSettings::populateAALevels() {
  using graphics::gl::uint_t;
  using graphics::gl::SystemInfo;
  
  m_aaLevels.assign({
    {2u},
    {4u},
    {8u},
    {16u},
  });

  uint_t maxSamples = std::min(
    SystemInfo::maxColourTextureSamples(),
    SystemInfo::maxDepthStencilTextureSamples()
  );
  LOG_SEVERITY(logger::get(), Debug) << "Max samples: " << maxSamples;

  auto newEnd = std::remove_if(
    m_aaLevels.begin(), m_aaLevels.end(),
    [maxSamples](const AntiAliasingLevel& aaLevel) -> bool {
      return aaLevel.level > maxSamples;
    });
  m_aaLevels.erase(newEnd, m_aaLevels.end());

  std::ostringstream ss;
  ss << "Available anti-aliasing levels:\n";
  for (auto& level : m_aaLevels)
    ss << "MSAA x" << level.level << '\n';
  LOG_SEVERITY(logger::get(), Debug) << ss.str();

  if (m_aaLevels.empty())
    LOG_SEVERITY(logger::get(), Warning) << "Multisampling appears to be unsupported on this platform";
}
void GameSettings::setDefaultValues() {
  LOG_SEVERITY(logger::get(), Debug) << "Setting default values";

  using graphics::gl::uint_t;
  using graphics::gl::SystemInfo;

  m_screenResolution = glm::ivec2{1280, 720};
  m_displayMode = DisplayMode::Windowed;
  m_aaEnabled = true;
  m_aaLevel.level = 4u;
  m_vsyncEnabled = true;

  m_audioEnabled = true;
  m_masterVolume = 1.f;
  m_musicEnabled = true;
  m_musicVolume = 1.f;
  m_sfxEnabled = true;
  m_sfxVolume = 1.f;

  // Check if default resolution is larger than the screen
  if (glm::any(glm::greaterThan(m_screenResolution, m_nativeResolution))) {
    LOG_SEVERITY(logger::get(), Warning)
      << "Default resolution (" << m_screenResolution.x << " x " << m_screenResolution.y
      << ") is larger than the native resolution, falling back to "
      << minResolution.x << " x " << minResolution.y << " instead. "
      << "This probably means something is wrong.";
    // Fall back to the smallest supported resolution to try to ensure 
    // the window doesn't take up the whole screen.
    m_screenResolution = minResolution;
  }

  uint_t maxSamples = std::min(
    SystemInfo::maxColourTextureSamples(),
    SystemInfo::maxDepthStencilTextureSamples()
  );
  // MSAA not supported
  if (m_aaLevels.empty())
    m_aaEnabled = false;
  // Default AA is higher than max supported
  else if (m_aaLevel.level > maxSamples) {
    ASSERT(!m_aaLevels.empty());
    // Default to the highest available setting
    m_aaLevel = m_aaLevels.back();
  }
}

bool loadSettingsFile(const std::string& settingsFileName,
                      po::options_description& settingsOptions,
                      po::variables_map& variables,
                      logging::Logger& logger)
{
  std::ifstream settingsFile(settingsFileName);
  const bool filePresent = bool(settingsFile);
  if (filePresent) {
    LOG_SEVERITY(logger, Debug) << "Loading settings file...";
    po::store(po::parse_config_file<char>(settingsFile, settingsOptions), variables);
    LOG_SEVERITY(logger, Debug) << "Loading complete; validating...";
    // Check all values are present (i.e. settings file is valid)
    auto varNames = {
      "width",
      "height",
      "display-mode",
      "enable-aa",
      "msaa-level",
      "enable-vsync",
      "enable-music",
      "music-volume",
      "enable-sfx",
      "sfx-volume",
    };
    for (auto name : varNames) {
      if (variables.count(name) == 0) {
        std::ostringstream ss;
        ss << "Invalid settings file; setting \"" << name << "\" is missing.\n";
        ss << "Please try deleting " << settingsFileName << "; it will regenerate with default values.";
        throw MAKE_EXCEPTION(ss.str());
      }
    }
    LOG_SEVERITY(logger, Debug) << "Completed settings file validation.";
  }
  return filePresent;
}
bool validateScreenResolution(const GameSettings& settings,
                              const po::variables_map& variables,
                              const glm::ivec2& resolution,
                              bool setFromCmdLine)
{
  if (variables.count("width") > 0) {
    ASSERT(variables.count("height") > 0);
    if (glm::any(glm::lessThan(resolution, GameSettings::minResolution)) ||
        glm::any(glm::greaterThan(resolution, settings.maxResolution()))) {
      std::ostringstream ss;
      ss << "Invalid setting for resolution in "
        << (setFromCmdLine ? "command line" : "settings file")
        << ": " << resolution.x << " x " << resolution.y
        << "; minimum resolution is " << GameSettings::minResolution.x << " x " << GameSettings::minResolution.y
        << ", maximum is " << settings.maxResolution().x << " x " << settings.maxResolution().y << " (native).";
      throw MAKE_EXCEPTION(ss.str());
    }
    return true;
  }
  return false;
}
bool validateAntiAliasingLevel(const GameSettings& settings,
                               const po::variables_map& variables,
                               graphics::gl::uint_t msaaLevel,
                               bool setFromCmdLine)
{
  auto& aaLevels = settings.getAALevels();
  bool varSet = variables.count("msaa-level") > 0;
  if (settings.isAAAvailable() && varSet) {
    if (std::find(aaLevels.begin(), aaLevels.end(),
                  AntiAliasingLevel{msaaLevel}) == aaLevels.end()) {
      std::ostringstream ss;
      ss << "Invalid setting for anti-aliasing level in"
        << (setFromCmdLine ? "command line" : "settings file")
        << ": MSAA x" << msaaLevel;
      ss << "\nSupported levels are:\n";
      for (auto& l : aaLevels)
        ss << "MSAA x" << l.level << '\n';
      throw MAKE_EXCEPTION(ss.str());
    }
    return true;
  } else if (!settings.isAAAvailable() && varSet && setFromCmdLine) {
    std::ostringstream ss;
    ss << "Tried to set anti-aliasing level to unsupported value via command line: MSAA x"
       << msaaLevel << "; MSAA is not supported on this platform.";
    throw MAKE_EXCEPTION(ss.str());
  }
  return false;
}

bool GameSettings::initializeImpl(po::command_line_parser& cmdParser, const char* settingsFileName) {
  populateScreenResolutions();
  populateAALevels();
  setDefaultValues();

  m_settingsFileName = settingsFileName;

  po::options_description general("General Options");
  general.add_options()
    ("help,?", "Show program options")
    ("version,v", "Show program version");

  glm::uvec2 resolution;
  graphics::gl::uint_t msaaLevel = 0u;

  po::options_description video("Video");
  video.add_options()
    ("width,w",
     po::value<unsigned int>(&resolution.x),
     "Width of the screen to use in pixels")
    ("height,h",
     po::value<unsigned int>(&resolution.y),
     "Height of the screen to use in pixels")
    ("display-mode",
     po::value<DisplayMode>()
      ->notifier([this](DisplayMode mode) { setDisplayMode(mode); }),
     "Display mode (windowed, borderless, fullscreen)")
    ("enable-aa",
     po::value<bool>()
      ->notifier([this](bool enable) { setAAEnabled(enable); }),
     "Enables Anti-Aliasing; true to enable, false to disable")
    ("msaa-level",
     po::value<graphics::gl::uint_t>(&msaaLevel),
     "Multi-Sampling Anti-Aliasing level (2,4,8,16; maximum depends on hardware)")
    ("enable-vsync",
     po::value<bool>()
      ->notifier([this](bool enable) { setVSyncEnabled(enable); }),
     "Enables Vertical-Sync; true to enable, false to disable");

  po::options_description audio("Audio");
  audio.add_options()
    ("enable-audio",
     po::value<bool>()
      ->notifier([this](bool enable) { setAudioEnabled(enable); }),
     "Enables audio; true to enable false to disable")
    ("master-volume",
     po::value<float>()
      ->notifier([this](float volume) { setMasterVolume(volume); }),
     "Master volume; fractional value in range 0..1 inclusive")
    ("enable-music",
     po::value<bool>()
      ->notifier([this](bool enable) { setMusicEnabled(enable); }),
     "Enables music; true to enable, false to disable")
    ("music-volume",
     po::value<float>()
      ->notifier([this](float volume) { setMusicVolume(volume); }),
     "Music volume; fractional value in range 0..1 inclusive")
    ("enable-sfx",
     po::value<bool>()
      ->notifier([this](bool enable) { setSFXEnabled(enable); }),
     "Enables sound effects; true to enable, false to disable")
    ("sfx-volume",
     po::value<float>()
      ->notifier([this](float volume) { setSFXVolume(volume); }),
     "Sound effects volume; fractional value in range 0..1 inclusive");

  po::options_description commandLineOptions;
  commandLineOptions.add(general).add(video).add(audio);

  po::options_description settingsOptions;
  settingsOptions.add(video).add(audio);

  po::variables_map variables;
  LOG_SEVERITY(logger::get(), Debug) << "Processing command line arguments...";
  po::store(cmdParser
            .options(commandLineOptions)
            .style(po::command_line_style::default_style)
            .run(), variables);
  auto cmdResCount = variables.count("width") + variables.count("height");
  if (cmdResCount == 1)
    throw MAKE_EXCEPTION("Specified one dimension for screen resolution in command line without the other");
  bool msaaSetByCmdLine = variables.count("msaa-level") > 0;
  const bool settingsFilePresent = loadSettingsFile(m_settingsFileName, settingsOptions, variables, logger::get());
  po::notify(variables);

  if (variables.count("help") > 0) {
    std::cout << commandLineOptions << std::endl;
    return true;
  } else if (variables.count("version") > 0) {
    std::cout << "Parallel Assembly version " << PA_VERSION_STRING << std::endl;
    return true;
  }

  // Validate screen resolution and msaa level if set
  if (validateScreenResolution(*this, variables, resolution, cmdResCount > 0))
    setScreenResolution(resolution);
  if (validateAntiAliasingLevel(*this, variables, msaaLevel, msaaSetByCmdLine))
    setAALevel({msaaLevel});

  // No settings file at program load; write default values to it
  if (!settingsFilePresent) {
    LOG_SEVERITY(logger::get(), Warning) << "Couldn't find/open settings file; creating a new one";
    writeSettingsFile();
  }

  return false;
}

bool GameSettings::initialize(const char* lpCmdLine, const char* settingsFileName) {
  auto args = po::split_winmain(lpCmdLine);
  po::command_line_parser cmdParser(args);
  return initializeImpl(cmdParser, settingsFileName);
}
bool GameSettings::initialize(int argc, const char* argv[], const char* settingsFileName) {
  po::command_line_parser cmdParser(argc, argv);
  return initializeImpl(cmdParser, settingsFileName);
}

void GameSettings::writeSettingsFile() const {
  LOG_SEVERITY(logger::get(), Debug) << "Writing to settings file...";

  if (m_settingsFileName.empty())
    throw MAKE_EXCEPTION("Tried to write settings to settings file before initializing");

  // Write to temporary file first to avoid corrupting the original if something goes wrong
  auto tempFilePath = fs::path(m_settingsFileName).concat(".tmp");
  std::ofstream settingsFile(tempFilePath.string(), std::ios::out | std::ios::trunc);
  if (!settingsFile) {
    std::ostringstream ss;
    ss << "Failed to open temporary file " << tempFilePath
       << " for writing settings.\nDid you set an existing one as read-only?";
    throw MAKE_EXCEPTION(ss.str());
  }

  settingsFile << std::boolalpha;
  settingsFile << "#########\n# Video #\n#########\n\n";
  settingsFile << "# Screen resolution (width x height)\n";
  settingsFile << "width=" << m_screenResolution.x << '\n';
  settingsFile << "height=" << m_screenResolution.y << '\n';
  settingsFile << "# Display mode (windowed, borderless, fullscreen)\n";
  settingsFile << "display-mode=" << m_displayMode << '\n';
  settingsFile << "enable-aa=" << m_aaEnabled << '\n';
  settingsFile << "# Multi-Sampling Anti-Aliasing level (2,4,8,16; maximum depends on hardware)\n";
  settingsFile << "msaa-level=" << m_aaLevel.level << '\n';
  settingsFile << "enable-vsync=" << m_vsyncEnabled << '\n';
  settingsFile << "\n#########\n# Audio #\n#########\n\n";
  settingsFile << "enable-audio=" << m_audioEnabled << '\n';
  settingsFile << "# fractional value in range 0..1 inclusive\n";
  settingsFile << "master-volume=" << m_masterVolume << '\n';
  settingsFile << "enable-music=" << m_musicEnabled << '\n';
  settingsFile << "# freactional value in range 0..1 inclusive\n";
  settingsFile << "music-volume=" << m_musicVolume << '\n';
  settingsFile << "enable-sfx=" << m_sfxEnabled << '\n';
  settingsFile << "# freactional value in range 0..1 inclusive\n";
  settingsFile << "sfx-volume=" << m_sfxVolume << '\n';

  // Close temp file to commit to disk
  settingsFile.close();

  fs::path settingsFilePath{m_settingsFileName};
  boost::system::error_code error;
  // Remove the original if it exists
  if (fs::exists(settingsFilePath) &&
      !fs::remove(settingsFilePath, error))
  {
    std::ostringstream ss;
    ss << "Failed to replace settings file " << settingsFilePath << ": " << error.message()
       << "\nDid you set the file as read-only?\n\n"
       << "Your changes have been saved in " << tempFilePath
       << "; delete " << settingsFilePath.filename()
       << " and rename " << tempFilePath.filename()
       << " to " << settingsFilePath.filename() << " to update the file manually.";
    throw MAKE_EXCEPTION(ss.str());
  }
  // Rename temporary to original file name
  fs::rename(tempFilePath, settingsFilePath);
}

bool GameSettings::setScreenResolution(const glm::ivec2& screenResolution) {
  if (glm::any(glm::lessThan(screenResolution, minResolution)) ||
      glm::any(glm::greaterThan(screenResolution, maxResolution())))
  {
    LOG_SEVERITY(logger::get(), Warning)
      << "Tried to set screen resolution to something outside bounds: "
      << screenResolution.x << " x " << screenResolution.y;
    return false;
  }
  if (m_screenResolution != screenResolution) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Changing screen resolution from " << m_screenResolution.x << " x " << m_screenResolution.y
      << " to " << screenResolution.x << " x " << screenResolution.y;
    m_screenResolution = screenResolution;
    setVideoDirty();
  }
  return true;
}

void GameSettings::setDisplayMode(DisplayMode mode) {
  if (m_displayMode != mode) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Changing display mode from " << m_displayMode << " to " << mode;
    m_displayMode = mode;
    setVideoDirty();
  }
}

bool GameSettings::setAAEnabled(bool enabled) {
  if (enabled && !isAAAvailable()) {
    LOG_SEVERITY(logger::get(), Warning)
      << "Tried to enable anti-aliasing; hardware does not support it on this platform.";
    return false;
  }
  if (m_aaEnabled != enabled) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Changing AA enabled state from " << std::boolalpha << m_aaEnabled << " to " << enabled;
    m_aaEnabled = enabled;
    setVideoDirty();
  }
  return true;
}

bool GameSettings::setAALevel(const AntiAliasingLevel& level) {
  if (!isAAAvailable()) {
    LOG_SEVERITY(logger::get(), Warning)
      << "Tried to set anti-aliasing level to MSAA x" << level.level
      << "; MSAA is not supported on this platform.";
    return false;
  }
  if (std::find(m_aaLevels.begin(), m_aaLevels.end(), level) == m_aaLevels.end()) {
    std::ostringstream ss;
    ss << "Tried to set anti-aliasing level to unsupported value: MSAA x" << level.level;
    ss << "\nSupported levels are:\n";
    for (auto& l : m_aaLevels)
      ss << "MSAA x" << l.level << '\n';
    LOG_SEVERITY(logger::get(), Warning) << ss.str();
    return false;
  }
  if (m_aaLevel != level) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Changing MSAA level from x" << m_aaLevel.level << " to x" << level.level;
    m_aaLevel = level;
    setVideoDirty();
  }
  return true;
}

void GameSettings::setVSyncEnabled(bool enabled) {
  if (m_vsyncEnabled != enabled) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Changing VSync enabled state from " << std::boolalpha << m_vsyncEnabled << " to " << enabled;
    m_vsyncEnabled = enabled;
    setVideoDirty();
  }
}

void GameSettings::setAudioEnabled(bool enabled) {
  if (m_audioEnabled != enabled) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Changing audio enabled state from " << std::boolalpha << m_audioEnabled << " to " << enabled;
    m_audioEnabled = enabled;
    setAudioDirty();
  }
}
void GameSettings::setMasterVolume(float volume) {
  float clamped = glm::clamp(volume, 0.f, 1.f);
  if (clamped != volume) {
    LOG_SEVERITY(logger::get(), Warning)
      << "Tried to set master volume to " << volume << " (" << std::round(volume * 100.f)
      << " %). Accepted range is between 0 and 1; volume set to "
      << clamped << " (" << std::round(clamped * 100.f) << " %) instead.";
  }
  if (m_masterVolume != clamped) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Changing master volume from " << m_masterVolume << " (" << std::round(m_masterVolume * 100.f)
      << " %) to " << clamped << " (" << std::round(clamped * 100.f) << " %)";
    m_masterVolume = clamped;
    setAudioDirty();
  }
}

void GameSettings::setMusicEnabled(bool enabled) {
  if (m_musicEnabled != enabled) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Changing music enabled state from " << std::boolalpha << m_musicEnabled << " to " << enabled;
    m_musicEnabled = enabled;
    setAudioDirty();
  }
}
void GameSettings::setMusicVolume(float volume) {
  float clamped = glm::clamp(volume, 0.f, 1.f);
  if (clamped != volume) {
    LOG_SEVERITY(logger::get(), Warning)
      << "Tried to set music volume to " << volume << " (" << std::round(volume * 100.f)
      << " %). Accepted range is between 0 and 1; volume set to "
      << clamped << " (" << std::round(clamped * 100.f) << " %) instead.";
  }
  if (m_musicVolume != clamped) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Changing music volume from " << m_musicVolume << " (" << std::round(m_musicVolume * 100.f)
      << " %) to " << clamped << " (" << std::round(clamped * 100.f) << " %)";
    m_musicVolume = clamped;
    setAudioDirty();
  }
}

void GameSettings::setSFXEnabled(bool enabled) {
  if (m_sfxEnabled != enabled) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Changing sfx enabled state from " << std::boolalpha << m_sfxEnabled << " to " << enabled;
    m_sfxEnabled = enabled;
    setAudioDirty();
  }
}
void GameSettings::setSFXVolume(float volume) {
  float clamped = glm::clamp(volume, 0.f, 1.f);
  if (clamped != volume) {
    LOG_SEVERITY(logger::get(), Warning)
      << "Tried to set sfx volume to " << volume << " (" << std::round(volume * 100.f)
      << " %). Accepted range is between 0 and 1; volume set to "
      << clamped << " (" << std::round(clamped * 100.f) << " %) instead.";
  }
  if (m_sfxVolume != clamped) {
    LOG_SEVERITY(logger::get(), Debug)
      << "Changing sfx volume from " << m_sfxVolume << " (" << std::round(m_sfxVolume * 100.f)
      << " %) to " << clamped << " (" << std::round(clamped * 100.f) << " %)";
    m_sfxVolume = clamped;
    setAudioDirty();
  }
}

void GameSettings::backup() {
  if (settingsBackupInUse)
    throw MAKE_EXCEPTION("Tried to backup settings with backup already in use");
  settingsBackupInUse = true;
  settingsBackup = *this;
}
void GameSettings::commit() {
  settingsBackupInUse = false;
  // No need to bother clearing the backup

  // Write new settings to file
  if (isSettingsFileDirty()) {
    writeSettingsFile();
    clearSettingsFileDirty();
  }
}
void GameSettings::revertToBackup() {
  if (!settingsBackupInUse)
    throw MAKE_EXCEPTION("Tried to revert settings without having backed up settings");
  settingsBackupInUse = false;
  *this = settingsBackup;
  // ensure old settings are re-applied.
  setVideoDirty(true);
  setAudioDirty(true);
  // prevent writing the old values to the settings file again
  clearSettingsFileDirty();
}

namespace {
STATIC_LOGGER_INIT(logger) {
  logging::Logger lg;
  logging::setModuleName(lg, "GameSettings");
  return lg;
}
}
