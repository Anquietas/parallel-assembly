#pragma once
#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <exception>
#include <string>

class Exception : public std::exception {
  int m_line;
  std::string m_filePath;
  std::string m_what;
  std::string m_stackTrace;
public:
  explicit Exception(const std::string& message, const char* file, int line);
  explicit Exception(const char* message, const char* file, int line);
  Exception(const Exception& o) noexcept = delete;
  Exception(Exception&& o) noexcept = default;
  virtual ~Exception() override = default;

  Exception& operator=(const Exception& o) noexcept = delete;
  Exception& operator=(Exception&& o) noexcept = default;

  int line() const noexcept { return m_line; }
  const std::string& filePath() const noexcept { return m_filePath; }
  virtual const char* what() const noexcept override;
  const std::string& stackTrace() const noexcept { return m_stackTrace; }
};

#define MAKE_EXCEPTION(message) Exception(message, __FILE__, __LINE__)

#endif // EXCEPTION_H
