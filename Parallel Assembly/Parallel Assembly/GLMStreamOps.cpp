#include "GLMStreamOps.h"

#include <iostream>

namespace glm {

std::ostream& operator<<(std::ostream& out, const ivec2& v) {
  return out << '(' << v.x << ", " << v.y << ')';
}
std::ostream& operator<<(std::ostream& out, const ivec3& v) {
  return out << '(' << v.x << ", " << v.y << ", " << v.z << ')';
}
std::ostream& operator<<(std::ostream& out, const ivec4& v) {
  return out << '(' << v.x << ", " << v.y << ", " << v.z << ", " << v.w << ')';
}

std::ostream& operator<<(std::ostream& out, const vec2& v) {
  return out << '(' << v.x << ", " << v.y << ')';
}
std::ostream& operator<<(std::ostream& out, const vec3& v) {
  return out << '(' << v.x << ", " << v.y << ", " << v.z << ')';
}
std::ostream& operator<<(std::ostream& out, const vec4& v) {
  return out << '(' << v.x << ", " << v.y << ", " << v.z << ", " << v.w << ')';
}

std::ostream& operator<<(std::ostream& out, const mat2& m) {
  out << "mat2[";
  for (int i = 0; i < 2; ++i) {
    out << '[';
    for (int j = 0; j < 2; ++j)
      out << m[i][j] << ", ";
    out << "], ";
  }
  return out << ']';
}
std::ostream& operator<<(std::ostream& out, const mat3& m) {
  out << "mat3[";
  for (int i = 0; i < 3; ++i) {
    out << '[';
    for (int j = 0; j < 3; ++j)
      out << m[i][j] << ", ";
    out << "], ";
  }
  return out << ']';
}
std::ostream& operator<<(std::ostream& out, const mat4& m) {
  out << "mat4[";
  for (int i = 0; i < 4; ++i) {
    out << '[';
    for (int j = 0; j < 4; ++j)
      out << m[i][j] << ", ";
    out << "], ";
  }
  return out << ']';
}

std::ostream& operator<<(std::ostream& out, const quat& q) {
  return out << "quat(" << q.w << ", " << q.x << ", " << q.y << ", " << q.z << ')';
}
std::ostream& operator<<(std::ostream& out, const dualquat& dq) {
  return out << "dualquat(("
             << dq.real.w << ", " << dq.real.x << ", " << dq.real.y << ", " << dq.real.z << "), ("
             << dq.dual.w << ", " << dq.dual.x << ", " << dq.dual.y << ", " << dq.dual.z << "))";
}

} // namespace glm
