#ifndef GLMINCLUDES_H
#define GLMINCLUDES_H

#pragma once

#pragma warning(push)
#pragma warning(disable : 4201)

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat3x3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/dual_quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#pragma warning(pop)

#endif // GLMINCLUDES_H
