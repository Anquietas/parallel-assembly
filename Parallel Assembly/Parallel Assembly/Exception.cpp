#include "Exception.h"

#include <cstdlib>
#include <memory>
#include <sstream>

#include <boost/algorithm/string/case_conv.hpp>

#include "Logger.h"

#ifdef _WIN32
#undef WIN32_LEAN_AND_MEAN
#undef VC_EXTRALEAN
#include <Windows.h>
#include <DbgHelp.h>
#else
#include <execinfo.h>
#error Non-windows implementation is untested
#endif

STATIC_LOGGER_DECLARE(exceptionLogger);

std::string sanitizeFilePath(const char* filePath) {
  std::string filePathStr(filePath);
  std::string filePathStrLower = boost::algorithm::to_lower_copy(filePathStr);
  auto pos = filePathStrLower.rfind("parallel assembly");
  if (pos == std::string::npos)
    pos = 0;
  return filePathStr.substr(pos);
}

void dumpStackTrace(std::ostream& out) {
#ifdef _WIN32
  void* stack[60];
  USHORT frames = 0;
  std::unique_ptr<SYMBOL_INFO, void(*)(SYMBOL_INFO*)> symbol {
    static_cast<SYMBOL_INFO*>(std::calloc(1, sizeof(SYMBOL_INFO) + sizeof(char) * 256)),
    [](SYMBOL_INFO* ptr) {
      std::free(ptr);
    }
  };
  if (!symbol) {
    out << "Failed to get stack trace: error allocating symbol info";
    return;
  }
  symbol->MaxNameLen = 255;
  symbol->SizeOfStruct = sizeof(SYMBOL_INFO);

  HANDLE process = GetCurrentProcess();
  if (SymInitialize(process, nullptr, TRUE) == FALSE) {
    out << "Failed to get stack trace: error loading symbols";
    return;
  }

  // Skip first 2 frames since those are dumpStackTrace() and Exception's constructor
  frames = CaptureStackBackTrace(2, 60, stack, nullptr);
  out << std::showbase;
  for (USHORT i = 0; i < frames; ++i) {
    SymFromAddr(process, reinterpret_cast<DWORD64>(stack[i]), nullptr, symbol.get());

    out << std::dec << (frames - i - 1) << ": " << symbol->Name << " - " << std::hex << symbol->Address << '\n';
  }

  if (SymCleanup(process) == FALSE) {
    LOG_SEVERITY(exceptionLogger::get(), Error)
      << "Failed to clean up symbols for stack trace";
  }
#else
  void* stack[60]; // Can go much larger but windows only supports 60, so use 60 for consistency
  const int traceSize = backtrace(stack, 60);
  // Skip the first 2 frames since those are dumpStackTrace() and Exception's constructor
  std::unique_ptr<char*[], void(*)(char**)> traceSymbols{
    backtrace_symbols(stack + 2, traceSize - 2),
    [](char** ptr) {
      std::free(ptr);
    }
  };

  out << std::showbase;
  for (int i = 0; i < traceSize - 2; ++i)
    out << std::dec << (traceSize - i - 3) << ": " << traceSymbols[i] << " - " << std::hex << stack[i + 2] << '\n';
#endif
}

Exception::Exception(const std::string& message, const char* file, int line) try
  : exception()
  , m_line(line)
  , m_filePath(sanitizeFilePath(file))
  , m_what(message)
  , m_stackTrace()
{
  std::ostringstream ss;
  dumpStackTrace(ss);
  m_stackTrace = ss.str();
} catch (...) {
  LOG_SEVERITY(exceptionLogger::get(), Fatal)
    << "Exception thrown while constructing Exception(" << message << ", " << file << ", " << line << ')';
  throw;
}
Exception::Exception(const char* message, const char* file, int line) try
  : exception()
  , m_line(line)
  , m_filePath(sanitizeFilePath(file))
  , m_what(message)
  , m_stackTrace()
{
  std::ostringstream ss;
  dumpStackTrace(ss);
  m_stackTrace = ss.str();
} catch (...) {
  LOG_SEVERITY(exceptionLogger::get(), Fatal)
    << "Exception thrown while constructing Exception(" << message << ", " << file << ", " << line << ')';
  throw;
}

const char* Exception::what() const noexcept {
  return m_what.c_str();
}

STATIC_LOGGER_INIT(exceptionLogger) {
  logging::Logger lg;
  logging::setModuleName(lg, "Exception");
  return lg;
}
