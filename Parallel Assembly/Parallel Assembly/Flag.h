#pragma once
#ifndef FLAG_H
#define FLAG_H

class Flag final {
  bool m_value;

public:
  Flag(bool value = false)
    : m_value(value)
  {}
  Flag(const Flag& o) noexcept = default;
  Flag(Flag&& o) noexcept = default;
  ~Flag() = default;

  Flag& operator=(const Flag& o) noexcept = default;
  Flag& operator=(Flag&& o) noexcept = default;

  operator bool() const noexcept { return m_value; }

  bool get() const noexcept { return m_value; }
  void set() noexcept { m_value = true; }
  void clear() noexcept { m_value = false; }
};

#endif // FLAG_H