#include "Assert.h"

#include <cstdlib>
#include <cstdio>
#include <exception>
#include <sstream>

#include <SDL.h>

#include "Logger.h"

namespace boost {

namespace {
STATIC_LOGGER_DECLARE(logger);
}

// Cannot safely throw an exception of any kind!
void assertion_failed(const char* expr, const char* function, const char* file, long line) {
  try {
    try {
      std::ostringstream ss;
      ss << "expression \"" << expr << "\" was false in function:\n"
         << function << "\nin file: \"" << file << "\"\non line " << line;
      const auto message = ss.str();
      LOG_SEVERITY(logger::get(), Fatal) << "Assertion failure: " << message;

      if (SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Assertion Failure", message.c_str(), nullptr))
        LOG_SEVERITY(logger::get(), Fatal) << "Failed to display message box";
    // Exception logging assertion
    } catch (const std::exception& e) {
      LOG_SEVERITY(logger::get(), Fatal)
        << "Exception processing assertion failure: " << e.what();
    } catch (...) {
      LOG_SEVERITY(logger::get(), Fatal)
        << "Unknown exception processing assertion failure";
    }
  // Exception occurred handling exception from logging, fall back to C functions
  } catch (...) {
    fprintf(stderr, "Fatal error logging assertion failure");
  }

  std::abort();
}

// Cannot safely throw an exception of any kind!
void assertion_failed_msg(const char* expr, const char* msg, const char* function, const char* file, long line) {
  try {
    try {
      std::ostringstream ss;
      ss << msg << "\nexpression \"" << expr << "\" was false in function:\n"
         << function << "\nin file: \"" << file << "\"\non line " << line;
      const auto message = ss.str();
      LOG_SEVERITY(logger::get(), Fatal) << "Assertion failure: " << message;

      if (SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Assertion Failure", message.c_str(), nullptr))
        LOG_SEVERITY(logger::get(), Fatal) << "Failed to display message box";
      // Exception logging assertion
    } catch (const std::exception& e) {
      LOG_SEVERITY(logger::get(), Fatal)
        << "Exception processing assertion failure: " << e.what();
    } catch (...) {
      LOG_SEVERITY(logger::get(), Fatal)
        << "Unknown exception processing assertion failure";
    }
    // Exception occurred handling exception from logging, fall back to C functions
  } catch (...) {
    fprintf(stderr, "Fatal error logging assertion failure");
  }

  std::abort();
}

namespace {
STATIC_LOGGER_INIT(logger) {
  logging::Logger lg;
  return lg;
}
}

} // namespace boost
