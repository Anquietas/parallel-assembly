#include <atomic>
#include <chrono>
#include <csignal>
#include <iostream>
#include <mutex>
#include <sstream>
#include <vector>

#include <boost/core/ignore_unused.hpp>

#include <SDL.h>
#include <SDL_mixer.h>

#include "SDL/SDL.h"
#include "SDL/SDLMixer.h"
#include "SDL/GLWindow.h"
#include "SDL/EventHandler.h"

#include "Graphics/GUIRenderStage.h"
#include "Graphics/ShaderManager.h"
#include "Graphics/LevelGridRenderer.h"

#include "GUI/GUIManager.h"
#include "GUI/ConfirmExitPopup.h"
#include "GUI/SceneNames.h"
#include "GUI/Scene.h"
#include "GUI/PerformanceInfoComponent.h"

#include "Game/LevelManager.h"
#include "Game/SolutionFactoryManager.h"
#include "Game/ProfileManager.h"

#include "Assert.h"
#include "Exception.h"
#include "GameSettings.h"
#include "GLMStreamOps.h"
#include "Logger.h"

#ifndef NDEBUG
#include "Tests/UnitTests.h"
#endif

using namespace std::chrono_literals;
using Clock = std::chrono::steady_clock;
using std::chrono::duration_cast;
using std::chrono::nanoseconds;

// Returns only on SDL_QUIT or error
void gameLoop(sdl::GLWindow& window, sdl::EventHandler& handler);

#ifdef _WIN32
struct Console {
  Console() {
    // Try to attach to the existing console before creating one
    if (!AttachConsole(ATTACH_PARENT_PROCESS) && !AllocConsole())
      throw MAKE_EXCEPTION("Failed to create console");

    FILE* dummy = nullptr;
    freopen_s(&dummy, "CONIN$", "r", stdin);
    ASSERT(dummy == stdin);
    freopen_s(&dummy, "CONOUT$", "w", stdout);
    ASSERT(dummy == stdout);
    freopen_s(&dummy, "CONOUT$", "w", stderr);
    ASSERT(dummy == stderr);

    std::cout.clear();
    std::wcout.clear();
    std::cerr.clear();
    std::wcerr.clear();
    std::clog.clear();
    std::wclog.clear();

    std::cin.clear();
    std::wcin.clear();
  }
  ~Console() {
    auto result = FreeConsole();
    ASSERT(result); boost::ignore_unused(result);
  }
};
#endif // _WIN32

extern "C"
[[noreturn]] void signalHandlerSIGSEGV(int) {
  try {
    logging::Logger logger;
    LOG_SEVERITY(logger, Fatal) << "Segmentation Fault";
  } catch (const std::exception& e) {
    if (SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
                                 "Error logging segmentation fault",
                                 e.what(), nullptr))
      std::cerr << "Error: failed to log segfault: " << e.what() << std::endl;
  } catch (...) {
    if (SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
                                 "Error logging segmentation fault",
                                 "Unknown error", nullptr))
      std::cerr << "Error: failed to log segfault" << std::endl;
  }
  // Proceed to terminate
  std::terminate();
}
extern "C"
[[noreturn]] void signalHandlerSIGABRT(int) {
  try {
    logging::Logger logger;
    LOG_SEVERITY(logger, Fatal) << "abort() called";
  } catch (const std::exception& e) {
    if (SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
                                 "Error logging abort signal",
                                 e.what(), nullptr))
      std::cerr << "Error: failed to log abort: " << e.what() << std::endl;
  } catch (...) {
    if (SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
                                 "Error logging abort signal",
                                 "Unknown error", nullptr))
      std::cerr << "Error: failed to log abort" << std::endl;
  }
  // Restore default abort handler and abort
  std::signal(SIGABRT, SIG_DFL);
  std::abort();
}

class LogFlushThread : std::thread {
  std::atomic_flag keepFlushingLogs;

  // Flushes logs every second
  void flushLogThreadMain() {
    using namespace std::chrono_literals;
    while (keepFlushingLogs.test_and_set()) {
      std::this_thread::sleep_for(1s);
      logging::flush();
    }
    keepFlushingLogs.clear();
  }

public:
  LogFlushThread()
    : thread(&LogFlushThread::flushLogThreadMain, this)
    , keepFlushingLogs{ATOMIC_FLAG_INIT}
  {}
  ~LogFlushThread() {
    keepFlushingLogs.clear();
    join();
  }
};

//int main(int argc, const char* argv[]) {

int WINAPI WinMain(_In_ HINSTANCE /*hInstance*/,
                   _In_opt_ HINSTANCE /*hPrevInstance*/,
                   _In_ LPSTR lpCmdLine,
                   _In_ int /*nCmdShow*/)
{
  {
  auto start = Clock::now();
  std::ios::sync_with_stdio(false);
  Console console;

  logging::initialize();
  std::signal(SIGSEGV, signalHandlerSIGSEGV);
  std::signal(SIGABRT, signalHandlerSIGABRT);

  logging::Logger logger;
  logging::setModuleName(logger, "Main");

  try {
    LogFlushThread logFlushThread;
    sdl::SDL sdl(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
    sdl::SDLMixer sdlMixer;
    LOG_SEVERITY(logger, Debug) << "Initialized SDL";

    sdl::GLWindow window("Parallel Assembly", {1280, 720});
    auto gameSettings = GameSettings::get();
    if (gameSettings->initialize(lpCmdLine, "settings.cfg"))
      return EXIT_SUCCESS;
    window.updateDisplaySettings(); // Update display settings from newly loaded settings
    gameSettings->clearVideoDirty(); // Initialization should be complete

#ifndef NDEBUG
    test::runAllUnitTests();
#endif

    game::LevelManager levelManager;
    game::SolutionFactoryManager solutionFactoryManager;
    game::ProfileManager profileManager{levelManager, solutionFactoryManager};

    sdl::EventHandler eventHandler(window);
    eventHandler.attachLoggingHandlers();

    graphics::ShaderManager shaderManager;
    auto guiManager = gui::GUIManager::get();
    guiManager->initializeCustomWidgetData(shaderManager);
    guiManager->setupInputHandlers(eventHandler);

    graphics::LevelGridRenderer::setupShaders(shaderManager);

    auto& resolution = gameSettings->screenResolution();
    auto renderer = window.renderer();
    renderer->addRenderStage(std::make_unique<graphics::GUIRenderStage>(resolution));

    eventHandler.addWindowEventHandler(
      SDL_WINDOWEVENT_RESIZED,
      [](const SDL_WindowEvent& event, sdl::GLWindow& window) {
        window.renderer()->onResize({event.data1, event.data2});
      });
    eventHandler.addWindowEventHandler(
      SDL_WINDOWEVENT_SIZE_CHANGED,
      [](const SDL_WindowEvent&, sdl::GLWindow& window) {
        auto size = window.getSize();
        window.renderer()->onResize(size);
      });
    // Fix window not being redrawn during resize and move
    SDL_SetEventFilter(
      [](void* userdata, SDL_Event* event) -> int {
        if (event->type == SDL_WINDOWEVENT) {
          auto window = static_cast<sdl::GLWindow*>(userdata);
          switch (event->window.event) {
          case SDL_WINDOWEVENT_RESIZED:
            window->renderer()->onResize({event->window.data1, event->window.data2});
            // fallthrough
          case SDL_WINDOWEVENT_MOVED:
            window->draw();
            break;
          default:;
            // do nothing
          }
        }
        return 1;
      }, &window);

    guiManager->registerScenes(levelManager, profileManager);

    ASSERT_MSG(guiManager->sceneStackEmpty(),
               "GUIManager's scene stack is non-empty after initialization completed");
    if (profileManager.activeProfile()) {
      auto mainMenuScene = guiManager->lookupScene(gui::mainMenuSceneName);
      ASSERT_MSG(mainMenuScene, "main menu scene not registered");
      guiManager->pushSceneStack(*mainMenuScene);
    } else {
      auto switchProfileScene = guiManager->lookupScene(gui::switchProfileSceneName);
      ASSERT_MSG(switchProfileScene, "switch profile scene not registered");
      guiManager->pushSceneStack(*switchProfileScene);
    }

    SDL_ShowCursor(0);

    window.show();

    // Hide the cursor at startup in case it's outside the window
    // If it's not, SDL will trigger a mouse event when the window appears
    // which will trigger the cursor to be shown.
    guiManager->hideCursor();

    auto elapsed = Clock::now() - start;

    LOG_SEVERITY(logger, Profiling)
      << "Initialization complete in " << duration_cast<nanoseconds>(elapsed).count() << " ns";

    gameLoop(window, eventHandler);

    SDL_ShowCursor(1);
  } catch (const Exception& e) {
    LOG_SEVERITY(logger, Fatal)
      << e.filePath() << ':' << e.line() << ": " << e.what()
      << "\nStack trace:\n" << e.stackTrace();
    if (SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Exception", e.what(), nullptr))
      LOG_SEVERITY(logger, Fatal) << "Failed to display message box";

    return EXIT_FAILURE;
  } catch (const std::exception& e) {
    LOG_SEVERITY(logger, Fatal) << e.what();
    if (SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Exception", e.what(), nullptr))
      LOG_SEVERITY(logger, Fatal) << "Failed to display message box";

    return EXIT_FAILURE;
  } catch (...) {
    LOG_SEVERITY(logger, Fatal) << "Unknown Exception";
    if (SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Exception", "Unknown Exception; you should never see this", nullptr))
      LOG_SEVERITY(logger, Fatal) << "Failed to display message box";

    return EXIT_FAILURE;
  }

  }
  //std::system("pause");
  return EXIT_SUCCESS;
}

void gameLoop(sdl::GLWindow& window, sdl::EventHandler& handler) {
  bool quit = false;
  SDL_Event event;
  int result = 0;

  logging::Logger logger;
  logging::setModuleName(logger, "GameLoop");

  auto settings = GameSettings::get();
  auto guiManager = gui::GUIManager::get();

  gui::ConfirmExitPopup confirmExit;
  gui::PerformanceInfoComponent perfInfo{*window.renderer()};

  handler.addEventHandler(
    SDL_KEYUP, [&perfInfo, windowID = window.getID()](const SDL_Event& e, sdl::GLWindow& window) {
      auto& event = e.key;
      if (windowID == window.getID()) {
        if (event.keysym.sym == SDLK_F8) {
          if (perfInfo.isAttached())
            perfInfo.hide();
          else
            perfInfo.show();
        }
      }
    });

  while (!quit) {
    bool quitEvent = false;

    auto start = Clock::now();
    while (!quitEvent && (result = SDL_PollEvent(&event)) != 0)
      quitEvent = handler.dispatch(event);
    auto elapsed = Clock::now() - start;
    LOG_SEVERITY(logger, Profiling)
      << "Processing events took " << duration_cast<nanoseconds>(elapsed).count() << " ns";

    if (quitEvent) {
      if (confirmExit.isAttached())
        confirmExit.detachFromTarget();
      confirmExit.attachToActiveScene();
      confirmExit.show();
    }

    guiManager->injectTimePulse();

    quit = confirmExit.exitConfirmed();

    if (!quit) {
      if (settings->isVideoDirty()) {
        LOG_SEVERITY(logger, Debug) << "Game settings out of date, updating";
        window.updateDisplaySettings();

        settings->clearVideoDirty();
      }
      window.draw();
    }
  }
}
