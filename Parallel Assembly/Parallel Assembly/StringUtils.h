#pragma once
#ifndef STRING_UTILS_H
#define STRING_UTILS_H

#include <string>
#include <utility>

std::size_t utf32Length(const char* str, std::size_t utf8Size);
inline std::size_t utf32Length(const std::string& str) {
  return utf32Length(str.data(), str.size());
}

// Computes the (line number, column number) position of "where" in the range [begin, end)
// Will count null characters as any other character
std::pair<std::size_t, std::size_t>
getLinePosition(const char* begin, const char* end, const char* where);

#endif // STRING_UTILS_H
