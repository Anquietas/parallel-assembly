#pragma once
#ifndef PHOENIX_SINGLETON_INL
#define PHOENIX_SINGLETON_INL

#include "PhoenixSingleton.h"

template <typename T>
std::shared_ptr<T> PhoenixSingleton<T>::get() {
  static std::weak_ptr<T> instance;
  static std::mutex mutex;
  std::lock_guard<std::mutex> lock(mutex);
  auto ptr = instance.lock();
  if (!ptr) {
    ptr.reset(new T());
    instance = ptr;
  }
  return ptr;
}

#endif // PHOENIX_SINGLETON_INL
