#pragma once
#ifndef SIMPLEC_PARSER_ERROR_DIAGNOSTIC_H
#define SIMPLEC_PARSER_ERROR_DIAGNOSTIC_H

#include <queue>
#include <string>
#include <utility>

#include "SourceLocationInfo.h"

namespace simplec {
namespace parser {

class ErrorDiagnostic {
  std::queue<std::pair<SourcePosition, std::string>> m_diagnosticStack;
  bool m_finalized = false;

public:
  ErrorDiagnostic() = default;
  ErrorDiagnostic(const ErrorDiagnostic& o) = default;
  ErrorDiagnostic(ErrorDiagnostic&& o) = default;
  ~ErrorDiagnostic() = default;

  ErrorDiagnostic& operator=(const ErrorDiagnostic& o) = default;
  ErrorDiagnostic& operator=(ErrorDiagnostic&& o) = default;

  bool empty() const noexcept { return m_diagnosticStack.empty(); }

  const std::pair<SourcePosition, std::string>& nextError() const noexcept {
    return m_diagnosticStack.front();
  }

  void pop() {
    m_diagnosticStack.pop();
  }
  void push(const SourcePosition& pos, std::string message) {
    m_diagnosticStack.emplace(pos, std::move(message));
  }

  // Used to prevent error handling from propagating further up the grammar.
  bool isFinalized() const noexcept { return m_finalized; }
  void finalize() noexcept { m_finalized = true; }
  void reset() noexcept { m_finalized = false; }
};

}
}

#endif // SIMPLEC_PARSER_ERROR_DIAGNOSTIC_H
