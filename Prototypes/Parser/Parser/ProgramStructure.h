#pragma once
#ifndef SIMPLEC_PROGRAMSTRUCTURE_H
#define SIMPLEC_PROGRAMSTRUCTURE_H

#include "SimpleC_Core.h"
#include "AST_Expressions.h"
#include "AST_Statements.h"

#include <iostream>
#include <vector>

namespace simplec {
namespace ast {

class ProgramStructure {
  std::vector<GlobalDeclaration> m_globalVariables;
  std::vector<Function> m_functions;
  std::vector<Comment> m_comments;
public:
  ProgramStructure() = default;
  ProgramStructure(const ProgramStructure& o) = default;
  ProgramStructure(ProgramStructure&& o) noexcept = default;
  ~ProgramStructure() = default;

  ProgramStructure& operator=(const ProgramStructure& o) = default;
  ProgramStructure& operator=(ProgramStructure&& o) noexcept = default;

  void addGlobal(GlobalDeclaration global);
  std::size_t numGlobals() const noexcept { return m_globalVariables.size(); }
  auto globalsBegin() const noexcept { return m_globalVariables.cbegin(); }
  auto globalsEnd() const noexcept { return m_globalVariables.cend(); }
  const GlobalDeclaration& global(std::size_t index) const noexcept {
    return m_globalVariables[index];
  }

  void addFunction(Function function);
  std::size_t numFunctions() const noexcept { return m_functions.size(); }
  auto funcBegin() const noexcept { return m_functions.cbegin(); }
  auto funcEnd() const noexcept { return m_functions.cend(); }
  const Function& function(std::size_t index) const noexcept {
    return m_functions[index];
  }

  void addComment(Comment comment);
  std::size_t numComments() const noexcept { return m_comments.size(); }
  auto commentBegin() const noexcept { return m_comments.cbegin(); }
  auto commentEnd() const noexcept { return m_comments.cend(); }
  const Comment& comment(std::size_t index) const noexcept { return m_comments[index]; }
};

std::ostream& operator<<(std::ostream& out, const ProgramStructure& program);

}
}

#endif // SIMPLEC_PROGRAMSTRUCTURE_H
