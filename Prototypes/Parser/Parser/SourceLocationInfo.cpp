#include "SourceLocationInfo.h"

namespace simplec {

std::ostream& operator<<(std::ostream& out, const SourcePosition& pos)
{
  return out << '(' << pos.line() << ":" << pos.column() << ')';
}

}