#include "Skipper.h"

#ifdef _MSC_VER
#pragma warning(push)
// Redefinitions - internal problem with spirit, compiles anyway.
#pragma warning(disable:4348)
// Declaration hides outer declaration
#pragma warning(disable:4459)
// Unreferenced formal parameter
#pragma warning(disable:4100)
#endif 

#include <boost/spirit/include/qi_auxiliary.hpp>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include "LinePosIterator.h"
#include "ParserBuildConfig.h"

// Decorated name length exceeded
#ifdef _MSC_VER
#pragma warning(disable:4503)
#endif

namespace simplec {
namespace parser {
namespace detail {

namespace ascii = boost::spirit::ascii;

template <typename Iterator>
Skipper<Iterator>::Skipper()
  : qi::grammar<Iterator>(start)
{
  // Comment grammar handles error detection in comments.
  commentEnd %= qi::eol | qi::eoi;
  comment = qi::lexeme["//" >> *(ascii::char_ - commentEnd) >> commentEnd];

  start.name("simplec-skipper");
  start = +(ascii::space | comment);
}

template <typename Iterator>
using LinePos = utility::LinePosIterator<Iterator>;

template class Skipper<LinePos<const char*>>;

}
}
}
