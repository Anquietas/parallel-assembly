#include "Program.h"

#include <utility>

#include "ProgramStructure.h"

namespace simplec {

Program::Program(std::unique_ptr<ast::ProgramStructure> structure) noexcept
  : m_structure(std::move(structure))
{}
Program::Program(const Program& p)
  : m_structure(std::make_unique<ast::ProgramStructure>(*p.m_structure))
{}
Program::Program(Program&& p) noexcept
  : m_structure(std::move(p.m_structure))
{}
Program::~Program() {}

Program& Program::operator=(const Program& p) {
  m_structure = std::make_unique<ast::ProgramStructure>(*p.m_structure);

  return *this;
}
Program& Program::operator=(Program&& p) noexcept {
  m_structure = std::move(p.m_structure);

  return *this;
}

}
