#pragma once
#ifndef SIMPLEC_TYPECHECKER_ENVIRONMENT_H
#define SIMPLEC_TYPECHECKER_ENVIRONMENT_H

#include <vector>
#include <map>
#include <utility>

#include <boost/optional.hpp>

#include "TypeCheckerTypes.h"

namespace simplec {
namespace typecheck {

using ast::Identifier;
inline namespace literals {
using namespace ast::literals;
}

namespace detail {
// To get rid of line length warnings
struct Scope : public std::map<Identifier, Type> {
  using std::map<Identifier, Type>::map;
};
}

class Environment {
protected:
  using FuncTable = std::multimap<Identifier, FunctionType>;
public:
  using FuncMatch = std::pair<FuncTable::const_iterator, FuncTable::const_iterator>;
protected:
  using MemFuncTable = std::map<Identifier, FunctionType>;
  using Scope = detail::Scope;
  using FuncScope = std::vector<Scope>;

  // User functions and simple library functions
  FuncTable m_functions;

  MemFuncTable m_stringMemFuncs;
  MemFuncTable m_arrayMemFuncs;

  Scope m_globals;
  FuncScope m_locals;
public:
  Environment();
  Environment(const Environment& o) = default;
  Environment(Environment&& o) = default;
  ~Environment() = default;

  Environment& operator=(const Environment& o) = default;
  Environment& operator=(Environment&& o) = default;

  // type will be moved if successful, but not if it fails.
  bool addFunction(const Identifier& id, FunctionType& type);
  bool addGlobal(const Identifier& id, Type type);
  bool addLocal(const Identifier& id, Type type);

  const FunctionType* lookupFunction(
    const Identifier& id,
    const std::vector<Type>& paramTypes) const;
  FuncMatch lookupFunction(const Identifier& id) const;
  // Used for special functions that don't fit in a FuncTable.
  // e.g. print
  boost::optional<FunctionType> lookupLibraryFunction(
    const Identifier& id,
    const std::vector<Type>& paramTypes) const;
  const FunctionType * lookupStringMemFunc(const Identifier& id) const;
  boost::optional<FunctionType> lookupArrayMemFunc(
    ArrayType type,
    const Identifier& id) const;
  const Type* lookupVariable(const Identifier& id) const;

  bool atRootScope() const noexcept { return m_locals.empty(); }
  void scopePush();
  void scopePop();
};

}
}

#endif // SIMPLEC_TYPECHECKER_ENVIRONMENT_H
