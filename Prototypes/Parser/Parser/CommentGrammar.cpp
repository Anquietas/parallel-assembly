#include "CommentGrammar.h"

#ifdef _MSC_VER
#pragma warning(push)
// Redefinitions - internal problem with spirit, compiles anyway.
#pragma warning(disable:4348)
// Declaration hides outer declaration
#pragma warning(disable:4459)
// Unreferenced formal parameter
#pragma warning(disable:4100)
#endif

#include <boost/spirit/include/qi_auxiliary.hpp>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include "Annotator.h"
#include "ErrorHandlers.h"
#include "AST_Statements.h"
#include "SpiritProgramStructureHandler.h"
#include "LinePosIterator.h"
#include "ParserBuildConfig.h"

// Decorated name length exceeded
#ifdef _MSC_VER
#pragma warning(disable:4503)
#endif

namespace simplec {
namespace parser {
namespace detail {

namespace ascii {
using namespace boost::spirit::ascii;
}

template <typename Iterator>
CommentSkipper<Iterator>::CommentSkipper()
  : qi::grammar<Iterator>(start)
{
  using ascii::char_;

  start.name("comment-skipper");
  start = +(char_ - "//");
}

template <typename Iterator>
CommentGrammar<Iterator>::CommentGrammar(ErrorDiagnostic& diagnostic)
  : base_type(program, "program (comment parser)")
{
    using ascii::char_;

    phx::function<Annotator<Iterator>> annotator;
    phx::function<DefaultErrorHandler<Iterator>> defaultErrorHandler;

    auto annotate = annotator(qi::_val, qi::_1, qi::_2, qi::_3);
    auto defaultError = defaultErrorHandler(&diagnostic, false, qi::_1, qi::_2, qi::_3, qi::_4);

    static rule<qi::unused_type, qi::unused_type> commentEnd;
    static rule<ast::Comment()> comment;

    commentEnd %= qi::eol | qi::eoi;
    comment %= qi::as_string[qi::lexeme["//" > *(char_ - commentEnd) > commentEnd]];

    qi::on_success(comment, annotate);
    qi::on_error(comment, defaultError);

    program %= *comment;

#ifdef USE_DEBUG_RULE_NAMES
    commentEnd.name("commentEnd");
    comment.name("comment");
    program.name("program (comment parser)");
#else
    commentEnd.name("newline or end of program");
    comment.name("comment");
    program.name("program (comment parser)");
#endif

#ifdef ENABLE_RULE_DEBUG
    qi::debug(commentEnd);
    qi::debug(comment);
    qi::debug(program);
#endif
}

template <typename Iterator>
using LinePos = utility::LinePosIterator<Iterator>;

template class CommentSkipper<LinePos<const char*>>;
template class CommentGrammar<LinePos<const char*>>;

}
}
}
