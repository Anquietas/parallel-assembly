#include "ParserHelperFunctors.h"

#include <utility>

namespace simplec {
namespace parser {
namespace detail {

#define DELETE_DEFAULT_CONSTRUCTOR_AND_ASSIGN_OPERATOR(T) \
  T () = delete; \
  T & operator=(const T &) = delete

ast::Expression StringLitExpressionHelper::operator()(
  ast::string_t strLit,
  boost::optional<ast::FuncCall> memFunc) const
{
  if (memFunc)
    return ast::Expression(ast::MemberExpression(
      ast::SimpleValue(std::move(strLit)), std::move(*memFunc)));
  else
    return ast::Expression(ast::SimpleExpression(ast::SimpleValue(std::move(strLit))));
}

struct ParamsOrSubscriptsMemIncrVisitor : boost::static_visitor<ast::Expression> {
  ast::Identifier& identifier;

  ParamsOrSubscriptsMemIncrVisitor(ast::Identifier& id) : identifier(id) {}

  DELETE_DEFAULT_CONSTRUCTOR_AND_ASSIGN_OPERATOR(ParamsOrSubscriptsMemIncrVisitor);

  // basic expression is a function
  ast::Expression operator()(ExprList& funcParams) const {
    return ast::Expression(ast::SimpleExpression(
      ast::FuncCall(std::move(identifier), std::move(funcParams))));
  }

  struct IncrMemExprVisitor : boost::static_visitor<ast::Expression> {
    ast::Identifier& identifier;
    ExprList& exprList;

    IncrMemExprVisitor(ast::Identifier& id, ExprList& exprs)
      : identifier(id), exprList(exprs)
    {}

    DELETE_DEFAULT_CONSTRUCTOR_AND_ASSIGN_OPERATOR(IncrMemExprVisitor);

    // basic expression is an identifier or array lookup, followed by a post-increment
    ast::Expression operator()(ast::IncrOp op) const {
      // basic expression is an identifier followed by a post-increment
      if (exprList.empty())
        return ast::Expression(ast::IncrExpression(op,
          ast::BasicExpression(std::move(identifier))));
      // basic expression is an array lookup followed by a post-increment
      else
        return ast::Expression(ast::IncrExpression(op,
          ast::BasicExpression(ast::ArrayLookup(
            std::move(identifier), std::move(exprList)))));
    }
    // basic expression is an identifier or array lookup, followed by a member expression
    ast::Expression operator()(ast::FuncCall& funcCall) const {
      // basic expression is an identifier followed by a member expression
      if (exprList.empty())
        return ast::Expression(ast::MemberExpression(
          ast::BasicExpression(std::move(identifier)),
          std::move(funcCall)));
      // basic expression is an array lookup followed by a member expression
      else
        return ast::Expression(ast::MemberExpression(
          ast::BasicExpression(ast::ArrayLookup(
            std::move(identifier), std::move(exprList))),
          std::move(funcCall)));
    }
  };

  ast::Expression operator()(SubscriptsIncrOrMemFunc& pair) const {
    using fus::at_c;
    // basic expression is an identifier or an array lookup
    if (!at_c<1>(pair)) {
      // basic expression is just a lone identifier.
      if (at_c<0>(pair).size() == 0)
        return ast::Expression(ast::SimpleExpression(
          ast::BasicExpression(std::move(identifier))));
      // basic expression is a simple array lookup
      else
        return ast::Expression(ast::SimpleExpression(
          ast::BasicExpression(ast::ArrayLookup(
            std::move(identifier), std::move(at_c<0>(pair))))));
    }
    // basic expression is an identifier or array lookup, followed by a post-increment or a member function call
    else
      return boost::apply_visitor(IncrMemExprVisitor{ identifier, at_c<0>(pair) }, *at_c<1>(pair));
  }
};

ast::Expression IdentifierExpressionHelper::operator()(
  ast::Identifier identifier,
  ParamsOrSubscriptsMemIncr expr) const
{
  return boost::apply_visitor(ParamsOrSubscriptsMemIncrVisitor{ identifier }, expr);
}

ast::IncrExpression PreIncrExpressionHelper::operator()(
  ast::IncrOp op,
  VarOrArrayLookup expr) const
{
  using fus::at_c;
  if (at_c<1>(expr).empty())
    return ast::IncrExpression(op, ast::BasicExpression(std::move(at_c<0>(expr))));
  else
    return ast::IncrExpression(op,
      ast::BasicExpression(ast::ArrayLookup(std::move(at_c<0>(expr)),
        std::move(at_c<1>(expr)))));
}

ast::Expression ExpressionHelper::operator()(
  ast::Expression left,
  std::vector<fus::vector2<ast::BinaryOp, ast::Expression>> rest) const
{
  using fus::at_c;
  for (auto& pair : rest) {
    left = ast::Expression(
      ast::BinaryOpExpression(
        std::move(left),
        std::move(at_c<0>(pair)),
        std::move(at_c<1>(pair))));
  }

  return std::move(left);
}

struct SubscriptOrFuncCallVisitor : boost::static_visitor<ast::ExpressionStatement> {
  using base_type = boost::static_visitor<ast::ExpressionStatement>;
  ast::Identifier& identifier;

  SubscriptOrFuncCallVisitor(ast::Identifier& id)
    : base_type(), identifier(id)
  {}

  DELETE_DEFAULT_CONSTRUCTOR_AND_ASSIGN_OPERATOR(SubscriptOrFuncCallVisitor);

  struct AssignOrIncrVisitor : boost::static_visitor<ast::ExpressionStatement> {
    using base_type = boost::static_visitor<ast::ExpressionStatement>;
    ast::BasicExpression left;

    AssignOrIncrVisitor(ast::BasicExpression l)
      : base_type(), left(std::move(l))
    {}

    ast::ExpressionStatement operator()(AssignRHS& assign) const {
      using fus::at_c;
      auto op = at_c<0>(assign);
      auto& right = at_c<1>(assign);
      return ast::ExpressionStatement(
        ast::AssignExpression(std::move(left), op, std::move(right)));
    }
    ast::ExpressionStatement operator()(ast::IncrOp op) const {
      return ast::ExpressionStatement(ast::IncrExpression(op, std::move(left)));
    }
  };

  ast::ExpressionStatement operator()(SubscriptAssignIncr& assignOrIncr) const {
    using fus::at_c;
    auto& subscripts = at_c<0>(assignOrIncr);
    auto& right = at_c<1>(assignOrIncr);

    if (subscripts.empty())
      return boost::apply_visitor(
        AssignOrIncrVisitor(ast::BasicExpression(std::move(identifier))), right);
    else
      return boost::apply_visitor(
        AssignOrIncrVisitor(ast::BasicExpression(
          ast::ArrayLookup(std::move(identifier), std::move(subscripts)))), right);
  }
  ast::ExpressionStatement operator()(ExprList& funcParams) const {
    return ast::ExpressionStatement(
      ast::FuncCall(std::move(identifier), std::move(funcParams)));
  }
};

ast::ExpressionStatement AssignExpressionHelper::operator()(
  ast::Identifier identifier,
  SubscriptOrFuncCall expr) const
{
  return boost::apply_visitor(SubscriptOrFuncCallVisitor(identifier), expr);
}

struct VarInitializerVisitor : boost::static_visitor<ast::DeclarationStatement> {
  using base_type = boost::static_visitor<ast::DeclarationStatement>;
  bool isConst;
  ast::PrimitiveType type;
  ast::Identifier& identifier;

  VarInitializerVisitor(bool isConst_, ast::PrimitiveType t, ast::Identifier& id)
    : base_type(), isConst(isConst_), type(t), identifier(id)
  {}

  DELETE_DEFAULT_CONSTRUCTOR_AND_ASSIGN_OPERATOR(VarInitializerVisitor);

  ast::DeclarationStatement operator()(ast::Expression& expr) const {
    return ast::VariableDeclaration(type, isConst, std::move(identifier), std::move(expr));
  }
  ast::DeclarationStatement operator()(ArraySubscriptsInitializer& arraySubsAndInit) const {
    using fus::at_c;
    BOOST_ASSERT_MSG(at_c<0>(arraySubsAndInit).size() >= 1,
      "Empty TypeSubscript list parsed for array declaration");
    return ast::ArrayDeclaration(
      ast::ArrayType<true>(type, isConst, std::move(at_c<0>(arraySubsAndInit))),
      std::move(identifier),
      std::move(at_c<1>(arraySubsAndInit)));
  }
};

ast::DeclarationStatement VarDeclareHelper::operator()(
  bool isConst,
  DeclarationStmt varDecl) const
{
  using fus::at_c;
  auto& type = at_c<0>(varDecl);
  auto& identifier = at_c<1>(varDecl);
  auto& initOrArray = at_c<2>(varDecl);
  return boost::apply_visitor(VarInitializerVisitor{ isConst, type, identifier }, initOrArray);
}

ast::Parameter ParamHelper::operator()(
  ast::PrimitiveType type,
  ast::Identifier identifier,
  std::vector<ast::OptTypeSubscript> subscripts) const
{
  if (subscripts.empty())
    return ast::Parameter(type, std::move(identifier));
  else
    return ast::Parameter(ast::ArrayType<true>(type, false, std::move(subscripts)),
      std::move(identifier));
}

struct GlobalVarInitVisitor : boost::static_visitor<ast::GlobalDeclaration> {
  using base_type = boost::static_visitor<ast::GlobalDeclaration>;
  ast::PrimitiveType type;
  bool isConst;
  ast::Identifier& identifier;

  GlobalVarInitVisitor(ast::PrimitiveType t, bool isConst_, ast::Identifier& id)
    : base_type(), type(t), isConst(isConst_), identifier(id)
  {}
  DELETE_DEFAULT_CONSTRUCTOR_AND_ASSIGN_OPERATOR(GlobalVarInitVisitor);

  // Definitely a primitive type variable
  ast::GlobalDeclaration operator()(ast::Expression& initializer) const {
    return ast::VariableDeclaration(type, isConst, std::move(identifier), std::move(initializer));
  }
  // Definitely a global array
  ast::GlobalDeclaration operator()(GlobalArrayInitializer& subscriptInit) const {
    using fus::at_c;
    auto& subscripts = at_c<0>(subscriptInit);
    auto& initializer = at_c<1>(subscriptInit);
    auto arrayType = ast::ArrayType<false>(type, isConst, std::move(subscripts));
    return ast::GlobalArrayDeclaration(
      std::move(arrayType),
      std::move(identifier),
      std::move(initializer));
  }
};

struct BasicGlobalStatementVisitor : boost::static_visitor<GlobalStatement> {
  using base_type = boost::static_visitor<GlobalStatement>;
  ast::PrimitiveType type;

  BasicGlobalStatementVisitor(ast::PrimitiveType t)
    : base_type(), type(t)
  {}

  struct VarInitOrFuncDeclVisitor : boost::static_visitor<GlobalStatement> {
    using base_type = boost::static_visitor<GlobalStatement>;
    ast::PrimitiveType type;
    ast::Identifier& identifier;

    VarInitOrFuncDeclVisitor(ast::PrimitiveType t, ast::Identifier& id)
      : base_type(), type(t), identifier(id)
    {}

    DELETE_DEFAULT_CONSTRUCTOR_AND_ASSIGN_OPERATOR(VarInitOrFuncDeclVisitor);

    // Definitely a global variable or array
    GlobalStatement operator()(GlobalVarInitializer& varDecl) const {
      return boost::apply_visitor(GlobalVarInitVisitor(type, false, identifier), varDecl);
    }
    // Definitely a function declaration
    GlobalStatement operator()(FuncParamAndBody& funcDecl) const {
      using fus::at_c;
      ast::ReturnType returnType = type;
      auto& params = at_c<0>(funcDecl);
      auto& body = at_c<1>(funcDecl);
      return ast::Function(
        std::move(returnType),
        std::move(identifier),
        std::move(params),
        std::move(body));
    }
  };

  // First option for basicGlobalStatement
  GlobalStatement operator()(IdVarInitOrFuncDecl& funcOrValDecl) const {
    using fus::at_c;
    auto& ident = at_c<0>(funcOrValDecl);
    auto& varOrFuncDecl = at_c<1>(funcOrValDecl);
    return boost::apply_visitor(VarInitOrFuncDeclVisitor(type, ident), varOrFuncDecl);
  }
  // Second option for basicGlobalStatement
  GlobalStatement operator()(ArrayReturnFuncDecl& funcDecl) const {
    using fus::at_c;
    ast::ReturnType returnType =
      ast::ArrayType<true>(type, false, std::move(at_c<0>(funcDecl)));
    auto& ident = at_c<1>(funcDecl);
    auto& paramsBody = at_c<2>(funcDecl);
    auto& params = at_c<0>(paramsBody);
    auto& body = at_c<1>(paramsBody);
    return ast::Function(
      std::move(returnType),
      std::move(ident),
      std::move(params),
      std::move(body));
  }
};

GlobalStatement GlobalStatementHelper::operator()(
  ast::PrimitiveType type,
  BasicGlobalStatement funcOrVarDecl) const
{
  return boost::apply_visitor(BasicGlobalStatementVisitor(type), funcOrVarDecl);
}
ast::GlobalDeclaration GlobalStatementHelper::operator()(
  ast::PrimitiveType type,
  ast::Identifier identifier,
  GlobalVarInitializer varDecl) const
{
  // Const matched as part of the rule
  return boost::apply_visitor(GlobalVarInitVisitor{ type, true, identifier }, varDecl);
}
GlobalStatement GlobalStatementHelper::operator()(
  ast::Identifier identifier,
  FuncParamAndBody funcDecl) const
{
  using fus::at_c;
  ast::ReturnType returnType = ast::Void();
  auto& params = at_c<0>(funcDecl);
  auto& body = at_c<1>(funcDecl);
  return ast::Function(
    std::move(returnType),
    std::move(identifier),
    std::move(params),
    std::move(body));
}

#undef DELETE_DEFAULT_CONSTRUCTOR_AND_ASSIGN_OPERATOR

}
}
}
