#pragma once
#ifndef SIMPLEC_PARSER_SIMPLEC_GRAMMAR_H
#define SIMPLEC_PARSER_SIMPLEC_GRAMMAR_H

#define BOOST_SPIRIT_USE_PHOENIX_V3 1

#ifdef _MSC_VER
#pragma warning(push)
// Redefinitions - internal problem with spirit, compiles anyway.
#pragma warning(disable:4348)
// Declaration hides outer declaration
#pragma warning(disable:4459)
// Unreferenced formal parameter
#pragma warning(disable:4100)
#endif 

#include <boost/spirit/include/qi_core.hpp>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include "Skipper.h"
#include "ParserInternalTypes.h"
#include "ProgramStructure.h"
#include "ErrorDiagnostic.h"

#define MAKE_NON_COPY_MOVEABLE(T) \
  T (const T &) = delete; \
  T ( T &&) = delete; \
  T& operator=(const T &) = delete; \
  T& operator=( T &&) = delete

namespace simplec {
namespace parser {
namespace detail {

namespace qi {
using namespace boost::spirit::qi;
}
namespace phx {
using namespace boost::phoenix;
}

template <typename Iterator>
class SimpleCGrammar : public qi::grammar<Iterator, Skipper<Iterator>, ast::ProgramStructure()> {
  using base_type = qi::grammar<Iterator, Skipper<Iterator>, ast::ProgramStructure()>;
  template <typename T = qi::unused_type, typename skip = Skipper<Iterator>>
  using rule = qi::rule<Iterator, skip, T>;

  rule<ast::ProgramStructure()>                   program;

public:
  SimpleCGrammar(ErrorDiagnostic& diagnostic);
  MAKE_NON_COPY_MOVEABLE(SimpleCGrammar);
};

}
}
}

#undef MAKE_NON_COPY_MOVEABLE

#endif // SIMPLEC_PARSER_SIMPLEC_GRAMMAR_H
