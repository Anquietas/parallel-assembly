#include "TypeChecker.h"

#include <sstream>
#include <cassert>
#include <algorithm>
#include <iterator>

namespace simplec {
namespace typecheck {

TypePrinter::TypePrinter(std::ostream& out) : m_out(out) {}

void TypePrinter::operator()(PrimitiveType type) const {
  switch (type) {
  case PrimitiveType::Bool:
    m_out << "bool";
    break;
  case PrimitiveType::Int:
    m_out << "int";
    break;
  case PrimitiveType::Float:
    m_out << "float";
    break;
  case PrimitiveType::Char:
    m_out << "char";
    break;
  case PrimitiveType::String:
    m_out << "string";
    break;
  default:
    assert(false);
  }
}
void TypePrinter::operator()(const Void&) const {
  m_out << "void";
}
void TypePrinter::operator()(const DeferredType&) const {
  m_out << "<deferred>";
}
void TypePrinter::operator()(const SimpleType& type) const {
  if (type.isConst())
    m_out << "const ";
  (*this)(type.type());
}
void TypePrinter::operator()(const ArrayType& type) const {
  if (type.isConst())
    m_out << "const ";

  assert(type.rank() > 0);

  (*this)(type.elemType());
  auto begin = type.extentsBegin();
  auto end = type.extentsEnd();
  for (; begin != end; ++begin) {
    m_out << '[';
    if (*begin != 0)
      m_out << *begin;
    m_out << ']';
  }
}

struct FuncTypeSubscriptVisitor : boost::static_visitor<std::size_t> {
  std::size_t operator()(std::size_t value) const {
    return value;
  }
  std::size_t operator()(const Identifier& id) const {
    std::ostringstream ss;
    ss << "identifier \'" << id.name()
      << "\' used as type subscript in function declaration. "
         "Use an empty subscript (\"[]\") for arrays of unknown size.";
    throw TypeError(id.begin(), ss.str());
  }

  std::size_t operator()(const ast::TypeSubscript& subscript) const {
    return boost::apply_visitor(*this, subscript.value());
  }
  std::size_t operator()(const ast::OptTypeSubscript& optSubscript) const {
    if (optSubscript)
      return boost::apply_visitor(*this, optSubscript->value());
    else
      return 0;
  }
};

class TypeSubscriptVisitor : public boost::static_visitor<std::size_t> {
  const Environment& m_env;
public:
  TypeSubscriptVisitor(const Environment& env) : m_env(env) {}

  std::size_t operator()(std::size_t value) const {
    return value;
  }
  std::size_t operator()(const Identifier& id) const {
    auto type = m_env.lookupVariable(id);
    if (type == nullptr) {
      std::ostringstream ss;
      ss << "Undeclared identifier \'" << id.name() << "\' used as type subscript.";
      throw TypeError(id.begin(), ss.str());
    }
    assert(!isDeferred(*type));
    bool validType = true;
    if (!isSimple(*type))
      validType = false;
    else {
      auto& simpleType = boost::get<SimpleType>(*type);
      if (!simpleType.isConst() || simpleType.type() != PrimitiveType::Int)
        validType = false;
    }
    if (!validType) {
      std::ostringstream ss;

      ss << "Invalid type subscript: identifier \'" << id.name() << "\' is of type ";
      TypePrinter printer(ss);
      boost::apply_visitor(printer, *type);
      ss << "; type subscripts must be const int.";

      throw TypeError(id.begin(), ss.str());
    }
    return 0;
  }

  std::size_t operator()(const ast::TypeSubscript& subscript) const {
    return boost::apply_visitor(*this, subscript.value());
  }
  std::size_t operator()(const ast::OptTypeSubscript& optSubscript) const {
    if (optSubscript)
      return boost::apply_visitor(*this, optSubscript->value());
    else
      return 0;
  }
};

struct FunctionComponentTypeVisitor : boost::static_visitor<Type> {
  Type operator()(const Void& type) const {
    return type;
  }
  Type operator()(PrimitiveType type) const {
    return SimpleType(type, true);
  }
  Type operator()(const ast::ArrayType<true>& type) const {
    std::size_t numSubscripts = type.numSubscripts();
    auto result = ArrayType(type.elemType(), true, numSubscripts);

    for (std::size_t i = 0; i < numSubscripts; ++i) {
      auto& optSubscript = type.subscript(i);
      if (!optSubscript)
        // extent already initialized to 0 by default
        continue;
      auto& subscript = *optSubscript;
      result.setExtent(i, boost::apply_visitor(FuncTypeSubscriptVisitor(), subscript.value()));
    }

    return result;
  }
};

template <typename Iterator>
void printFuncParams(std::ostream& out, Iterator begin, const Iterator& end) {
  static_assert(
    std::is_same<std::iterator_traits<Iterator>::value_type, Type>::value,
    "printFuncParams expects iterators that dereference to type Type");
  TypePrinter printer(out);
  if (begin != end)
    boost::apply_visitor(printer, *begin++);
  for (; begin != end; ++begin) {
    out << ", ";
    boost::apply_visitor(printer, *begin);
  }
}

FunctionType computeFunctionType(const ast::Function& function) {
  // Extract parameter types from function
  std::vector<Type> params;
  params.reserve(function.numParams());
  auto paramsIter = function.paramsBegin();
  auto paramsEnd = function.paramsEnd();

  FunctionComponentTypeVisitor visitor;
  for (; paramsIter != paramsEnd; ++paramsIter)
    params.emplace_back(boost::apply_visitor(visitor, paramsIter->type()));

  return FunctionType(
    boost::apply_visitor(visitor, function.returnType()),
    std::move(params));
}

void validate(const ast::Function& function, Environment& env) {
  auto type = computeFunctionType(function);
  if (function.id().name() == "main" &&
      (type.numParams() != 0 || type.returnType() != Type(Void())))
  {
    std::ostringstream ss;
    TypePrinter printer(ss);
    ss << "main function declared as ";
    boost::apply_visitor(printer, type.returnType());
    ss << " main(";
    printFuncParams(ss, type.paramsBegin(), type.paramsEnd());
    ss << "), expected void main()";
    throw TypeError(function.begin(), ss.str());
  }
  if (!env.addFunction(function.id(), type)) {
    std::ostringstream ss;
    ss << "Redefinition of function " << function.id();
    ss << '(';
    printFuncParams(ss, type.paramsBegin(), type.paramsEnd());
    ss << ')';

    throw TypeError(function.begin(), ss.str());
  }
}

Type computeType(const ast::Expression& expr, const Environment& env);

void validateSubscripts(
  const ast::ArrayLookup& lookup,
  const ArrayType& type,
  const Environment& env)
{
  std::size_t numProvidedSubscripts = lookup.numSubscripts();
  if (type.rank() < numProvidedSubscripts) {
    std::ostringstream ss;
    ss << "Too many subscripts provided in array lookup.\n";
    ss << "Identifier \'" << lookup.id().name() << "\' declared as ";
    TypePrinter printer(ss);
    printer(type);
    ss << "; " << numProvidedSubscripts << " subscripts provided.";
    throw TypeError(lookup.id().begin(), ss.str());
  }

  auto subsBegin = lookup.subscriptsBegin();
  auto subsEnd = lookup.subscriptsEnd();
  for (auto iter = subsBegin; iter != subsEnd; ++iter) {
    auto subscriptType = computeType(*iter, env);

    bool correctType = true;
    switch (category(subscriptType)) {
    case TypeCategory::Deferred:
      break;
    case TypeCategory::Simple: {
      const auto& rawType = boost::get<SimpleType>(subscriptType);
      correctType = rawType.type() == PrimitiveType::Int;
    } break;
    default:
      correctType = false;
    }

    if (!correctType) {
      std::ostringstream ss;
      ss << "Invalid subscript expression. Expression evaluates as type ";
      boost::apply_visitor(TypePrinter(ss), subscriptType);
      ss << ", but expected int";
      throw TypeError(iter->begin(), ss.str());
    }
  }
}

Type computeType(
  const ast::ArrayLookup& lookup,
  const ArrayType& arrayType,
  const Environment& env)
{
  // Validate provided subscript expressions
  validateSubscripts(lookup, arrayType, env);
  // Compute type for remaining part of array
  std::size_t numProvidedSubscripts = lookup.numSubscripts();
  // All subscripts provided - result is the element type
  if (arrayType.rank() == numProvidedSubscripts)
    return SimpleType(arrayType.elemType(), arrayType.isConst());
  // Some subscripts ommitted - result is a subarray
  else {
    auto extentsBegin = arrayType.extentsBegin() + numProvidedSubscripts;
    auto extentsEnd = arrayType.extentsEnd();
    return ArrayType(
      arrayType.elemType(),
      arrayType.isConst(),
      std::vector<std::size_t>(extentsBegin, extentsEnd));
  }
}

class BasicExpressionVisitor : public boost::static_visitor<Type> {
  const Environment& m_env;

public:
  BasicExpressionVisitor(const Environment& env) : m_env(env) {}

  Type operator()(const ast::ArrayLookup& lookup) const {
    auto type = (*this)(lookup.id());
    assert(!isDeferred(type));

    if (!isArray(type)) {
      std::ostringstream ss;
      ss << "Identifier \'" << lookup.id().name() << "\' declared as ";
      boost::apply_visitor(TypePrinter(ss), type);
      ss << " can not be the subject of an array lookup.";
      throw TypeError(lookup.id().begin(), ss.str());
    }
    auto& arrayType = boost::get<ArrayType>(type);

    return computeType(lookup, arrayType, m_env);
  }
  Type operator()(const Identifier& id) const {
    auto type = m_env.lookupVariable(id);
    if (type != nullptr) {
      assert(!isDeferred(*type));
      return *type;
    } else {
      std::ostringstream ss;
      ss << "Identifier \'" << id.name() << "\' is undeclared.\n";
      throw TypeError(id.begin(), ss.str());
    }
  }
};

struct SimpleValueVisitor : boost::static_visitor<SimpleType> {
  SimpleType operator()(ast::bool_t) const {
    return SimpleType(PrimitiveType::Bool, true);
  }
  SimpleType operator()(ast::int_t) const {
    return SimpleType(PrimitiveType::Int, true);
  }
  SimpleType operator()(ast::float_t) const {
    return SimpleType(PrimitiveType::Float, true);
  }
  SimpleType operator()(ast::char_t) const {
    return SimpleType(PrimitiveType::Char, true);
  }
  SimpleType operator()(const ast::string_t&) const {
    return SimpleType(PrimitiveType::String, true);
  }
};

std::vector<Type> computeParamTypes(
  const ast::FuncCall& funcCall,
  const Environment& env) 
{
  std::vector<Type> paramTypes;

  auto paramsEnd = funcCall.paramsEnd();
  for (auto iter = funcCall.paramsBegin(); iter != paramsEnd; ++iter)
    paramTypes.emplace_back(computeType(*iter, env));

  return paramTypes;
}

Type computeType(const ast::FuncCall& funcCall, const Environment& env) {
  auto paramTypes = computeParamTypes(funcCall, env);
  auto range = env.lookupFunction(funcCall.funcId());
  // Not a user/library function
  if (range.first == range.second) {
    auto libFunc = env.lookupLibraryFunction(funcCall.funcId(), paramTypes);
    // Not a special library function; not a function at all.
    if (!libFunc) {
      std::ostringstream ss;
      ss << "Identifier \'" << funcCall.funcId().name() << "\' is not a function.";
      throw TypeError(funcCall.funcId().begin(), ss.str());
    }
    return libFunc->returnType();
  }
  // Name exists either as a user/library function - check overloads
  else {
    auto iter = std::find_if(range.first, range.second,
      [&paramTypes](const auto& type) -> bool {
      return type.second.paramsMatch(paramTypes);
    });
    // Function declared but usage doesn't fit any user/library overloads
    if (iter == range.second) {
      auto libFunc = env.lookupLibraryFunction(funcCall.funcId(), paramTypes);
      // Not a special library function.
      if (!libFunc) {
        std::ostringstream ss;
        ss << "No overload of function \'" << funcCall.funcId().name();
        ss << "\' matches the parameters: (";
        printFuncParams(ss, paramTypes.cbegin(), paramTypes.cend());
        ss << ")\nCandidates are:";
        for (iter = range.first; iter != range.second; ++iter) {
          ss << '\n' << iter->first.name() << '(';
          printFuncParams(ss, iter->second.paramsBegin(), iter->second.paramsEnd());
          ss << ')';
        }
        throw TypeError(funcCall.funcId().begin(), ss.str());
      }
      // Is a special library function.
      return libFunc->returnType();
    }
    return iter->second.returnType();
  }
}

class SimpleExpressionVisitor : public boost::static_visitor<Type> {
  const Environment& m_env;

public:
  SimpleExpressionVisitor(const Environment& env) : m_env(env) {}

  Type operator()(const ast::SimpleValue& value) const {
    return boost::apply_visitor(SimpleValueVisitor(), value);
  }
  Type operator()(const ast::BasicExpression& expr) const {
    return boost::apply_visitor(BasicExpressionVisitor(m_env), expr);
  }
  Type operator()(const ast::FuncCall& funcCall) const {
    return computeType(funcCall, m_env);
  }
};

Type computeType(const ast::IncrExpression& expr, const Environment& env) {
  auto exprType = boost::apply_visitor(BasicExpressionVisitor(env), expr.expr());
  if (isDeferred(exprType))
    return exprType;
  else if (isSimple(exprType)) {
    const auto& simpleType = boost::get<SimpleType>(exprType);
    if (simpleType.type() == PrimitiveType::String)
      throw TypeError(
        expr.begin(),
        "Expression evalutes to string, expected bool, int, float or char.");
    else if (simpleType.isConst())
      throw TypeError(expr.begin(), "Cannot assign to const variable.");
    // Make type const
    return SimpleType(simpleType.type(), true);
  } else {
    std::ostringstream ss;
    ss << "Expression evaluates to ";
    boost::apply_visitor(TypePrinter(ss), exprType);
    ss << ", expected bool, int, float or char.";
    throw TypeError(expr.begin(), ss.str());
  }
}

void throwNotAMemberFuncError(const Identifier& funcId, const Type& leftType) {
  std::ostringstream ss;
  ss << "Identifier \'" << funcId << "\' is not a member function of ";
  boost::apply_visitor(TypePrinter(ss), leftType);
  ss << '.';
  throw TypeError(funcId.begin(), ss.str());
}
void throwMemFuncParamMismatchError(
  const Identifier& funcId,
  const std::vector<Type>& paramTypes,
  const FunctionType& expected)
{
  std::ostringstream ss;
  ss << "Member function \'" << funcId.name();
  ss << "\' does not match the parameters:\n(";
  printFuncParams(ss, paramTypes.cbegin(), paramTypes.cend());
  ss << ")\nExpected:\n(";
  printFuncParams(ss, expected.paramsBegin(), expected.paramsEnd());
  ss << ')';
  throw TypeError(funcId.begin(), ss.str());
}

Type computeType(const ast::MemberExpression& expr, const Environment& env) {
  auto leftType = boost::apply_visitor(SimpleExpressionVisitor(env), expr.expr());
  auto& funcId = expr.funcCall().funcId();
  switch (category(leftType)) {
  case TypeCategory::Deferred:
    // Can't do anything useful if we don't know the type of the left hand operand
    // - even if it's really string or an array, we can't know that yet.
    return leftType;
  case TypeCategory::Void:
    throw TypeError(expr.begin(),
      "left hand operand of member expression evaluates to void, expected string or array type");
  case TypeCategory::Simple:
  {
    const auto& leftSimple = boost::get<SimpleType>(leftType);
    if (leftSimple.type() != PrimitiveType::String) {
      std::ostringstream ss;
      ss << "left hand operand of member expression evaluates to ";
      TypePrinter printer(ss);
      printer(leftSimple);
      ss << ", expected string or array type";
      throw TypeError(expr.begin(), ss.str());
    }
    auto memFuncType = env.lookupStringMemFunc(funcId);
    if (memFuncType == nullptr)
      throwNotAMemberFuncError(funcId, leftType);
    else {
      auto paramTypes = computeParamTypes(expr.funcCall(), env);
      if (!memFuncType->paramsMatch(paramTypes))
        throwMemFuncParamMismatchError(funcId, paramTypes, *memFuncType);
    }
    return memFuncType->returnType();
  } break;
  case TypeCategory::Array:
  {
    const auto& leftArray = boost::get<ArrayType>(leftType);
    auto memFuncType = env.lookupArrayMemFunc(leftArray, funcId);
    if (!memFuncType)
      throwNotAMemberFuncError(funcId, leftType);
    else {
      auto paramTypes = computeParamTypes(expr.funcCall(), env);
      if (!memFuncType->paramsMatch(paramTypes))
        throwMemFuncParamMismatchError(funcId, paramTypes, *memFuncType);
    }
    return memFuncType->returnType();
  } break;
  default:
    assert(false);
    return Void();
  }
}

class ExpressionVisitor : public boost::static_visitor<Type> {
  const Environment& m_env;

public:
  ExpressionVisitor(const Environment& env) : m_env(env) {}

  Type operator()(const ast::SimpleExpression& expr) const {
    return boost::apply_visitor(SimpleExpressionVisitor(m_env), expr);
  }
  Type operator()(const ast::MemberExpression& expr) const {
    return computeType(expr, m_env);
  }
  Type operator()(const ast::IncrExpression& expr) const {
    return computeType(expr, m_env);
  }
  Type operator()(const ast::CastExpression& expr) const {
    auto exprType = computeType(expr.expr(), m_env);
    switch (category(exprType)) {
    case TypeCategory::Deferred:
    case TypeCategory::Simple:
      return SimpleType(expr.castType(), true);
    default: {
      std::ostringstream ss;
      ss << "Invalid cast: ";
      TypePrinter printer(ss);
      boost::apply_visitor(printer, exprType);
      ss << " is not convertible to ";
      printer(expr.castType());
      throw TypeError(expr.begin(), ss.str());
    }}
  }
  Type operator()(const ast::UnaryOpExpression& expr) const {
    using ast::UnaryOp;

    auto exprType = computeType(expr.expr(), m_env);
    bool validType = true;
    switch (category(exprType)) {
    case TypeCategory::Deferred:
      return exprType;
    case TypeCategory::Simple: {
      const auto& simpleType = boost::get<SimpleType>(exprType);
      if (simpleType.type() == PrimitiveType::String)
        validType = false;
      else {
        switch (expr.op()) {
        case UnaryOp::Negate:
          switch (simpleType.type()) {
          case PrimitiveType::Int:
          case PrimitiveType::Float:
            break;
          default:
            validType = false;
          }
          break;
        case UnaryOp::Not:
          validType = simpleType.type() != PrimitiveType::Bool;
          break;
        case UnaryOp::BinNot:
          // Already checked for string earlier
          break;
        default:
          assert(false);
        }
        if (validType)
          return SimpleType(simpleType.type(), true);
      }
    } break;
    default:
      validType = false;
    }
    assert(!validType);
    std::ostringstream ss;
    ss << "Operator " << expr.op() << " is not applicable to type ";
    boost::apply_visitor(TypePrinter(ss), exprType);
    ss << ". Applicable types are ";
    switch (expr.op()) {
    case UnaryOp::Negate:
      ss << "int, float";
      break;
    case UnaryOp::Not:
      ss << "bool";
      break;
    case UnaryOp::BinNot:
      ss << "bool, int, float, char";
      break;
    default:
      assert(false);
    }
    ss << '.';
    throw TypeError(expr.begin(), ss.str());
  }
  
private:
  void printExpected(std::ostream& out, ast::BinaryOp op, bool left) const {
    using ast::BinaryOp;
    switch (op) {
    case BinaryOp::Less:
    case BinaryOp::LessEqual:
    case BinaryOp::Greater:
    case BinaryOp::GreaterEqual:
    case BinaryOp::Equal:
    case BinaryOp::NotEqual:
      out << "bool, int, float, char, string";
      break;
    case BinaryOp::Multiply:
    case BinaryOp::Divide:
    case BinaryOp::Modulus:
      out << "int, float";
      break;
    case BinaryOp::Minus:
      out << "int, float, char";
      break;
    case BinaryOp::Plus:
      out << "int, float, char, string";
      break;
    case BinaryOp::ShiftLeft:
    case BinaryOp::ShiftRight:
      if (left)
        out << "int, float, char";
      else
        out << "int";
      break;
    case BinaryOp::BinAnd:
    case BinaryOp::BinXor:
    case BinaryOp::BinOr:
      out << "bool, int, float, char";
      break;
    case BinaryOp::And:
    case BinaryOp::Or:
      out << "bool";
    }
  }
  bool checkType(const SimpleType& type, ast::BinaryOp op, bool left) const {
    using ast::BinaryOp;

    switch (op) {
    case BinaryOp::Less:
    case BinaryOp::LessEqual:
    case BinaryOp::Greater:
    case BinaryOp::GreaterEqual:
    case BinaryOp::Equal:
    case BinaryOp::NotEqual:
      return true;
    case BinaryOp::Multiply:
    case BinaryOp::Divide:
    case BinaryOp::Modulus:
      switch (type.type()) {
      case PrimitiveType::Int:
      case PrimitiveType::Float:
        return true;
      default:
        return false;
      }
    case BinaryOp::Minus:
      switch (type.type()) {
      case PrimitiveType::Int:
      case PrimitiveType::Float:
      case PrimitiveType::Char:
        return true;
      default:
        return false;
      }
    case BinaryOp::Plus:
      return type.type() != PrimitiveType::Bool;
    case BinaryOp::ShiftLeft:
    case BinaryOp::ShiftRight:
      if (left)
        switch (type.type()) {
        case PrimitiveType::Int:
        case PrimitiveType::Float:
        case PrimitiveType::Char:
          return true;
        default:
          return false;
        }
      else
        return type.type() == PrimitiveType::Int;
    case BinaryOp::BinAnd:
    case BinaryOp::BinXor:
    case BinaryOp::BinOr:
      return type.type() != PrimitiveType::String;
    case BinaryOp::And:
    case BinaryOp::Or:
      return type.type() == PrimitiveType::Bool;
    default:
      assert(false);
      return false;
    }
  }
  Type computeExprType(
    const ast::BinaryOpExpression& expr,
    const SimpleType& left,
    const SimpleType& right) const
  {
    using ast::BinaryOp;
    

    switch (expr.op()) {
    case BinaryOp::Less:
    case BinaryOp::LessEqual:
    case BinaryOp::Greater:
    case BinaryOp::GreaterEqual:
    case BinaryOp::Equal:
    case BinaryOp::NotEqual:
    case BinaryOp::BinAnd:
    case BinaryOp::BinXor:
    case BinaryOp::BinOr:
    case BinaryOp::And:
    case BinaryOp::Or:
      if (left.type() != right.type()) {
        std::ostringstream ss;
        ss << "Operator " << expr.op() << " expects operands of the same type.\n";
        ss << "left operand evaluates to type ";
        TypePrinter printer(ss);
        printer(left);
        ss << ", right operand evaluates to type ";
        printer(right);
        ss << '.';
        throw TypeError(expr.left().begin(), ss.str());
      }
      switch (expr.op()) {
      case BinaryOp::Less:
      case BinaryOp::LessEqual:
      case BinaryOp::Greater:
      case BinaryOp::GreaterEqual:
      case BinaryOp::Equal:
      case BinaryOp::NotEqual:
      case BinaryOp::And:
      case BinaryOp::Or:
        return SimpleType(PrimitiveType::Bool, true);
      case BinaryOp::BinAnd:
      case BinaryOp::BinXor:
      case BinaryOp::BinOr:
        return SimpleType(left.type(), true);
      default:
        assert(false);
        return Void();
      }
    case BinaryOp::Multiply:
    case BinaryOp::Divide:
    case BinaryOp::Modulus:
      if (left.type() == right.type())
        return SimpleType(left.type(), true);
      // One is float since the only valid options are int or float
      else
        return SimpleType(PrimitiveType::Float, true);
    case BinaryOp::Minus:
      // One or more operands is char
      if (left.type() == PrimitiveType::Char || right.type() == PrimitiveType::Char) {
        // legal only for char - int, char - char.
        if (left.type() != PrimitiveType::Char) {
          std::ostringstream ss;
          ss << "Operator " << expr.op() << " expects left operand to be of type char ";
          ss << "if at least one operand is of type char.\n";
          ss << "left operand evaluates to type ";
          TypePrinter printer(ss);
          printer(left);
          ss << '.';
          throw TypeError(expr.left().begin(), ss.str());
        } else {
          switch (right.type()) {
          case PrimitiveType::Int:
            // char - int = char
            return SimpleType(PrimitiveType::Char, true);
          case PrimitiveType::Char:
            // char - char = int
            return SimpleType(PrimitiveType::Int, true);
          default:
          {
            std::ostringstream ss;
            ss << "Operator " << expr.op() << " expects right operand to be of type char or int ";
            ss << "if the left operand is of type char.\n";
            ss << "right operand evaluates to type ";
            TypePrinter printer(ss);
            printer(right);
            ss << '.';
            throw TypeError(expr.left().begin(), ss.str());
          }
          }
        }
      }
      // Neither operand is char
      else if (left.type() == right.type())
        return SimpleType(left.type(), true);
      // One is float since the only remaining valid options are int or float
      else
        return SimpleType(PrimitiveType::Float, true);
    case BinaryOp::Plus:
      // Left operand is char
      if (left.type() == PrimitiveType::Char) {
        if (right.type() == PrimitiveType::Int)
          // char + int = char
          return SimpleType(PrimitiveType::Char, true);
        else {
          std::ostringstream ss;
          ss << "Operator " << expr.op() << " expects right operand to be of type int ";
          ss << "if the left operand is of type char.\n";
          ss << "right operand evaluates to type ";
          TypePrinter printer(ss);
          printer(right);
          ss << '.';
          throw TypeError(expr.left().begin(), ss.str());
        }
      }
      // Right operand is char
      else if (right.type() == PrimitiveType::Char) {
        if (left.type() == PrimitiveType::Int)
          // int + char = char
          return SimpleType(PrimitiveType::Char, true);
        else {
          std::ostringstream ss;
          ss << "Operator " << expr.op() << " expects left operand to be of type char ";
          ss << "if the right operand is of type char.\n";
          ss << "left operand evaluates to type ";
          TypePrinter printer(ss);
          printer(left);
          ss << '.';
          throw TypeError(expr.left().begin(), ss.str());
        }
      }
      // Neither operand is char
      else if (left.type() == right.type())
        return SimpleType(left.type(), true);
      // One operand is string while the other isn't.
      else if (left.type() == PrimitiveType::String || right.type() == PrimitiveType::String) {
        std::ostringstream ss;
        ss << "Operator " << expr.op() << " expects both operands to be of type string ";
        ss << "if either one is.\n";
        ss << "left operand evaluates to type ";
        TypePrinter printer(ss);
        printer(left);
        ss << ", right operand evaluates to type ";
        printer(right);
        ss << '.';
        throw TypeError(expr.left().begin(), ss.str());
      }
      // Types are either int or float, and they're different from each other.
      else {
        assert(left.type() == PrimitiveType::Int || left.type() == PrimitiveType::Float);
        assert(right.type() == PrimitiveType::Int || right.type() == PrimitiveType::Float);
        return SimpleType(PrimitiveType::Float, true);
      }
    case BinaryOp::ShiftLeft:
    case BinaryOp::ShiftRight:
      // Validation handled by checkType
      return SimpleType(left.type(), true);
    default:
      assert(false);
      return Void();
    }
  }
public:
  Type operator()(const ast::BinaryOpExpression& expr) const {
    auto leftType = computeType(expr.left(), m_env);
    auto rightType = computeType(expr.right(), m_env);
    bool leftDeferred = isDeferred(leftType);
    bool rightDeferred = isDeferred(rightType);

    // Both operands are deferred types
    if (leftDeferred && rightDeferred)
      return leftType;
    // Exactly one opeand is a deferred type
    else if (leftDeferred || rightDeferred) {
      const auto& deferredType  = leftDeferred ? leftType : rightType;
      const auto& specifiedType = leftDeferred ? rightType : leftType;

      if (!isSimple(specifiedType) ||
          !checkType(boost::get<SimpleType>(specifiedType), expr.op(), rightDeferred))
      {
        std::ostringstream ss;
        ss << "Binary expression with operator " << expr.op() << " has incompatible ";
        ss << (leftDeferred ? "right" : "left");
        ss << " operand evaluating to type ";
        boost::apply_visitor(TypePrinter(ss), specifiedType);
        ss << "; expected one of ";
        printExpected(ss, expr.op(), rightDeferred);
        ss << '.';
        throw TypeError(expr.left().begin(), ss.str());
      }
      return deferredType;
    } else {
      bool validTypes = true;
      if (!isSimple(leftType) || !isSimple(rightType))
        validTypes = false;
      else {
        assert(isSimple(leftType) && isSimple(rightType));
        const auto& simpleLeft = boost::get<SimpleType>(leftType);
        const auto& simpleRight = boost::get<SimpleType>(rightType);

        validTypes = checkType(simpleLeft, expr.op(), true);
        validTypes = validTypes && checkType(simpleRight, expr.op(), false);
        if (validTypes)
          return computeExprType(expr, simpleLeft, simpleRight);
      }

      assert(!validTypes);
      std::ostringstream ss;
      ss << "Binary expression with operator " << expr.op()
        << " has incompatible left and right operands.\n";
      ss << "left operand evaluates to type ";
      TypePrinter printer(ss);
      boost::apply_visitor(printer, leftType);
      ss << "; expected one of ";
      printExpected(ss, expr.op(), true);
      ss << ".\nright operand evaluates to type ";
      boost::apply_visitor(printer, rightType);
      ss << "; expected one of ";
      printExpected(ss, expr.op(), false);
      ss << '.';
      throw TypeError(expr.left().begin(), ss.str());
    }
  }
};

Type computeType(const ast::Expression& expr, const Environment& env) {
  return boost::apply_visitor(ExpressionVisitor(env), expr.expr());
}

Type validateVariableDeclaration(
  const ast::VariableDeclaration& varDecl,
  const Environment& env)
{
  auto type = SimpleType(varDecl.type(), varDecl.isConst());
  auto initializerType = computeType(varDecl.initializer(), env);

  bool valid = true;
  switch (category(initializerType)) {
  case TypeCategory::Deferred:
    break;
  case TypeCategory::Simple: {
    const auto& simpleInitType = boost::get<SimpleType>(initializerType);
    valid = type.type() == simpleInitType.type();
  } break;
  default:
    valid = false;
  }
  if (!valid) {
    std::ostringstream ss;
    ss << "Identifier \'" << varDecl.id() << "\' declared as type ";
    TypePrinter printer(ss);
    printer(type);
    ss << ", but initialized with an expression of type ";
    boost::apply_visitor(printer, initializerType);
    ss << '.';
    throw TypeError(varDecl.begin(), ss.str());
  }
  return type;
}

class BasicArrayInitializerVisitor : public boost::static_visitor<Type> {
  const Environment& m_env;
  const SourcePosition& m_pos;

public:
  BasicArrayInitializerVisitor(
    const Environment& env,
    const SourcePosition& pos)
    : m_env(env)
    , m_pos(pos)
  {}

private:
  template <typename T>
  std::vector<Type> computeInitTypes(const std::vector<T>& inits) const {
    std::vector<Type> initTypes;
    initTypes.reserve(inits.size());
    for (auto& init : inits)
      initTypes.emplace_back(computeType(init, m_env));
    return initTypes;
  }
  template <typename Iterator>
  static Iterator findNonDeferred(const Iterator& begin, const Iterator& end) {
    static_assert(
      std::is_same<std::iterator_traits<Iterator>::value_type, Type>::value,
      "findNonDeferred expects iterators that dereference to type Type");
    return std::find_if(begin, end, [](const Type& type) { return !isDeferred(type); });
  }
  template <typename Iterator>
  ArrayType computeInitType(
    const Iterator& nonDeferred,
    const Iterator& end,
    std::size_t initializerSize) const
  {
    static_assert(
      std::is_same<std::iterator_traits<Iterator>::value_type, Type>::value,
      "computeInitType expects iterators that dereference to type Type");
    assert(isArray(*nonDeferred));
    auto arrayType = boost::get<ArrayType>(*nonDeferred);
    bool allCompatible = std::all_of(std::next(nonDeferred), end,
      [&arrayType](const Type& type) -> bool {
      if (!isDeferred(type)) {
        assert(isArray(type));
        auto& arrayType2 = boost::get<ArrayType>(type);
        bool compatible = arrayType.compatible(arrayType2);
        if (compatible) {
          arrayType.makeConvertibleFrom(arrayType2);
          assert(arrayType2.convertibleTo(arrayType));
        }
        return compatible;
      } else
        return true;
    });
    if (!allCompatible) {
      throw TypeError(
        m_pos,
        "Array initializer contains expressions of incompatible array types");
    }
    std::size_t newRank = arrayType.rank() + 1;
    ArrayType result(arrayType.elemType(), true, newRank);
    result.setExtent(0, initializerSize);
    for (std::size_t index = 1; index < newRank; ++index)
      result.setExtent(index, arrayType.extent(index - 1));
    return result;
  }
public:
  Type operator()(const std::vector<ast::Expression>& inits) const {
    std::vector<Type> initTypes = computeInitTypes(inits);
    // Find the first expression, if it exists, that doesn't have a deferred type.
    auto nonDeferred = findNonDeferred(initTypes.begin(), initTypes.end());
    // All expressions have deferred type - whole initializer has deferred type.
    if (nonDeferred == initTypes.end())
      return DeferredType();
    auto nonDeferredCategory = category(*nonDeferred);
    bool allSameCategory = std::all_of(std::next(nonDeferred), initTypes.end(),
      [nonDeferredCategory](const Type& type) -> bool {
        return isDeferred(type) || category(type) == nonDeferredCategory;
      });
    if (!allSameCategory)
      throw TypeError(
        m_pos,
        "Array initializer contains expressions of both primitive and array types");
    switch (nonDeferredCategory) {
    case TypeCategory::Simple: {
      bool allEqual = std::all_of(std::next(nonDeferred), initTypes.end(),
        [&nonDeferred](const Type& type) -> bool {
          return equal(type, *nonDeferred);
        });
      if (!allEqual)
        throw TypeError(
          m_pos,
          "Array initializer contains expressions of different primitive types");
      auto& simpleType = boost::get<SimpleType>(*nonDeferred);
      return ArrayType(simpleType.type(), true, std::vector<std::size_t>{ inits.size() });
    } break;
    case TypeCategory::Array:
      return computeInitType(nonDeferred, initTypes.end(), inits.size());
    default:
      assert(false);
      return Void();
    }
  }
  Type operator()(const std::vector<ast::BasicArrayInitializer>& inits) const {
    auto initTypes = computeInitTypes(inits);
    // Find the first subinitializer, if it exists, that doesn't have a deferred type.
    auto nonDeferred = findNonDeferred(initTypes.begin(), initTypes.end());
    // All subinitializers have deferred type - whole initializer has deferred type.
    if (nonDeferred == initTypes.end())
      return DeferredType();

    return computeInitType(nonDeferred, initTypes.end(), inits.size());
  }
};

Type computeType(
  const ast::BasicArrayInitializer& initializer,
  const Environment& env)
{
  return boost::apply_visitor(
    BasicArrayInitializerVisitor(env, initializer.begin()),
    initializer.value());
}

void validateArrayInitializerType(
  const ArrayType& arrayType,
  const Identifier& arrayId,
  const SourcePosition& declPosition,
  const Type& initializerType)
{
  if (!isDeferred(initializerType)) {
    bool validType = false;
    if (isArray(initializerType)) {
      const auto& initType = boost::get<ArrayType>(initializerType);
      validType = initType.convertibleTo(arrayType);
    }
    if (!validType) {
      std::ostringstream ss;
      ss << "Identifier \'" << arrayId << "\' declared as type ";
      TypePrinter printer(ss);
      printer(arrayType);
      ss << ", but array initializer evaluates to type ";
      boost::apply_visitor(printer, initializerType);
      ss << '.';
      throw TypeError(declPosition, ss.str());
    }
  }
}

class ArrayInitializerVisitor : public boost::static_visitor<void> {
  const ArrayType& m_arrayType;
  const Identifier& m_arrayId;
  const SourcePosition& m_declPosition;
  const Environment& m_env;

public:
  ArrayInitializerVisitor(
    const ArrayType& arrayType,
    const Identifier& arrayId,
    const SourcePosition& declPosition,
    const Environment& env)
    : m_arrayType(arrayType)
    , m_arrayId(arrayId)
    , m_declPosition(declPosition)
    , m_env(env)
  {}

  void operator()(const ast::BasicArrayInitializer& initializer) const {
    auto initializerType = computeType(initializer, m_env);
    validateArrayInitializerType(m_arrayType, m_arrayId, m_declPosition, initializerType);
  }
  void operator()(const ast::FuncCall& funcCall) const {
    auto initializerType = computeType(funcCall, m_env);
    validateArrayInitializerType(m_arrayType, m_arrayId, m_declPosition, initializerType);
  }
};

void validateArrayInitializer(
  const ArrayType& arrayType,
  const Identifier& arrayId,
  const SourcePosition& declPosition,
  const ast::ArrayInitializer& initializer,
  const Environment& env)
{
  boost::apply_visitor(ArrayInitializerVisitor(arrayType, arrayId, declPosition, env), initializer);
}

void throwGlobalRedefinitionError(
  const Type& type, 
  const Identifier& id,
  const SourcePosition& declPosition,
  const Environment env)
{
  assert(isSimple(type) || isArray(type));
  std::ostringstream ss;
  ss << "Redefinition of global identifier \'" << id.name() << "\' as type ";
  TypePrinter printer(ss);
  boost::apply_visitor(printer, type);
  ss << ", previously declared as type ";

  assert(env.atRootScope());
  auto prevType = env.lookupVariable(id);
  assert(prevType != nullptr);
  boost::apply_visitor(printer, *prevType);
  ss << '.';
  throw TypeError(declPosition, ss.str());
}

class GlobalDeclarationVisitor : public boost::static_visitor<void> {
  Environment& m_env;

public:
  GlobalDeclarationVisitor(Environment& env) : m_env(env) {}

  void operator()(const ast::VariableDeclaration& varDecl) const {
    auto type = validateVariableDeclaration(varDecl, m_env);
    if (!m_env.addGlobal(varDecl.id(), type))
      throwGlobalRedefinitionError(type, varDecl.id(), varDecl.begin(), m_env);
  }

  void operator()(const ast::GlobalArrayDeclaration& arrayDecl) const {
    auto& declType = arrayDecl.type();
    auto type = computeArrayType(arrayDecl.type(), TypeSubscriptVisitor(m_env));
    
    validateArrayInitializer(type, arrayDecl.id(), arrayDecl.begin(), arrayDecl.initializer(), m_env);
    if (!m_env.addGlobal(arrayDecl.id(), type))
      throwGlobalRedefinitionError(type, arrayDecl.id(), arrayDecl.begin(), m_env);
  }
};

void validate(const ast::GlobalDeclaration& global, Environment& env) {
  boost::apply_visitor(GlobalDeclarationVisitor(env), global);
}

void throwLocalRedefinitionError(
  const Type& type,
  const Identifier& id,
  const SourcePosition& declPosition,
  const Environment env)
{
  assert(isSimple(type) || isArray(type));
  std::ostringstream ss;
  ss << "Redefinition of local identifier \'" << id.name() << "\' as type ";
  TypePrinter printer(ss);
  boost::apply_visitor(printer, type);
  ss << ", previously declared as type ";

  assert(!env.atRootScope());
  auto prevType = env.lookupVariable(id);
  assert(prevType != nullptr);
  boost::apply_visitor(printer, *prevType);
  ss << '.';
  throw TypeError(declPosition, ss.str());
}

class DeclarationStatementVisitor : public boost::static_visitor<void> {
  Environment& m_env;

public:
  DeclarationStatementVisitor(Environment& env) : m_env(env) {}

  void operator()(const ast::VariableDeclaration& varDecl) const {
    auto type = validateVariableDeclaration(varDecl, m_env);
    if (!m_env.addLocal(varDecl.id(), type))
      throwLocalRedefinitionError(type, varDecl.id(), varDecl.begin(), m_env);
  }
  void operator()(const ast::ArrayDeclaration& arrayDecl) const {
    auto& declType = arrayDecl.type();
    auto type = computeArrayType(arrayDecl.type(), TypeSubscriptVisitor(m_env));

    auto& initializer = arrayDecl.initializer();
    if (initializer)
      validateArrayInitializer(type, arrayDecl.id(), arrayDecl.begin(), *initializer, m_env);
    if (!m_env.addLocal(arrayDecl.id(), type))
      throwLocalRedefinitionError(type, arrayDecl.id(), arrayDecl.begin(), m_env);
  }
};
void validate(const ast::DeclarationStatement& decl, Environment& env) {
  boost::apply_visitor(DeclarationStatementVisitor(env), decl);
}

class ExpressionStatementVisitor : public boost::static_visitor<void> {
  const Environment& m_env;

public:
  ExpressionStatementVisitor(const Environment& env) : m_env(env) {}

private:
  void validateAssignExpr(
    const ast::AssignExpression& expr,
    const Type& left,
    const Type& right) const
  {
    using ast::AssignOp;

    assert(isSimple(left) || isArray(left));
    switch (expr.op()) {
    case AssignOp::Assign:
      if (!convertibleTo(right, left)) {
        std::ostringstream ss;
        ss << "Operator " << expr.op();
        ss << " expects its right operand to be convertible to its left operand.\n";
        ss << "left operand evaluates to type ";
        TypePrinter printer(ss);
        boost::apply_visitor(printer, left);
        ss << ", right operand evaluates to type ";
        boost::apply_visitor(printer, right);
        ss << '.';
        throw TypeError(expr.begin(), ss.str());
      }
      break;
    case AssignOp::Plus:
      if (isSimple(left)) {
        auto& simpleLeft = boost::get<SimpleType>(left);
        if (simpleLeft.type() == PrimitiveType::Bool) {
          std::ostringstream ss;
          ss << "Operator " << expr.op()
             << " expects its left operand to be of type "
                "int, float, char, string, or an array type.";
          throw TypeError(expr.begin(), ss.str());
        }
        if (isDeferred(right))
          return;
        else if (isSimple(right)) {
          auto& simpleRight = boost::get<SimpleType>(right);
          // Allow: int += int, float += int,float, char += int, string += char,string
          switch (simpleLeft.type()) {
          case PrimitiveType::Char:
            if (simpleRight.type() != PrimitiveType::Int) {
              std::ostringstream ss;
              ss << "Operator " << expr.op()
                 << " expects its right operand to be of type int if its left operand "
                    "is of type char.\nright operand evaluates to type ";
              TypePrinter printer(ss);
              printer(simpleRight);
              ss << '.';
              throw TypeError(expr.begin(), ss.str());
            }
            break;
          case PrimitiveType::String:
            if (simpleRight.type() != PrimitiveType::String &&
                simpleRight.type() != PrimitiveType::Char)
            {
              std::ostringstream ss;
              ss << "Operator " << expr.op()
                 << " expects its right operand to be of type string or char if its left "
                    "operand is of type string.\nright operand evaluates to type ";
              TypePrinter printer(ss);
              printer(simpleRight);
              ss << '.';
              throw TypeError(expr.begin(), ss.str());
            }
            break;
          case PrimitiveType::Int:
          case PrimitiveType::Float:
            if (!simpleRight.convertibleTo(simpleLeft)) {
              std::ostringstream ss;
              ss << "Operator " << expr.op()
                 << " expects its right operand to be convertible to its left operand if "
                    "its left operand is of type int or float.\n";
              ss << "left operand evaluates to type ";
              TypePrinter printer(ss);
              printer(simpleLeft);
              ss << ", right operand evaluates to type ";
              printer(simpleRight);
              ss << '.';
              throw TypeError(expr.begin(), ss.str());
            }
            break;
          default:
            assert(false);
          }
        }
        else {
          assert(isArray(right));
          std::ostringstream ss;
          ss << "Operator " << expr.op()
             << " does not accept array types as its right operand if "
             << " its left operand is not an array type.\n"
             << "left operand evaluates to type ";
          TypePrinter printer(ss);
          printer(simpleLeft);
          ss << ", right operand evaluates to type ";
          boost::apply_visitor(printer, right);
          ss << '.';
          throw TypeError(expr.begin(), ss.str());
        }
      }
      else {
        assert(isArray(left));
        auto& arrayLeft = boost::get<ArrayType>(left);
        if (isSimple(right)) {
          auto& simpleRight = boost::get<SimpleType>(right);
          if (arrayLeft.rank() != 1 || arrayLeft.elemType() != simpleRight.type()) {
            std::ostringstream ss;
            ss << "Cannot append an object of type ";
            TypePrinter printer(ss);
            printer(simpleRight);
            ss << " to an array of type ";
            printer(arrayLeft);
            ss << '.';
            throw TypeError(expr.begin(), ss.str());
          }
          if (arrayLeft.extent(0) != 0) {
            std::ostringstream ss;
            ss << "Operator " << expr.op() << " cannot be applied to a fixed-sized array.";
            ss << "left operand evaluates to type ";
            TypePrinter printer(ss);
            printer(arrayLeft);
            ss << ", right operand evaluates to type ";
            printer(simpleRight);
            ss << '.';
            throw TypeError(expr.begin(), ss.str());
          }
        } 
        else if (isArray(right)) {
          auto& arrayRight = boost::get<ArrayType>(right);
          if (!arrayLeft.canAppend(arrayRight)) {
            std::ostringstream ss;
            ss << "Operator " << expr.op() << " cannot append an array of type ";
            TypePrinter printer(ss);
            printer(arrayRight);
            ss << " to an array of type ";
            printer(arrayLeft);
            ss << '.';
            throw TypeError(expr.begin(), ss.str());
          }
        }
        else
          assert(isDeferred(right));
      }
      break;
    case AssignOp::Minus: {
      bool arrays = false;
      if (isSimple(left)) {
        if (isSimple(right)) {
          auto& simpleLeft = boost::get<SimpleType>(left);
          auto& simpleRight = boost::get<SimpleType>(right);
          // Allow: int -= int, float -= int, float, char -= int.
          switch (simpleLeft.type()) {
          case PrimitiveType::Int:
          case PrimitiveType::Float:
            if (!simpleRight.convertibleTo(simpleLeft)) {
              std::ostringstream ss;
              ss << "Operator " << expr.op()
                 << " expects its right operand to be convertible to its left operand if "
                    "its left operand is of type int or float.\n";
              ss << "left operand evaluates to type ";
              TypePrinter printer(ss);
              printer(simpleLeft);
              ss << ", right operand evaluates to type ";
              printer(simpleRight);
              ss << '.';
              throw TypeError(expr.begin(), ss.str());
            }
            break;
          case PrimitiveType::Char:
            if (simpleRight.type() != PrimitiveType::Int) {
              std::ostringstream ss;
              ss << "Operator " << expr.op()
                 << " expects its right operand to be of type int if its left operand "
                    "is of type char.\nright operand evaluates to type ";
              TypePrinter printer(ss);
              printer(simpleRight);
              ss << '.';
              throw TypeError(expr.begin(), ss.str());
            }
          }
        }
        else if (isArray(right))
          arrays = true;
        else
          assert(isDeferred(right));
      } 
      else
        arrays = true;
      if (arrays) {
        std::ostringstream ss;
        ss << "Operator " << expr.op() << " does not accept arrays for either operand.\n";
        ss << "left operand evaluates to type ";
        TypePrinter printer(ss);
        boost::apply_visitor(printer, left);
        ss << ", right operand evaluates to type ";
        boost::apply_visitor(printer, right);
        ss << '.';
        throw TypeError(expr.begin(), ss.str());
      }
    } break;
    case AssignOp::Multiply:
    case AssignOp::Divide:
    case AssignOp::Modulus: {
      bool arrays = false;
      // Allow: int *= int, float *= int, float.
      if (isSimple(left)) {
        auto& simpleLeft = boost::get<SimpleType>(left);
        switch (simpleLeft.type()) {
        case PrimitiveType::Int:
        case PrimitiveType::Float:
          break;
        default: {
          std::ostringstream ss;
          ss << "Operator " << expr.op() << " expects its left operand to be of type int or float.\n";
          ss << "left operand evaluates to type ";
          TypePrinter printer(ss);
          printer(simpleLeft);
          ss << '.';
          throw TypeError(expr.begin(), ss.str());
        }}
        if (isDeferred(right))
          return;
        else if (isSimple(right)) {
          auto& simpleRight = boost::get<SimpleType>(right);
          if (!simpleRight.convertibleTo(simpleLeft)) {
            std::ostringstream ss;
            ss << "Operator " << expr.op() << " expects its right operand to be convertible to its left operand.\n";
            ss << "left operand evaluates to type ";
            TypePrinter printer(ss);
            printer(simpleLeft);
            ss << ", right operand evaluates to type ";
            printer(simpleRight);
            ss << '.';
            throw TypeError(expr.begin(), ss.str());
          }
          return;
        }
        else {
          assert(isArray(right));
          arrays = true;
        }
      } 
      else
        arrays = true;
      if (arrays) {
        std::ostringstream ss;
        ss << "Operator " << expr.op() << " does not accept arrays for either operand.\n";
        ss << "left operand evaluates to type ";
        TypePrinter printer(ss);
        boost::apply_visitor(printer, left);
        ss << ", right operand evaluates to type ";
        boost::apply_visitor(printer, right);
        ss << '.';
        throw TypeError(expr.begin(), ss.str());
      }
    } break;
    case AssignOp::ShiftLeft:
    case AssignOp::ShiftRight: {
      bool arrays = false;
      // Allow: bool, int, float, char <<= int
      if (isSimple(left)) {
        auto& simpleLeft = boost::get<SimpleType>(left);
        if (simpleLeft.type() == PrimitiveType::String) {
          std::ostringstream ss;
          ss << "Operator " << expr.op() << " does not accept strings for either operand.\n";
          ss << "left operand evaluates to type string, right operand evaluates to type ";
          TypePrinter printer(ss);
          boost::apply_visitor(printer, right);
          ss << '.';
          throw TypeError(expr.begin(), ss.str());
        }
        if (isDeferred(right))
          return;
        else if (isSimple(right)) {
          auto& simpleRight = boost::get<SimpleType>(left);
          if (simpleRight.type() != PrimitiveType::Int) {
            std::ostringstream ss;
            ss << "Operator " << expr.op() << " only accepts ints for its right operand.\n";
            ss << "right operand evaluates to type ";
            TypePrinter printer(ss);
            printer(simpleRight);
            ss << '.';
            throw TypeError(expr.begin(), ss.str());
          }
          return;
        } else {
          assert(isArray(right));
          arrays = true;
        }
      }
      else
        arrays = true;
      if (arrays) {
        std::ostringstream ss;
        ss << "Operator " << expr.op() << " does not accept arrays for either operand.\n";
        ss << "left operand evaluates to type ";
        TypePrinter printer(ss);
        boost::apply_visitor(printer, left);
        ss << ", right operand evaluates to type ";
        boost::apply_visitor(printer, right);
        ss << '.';
        throw TypeError(expr.begin(), ss.str());
      }
    } break;
    case AssignOp::BinAnd:
    case AssignOp::BinXor:
    case AssignOp::BinOr: {
      bool arrays = false;
      // Allow x &= x for x in {bool, int, float, char}
      if (isSimple(left)) {
        auto& simpleLeft = boost::get<SimpleType>(left);
        if (simpleLeft.type() == PrimitiveType::String) {
          std::ostringstream ss;
          ss << "Operator " << expr.op() << " does not accept strings for either operand.\n";
          ss << "left operand evaluates to type string, right operand evaluates to type ";
          TypePrinter printer(ss);
          boost::apply_visitor(printer, right);
          ss << '.';
          throw TypeError(expr.begin(), ss.str());
        }
        if (isDeferred(right))
          return;
        else if (isSimple(right)) {
          auto& simpleRight = boost::get<SimpleType>(right);
          if (simpleLeft.type() != simpleRight.type()) {
            std::ostringstream ss;
            ss << "Operator " << expr.op() << " expects operands of the same type.\n";
            ss << "left operand evaluates to type ";
            TypePrinter printer(ss);
            printer(simpleLeft);
            ss << ", right operand evaluates to type ";
            printer(simpleRight);
            ss << '.';
            throw TypeError(expr.begin(), ss.str());
          }
        }
        else {
          assert(isArray(right));
          arrays = true;
        }
      }
      else
        arrays = true;
      if (arrays) {
        std::ostringstream ss;
        ss << "Operator " << expr.op() << " does not accept arrays for either operand.\n";
        ss << "left operand evaluates to type ";
        TypePrinter printer(ss);
        boost::apply_visitor(printer, left);
        ss << ", right operand evaluates to type ";
        boost::apply_visitor(printer, right);
        ss << '.';
        throw TypeError(expr.begin(), ss.str());
      }
    } break;
    default:
      assert(false);
    }
  }
public:
  void operator()(const ast::AssignExpression& expr) const {
    auto leftType = boost::apply_visitor(BasicExpressionVisitor(m_env), expr.left());
    assert(!isDeferred(leftType) && !isVoid(leftType));

    bool isConst = false;
    // Check left is not const
    switch (category(leftType)) {
    case TypeCategory::Simple: {
      const auto& simpleLeft = boost::get<SimpleType>(leftType);
      isConst = simpleLeft.isConst();
    } break;
    case TypeCategory::Array: {
      const auto& arrayLeft = boost::get<ArrayType>(leftType);
      isConst = arrayLeft.isConst();
    } break;
    default:
      assert(false);
    }
    if (isConst)
      throw TypeError(expr.begin(), "Cannot assign to const variable.");

    auto rightType = computeType(expr.right(), m_env);
    validateAssignExpr(expr, leftType, rightType);
  }
  void operator()(const ast::IncrExpression& expr) const {
    // Throw away return type - we only care that the expression is valid
    computeType(expr, m_env);
  }
  void operator()(const ast::FuncCall& funcCall) const {
    // Throw away return type - we only care that the expression is valid
    computeType(funcCall, m_env);
  }
};
void validate(const ast::ExpressionStatement& statement, const Environment& env) {
  boost::apply_visitor(ExpressionStatementVisitor(env), statement.statement());
}

void validateCondition(const ast::Expression& condition, const Environment& env) {
  auto conditionType = computeType(condition, env);
  bool isBool = false;
  switch (category(conditionType)) {
  case TypeCategory::Deferred:
    return;
  case TypeCategory::Simple: {
    const auto& simpleType = boost::get<SimpleType>(conditionType);
    if (simpleType.type() == PrimitiveType::Bool)
      isBool = true;
  } break;
  default:
    break;
  }

  if (!isBool) {
    std::ostringstream ss;
    ss << "Condition expression evaluates to type ";
    boost::apply_visitor(TypePrinter(ss), conditionType);
    ss << ", expected bool.";
    throw TypeError(condition.begin(), ss.str());
  }
}

class ForInitVisitor : public boost::static_visitor<void> {
  Environment& m_env;

public:
  ForInitVisitor(Environment& env) : m_env(env) {}

  void operator()(const ast::VariableDeclaration& varDecl) const {
    auto type = validateVariableDeclaration(varDecl, m_env);
    if (!m_env.addLocal(varDecl.id(), type))
      throwLocalRedefinitionError(type, varDecl.id(), varDecl.begin(), m_env);
  }
  void operator()(const ast::ExpressionStatement& expression) const {
    validate(expression, const_cast<const Environment&>(m_env));
  }
};
void validate(const ast::ForInitStatement& init, Environment& env) {
  boost::apply_visitor(ForInitVisitor(env), init);
}

void validateReturnStatement(
  const FunctionType& funcType,
  const Identifier& funcId,
  const ast::ReturnStatement& returnStatement,
  const Environment& env)
{
  auto& expectedType = funcType.returnType();
  assert(!isDeferred(expectedType));
  if (isVoid(expectedType)) {
    if (returnStatement.hasExpression()) {
      std::ostringstream ss;
      ss << "Unexpected return value; return value of " << funcId.name() << " is void.";
      throw TypeError(returnStatement.begin(), ss.str());
    }
  } else {
    if (returnStatement.hasExpression()) {
      auto returnType = computeType(returnStatement.expression(), env);

      if (!convertibleTo(returnType, expectedType)) {
        std::ostringstream ss;
        ss << "Invalid return type; return value of " << funcId.name() << " is ";
        TypePrinter printer(ss);
        boost::apply_visitor(printer, expectedType);
        ss << ",\nreturn expression evaluates to type ";
        boost::apply_visitor(printer, returnType);
        ss << '.';
        throw TypeError(returnStatement.begin(), ss.str());
      }
    } else {
      std::ostringstream ss;
      ss << "Expected return value; return value of " << funcId.name() << " is ";
      boost::apply_visitor(TypePrinter(ss), expectedType);
      ss << '.';
      throw TypeError(returnStatement.begin(), ss.str());
    }
  }
}

void validateRangeExpression(const ast::ForeachLoop& foreachLoop, Environment& env) {
  // Validate elem/range expressions
  auto& elemDecl = foreachLoop.elemDeclaration();
  auto elemType = SimpleType(elemDecl.type(), elemDecl.isConst());

  auto& rangeExpr = foreachLoop.rangeExpression();
  auto rangeType = computeType(rangeExpr, env);

  bool constRange = false;
  switch (category(rangeType)) {
  case TypeCategory::Deferred:
    break;
  case TypeCategory::Void:
    throw TypeError(
      rangeExpr.begin(),
      "Range expression evaluates to type void, expected string or array type.");
  case TypeCategory::Simple: {
    auto& simpleType = boost::get<SimpleType>(rangeType);
    if (simpleType.type() != PrimitiveType::String) {
      std::ostringstream ss;
      ss << "Range expression evaluates to type ";
      TypePrinter printer(ss);
      printer(simpleType);
      ss << ", expected string or array type.";
      throw TypeError(rangeExpr.begin(), ss.str());
    }
    if (elemType.type() != PrimitiveType::Char) {
      std::ostringstream ss;
      ss << "Element declaration must be of type char; range expression evaluates to type string.\n";
      ss << "Element declaration is of type ";
      TypePrinter printer(ss);
      printer(elemType);
      ss << '.';
      throw TypeError(rangeExpr.begin(), ss.str());
    }
    constRange = simpleType.isConst();
  } break;
  case TypeCategory::Array: {
    auto& arrayType = boost::get<ArrayType>(rangeType);
    if (arrayType.rank() != 1) {
      std::ostringstream ss;
      ss << "Only one-dimensional arrays can be used as range expressions.\n";
      ss << "Range expression evaluates to type ";
      TypePrinter printer(ss);
      printer(arrayType);
      ss << '.';
      throw TypeError(rangeExpr.begin(), ss.str());
    }
    constRange = arrayType.isConst();
    if (elemType.type() != arrayType.elemType()) {
      std::ostringstream ss;
      ss << "Element declaration must be of type ";
      if (constRange)
        ss << "const ";
      TypePrinter printer(ss);
      printer(arrayType.elemType());
      ss << "; range expression evaluates to type ";
      printer(arrayType);
      ss << '.';
      throw TypeError(elemDecl.begin(), ss.str());
    }
  } break;
  default:
    assert(false);
  }
  if (constRange && !elemType.isConst())
    throw TypeError(
      elemDecl.begin(),
      "Element declaration must be const since range expression is const.");

  if (!env.addLocal(elemDecl.id(), elemType))
    assert(false);
}

class StatementVisitor;

template <typename Iterator>
void validateStatements(const StatementVisitor& visitor, Iterator begin, const Iterator& end);

class StatementVisitor : public boost::static_visitor<void> {
  const FunctionType& m_funcType;
  const Identifier& m_funcId;
  Environment& m_env;

public:
  StatementVisitor(const FunctionType& funcType, const Identifier& funcId, Environment& env)
    : m_funcType(funcType)
    , m_funcId(funcId)
    , m_env(env)
  {}

  // SimpleStatement

  void operator()(const ast::DeclarationStatement& declaration) const {
    validate(declaration, m_env);
  }
  void operator()(const ast::ExpressionStatement& expression) const {
    validate(expression, const_cast<const Environment&>(m_env));
  }
  void operator()(ast::LoopControl) const {
    // Nothing here to typecheck.
  }
  void operator()(const ast::ReturnStatement& returnStatement) const {
    validateReturnStatement(m_funcType, m_funcId, returnStatement, m_env);
  }

  // Statement

  void operator()(const ast::SimpleStatement& statement) const {
    boost::apply_visitor(*this, statement);
  }
  void operator()(const ast::IfStatement& ifStatement) const {
    validateCondition(ifStatement.condition(), m_env);
    m_env.scopePush();
    validateStatements(*this, ifStatement.trueStatementsBegin(), ifStatement.trueStatementsEnd());
    // true/false blocks are separate scopes
    m_env.scopePop();
    m_env.scopePush();
    validateStatements(*this, ifStatement.falseStatementsBegin(), ifStatement.falseStatementsEnd());
    m_env.scopePop();
  }
  void operator()(const ast::WhileLoop& whileLoop) const {
    validateCondition(whileLoop.condition(), m_env);
    m_env.scopePush();
    validateStatements(*this, whileLoop.statementsBegin(), whileLoop.statementsEnd());
    m_env.scopePop();
  }
  void operator()(const ast::ForLoop& forLoop) const {
    m_env.scopePush();
    if (forLoop.hasInitStatement())
      validate(forLoop.initStatement(), m_env);
    if (forLoop.hasStepCondition())
      validateCondition(forLoop.stepCondition(), m_env);
    if (forLoop.hasStepStatement())
      (*this)(forLoop.stepStatement());
    validateStatements(*this, forLoop.statementsBegin(), forLoop.statementsEnd());
    m_env.scopePop();
  }
  void operator()(const ast::ForeachLoop& foreachLoop) const {
    m_env.scopePush();
    validateRangeExpression(foreachLoop, m_env);
    validateStatements(*this, foreachLoop.statementsBegin(), foreachLoop.statementsEnd());
    m_env.scopePop();
  }
};

template <typename Iterator>
void validateStatements(const StatementVisitor& visitor, Iterator begin, const Iterator& end) {
  static_assert(
    std::is_same<std::iterator_traits<Iterator>::value_type, ast::Statement>::value,
    "validateStatements expects iterators which dereference to type ast::Statement");
  for (; begin != end; ++begin)
    boost::apply_visitor(visitor, begin->statement());
}

void validateFuncBody(const ast::Function& function, Environment& env) {
  // Easier to recompute than to look up function and match parameters
  const auto funcType = computeFunctionType(function);

  env.scopePush();
  // Add function parameters to scope
  auto typeIter = funcType.paramsBegin();
  auto typeEnd  = funcType.paramsEnd();
  auto idIter   = function.paramsBegin();
  assert(std::distance(typeIter, typeEnd) == std::distance(idIter, function.paramsEnd()));

  for (; typeIter != typeEnd; ++typeIter, ++idIter) {
    // Make parameters non-const within function (pass-by-value)
    auto paramType = *typeIter;
    assert(isSimple(paramType) || isArray(paramType));
    if (isSimple(paramType)) {
      auto& simpleType = boost::get<SimpleType>(paramType);
      simpleType.setConst(false);
    } else {
      auto& arrayType = boost::get<ArrayType>(paramType);
      arrayType.setConst(false);
    }
    if (!env.addLocal(idIter->id(), paramType))
      throwLocalRedefinitionError(paramType, idIter->id(), idIter->begin(), env);
  }

  StatementVisitor visitor(funcType, function.id(), env);
  validateStatements(visitor, function.statementsBegin(), function.statementsEnd());
  env.scopePop();
}

bool checkTypes(const ast::ProgramStructure& structure,
                ErrorDiagnostic& diagnostic)
{
  try {
    Environment env;

    // Start by checking function types - don't need to check contents immediately.
    auto funcBegin = structure.funcBegin();
    auto funcEnd = structure.funcEnd();
    bool emptyProgram = funcBegin == funcEnd;
    for (auto iter = funcBegin; iter != funcEnd; ++iter)
      validate(*iter, env);

    // Next check global variables
    auto globalBegin = structure.globalsBegin();
    auto globalEnd   = structure.globalsEnd();
    emptyProgram = emptyProgram && globalBegin == globalEnd;
    for (auto iter = globalBegin; iter != globalEnd; ++iter)
      validate(*iter, env);

    using namespace literals;
    // Verify main() is declared if program is not empty
    if (!emptyProgram) {
      auto mainType = env.lookupFunction("main"_id, std::vector<Type>());
      if (mainType == nullptr)
        throw TypeError(SourcePosition(1, 1), "main() not declared");
    }

    // Finally check function contents
    for (auto iter = funcBegin; iter != funcEnd; ++iter)
      validateFuncBody(*iter, env);

    assert(env.atRootScope());

    return true;
  } catch (const TypeError& e) {
    diagnostic.push(e.position(), e.what());
    return false;
  }
}

}
}
