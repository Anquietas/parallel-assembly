#include "SimpleCGrammar.h"

#ifdef _MSC_VER
#pragma warning(push)
// Redefinitions - internal problem with spirit, compiles anyway.
#pragma warning(disable:4348)
// Declaration hides outer declaration
#pragma warning(disable:4459)
// Unreferenced formal parameter
#pragma warning(disable:4100)
// Decorated name length exceeded
#pragma warning(disable:4503)
#endif 

#include <boost/spirit/include/qi_symbols.hpp>
#include <boost/spirit/include/qi_auxiliary.hpp>

#include <boost/spirit/include/phoenix.hpp>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include "Annotator.h"
#include "ErrorHandlers.h"
#include "ParserHelperFunctors.h"
#include "SpiritProgramStructureHandler.h"
#include "ExpressionGrammar.h"
#include "Annotator.h"
#include "ErrorHandlers.h"
#include "ParserBuildConfig.h"

// Decorated name length exceeded
#ifdef _MSC_VER
#pragma warning(disable:4503)
#endif

namespace simplec {
namespace parser {
namespace detail {

namespace ascii = boost::spirit::ascii;

template <typename Iterator>
SimpleCGrammar<Iterator>::SimpleCGrammar(ErrorDiagnostic& diagnostic)
  : base_type(program, "program")
{
  using AssignSymbols = qi::symbols<char, ast::AssignOp>;

  static ExpressionGrammar<Iterator>                     expression(diagnostic);

  phx::function<Annotator<Iterator>> annotator;
  phx::function<DefaultErrorHandler<Iterator>> defaultErrorHandler;
  phx::function<StatementErrorHandler<Iterator>> statementErrorHandler;
  phx::function<ProgramErrorHandler<Iterator>> programErrorHandler;

  static AssignSymbols assignOp;       // = += -= *= /= %= <<= >>= &= ^= |=

  static Skipper<Iterator> skipper;

  static rule<AssignOrIncr()>                            assignOrIncr;
  static rule<SubscriptOrFuncCall()>                     subscriptOrFuncCall;
  static rule<ast::ExpressionStatement()>                assignExpression;

  // Statement rules
  static rule<ast::OptTypeSubscript()>                   optTypeSubscript;
  static rule<ast::TypeSubscript()>                      typeSubscript;

  static rule<ast::BasicArrayInitializer::variant_t()>   arrayInitBraceListContent;
  static rule<ast::BasicArrayInitializer()>              arrayInitBraceList;
  static rule<ast::FuncCall()>                           arrayInitFuncCall;
  static rule<ast::ArrayInitializer()>                   arrayInit;
  static rule<VarInitializer()>                          varInitializer;
  static rule<bool()>                                    optionalConst;
  static rule<DeclarationStmt()>                         varDeclareHelper;
  static rule<ast::DeclarationStatement()>               varDeclare;

  static rule<ast::Expression()>                         condition;
  static rule<std::vector<ast::Statement>()>             optElseBlock;
  static rule<ast::IfStatement()>                        ifStatement;
  // If statement rules that can be used inside loops
  static rule<std::vector<ast::Statement>()>             loopOptElseBlock;
  static rule<ast::IfStatement()>                        loopIfStatement;
  static rule<ast::WhileLoop>                            whileLoop;
  static rule<ast::ForInitStatement()>                   forInit;
  static rule<ast::ForLoop()>                            forLoop;
  static rule<ForeachElemDecl()>                         foreachElemDeclHelper;
  static rule<ast::ForeachElemDeclaration()>             foreachElemDecl;
  static rule<ast::ForeachLoop()>                        foreachLoop;

  static rule<ast::LoopControl()>                        loopControlStatement;
  static rule<ast::ReturnStatement()>                    returnStatement;
  static rule<ast::SimpleStatement()>                    simpleStatement;

  static rule<ast::Statement()>                          statement;
  // statement but also with loop control statements
  static rule<ast::Statement()>                          loopStatement;

  static rule<std::vector<ast::Statement>()>             statementGroup;
  // A variant of statementGroup enforcing the single-statement case
  // has a space before it (required for else blocks).
  static rule<std::vector<ast::Statement>()>             statementGroup2;
  // statementGroup but also with loop control statements
  static rule<std::vector<ast::Statement>()>             loopStatementGroup;
  // statementGroup2 but also with loop control statements
  static rule<std::vector<ast::Statement>()>             loopStatementGroup2;

  static rule<ast::Parameter()>                          param;
  static rule<std::vector<ast::Parameter>>               paramList;
  static rule<FuncParamAndBody()>                        funcDeclare;
  static rule<GlobalVarInitializer()>                    globalVarInitializer;
  static rule<VarInitOrFuncDecl()>                       varInitOrFuncDecl;
  static rule<BasicGlobalStatement()>                    basicGlobalStatement;
  static rule<GlobalStatement()>                         globalStatement;

  // Set symbols
  {
    using namespace simplec::ast;
    assignOp.add
      ("=", AssignOp::Assign)
      ("+=", AssignOp::Plus)
      ("-=", AssignOp::Minus)
      ("*=", AssignOp::Multiply)
      ("/=", AssignOp::Divide)
      ("%=", AssignOp::Modulus)
      ("<<=", AssignOp::ShiftLeft)
      (">>=", AssignOp::ShiftRight)
      ("&=", AssignOp::BinAnd)
      ("^=", AssignOp::BinXor)
      ("|=", AssignOp::BinOr);
  }

  // Set rules
  {
    auto& primitiveType = expression.primitiveType;
    auto& postIncrOp = expression.postIncrOp;
    auto& identifier = expression.identifier;
    auto& intLiteral = expression.intLiteral;
    auto& subscript = expression.subscript;
    auto& funcParams = expression.funcParams;
    auto& preIncrExpression = expression.preIncrExpression;

    assignOrIncr %= assignOp > expression | postIncrOp;
    subscriptOrFuncCall %= *subscript >> assignOrIncr
                         | funcParams;
    assignExpression = (identifier > subscriptOrFuncCall)
                        [qi::_val = phx::bind(AssignExpressionHelper(), qi::_1, qi::_2)]
                     | preIncrExpression[qi::_val = qi::_1];
    
    qi::uint_parser<std::size_t> litSubscript;
    optTypeSubscript %= '[' > -(litSubscript | identifier) > ']';
    typeSubscript    %= '[' > (litSubscript | identifier) > ']';

    arrayInitBraceListContent %= expression % ','
                               | arrayInitBraceList % ',';
    arrayInitBraceList %= '{' > arrayInitBraceListContent > '}';
    arrayInitFuncCall = (identifier > funcParams)
      [qi::_val = phx::construct<ast::FuncCall>(qi::_1, qi::_2)];
    arrayInit %= arrayInitBraceList
               | '=' > arrayInitFuncCall;
    varInitializer %= '=' > expression
                    | +optTypeSubscript > -arrayInit;
    optionalConst %= qi::no_skip[-skipper >> "const" > skipper] > qi::attr(true)
                   | qi::attr(false);
    varDeclareHelper %= qi::no_skip[-skipper >> primitiveType > skipper] > identifier > varInitializer;
    varDeclare = (optionalConst >> varDeclareHelper)
      [qi::_val = phx::bind(VarDeclareHelper(), qi::_1, qi::_2)];

    condition %= expression;
    optElseBlock %= -("else" > statementGroup2);
    ifStatement = (qi::lit("if") > '(' > condition > ')' > statementGroup > optElseBlock)
      [qi::_val = phx::construct<ast::IfStatement>(qi::_1, qi::_2, qi::_3)];

    loopOptElseBlock %= -("else" > loopStatementGroup2);
    loopIfStatement = (qi::lit("if") > '(' > condition > ')' > loopStatementGroup > loopOptElseBlock)
      [qi::_val = phx::construct<ast::IfStatement>(qi::_1, qi::_2, qi::_3)];

    whileLoop = (qi::lit("while") > '(' > condition > ')' > loopStatementGroup)
      [qi::_val = phx::construct<ast::WhileLoop>(qi::_1, qi::_2)];

    forInit = (qi::no_skip[-skipper >> primitiveType > skipper] > identifier > '=' > expression)
                [qi::_val = phx::construct<ast::VariableDeclaration>(qi::_1, false, qi::_2, qi::_3)]
            | assignExpression  [qi::_val = qi::_1];
    forLoop = (qi::lit("for") > '(' > -forInit
            > ';' > -condition
            > ';' > -assignExpression > ')'
            > loopStatementGroup)
      [qi::_val = phx::construct<ast::ForLoop>(qi::_1, qi::_2, qi::_3, qi::_4)];

    foreachElemDeclHelper %= qi::no_skip[-skipper >> primitiveType > skipper] > identifier;
    foreachElemDecl = (optionalConst >> foreachElemDeclHelper)
      [qi::_val = phx::bind(ForeachElemDeclHelper(), qi::_1, qi::_2)];
    foreachLoop = (qi::lit("foreach") > '('
                > foreachElemDecl
                > ':' > expression > ')'
                > loopStatementGroup)
      [qi::_val = phx::construct<ast::ForeachLoop>(qi::_1, qi::_2, qi::_3)];

    loopControlStatement %= qi::lit("break") > ';' > qi::attr(ast::LoopControl::Break)
                          | qi::lit("continue") > ';' > qi::attr(ast::LoopControl::Continue);
    // There must be a space between return and the expression if there is one
    returnStatement %= "return" > -qi::no_skip[skipper >> qi::skip[expression]] > ';';
    simpleStatement %= varDeclare > ';'
                     | returnStatement
                     | assignExpression > ';';

    statement %= ifStatement
               | whileLoop
               | foreachLoop
               | forLoop
               | loopControlStatement > qi::eps [qi::_pass = false]
               | simpleStatement;
    loopStatement %= loopIfStatement
                   | whileLoop
                   | foreachLoop
                   | forLoop
                   | loopControlStatement [qi::_val = phx::construct<ast::SimpleStatement>(qi::_1)]
                   | simpleStatement;
    // Tested
    statementGroup %= ';'
                    | statement
                    | '{' > *statement > '}';
    statementGroup2 %= ';'
                     | qi::no_skip[skipper > qi::skip[statement]]
                     | '{' > *statement > '}';
    loopStatementGroup %= ';'
                        | loopStatement
                        | '{' > *loopStatement > '}';
    loopStatementGroup2 %= ';'
                         | qi::no_skip[skipper > qi::skip[loopStatement]]
                         | '{' > *loopStatement > '}';

    param = (primitiveType > identifier > *optTypeSubscript)
      [qi::_val = phx::bind(ParamHelper(), qi::_1, qi::_2, qi::_3)];
    paramList %= -(param % ',');
    funcDeclare %= '(' > paramList > ')' > '{' > *statement > '}';
    globalVarInitializer %= '=' > expression
                          | +typeSubscript > arrayInitBraceList;
    varInitOrFuncDecl %= globalVarInitializer > ';'
                       | funcDeclare;
    basicGlobalStatement %= qi::no_skip[skipper > qi::skip[identifier]] > varInitOrFuncDecl
                          | +optTypeSubscript > identifier > funcDeclare;
    // Force spaces between types, const etc
    globalStatement = (qi::no_skip[-skipper >> primitiveType] > basicGlobalStatement)
                      [qi::_val = phx::bind(GlobalStatementHelper(), qi::_1, qi::_2)]
                    | (qi::no_skip[-skipper >> "const" > skipper > primitiveType > skipper] > identifier > globalVarInitializer > ';')
                      [qi::_val = phx::bind(GlobalStatementHelper(), qi::_1, qi::_2, qi::_3)]
                    | (qi::no_skip[-skipper >> "void" > skipper] > identifier > funcDeclare)
                      [qi::_val = phx::bind(GlobalStatementHelper(), qi::_1, qi::_2)];
    // Program is one or more statements, or it's completely empty
    // (except comments and whitespace)
    program %= *globalStatement > qi::eoi;
  }

  // Set rule names
#ifdef USE_DEBUG_RULE_NAMES
  {
    assignOp.name("assignOp");

    // Statements
    optTypeSubscript.name("optTypeSubscript");
    typeSubscript.name("typeSubscript");

    assignOrIncr.name("assignOrIncr");
    subscriptOrFuncCall.name("subscriptOrFuncCall");
    assignExpression.name("assignExpression");

    arrayInitBraceList.name("arrayInitBraceList");
    arrayInitBraceListContent.name("arrayInitBraceListContent");
    arrayInitFuncCall.name("arrayInitFuncCall");
    arrayInit.name("arrayInit");
    varInitializer.name("varInitializer");
    optionalConst.name("optionalConst");
    varDeclareHelper.name("varDeclareHelper");
    varDeclare.name("varDeclare");

    loopControlStatement.name("loopControlStatement");
    returnStatement.name("returnStatement");
    simpleStatement.name("simpleStatement");

    condition.name("condition");
    optElseBlock.name("optElseBlock");
    ifStatement.name("ifStatement");
    loopOptElseBlock.name("loopOptElseBlock");
    loopIfStatement.name("loopIfStatement");
    whileLoop.name("whileLoop");
    forInit.name("forInit");
    forLoop.name("forLoop");
    foreachElemDeclHelper.name("foreachElemDeclHelper");
    foreachElemDecl.name("foreachElemDecl");
    foreachLoop.name("foreachLoop");

    statement.name("statement");
    loopStatement.name("loopStatement");
    statementGroup.name("statementGroup");
    statementGroup2.name("statementGroup2");
    loopStatementGroup.name("loopStatementGroup");
    loopStatementGroup2.name("loopStatementGroup2");

    param.name("param");
    paramList.name("paramList");
    funcDeclare.name("funcDeclare");
    globalVarInitializer.name("globalVarInitializer");
    varInitOrFuncDecl.name("varInitOrFuncDecl");
    basicGlobalStatement.name("basicGlobalStatement");
    globalStatement.name("globalStatement");
    program.name("program");
  }
#else // !defined(USE_DEBUG_RULE_NAMES)
  {
    assignOp.name("assignment operator (=, +=, -=, *=, /=, %= <<=, >>=, &=, ^=, |=");

    optTypeSubscript.name("type subscript");
    typeSubscript.name("type subscript (required arg)");

    // Statements
    assignOrIncr.name("assignment or post-increment or decrement");
    assignExpression.name("assignment or post-increment or decrement, or function call");

    arrayInitBraceList.name("array brace initializer");
    arrayInitBraceListContent.name(
      "a comma-separated list of expressions or " + arrayInitBraceList.name());
    arrayInitFuncCall.name("assignment from a function call returning an array");
    arrayInit.name("array initializer");
    varInitializer.name("variable initializer, or " + optTypeSubscript.name()
      + "s possibly followed by an array initializer");
    optionalConst.name("const specifier (optional)");
    varDeclare.name("variable or array declaration");

    loopControlStatement.name("loop control statement (break, continue)");
    returnStatement.name("return statement");

    condition.name("condition expression");
    optElseBlock.name("optional else block");
    ifStatement.name("if statement");
    loopOptElseBlock.name(optElseBlock.name());
    loopIfStatement.name(ifStatement.name());
    whileLoop.name("while loop");
    forInit.name("for loop initializer");
    forLoop.name("for loop");
    foreachElemDecl.name("foreach loop element declaration");
    foreachLoop.name("foreach loop");

    statement.name("statement");
    loopStatement.name(statement.name());
    statementGroup.name("a \';\', a single statement, or a sequence of statements enclosed in braces");
    statementGroup2.name(statementGroup.name());
    loopStatementGroup.name(statementGroup.name());
    loopStatementGroup2.name(statementGroup2.name());

    param.name("parameter specification");
    paramList.name("comma-separated list of parameter specifications");
    funcDeclare.name("function parameter list and body");
    globalVarInitializer.name("variable initializer, or subscripts followed by an array initializer");
    varInitOrFuncDecl.name(globalVarInitializer.name()
      + ", or a " + funcDeclare.name());
    basicGlobalStatement.name("global variable or function declaration");
    globalStatement.name("global variable or function declaration");
    program.name("program");
  }
#endif // USE_DEBUG_RULE_NAMES

  // Set debug
#ifdef ENABLE_RULE_DEBUG
  {
    // Statements
    qi::debug(optTypeSubscript);
    qi::debug(typeSubscript);

    qi::debug(assignOrIncr);
    qi::debug(subscriptOrFuncCall);
    qi::debug(assignExpression);

    qi::debug(arrayInitBraceListContent);
    qi::debug(arrayInitBraceList);
    qi::debug(arrayInitFuncCall);
    qi::debug(arrayInit);
    qi::debug(varInitializer);
    qi::debug(optionalConst);
    qi::debug(varDeclareHelper);
    qi::debug(varDeclare);

    qi::debug(loopControlStatement);
    qi::debug(returnStatement);
    qi::debug(simpleStatement);

    qi::debug(condition);
    qi::debug(optElseBlock);
    qi::debug(ifStatement);
    qi::debug(loopOptElseBlock);
    qi::debug(loopIfStatement);
    qi::debug(whileLoop);
    qi::debug(forInit);
    qi::debug(forLoop);
    qi::debug(foreachElemDeclHelper);
    qi::debug(foreachElemDecl);
    qi::debug(foreachLoop);

    qi::debug(statement);
    qi::debug(loopStatement);
    qi::debug(statementGroup);
    qi::debug(statementGroup2);
    qi::debug(loopStatementGroup);
    qi::debug(loopStatementGroup2);

    qi::debug(param);
    qi::debug(paramList);
    qi::debug(funcDeclare);
    qi::debug(globalVarInitializer);
    qi::debug(varInitOrFuncDecl);
    qi::debug(basicGlobalStatement);
    qi::debug(globalStatement);
    qi::debug(program);
  }
#endif // ENABLE_RULE_DEBUG

  // Set success handlers
  {
    auto annotate = annotator(qi::_val, qi::_1, qi::_2, qi::_3);

    qi::on_success(forInit, annotate);
    qi::on_success(assignExpression, annotate);
    qi::on_success(arrayInitBraceList, annotate);
    qi::on_success(varDeclare, annotate);
    qi::on_success(foreachElemDecl, annotate);
    qi::on_success(returnStatement, annotate);
    qi::on_success(statement, annotate);
    qi::on_success(param, annotate); 
    qi::on_success(globalStatement, annotate);
  }

  // Set error handlers
  {
    auto defaultError = defaultErrorHandler(&diagnostic, false, qi::_1, qi::_2, qi::_3, qi::_4);
    auto defaultErrorFinal = defaultErrorHandler(&diagnostic, true, qi::_1, qi::_2, qi::_3, qi::_4);

    qi::on_error<qi::fail>(optTypeSubscript, defaultError);
    qi::on_error<qi::fail>(typeSubscript, defaultError);

    qi::on_error<qi::fail>(assignOrIncr, defaultError);
    qi::on_error<qi::fail>(subscriptOrFuncCall, defaultError);
    qi::on_error<qi::fail>(assignExpression, defaultError);

    qi::on_error<qi::fail>(arrayInitBraceListContent, defaultError);
    qi::on_error<qi::fail>(arrayInitBraceList, defaultError);
    qi::on_error<qi::fail>(arrayInitFuncCall, defaultError);
    qi::on_error<qi::fail>(arrayInit, defaultError);
    qi::on_error<qi::fail>(varInitializer, defaultError);
    qi::on_error<qi::fail>(varDeclareHelper, defaultError);
    qi::on_error<qi::fail>(varDeclare, defaultError);

    qi::on_error<qi::fail>(optElseBlock, defaultErrorFinal);
    qi::on_error<qi::fail>(ifStatement, defaultErrorFinal);
    qi::on_error<qi::fail>(loopOptElseBlock, defaultErrorFinal);
    qi::on_error<qi::fail>(loopIfStatement, defaultErrorFinal);
    qi::on_error<qi::fail>(whileLoop, defaultErrorFinal);
    qi::on_error<qi::fail>(forInit, defaultError);
    qi::on_error<qi::fail>(forLoop, defaultErrorFinal);
    qi::on_error<qi::fail>(foreachElemDeclHelper, defaultError);
    qi::on_error<qi::fail>(foreachElemDecl, defaultError);
    qi::on_error<qi::fail>(foreachLoop, defaultErrorFinal);

    qi::on_error<qi::fail>(loopControlStatement, defaultError);
    qi::on_error<qi::fail>(returnStatement, defaultError);
    qi::on_error<qi::fail>(simpleStatement, defaultError);

    qi::on_error<qi::fail>(statement, statementErrorHandler(
      &diagnostic, true, qi::_1, qi::_2, qi::_3, qi::_4));
    qi::on_error<qi::fail>(loopStatement, defaultErrorFinal);
    qi::on_error<qi::fail>(statementGroup, defaultErrorFinal);
    qi::on_error<qi::fail>(statementGroup2, defaultErrorFinal);
    qi::on_error<qi::fail>(loopStatementGroup, defaultErrorFinal);
    qi::on_error<qi::fail>(loopStatementGroup2, defaultErrorFinal);

    qi::on_error<qi::fail>(param, defaultError);
    qi::on_error<qi::fail>(paramList, defaultError);
    qi::on_error<qi::fail>(funcDeclare, defaultErrorFinal);
    qi::on_error<qi::fail>(globalVarInitializer, defaultError);
    qi::on_error<qi::fail>(varInitOrFuncDecl, defaultError);
    qi::on_error<qi::fail>(basicGlobalStatement, defaultError);
    qi::on_error<qi::fail>(globalStatement, defaultError);

    qi::on_error<qi::fail>(program, programErrorHandler(
      &diagnostic, true, phx::cref(globalStatement.name()),
      qi::_1, qi::_2, qi::_3, qi::_4));
  }
}

template <typename Iterator>
using LinePos = utility::LinePosIterator<Iterator>;

extern template Skipper<LinePos<const char*>>;
extern template ExpressionGrammar<LinePos<const char*>>;

template class SimpleCGrammar<LinePos<const char*>>;

}
}
}
