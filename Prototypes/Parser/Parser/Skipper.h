#pragma once
#ifndef SIMPLEC_PARSER_SKIPPER_H
#define SIMPLEC_PARSER_SKIPPER_H

#define BOOST_SPIRIT_USE_PHOENIX_V3 1

#ifdef _MSC_VER
#pragma warning(push)
// Redefinitions - internal problem with spirit, compiles anyway.
#pragma warning(disable:4348)
// Declaration hides outer declaration
#pragma warning(disable:4459)
// Unreferenced formal parameter
#pragma warning(disable:4100)
#endif 

#include <boost/spirit/include/qi_core.hpp>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

namespace simplec {
namespace parser {
namespace detail {

namespace qi {
using namespace boost::spirit::qi;
}
namespace phx {
using namespace boost::phoenix;
}

template <typename Iterator>
class Skipper : public qi::grammar<Iterator> {
  using rule = qi::rule<Iterator>;

  rule commentEnd;
  rule comment;
  rule start;
public:
  Skipper();
  Skipper(const Skipper&) = delete;
  Skipper(Skipper&&) = delete;

  Skipper& operator=(const Skipper&) = delete;
  Skipper& operator=(Skipper&&) = delete;
};

}
}
}

#endif // SIMPLEC_PARSER_SKIPPER_H
