#pragma once
#ifndef SIMPLEC_TYPECHECKER
#define SIMPLEC_TYPECHECKER

#include <map>
#include <set>
#include <utility>
#include <string>
#include <stdexcept>
#include <iostream>

#include "ProgramStructure.h"
#include "ErrorDiagnostic.h"
#include "TypeCheckerTypes.h"
#include "TypeCheckerEnvironment.h"

namespace simplec {
namespace typecheck {

using parser::ErrorDiagnostic;

class TypePrinter : public boost::static_visitor<void> {
  std::ostream& m_out;
public:
  TypePrinter(std::ostream& out);

  void operator()(PrimitiveType type) const;
  void operator()(const Void&) const;
  void operator()(const DeferredType&) const;
  void operator()(const SimpleType&) const;
  void operator()(const ArrayType&) const;
};

class TypeError : public std::runtime_error {
  SourcePosition m_position;

public:
  TypeError(const SourcePosition& position,
            const char* message)
    : std::runtime_error(message)
    , m_position(position)
  {}
  TypeError(const SourcePosition& position,
            std::string message)
    : std::runtime_error(std::move(message))
    , m_position(position)
  {}

  const SourcePosition& position() const { return m_position; }
};

template <bool optSubscripts, typename SubscriptVisitor>
ArrayType computeArrayType(
  const ast::ArrayType<optSubscripts>& arrayType,
  const SubscriptVisitor& visitor)
{
  std::size_t numSubscripts = arrayType.numSubscripts();
  auto result = ArrayType(arrayType.elemType(), arrayType.isConst(), numSubscripts);

  for (std::size_t i = 0; i < numSubscripts; ++i)
    result.setExtent(i, visitor(arrayType.subscript(i)));
  return result;
}

// Also validates subscripts
Type computeType(
  const ast::ArrayLookup& lookup,
  const ArrayType& arrayType,
  const Environment& env);
Type computeType(const ast::FuncCall& funcCall, const Environment& env);
Type computeType(const ast::IncrExpression& expr, const Environment& env);
Type computeType(const ast::Expression& expr, const Environment& env);
Type computeType(const ast::BasicArrayInitializer& initializer, const Environment& env);

void validateArrayInitializer(
  const ArrayType& arrayType,
  const Identifier& arrayId,
  const SourcePosition& declPosition,
  const ast::ArrayInitializer& initializer,
  const Environment& env);
void validate(const ast::GlobalDeclaration& global, Environment& env);
void validate(const ast::DeclarationStatement& decl, Environment& env);
void validate(const ast::ExpressionStatement& statement, const Environment& env);
void validateReturnStatement(
  const FunctionType& funcType,
  const Identifier& funcId,
  const ast::ReturnStatement& returnStatement,
  const Environment& env);
void validateCondition(const ast::Expression& condition, const Environment& env);
void validate(const ast::ForInitStatement& init, Environment& env);
void validateRangeExpression(const ast::ForeachLoop& foreachLoop, Environment& env);

bool checkTypes(const ast::ProgramStructure& structure,
                ErrorDiagnostic& diagnostic);

}
}

#endif // SIMPLEC_TYPECHECKER
