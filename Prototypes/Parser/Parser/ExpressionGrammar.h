#pragma once
#ifndef SIMPLEC_PARSER_EXPRESSION_GRAMMAR_H
#define SIMPLEC_PARSER_EXPRESSION_GRAMMAR_H

#define BOOST_SPIRIT_USE_PHOENIX_V3 1

#ifdef _MSC_VER
#pragma warning(push)
// Redefinitions - internal problem with spirit, compiles anyway.
#pragma warning(disable:4348)
// Declaration hides outer declaration
#pragma warning(disable:4459)
// Unreferenced formal parameter
#pragma warning(disable:4100)
#endif 

#include <boost/spirit/include/qi_core.hpp>
#include <boost/spirit/include/qi_symbols.hpp>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include "SimpleC_Core.h"
#include "AST_Expressions.h"
#include "Skipper.h"
#include "ParserInternalTypes.h"
#include "ErrorDiagnostic.h"

#define MAKE_NON_COPY_MOVEABLE(T) \
  T (const T &) = delete; \
  T ( T &&) = delete; \
  T& operator=(const T &) = delete; \
  T& operator=( T &&) = delete

namespace simplec {
namespace parser {
namespace detail {

namespace qi {
using namespace boost::spirit::qi;
}
namespace phx {
using namespace boost::phoenix;
}

template <typename Iterator> class SimpleCGrammar;

template <typename Iterator>
class ExpressionGrammar : public qi::grammar<Iterator, Skipper<Iterator>, ast::Expression()> {
  using base_type = qi::grammar<Iterator, Skipper<Iterator>, ast::Expression()>;
  template <typename T = qi::unused_type, typename skip = Skipper<Iterator>>
  using rule = qi::rule<Iterator, skip, T>;

  using TypeSymbols = qi::symbols<char, ast::PrimitiveType>;
  using IncrSymbols = qi::symbols<char, ast::IncrOp>;

  friend class SimpleCGrammar<Iterator>;

  // Accessed by SimpleCGrammar<Iterator>
  TypeSymbols primitiveType;    // bool int float char string
  IncrSymbols postIncrOp;       // x++ x--
  rule<ast::Identifier()>                         identifier;
  rule<ast::int_t()>                              intLiteral;
  rule<ast::Expression()>                         subscript;
  rule<ExprList()>                                funcParams;
  rule<ast::IncrExpression()>                     preIncrExpression;

  // Required for base class
  rule<ast::Expression()>                         expression;
protected:
  ExpressionGrammar(ErrorDiagnostic& diagnostic);
  MAKE_NON_COPY_MOVEABLE(ExpressionGrammar);
};

}
}
}

#undef MAKE_NON_COPY_MOVEABLE

#endif // SIMPLEC_PARSER_EXPRESSION_GRAMMAR_H
