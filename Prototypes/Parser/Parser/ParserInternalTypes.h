#pragma once
#ifndef SIMPLEC_PARSER_INTERNAL_TYPES_H
#define SIMPLEC_PARSER_INTERNAL_TYPES_H

#include <vector>
#include <boost/optional.hpp>
#include <boost/variant.hpp>
#include <boost/fusion/container.hpp>

#include "SimpleC_Core.h"
#include "AST_Expressions.h"
#include "AST_Statements.h"

namespace simplec {
namespace parser {
namespace detail {

namespace fus = boost::fusion;

using ExprList = std::vector<ast::Expression>;
using SubscriptList = std::vector<ast::Expression>;

using IncrOrMemFuncCall = boost::variant<
  ast::IncrOp,
  ast::FuncCall>;

using SubscriptsIncrOrMemFunc = fus::vector2<
  ExprList,
  boost::optional<IncrOrMemFuncCall>>;

using ParamsOrSubscriptsMemIncr = boost::variant<
  ExprList,
  SubscriptsIncrOrMemFunc>;

using VarOrArrayLookup = fus::vector2<
  ast::Identifier,
  SubscriptList>;

using AssignRHS = fus::vector2<
  ast::AssignOp,
  ast::Expression>;
using AssignOrIncr = boost::variant<
  AssignRHS,
  ast::IncrOp>;

using SubscriptAssignIncr = fus::vector2<
  SubscriptList,
  AssignOrIncr>;
using SubscriptOrFuncCall = boost::variant<
  SubscriptAssignIncr,
  ExprList>;

using ArraySubscriptsInitializer = fus::vector2<
  std::vector<ast::OptTypeSubscript>,
  ast::ArrayDeclaration::Initializer>;
using VarInitializer = boost::variant<
  ast::Expression,
  ArraySubscriptsInitializer>;
using DeclarationStmt = fus::vector3<
  ast::PrimitiveType,
  ast::Identifier,
  VarInitializer>;

using ForeachElemDecl = fus::vector2<
  ast::PrimitiveType,
  ast::Identifier>;

using FuncBody = std::vector<ast::Statement>;
using FuncParamAndBody = fus::vector2<
  std::vector<ast::Parameter>,
  FuncBody>;

using GlobalArrayInitializer = fus::vector2<
  std::vector<ast::TypeSubscript>,
  ast::BasicArrayInitializer>;
using GlobalVarInitializer = boost::variant<
  ast::Expression,
  GlobalArrayInitializer>;
using VarInitOrFuncDecl = boost::variant<
  GlobalVarInitializer,
  FuncParamAndBody>;

using IdVarInitOrFuncDecl = fus::vector2<
  ast::Identifier,
  VarInitOrFuncDecl>;
using ArrayReturnFuncDecl = fus::vector<
  std::vector<ast::OptTypeSubscript>,
  ast::Identifier,
  FuncParamAndBody>;
using BasicGlobalStatement = boost::variant<
  IdVarInitOrFuncDecl,
  ArrayReturnFuncDecl>;

using GlobalStatement = boost::variant<
  ast::GlobalDeclaration,
  ast::Function,
  ast::Comment>;

}
}
}

#endif // SIMPLEC_PARSER_INTERNAL_TYPES_H
