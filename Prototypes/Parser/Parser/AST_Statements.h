#pragma once
#ifndef SIMPLEC_AST_STATEMENTS_H
#define SIMPLEC_AST_STATEMENTS_H

#include <iostream>
#include <vector>
#include <boost/variant.hpp>
#include <boost/optional.hpp>

#include "SourceLocationInfo.h"
#include "SimpleC_Core.h"
#include "AST_Expressions.h"

namespace simplec {
namespace ast {

class VariableDeclaration final : public SourceLocationInfo {
  PrimitiveType m_type;
  bool m_const;
  Identifier m_id;
  Expression m_initializer;
public:
  VariableDeclaration() = default;
  VariableDeclaration(PrimitiveType type, bool isConst, Identifier id, Expression initializer);
  VariableDeclaration(const VariableDeclaration& o) = default;
  VariableDeclaration(VariableDeclaration&& o) = default;
  ~VariableDeclaration() = default;

  VariableDeclaration& operator=(const VariableDeclaration& o) = default;
  VariableDeclaration& operator=(VariableDeclaration&& o) = default;

  PrimitiveType type() const noexcept { return m_type; }
  bool isConst() const noexcept { return m_const; }
  const Identifier& id() const noexcept { return m_id; }
  const Expression& initializer() const noexcept { return m_initializer; }
};
std::ostream& operator<<(std::ostream& out, const VariableDeclaration& varDecl);

class BasicArrayInitializer final : public SourceLocationInfo {
public:
  using variant_t = boost::variant<
    std::vector<Expression>,
    std::vector<BasicArrayInitializer>>;
private:
  variant_t m_value;
public:
  BasicArrayInitializer() = default;
  explicit BasicArrayInitializer(variant_t value);
  BasicArrayInitializer(const BasicArrayInitializer& o) = default;
  BasicArrayInitializer(BasicArrayInitializer&& o) = default;
  ~BasicArrayInitializer() = default;

  BasicArrayInitializer& operator=(const BasicArrayInitializer& o) = default;
  BasicArrayInitializer& operator=(BasicArrayInitializer&& o) = default;

  const variant_t& value() const noexcept { return m_value; }
};
std::ostream& operator<<(std::ostream& out, const BasicArrayInitializer& arrayInit);

using ArrayInitializer = boost::variant<FuncCall, BasicArrayInitializer>;

class ArrayDeclaration final : public SourceLocationInfo {
public:
  using Initializer = boost::optional<ArrayInitializer>;
private:
  ArrayType<true> m_type;
  Identifier m_id;
  Initializer m_initializer;
public:
  ArrayDeclaration() = default;
  ArrayDeclaration(ArrayType<true> type, Identifier id, Initializer initializer);
  ArrayDeclaration(const ArrayDeclaration& o) = default;
  ArrayDeclaration(ArrayDeclaration&& o) = default;
  ~ArrayDeclaration() = default;

  ArrayDeclaration& operator=(const ArrayDeclaration& o) = default;
  ArrayDeclaration& operator=(ArrayDeclaration&& o) = default;

  const ArrayType<true>& type() const noexcept { return m_type; }
  const Identifier& id() const noexcept { return m_id; }
  const Initializer& initializer() const noexcept { return m_initializer; }
};
std::ostream& operator<<(std::ostream& out, const ArrayDeclaration& arrayDecl);

using DeclarationStatement = boost::variant<VariableDeclaration, ArrayDeclaration>;

class GlobalArrayDeclaration final : public SourceLocationInfo {
  ArrayType<false> m_type;
  Identifier m_id;
  BasicArrayInitializer m_initializer;
public:
  GlobalArrayDeclaration() = default;
  GlobalArrayDeclaration(
    ArrayType<false> type,
    Identifier id, 
    BasicArrayInitializer initializer);
  GlobalArrayDeclaration(const GlobalArrayDeclaration& o) = default;
  GlobalArrayDeclaration(GlobalArrayDeclaration&& o) = default;
  ~GlobalArrayDeclaration() = default;

  GlobalArrayDeclaration& operator=(const GlobalArrayDeclaration& o) = default;
  GlobalArrayDeclaration& operator=(GlobalArrayDeclaration&& o) = default;

  const ArrayType<false>& type() const noexcept { return m_type; }
  const Identifier& id() const noexcept { return m_id; }
  const BasicArrayInitializer& initializer() const noexcept { return m_initializer; }
};
std::ostream& operator<<(std::ostream& out, const GlobalArrayDeclaration& arrayDecl);

using GlobalDeclaration = boost::variant<VariableDeclaration, GlobalArrayDeclaration>;

class ExpressionStatement final : public SourceLocationInfo {
public:
  using variant_t = boost::variant<AssignExpression, IncrExpression, FuncCall>;
private:
  variant_t m_statement;
public:
  ExpressionStatement() = default;
  explicit ExpressionStatement(AssignExpression statement);
  explicit ExpressionStatement(IncrExpression statement);
  explicit ExpressionStatement(FuncCall statement);
  ExpressionStatement(const ExpressionStatement& o) = default;
  ExpressionStatement(ExpressionStatement&& o) = default;
  ~ExpressionStatement() = default;

  ExpressionStatement& operator=(const ExpressionStatement& o) = default;
  ExpressionStatement& operator=(ExpressionStatement&& o) = default;
  ExpressionStatement& operator=(AssignExpression statement);
  ExpressionStatement& operator=(IncrExpression statement);
  ExpressionStatement& operator=(FuncCall statement);

  const variant_t& statement() const noexcept { return m_statement; }

  virtual void setEndpoints(const SourcePosition& begin, const SourcePosition& end) override;
  virtual void resetEndpoints() noexcept override;
};
std::ostream& operator<<(std::ostream& out, const ExpressionStatement& statement);

enum class LoopControl {
  Break,
  Continue,
};
std::ostream& operator<<(std::ostream& out, const LoopControl& loopCtrl);

class ReturnStatement final : public SourceLocationInfo {
  boost::optional<Expression> m_returnExpr;
public:
  ReturnStatement() = default;
  ReturnStatement(boost::optional<Expression> returnExpr);
  ReturnStatement(const ReturnStatement& o) = default;
  ReturnStatement(ReturnStatement&& o) = default;
  ~ReturnStatement() = default;

  ReturnStatement& operator=(const ReturnStatement& o) = default;
  ReturnStatement& operator=(ReturnStatement&& o) = default;

  bool hasExpression() const noexcept { return static_cast<bool>(m_returnExpr); }
  const Expression& expression() const { return *m_returnExpr; }
};
std::ostream& operator<<(std::ostream& out, const ReturnStatement& returnStmt);

class Statement;

class IfStatement final {
  Expression m_condition;
  std::vector<Statement> m_trueStatements;
  std::vector<Statement> m_falseStatements;
public:
  IfStatement() = default;
  IfStatement(Expression condition,
              std::vector<Statement> trueStatements,
              std::vector<Statement> falseStatements);
  IfStatement(const IfStatement& o) = default;
  IfStatement(IfStatement&& o) = default;
  ~IfStatement() = default;

  IfStatement& operator=(const IfStatement& o) = default;
  IfStatement& operator=(IfStatement&& o) = default;

  const Expression& condition() const noexcept { return m_condition; }

  std::size_t numTrueStatements() const noexcept { return m_trueStatements.size(); }
  auto trueStatementsBegin() const noexcept { return m_trueStatements.cbegin(); }
  auto trueStatementsEnd() const noexcept { return m_trueStatements.cend(); }
  const Statement& trueStatement(std::size_t index) const noexcept { return m_trueStatements[index]; }

  std::size_t numFalseStatements() const noexcept { return m_falseStatements.size(); }
  auto falseStatementsBegin() const noexcept { return m_falseStatements.cbegin(); }
  auto falseStatementsEnd() const noexcept { return m_falseStatements.cend(); }
  const Statement& falseStatement(std::size_t index) const noexcept { return m_falseStatements[index]; }
};
std::ostream& operator<<(std::ostream& out, const IfStatement& ifStmt);

class WhileLoop final {
  Expression m_condition;
  std::vector<Statement> m_statements;
public:
  WhileLoop() = default;
  WhileLoop(Expression condition, std::vector<Statement> statements);
  WhileLoop(const WhileLoop& o) = default;
  WhileLoop(WhileLoop&& o) = default;
  ~WhileLoop() = default;

  WhileLoop& operator=(const WhileLoop& o) = default;
  WhileLoop& operator=(WhileLoop&& o) = default;

  const Expression condition() const noexcept { return m_condition; }
  std::size_t numStatements() const noexcept { return m_statements.size(); }
  auto statementsBegin() const noexcept { return m_statements.cbegin(); }
  auto statementsEnd() const noexcept { return m_statements.cend(); }
  const Statement& statement(std::size_t index) const noexcept { return m_statements[index]; }
};
std::ostream& operator<<(std::ostream& out, const WhileLoop& whileLoop);

using ForInitStatement = boost::variant<VariableDeclaration, ExpressionStatement>;

class ForLoop final {
  boost::optional<ForInitStatement> m_initStatement;
  boost::optional<Expression> m_stepCondition;
  boost::optional<ExpressionStatement> m_stepStatement;
  std::vector<Statement> m_statements;
public:
  ForLoop() = default;
  ForLoop(boost::optional<ForInitStatement> initStatement,
          boost::optional<Expression> stepCondition,
          boost::optional<ExpressionStatement> stepStatement,
          std::vector<Statement> statements);
  ForLoop(const ForLoop& o) = default;
  ForLoop(ForLoop&& o) = default;
  ~ForLoop() = default;

  ForLoop& operator=(const ForLoop& o) = default;
  ForLoop& operator=(ForLoop&& o) = default;

  bool hasInitStatement() const noexcept { return static_cast<bool>(m_initStatement); }
  const ForInitStatement& initStatement() const { return *m_initStatement; }

  bool hasStepCondition() const noexcept { return static_cast<bool>(m_stepCondition); }
  const Expression& stepCondition() const { return *m_stepCondition; }

  bool hasStepStatement() const noexcept { return static_cast<bool>(m_stepStatement); }
  const ExpressionStatement& stepStatement() const { return *m_stepStatement; }

  std::size_t numStatements() const noexcept { return m_statements.size(); }
  auto statementsBegin() const noexcept { return m_statements.cbegin(); }
  auto statementsEnd() const noexcept { return m_statements.cend(); }
  const Statement& statement(std::size_t index) const { return m_statements[index]; }
};
std::ostream& operator<<(std::ostream& out, const ForLoop& forLoop);

class ForeachElemDeclaration final : public SourceLocationInfo {
  PrimitiveType m_type;
  bool m_const;
  Identifier m_id;
public:
  ForeachElemDeclaration() = default;
  ForeachElemDeclaration(PrimitiveType type, bool isConst, Identifier id);
  ForeachElemDeclaration(const ForeachElemDeclaration& o) = default;
  ForeachElemDeclaration(ForeachElemDeclaration&& o) = default;
  ~ForeachElemDeclaration() = default;

  ForeachElemDeclaration& operator=(const ForeachElemDeclaration& o) = default;
  ForeachElemDeclaration& operator=(ForeachElemDeclaration&& o) = default;

  PrimitiveType type() const noexcept { return m_type; }
  bool isConst() const noexcept { return m_const; }
  const Identifier& id() const noexcept { return m_id; }
};
std::ostream& operator<<(std::ostream& out, const ForeachElemDeclaration& elemDecl);

class ForeachLoop final {
  ForeachElemDeclaration m_elemDeclaration;
  Expression m_rangeExpression;
  std::vector<Statement> m_statements;
public:
  ForeachLoop() = default;
  ForeachLoop(ForeachElemDeclaration elemDeclaration,
              Expression rangeExpression,
              std::vector<Statement> statements);
  ForeachLoop(const ForeachLoop& o) = default;
  ForeachLoop(ForeachLoop&& o) = default;
  ~ForeachLoop() = default;

  ForeachLoop& operator=(const ForeachLoop& o) = default;
  ForeachLoop& operator=(ForeachLoop&& o) = default;

  const ForeachElemDeclaration& elemDeclaration() const noexcept { return m_elemDeclaration; }
  const Expression& rangeExpression() const noexcept { return m_rangeExpression; }

  std::size_t numStatements() const noexcept { return m_statements.size(); }
  auto statementsBegin() const noexcept { return m_statements.cbegin(); }
  auto statementsEnd() const noexcept { return m_statements.cend(); }
  const Statement& statement(std::size_t index) const noexcept { return m_statements[index]; }
};
std::ostream& operator<<(std::ostream& out, const ForeachLoop& foreachLoop);

using SimpleStatement = boost::variant<
  DeclarationStatement,
  ExpressionStatement,
  LoopControl,
  ReturnStatement
>;

class Statement final : public SourceLocationInfo {
public:
  using variant_t = boost::variant<
    SimpleStatement,
    IfStatement,
    WhileLoop,
    ForLoop,
    ForeachLoop
  >;
private:
  variant_t m_statement;
public:
  Statement() = default;
  explicit Statement(SimpleStatement statement);
  explicit Statement(IfStatement statement);
  explicit Statement(WhileLoop statement);
  explicit Statement(ForLoop statement);
  explicit Statement(ForeachLoop statement);
  Statement(const Statement& o) = default;
  Statement(Statement&& o) = default;
  ~Statement() = default;

  Statement& operator=(SimpleStatement statement);
  Statement& operator=(IfStatement statement);
  Statement& operator=(WhileLoop statement);
  Statement& operator=(ForLoop statement);
  Statement& operator=(ForeachLoop statement);
  Statement& operator=(const Statement& o) = default;
  Statement& operator=(Statement&& o) = default;

  const variant_t& statement() const { return m_statement; }
};
std::ostream& operator<<(std::ostream& out, const Statement& statement);

using ReturnType = boost::variant<Void, PrimitiveType, ArrayType<true>>;

class Parameter final : public SourceLocationInfo {
public:
  using Type = boost::variant<PrimitiveType, ArrayType<true>>;
private:
  Type m_type;
  Identifier m_id;
public:
  Parameter() = default;
  Parameter(PrimitiveType type, Identifier id);
  Parameter(ArrayType<true> type, Identifier id);
  Parameter(const Parameter& o) = default;
  Parameter(Parameter&& o) = default;
  ~Parameter() = default;

  Parameter& operator=(const Parameter& o) = default;
  Parameter& operator=(Parameter&& o) = default;

  const Type& type() const noexcept { return m_type; }
  const Identifier& id() const noexcept { return m_id; }
};
std::ostream& operator<<(std::ostream& out, const Parameter& param);

class Function final : public SourceLocationInfo {
  ReturnType m_returnType;
  Identifier m_id;
  std::vector<Parameter> m_params;
  std::vector<Statement> m_statements;
public:
  Function() = default;
  Function(ReturnType returnType,
           Identifier id,
           std::vector<Parameter> params,
           std::vector<Statement> statements);
  Function(const Function& o) = default;
  Function(Function&& o) = default;
  ~Function() = default;

  Function& operator=(const Function& o) = default;
  Function& operator=(Function&& o) = default;

  const ReturnType& returnType() const noexcept { return m_returnType; }
  const Identifier& id() const noexcept { return m_id; }

  std::size_t numParams() const noexcept { return m_params.size(); }
  auto paramsBegin() const noexcept { return m_params.cbegin(); }
  auto paramsEnd() const noexcept { return m_params.cend(); }
  const Parameter& param(std::size_t index) const noexcept { return m_params[index]; }

  std::size_t numStatements() const noexcept { return m_statements.size(); }
  auto statementsBegin() const noexcept { return m_statements.cbegin(); }
  auto statementsEnd() const noexcept { return m_statements.cend(); }
  const Statement& statement(std::size_t index) const noexcept { return m_statements[index]; }
};
std::ostream& operator<<(std::ostream& out, const Function& funcDef);

class Comment final : public SourceLocationInfo {
  std::string m_text;
public:
  Comment() = default;
  explicit Comment(std::string text);
  Comment(const Comment& o) = default;
  Comment(Comment&& o) = default;
  ~Comment() = default;

  Comment& operator=(const Comment& o) = default;
  Comment& operator=(Comment&& o) = default;

  const std::string& text() const noexcept { return m_text; }
};
std::ostream& operator<<(std::ostream& out, const Comment& comment);

}
}

#endif // SIMPLEC_AST_STATEMENTS_H
