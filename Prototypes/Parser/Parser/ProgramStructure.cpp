#include "ProgramStructure.h"

#include <utility>

namespace simplec {
namespace ast {

void ProgramStructure::addGlobal(GlobalDeclaration global) {
  m_globalVariables.emplace_back(std::move(global));
}

void ProgramStructure::addFunction(Function funcDef) {
  m_functions.emplace_back(std::move(funcDef));
}

void ProgramStructure::addComment(Comment comment) {
  m_comments.emplace_back(std::move(comment));
}

std::ostream& operator<<(std::ostream& out, const ProgramStructure& program) {
  out << "ProgramStructure {\n";
  out << "  numComments=" << program.numComments(); out << ",\n";
  out << "  comments = [\n";
  for (std::size_t i = 0; i < program.numComments(); ++i)
    out << "    " << program.comment(i) << ",\n";
  out << "  ],\n  numGlobals=" << program.numGlobals() << ",\n";
  out << "  globals=[\n";
  for (std::size_t i = 0; i < program.numGlobals(); ++i)
    out << "    " << program.global(i) << ",\n";
  out << "  ],\n  numFunctions=" << program.numFunctions() << ",\n";
  out << "  functions=[\n";
  for (std::size_t i = 0; i < program.numFunctions(); ++i)
    out << "    " << program.function(i) << ",\n";
  return out << "  ]\n}";
}

}
}
