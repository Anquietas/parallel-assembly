#pragma once
#ifndef SIMPLEC_AST_EXPRESSIONS_H
#define SIMPLEC_AST_EXPRESSIONS_H

#include <iostream>
#include <vector>
#include <utility>
#include <boost/variant.hpp>

#include "SimpleC_Core.h"
#include "SourceLocationInfo.h"
#include "Stepable.h"

namespace simplec {
namespace ast {

class Expression;

class ArrayLookup final : public eval::Stepable {
  Identifier m_id;
  std::vector<Expression> m_subscripts;
public:
  ArrayLookup() = default;
  ArrayLookup(Identifier id, std::vector<Expression> subscripts);
  ArrayLookup(const ArrayLookup& o) = default;
  ArrayLookup(ArrayLookup&& o) = default;
  ~ArrayLookup() = default;

  ArrayLookup& operator=(const ArrayLookup& o) = default;
  ArrayLookup& operator=(ArrayLookup&& o) = default;

  const Identifier& id() const noexcept { return m_id; }
  std::size_t numSubscripts() const noexcept { return m_subscripts.size(); }
  auto subscriptsBegin() const noexcept { return m_subscripts.cbegin(); }
  auto subscriptsEnd() const noexcept { return m_subscripts.cend(); }
  const Expression& subscript(std::size_t index) const noexcept { return m_subscripts[index]; }

  virtual void step(eval::ProgramState& state) const override;
};
std::ostream& operator<<(std::ostream& out, const ArrayLookup& arrayLookup);

using BasicExpression = boost::variant<
  ArrayLookup,
  Identifier>;

class FuncCall final : public eval::Stepable {
  Identifier m_funcId;
  std::vector<Expression> m_params;
public:
  FuncCall() = default;
  FuncCall(Identifier funcId, std::vector<Expression> params);
  FuncCall(const FuncCall& o) = default;
  FuncCall(FuncCall&& o) = default;
  ~FuncCall() = default;

  FuncCall& operator=(const FuncCall& o) = default;
  FuncCall& operator=(FuncCall&& o) = default;

  const Identifier& funcId() const noexcept { return m_funcId; }

  std::size_t numParams() const noexcept { return m_params.size(); }
  auto paramsBegin() const noexcept { return m_params.cbegin(); }
  auto paramsEnd() const noexcept { return m_params.cend(); }
  const Expression& param(std::size_t index) const noexcept { return m_params[index]; }

  virtual void step(eval::ProgramState& state) const override;
};
std::ostream& operator<<(std::ostream& out, const FuncCall& funcCall);

using SimpleExpression = boost::variant<
  SimpleValue,
  BasicExpression,
  FuncCall>;

class MemberExpression final : public SourceLocationInfo, public eval::Stepable {
  SimpleExpression m_expr;
  FuncCall m_funcCall;
public:
  MemberExpression() = default;
  MemberExpression(SimpleExpression expr, FuncCall funcCall);
  MemberExpression(const MemberExpression& o) = default;
  MemberExpression(MemberExpression&& o) = default;
  ~MemberExpression() = default;

  MemberExpression& operator=(const MemberExpression& o) = default;
  MemberExpression& operator=(MemberExpression&& o) = default;

  const SimpleExpression& expr() const noexcept { return m_expr; }
  const FuncCall& funcCall() const noexcept { return m_funcCall; }

  virtual void step(eval::ProgramState& state) const override;
};
std::ostream& operator<<(std::ostream& out, const MemberExpression& memExpr);

class IncrExpression final : public SourceLocationInfo, public eval::Stepable {
  IncrOp m_op;
  BasicExpression m_expr;
public:
  IncrExpression() = default;
  IncrExpression(IncrOp op, BasicExpression expr);
  IncrExpression(const IncrExpression& o) = default;
  IncrExpression(IncrExpression&& o) = default;
  ~IncrExpression() = default;

  IncrExpression& operator=(const IncrExpression& o) = default;
  IncrExpression& operator=(IncrExpression&& o) = default;

  IncrOp op() const noexcept { return m_op; }
  const BasicExpression& expr() const noexcept { return m_expr; }

  virtual void step(eval::ProgramState& state) const override;
};
std::ostream& operator<<(std::ostream& out, const IncrExpression& incrExpr);

class CastExpression;
class UnaryOpExpression;
class BinaryOpExpression;

class Expression final : public SourceLocationInfo, public eval::Stepable {
public:
  using variant_t = boost::variant <
    SimpleExpression,
    MemberExpression,
    IncrExpression,
    boost::recursive_wrapper<CastExpression>,
    boost::recursive_wrapper<UnaryOpExpression>,
    boost::recursive_wrapper<BinaryOpExpression>
  >;
private:
  variant_t m_expr;
public:
  Expression() = default;
  explicit Expression(SimpleExpression expr);
  explicit Expression(MemberExpression expr);
  explicit Expression(IncrExpression expr);
  explicit Expression(CastExpression expr);
  explicit Expression(UnaryOpExpression expr);
  explicit Expression(BinaryOpExpression expr);
  Expression(const Expression& o) = default;
  Expression(Expression&& o) = default;
  ~Expression() = default;

  Expression& operator=(SimpleExpression expr);
  Expression& operator=(MemberExpression expr);
  Expression& operator=(IncrExpression expr);
  Expression& operator=(CastExpression expr);
  Expression& operator=(UnaryOpExpression expr);
  Expression& operator=(BinaryOpExpression expr);
  Expression& operator=(const Expression& o) = default;
  Expression& operator=(Expression&& o) = default;

  const variant_t& expr() const noexcept { return m_expr; }

  virtual void setEndpoints(const SourcePosition& begin, const SourcePosition& end) override;
  virtual void resetEndpoints() noexcept override;

  virtual void step(eval::ProgramState& state) const override;
};
std::ostream& operator<<(std::ostream& out, const Expression& expr);

class CastExpression final : public SourceLocationInfo, public eval::Stepable {
  PrimitiveType m_castType;
  Expression m_expr;
public:
  CastExpression() = default;
  CastExpression(PrimitiveType castType, Expression expr);
  CastExpression(const CastExpression& o) = default;
  CastExpression(CastExpression&& o) = default;
  ~CastExpression() = default;

  CastExpression& operator=(const CastExpression& o) = default;
  CastExpression& operator=(CastExpression&& o) = default;

  PrimitiveType castType() const { return m_castType; }
  const Expression& expr() const { return m_expr; }

  virtual void step(eval::ProgramState& state) const override;
};
std::ostream& operator<<(std::ostream& out, const CastExpression& cast);

class UnaryOpExpression final : public SourceLocationInfo, public eval::Stepable {
  UnaryOp m_op;
  Expression m_expr;
public:
  UnaryOpExpression() = default;
  UnaryOpExpression(UnaryOp op, Expression expr);
  UnaryOpExpression(const UnaryOpExpression& o) = default;
  UnaryOpExpression(UnaryOpExpression&& o) = default;
  ~UnaryOpExpression() = default;

  UnaryOpExpression& operator=(const UnaryOpExpression& o) = default;
  UnaryOpExpression& operator=(UnaryOpExpression&& o) = default;

  UnaryOp op() const noexcept { return m_op; }
  const Expression& expr() const noexcept { return m_expr; }

  virtual void step(eval::ProgramState& state) const override;
};
std::ostream& operator<<(std::ostream& out, const UnaryOpExpression& unaryExpr);

class BinaryOpExpression final : public eval::Stepable {
  Expression m_left;
  BinaryOp m_op;
  Expression m_right;
public:
  BinaryOpExpression() = default;
  BinaryOpExpression(Expression left, BinaryOp op, Expression right);
  BinaryOpExpression(const BinaryOpExpression& o) = default;
  BinaryOpExpression(BinaryOpExpression&& o) = default;
  ~BinaryOpExpression() = default;

  BinaryOpExpression& operator=(const BinaryOpExpression& o) = default;
  BinaryOpExpression& operator=(BinaryOpExpression&& o) = default;

  BinaryOp op() const noexcept { return m_op; }
  const Expression& left() const noexcept { return m_left; }
  const Expression& right() const noexcept { return m_right; }

  virtual void step(eval::ProgramState& state) const override;
};
std::ostream& operator<<(std::ostream& out, const BinaryOpExpression& binExpr);

class AssignExpression final : public SourceLocationInfo, public eval::Stepable {
  BasicExpression m_left;
  AssignOp m_op;
  Expression m_right;
public:
  AssignExpression() = default;
  AssignExpression(BasicExpression left, AssignOp op, Expression right);
  AssignExpression(const AssignExpression& o) = default;
  AssignExpression(AssignExpression&& o) = default;
  ~AssignExpression() = default;

  AssignExpression& operator=(const AssignExpression& o) = default;
  AssignExpression& operator=(AssignExpression&& o) = default;

  AssignOp op() const noexcept { return m_op; }
  const BasicExpression& left() const noexcept { return m_left; }
  const Expression& right() const noexcept { return m_right; }

  virtual void step(eval::ProgramState& state) const override;
};
std::ostream& operator<<(std::ostream& out, const AssignExpression& assignExpr);

}
}

#endif // SIMPLEC_AST_EXPRESSIONS_H
