#pragma once
#ifndef PARSER_UTILITIES_H
#define PARSER_UTILITIES_H

#include <cctype>
#include <algorithm>
#include <iterator>
#include <string>

namespace utility {

template <typename Iterator>
void trimWhitespaceBefore(Iterator& begin, const Iterator& end) {
  begin = std::find_if_not(begin, end,
    [](auto c) -> bool {
      return std::isspace(c) != 0;
    });
  assert(begin == end || std::isspace(*begin) == 0);
}
template <typename Iterator>
void trimWhitespaceAfter(const Iterator& begin, Iterator& end) {
  auto rBegin = std::make_reverse_iterator(end);
  auto rEnd = std::make_reverse_iterator(begin);

  end = std::find_if_not(rBegin, rEnd, 
    [](auto c) -> bool {
      return std::isspace(c) != 0;
    }).base();
  assert(begin == end || std::isspace(*std::prev(end)) == 0);
}

template <typename Iterator>
void trimWhitespace(Iterator& begin, Iterator& end) {
  trimWhitespaceBefore(begin, end);
  trimWhitespaceAfter(begin, end);
}

}

#endif // PARSER_UTILITIES_H
