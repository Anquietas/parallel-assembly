#include "ExpressionGrammar.h"

#ifdef _MSC_VER
#pragma warning(push)
// Redefinitions - internal problem with spirit, compiles anyway.
#pragma warning(disable:4348)
// Declaration hides outer declaration
#pragma warning(disable:4459)
// Unreferenced formal parameter
#pragma warning(disable:4100)
// Decorated name length exceeded
#pragma warning(disable:4503)
#endif 

#include <boost/spirit/include/qi_auxiliary.hpp>
#include <boost/spirit/repository/include/qi_distinct.hpp>

#include <boost/spirit/include/phoenix.hpp>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include "Annotator.h"
#include "ErrorHandlers.h"
#include "ParserHelperFunctors.h"
#include "LinePosIterator.h"
#include "ParserBuildConfig.h"

// Decorated name length exceeded
#ifdef _MSC_VER
#pragma warning(disable:4503)
#endif

namespace simplec {
namespace parser {
namespace detail {

namespace qi {
using namespace boost::spirit::repository::qi;
}

namespace ascii = boost::spirit::ascii;

template <typename Iterator>
ExpressionGrammar<Iterator>::ExpressionGrammar(ErrorDiagnostic& diagnostic)
  : base_type(expression, "expression")
{
  using UnarySymbols = qi::symbols<char, ast::UnaryOp>;
  using BinarySymbols = qi::symbols<char, ast::BinaryOp>;

  phx::function<Annotator<Iterator>> annotator;
  phx::function<DefaultErrorHandler<Iterator>> defaultErrorHandler;

  static qi::symbols<char, char> escapedChar;   // escaped characters
  static IncrSymbols preIncrOp;                 // ++x --x
  static UnarySymbols unaryOp;                  // -, !, ~
  static BinarySymbols multDivMod;              // * / %
  static BinarySymbols plusMinus;               // + -
  static BinarySymbols shiftLeftRight;          // << >>
  static BinarySymbols comparisonOp;            // < <= > >=
  static BinarySymbols equalityOp;              // == !=
  static BinarySymbols binAndOp;                // &
  static BinarySymbols binXorOp;                // ^
  static BinarySymbols binOrOp;                 // |
  static BinarySymbols logicalAndOp;            // &&
  static BinarySymbols logicalOrOp;             // ||

  static rule<std::string()>                             identifierRaw;

  static qi::bool_parser<ast::bool_t>                    bool_;
  static qi::int_parser<ast::int_t>                      int_;
  using FloatParser = qi::real_parser<ast::float_t, qi::strict_real_policies<ast::float_t>>;
  static FloatParser                                     float_;
  static rule<ast::bool_t()>                             boolLiteral;
  static rule<ast::float_t()>                            floatLiteral;
  static rule<ast::char_t(), qi::unused_type>            literalChar;
  static rule<ast::char_t()>                             charLiteral;
  static rule<ast::string_t()>                           stringLiteral;
  static rule<ast::SimpleValue()>                        simpleLiteral;

  static rule<ast::FuncCall()>                           memberFuncCall;
  static rule<ast::Expression()>                         stringLiteralExpression;
  static rule<IncrOrMemFuncCall()>                       incrOrMemFuncCall;
  static rule<ParamsOrSubscriptsMemIncr()>               paramsOrSubscriptMemIncr;
  static rule<ast::Expression()>                         identifierExpression;
  static rule<ast::CastExpression()>                     castExpression;
  static rule<ast::Expression()>                         castOrBrackets;
  static rule<VarOrArrayLookup()>                        varOrArrayLookup;

  static rule<ast::Expression()>                         basicExpression;
  static rule<ast::Expression()>                         unaryOpExpression;
  static rule<ast::Expression()>                         binaryExpr1;
  static rule<ast::Expression()>                         binaryExpr2;
  static rule<ast::Expression()>                         binaryExpr3;
  static rule<ast::Expression()>                         binaryExpr4;
  static rule<ast::Expression()>                         binaryExpr5;
  static rule<ast::Expression()>                         binaryExpr6;
  static rule<ast::Expression()>                         binaryExpr7;
  static rule<ast::Expression()>                         binaryExpr8;
  static rule<ast::Expression()>                         binaryExpr9;

  // Set symbols
  {
    using namespace simplec::ast;
    escapedChar.add
      ("\\0", '\0')
      ("\\\'", '\'')
      ("\\\"", '\"')
      ("\\n", '\n')
      ("\\t", '\t')
      ("\\\\", '\\');
    primitiveType.add
      ("bool", PrimitiveType::Bool)
      ("int", PrimitiveType::Int)
      ("float", PrimitiveType::Float)
      ("char", PrimitiveType::Char)
      ("string", PrimitiveType::String);
    preIncrOp.add
      ("++", IncrOp::PreIncrement)
      ("--", IncrOp::PreDecrement);
    postIncrOp.add
      ("++", IncrOp::PostIncrement)
      ("--", IncrOp::PostDecrement);
    unaryOp.add
      ("-", UnaryOp::Negate)
      ("!", UnaryOp::Not)
      ("~", UnaryOp::BinNot);
    multDivMod.add
      ("*", BinaryOp::Multiply)
      ("/", BinaryOp::Divide)
      ("%", BinaryOp::Modulus);
    plusMinus.add
      ("+", BinaryOp::Plus)
      ("-", BinaryOp::Minus);
    shiftLeftRight.add
      ("<<", BinaryOp::ShiftLeft)
      (">>", BinaryOp::ShiftRight);
    comparisonOp.add
      ("<", BinaryOp::Less)
      ("<=", BinaryOp::LessEqual)
      (">", BinaryOp::Greater)
      (">=", BinaryOp::GreaterEqual);
    equalityOp.add
      ("==", BinaryOp::Equal)
      ("!=", BinaryOp::NotEqual);
    binAndOp.add
      ("&", BinaryOp::BinAnd);
    binXorOp.add
      ("^", BinaryOp::BinXor);
    binOrOp.add
      ("|", BinaryOp::BinOr);
    logicalAndOp.add
      ("&&", BinaryOp::And);
    logicalOrOp.add
      ("||", BinaryOp::Or);
  }

  // Set rules
  {
    using ascii::char_;

    identifier %= qi::as_string[qi::lexeme[char_("_a-zA-Z") > *char_("_a-zA-Z0-9")]];

    boolLiteral %= bool_;
    intLiteral %= int_;
    floatLiteral %= float_;
    literalChar %= ~char_("\'\"\n\t\\") | escapedChar;
    charLiteral %= qi::lexeme['\'' > literalChar > '\''];
    stringLiteral %= qi::lexeme['\"' > *literalChar > '\"'];
    simpleLiteral %= boolLiteral
                   | floatLiteral 
                   | intLiteral
                   | charLiteral;

    subscript %= '[' > expression > ']';

    funcParams %= '(' > -(expression % ',') > ')';
    memberFuncCall = ('.' > identifier > funcParams)
      [qi::_val = phx::construct<ast::FuncCall>(qi::_1, qi::_2)];

    stringLiteralExpression = (stringLiteral > -memberFuncCall)
      [qi::_val = phx::bind(StringLitExpressionHelper(), qi::_1, qi::_2)];

    incrOrMemFuncCall %= postIncrOp | memberFuncCall;
    paramsOrSubscriptMemIncr %= funcParams
      | *subscript > -incrOrMemFuncCall;

    identifierExpression = (identifier > paramsOrSubscriptMemIncr)
      [qi::_val = phx::bind(IdentifierExpressionHelper(), qi::_1, qi::_2)];

    castExpression = (primitiveType > ')' > basicExpression)
      [qi::_val = phx::construct<ast::CastExpression>(qi::_1, qi::_2)];
    castOrBrackets %= '(' > (castExpression | expression > ')');

    varOrArrayLookup %= identifier > *subscript;
    preIncrExpression = (preIncrOp > varOrArrayLookup)
      [qi::_val = phx::bind(PreIncrExpressionHelper(), qi::_1, qi::_2)];

    basicExpression %= simpleLiteral
        [qi::_val = phx::construct<ast::SimpleExpression>(qi::_1)]
      | stringLiteralExpression
      | identifierExpression
      | castOrBrackets
      | preIncrExpression;

    auto exprHelper = (qi::_val = phx::bind(ExpressionHelper(), qi::_1, qi::_2));
    unaryOpExpression = basicExpression[qi::_val = qi::_1]
      | (unaryOp > unaryOpExpression)[exprHelper];
    binaryExpr1 = (unaryOpExpression > *(multDivMod > unaryOpExpression))[exprHelper];
    binaryExpr2 = (binaryExpr1 > *(plusMinus > binaryExpr1))[exprHelper];
    binaryExpr3 = (binaryExpr2 > *(shiftLeftRight > binaryExpr2))[exprHelper];
    binaryExpr4 = (binaryExpr3 > *(comparisonOp > binaryExpr3))[exprHelper];
    binaryExpr5 = (binaryExpr4 > *(equalityOp > binaryExpr4))[exprHelper];
    // operator && is ambiguous - could be & or &&
    binaryExpr6 = (binaryExpr5
                > *(qi::distinct('&')[binAndOp] > binaryExpr5))[exprHelper];
    binaryExpr7 = (binaryExpr6 > *(binXorOp > binaryExpr6))[exprHelper];
    // same problem as &&
    binaryExpr8 = (binaryExpr7
                > *(qi::distinct('|')[binOrOp] > binaryExpr7))[exprHelper];
    binaryExpr9 = (binaryExpr8 > *(logicalAndOp > binaryExpr8))[exprHelper];
    expression = (binaryExpr9 > *(logicalOrOp > binaryExpr9))[exprHelper];
  }

  // Set rule names
#ifdef USE_DEBUG_RULE_NAMES
  {
    // Symbols
    escapedChar.name("escapedChar");
    primitiveType.name("primitiveType");
    preIncrOp.name("preIncrOp");
    postIncrOp.name("postIncrOp");
    unaryOp.name("unaryOp");
    multDivMod.name("multDivMod");
    plusMinus.name("plusMinus");
    shiftLeftRight.name("shiftLeftRight");
    comparisonOp.name("comparisonOp");
    equalityOp.name("equalityOp");
    binAndOp.name("binAndOp");
    binXorOp.name("binXorOp");
    binOrOp.name("binOrOp");
    logicalAndOp.name("logicalAndOp");
    logicalOrOp.name("logicalOrOp");

    // Primitivies
    identifier.name("identifier");

    boolLiteral.name("boolLiteral");
    intLiteral.name("intLiteral");
    floatLiteral.name("floatLiteral");
    literalChar.name("literalChar");
    charLiteral.name("charLiteral");
    stringLiteral.name("stringLiteral");
    simpleLiteral.name("simpleLiteral");

    // Expressions
    subscript.name("subscript");
    funcParams.name("funcParams");
    memberFuncCall.name("memberFuncCall");
    stringLiteralExpression.name("stringLiteralExpression");
    incrOrMemFuncCall.name("incrOrMemFuncCall");
    paramsOrSubscriptMemIncr.name("paramsOrSubscriptMemIncr");
    identifierExpression.name("identifierIexpression");
    castExpression.name("castExpression");
    castOrBrackets.name("castOrBrackets");
    varOrArrayLookup.name("varOrArrayLookup");
    preIncrExpression.name("preIncrExpression");
    basicExpression.name("basicExpression");

    unaryOpExpression.name("unaryOpExpression");
    binaryExpr1.name("binaryExpr1");
    binaryExpr2.name("binaryExpr2");
    binaryExpr3.name("binaryExpr3");
    binaryExpr4.name("binaryExpr4");
    binaryExpr5.name("binaryExpr5");
    binaryExpr6.name("binaryExpr6");
    binaryExpr7.name("binaryExpr7");
    binaryExpr8.name("binaryExpr8");
    binaryExpr9.name("binaryExpr9");
    expression.name("expression");
  }
#else // !defined(USE_DEBUG_RULE_NAMES)
  {
    // Symbols
    primitiveType.name("primitive type (bool, int, float, char, string)");
    preIncrOp.name("pre-increment (++) or decrement (--)");
    postIncrOp.name("post-increment (++) or decrement (--)");
    unaryOp.name("unary operator (-, !, ~)");
    multDivMod.name("arithmetic operator (*, /, %)");
    plusMinus.name("arithmetic operator (+, -)");
    shiftLeftRight.name("bitwise shift operator (<<, >>)");
    comparisonOp.name("comparison operator (<, <=, >, >=)");
    equalityOp.name("equality operator (==, !=)");
    binAndOp.name("bitwise and operator (&)");
    binXorOp.name("bitwise xor operator (^)");
    binOrOp.name("bitwise or operator (|)");
    logicalAndOp.name("logical and operator (&&)");
    logicalOrOp.name("logical or operator (||)");

    // Primitives
    identifier.name("identifier");

    boolLiteral.name("literal bool");
    intLiteral.name("literal int");
    floatLiteral.name("float literal");
    charLiteral.name("literal char");
    stringLiteral.name("literal string");

    // Expressions
    subscript.name("subscript");
    funcParams.name("function call parameter list");
    memberFuncCall.name("member function call");
    incrOrMemFuncCall.name("post-increment or decrement or a member function call");
    identifierExpression.name(
      "function call, or identifier or array lookup possibly followed by a "
      + incrOrMemFuncCall.name());
    castExpression.name("cast expression");
    castOrBrackets.name(castExpression.name() + " or "
      + expression.name() + " enclosed in parentheses");
    varOrArrayLookup.name("identifier or array lookup expression");
    preIncrExpression.name("pre-increment or decrement expression");
    basicExpression.name("basic expression");

    unaryOpExpression.name("unary operator expression");
    binaryExpr1.name("binary operator expression[1]");
    binaryExpr2.name("binary operator expression[2]");
    binaryExpr3.name("binary operator expression[3]");
    binaryExpr4.name("binary operator expression[4]");
    binaryExpr5.name("binary operator expression[5]");
    binaryExpr6.name("binary operator expression[6]");
    binaryExpr7.name("binary operator expression[7]");
    binaryExpr8.name("binary operator expression[8]");
    binaryExpr9.name("binary operator expression[9]");
    expression.name("expression");
  }
#endif

  // Set debug
#ifdef ENABLE_RULE_DEBUG
  {
    // Primitives
    qi::debug(identifier);

    qi::debug(boolLiteral);
    qi::debug(intLiteral);
    qi::debug(floatLiteral);
    qi::debug(literalChar);
    qi::debug(charLiteral);
    qi::debug(stringLiteral);
    qi::debug(simpleLiteral);

    // Expressions
    qi::debug(subscript);
    qi::debug(funcParams);
    qi::debug(memberFuncCall);
    qi::debug(stringLiteralExpression);
    qi::debug(incrOrMemFuncCall);
    qi::debug(paramsOrSubscriptMemIncr);
    qi::debug(identifierExpression);
    qi::debug(castExpression);
    qi::debug(castOrBrackets);
    qi::debug(varOrArrayLookup);
    qi::debug(preIncrExpression);
    qi::debug(basicExpression);

    qi::debug(unaryOpExpression);
    qi::debug(binaryExpr1);
    qi::debug(binaryExpr2);
    qi::debug(binaryExpr3);
    qi::debug(binaryExpr4);
    qi::debug(binaryExpr5);
    qi::debug(binaryExpr6);
    qi::debug(binaryExpr7);
    qi::debug(binaryExpr8);
    qi::debug(binaryExpr9);
    qi::debug(expression);
  }
#endif // ENABLE_RULE_DEBUG

  // Set success handlers
  {
    auto annotate = annotator(qi::_val, qi::_1, qi::_2, qi::_3);

    qi::on_success(identifier, annotate);

    qi::on_success(subscript, annotate);

    qi::on_success(stringLiteralExpression, annotate);
    qi::on_success(identifierExpression, annotate);
    qi::on_success(castOrBrackets, annotate);
    qi::on_success(basicExpression, annotate);

    qi::on_success(unaryOpExpression, annotate);
    qi::on_success(binaryExpr1, annotate);
    qi::on_success(binaryExpr2, annotate);
    qi::on_success(binaryExpr3, annotate);
    qi::on_success(binaryExpr4, annotate);
    qi::on_success(binaryExpr5, annotate);
    qi::on_success(binaryExpr6, annotate);
    qi::on_success(binaryExpr7, annotate);
    qi::on_success(binaryExpr8, annotate);
    qi::on_success(binaryExpr9, annotate);
    qi::on_success(expression, annotate);
  }

  // Set error handlers
  {
    auto defaultError = defaultErrorHandler(&diagnostic, false, qi::_1, qi::_2, qi::_3, qi::_4);

    qi::on_error<qi::fail>(identifier, defaultError);

    qi::on_error<qi::fail>(charLiteral, defaultError);
    qi::on_error<qi::fail>(stringLiteral, defaultError);
    qi::on_error<qi::fail>(simpleLiteral, defaultError);

    qi::on_error<qi::fail>(subscript, defaultError);

    qi::on_error<qi::fail>(funcParams, defaultError);
    qi::on_error<qi::fail>(memberFuncCall, defaultError);
    qi::on_error<qi::fail>(stringLiteralExpression, defaultError);
    qi::on_error<qi::fail>(paramsOrSubscriptMemIncr, defaultError);
    qi::on_error<qi::fail>(identifierExpression, defaultError);
    qi::on_error<qi::fail>(castExpression, defaultError);
    qi::on_error<qi::fail>(castOrBrackets, defaultError);
    qi::on_error<qi::fail>(varOrArrayLookup, defaultError);
    qi::on_error<qi::fail>(preIncrExpression, defaultError);
    qi::on_error<qi::fail>(basicExpression, defaultError);

    qi::on_error<qi::fail>(unaryOpExpression, defaultError);
    qi::on_error<qi::fail>(binaryExpr1, defaultError);
    qi::on_error<qi::fail>(binaryExpr2, defaultError);
    qi::on_error<qi::fail>(binaryExpr3, defaultError);
    qi::on_error<qi::fail>(binaryExpr4, defaultError);
    qi::on_error<qi::fail>(binaryExpr5, defaultError);
    qi::on_error<qi::fail>(binaryExpr6, defaultError);
    qi::on_error<qi::fail>(binaryExpr7, defaultError);
    qi::on_error<qi::fail>(binaryExpr8, defaultError);
    qi::on_error<qi::fail>(binaryExpr9, defaultError);
    qi::on_error<qi::fail>(expression, defaultError);
  }
}

template <typename Iterator>
using LinePos = utility::LinePosIterator<Iterator>;

extern template Skipper<LinePos<const char*>>;

template class ExpressionGrammar<LinePos<const char*>>;

}
}
}
