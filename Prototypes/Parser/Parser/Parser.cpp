#include "Parser.h"

#include "ProgramStructure.h"
#include "Program.h"
#include "LinePosIterator.h"
#include "CommentGrammar.h"
#include "Skipper.h"
#include "ExpressionGrammar.h"
#include "SimpleCGrammar.h"
#include "ErrorDiagnostic.h"
#include "TypeChecker.h"

namespace simplec {
namespace parser {
namespace detail {

template <typename Iterator>
using LinePos = utility::LinePosIterator<Iterator>;

extern template CommentGrammar<LinePos<const char*>>;
extern template CommentSkipper<LinePos<const char*>>;
extern template SimpleCGrammar<LinePos<const char*>>;
extern template Skipper<LinePos<const char*>>;

class ParserImpl {
  using iter_t = LinePos<const char*>;

  // Should be empty before and after calling parse()
  mutable ErrorDiagnostic m_diagnostic;

  CommentGrammar<iter_t> m_commentGrammar;
  CommentSkipper<iter_t> m_commentSkipper;
  SimpleCGrammar<iter_t> m_grammar;
  Skipper<iter_t> m_skipper;
public:
  ParserImpl()
    : m_diagnostic()
    , m_commentGrammar(m_diagnostic)
    , m_commentSkipper()
    , m_grammar(m_diagnostic)
    , m_skipper()
  {}
  ParserImpl(const ParserImpl& o)
    : m_diagnostic(o.m_diagnostic)
    , m_commentGrammar(m_diagnostic)
    , m_commentSkipper()
    , m_grammar(m_diagnostic)
    , m_skipper()
  {}
  ParserImpl(ParserImpl&& o)
    : m_diagnostic(std::move(o.m_diagnostic))
    , m_commentGrammar(m_diagnostic)
    , m_commentSkipper()
    , m_grammar(m_diagnostic)
    , m_skipper()
  {}
  ~ParserImpl() = default;

  ParserImpl& operator=(const ParserImpl& o) {
    m_diagnostic = o.m_diagnostic;
    return *this;
  }
  ParserImpl& operator=(ParserImpl&& o) {
    m_diagnostic = std::move(o.m_diagnostic);
    return *this;
  }

private:
  template <typename Iterator, typename Gram, typename Skip>
  bool doParse(Iterator begin, Iterator end,
               const Gram& gram, const Skip& skip,
               ast::ProgramStructure& structure, ErrorHandler& eh) const
  {
    bool pass = qi::phrase_parse(begin, end, gram, skip, structure);
    if (!pass) {
      assert(!m_diagnostic.empty());
      while (!m_diagnostic.empty()) {
        const auto& error = m_diagnostic.nextError();

        eh.parseError(error.first, error.second);
        m_diagnostic.pop();
      }
      m_diagnostic.reset();
    } else if (begin != end) {
      std::ostringstream ss;
      ss << "Parser failed to parse whole input!\n";
      ss << "name: " << gram.name() << '\n';
      ss << "remaining: \"" << std::string(begin, end) << '\"';
      throw std::logic_error(ss.str());
    }
    // Discard any intermediate parse errors if the program parsed successfully anyway.
    while (!m_diagnostic.empty())
      m_diagnostic.pop();
    m_diagnostic.reset();
    return pass;
  }

public:
  boost::optional<Program> parse(const char* sourceBegin, const char* sourceEnd, ErrorHandler& eh) const {
    auto structure = std::make_unique<ast::ProgramStructure>();

    auto begin = iter_t(sourceBegin, sourceBegin);
    auto end   = iter_t(sourceEnd, sourceBegin);

    assert(m_diagnostic.empty());
    // Parse comments
    bool pass =    doParse(begin, end, m_commentGrammar, m_commentSkipper, *structure, eh);
    assert(pass || m_diagnostic.empty());
    // Parse main program if comments passed
    pass = pass && doParse(begin, end, m_grammar, m_skipper, *structure, eh);
    assert(pass || m_diagnostic.empty());

    // Run typechecker
    pass = pass && typecheck::checkTypes(*structure, m_diagnostic);
    if (!pass)
      while (!m_diagnostic.empty()) {
        const auto& error = m_diagnostic.nextError();

        eh.typeError(error.first, error.second);
        m_diagnostic.pop();
      }
    assert(m_diagnostic.empty());

    if (!pass)
      return boost::optional<Program>();
    else
      return boost::make_optional(Program(std::move(structure)));
  }
};

} // namespace detail

Parser::Parser()
  : m_impl(std::make_unique<detail::ParserImpl>())
{}
Parser::Parser(const Parser& p)
  : m_impl(std::make_unique<detail::ParserImpl>(*p.m_impl))
{}
Parser::Parser(Parser&& p) noexcept
  : m_impl(std::move(p.m_impl))
{}
Parser::~Parser() {}

Parser& Parser::operator=(const Parser& p) {
  m_impl = std::make_unique<detail::ParserImpl>(*p.m_impl);
  return *this;
}
Parser& Parser::operator=(Parser&& p) noexcept {
  m_impl = std::move(p.m_impl);
  return *this;
}

boost::optional<Program> Parser::parse(const std::string& source, ErrorHandler& eh) const {
  return m_impl->parse(source.data(), source.data() + source.length(), eh);
}
boost::optional<Program> Parser::parse(const char* source, ErrorHandler& eh) const {
  return m_impl->parse(source, source + std::strlen(source), eh);
}
boost::optional<Program> Parser::parse(const char* source, std::size_t length, ErrorHandler& eh) const {
  return m_impl->parse(source, source + length, eh);
}
boost::optional<Program> Parser::parse(const char* sourceBegin, const char* sourceEnd, ErrorHandler& eh) const {
  return m_impl->parse(sourceBegin, sourceEnd, eh);
}

} // namespace parser
} // namespace simplec
