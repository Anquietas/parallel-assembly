#pragma once
#ifndef SIMPLEC_PARSER_SPIRIT_PROGRAM_STRUCTURE_HANDLER_H
#define SIMPLEC_PARSER_SPIRIT_PROGRAM_STRUCTURE_HANDLER_H

#include <boost/variant.hpp>

#ifdef _MSC_VER
#pragma warning(push)
// Redefinitions - internal problem with spirit, compiles anyway.
#pragma warning(disable:4348)
// Declaration hides outer declaration
#pragma warning(disable:4459)
// Unreferenced formal parameter
#pragma warning(disable:4100)
#endif

#include <boost/spirit/include/support_container.hpp>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include "AST_Statements.h"
#include "ProgramStructure.h"
#include "ParserInternalTypes.h"

namespace boost {
namespace spirit {
namespace traits {

namespace {
namespace ast = simplec::ast;

using GlobalStatement = simplec::parser::detail::GlobalStatement;
}

template <>
struct container_value<ast::ProgramStructure, void> {
  using type = GlobalStatement;
};

template <>
struct push_back_container<ast::ProgramStructure, GlobalStatement, void> {
  struct GlobalStatementVisitor : boost::static_visitor<void> {
    ast::ProgramStructure& structure;

    GlobalStatementVisitor(ast::ProgramStructure& structure_)
      : structure(structure_)
    {}

    GlobalStatementVisitor() = delete;
    GlobalStatementVisitor& operator=(const GlobalStatementVisitor&) = delete;

    void operator()(ast::GlobalDeclaration globDecl) const {
      structure.addGlobal(std::move(globDecl));
    }
    void operator()(ast::Function funcDef) const {
      structure.addFunction(std::move(funcDef));
    }
    void operator()(ast::Comment comment) const {
      structure.addComment(std::move(comment));
    }
  };

  static bool call(ast::ProgramStructure& structure, const GlobalStatement& statement) {
    boost::apply_visitor(GlobalStatementVisitor{ structure }, statement);
    return true;
  }
};

}
}
}

#endif // SIMPLEC_PARSER_SPIRIT_PROGRAM_STRUCTURE_HANDLER_H
