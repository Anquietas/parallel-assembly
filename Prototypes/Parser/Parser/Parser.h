#pragma once
#ifndef SIMPLEC_PARSER_H
#define SIMPLEC_PARSER_H

#include <memory>
#include <string>

#include <boost/optional.hpp>

#include "SourceLocationInfo.h"
#include "Program.h"

namespace simplec {
namespace parser {

namespace detail {
class ParserImpl;
}

struct ErrorHandler {
  virtual void parseError(const SourcePosition& position, 
                          const std::string& message) = 0;
  virtual void typeError(const SourcePosition& position,
                         const std::string& message) = 0;
};

class Parser final {
  std::unique_ptr<detail::ParserImpl> m_impl;
public:
  Parser();
  Parser(const Parser& p);
  Parser(Parser&& p) noexcept;
  ~Parser();

  Parser& operator=(const Parser& p);
  Parser& operator=(Parser&& p) noexcept;

  boost::optional<Program> parse(const std::string& source, ErrorHandler& eh) const;
  boost::optional<Program> parse(const char* source, ErrorHandler& eh) const;
  boost::optional<Program> parse(const char* source, std::size_t length, ErrorHandler& eh) const;
  boost::optional<Program> parse(const char* sourceBegin, const char* sourceEnd, ErrorHandler& eh) const;
};

}
}

#endif // SIMPLEC_PARSER_H
