#pragma once
#ifndef SIMPLEC_TYPECHECKER_TYPES_H
#define SIMPLEC_TYPECHECKER_TYPES_H

#include <vector>
#include <functional>
#include <type_traits>

#include <boost/variant.hpp>

#include "SimpleC_Core.h"

namespace simplec {
namespace typecheck {

using ast::PrimitiveType;
using ast::Void;

// true if t1 is convertible to t2
bool convertibleTo(PrimitiveType t1, PrimitiveType t2);

namespace detail {
template <typename T> struct Hash;
template <>
struct Hash<PrimitiveType> {
  std::size_t operator()(PrimitiveType t) const {
    using Base = std::underlying_type_t<PrimitiveType>;
    return std::hash<Base>()(static_cast<Base>(t));
  }
};
template <>
struct Hash<Void> {
  std::size_t operator()(const Void&) const {
    // An arbitrary number intended to be wildly different from hashes for
    // SimpleType and ArrayType.
    return std::hash<std::int32_t>()(30922646);
  }
};
}

class SimpleType {
  PrimitiveType m_type;
  bool m_const;

public:
  SimpleType(PrimitiveType type, bool isConst)
    : m_type(type), m_const(isConst)
  {}
  SimpleType(const SimpleType& o) = default;
  SimpleType(SimpleType&& o) = default;
  ~SimpleType() = default;

  SimpleType& operator=(const SimpleType& o) = default;
  SimpleType& operator=(SimpleType&& o) = default;

  PrimitiveType type() const noexcept { return m_type; }
  bool isConst() const noexcept { return m_const; }
  void setConst(bool const_) noexcept { m_const = const_; }

  bool operator==(const SimpleType& t) const;

  // true if this is convertible to t
  bool convertibleTo(const SimpleType& t) const;
  bool paramConvertibleTo(const SimpleType& t) const;
};

namespace detail {
template <>
struct Hash<SimpleType> {
  std::size_t operator()(const SimpleType& t) const {
    std::size_t h1 = Hash<PrimitiveType>()(t.type());
    std::size_t h2 = std::hash<bool>()(t.isConst());
    return h1 ^ (h2 << 1);
  }
};
}

class ArrayType {
  PrimitiveType m_elemType;
  bool m_const;
  std::vector<std::size_t> m_extents;

public:
  ArrayType(PrimitiveType elemType, bool isConst, std::size_t numExtents)
    : m_elemType(elemType), m_const(isConst), m_extents(numExtents, 0)
  {}
  ArrayType(PrimitiveType elemType, bool isConst, std::vector<std::size_t> extents)
    : m_elemType(elemType), m_const(isConst), m_extents(std::move(extents))
  {}
  ArrayType(const ArrayType& o) = default;
  ArrayType(ArrayType&& o) = default;
  ~ArrayType() = default;

  ArrayType& operator=(const ArrayType& o) = default;
  ArrayType& operator=(ArrayType&& o) = default;

  PrimitiveType elemType() const noexcept { return m_elemType; }
  bool isConst() const noexcept { return m_const; }
  void setConst(bool const_) noexcept { m_const = const_; }

  std::size_t rank() const noexcept { return m_extents.size(); }
  auto extentsBegin() const noexcept { return m_extents.cbegin(); }
  auto extentsEnd() const noexcept { return m_extents.cend(); }
  std::size_t extent(std::size_t index) const noexcept { return m_extents[index]; }
  void setExtent(std::size_t index, std::size_t extent) {
    m_extents[index] = extent;
  }

  bool operator==(const ArrayType& t) const;

  bool compatible(const ArrayType& t) const;
  // Requires that t be compatible with this
  void makeExtentsCompatible(const ArrayType& t);
  // Requires that t be compatible with this,
  // guarantees t.convertibleTo(*this) after.
  void makeConvertibleFrom(const ArrayType& t);

private:
  bool convertibleToHelper(const ArrayType& t) const;
public:

  // true if this is convertible to t
  bool convertibleTo(const ArrayType& t) const;
  bool paramConvertibleTo(const ArrayType& t) const;

  // true if t is appendable to this
  bool canAppend(const ArrayType& t) const;
};

namespace detail {
template <>
struct Hash<ArrayType> {
  std::size_t operator()(const ArrayType& t) const {
    std::size_t h1 = Hash<PrimitiveType>()(t.elemType());
    std::size_t h2 = std::hash<bool>()(t.isConst());
    std::size_t result = h1 ^ (h2 << 1);

    std::hash<std::size_t> extentHash;
    auto end = t.extentsEnd();
    for (auto iter = t.extentsBegin(); iter != end; ++iter)
      result ^= (extentHash(*iter) << 1);

    return result;
  }
};
}

// Used to signal type is unknown until runtime
struct DeferredType {};
inline bool operator==(const DeferredType&, const DeferredType&) { return true; }

namespace detail {
template <>
struct Hash<DeferredType> {
  std::size_t operator()(const DeferredType&) const {
    // An arbitrary number intended to be wildly different from hashes for
    // Void, SimpleType and ArrayType.
    return std::hash<std::int32_t>()(51518934);
  }
};
}

using Type = boost::variant<ast::Void, DeferredType, SimpleType, ArrayType>;
// operator == doesn't quite do what we want.
bool equal(const Type& t1, const Type& t2);
bool convertibleTo(const Type& t1, const Type& t2);
bool paramConvertibleTo(const Type& t1, const Type& t2);

namespace detail {
template <>
struct Hash<Type> {
private:
  struct TypeVisitor : boost::static_visitor<std::size_t> {
    template <typename T>
    std::size_t operator()(const T& t) const {
      return Hash<T>()(t);
    }
  };
public:
  std::size_t operator()(const Type& t) const {
    return boost::apply_visitor(TypeVisitor(), t);
  }
};
}

enum class TypeCategory {
  Void,
  Deferred,
  Simple,
  Array,
};

TypeCategory category(const Type& type);
bool isVoid(const Type& type);
bool isDeferred(const Type& type);
bool isSimple(const Type& type);
bool isArray(const Type& type);

class FunctionType {
  Type m_returnType;
  std::vector<Type> m_paramTypes;

public:
  FunctionType(Type returnType, std::vector<Type> paramTypes)
    : m_returnType(std::move(returnType))
    , m_paramTypes(std::move(paramTypes))
  {}
  FunctionType(const FunctionType& o) = default;
  FunctionType(FunctionType&& o) = default;
  ~FunctionType() = default;

  FunctionType& operator=(const FunctionType& o) = default;
  FunctionType& operator=(FunctionType&& o) = default;

  const Type& returnType() const noexcept { return m_returnType; }
  void setReturnType(Type type) { m_returnType = std::move(type); }

  std::size_t numParams() const noexcept { return m_paramTypes.size(); }
  auto paramsBegin() const noexcept { return m_paramTypes.cbegin(); }
  auto paramsEnd() const noexcept { return m_paramTypes.cend(); }
  const Type& paramType(std::size_t index) const noexcept {
    return m_paramTypes[index];
  }

  bool operator==(const FunctionType& t) const;
  bool paramsMatch(const FunctionType& t) const;
  bool paramsMatch(const std::vector<Type>& paramTypes) const;
};

}
}

namespace std {
template <> struct hash<simplec::typecheck::PrimitiveType> {
  using argument_type = simplec::typecheck::PrimitiveType;
  using result_type = std::size_t;
  result_type operator()(const argument_type& t) const {
    return simplec::typecheck::detail::Hash<argument_type>()(t);
  }
};
template <> struct hash<simplec::typecheck::Void> {
  using argument_type = simplec::typecheck::Void;
  using result_type = std::size_t;
  result_type operator()(const argument_type& t) const {
    return simplec::typecheck::detail::Hash<argument_type>()(t);
  }
};
template <> struct hash<simplec::typecheck::SimpleType> {
  using argument_type = simplec::typecheck::SimpleType;
  using result_type = std::size_t;
  result_type operator()(const argument_type& t) const {
    return simplec::typecheck::detail::Hash<argument_type>()(t);
  }
};
template <> struct hash<simplec::typecheck::ArrayType> {
  using argument_type = simplec::typecheck::ArrayType;
  using result_type = std::size_t;
  result_type operator()(const argument_type& t) const {
    return simplec::typecheck::detail::Hash<argument_type>()(t);
  }
};
template <> struct hash<simplec::typecheck::DeferredType> {
  using argument_type = simplec::typecheck::DeferredType;
  using result_type = std::size_t;
  result_type operator()(const argument_type& t) const {
    return simplec::typecheck::detail::Hash<argument_type>()(t);
  }
};
template <> struct hash<simplec::typecheck::Type> {
  using argument_type = simplec::typecheck::Type;
  using result_type = std::size_t;
  result_type operator()(const argument_type& t) const {
    return simplec::typecheck::detail::Hash<argument_type>()(t);
  }
};
}

#endif // SIMPLEC_TYPECHECKER_TYPES_H
