#include "TypeCheckerTypes.h"

#include <algorithm>

namespace simplec {
namespace typecheck {

bool convertibleTo(PrimitiveType t1, PrimitiveType t2) {
  return t1 == t2 || (t1 == PrimitiveType::Int && t2 == PrimitiveType::Float);
}
bool paramConvertibleTo(PrimitiveType t1, PrimitiveType t2) {
  return t1 == t2;
}

bool SimpleType::operator==(const SimpleType& t) const {
  return m_type == t.m_type
    && m_const == t.m_const;
}
bool SimpleType::convertibleTo(const SimpleType& t) const {
  // Don't need to care about const - can always just make a copy.
  return typecheck::convertibleTo(m_type, t.m_type);
}
bool SimpleType::paramConvertibleTo(const SimpleType& t) const {
  // Don't need to care about const - can always just make a copy.
  return typecheck::paramConvertibleTo(m_type, t.m_type);
}

bool ArrayType::operator==(const ArrayType& t) const {
  return m_elemType == t.m_elemType
    && m_const == t.m_const
    && std::equal(m_extents.begin(), m_extents.end(),
      t.m_extents.begin(), t.m_extents.end(),
      [](std::size_t x, std::size_t y) -> bool {
        // 0 indicates arbitrary size.
        return (x & y) == 0 || x == y;
      });
}
bool ArrayType::compatible(const ArrayType& t) const {
  return (typecheck::convertibleTo(m_elemType, t.m_elemType)
    || typecheck::convertibleTo(t.m_elemType, m_elemType))
    && m_extents.size() == t.m_extents.size();
}
void ArrayType::makeExtentsCompatible(const ArrayType& t) {
  assert(compatible(t));
  auto numExtents = rank();
  for (std::size_t index = 0; index < numExtents; ++index)
    if (extent(index) != t.extent(index))
      setExtent(index, 0);
}
void ArrayType::makeConvertibleFrom(const ArrayType& t) {
  if (m_elemType == PrimitiveType::Int && t.m_elemType == PrimitiveType::Float)
    m_elemType = t.m_elemType;
  assert(m_elemType == t.m_elemType);
  makeExtentsCompatible(t);
}
bool ArrayType::convertibleToHelper(const ArrayType& t) const {
  // Don't need to care about const - can always just make a copy.
  return m_extents.size() == t.m_extents.size()
    && std::equal(
      m_extents.begin(), m_extents.end(),
      t.m_extents.begin(), t.m_extents.end(),
      [](std::size_t x, std::size_t y) -> bool {
        return y == 0 || x == y;
      });
}
bool ArrayType::convertibleTo(const ArrayType& t) const {
  return typecheck::convertibleTo(m_elemType, t.m_elemType)
    && convertibleToHelper(t);
}
bool ArrayType::paramConvertibleTo(const ArrayType& t) const {
  return typecheck::paramConvertibleTo(m_elemType, t.m_elemType)
    && convertibleToHelper(t);
}
bool ArrayType::canAppend(const ArrayType& t) const {
  // Obviously can't append if types don't match or this is const
  if (m_elemType != t.m_elemType || m_const)
    return false;
  // Can only append t if this array is 1 rank higher than t.
  if (rank() != t.rank() + 1)
    return false;
  // Outermost extent needs to be variable.
  if (m_extents[0] != 0)
    return false;
  // Either all other extents must match, or our extents are variable.
  std::size_t rank_ = rank();
  for (std::size_t i = 1; i < rank_; ++i)
    if (m_extents[i] != 0 && m_extents[i] != t.m_extents[i - 1])
      return false;
  return true;
}

struct TypeEqualVisitor : boost::static_visitor<bool> {
  bool operator()(const ast::Void&, const ast::Void&) const {
    return true;
  }
  bool operator()(const SimpleType& t1, const SimpleType& t2) const {
    return t1 == t2;
  }
  bool operator()(const ArrayType& t1, const ArrayType& t2) const {
    return t1 == t2;
  }

  // All other cases except T or U == DeferredType
  template <typename T, typename U>
  bool operator()(const T&, const U&) const {
    return false;
  }
};
bool equal(const Type& t1, const Type& t2) {
  // Assume deferred types are equal
  return isDeferred(t1)
    || isDeferred(t2)
    || boost::apply_visitor(TypeEqualVisitor(), t1, t2);
}

struct TypeConvertibleVisitor : boost::static_visitor<bool> {
  bool operator()(const ast::Void&, const ast::Void&) const {
    return true;
  }
  bool operator()(const SimpleType& t1, const SimpleType& t2) const {
    return t1.convertibleTo(t2);
  }
  bool operator()(const ArrayType& t1, const ArrayType& t2) const {
    return t1.convertibleTo(t2);
  }

  // All other cases except T or U == DeferredType
  template <typename T, typename U>
  bool operator()(const T&, const U&) const {
    return false;
  }
};
bool convertibleTo(const Type& t1, const Type& t2) {
  return isDeferred(t1)
    || isDeferred(t2)
    || boost::apply_visitor(TypeConvertibleVisitor(), t1, t2);
}

struct ParamTypeConvertibleVisitor : boost::static_visitor<bool> {
  bool operator()(const ast::Void&, const ast::Void&) const {
    assert(false);
    return true;
  }
  bool operator()(const SimpleType& t1, const SimpleType& t2) const {
    return t1.paramConvertibleTo(t2);
  }
  bool operator()(const ArrayType& t1, const ArrayType& t2) const {
    return t1.paramConvertibleTo(t2);
  }

  // All other cases except T or U == DeferredType
  template <typename T, typename U>
  bool operator()(const T&, const U&) const {
    return false;
  }
};
bool paramConvertibleTo(const Type& t1, const Type& t2) {
  return isDeferred(t1)
    || isDeferred(t2)
    || boost::apply_visitor(ParamTypeConvertibleVisitor(), t1, t2);
}

struct TypeCategoryVisitor : boost::static_visitor<TypeCategory> {
  TypeCategory operator()(const ast::Void&) const {
    return TypeCategory::Void;
  }
  TypeCategory operator()(const DeferredType&) const {
    return TypeCategory::Deferred;
  }
  TypeCategory operator()(const SimpleType&) const {
    return TypeCategory::Simple;
  }
  TypeCategory operator()(const ArrayType&) const {
    return TypeCategory::Array;
  }
};

TypeCategory category(const Type& type) {
  return boost::apply_visitor(TypeCategoryVisitor(), type);
}

bool isVoid(const Type& type) {
  return category(type) == TypeCategory::Void;
}
bool isDeferred(const Type& type) {
  return category(type) == TypeCategory::Deferred;
}
bool isSimple(const Type& type) {
  return category(type) == TypeCategory::Simple;
}
bool isArray(const Type& type) {
  return category(type) == TypeCategory::Array;
}

bool FunctionType::operator==(const FunctionType& t) const {
  return equal(m_returnType, t.m_returnType)
    && paramsMatch(t.m_paramTypes);
}
bool FunctionType::paramsMatch(const FunctionType& t) const {
  return paramsMatch(t.m_paramTypes);
}
bool FunctionType::paramsMatch(const std::vector<Type>& paramTypes) const {
  return std::equal(
    m_paramTypes.cbegin(), m_paramTypes.cend(),
    paramTypes.cbegin(), paramTypes.cend(),
    [](const Type& t1, const Type& t2) -> bool {
      return paramConvertibleTo(t2, t1);
    });
}

}
}
