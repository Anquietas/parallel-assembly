#pragma once
#ifndef UTILITY_LINE_POS_ITERATOR_H
#define UTILITY_LINE_POS_ITERATOR_H

#include <cassert>
#include <stdexcept>
#include <utility>
#include <boost/iterator_adaptors.hpp>
#include <boost/iterator/iterator_traits.hpp>
#include <boost/range/iterator_range.hpp>

#include "SourceLocationInfo.h"

namespace utility {

// A more efficient and less buggy replacement of boost::spirit::line_pos_iterator
template <typename Iterator>
class LinePosIterator final
  : public boost::iterator_adaptor<LinePosIterator<Iterator>,
                                   Iterator,
                                   boost::use_default,
                                   boost::bidirectional_traversal_tag>
{
  Iterator m_inputBegin;
  Iterator m_lineBegin;
  std::size_t m_lineNumber;
public:
  LinePosIterator()
    : iterator_adaptor_()
    , m_inputBegin()
    , m_lineBegin()
    , m_lineNumber(1)
  {}
  LinePosIterator(const Iterator& iter, const Iterator& inputBegin)
    : iterator_adaptor_(iter)
    , m_inputBegin(inputBegin)
    , m_lineBegin(iter)
    , m_lineNumber(1)
  {}
  LinePosIterator(const LinePosIterator&) = default;
  LinePosIterator(LinePosIterator&&) = default;
  ~LinePosIterator() = default;

  LinePosIterator& operator=(const LinePosIterator&) = default;
  LinePosIterator& operator=(LinePosIterator&&) = default;

private:
  // Used to construct iterators for member functions
  LinePosIterator(const Iterator& iter,
                  const Iterator& inputBegin,
                  const Iterator& lineBegin,
                  std::size_t lineNumber)
    : iterator_adaptor_(iter)
    , m_inputBegin(inputBegin)
    , m_lineBegin(lineBegin)
    , m_lineNumber(lineNumber)
  {}

  friend class boost::iterator_core_access;

  void increment() {
    auto& ref = base_reference();
    // Don't need to care about '\r', sequence will always be \r\n for newline
    switch (*ref) {
    case '\n':
      ++m_lineNumber;
      m_lineBegin = ++ref;
      break;
    default:
      ++ref;
    }
  }
  void decrement() {
    auto& ref = base_reference();
    assert(ref != m_inputBegin && "Attempt to decrement LinePosIterator past start of input");

    switch (*--ref) {
    case '\n':
      --m_lineNumber;
      assert(m_lineNumber >= 1);
      m_lineBegin = ref;
      // Find the previous line start
      do {
        --m_lineBegin;
      } while (*m_lineBegin != '\n' && m_lineBegin != m_inputBegin);
      // Set line position to the character after the newline
      if (m_lineBegin != m_inputBegin)
        ++m_lineBegin;
      break;
    default:
      // do nothing - already decremented iterator.
      break;
    }
  }

public:
  std::size_t lineNumber() const noexcept { return m_lineNumber; }
  std::size_t columnNumber() const {
    // treats tabs as single columns - maybe fix later?
    // column number starts at 1
    return 1 + std::distance(m_lineBegin, base());
  }
  simplec::SourcePosition position() const {
    return simplec::SourcePosition{ lineNumber(), columnNumber() };
  }
  LinePosIterator lineStart() const {
    return LinePosIterator(m_lineBegin, m_inputBegin, m_lineBegin, m_lineNumber);
  }
  LinePosIterator lineEnd(const LinePosIterator& inputEnd) const {
    auto lineEnd = base();
    auto end = inputEnd.base();
    while (lineEnd != end && *lineEnd != '\n')
      ++lineEnd;
    return LinePosIterator(lineEnd, m_inputBegin, m_lineBegin, m_lineNumber);
  }
  boost::iterator_range<LinePosIterator> getLine(const LinePosIterator& inputEnd) const {
    return boost::make_iterator_range(lineStart(), lineEnd(inputEnd));
  }
};

}

#endif // UTILITY_LINE_POS_ITERATOR_H
