#include "EvaluationEnvironment.h"

#include <iostream>
#include <algorithm>
#include <stdexcept>
#include <sstream>
#include <cassert>
#include <random>
#include <limits>

namespace simplec {
namespace eval {

struct ElementVisitor : boost::static_visitor<std::size_t> {
  std::size_t operator()(const ast::SimpleValue&) const {
    return 0;
  }
  std::size_t operator()(const ArrayValue& value) const {
    return value.rank();
  }
};

ArrayValue::ArrayValue(const_iterator begin, const_iterator end)
  : m_contents(begin, end)
{}

std::size_t ArrayValue::rank() const {
  ElementVisitor visitor;
  auto maxRank = std::max_element(
    m_contents.begin(), m_contents.end(),
    [&visitor](const Element& x, const Element& y) -> bool {
    return boost::apply_visitor(visitor, x) < boost::apply_visitor(visitor, y);
  });
  return 1 + boost::apply_visitor(visitor, *maxRank);
}

void ArrayValue::push_back(Element element) {
  m_contents.push_back(std::move(element));
}

ArrayValue ArrayValue::subarray(int_t offset, int_t length) const {
  // TODO: validate offset, length.
  auto begin = m_contents.cbegin() + offset;
  return ArrayValue(begin, begin + length);
}

struct ValueCategoryVisitor : boost::static_visitor<ValueCategory> {
  ValueCategory operator()(const SimpleValue&) const {
    return ValueCategory::Simple;
  }
  ValueCategory operator()(const ArrayValue&) const {
    return ValueCategory::Array;
  }
  ValueCategory operator()(const Void&) const {
    return ValueCategory::Void;
  }
};
ValueCategory category(const Value& value) {
  return boost::apply_visitor(ValueCategoryVisitor(), value);
}
bool isSimple(const Value& value) {
  return category(value) == ValueCategory::Simple;
}
bool isArray(const Value& value) {
  return category(value) == ValueCategory::Array;
}

ValueCategory category(const ReturnValue& value) {
  return boost::apply_visitor(ValueCategoryVisitor(), value);
}
bool isSimple(const ReturnValue& value) {
  return category(value) == ValueCategory::Simple;
}
bool isArray(const ReturnValue& value) {
  return category(value) == ValueCategory::Array;
}
bool isVoid(const ReturnValue& value) {
  return category(value) == ValueCategory::Void;
}

namespace lib {

// Normal functions

class PrintVisitor : public boost::static_visitor<void> {
  std::ostream& m_out;
public:
  PrintVisitor(std::ostream& out) : m_out(out) {}

  void operator()(bool_t value) const {
    m_out << std::boolalpha << value;
  }
  void operator()(int_t value) const {
    m_out << std::dec << value;
  }
  void operator()(float_t value) const {
    m_out << std::dec << value;
  }
  void operator()(char_t value) const {
    m_out << value;
  }
  void operator()(const string_t& value) const {
    m_out << value;
  }
  void operator()(const SimpleValue& value) const {
    boost::apply_visitor(*this, value);
  }

  void operator()(const ArrayValue& value) const {
    m_out << '{';
    for (auto& elem : value) {
      boost::apply_visitor(*this, elem);
      m_out << ", ";
    }
    m_out << '}';
  }
};

struct Print : LibraryFunction {
  virtual void operator()(const std::vector<Value>& params, ReturnValue& result) const override {
    result = Void();
    std::ostringstream ss;
    ss << "[print] ";
    PrintVisitor printer(ss);
    for (auto& param : params)
      boost::apply_visitor(printer, param);
    std::cout << ss.str() << std::endl;
  }
};

struct NumProcessors : LibraryFunction {
  virtual void operator()(const std::vector<Value>& params, ReturnValue& result) const override {
    // TODO: proper implementation
    assert(params.empty());
    result = SimpleValue(int_t(1));
  }
};

struct InputSize : LibraryFunction {
  virtual void operator()(const std::vector<Value>& params, ReturnValue& result) const override {
    // TODO: proper implementation
    switch (params.size()) {
    case 0:
      result = SimpleValue(int_t(0));
      break;
    case 1:
      result = SimpleValue(int_t(0));
      break;
    default:
      assert(false);
    }
  }
};

struct Input : LibraryFunction {
  virtual void operator()(const std::vector<Value>& params, ReturnValue& result) const override {
    // TODO: proper implementation
    switch (params.size()) {
    case 0:
      result = SimpleValue(int_t(0));
      break;
    case 1:
      result = SimpleValue(int_t(0));
      break;
    default:
      assert(false);
    }
  }
};

struct Output : LibraryFunction {
  virtual void operator()(const std::vector<Value>& params, ReturnValue& result) const override {
    // TODO: proper implementation
    result = Void();
    std::ostringstream ss;
    PrintVisitor printer(ss);
    ss << "[output] ";
    switch (params.size()) {
    case 1:
      boost::apply_visitor(printer, params[0]);
      break;
    case 2:
      boost::apply_visitor(printer, params[1]);
      break;
    default:
      assert(false);
    }
    std::cout << ss.str() << std::endl;
  }
};

struct Sync : LibraryFunction {
  virtual void operator()(const std::vector<Value>& params, ReturnValue& result) const override {
    // TODO: proper implementation
    assert(params.empty());
    result = Void();
  }
};

class Random : public LibraryFunction {
  mutable std::mt19937 m_gen;
public:
  Random() : LibraryFunction(), m_gen(std::random_device()()) {}

  SimpleValue operator()(bool_t min, bool_t max) const {
    int intMin = min ? 1 : 0;
    int intMax = max ? 1 : 0;
    std::uniform_int_distribution<int> dist(intMin, intMax);
    return bool_t(dist(m_gen) != 0);
  }
  SimpleValue operator()(int_t min, int_t max) const {
    std::uniform_int_distribution<int_t> dist(min, max);
    return dist(m_gen);
  }
  SimpleValue operator()(float_t min, float_t max) const {
    std::uniform_real_distribution<float_t> dist(min, max);
    return dist(m_gen);
  }
  SimpleValue operator()(char_t min, char_t max) const {
    int intMin = static_cast<int>(min);
    int intMax = static_cast<int>(max);
    std::uniform_int_distribution<int> dist(intMin, intMax);
    return static_cast<char_t>(dist(m_gen));
  }

  template <typename T, typename U>
  SimpleValue operator()(const T&, const U&) const {
    assert(false);
    return bool_t(false);
  }

  virtual void operator()(const std::vector<Value>& params, ReturnValue& result) const override {
    assert(params.size() == 2);
    assert(isSimple(params[0]));
    assert(isSimple(params[1]));

    auto& min = boost::get<SimpleValue>(params[0]);
    auto& max = boost::get<SimpleValue>(params[1]);

    SimpleValue simpleResult = boost::apply_visitor(*this, min, max);
  }
};

}

// Member functions

int_t StringMemFunction::length(const string_t& value) {
  // Very unlikely value is a 2GB string...
  assert(value.size() < static_cast<std::size_t>(std::numeric_limits<int_t>::max()));
  return static_cast<int_t>(value.size());
}
string_t StringMemFunction::substr(const string_t& value, int_t offset, int_t length) {
  // TODO: validate offset, length.
  return value.substr(static_cast<std::size_t>(offset), static_cast<std::size_t>(length));
}

int_t ArrayMemFunction::length(const ArrayValue& value) {
  // Even more unlikely value is a 2 billion element array.
  assert(value.size() < static_cast<std::size_t>(std::numeric_limits<int_t>::max()));
  return static_cast<int_t>(value.size());
}
ArrayValue ArrayMemFunction::subarray(const ArrayValue& value, int_t offset, int_t length) {
  // Handles validation internally.
  return value.subarray(offset, length);
}


Environment::Environment()
  : m_typeEnv()
  , m_userFunctions()
  , m_libFunctions()
{
  m_libFunctions.emplace("print"_id, std::make_unique<lib::Print>());
  m_libFunctions.emplace("numProcessors"_id, std::make_unique<lib::NumProcessors>());
  m_libFunctions.emplace("inputSize"_id, std::make_unique<lib::InputSize>());
  m_libFunctions.emplace("input"_id, std::make_unique<lib::Input>());
  m_libFunctions.emplace("output"_id, std::make_unique<lib::Output>());
  m_libFunctions.emplace("sync"_id, std::make_unique<lib::Sync>());
  m_libFunctions.emplace("random"_id, std::make_unique<lib::Random>());
}

bool Environment::addUserFunction(
  const Identifier& id,
  FunctionType& type,
  const ast::Function& func) 
{
  std::vector<Type> paramTypes(type.paramsBegin(), type.paramsEnd());
  bool result = m_typeEnv.addFunction(id, type);
  if (result) {
    auto funcType = m_typeEnv.lookupFunction(id, paramTypes);
    auto overloadMap = m_userFunctions[id];
    result = overloadMap.emplace(funcType, &func).second;
    assert(result);
  }
  return result;
}

const ast::Function* Environment::lookupUserFunction(
  const Identifier& id,
  const std::vector<Type>& paramTypes) const
{
  auto funcType = m_typeEnv.lookupFunction(id, paramTypes);
  if (funcType) {
    const auto& overloads = m_userFunctions.at(id);
    auto result = overloads.at(funcType);
    return result;
  } else
    return nullptr;
}
const LibraryFunction* Environment::lookupLibraryFunction(const Identifier& id) const {
  auto func = m_libFunctions.find(id);
  if (func != m_libFunctions.end())
    return func->second.get();
  else
    return nullptr;
}

}
}
