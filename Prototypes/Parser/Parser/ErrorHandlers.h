#pragma once
#ifndef SIMPLEC_PARSER_ERROR_HANDLERS_H
#define SIMPLEC_PARSER_ERROR_HANDLERS_H

#include <string>
#include <iostream>
#include <sstream>
#include <type_traits>
#include <cassert>

#include <boost/spirit/include/support_info.hpp>
#include <boost/variant.hpp>

#include "SourceLocationInfo.h"
#include "LinePosIterator.h"
#include "ErrorDiagnostic.h"
#include "ParserUtilities.h"

namespace simplec {
namespace parser {
namespace detail {

template <typename Iterator>
class InfoPrinter {
  std::ostream& m_out;
public:
  InfoPrinter(std::ostream& out) : m_out(out) {}
  InfoPrinter(const InfoPrinter&) = delete;
  InfoPrinter& operator=(const InfoPrinter&) = delete;

  using string = boost::spirit::utf8_string;

  void element(const string& tag, const string& value, int /*depth*/) const {
    if (value == "")
      m_out << tag;
    else
      m_out << '\"' << value << '\"';
  }
};

template <typename Iterator>
struct DefaultErrorHandler {
  template <typename Iter>
  void operator()(ErrorDiagnostic* diagnostic,
                  bool isFinal,
                  utility::LinePosIterator<Iter> ruleBegin,
                  const utility::LinePosIterator<Iter>& inputEnd,
                  const utility::LinePosIterator<Iter>& errorBegin,
                  const boost::spirit::info& what) const
  {
    static_assert(std::is_same<Iterator, utility::LinePosIterator<Iter>>::value,
      "Type mismatch in DefaultErrorHandler::operator()");

    // A bit of a hack to get around the fact none of spirit's error modes
    // allow you to stop error handling after a certain point.
    if (diagnostic->isFinalized()) return;

    // Skip newlines etc before the error that spirit didn't remove
    utility::trimWhitespaceBefore(ruleBegin, errorBegin);

    using boost::spirit::basic_info_walker;
    std::ostringstream ss;
    InfoPrinter<Iterator> printer(ss);
    basic_info_walker<InfoPrinter<Iterator>> walker(printer, what.tag, 0);

    auto ruleStart  = ruleBegin.position();
    auto errorPos   = errorBegin.position();
    ss << "expecting ";
    boost::apply_visitor(walker, what.value);
    ss << ", got \""
      << std::string(errorBegin, errorBegin.lineEnd(inputEnd))
      << "\"\nafter processing \"" << std::string(ruleBegin, errorBegin)
      << "\"\nstarting at " << ruleStart;

    diagnostic->push(errorPos, ss.str());
    if (isFinal)
      diagnostic->finalize();
  }
};

template <typename Iterator>
struct StatementErrorHandler {
  template <typename Iter>
  void operator()(ErrorDiagnostic* diagnostic,
                  bool isFinal,
                  utility::LinePosIterator<Iter> ruleBegin,
                  const utility::LinePosIterator<Iter>& inputEnd,
                  const utility::LinePosIterator<Iter>& errorBegin,
                  const boost::spirit::info& what) const
  {
    if (diagnostic->isFinalized()) return;

    static_assert(std::is_same<Iterator, utility::LinePosIterator<Iter>>::value,
      "Type mismatch in StatementErrorHandler::operator()");
    if (what.tag == "eps") {
      utility::trimWhitespaceBefore(ruleBegin, errorBegin);
      std::ostringstream ss;
      ss << "Unexpected ";

      std::string content(ruleBegin, errorBegin);
      if (content.find("break") != std::string::npos) {
        ss << "break";
      } else if (content.find("continue") != std::string::npos) {
        ss << "continue";
      } else
        assert(false);

      ss << " statement. Loop control statements can't be used outside loop bodies.";

      auto errorPos = errorBegin.position();
      diagnostic->push(errorPos, ss.str());
      if (isFinal)
        diagnostic->finalize();
    } else
      DefaultErrorHandler<Iterator>()(diagnostic, isFinal, ruleBegin, inputEnd, errorBegin, what);
  }
};

template <typename Iterator>
struct ProgramErrorHandler {
  template <typename Iter>
  void operator()(ErrorDiagnostic* diagnostic,
                  bool isFinal,
                  const std::string& globalStatementName,
                  utility::LinePosIterator<Iter> ruleBegin,
                  const utility::LinePosIterator<Iter>& inputEnd,
                  const utility::LinePosIterator<Iter>& errorBegin,
                  const boost::spirit::info&) const
  {
    static_assert(std::is_same<Iterator, utility::LinePosIterator<Iter>>::value,
      "Type mismatch in DefaultErrorHandler::operator()");

    if (diagnostic->isFinalized()) return;

    utility::trimWhitespaceBefore(ruleBegin, errorBegin);

    auto ruleStart = ruleBegin.position();
    auto errorPos = errorBegin.position();
    std::ostringstream ss;
    ss  << "expecting " << globalStatementName << " or nothing, got \""
        << std::string(errorBegin, errorBegin.lineEnd(inputEnd))
        << "\"\nafter processing \"" << std::string(ruleBegin, errorBegin)
        << "\"\nstarting at " << ruleStart;

    diagnostic->push(errorPos, ss.str());
    if (isFinal)
      diagnostic->finalize();
  }
};

}
}
}

#endif // SIMPLEC_PARSER_ERROR_HANDLERS_H
