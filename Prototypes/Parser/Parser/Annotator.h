#pragma once
#ifndef SIMPLEC_PARSER_ANNOTATOR_H
#define SIMPLEC_PARSER_ANNOTATOR_H

#include <boost/variant.hpp>
#include <boost/phoenix/bind.hpp>

#include "LinePosIterator.h"
#include "SourceLocationInfo.h"
#include "ParserUtilities.h"

//#define ANNOTATE_PRINT_DEBUG

namespace simplec {
namespace parser {
namespace detail {

template <typename Iterator>
class Annotator {
  // Handles boost::variants
  template <typename... Ts>
  void do_annotate(boost::variant<Ts...>& value,
                   const Iterator& ruleBegin,
                   const Iterator& inputEnd,
                   const Iterator& ruleEnd) const
  {
    using namespace phx::arg_names;
    // Handle nested variants correctly
    boost::apply_visitor(phx::bind(*this, _1, ruleBegin, inputEnd, ruleEnd), value);
  }
  // Handles ast classes
  template <typename Iter>
  void do_annotate(SourceLocationInfo& info,
                   utility::LinePosIterator<Iter> ruleBegin,
                   const utility::LinePosIterator<Iter>& inputEnd,
                   utility::LinePosIterator<Iter> ruleEnd) const
  {
    static_assert(std::is_same<Iterator, utility::LinePosIterator<Iter>>::value,
      "Type mismatch for iterator arguments of Annotator::do_annotate");
    // Trim spaces from both ends of the match string
    // - spirit sometimes doesn't remove them with the skipper.
    utility::trimWhitespace(ruleBegin, ruleEnd);
    assert(ruleBegin != ruleEnd && "spirit matched empty string as rule");

    info.setEndpoints(ruleBegin.position(), ruleEnd.position());

#ifdef ANNOTATE_PRINT_DEBUG
    std::cout << "annotating \"" << std::string(ruleBegin, ruleEnd) << "\" as:\n";
    std::cout << info.begin() << " to " << info.end() << std::endl;
    //auto range = ruleBegin.getLine(inputEnd);
    //std::cout << "line: \"" << std::string(range.begin(), range.end()) << "\"" << std::endl;
#endif // ANNOTATE_PRINT_DEBUG
  }

public:
  template <typename Value>
  void operator()(Value& value,
                  const Iterator& ruleBegin,
                  const Iterator& inputEnd,
                  const Iterator& ruleEnd) const
  {
    do_annotate(value, ruleBegin, inputEnd, ruleEnd);
  }
};

}
}
}

#endif // SIMPLEC_PARSER_ANNOTATOR_H
