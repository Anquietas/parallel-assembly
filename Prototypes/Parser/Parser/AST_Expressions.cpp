#include "AST_Expressions.h"

#include <utility>

namespace simplec {
namespace ast {

ArrayLookup::ArrayLookup(Identifier id, std::vector<Expression> subscripts)
  : Stepable()
  , m_id(std::move(id))
  , m_subscripts(std::move(subscripts))
{}
std::ostream& operator<<(std::ostream& out, const ArrayLookup& arrayLookup) {
  out << "ArrayLookup{\n";
  out << "  id=" << arrayLookup.id() << ",\n";
  out << "  numSubscripts=" << arrayLookup.numSubscripts() << ",\n";
  out << "  subscripts=[\n";
  for (std::size_t i = 0; i < arrayLookup.numSubscripts(); ++i) {
    out << "    " << arrayLookup.subscript(i) << ",\n";
  }
  return out << "  ]\n}";
}

FuncCall::FuncCall(Identifier funcId, std::vector<Expression> params)
  : Stepable()
  , m_funcId(std::move(funcId))
  , m_params(std::move(params))
{}
std::ostream& operator<<(std::ostream& out, const FuncCall& funcCall) {
  out << "FuncCall{\n";
  out << "  funcId=" << funcCall.funcId() << ",\n";
  out << "  numParams=" << funcCall.numParams() << ",\n";
  out << "  params=[\n";
  for (std::size_t i = 0; i < funcCall.numParams(); ++i)
    out << "    " << funcCall.param(i) << ",\n";
  return out << "  ]\n}";
}

MemberExpression::MemberExpression(SimpleExpression expr, FuncCall funcCall)
  : SourceLocationInfo()
  , Stepable()
  , m_expr(std::move(expr))
  , m_funcCall(std::move(funcCall))
{}
std::ostream& operator<<(std::ostream& out, const MemberExpression& memExpr) {
  out << "MemberExpression{\n";
  out << "  expr=" << memExpr.expr() << ",\n";
  return out << "  funcCall=" << memExpr.funcCall()  << "\n}";
}

IncrExpression::IncrExpression(IncrOp op, BasicExpression expr)
  : SourceLocationInfo()
  , Stepable()
  , m_op(op)
  , m_expr(std::move(expr))
{}
std::ostream& operator<<(std::ostream& out, const IncrExpression& incrExpr) {
  out << "IncrExpression{\n";
  out << "  op=" << incrExpr.op() << ",\n";
  return out << "  expr=" << incrExpr.expr() << "\n}";
}

Expression::Expression(SimpleExpression expr)
  : SourceLocationInfo()
  , Stepable()
  , m_expr(std::move(expr))
{}
Expression::Expression(MemberExpression expr)
  : SourceLocationInfo()
  , Stepable()
  , m_expr(std::move(expr))
{}
Expression::Expression(IncrExpression expr)
  : SourceLocationInfo()
  , Stepable()
  , m_expr(std::move(expr))
{}
Expression::Expression(CastExpression expr)
  : SourceLocationInfo()
  , Stepable()
  , m_expr(std::move(expr))
{}
Expression::Expression(UnaryOpExpression expr)
  : SourceLocationInfo()
  , Stepable()
  , m_expr(std::move(expr))
{}
Expression::Expression(BinaryOpExpression expr)
  : SourceLocationInfo()
  , Stepable()
  , m_expr(std::move(expr))
{}

Expression& Expression::operator=(SimpleExpression expr) {
  m_expr = std::move(expr);
  resetEndpoints();
  return *this;
}
Expression& Expression::operator=(MemberExpression expr) {
  m_expr = std::move(expr);
  resetEndpoints();
  return *this;
}
Expression& Expression::operator=(IncrExpression expr) {
  m_expr = std::move(expr);
  resetEndpoints();
  return *this;
}
Expression& Expression::operator=(CastExpression expr) {
  m_expr = std::move(expr);
  resetEndpoints();
  return *this;
}
Expression& Expression::operator=(UnaryOpExpression expr) {
  m_expr = std::move(expr);
  resetEndpoints();
  return *this;
}
Expression& Expression::operator=(BinaryOpExpression expr) {
  m_expr = std::move(expr);
  resetEndpoints();
  return *this;
}

class ExpressionSetEndpoints : public boost::static_visitor<void> {
  const SourcePosition& m_begin;
  const SourcePosition& m_end;

public:
  ExpressionSetEndpoints(const SourcePosition& begin, const SourcePosition& end)
    : m_begin(begin)
    , m_end(end)
  {}

  void operator()(const SimpleExpression&) const {}
  void operator()(MemberExpression& expr) const {
    expr.setEndpoints(m_begin, m_end);
  }
  void operator()(IncrExpression& expr) const {
    expr.setEndpoints(m_begin, m_end);
  }
  void operator()(CastExpression& expr) const {
    expr.setEndpoints(m_begin, m_end);
  }
  void operator()(UnaryOpExpression& expr) const {
    expr.setEndpoints(m_begin, m_end);
  }
  void operator()(const BinaryOpExpression&) const {}
};

struct ExpressionResetEndpoints : boost::static_visitor<void> {
  void operator()(const SimpleExpression&) const {}
  void operator()(MemberExpression& expr) const {
    expr.resetEndpoints();
  }
  void operator()(IncrExpression& expr) const {
    expr.resetEndpoints();
  }
  void operator()(CastExpression& expr) const {
    expr.resetEndpoints();
  }
  void operator()(UnaryOpExpression& expr) const {
    expr.resetEndpoints();
  }
  void operator()(const BinaryOpExpression&) const {}
};

void Expression::setEndpoints(const SourcePosition& begin, const SourcePosition& end) {
  this->SourceLocationInfo::setEndpoints(begin, end);
  boost::apply_visitor(ExpressionSetEndpoints(begin, end), m_expr);
}
void Expression::resetEndpoints() noexcept {
  this->SourceLocationInfo::resetEndpoints();
  boost::apply_visitor(ExpressionResetEndpoints(), m_expr);
}

std::ostream& operator<<(std::ostream& out, const Expression& expr) {
  return out << "Expression{expr=" << expr.expr() << "}";
}

CastExpression::CastExpression(PrimitiveType castType, Expression expr)
  : SourceLocationInfo()
  , Stepable()
  , m_castType(castType)
  , m_expr(std::move(expr))
{}
std::ostream& operator<<(std::ostream& out, const CastExpression& cast) {
  out << "CastExpression{\n";
  out << "  castType=" << cast.castType() << ",\n";
  return out << "  expr=" << cast.expr() << "\n}";
}

UnaryOpExpression::UnaryOpExpression(UnaryOp op, Expression expr)
  : SourceLocationInfo()
  , Stepable()
  , m_op(op)
  , m_expr(std::move(expr))
{}
std::ostream& operator<<(std::ostream& out, const UnaryOpExpression& unaryExpr) {
  out << "UnaryOpExpression{\n";
  out << "  op=" << unaryExpr.op() << ",\n";
  return out << "  expr=" << unaryExpr.expr() << "\n}";
}

BinaryOpExpression::BinaryOpExpression(Expression left, BinaryOp op, Expression right)
  : Stepable()
  , m_left(std::move(left))
  , m_op(op)
  , m_right(std::move(right))
{}
std::ostream& operator<<(std::ostream& out, const BinaryOpExpression& binExpr) {
  out << "BinaryOpExpression{\n";
  out << "  left=" << binExpr.left() << ",\n";
  out << "  op=" << binExpr.op() << ",\n";
  return out << "  right=" << binExpr.right() << "\n}";
}

AssignExpression::AssignExpression(BasicExpression left, AssignOp op, Expression right)
  : SourceLocationInfo()
  , Stepable()
  , m_left(std::move(left))
  , m_op(op)
  , m_right(std::move(right))
{}
std::ostream& operator<<(std::ostream& out, const AssignExpression& assignExpr) {
  out << "AssignExpression{\n";
  out << "  left=" << assignExpr.left() << ",\n";
  out << "  op=" << assignExpr.op() << ",\n";
  return out << "  right=" << assignExpr.right() << "\n}";
}

}
}
