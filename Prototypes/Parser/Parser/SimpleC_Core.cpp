#include "SimpleC_Core.h"

#include <iostream>
#include <stdexcept>
#include <cassert>

namespace simplec {

std::pair<char, boost::optional<char>> escape(char c) noexcept {
  constexpr char esc = '\\';
  switch (c) {
  case '\0':
    return std::make_pair(esc, boost::make_optional('0'));
  case '\'':
    return std::make_pair(esc, boost::make_optional('\''));
  case '\"':
    return std::make_pair(esc, boost::make_optional('\"'));
  case '\n':
    return std::make_pair(esc, boost::make_optional('n'));
  case '\t':
    return std::make_pair(esc, boost::make_optional('t'));
  case '\\':
    return std::make_pair(esc, boost::make_optional('\\'));
  default:
    return std::make_pair(c, boost::optional<char>());
  }
}

char unescape(char c) {
  std::cout << "\nunescape called with c = \'" << c << "\'\n";
  switch (c) {
  case '0':
    return '\0';
  case '\'':
    return '\'';
  case '\"':
    return '\"';
  case 'n':
    return '\n';
  case 't':
    return '\t';
  case '\\':
    return '\\';
  default:
    throw std::runtime_error("Unexpected character passed to unescape");
  }
}

namespace ast {

std::ostream& operator<<(std::ostream& out, const PrimitiveType& type) {
  switch (type) {
  case PrimitiveType::Bool:
    out << "Bool";
    break;
  case PrimitiveType::Int:
    out << "Int";
    break;
  case PrimitiveType::Float:
    out << "Float";
    break;
  case PrimitiveType::Char:
    out << "Char";
    break;
  case PrimitiveType::String:
    out << "String";
    break;
  default:
    assert(false);
  }
  return out;
}

Identifier::Identifier(std::string name)
  : SourceLocationInfo(), m_name(std::move(name))
{}
std::ostream& operator<<(std::ostream& out, const Identifier& id) {
  return out << id.name();
}
bool operator<(const Identifier& x, const Identifier& y) {
  return x.name() < y.name();
}
bool operator==(const Identifier& x, const Identifier& y) {
  return x.name() == y.name();
}

TypeSubscript::TypeSubscript(variant_t value)
  : m_value(std::move(value))
{}
std::ostream& operator<<(std::ostream& out, const TypeSubscript& typeSub) {
  return out << "TypeSubscript{value=" << typeSub.value() << "}";
}
std::ostream& operator<<(std::ostream& out, const OptTypeSubscript& typeSub) {
  out << "OptTypeSubscript{";
  if (typeSub)
    out << "value=" << typeSub->value();
  return out << "}";
}

template <bool optSubscripts>
ArrayType<optSubscripts>::ArrayType(
  PrimitiveType elemType, bool isConst, std::vector<Subscript> subscripts)
  : m_elemType(elemType)
  , m_const(isConst)
  , m_subscripts(std::move(subscripts))
{}

template class ArrayType<true>;
template class ArrayType<false>;

std::ostream& operator<<(std::ostream& out, const Void&) {
  return out << "Void{}";
}

std::ostream& operator<<(std::ostream& out, const IncrOp& op) {
  switch (op) {
  case IncrOp::PreIncrement:
    out << "++pre";
    break;
  case IncrOp::PreDecrement:
    out << "--pre";
    break;
  case IncrOp::PostIncrement:
    out << "post++";
    break;
  case IncrOp::PostDecrement:
    out << "post--";
    break;
  default:
    assert(false);
  }
  return out;
}

std::ostream& operator<<(std::ostream& out, const UnaryOp& op) {
  switch (op) {
  case UnaryOp::Negate:
    out << "-(unary)";
    break;
  case UnaryOp::Not:
    out << "!";
    break;
  case UnaryOp::BinNot:
    out << "~";
    break;
  default:
    assert(false);
  }
  return out;
}

std::ostream& operator<<(std::ostream& out, const BinaryOp& op) {
#define OP_OUT(_op, str) \
  case BinaryOp:: _op : \
    out << str; \
    break

  switch (op) {
    OP_OUT(Multiply, "*");
    OP_OUT(Divide, "/");
    OP_OUT(Modulus, "%");
    OP_OUT(Plus, "+");
    OP_OUT(Minus, "-");
    OP_OUT(ShiftLeft, "<<");
    OP_OUT(ShiftRight, ">>");
    OP_OUT(Less, "<");
    OP_OUT(LessEqual, "<=");
    OP_OUT(Greater, ">");
    OP_OUT(GreaterEqual, ">=");
    OP_OUT(Equal, "==");
    OP_OUT(NotEqual, "!=");
    OP_OUT(BinAnd, "&");
    OP_OUT(BinXor, "^");
    OP_OUT(BinOr, "|");
    OP_OUT(And, "&&");
    OP_OUT(Or, "||");
  default:
    assert(false);
  }
  return out;
#undef OP_OUT
}

std::ostream& operator<<(std::ostream& out, const AssignOp& op) {
#define OP_OUT(_op, str) \
  case AssignOp:: _op : \
    out << str; \
    break

  switch (op) {
    OP_OUT(Assign, "=");
    OP_OUT(Plus, "+=");
    OP_OUT(Minus, "-=");
    OP_OUT(Multiply, "*=");
    OP_OUT(Divide, "/=");
    OP_OUT(Modulus, "%=");
    OP_OUT(ShiftLeft, "<<=");
    OP_OUT(ShiftRight, ">>=");
    OP_OUT(BinAnd, "&=");
    OP_OUT(BinXor, "^=");
    OP_OUT(BinOr, "|=");
  default:
    assert(false);
  }
  return out;
}

}
}
