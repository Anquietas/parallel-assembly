#pragma once
#ifndef SIMPLEC_PROGRAM_H
#define SIMPLEC_PROGRAM_H

#include <memory>

namespace simplec {

namespace ast {
class ProgramStructure;
}

class Program {
  std::unique_ptr<ast::ProgramStructure> m_structure;
  // TODO: add evaluation context
public:
  Program(std::unique_ptr<ast::ProgramStructure> structure) noexcept;
  Program(const Program& p);
  Program(Program&& p) noexcept;
  ~Program();

  Program& operator=(const Program& p);
  Program& operator=(Program&& p) noexcept;

  const ast::ProgramStructure& structure() const { return *m_structure; }
};

}

#endif // SIMPLEC_PROGRAM_H
