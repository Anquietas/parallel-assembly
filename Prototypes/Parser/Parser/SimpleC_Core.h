#pragma once
#ifndef SIMPLEC_CORE_H
#define SIMPLEC_CORE_H

#include <iostream>
#include <string>
#include <vector>
#include <cstdint>
#include <utility>

#include <boost/variant.hpp>
#include <boost/optional.hpp>

#include "SourceLocationInfo.h"

namespace simplec {

std::pair<char, boost::optional<char>> escape(char c) noexcept;
// Throws runtime_error if c is not an escaped char
char unescape(char c);

namespace ast {

// Basic Types
using bool_t = bool;
using int_t = std::int32_t;
using float_t = double;
using char_t = char;
using string_t = std::string;

class Identifier final : public SourceLocationInfo {
  std::string m_name;
public:
  Identifier() = default;
  explicit Identifier(std::string name);
  Identifier(const Identifier& o) = default;
  Identifier(Identifier&& o) = default;
  ~Identifier() = default;

  Identifier& operator=(const Identifier& o) = default;
  Identifier& operator=(Identifier&& o) = default;

  const std::string& name() const noexcept { return m_name; }

  // Provided exclusively for boost::fusion adaption - do not use
  void setName(std::string name) { m_name = std::move(name); }
};
std::ostream& operator<<(std::ostream& out, const Identifier& id);
bool operator<(const Identifier& x, const Identifier& y);
bool operator==(const Identifier& x, const Identifier& y);

inline namespace literals {
inline Identifier operator"" _id(const char* name, std::size_t len) {
  return Identifier(std::string(name, len));
}
}

// Type identification
enum class PrimitiveType {
  Bool,
  Int,
  Float,
  Char,
  String,
};
std::ostream& operator<<(std::ostream& out, const PrimitiveType& type);

class TypeSubscript final {
  using variant_t = boost::variant<std::size_t, Identifier>;
  variant_t m_value;
public:
  TypeSubscript() = default;
  explicit TypeSubscript(variant_t value);
  TypeSubscript(const TypeSubscript& o) = default;
  TypeSubscript(TypeSubscript&& o) = default;
  ~TypeSubscript() = default;

  TypeSubscript& operator=(const TypeSubscript& o) = default;
  TypeSubscript& operator=(TypeSubscript&& o) = default;

  const variant_t& value() const noexcept { return m_value; }
};
std::ostream& operator<<(std::ostream& out, const TypeSubscript& typeSub);
using OptTypeSubscript = boost::optional<TypeSubscript>;
std::ostream& operator<<(std::ostream& out, const OptTypeSubscript& typeSub);

template <bool optSubscripts = false>
struct TypeSubscriptType {
  using type = TypeSubscript;
};
template <>
struct TypeSubscriptType<true> {
  using type = OptTypeSubscript;
};
template <bool optSubscripts>
class ArrayType final {
public:
  using Subscript = typename TypeSubscriptType<optSubscripts>::type;
private:
  PrimitiveType m_elemType;
  bool m_const;
  std::vector<Subscript> m_subscripts;
public:
  ArrayType() = default;
  ArrayType(PrimitiveType elemType, bool isConst, std::vector<Subscript> subscripts);
  ArrayType(const ArrayType& o) = default;
  ArrayType(ArrayType&& o) = default;
  ~ArrayType() = default;

  ArrayType& operator=(const ArrayType& o) = default;
  ArrayType& operator=(ArrayType&& o) = default;

  PrimitiveType elemType() const noexcept { return m_elemType; }
  bool isConst() const noexcept { return m_const; }
  std::size_t numSubscripts() const noexcept { return m_subscripts.size(); }
  auto subscriptsBegin() const noexcept { return m_subscripts.cbegin(); }
  auto subscriptsEnd() const noexcept { return m_subscripts.cend(); }
  const Subscript& subscript(std::size_t index) const noexcept { return m_subscripts[index]; }
};
template <bool optSubscripts>
std::ostream& operator<<(std::ostream& out, const ArrayType<optSubscripts>& arrayType) {
  out << "ArrayType<" << std::boolalpha << optSubscripts << ">{\n";
  out << "  elemType=" << arrayType.elemType() << ",\n";
  out << "  isConst=" << arrayType.isConst() << ",\n";
  out << "  numSubscripts=" << arrayType.numSubscripts() << ",\n";
  out << "  subscripts=[\n";
  for (std::size_t i = 0; i < arrayType.numSubscripts(); ++i)
    out << "    " << arrayType.subscript(i) << ",\n";
  return out << "  ]\n}";
}

struct Void {};
inline bool operator==(const Void&, const Void&) { return true; }
std::ostream& operator<<(std::ostream& out, const Void&);

// Values
using SimpleValue = boost::variant<bool_t, int_t, float_t, char_t, string_t>;

// Operators
enum class IncrOp {
  PreIncrement, // ++x
  PreDecrement, // --x
  PostIncrement,// x++
  PostDecrement,// x--
};
std::ostream& operator<<(std::ostream& out, const IncrOp& op);
enum class UnaryOp {
  Negate,       // -x
  Not,          // !x
  BinNot,       // ~x
};
std::ostream& operator<<(std::ostream& out, const UnaryOp& op);
enum class BinaryOp {
  Multiply,     // x * y 
  Divide,       // x / y
  Modulus,      // x % y
  Plus,         // x + y
  Minus,        // x - y
  ShiftLeft,    // x << y
  ShiftRight,   // x >> y
  Less,         // x < y
  LessEqual,    // x <= y
  Greater,      // x > y
  GreaterEqual, // x >= y
  Equal,        // x == y
  NotEqual,     // x != y
  BinAnd,       // x & y
  BinXor,       // x ^ y
  BinOr,        // x | y
  And,          // x && y
  Or,           // x || y
};
std::ostream& operator<<(std::ostream& out, const BinaryOp& op);
enum class AssignOp {
  Assign,     // x = y
  Plus,       // x += y
  Minus,      // x -= y
  Multiply,   // x *= y
  Divide,     // x /= y
  Modulus,    // x %= y
  ShiftLeft,  // x <<= y
  ShiftRight, // x >>= y
  BinAnd,     // x &= y
  BinXor,     // x ^= y
  BinOr,      // x |= y
};
std::ostream& operator<<(std::ostream& out, const AssignOp& op);

// Operator precedence lookup for binary operators (used by Generator)
template <BinaryOp op>
struct BinaryOpPrecedence {};
#ifdef BIN_OP_PREC
#error BIN_OP_PREC already defined
#endif
#define BIN_OP_PREC(op, prec) \
  template <> \
  struct BinaryOpPrecedence< BinaryOp:: op > { \
    static constexpr int value = prec ; \
  }

// * / %
BIN_OP_PREC(Multiply, 0);
BIN_OP_PREC(Divide, 0);
BIN_OP_PREC(Modulus, 0);
// + -
BIN_OP_PREC(Plus, 1);
BIN_OP_PREC(Minus, 1);
// << >>
BIN_OP_PREC(ShiftLeft, 2);
BIN_OP_PREC(ShiftRight, 2);
// < <= > >=
BIN_OP_PREC(Less, 3);
BIN_OP_PREC(LessEqual, 3);
BIN_OP_PREC(Greater, 3);
BIN_OP_PREC(GreaterEqual, 3);
// == !=
BIN_OP_PREC(Equal, 4);
BIN_OP_PREC(NotEqual, 4);
// &
BIN_OP_PREC(BinAnd, 5);
// ^
BIN_OP_PREC(BinXor, 6);
// |
BIN_OP_PREC(BinOr, 7);
// &&
BIN_OP_PREC(And, 8);
// ||
BIN_OP_PREC(Or, 9);

#undef BIN_OP_PREC

}
}

#endif // SIMPLEC_CORE_H
