#include <iostream>
#include <iomanip>
#include <exception>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <string>
#include <chrono>
#include <vector>
#include <utility>
#include <algorithm>

#include <boost/optional.hpp>
#include <boost/filesystem.hpp>
#include <boost/assert.hpp>

#include "Parser.h"

using simplec::parser::Parser;
using Clock = std::chrono::high_resolution_clock;
using std::chrono::duration_cast;
using nanos = std::chrono::nanoseconds;

namespace fs = boost::filesystem;

struct BasicErrorHandler : simplec::parser::ErrorHandler {
  virtual void parseError(const simplec::SourcePosition& pos,
                          const std::string& message) override
  {
    std::cerr << "Parse error at " << pos << ": " << message << std::endl;
  }
  virtual void typeError(const simplec::SourcePosition& pos,
                         const std::string& message) override
  {
    std::cerr << "Type error at " << pos << ": " << message << std::endl;
  }
};

boost::optional<std::string> readFile(const std::string& fileName) {
  using namespace std::string_literals;
  std::ifstream file(fileName);
  if (!file) {
    std::cerr << "Failed to open file: " << fileName << '\n';
    return boost::optional<std::string>();
  }
  std::stringstream ss;
  ss << file.rdbuf();
  if (!ss || !file) {
    if (!file)
      std::cerr << "Error reading file: " << fileName << '\n';
    else if (file.tellg() == static_cast<std::streamsize>(0) &&
             file.peek() == std::ifstream::traits_type::eof())
      return boost::make_optional(""s);
    return boost::optional<std::string>();
  }
  file.close();

  return boost::make_optional(ss.str());
}

bool testParser(Parser& parser, const std::string& fileName) {
  BasicErrorHandler eh;

  std::cout << "Testing file: " << fileName << '\n';
  auto source = readFile(fileName);
  if (!source) return false;

  //std::cout << "Program source:\n\"" << *source << '\"' << std::endl;
  //std::system("pause");

  auto start = Clock::now();
  auto program = parser.parse(*source, eh);
  auto elapsed = Clock::now() - start;
  if (program)
    std::cout << "Parse succeeded\n";
  else
    std::cout << "Parse failed\n";

  std::cout << "Took: " << duration_cast<nanos>(elapsed).count() << " ns\n" << std::endl;
  return program ? true : false;
}

void runTests(Parser& parser) {
  fs::path currentDir = ".";
  assert(fs::is_directory(currentDir));

  std::vector<std::pair<std::string, bool>> testFiles;
  for (auto&& x : fs::directory_iterator(currentDir)) {
    auto path = x.path();
    if (fs::is_regular_file(path) && path.extension() == ".simplec") {
      // If name doesn't contain invalid, assume it's valid.
      bool isValid = path.stem().string().find("invalid") == std::string::npos;
      testFiles.emplace_back(path.filename().string(), isValid);
    }
  }
  std::cout << "identified test files:\n";
  for (auto& file : testFiles) {
    std::cout << "  " << file.first << " expected: " << (file.second ? "pass" : "fail") << '\n';
  }
  std::cout << "\n" << std::endl;

  // Sort so that invalid tests are ran first,
  // and tests are ran in lexicographical order of filename
  std::sort(testFiles.begin(), testFiles.end(),
  [](const auto& x, const auto& y) -> bool {
    return x.second < y.second || (x.second == y.second && x.first < y.first);
  });

  for (auto& file : testFiles) {
    if (file.second != testParser(parser, file.first)) {
      std::cerr << "****** TEST FAILED ******\n\n";
      std::system("pause");
      std::cout << std::endl;
    }
  }
}

int main() {
  std::ios::sync_with_stdio(false);
  try {
    auto start = Clock::now();
    Parser parser;
    auto elapsed = Clock::now() - start;
    std::cout << "Constructing parser took: "
              << duration_cast<nanos>(elapsed).count() << " ns" << std::endl;
    
    runTests(parser);
    //testParser(parser, "invalid_typecheck.simplec");
    std::system("pause");
  } catch (const std::exception& e) {
    std::cerr << "Exception: " << e.what() << std::endl;
    std::system("pause");
  } catch (...) {
    std::cerr << "Unknown exception occurred" << std::endl;
    std::system("pause");
  }
}
