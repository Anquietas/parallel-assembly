#pragma once
#ifndef SIMPLEC_EVALUATION_ENVIRONMENT_H
#define SIMPLEC_EVALUATION_ENVIRONMENT_H

#include <vector>
#include <map>
#include <utility>
#include <memory>

#include <boost/variant.hpp>
#include <boost/optional.hpp>

#include "SimpleC_Core.h"
#include "AST_Statements.h"
#include "SourceLocationInfo.h"
#include "TypeCheckerEnvironment.h"

namespace simplec {
namespace eval {

using typecheck::Identifier;
inline namespace {
using namespace typecheck::literals;
}
using typecheck::SimpleType;
using typecheck::ArrayType;
using typecheck::Type;
using typecheck::Void;
using typecheck::FunctionType;
using ast::bool_t;
using ast::int_t;
using ast::float_t;
using ast::char_t;
using ast::string_t;
using ast::SimpleValue;

class ArrayValue {
public:
  using Element = boost::variant<ast::SimpleValue, ArrayValue>;

private:
  std::vector<Element> m_contents;

  using const_iterator = std::vector<Element>::const_iterator;
  ArrayValue(const_iterator begin, const_iterator end);
public:
  ArrayValue() = default;
  ArrayValue(const ArrayValue& o) = default;
  ArrayValue(ArrayValue&& o) = default;
  ~ArrayValue() = default;

  ArrayValue& operator=(const ArrayValue& o) = default;
  ArrayValue& operator=(ArrayValue&& o) = default;

  const Element& operator[](std::size_t index) const { return m_contents[index]; }
  Element& operator[](std::size_t index) { return m_contents[index]; }

  std::size_t rank() const;
  std::size_t size() const noexcept { return m_contents.size(); }

  auto begin() const { return m_contents.cbegin(); }
  auto end() const { return m_contents.cend(); }

  void push_back(Element element);

  ArrayValue subarray(int_t offset, int_t length) const;
};

enum class ValueCategory {
  Simple, Array, Void,
};

using Value = boost::variant<SimpleValue, ArrayValue>;
ValueCategory category(const Value& value);
bool isSimple(const Value& value);
bool isArray(const Value& value);

using ReturnValue = boost::variant<Void, SimpleValue, ArrayValue>;
ValueCategory category(const ReturnValue& value);
bool isSimple(const ReturnValue& value);
bool isArray(const ReturnValue& value);
bool isVoid(const ReturnValue& value);

struct LibraryFunction {
  virtual void operator()(const std::vector<Value>& params, ReturnValue& result) const = 0;
};
struct StringMemFunction {
  static int_t length(const string_t& value);
  static string_t substr(const string_t& value, int_t begin, int_t length);
};
struct ArrayMemFunction {
  static int_t length(const ArrayValue& value);
  static ArrayValue subarray(const ArrayValue& value, int_t begin, int_t length);
};

class Environment {
  typecheck::Environment m_typeEnv;

  using UserOverloadTable = std::map<const FunctionType*, const ast::Function*>;
  using UserFuncTable = std::map<ast::Identifier, UserOverloadTable>;
  using LibFuncTable = std::map<ast::Identifier, std::unique_ptr<LibraryFunction>>;

  UserFuncTable m_userFunctions;
  LibFuncTable m_libFunctions;
public:
  Environment();
  Environment(const Environment& o) = default;
  Environment(Environment&& o) = default;
  ~Environment() = default;

  Environment& operator=(const Environment& o) = default;
  Environment& operator=(Environment&& o) = default;

  bool addUserFunction(const Identifier& id, FunctionType& type, const ast::Function& func);

  const ast::Function* lookupUserFunction(
    const Identifier& id,
    const std::vector<Type>& paramTypes) const;
  const LibraryFunction* lookupLibraryFunction(const Identifier& id) const;

  //typecheck::Environment& typeEnv() { return m_typeEnv; }
  const typecheck::Environment& typeEnv() const { return m_typeEnv; }
};

} // namespace eval
} // namespace simplec

#endif // SIMPLEC_EVALUATION_ENVIRONMENT_H
