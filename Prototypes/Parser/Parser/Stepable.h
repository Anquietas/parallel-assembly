#pragma once
#ifndef SIMPLEC_EVAL_STEPABLE_H
#define SIMPLEC_EVAL_STEPABLE_H

#include "ProgramState.h"

namespace simplec {
namespace eval {

struct Stepable {
protected:
  Stepable() = default;
  Stepable(const Stepable& o) = default;
  Stepable(Stepable&& o) = default;
  ~Stepable() = default;

  Stepable& operator=(const Stepable& o) = default;
  Stepable& operator=(Stepable&& o) = default;

public:
  virtual void step(ProgramState& state) const = 0;
};

}
}

#endif // SIMPLEC_EVAL_STEPABLE_H
