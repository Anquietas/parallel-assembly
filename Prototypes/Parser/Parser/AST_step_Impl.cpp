#include "AST_Expressions.h"

namespace simplec {
namespace ast {

void ArrayLookup::step(eval::ProgramState& state) const {
}
void FuncCall::step(eval::ProgramState& state) const {
}
void MemberExpression::step(eval::ProgramState& state) const {
}
void IncrExpression::step(eval::ProgramState& state) const {
}
void Expression::step(eval::ProgramState& state) const {
}
void CastExpression::step(eval::ProgramState& state) const {
}
void UnaryOpExpression::step(eval::ProgramState& state) const {
}
void BinaryOpExpression::step(eval::ProgramState& state) const {
}
void AssignExpression::step(eval::ProgramState& state) const {
}

}
}