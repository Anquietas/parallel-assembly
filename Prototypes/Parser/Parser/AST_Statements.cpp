#include "AST_Statements.h"

#include <utility>
#include <cassert>

namespace simplec {
namespace ast {

VariableDeclaration::VariableDeclaration(
  PrimitiveType type, bool isConst, Identifier id, Expression initializer)
  : SourceLocationInfo()
  , m_type(type)
  , m_const(isConst)
  , m_id(std::move(id))
  , m_initializer(std::move(initializer))
{}
std::ostream& operator<<(std::ostream& out, const VariableDeclaration& varDecl) {
  out << "VariableDeclaration{\n";
  out << "  type=" << varDecl.type() << ",\n";
  out << "  isConst=" << varDecl.isConst() << ",\n";
  out << "  id=" << varDecl.id() << ",\n";
  return out << "  initializer=" << varDecl.initializer() << "\n}";
}

BasicArrayInitializer::BasicArrayInitializer(variant_t value)
  : m_value(std::move(value))
{}
struct BasicArrayInitVisitor : boost::static_visitor<void> {
  std::ostream& out;

  BasicArrayInitVisitor(std::ostream& out_) : out(out_) {}
  BasicArrayInitVisitor(const BasicArrayInitVisitor&) = delete;
  BasicArrayInitVisitor& operator=(const BasicArrayInitVisitor&) = delete;

  template <typename T>
  void operator()(const std::vector<T>& exprs) const {
    for (const auto& expr : exprs)
      out << "    " << expr << ",\n";
  }
};
std::ostream& operator<<(std::ostream& out, const BasicArrayInitializer& arrayInit) {
  out << "BasicArrayInitializer{\n  value=[\n";
  boost::apply_visitor(BasicArrayInitVisitor(out), arrayInit.value());
  return out << "]\n}";
}

ArrayDeclaration::ArrayDeclaration(
  ArrayType<true> type,
  Identifier id,
  Initializer initializer)
  : SourceLocationInfo()
  , m_type(std::move(type))
  , m_id(std::move(id))
  , m_initializer(std::move(initializer))
{}
std::ostream& operator<<(std::ostream& out, const ArrayDeclaration& arrayDecl) {
  out << "ArrayDeclaration{\n";
  out << "  type=" << arrayDecl.type() << ",\n";
  out << "  id=" << arrayDecl.id() << ",\n";
  out << "  initializer=";
  if (arrayDecl.initializer())
    out << *arrayDecl.initializer();
  return out << "\n}";
}

GlobalArrayDeclaration::GlobalArrayDeclaration(
  ArrayType<false> type,
  Identifier id,
  BasicArrayInitializer initializer)
  : SourceLocationInfo()
  , m_type(std::move(type))
  , m_id(std::move(id))
  , m_initializer(std::move(initializer))
{}
std::ostream& operator<<(std::ostream& out, const GlobalArrayDeclaration& arrayDecl) {
  out << "GlobalArrayDeclaration{\n";
  out << "  type=" << arrayDecl.type() << ",\n";
  out << "  id=" << arrayDecl.id() << ",\n";
  return out << "  initializer=" << arrayDecl.initializer() << "\n}";
}

ExpressionStatement::ExpressionStatement(AssignExpression statement)
  : SourceLocationInfo()
  , m_statement(std::move(statement))
{}
ExpressionStatement::ExpressionStatement(IncrExpression statement)
  : SourceLocationInfo()
  , m_statement(std::move(statement))
{}
ExpressionStatement::ExpressionStatement(FuncCall statement)
  : SourceLocationInfo()
  , m_statement(std::move(statement))
{}

ExpressionStatement& ExpressionStatement::operator=(AssignExpression statement) {
  m_statement = std::move(statement);
  resetEndpoints();
  return *this;
}
ExpressionStatement& ExpressionStatement::operator=(IncrExpression statement) {
  m_statement = std::move(statement);
  resetEndpoints();
  return *this;
}
ExpressionStatement& ExpressionStatement::operator=(FuncCall statement) {
  m_statement = std::move(statement);
  resetEndpoints();
  return *this;
}

class ExpressionStatementSetEndpoints : public boost::static_visitor<void> {
  const SourcePosition& m_begin;
  const SourcePosition& m_end;

public:
  ExpressionStatementSetEndpoints(const SourcePosition& begin, const SourcePosition& end)
    : m_begin(begin)
    , m_end(end)
  {}

  void operator()(AssignExpression& assignExpr) const {
    assignExpr.setEndpoints(m_begin, m_end);
  }
  void operator()(IncrExpression& incrExpr) const {
    incrExpr.setEndpoints(m_begin, m_end);
  }
  void operator()(const FuncCall&) const {}
};

struct ExpressionStatementResetEndpoints : boost::static_visitor<void> {
  void operator()(AssignExpression& assignExpr) const {
    assignExpr.resetEndpoints();
  }
  void operator()(IncrExpression& incrExpr) const {
    incrExpr.resetEndpoints();
  }
  void operator()(const FuncCall&) const {}
};

void ExpressionStatement::setEndpoints(const SourcePosition& begin, const SourcePosition& end) {
  this->SourceLocationInfo::setEndpoints(begin, end);
  boost::apply_visitor(ExpressionStatementSetEndpoints(begin, end), m_statement);
}
void ExpressionStatement::resetEndpoints() noexcept {
  this->SourceLocationInfo::resetEndpoints();
  boost::apply_visitor(ExpressionStatementResetEndpoints(), m_statement);
}

std::ostream& operator<<(std::ostream& out, const ExpressionStatement& statement) {
  //return out << "ExpressionStatement{statement=" << statement.statement() << "}";
  return out;
}

std::ostream& operator<<(std::ostream& out, const LoopControl& loopCtrl) {
  switch (loopCtrl) {
  case LoopControl::Break:
    out << "Break";
    break;
  case LoopControl::Continue:
    out << "Continue";
    break;
  default:
    assert(false);
  }
  return out;
}

ReturnStatement::ReturnStatement(boost::optional<Expression> returnExpr)
  : SourceLocationInfo()
  , m_returnExpr(std::move(returnExpr))
{}
std::ostream& operator<<(std::ostream& out, const ReturnStatement& returnStmt) {
  out << "ReturnStatement{";
  if (returnStmt.hasExpression())
    out << "expression=" << returnStmt.expression();
  return out << "}";
}

IfStatement::IfStatement(
  Expression condition,
  std::vector<Statement> trueStatements,
  std::vector<Statement> falseStatements)
  : m_condition(std::move(condition))
  , m_trueStatements(std::move(trueStatements))
  , m_falseStatements(std::move(falseStatements))
{}
std::ostream& operator<<(std::ostream& out, const IfStatement& ifStmt) {
  out << "IfStatement{\n";
  out << "  condition=" << ifStmt.condition() << ",\n";
  out << "  numTrueStatements=" << ifStmt.numTrueStatements() << ",\n";
  out << "  trueStatements=[\n";
  for (std::size_t i = 0; i < ifStmt.numTrueStatements(); ++i)
    out << "    " << ifStmt.trueStatement(i) << ",\n";
  out << "  ],\n  numFalseStatements=" << ifStmt.numFalseStatements() << ",\n";
  out << "  falseStatements=[\n";
  for (std::size_t i = 0; i < ifStmt.numFalseStatements(); ++i)
    out << "    " << ifStmt.falseStatement(i) << ",\n";
  return out << "  ]\n}";
}

WhileLoop::WhileLoop(Expression condition, std::vector<Statement> statements)
  : m_condition(std::move(condition))
  , m_statements(std::move(statements))
{}
std::ostream& operator<<(std::ostream& out, const WhileLoop& whileLoop) {
  out << "WhileLoop{\n";
  out << "  condition=" << whileLoop.condition() << ",\n";
  out << "  numStatements=" << whileLoop.numStatements() << ",\n";
  out << "  statements=[\n";
  for (std::size_t i = 0; i < whileLoop.numStatements(); ++i)
    out << "    " << whileLoop.statement(i) << ",\n";
  return out << "  ]\n}";
}

ForLoop::ForLoop(
  boost::optional<ForInitStatement> initStatement,
  boost::optional<Expression> stepCondition,
  boost::optional<ExpressionStatement> stepStatement,
  std::vector<Statement> statements)
  : m_initStatement(std::move(initStatement))
  , m_stepCondition(std::move(stepCondition))
  , m_stepStatement(std::move(stepStatement))
  , m_statements(std::move(statements))
{}
std::ostream& operator<<(std::ostream& out, const ForLoop& forLoop) {
  out << "ForLoop{\n";

  out << "  initStatement=";
  if (forLoop.hasInitStatement())
    out << forLoop.initStatement();
  else
    out << "{}";

  out << ",\n  stepCondition=";
  if (forLoop.hasStepCondition())
    out << forLoop.stepCondition();
  else
    out << "{}";

  out << ",\n  stepStatement=";
  if (forLoop.hasStepStatement())
    out << forLoop.stepStatement();
  else
    out << "{}";

  out << ",\n  numStatements=" << forLoop.numStatements() << ",\n";
  out << "  statements=[\n";
  for (std::size_t i = 0; i < forLoop.numStatements(); ++i)
    out << "    " << forLoop.statement(i) << ",\n";
  return out << "  ]\n}";
}

ForeachElemDeclaration::ForeachElemDeclaration(
  PrimitiveType type, bool isConst, Identifier id)
  : SourceLocationInfo()
  , m_type(type)
  , m_const(isConst)
  , m_id(std::move(id))
{}
std::ostream& operator<<(std::ostream& out, const ForeachElemDeclaration& elemDecl) {
  out << "ForeachElemDeclaration{\n";
  out << "  type=" << elemDecl.type() << ",\n";
  out << "  isConst=" << elemDecl.isConst() << ",\n";
  return out << "  id=" << elemDecl.id() << "\n}";
}

ForeachLoop::ForeachLoop(
  ForeachElemDeclaration elemDeclaration,
  Expression rangeExpression,
  std::vector<Statement> statements)
  : m_elemDeclaration(std::move(elemDeclaration))
  , m_rangeExpression(std::move(rangeExpression))
  , m_statements(std::move(statements))
{}
std::ostream& operator<<(std::ostream& out, const ForeachLoop& foreachLoop) {
  out << "ForeachLoop{\n";
  out << "  elemDeclaration=" << foreachLoop.elemDeclaration() << ",\n";
  out << "  rangeExpression=" << foreachLoop.rangeExpression() << ",\n";
  out << "  numStatements=" << foreachLoop.numStatements() << ",\n";
  out << "  statements=[\n";
  for (std::size_t i = 0; i < foreachLoop.numStatements(); ++i)
    out << "    " << foreachLoop.statement(i) << ",\n";
  return out << "  ]\n}";
}

Statement::Statement(SimpleStatement statement)
  : SourceLocationInfo()
  , m_statement(std::move(statement))
{}
Statement::Statement(IfStatement statement)
  : SourceLocationInfo()
  , m_statement(std::move(statement))
{}
Statement::Statement(WhileLoop statement)
  : SourceLocationInfo()
  , m_statement(std::move(statement))
{}
Statement::Statement(ForLoop statement)
  : SourceLocationInfo()
  , m_statement(std::move(statement))
{}
Statement::Statement(ForeachLoop statement)
  : SourceLocationInfo()
  , m_statement(std::move(statement))
{}

Statement& Statement::operator=(SimpleStatement statement) {
  m_statement = std::move(statement);
  resetEndpoints();
  return *this;
}
Statement& Statement::operator=(IfStatement statement) {
  m_statement = std::move(statement);
  resetEndpoints();
  return *this;
}
Statement& Statement::operator=(WhileLoop statement) {
  m_statement = std::move(statement);
  resetEndpoints();
  return *this;
}
Statement& Statement::operator=(ForLoop statement) {
  m_statement = std::move(statement);
  resetEndpoints();
  return *this;
}
Statement& Statement::operator=(ForeachLoop statement) {
  m_statement = std::move(statement);
  resetEndpoints();
  return *this;
}

std::ostream& operator<<(std::ostream& out, const Statement& statement) {
  return out << "Statement{statement=" << statement.statement() << "}";
}

Parameter::Parameter(PrimitiveType type, Identifier id)
  : SourceLocationInfo()
  , m_type(type)
  , m_id(std::move(id))
{}
Parameter::Parameter(ArrayType<true> type, Identifier id)
  : SourceLocationInfo()
  , m_type(std::move(type))
  , m_id(std::move(id))
{}
std::ostream& operator<<(std::ostream& out, const Parameter& param) {
  out << "Parameter{\n";
  out << "  type=" << param.type() << ",\n";
  return out << "  id=" << param.id() << "\n}";
}

Function::Function(
  ReturnType returnType,
  Identifier id,
  std::vector<Parameter> params,
  std::vector<Statement> statements)
  : SourceLocationInfo()
  , m_returnType(std::move(returnType))
  , m_id(std::move(id))
  , m_params(std::move(params))
  , m_statements(std::move(statements))
{}
std::ostream& operator<<(std::ostream& out, const Function& funcDef) {
  out << "Function{\n";
  out << "  returnType=" << funcDef.returnType() << ",\n";
  out << "  id=" << funcDef.id() << ",\n";
  out << "  numParams=" << funcDef.numParams() << ",\n";
  out << "  params=[\n";
  for (std::size_t i = 0; i < funcDef.numParams(); ++i)
    out << "    " << funcDef.param(i) << ",\n";
  out << "  ],\n  numStatements=" << funcDef.numStatements() << ",\n";
  out << "  statements=[\n";
  for (std::size_t i = 0; i < funcDef.numStatements(); ++i)
    out << "    " << funcDef.statement(i) << ",\n";
  return out << "  ]\n}";
}

Comment::Comment(std::string text)
  : SourceLocationInfo()
  , m_text(std::move(text))
{}
std::ostream& operator<<(std::ostream& out, const Comment& comment) {
  return out << "Comment{text=\"" << comment.text() << "\"}";
}

}
}
