#pragma once
#ifndef SIMPLEC_PARSER_COMMENT_GRAMMAR_H
#define SIMPLEC_PARSER_COMMENT_GRAMMAR_H

#define BOOST_SPIRIT_USE_PHOENIX_V3 1

#ifdef _MSC_VER
#pragma warning(push)
// Redefinitions - internal problem with spirit, compiles anyway.
#pragma warning(disable:4348)
// Declaration hides outer declaration
#pragma warning(disable:4459)
// Unreferenced formal parameter
#pragma warning(disable:4100)
#endif 

#include <boost/spirit/include/qi_core.hpp>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include "ProgramStructure.h"
#include "ErrorDiagnostic.h"

namespace simplec {
namespace parser {
namespace detail {

namespace qi {
using namespace boost::spirit::qi;
}
namespace phx {
using namespace boost::phoenix;
}

#define MAKE_NON_COPY_MOVEABLE(T) \
  T (const T &) = delete; \
  T ( T &&) = delete; \
  T& operator=(const T &) = delete; \
  T& operator=( T &&) = delete

template <typename Iterator>
class CommentSkipper : public qi::grammar<Iterator> {
  qi::rule<Iterator> start;
public:
  CommentSkipper();
  MAKE_NON_COPY_MOVEABLE(CommentSkipper);
};

template <typename Iterator>
class CommentGrammar : public qi::grammar<Iterator, CommentSkipper<Iterator>, ast::ProgramStructure()> {
  using base_type = qi::grammar<Iterator, CommentSkipper<Iterator>, ast::ProgramStructure()>;
  template <typename T = qi::unused_type, typename skipper = CommentSkipper<Iterator>>
  using rule = qi::rule<Iterator, skipper, T>;

  rule<ast::ProgramStructure()> program;
public:
  // Assumes that diagnostic will outlive this object
  // - stores a reference in error handler
  CommentGrammar(ErrorDiagnostic& diagnostic);
  MAKE_NON_COPY_MOVEABLE(CommentGrammar);
};

#undef MAKE_NON_COPY_MOVEABLE

}
}
}

#endif // SIMPLEC_PARSER_COMMENT_GRAMMAR_H
