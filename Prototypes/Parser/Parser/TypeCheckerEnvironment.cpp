#include "TypeCheckerEnvironment.h"

namespace simplec {
namespace typecheck {

Environment::Environment()
  : m_functions()
  , m_stringMemFuncs()
  , m_arrayMemFuncs()
  , m_globals()
  , m_locals()
{
  using namespace literals;

  auto intNoParams = FunctionType(
    SimpleType(PrimitiveType::Int, true),

    std::vector<Type>());
  // Normal library functions

  m_functions.emplace("numProcessors"_id, intNoParams);

  m_functions.emplace("inputSize"_id, intNoParams);
  m_functions.emplace("inputSize"_id, FunctionType(
    SimpleType(PrimitiveType::Int, true),
    std::vector<Type>{ SimpleType(PrimitiveType::Int, false) }));

  m_functions.emplace("input"_id, FunctionType(DeferredType(), std::vector<Type>()));
  m_functions.emplace("input"_id, FunctionType(
    DeferredType(),
    std::vector<Type>{ SimpleType(PrimitiveType::Int, false) }));

  m_functions.emplace("sync"_id, FunctionType(Void(), std::vector<Type>()));

  auto makeRandom = [](PrimitiveType type) -> FunctionType {
    auto simpleType = SimpleType(type, false);
    return FunctionType(SimpleType(type, true), std::vector<Type>{ simpleType, simpleType });
  };
  m_functions.emplace("random"_id, makeRandom(PrimitiveType::Bool));
  m_functions.emplace("random"_id, makeRandom(PrimitiveType::Int));
  m_functions.emplace("random"_id, makeRandom(PrimitiveType::Float));
  m_functions.emplace("random"_id, makeRandom(PrimitiveType::Char));

  // Member functions for string

  m_stringMemFuncs.emplace("length"_id, intNoParams);
  m_stringMemFuncs.emplace("substr"_id, FunctionType(
    SimpleType(PrimitiveType::String, true),
    std::vector<Type>{
    SimpleType(PrimitiveType::Int, false),
      SimpleType(PrimitiveType::Int, false),
  }));

  // Member functions for array
  m_arrayMemFuncs.emplace("length"_id, intNoParams);
  m_arrayMemFuncs.emplace("subarray"_id, FunctionType(
    DeferredType(), // placeholder - doesn't actually need to wait til runtime
    std::vector<Type>{
    SimpleType(PrimitiveType::Int, false),
      SimpleType(PrimitiveType::Int, false),
  }));
}

bool Environment::addFunction(const Identifier& id, FunctionType& type) {
  auto range = lookupFunction(id);
  if (range.first == range.second)
    // Identifier not already used for a function name
    m_functions.emplace_hint(range.second, id, std::move(type));
  else {
    // Identifier matches a previous function definition
    // Check for duplicates
    auto iter = std::find_if(range.first, range.second,
      [&type](const auto& t) -> bool {
      return type.paramsMatch(t.second);
    });
    if (iter != range.second)
      return false;
    m_functions.emplace_hint(range.second, id, std::move(type));
  }
  return true;
}
bool Environment::addGlobal(const Identifier& id, Type type) {
  auto pair = m_globals.emplace(id, std::move(type));
  return pair.second;
}
bool Environment::addLocal(const Identifier& id, Type type) {
  assert(!m_locals.empty());
  auto pair = m_locals.back().emplace(id, std::move(type));
  return pair.second;
}

const FunctionType* Environment::lookupFunction(
  const Identifier& id,
  const std::vector<Type>& paramTypes) const
{
  auto range = lookupFunction(id);
  auto iter = std::find_if(range.first, range.second,
    [&paramTypes](const auto& pair) -> bool {
      return pair.second.paramsMatch(paramTypes);
    });
  if (iter == range.second)
    return nullptr;
  else
    return &(iter->second);
}
auto Environment::lookupFunction(const Identifier& id) const -> FuncMatch {
  return m_functions.equal_range(id);
}
boost::optional<FunctionType> Environment::lookupLibraryFunction(
  const Identifier& id,
  const std::vector<Type>& paramTypes) const
{
  if (id.name() == "print")
    return boost::make_optional(FunctionType(Void(), paramTypes));
  else if (id.name() == "output") {
    if (paramTypes.size() == 1)
      return boost::make_optional(FunctionType(Void(), paramTypes));
    else if (paramTypes.size() == 2) {
      if (isSimple(paramTypes[0])) {
        auto& firstType = boost::get<SimpleType>(paramTypes[0]);
        if (firstType.type() == PrimitiveType::Int)
          return boost::make_optional(FunctionType(Void(), paramTypes));
      } else if (isDeferred(paramTypes[0]))
        return boost::make_optional(FunctionType(Void(), paramTypes));
    }
  }
  return boost::optional<FunctionType>();
}
const FunctionType* Environment::lookupStringMemFunc(const Identifier& id) const {
  auto iter = m_stringMemFuncs.find(id);
  if (iter != m_stringMemFuncs.end())
    return &(iter->second);
  else
    return nullptr;
}
boost::optional<FunctionType> Environment::lookupArrayMemFunc(
  ArrayType type,
  const Identifier& id) const
{
  auto iter = m_arrayMemFuncs.find(id);
  if (iter != m_arrayMemFuncs.end()) {
    auto funcType = iter->second;

    assert(type.rank() > 0);
    // Mark the first extent as unknown - depends on exact arguments
    type.setExtent(0, 0);
    funcType.setReturnType(type);

    return boost::make_optional(funcType);
  } else
    return boost::optional<FunctionType>();
}
const Type* Environment::lookupVariable(const Identifier& id) const {
  // Search for name in local variables
  auto scopeIter = m_locals.rbegin();
  auto scopeEnd = m_locals.rend();

  Scope::const_iterator result;
  bool found = false;
  for (; scopeIter != scopeEnd; ++scopeIter) {
    auto iter = scopeIter->find(id);
    if (iter != scopeIter->end()) {
      result = iter;
      found = true;
      break;
    }
  }
  if (!found) {
    // Not a local variable, try globals
    auto iter = m_globals.find(id);
    if (iter != m_globals.end()) {
      result = iter;
      found = true;
    }
  }
  if (found)
    return &(result->second);
  else
    return nullptr;
}

void Environment::scopePush() {
  m_locals.emplace_back();
}
void Environment::scopePop() {
  assert(!m_locals.empty());
  m_locals.pop_back();
}

}
}
