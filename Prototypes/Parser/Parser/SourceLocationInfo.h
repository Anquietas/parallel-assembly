#pragma once
#ifndef SIMPLEC_SOURCE_LOCATION_INFO_H
#define SIMPLEC_SOURCE_LOCATION_INFO_H

#include <iostream>
#include <cstddef>

namespace simplec {

class SourcePosition {
  std::size_t m_line, m_column;
public:
  SourcePosition() noexcept = default;
  SourcePosition(std::size_t line, std::size_t column) noexcept
    : m_line(line), m_column(column)
  {}
  SourcePosition(const SourcePosition& o) noexcept = default;
  SourcePosition(SourcePosition&& o) noexcept = default;
  ~SourcePosition() noexcept = default;

  SourcePosition& operator=(const SourcePosition& o) noexcept = default;
  SourcePosition& operator=(SourcePosition&& o) noexcept = default;

  std::size_t line() const noexcept { return m_line; }
  std::size_t column() const noexcept { return m_column; }
};
std::ostream& operator<<(std::ostream& out, const SourcePosition& pos);

class SourceLocationInfo {
  SourcePosition m_begin, m_end;

public:
  SourceLocationInfo() noexcept = default;
  SourceLocationInfo(const SourceLocationInfo& o) noexcept = default;
  SourceLocationInfo(SourceLocationInfo&& o) noexcept = default;
  ~SourceLocationInfo() noexcept = default;

  SourceLocationInfo& operator=(const SourceLocationInfo& o) noexcept = default;
  SourceLocationInfo& operator=(SourceLocationInfo&& o) noexcept = default;

  virtual void setEndpoints(const SourcePosition& begin, const SourcePosition& end) {
    m_begin = begin;
    m_end = end;
  }
  virtual void resetEndpoints() noexcept {
    *this = SourceLocationInfo();
  }
  const SourcePosition& begin() const noexcept { return m_begin; }
  const SourcePosition& end() const noexcept { return m_end; }
};

}

#endif //SIMPLEC_SOURCE_LOCATION_INFO_H
