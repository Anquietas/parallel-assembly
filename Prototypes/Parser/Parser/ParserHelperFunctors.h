#pragma once
#ifndef SIMPLEC_PARSER_HELPER_FUNCTORS_H
#define SIMPLEC_PARSER_HELPER_FUNCTORS_H

#include "ParserInternalTypes.h"
#include "ProgramStructure.h"

namespace simplec {
namespace parser {
namespace detail {

struct StringLitExpressionHelper {
  ast::Expression operator()(
    ast::string_t strLit,
    boost::optional<ast::FuncCall> memFunc) const;
};

struct IdentifierExpressionHelper {
  ast::Expression operator()(
    ast::Identifier identifier,
    ParamsOrSubscriptsMemIncr expr) const;
};

struct PreIncrExpressionHelper {
  ast::IncrExpression operator()(ast::IncrOp op, VarOrArrayLookup expr) const;
};

struct ExpressionHelper {
  // unaryOpExpression
  inline ast::Expression operator()(ast::UnaryOp op, ast::Expression expr) const {
    return ast::Expression(ast::UnaryOpExpression(op, std::move(expr)));
  }
  // binaryExpr1 to expression
  ast::Expression operator()(
    ast::Expression left,
    std::vector<fus::vector2<ast::BinaryOp, ast::Expression>> rest) const;
};

struct AssignExpressionHelper {
  ast::ExpressionStatement operator()(
    ast::Identifier identifier,
    SubscriptOrFuncCall expr) const;
};

struct VarDeclareHelper {
  ast::DeclarationStatement operator()(
    bool isConst,
    DeclarationStmt varDecl) const;
};

struct ForeachElemDeclHelper {
  inline ast::ForeachElemDeclaration operator()(
    bool isConst,
    ForeachElemDecl decl) const
  {
    using fus::at_c;
    return ast::ForeachElemDeclaration(at_c<0>(decl), isConst, at_c<1>(decl));
  }
};

struct ParamHelper {
  ast::Parameter operator()(
    ast::PrimitiveType type,
    ast::Identifier identifier,
    std::vector<ast::OptTypeSubscript> subscripts) const;
};

struct GlobalStatementHelper {
  // First option for globalStatement
  GlobalStatement operator()(
    ast::PrimitiveType type,
    BasicGlobalStatement funcOrVarDecl) const;

  // Second option for globalStatement
  ast::GlobalDeclaration operator()(
    ast::PrimitiveType type,
    ast::Identifier identifier,
    GlobalVarInitializer varDecl) const;

  // Third option for globalStatement
  GlobalStatement operator()(
    ast::Identifier identifier,
    FuncParamAndBody funcDecl) const;
};

}
}
}

#endif // SIMPLEC_PARSER_HELPER_FUNCTORS_H
