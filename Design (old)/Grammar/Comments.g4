grammar Comments;

// Comments
COMMENT : '//' ~[\n]* '\n';

REST : .+? -> skip;

program : COMMENT*;