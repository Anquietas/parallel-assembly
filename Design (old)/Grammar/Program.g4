grammar Program;

//
// Literals
//
BOOL_LIT : 'true' | 'false';
fragment DIGIT : [0-9];
INT_LIT : DIGIT+;
FLOAT_LIT : DIGIT+ '.' DIGIT* ([eE] [+\-]? DIGIT+)?;
fragment RAW_CHAR : ~['"\\] | '\\'[0'"n\\];
CHAR_LIT : '\'' RAW_CHAR '\'';
STRING_LIT : '"' RAW_CHAR* '"';

//
// Types
//
CONST : 'const';
VOID : 'void';
BOOL : 'bool';
INT : 'int';
FLOAT : 'float';
CHAR : 'char';
STRING : 'string';

// 
// Other tokens
// 
LB : '(';
RB : ')';
LSQB : '[';
RSQB : ']';
LCURLB : '{';
RCURLB : '}';
COMMA : ',';
COLON : ':';
SEMICOLON : ';';
RETURN : 'return';
BREAK : 'break';
CONTINUE : 'continue';
IF : 'if';
ELSE : 'else';
FOR : 'for';
FOREACH : 'foreach';
WHILE : 'while';

// Operators
INCR : '++';
DECR : '--';

DOT : '.';

PLUS : '+';
MINUS : '-';
NOT : '!';
BIN_NOT : '~';
MULT : '*';
DIV : '/';
MOD : '%';
SHIFT_LEFT : '<<';
SHIFT_RIGHT : '>>';
LESS : '<';
LESS_EQ : '<=';
GREATER : '>';
GREATER_EQ : '>=';
EQ : '==';
NOT_EQ : '!=';
BIN_AND : '&';
BIN_XOR : '^';
BIN_OR : '|';
AND : '&&';
OR : '||';

ASSIGN : '=';
ASSIGN_PLUS : '+=';
ASSIGN_MINUS : '-=';
ASSIGN_MULT : '*=';
ASSIGN_DIV : '/=';
ASSIGN_MOD : '%=';
ASSIGN_SHIFT_LEFT : '<<=';
ASSIGN_SHIFT_RIGHT : '>>=';
ASSIGN_BIT_AND : '&=';
ASSIGN_BIT_XOR : '^=';
ASSIGN_BIT_OR : '|=';

IDENTIFIER : [a-zA-Z_] [a-zA-Z_0-9]*;

// Stuff to skip
WS : [ \t\r\n\f]+ -> skip;
COMMENT : '//' ~[\n]* '\n' -> skip;

//
// Primitives
//
// Make sure literals are displayed with their interpreted type
boolLiteral : BOOL_LIT;
intLiteral : INT_LIT;
floatLiteral : FLOAT_LIT;
charLiteral : CHAR_LIT;
stringLiteral : STRING_LIT;
simpleLiteral : boolLiteral | intLiteral | floatLiteral | charLiteral;
primitiveType : BOOL | INT | FLOAT | CHAR | STRING;
typeSubscript : LSQB (intLiteral | IDENTIFIER)? RSQB;
subscript : LSQB nonAssignExpr RSQB;

//
// Expressions
//
exprList : nonAssignExpr (COMMA nonAssignExpr)*;
funcCall : LB exprList? RB;

memberOp : DOT IDENTIFIER funcCall;
incrOp : INCR | DECR;
basicExpr1 : funcCall
           | subscript* (incrOp | memberOp)?;
basicExpr2 : incrOp IDENTIFIER subscript*;
basicExpr : simpleLiteral
          | stringLiteral memberOp?
          | LB castOrBrackets
          | IDENTIFIER basicExpr1
          | basicExpr2;
castOrBrackets : primitiveType RB basicExpr
               | nonAssignExpr RB;

// All same precedence
unaryOp : MINUS | NOT | BIN_NOT;
// Highest level precedence
exprP1 : unaryOp? basicExpr;

binaryOpP1 : MULT | DIV | MOD;
exprP2 : exprP1 (binaryOpP1 exprP1)*;

binaryOpP2 : PLUS | MINUS;
exprP3 : exprP2 (binaryOpP2 exprP2)*;

binaryOpP3 : SHIFT_LEFT | SHIFT_RIGHT;
exprP4 : exprP3 (binaryOpP3 exprP3)*;

binaryOpP4 : LESS | LESS_EQ | GREATER | GREATER_EQ;
exprP5 : exprP4 (binaryOpP4 exprP4)*;

binaryOpP5 : EQ | NOT_EQ;
exprP6 : exprP5 (binaryOpP5 exprP5)*;

//binaryOpP6 : BIN_AND;
exprP7 : exprP6 (BIN_AND exprP6)*;

//binaryOpP7 : BIN_XOR;
exprP8 : exprP7 (BIN_XOR exprP7)*;

//binaryOpP8 : BIN_OR;
exprP9 : exprP8 (BIN_OR exprP8)*;

//binaryOpP9 : AND;
exprP10 : exprP9 (AND exprP9)*;

//binaryOpP10 : OR;
nonAssignExpr : exprP10 (OR exprP10)*;

assignOp : ASSIGN | ASSIGN_PLUS | ASSIGN_MINUS | ASSIGN_MULT | ASSIGN_DIV
         | ASSIGN_SHIFT_LEFT | ASSIGN_SHIFT_RIGHT | ASSIGN_BIT_AND
         | ASSIGN_BIT_XOR | ASSIGN_BIT_OR;

expr0 : assignOp nonAssignExpr | incrOp;
expr1 : subscript* expr0 | funcCall;
expr : IDENTIFIER expr1 | basicExpr2;

//
// Statements
//
arrayInit0 : exprList | arrayInit1 (COMMA arrayInit1)*;
// Arrays can either be initialized in-place or with the result of a function
arrayInit1 : LCURLB arrayInit0 RCURLB;
arrayInit : arrayInit1 | ASSIGN IDENTIFIER funcCall;
varDeclare0 : ASSIGN nonAssignExpr
            | typeSubscript+ arrayInit?;
varDeclare : CONST? primitiveType IDENTIFIER varDeclare0;

// Const values don't make much sense in a for loop
forInit : primitiveType IDENTIFIER ASSIGN nonAssignExpr | expr;
condition : nonAssignExpr;
statement : varDeclare SEMICOLON
          | expr SEMICOLON
          | RETURN nonAssignExpr? SEMICOLON
          | BREAK SEMICOLON
          | CONTINUE SEMICOLON
          // if (condition) statementGroup
          | IF LB condition RB statementGroup (ELSE statementGroup)?
          // for (init; condition; step) statementGroup
          | FOR LB forInit SEMICOLON condition SEMICOLON expr RB statementGroup
          // foreach (element : container) statementGroup
          | FOREACH LB CONST? primitiveType IDENTIFIER COLON nonAssignExpr RB statementGroup
          // while (condition) statementGroup
          | WHILE LB condition RB statementGroup;
statementGroup : SEMICOLON | statement | LCURLB statement* RCURLB;

funcBody : LCURLB statement* RCURLB | SEMICOLON;
param : primitiveType IDENTIFIER typeSubscript*;
paramList : param (COMMA param)*;
funcDeclare : LB paramList? RB funcBody;

// Initialization required for globals
globalVarDeclare : ASSIGN nonAssignExpr
                 | typeSubscript+ arrayInit;
globalStatement0 : globalVarDeclare SEMICOLON
                 | funcDeclare;
globalStatement1 : IDENTIFIER globalStatement0
                 | typeSubscript+ IDENTIFIER funcDeclare;
globalStatement : primitiveType globalStatement1
                | CONST primitiveType IDENTIFIER globalVarDeclare SEMICOLON
                | VOID IDENTIFIER funcDeclare;

program : globalStatement*;